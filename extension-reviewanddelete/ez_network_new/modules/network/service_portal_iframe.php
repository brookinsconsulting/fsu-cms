<?php
/**
 * service_portal/iframe View
 *
 * @copyright Copyright (C) 1999-2010 eZ Systems AS. All rights reserved.
 * @license http://ez.no/licenses/gnu_gpl GNU GPLv2
 *
 */

include_once( 'extension/ez_network/lib/oauth/OAuth.php' );
include_once( 'extension/ez_network/classes/eznetoauthclientconsumeruser.php' );

$tpl         = eZTemplate::factory();
$currentUser = eZUser::currentUser();
$http        = eZHTTPTool::instance();
$hasAccessToken      = false;
$currentUserRemoteID = $currentUser->attribute('contentobject')->attribute('remote_id');
$clientConsumer      = eznetOAuthClientConsumerUser::fetchByRemoteId( $currentUserRemoteID );

if ( $clientConsumer instanceof eznetOAuthClientConsumerUser )
{
    try
    {
        $clientConsumer->oauthVerifyToken();
        $clientConsumer->oauthRedirect( 'oauth/server/login_redirect', array( 'redirect' => urlencode( eznetOAuthClientConsumerUser::getServerUrl() ) ) );
    }
    catch ( OAuthPhpException $e )
    {
        print( "Got an oauth exception:\n<br />\n" );
        var_dump( $e->getMessage() );
    }
    catch ( OAuthJsonException $e )
    {
        print( "Could not json decode response from server:\n<br />\n" );
        var_dump( $e->getMessage() );
    }
}
else
{
    $tpl->setVariable( 'user', $currentUser );
    echo $tpl->fetch( 'design:network/login.tpl' );
}

eZDB::checkTransactionCounter();
eZExecution::cleanExit();

?>
