<?php
//
// Definition of eZNetCrypt class
//
// Created on: <06-Jul-2005 14:07:28 hovik>
//
// ## BEGIN COPYRIGHT, LICENSE AND WARRANTY NOTICE ##
// SOFTWARE NAME: eZ Network
// SOFTWARE RELEASE: 4.4.0
// COPYRIGHT NOTICE: Copyright (C) 1999-2010 eZ Systems AS
// SOFTWARE LICENSE: GNU General Public License v2.0
// NOTICE: >
//   This program is free software; you can redistribute it and/or
//   modify it under the terms of version 2.0  of the GNU General
//   Public License as published by the Free Software Foundation.
//
//   This program is distributed in the hope that it will be useful,
//   but WITHOUT ANY WARRANTY; without even the implied warranty of
//   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//   GNU General Public License for more details.
//
//   You should have received a copy of version 2.0 of the GNU General
//   Public License along with this program; if not, write to the Free
//   Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
//   MA 02110-1301, USA.
//
//
// ## END COPYRIGHT, LICENSE AND WARRANTY NOTICE ##
//

/*! \file eznetcrypt.php
*/

/**
 * @deprecated Class is no longer doing anything as of php5
 */
class eZNetCrypt
{

    /**
     * Encrypt text using DES if encryption is enabled (it's not)
     *
     * @param string $text
     * @return string
    */
    public static function encrypt( $text )
    {
        if ( self::$enabled === false )
        {
            return $text;
        }

        return mcrypt_encrypt( MCRYPT_3DES, self::$key, $text, MCRYPT_MODE_CFB, self::$IV );
    }

    /**
     * Decrypt text using DES if encryption is enabled (it's not)
     *
     * @param string $text
     * @return string
    */
    public static function decrypt( $enc )
    {
        if ( self::$enabled === false )
        {
            return $enc;
        }

        return mcrypt_decrypt( MCRYPT_3DES, self::$key, $enc, MCRYPT_MODE_CFB, self::$IV );
    }

    /**
     * Generate IV values
    */
    private function setIV()
    {
//        self::$IVSize = mcrypt_get_iv_size( MCRYPT_XTEA, MCRYPT_MODE_ECB );
//        self::$IV = mcrypt_create_iv( self::$IVSize, MCRYPT_RAND );
    }

    private static $IVSize = null;
    private static $IV = null;
    private static $key = '*321 regninger 123*';

    private static $enabled = false;
}

?>
