<?php
// Audio Datatype 
// Written by Stuart Fenton of Grandmore.com
// Stuart Fenton @ Grandmore.com
//
//  12 August 2006
//
//  email: fats@grandmore.com
//  web:   http://www.grandmore.com
//
// Definition of FileDirectHandler class
//

/*!
  \class FileDirectHandler ezfiledirecthandler.php
  \ingroup eZBinaryHandlers
  \brief Handles file downloading by passing an URL directly to the file.

*/
include_once( "kernel/classes/datatypes/ezbinaryfile/ezbinaryfile.php" );
#include_once( "kernel/classes/ezbinaryfilehandler.php" );
include_once( "extension/audio/modules/audio/binaryfilehandler.php" );

define( "FILE_DIRECT_ID", 'ezfiledirect' );

class FileDirectHandler extends BinaryFileHandler
{
    function FileDirectHandler()
    {
        $this->BinaryFileHandler( FILE_DIRECT_ID, "direct download", BINARY_FILE_HANDLE_DOWNLOAD );
    }

    function handleFileDownload( &$contentObject, &$contentObjectAttribute, $type, $mimeData )
    {
        return BINARY_FILE_RESULT_OK;
    }

    /*!
     \reimp
     \return the direct download template suffix
    */
    function &viewTemplate( &$contentobjectAttribute )
    {
        return 'direct';
    }

}

?>
