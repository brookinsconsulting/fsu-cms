<?php
// Audio Datatype 
// Written by Stuart Fenton of Grandmore.com
// Stuart Fenton @ Grandmore.com
//
//  12 August 2006
//
//  email: fats@grandmore.com
//  web:   http://www.grandmore.com
//
// Definition of BinaryFileHandler class
//
//

/*!
 \group BinaryHandlers Binary file handlers
*/

/*!
  \class BinaryFileHandler binaryfilehandler.php
  \ingroup eZKernel
  \brief Interface for all binary file handlers

*/
include_once( "kernel/classes/datatypes/ezmedia/ezmedia.php" );
include_once( "extension/audio/datatypes/audio/audio.php" );
include_once( "extension/audio/datatypes/audio/audiopls.php" );

define( "BINARY_FILE_HANDLE_UPLOAD", 0x1 );
define( "BINARY_FILE_HANDLE_DOWNLOAD", 0x2 );

define( "BINARY_FILE_HANDLE_ALL", BINARY_FILE_HANDLE_UPLOAD |
                                     BINARY_FILE_HANDLE_DOWNLOAD );

define( "BINARY_FILE_TYPE_FILE", 'file' );
define( "BINARY_FILE_TYPE_MEDIA", 'media' );
define( "BINARY_FILE_TYPE_AUDIO", 'audio' );
define( "BINARY_FILE_TYPE_LINK", 'm3u' );
define( "BINARY_FILE_TYPE_RAM_LINK", 'ram' );
define( "BINARY_FILE_RESULT_OK", 1 );
define( "BINARY_FILE_RESULT_UNAVAILABLE", 2 );

class BinaryFileHandler
{
    function BinaryFileHandler( $identifier, $name, $handleType )
    {
        $this->Info = array();
        $this->Info['identifier'] = $identifier;
        $this->Info['name'] = $name;
        $this->Info['handle-type'] = $handleType;
    }

    function attributes()
    {
        return array_keys( $this->Info );
    }

    function hasAttribute( $attribute )
    {
        return isset( $this->Info[$attribute] );
    }

    function &attribute( $attribute )
    {
        if ( isset( $this->Info[$attribute] ) )
            return $this->Info[$attribute];
        return null;
    }

    /*!
     \return the suffix for the template name which will be used for attribute viewing.
     \note Default returns false which means no special template.
    */
    function &viewTemplate( &$contentobjectAttribute )
    {
        return false;
    }

    /*!
     \return the suffix for the template name which will be used for attribute viewing.
     \note Default returns false which means no special template.
    */
    function &editTemplate( &$contentobjectAttribute )
    {
        return false;
    }

    /*!
     \return the suffix for the template name which will be used for attribute viewing.
     \note Default returns false which means no special template.
    */
    function &informationTemplate( &$contentobjectAttribute )
    {
        return false;
    }

    /*!
     Figures out the filename from the binary object \a $binary.
     Currently supports eZBinaryFile, eZMedia, Audio and eZImageAliasHandler.
     \return \c false if no file was found.
     \param $returnMimeData If this is set to \c true then it will return a mime structure, otherwise it returns the filename.
    */
    function storedFilename( &$binary, $returnMimeData = false )
    {
        $origDir = eZSys::storageDirectory() . '/original';

        $class = get_class( $binary );
        $fileName = false;
        $originalFilename = false;
        if ( in_array( $class, array( 'ezbinaryfile', 'ezmedia', 'audio', 'audiopls'  ) ) )
        {
            $fileName = $origDir . "/" . $binary->attribute( 'mime_type_category' ) . '/'.  $binary->attribute( "filename" );
            $originalFilename = $binary->attribute( 'original_filename' );
        }
        else if ( $class == 'ezimagealiashandler' )
        {
            $alias = $binary->attribute( 'original' );
            if ( $alias )
                $fileName = $alias['url'];
            $originalFilename = $binary->attribute( 'original_filename' );
        }
        if ( $fileName )
        {
            $mimeData = eZMimeType::findByFileContents( $fileName );
            $mimeData['original_filename'] = $originalFilename;

            if ( !isSet( $mimeData['name'] ) )
                $mimeData['name'] = 'application/octet-stream';

            if ( $returnMimeData )
                return $mimeData;
            else
                return $mimeData['url'];
        }
        return false;
    }

    function handleUpload()
    {
        return false;
    }

    /*!
     \return the file object which corresponds to \a $contentObject and \a $contentObjectAttribute.
    */
    function downloadFileObject( &$contentObject, &$contentObjectAttribute )
    {
        $contentObjectAttributeID = $contentObjectAttribute->attribute( 'id' );
        $version = $contentObject->attribute( 'current_version' );
        $fileObject =& eZBinaryFile::fetch( $contentObjectAttributeID, $version );
        if ( $fileObject )
            return $fileObject;

        $fileObject =& Audio::fetch( $contentObjectAttributeID, $version );
        if ( $fileObject )
            return $fileObject;

        $fileObject =& eZMedia::fetch( $contentObjectAttributeID, $version );
        return $fileObject;
    }

    /*!
     \return the file object type which corresponds to \a $contentObject and \a $contentObjectAttribute.
    */
    function downloadType( &$contentObject, &$contentObjectAttribute )
    {
        $contentObjectAttributeID = $contentObjectAttribute->attribute( 'id' );
        $version = $contentObject->attribute( 'current_version' );

        $fileObject =& eZBinaryFile::fetch( $contentObjectAttributeID, $version );
        if ( $fileObject )
            return BINARY_FILE_TYPE_FILE;

        $fileObject =& Audio::fetch( $contentObjectAttributeID, $version );
        if ( $fileObject )
            return BINARY_FILE_TYPE_AUDIO;
 
        $fileObject =& AudioPls::fetch( $contentObjectAttributeID, $version );
        if ( $fileObject )
            return BINARY_FILE_TYPE_LINK;

        $fileObject =& eZMedia::fetch( $contentObjectAttributeID, $version );
        if ( $fileObject )
            return BINARY_FILE_TYPE_MEDIA;

		return false;
    }

    /*!
     \return the download url for the file object which corresponds to \a $contentObject and \a $contentObjectAttribute.
    */
    function downloadURL( &$contentObject, &$contentObjectAttribute )
    {
        $contentObjectID = $contentObject->attribute( 'id' );
        $contentObjectAttributeID = $contentObjectAttribute->attribute( 'id' );
        $downloadType = BinaryFileHandler::downloadType( $contentObject, $contentObjectAttribute );
        $downloadObject = BinaryFileHandler::downloadFileObject( $contentObject, $contentObjectAttribute );
        $name = '';
        switch ( $downloadType )
        {
            case BINARY_FILE_TYPE_FILE:
            {
                $name = $downloadObject->attribute( 'original_filename' );
            } break;
            case BINARY_FILE_TYPE_MEDIA:
            {
                $name = $downloadObject->attribute( 'original_filename' );
            } break;
            case BINARY_FILE_TYPE_AUDIO:
            {
                $name = $downloadObject->attribute( 'original_filename' );
            } break;

            default:
            {
                eZDebug::writeWarning( "Unknown binary file type '$downloadType'", 'BinaryFileHandler::downloadURL' );
            } break;
        }
        $url = "/content/download/$contentObjectID/$contentObjectAttributeID/$downloadType/$name";
        return $url;
    }

    function handleDownload( &$contentObject, &$contentObjectAttribute, $type )
    {
        include_once( 'lib/ezutils/classes/ezmimetype.php' );
        include_once( 'kernel/classes/datatypes/ezimage/ezimagealiashandler.php' );
        $contentObjectAttributeID = $contentObjectAttribute->attribute( 'id' );
        $version = $contentObject->attribute( 'current_version' );


		if ( $type == BINARY_FILE_TYPE_LINK )
		{
			// Check to see it the creates m3u file points at the correct server. If not re-write 
			// it before continuing with the download.
			$fileObject =& AudioPls::fetch( $contentObjectAttributeID, $version );
            $fileINI =& eZINI::instance( 'audio.ini' );
		    $siteURL = $fileINI->variable( 'LinkFileFileSettings', 'LinkFileSiteName' );
	        if ( $fileObject->attribute( 'sitename' ) !== $siteURL )
	        {
				AudioPls::createPlsFile( $contentObjectAttributeID, $version );
	        	$fileObject =& AudioPls::fetch( $contentObjectAttributeID, $version );
	    	}
		}
		elseif ( $type == BINARY_FILE_TYPE_AUDIO )
		{
	      $fileObject =& Audio::fetch( $contentObjectAttributeID, $version );
		}
		elseif ( $type == BINARY_FILE_TYPE_MEDIA )
		{
		    $fileObject =& eZMedia::fetch( $contentObjectAttributeID, $version );
		}

        if ( $fileObject === null )
            return BINARY_FILE_RESULT_UNAVAILABLE;

        $mimeData = $this->storedFilename( $fileObject, true );
        if ( !$mimeData )
            return BINARY_FILE_RESULT_UNAVAILABLE;

//         $fileObject->attribute( 'mime_type' );
        return $this->handleFileDownload( $contentObject, $contentObjectAttribute, $type, $mimeData );
    }

    function handleFileDownload( &$contentObject, &$contentObjectAttribute, $type, $mimeData )
    {
        return false;
    }

    function repositories()
    {
#		return array( 'kernel/classes/binaryhandlers' );
		return array( 'extension/audio/modules/audio' );
	}

    function &instance( $identifier = false )
    {
        if ( $identifier === false )
        {
            $fileINI =& eZINI::instance( 'audio.ini' );
            $identifier = $fileINI->variable( 'AudioBinaryFileSettings', 'Handler' );
        }
        $instance =& $GLOBALS['BinaryFileHandlerInstance-' . $identifier];
        if ( !isset( $instance ) )
        {
            $handlerDirectory = $identifier;
            $handlerFilename = $identifier . "handler.php";
            $repositories = BinaryFileHandler::repositories();
            foreach ( $repositories as $repository )
            {
#				$file = eZDir::path( array( $repository, $handlerDirectory, $handlerFilename ) );
				$file = eZDir::path( array( $repository, $handlerFilename ) );
                if ( file_exists( $file ) )
                {
                    include_once( $file );
                    $classname = $identifier . "handler";
                    $instance = new $classname();
                    break;
                }
                else
                    eZDebug::writeError( "Could not find binary file handler '$identifier'", 'BinaryFileHandler::instance' );
            }
        }
        return $instance;
    }

    /// \privatesection
    var $Info;
}

?>
