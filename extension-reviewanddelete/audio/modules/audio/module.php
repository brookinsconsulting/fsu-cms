<?php
// Audio Datatype 
// Written by Stuart Fenton of Grandmore.com
// Stuart Fenton @ Grandmore.com
//
//  12 August 2006
//
//  email: fats@grandmore.com
//  web:   http://www.grandmore.com
//
// Definition of Membership module
//


/*
$Module = array( "name" => "Audio",
                 "variable_params" => true,
                 "function" => array(
                     "script" => "process.php") );
*/


$Module = array( 'name' => 'Audio',
                 'variable_params' => true );

$ViewList['download'] = array(
    'functions' => array( 'read' ),
    'default_navigation_part' => 'ezcontentnavigationpart',
    'script' => 'download.php',
    'params' => array( 'ContentObjectID', 'ContentObjectAttributeID', 'FileType' ) );


/*
$ViewList['download'] = array(
    'functions' => array( 'read' ),
    'default_navigation_part' => 'ezcontentnavigationpart',
    'script' => 'download.php',
    'params' => array( 'ContentObjectID', 'ContentObjectAttributeID', 'FileType' ) );
*/

/*
$SiteAccess = array(
    'name'=> 'SiteAccess',
    'values'=> array(),
    'path' => 'classes/',
    'file' => 'ezsiteaccess.php',
    'class' => 'eZSiteAccess',
    'function' => 'siteAccessList',
    'parameter' => array()
    );
*/

#	$FunctionList['login'] = array( 'SiteAccess' => $SiteAccess );
#	$FunctionList['adminstrate'] = array( );


/*
$FunctionList['read'] = array( 'Class' => $ClassID,
                               'Section' => $SectionID,
                               'Owner' => $Assigned,
                               'Node' => $Node,
                               'Subtree' => $Subtree);
*/
$FunctionList['read'] = array( );

?>