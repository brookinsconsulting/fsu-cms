<?php
// Audio Datatype 
// Written by Stuart Fenton of Grandmore.com
// Stuart Fenton @ Grandmore.com
//
//  12 August 2006
//
//  email: fats@grandmore.com
//  web:   http://www.grandmore.com
//
// Created on: <19-May-2004 10:40:11 sf>
//

include_once( "kernel/classes/ezcontentobject.php" );
include_once( "kernel/classes/ezcontentobjectattribute.php" );
include_once( "kernel/classes/datatypes/ezbinaryfile/ezbinaryfile.php" );

#include_once( "kernel/classes/ezbinaryfilehandler.php" );
include_once( "extension/audio/modules/audio/binaryfilehandler.php" );
include_once( "kernel/classes/datatypes/ezmedia/ezmedia.php" );
include_once( "extension/audio/datatypes/audio/audio.php" );
include_once( "extension/audio/datatypes/audio/audiopls.php" );
//$tpl =& templateInit();

$contentObjectID = $Params['ContentObjectID'];
$contentObjectAttributeID = $Params['ContentObjectAttributeID'];

$contentObject = eZContentObject::fetch( $contentObjectID );
if ( !is_object( $contentObject ) )
{
    return $Module->handleError( EZ_ERROR_KERNEL_NOT_AVAILABLE );
}
$version = $contentObject->attribute( 'current_version' );


$contentObjectAttribute = eZContentObjectAttribute::fetch( $contentObjectAttributeID, $version, true );
if ( !is_object( $contentObjectAttribute ) )
{
    return $Module->handleError( EZ_ERROR_KERNEL_NOT_AVAILABLE );
}
$contentObjectID = $contentObjectAttribute->attribute( 'contentobject_id' );

if ( ! $contentObject->attribute( 'can_read' ) )
{
    return $Module->handleError( EZ_ERROR_KERNEL_ACCESS_DENIED );
}

//Identify the object to download from it's extension
//If it ends with 'pls' then download the plsaudio file otherwise download the 'audio' file
$fullFilename = $Parameters[3];
$fileName = preg_replace('/\.(.*)$/', "", $fullFilename ) ;
$fileExtension = str_replace( $fileName, "", $fullFilename );

$fileHandler =& BinaryFileHandler::instance();
if ( $fileExtension == ".mp3" )
{
	$result = $fileHandler->handleDownload( $contentObject, $contentObjectAttribute, BINARY_FILE_TYPE_AUDIO );
}
elseif ( $fileExtension == ".ra" )
{
	$result = $fileHandler->handleDownload( $contentObject, $contentObjectAttribute, BINARY_FILE_TYPE_AUDIO );
}
elseif ( $fileExtension == ".rm" )
{
	$result = $fileHandler->handleDownload( $contentObject, $contentObjectAttribute, BINARY_FILE_TYPE_AUDIO );
}

if ( $fileExtension == ".m3u" )
{
	$result = $fileHandler->handleDownload( $contentObject, $contentObjectAttribute, BINARY_FILE_TYPE_LINK );
}
elseif ( $fileExtension == ".ram" )
{
	$result = $fileHandler->handleDownload( $contentObject, $contentObjectAttribute, BINARY_FILE_TYPE_LINK );
}
else
{
	$result = $fileHandler->handleDownload( $contentObject, $contentObjectAttribute, BINARY_FILE_TYPE_FILE );
}
	
if ( $result == BINARY_FILE_RESULT_UNAVAILABLE )
{
    eZDebug::writeError( "The specified file could not be found." );
    return $Module->handleError( EZ_ERROR_KERNEL_NOT_AVAILABLE, 'kernel' );
}

?>
