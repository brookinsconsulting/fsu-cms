<?php
// Audio Datatype 
// Written by Stuart Fenton of Grandmore.com
// Stuart Fenton @ Grandmore.com
//
//  12 August 2006
//
//  email: fats@grandmore.com
//  web:   http://www.grandmore.com
//
// Definition of FilePasstroughHandler class
//

/*!
  \class FilePasstroughHandler ezfilepasstroughhandler.php
  \ingroup eZBinaryHandlers
  \brief Handles file downloading by passing the file trough PHP

*/
include_once( "kernel/classes/datatypes/ezbinaryfile/ezbinaryfile.php" );
#include_once( "kernel/classes/ezbinaryfilehandler.php" );
include_once( "extension/audio/modules/audio/binaryfilehandler.php" );

define( "FILE_PASSTROUGH_ID", 'ezfilepasstrough' );

class FilePasstroughHandler extends BinaryFileHandler
{
    function FilePasstroughHandler()
    {
        $this->BinaryFileHandler( FILE_PASSTROUGH_ID, "PHP passtrough", BINARY_FILE_HANDLE_DOWNLOAD );
    }

    function handleFileDownload( &$contentObject, &$contentObjectAttribute, $type,
                                 $mimeData )
    {
        $fileName = $mimeData['url'];
        if ( $fileName != "" and file_exists( $fileName ) )
        {
            $fileSize = filesize( $fileName );
            $mimeType =  $mimeData['name'];
            $originalFileName = $mimeData['original_filename'];
            $contentLength = $fileSize;
            $fileOffset = false;
            $fileLength = false;
            if ( isset( $_SERVER['HTTP_RANGE'] ) )
            {
                $httpRange = trim( $_SERVER['HTTP_RANGE'] );
                if ( preg_match( "/^bytes=([0-9]+)-$/", $httpRange, $matches ) )
                {
                    $fileOffset = $matches[1];
                    header( "Content-Range: bytes $fileOffset-" . $fileSize - 1 . "/$fileSize" );
                    header( "HTTP/1.1 206 Partial content" );
                    $contentLength -= $fileOffset;
                }
            }

            header( "Pragma: " );
            header( "Cache-Control: " );
            header( "Content-Length: $contentLength" );
            header( "Content-Type: $mimeType" );
            header( "X-Powered-By: eZ publish" );
            header( "Content-disposition: attachment; filename=$originalFileName" );
            header( "Content-Transfer-Encoding: binary" );
            header( "Accept-Ranges: bytes" );

            $fh = fopen( "$fileName", "rb" );
            if ( $fileOffset )
            {
                eZDebug::writeDebug( $fileOffset, "seeking to fileoffset" );
                fseek( $fh, $fileOffset );
            }

            ob_end_clean();
            fpassthru( $fh );
            fclose( $fh );
            fflush();
            eZExecution::cleanExit();
        }
        return BINARY_FILE_RESULT_UNAVAILABLE;
    }
}

?>
