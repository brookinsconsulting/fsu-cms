<?php
// Audio Datatype 
// Written by Stuart Fenton of Grandmore.com
// Stuart Fenton @ Grandmore.com
//
//  12 August 2006
//
//  email: fats@grandmore.com
//  web:   http://www.grandmore.com
//
// Definition of Audio class
//

/*!
  \class Audio audio.php
  \ingroup eZKernel
  \brief The class Audio handles registered media files

*/

include_once( "lib/ezutils/classes/ezdebug.php" );
include_once( "lib/ezdb/classes/ezdb.php" );
include_once( "kernel/classes/ezpersistentobject.php" );
include_once( "kernel/classes/ezcontentclassattribute.php" );

class Audio extends eZPersistentObject
{
    function Audio( $row )
    {
        $this->eZPersistentObject( $row );
    }

    static function definition()
    {
        return array( "fields" => array( "contentobject_attribute_id" => array( 'name' => "ContentObjectAttributeID",
                                                                                'datatype' => 'integer',
                                                                                'default' => 0,
                                                                                'required' => true ),
                                         "version" => array( 'name' => "Version",
                                                             'datatype' => 'integer',
                                                             'default' => 0,
                                                             'required' => true ),
                                         "filename" => array( 'name' => "Filename",
                                                              'datatype' => 'string',
                                                              'default' => '',
                                                              'required' => true ),
                                         "original_filename" => array( 'name' => "OriginalFilename",
                                                                       'datatype' => 'string',
                                                                       'default' => '',
                                                                       'required' => true ),
                                         "mime_type" => array( 'name' => "MimeType",
                                                               'datatype' => 'string',
                                                               'default' => '',
                                                               'required' => true ),
                                         "storage" => array( 'name' => "Storage",
                                                               'datatype' => 'string',
                                                               'default' => '',
                                                               'required' => true ),
										 "pls_contentobject_attribute_id" => array( 'name' => "PlsContentObjectAttributeID",
	                                                            'datatype' => 'integer',
	                                                            'default' => 0,
	                                                            'required' => true ), 
                                         "pls_sitename" => array( 'name' => "PlsSitename",
                                                              'datatype' => 'string',
                                                              'default' => '',
                                                              'required' => true ),
                                         "pls_filename" => array( 'name' => "PlsFilename",
                                                              'datatype' => 'string',
                                                              'default' => '',
                                                              'required' => true ),
                                         "pls_original_filename" => array( 'name' => "PlsOriginalFilename",
                                                                       'datatype' => 'string',
                                                                       'default' => '',
                                                                       'required' => true ),
                                         "pls_mime_type" => array( 'name' => "PlsMimeType",
                                                               'datatype' => 'string',
                                                               'default' => '',
                                                               'required' => true ) ),
                      "keys" => array( "contentobject_attribute_id", "version" ),
                      "relations" => array( "contentobject_attribute_id" => array( "class" => "ezcontentobjectattribute",
                                                                                   "field" => "id" ),
                                            "version" => array( "class" => "ezcontentobjectattribute",
                                                                "field" => "version" )),
                      "class_name" => "Audio",
                      "name" => "audio" );
    }

    function hasAttribute( $attr )
    {
        return $attr == "mime_type_category" or
            $attr == "mime_type_part" or
            eZPersistentObject::hasAttribute( $attr ) ;
    }

    function attribute( $attr )
    {
        $ini =& eZINI::instance();

        switch( $attr )
        {
            case "mime_type_category":
            {
                $types = explode( "/", eZPersistentObject::attribute( "mime_type" ) );
                return $types[0];
            } break;
            case "mime_type_part":
            {
                $types = explode( "/", eZPersistentObject::attribute( "mime_type" ) );
                return $types[1];
            } break;
            default:
                return eZPersistentObject::attribute( $attr );
        }
        return null;
    }

    function create( $contentObjectAttributeID, $version )
    {
        $row = array( "contentobject_attribute_id" => $contentObjectAttributeID,
                      "version" => $version,
                      "filename" => "",
                      "original_filename" => "",
                      "mime_type" => "",
                      "storage" => "",
					  "pls_contentobject_attribute_id" => "",
                      "pls_sitename" => "",
                      "pls_filename" => "",
                      "pls_original_filename" => "",
                      "pls_mime_type" => "",
                      );
        return new Audio( $row );
    }

    function fetch( $id, $version, $asObject = true )
    {
        if( $version == null )
        {
            return eZPersistentObject::fetchObjectList( Audio::definition(),
                                                        null,
                                                        array( "contentobject_attribute_id" => $id ),
                                                        null,
                                                        null,
                                                        $asObject );
        }
        else
        {
            return eZPersistentObject::fetchObject( Audio::definition(),
                                                    null,
                                                    array( "contentobject_attribute_id" => $id,
                                                           "version" => $version ),
                                                    $asObject );
        }
    }

    function remove( $id, $version )
    {
        if( $version == null )
        {
            eZPersistentObject::removeObject( Audio::definition(),
                                              array( "contentobject_attribute_id" => $id ) );
        }
        else
        {
            eZPersistentObject::removeObject( Audio::definition(),
                                              array( "contentobject_attribute_id" => $id,
                                                     "version" => $version ) );
        }
    }

    var $ContentObjectAttributeID;
    var $Filename;
    var $OriginalFilename;
    var $MimeType;
    var $PlsContentObjectAttributeID;
    var $PlsSitename;
    var $PlsFilename;
    var $PlsOriginalFilename;
    var $PlsMimeType;
}

?>
