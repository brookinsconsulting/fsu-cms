<?php
// Audio Datatype 
// Written by Stuart Fenton of Grandmore.com
// Stuart Fenton @ Grandmore.com
//
//  12 August 2006
//
//  email: fats@grandmore.com
//  web:   http://www.grandmore.com
//
// Definition of AudioPls class
//

/*! \file audiopls.php
*/

/*!
  \class AudioPls audiopls.php
  \brief The class AudioPls does

*/

include_once( "lib/ezutils/classes/ezdebug.php" );
include_once( "kernel/classes/ezpersistentobject.php" );
include_once( "kernel/classes/ezcontentclassattribute.php" );
include_once( "lib/ezutils/classes/ezmimetype.php" );
include_once( "kernel/common/image.php" );

class AudioPls extends eZPersistentObject
{
    /*!
     Constructor
    */
    function AudioPls( $row )
    {
        $this->eZPersistentObject( $row );
    }


    static function definition()
    {
        return array( "fields" => array( "contentobject_attribute_id" => array( 'name' => "ContentObjectAttributeID",
                                                                                'datatype' => 'integer',
                                                                                'default' => 0,
                                                                                'required' => true ),
                                         "version" => array( 'name' => "Version",
                                                             'datatype' => 'integer',
                                                             'default' => 0,
                                                             'required' => true ),
                                         "sitename" => array( 'name' => "Sitename",
                                                              'datatype' => 'string',
                                                              'default' => '',
                                                              'required' => true ),
                                         "filename" => array( 'name' => "Filename",
                                                              'datatype' => 'string',
                                                              'default' => '',
                                                              'required' => true ),
                                         "original_filename" => array( 'name' => "OriginalFilename",
                                                                       'datatype' => 'string',
                                                                       'default' => '',
                                                                       'required' => true ),
                                         "mime_type" => array( 'name' => "MimeType",
                                                               'datatype' => 'string',
                                                               'default' => '',
                                                               'required' => true ) ),
                      "keys" => array( "contentobject_attribute_id", "version" ),
                      "relations" => array( "contentobject_attribute_id" => array( "class" => "ezcontentobjectattribute",
                                                                                   "field" => "id" ) ),
                      "class_name" => "AudioPls",
                      "name" => "audiopls" );
    }



    function hasAttribute( $attr )
    {
        return $attr == "mime_type_category" or
            $attr == "mime_type_part" or
            eZPersistentObject::hasAttribute( $attr ) ;
    }


    function attribute( $attr )
    {
        $ini =& eZINI::instance();

        switch( $attr )
        {
            case "mime_type_category":
            {
                $types = explode( "/", eZPersistentObject::attribute( "mime_type" ) );
                return $types[0];
            } break;
            case "mime_type_part":
            {
                $types = explode( "/", eZPersistentObject::attribute( "mime_type" ) );
                return $types[1];
            } break;
            default:
                return eZPersistentObject::attribute( $attr );
        }
        return null;
    }


    function create( $contentObjectAttributeID, $version )
    {
        $row = array( "contentobject_attribute_id" => $contentObjectAttributeID,
                      "version" => $version,
                      "sitename" => "",
                      "filename" => "",
                      "original_filename" => "",
                      "mime_type" => ""
                      );
        return new AudioPls( $row );
    }


    function fetch( $id, $version, $asObject = true )
    {
        if( $version == null )
        {
            return eZPersistentObject::fetchObjectList( AudioPls::definition(),
                                                        null,
                                                        array( "contentobject_attribute_id" => $id ),
                                                        null,
                                                        null,
                                                        $asObject );
        }
        else
        {
            return eZPersistentObject::fetchObject( AudioPls::definition(),
                                                    null,
                                                    array( "contentobject_attribute_id" => $id,
                                                           "version" => $version ),
                                                    $asObject );
        }
    }



    function remove( $id, $version )
    {
        if( $version == null )
        {
            eZPersistentObject::removeObject( AudioPls::definition(),
                                              array( "contentobject_attribute_id" => $id ) );
        }
        else
        {
            eZPersistentObject::removeObject( AudioPls::definition(),
                                              array( "contentobject_attribute_id" => $id,
                                                     "version" => $version ) );
        }
    }


   function fullPath()
    {
        $sys =& eZSys::instance();
        $ini =& eZINI::instance();
        $contentobjectAttributeID = $this->attribute( "contentobject_attribute_id" );
        $version = $this->attribute( "version" );
        $img_obj = eZImage::fetch( $contentobjectAttributeID, $version );

        $storageDir = $sys->storageDirectory();
        $category = $img_obj->attribute( "mime_type_category" );
        $additionalPath = $this->attribute( "additional_path" );
        $filename = $this->attribute( "filename" );

        return eZDir::path( array( $storageDir, $variationPath, $category, $additionalPath, $filename ) );
    }


	#######################
	
	//
	// Create the pls file from the audio objects objectattributeid and it's version number
	// The files name will be identical to the one provided.
	//
    function createPlsFile( $contentObjectAttributeID, $version)
	{
		$debug = 1;
	    if ($debug) eZDebug::writeNotice( "Started writing the linkfile", "Create_plsfile" );
	

		include_once( "extension/audio/datatypes/audio/audiopls.php" );
		include_once( "lib/ezutils/classes/ezmimetype.php" );
			
		// Read data from audio atribute fields
		$audioAttribute =& Audio::fetch( $contentObjectAttributeID, $version );
		if ( $audioAttribute == null )
		{
	    	if ($debug) eZDebug::writeNotice( "No matching Audio Attribute Object", "Create_plsfile" );
			return false;
		}
	
		$audioAttribute_Filename = $audioAttribute->attribute( "filename" );
		$audioAttribute_OriginalFilename = $audioAttribute->attribute( "original_filename" );

		//what type of pls file do we create?
		if ( $audioAttribute->attribute( "mime_type" ) == 'audio/x-realaudio') 
		{
			$fileName = preg_replace('/\.(.*)$/', "", $audioAttribute_Filename ) ;
			$newStoredFileName = $fileName . '.ram';			
		}
		elseif ( $audioAttribute->attribute( "mime_type" ) == 'application/vnd.rn-realmedia') 
		{
			$fileName = preg_replace('/\.(.*)$/', "", $audioAttribute_Filename ) ;
			$newStoredFileName = $fileName . '.ram';			
		}
		elseif ( $audioAttribute->attribute( "mime_type" ) == 'audio/mpeg') 
		{
			$fileName = preg_replace('/\.(.*)$/', "", $audioAttribute_Filename ) ;
			$newStoredFileName = $fileName . '.m3u';			
		}
		else 
		{
			$fileName = preg_replace('/\.(.*)$/', "", $audioAttribute_Filename ) ;
			$newStoredFileName = $fileName . '.m3u';			
		}
		
		
		$audioAttribute_Filename = $audioAttribute->attribute( "filename" );
		$audioAttribute_OriginalFilename = $audioAttribute->attribute( "original_filename" );
		
	    
		$plsfile =& AudioPls::fetch( $contentObjectAttributeID, $version );
		if ( $plsfile == null )
		{
			$plsfile =& AudioPls::create( $contentObjectAttributeID, $version );
		}
		

		
	
//		$fileName = preg_replace('/\.(.*)$/', "", $audioAttribute_OriginalFilename ) ;
//		$newOrigionalFileName = $fileName . '.m3u';
		if ( $audioAttribute->attribute( "mime_type" ) == 'audio/x-realaudio') 
		{
			$fileName = preg_replace('/\.(.*)$/', "", $audioAttribute_OriginalFilename ) ;
			$newOrigionalFileName = $fileName . '.ram';
		}
		elseif ( $audioAttribute->attribute( "mime_type" ) == 'application/vnd.rn-realmedia') 
		{
			$fileName = preg_replace('/\.(.*)$/', "", $audioAttribute_OriginalFilename ) ;
			$newOrigionalFileName = $fileName . '.ram';
		}
		elseif ( $audioAttribute->attribute( "mime_type" ) == 'audio/mpeg') 
		{
			$fileName = preg_replace('/\.(.*)$/', "", $audioAttribute_OriginalFilename ) ;
			$newOrigionalFileName = $fileName . '.m3u';
		}
		else 
		{
			$fileName = preg_replace('/\.(.*)$/', "", $audioAttribute_OriginalFilename ) ;
			$newOrigionalFileName = $fileName . '.m3u';
		}

	
		$plsfile->setAttribute( 'filename', $newStoredFileName );
		$plsfile->setAttribute( 'original_filename', $newOrigionalFileName );
	    if ($debug) eZDebug::writeNotice( $newStoredFileName, "Stored Filename" );
		if ($debug) eZDebug::writeNotice( $newOrigionalFileName, "Original Filename" );
		
		//get the mime type - REMEMBER to modiy the ezMimeType.php code to include the 'pls' extension
		$mimeData =& eZMimeType::findByFileContents( $newOrigionalFileName );
	    $mime = $mimeData['name'];
	    $plsfile->setAttribute( 'mime_type', $mime );
		if ($debug) eZDebug::writeNotice( $mime, "Mime Type" );
	
		$plsfile->store();
		
		// set the correct path to the origional MP3 file and save it in the m3u/ram/? file.
		$site = eZSys::siteDir();	
#		$storage_dir =  $audioAttribute->attribute( "storage" );
		$storage_dir = eZSys::storageDirectory() . '/original/' . 'audio' . '/';
	
		include_once( "lib/ezfile/classes/ezfile.php" );
		include_once( "lib/ezfile/classes/ezdir.php" );
	
		$ini =& eZINI::instance();
		$siteDesign = $ini->variable('SiteSettings', 'DefaultAccess' );
	
	    $fileINI =& eZINI::instance( 'audio.ini' );
	    $siteURL = $fileINI->variable( 'LinkFileFileSettings', 'LinkFileSiteName' );
	
		$contentAttributeObject = eZContentObjectAttribute::fetch( $contentObjectAttributeID, $version );
	    $objectID = $contentAttributeObject->attribute( 'contentobject_id' );
	    
	    // Write the files contents
		$cleanFilename = rawurlencode( $audioAttribute_OriginalFilename );
	    $data = "http://" . $siteURL ."/" . $siteDesign ."/audio/download/" 
				. $objectID . "/" . $contentObjectAttributeID . "/file/" . $cleanFilename . chr(13);
	
	
		//write the contents to the file acording to the standard play list format	
		if ( !eZFile::create($newStoredFileName, $storage_dir, $data) )
		{
			eZDebug::writeError( "Failed to store pls-file: " . $plsfile->attribute( "original_filename" ),
								 "AudioType" );
			return false;
		}
	
		$audioAttribute->setAttribute( "pls_contentobject_attribute_id", $plsfile->attribute( "contentobject_attribute_id" ) );
		$audioAttribute->setAttribute( "pls_sitename", $siteURL );
		$audioAttribute->setAttribute( "pls_filename", $plsfile->attribute( "filename" ) );
		$audioAttribute->setAttribute( "pls_original_filename", $plsfile->attribute( "original_filename" ) );
		$audioAttribute->setAttribute( "pls_mime_type", $plsfile->attribute( "mime_type" ) );
	
		$audioAttribute->store();
		
		$plsfile->setAttribute( "sitename", $siteURL );
		$plsfile->store();
		
		eZDebug::writeNotice( "postvariable accepted. Ending code...", "Create_plsfile" );

		return true;
	}
###

	var $Version;
	var $ContentObjectAttributeID;
	var $Sitename;
	var $Filename;
	var $MimeType;
	var $OriginalFilename;
}

?>
