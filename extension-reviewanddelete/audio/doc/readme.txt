// Audio Datatype 
// written by Stuart Fenton of Grandmore.com
// Stuart Fenton @ Grandmore.com
//
//  12 August 2006
//
//  email: fats@grandmore.com
//  web:   http://www.grandmore.com


The Audio datatype allows "sudo" streaming of audio files. This means you don't 
need a streaming server but allows your website users to hear your audio without 
downloading the file first.

The datatype creates a reference file that points to the original file and when 
clicked passes the original file to the mp3 or real player. The Player in turn 
downloads the file via ftp directly into the player which starts to play it in 
a stream before the entire file is downloaded.

---
INSTALLATION

1) In order to get this working the eZMimeType.php file needs to be modified to 
include the 'pls' and 'm3u' file type (without the quotes). These mime types are 
used by most mp3 software as well as Real Media to identify the file as a pointer 
to the real file for streaming.

The file can be found in /lib/ezutils/classes/ezmimetype.php

Add this line to the mimetype code in the "var $QuickMIMETypes = array(" 

    array( 'pls', 'audio/pls' ),
    array( 'm3u', 'audio/x-mpegurl' ),

For example look for mp3 and change the block of code here:

        ...
        array( 'mp3', 'audio/mpeg' ),
        ...

To look like this

        ...
        array( 'mp3', 'audio/mpeg' ),
        array( 'pls', 'audio/pls' ),
        array( 'm3u', 'audio/x-mpegurl' ),
        ...

2) You need to add two database tables to your database that can be found in the 
update/mysql folder called "audio.sql"

You can change the MyISAM type to innodb if your using the later 3.8 installation 
of ez although the extension itself does not use any form of rollback.


3) Clear your cache and activate the "Audio" extension in the admin side of your 
ezPublish installation. Normally in "setup/extensions"

4) Update the "audio.ini.append.php" file with your server location so the the 
files can be written with the right path to find your website.

You need to update the "LinkFileSiteName" to contain the website url.

For example:

LinkFileSiteName=www.grandmore.com

Although there are two methods of streaming the file "filepasstrough" is the 
only one that reliably works at present.

If you want to secure the file from being streamed using the permissions it will 
break the extension as the player has no way to login to get the stream. Instead 
you must block access the the file using the templates but leave the stream open 
for the players.



---
USING THE EXTENSION

1. To use the Audio extension, after is is activated, create or edit an class. 

2. A new Audio Attribute is available called "Audio".

3. Add the attribute to your class.

4. Specify if the file will be an "mp3" or a "Real Media" audio file. 
(note: you can have multiple Audio attributes in the same class so you can have 
one mp3 and one real audio file in the same class or more if you want)

5. Save the class.

6. Now use your newly created class when adding content.
Select the file to upload the rest is automatic.

If you have any questions or any problems let me know.

I have added a forum to discuss this in case you have any problems.
























