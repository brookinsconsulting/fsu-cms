<?php /* #?ini charset="iso-8859-1"?

[audio]
LinkFileSiteName=www.grandmore.com


[AudioBinaryFileSettings]
Handler=filepasstrough
#Handler=filedirect
# Swap with the above line to get direct downloads
# Note: Direct downloads does not check roles permissions.
#Handler=filedirect


[LinkFileFileSettings]
#eg LinkFileSiteName=www.site.com
# The LinkFileSiteName holds the name of the site that gets written into the m3u files
# This is usefull when testing the server and know it will be moved.
# This variable gets tested when a file is requested for download. If it does not match the
# m3u file it gets rewitten to this variable.
LinkFileSiteName=www.grandmore.com


[ArchiveSettings]
Handlers[]
Handlers[tar]=eztararchivehandler

[FileSettings]
Handlers[]
# Generic gzip handler, uses the zlib or shell handlers to do the job
Handlers[gzip]=ezgzipcompressionhandler
# Specific gzip handler, either using the zlib extension or gzip shell command
Handlers[gzipzlib]=ezgzipzlibcompressionhandler
Handlers[gzipshell]=ezgzipshellcompressionhandler

*/ ?>
