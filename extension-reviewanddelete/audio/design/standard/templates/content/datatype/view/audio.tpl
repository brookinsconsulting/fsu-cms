{* {$attribute.content|attribute(show)}<br> *}
{* {attribute_view_gui attribute=$attribute.object.data_map.original_filename} *}

{default icon_size='normal' icon_title=$attribute.content.mime_type}

{*{default icon_size='small' icon_title="test text"}*}

{*<a href={concat("audio/download/",$attribute.contentobject_id,"/",$attribute.id,"/file/",$attribute.content.pls_original_filename)|ezurl}>
	{$attribute.content.mime_type|mimetype_icon( $icon_size, $icon_title )} {$attribute.content.pls_original_filename|wash(xhtml)}</a> 
*}


{section show=$attribute.content}
	{switch match=$attribute.content.mime_type}
	    {case match='audio/mpeg'}
	        
	        <a href={concat("audio/download/",$attribute.contentobject_id,"/",$attribute.id,"/file/",$attribute.content.pls_original_filename)|ezurl}>
			{$attribute.content.mime_type|mimetype_icon( $icon_size, "Streaming mp3 audio. You need to have mp3 player software installed to hear this." )} {*{$attribute.content.pls_original_filename|wash(xhtml)}*}</a> 

	    {/case}

	    {case match='application/vnd.rn-realmedia'}
	        <a href={concat("audio/download/",$attribute.contentobject_id,"/",$attribute.id,"/file/",$attribute.content.pls_original_filename)|ezurl}>
			{'real_video'|class_icon( normal, 'Streaming Real Media audio. Download Real Player at http://www.real.com.' )}
		{*	{$attribute.content.mime_type|mimetype_icon( $icon_size, "Streaming Real Media file. Download Real Player at http://www.real.com." )}*}
		 {*{$attribute.content.pls_original_filename|wash(xhtml)}*}     </a> 

	    {/case}
	    
	    
	    {case}
	      {*  <a href={concat("audio/download/",$attribute.contentobject_id,"/",$attribute.id,"/file/",$attribute.content.pls_original_filename)|ezurl}>
			{$attribute.content.mime_type|mimetype_icon( $icon_size, $icon_title )}*} {*{$attribute.content.pls_original_filename|wash(xhtml)}*}</a> 
		{/case}
	
	{/switch}
{/section}

{*** START: TESTING CODE DISABLE WHEN LIVE
<br>
<form method="post" action={"content/action"|ezurl}>
    <div class="buttonblock">
        <input class="button" type="submit" name="create_plsfile" value="{'Create Link File'|i18n( 'extension/audio/' )}" />
        
        <input type="hidden" name="NodeID" value="{$node.node_id}" />
        <input type="hidden" name="ClassID" value="25" />
        <input type="hidden" name="ContentObjectAttributeID" value="{$attribute.content.contentobject_attribute_id}" />
        <input type="hidden" name="ContentObjectVersion" value="{$attribute.content.version}" />
    </div>
</form>

<br>

{$attribute.content|attribute(show)}<br>

***END: TESTING CODE DISABLE WHEN LIVE *}


{/default}
