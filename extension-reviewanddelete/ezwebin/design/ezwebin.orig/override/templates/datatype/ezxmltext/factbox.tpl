{if is_set($align)|not}
   {def $align = 'center'}
{/if}

{if is_set($title)|not}
   {def $title = ''}
{/if}


<div class="object-{$align}">
   <div class="factbox">
       <div class="factbox-header">
       	<h2>{$title}&nbsp;</h2>
	   </div>
       <div class="factbox-content">
       	{$content}
       </div>
   </div>
</div>

{undef}