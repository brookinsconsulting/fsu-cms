{default enable_print=true()}

<link rel="Home" href={"/"|ezroot} title="{'%sitetitle front page'|i18n('design/ezwebin/link',,hash('%sitetitle',$site.title))}" />
<link rel="Index" href={"/"|ezroot} />
<link rel="Top"  href={"/"|ezroot} title="{$site_title}" />
<link rel="Search" href={"content/advancedsearch"|ezroot} title="{'Search %sitetitle'|i18n('design/ezwebin/link',,hash('%sitetitle',$site.title))}" />
<link rel="Shortcut icon" href={"favicon.ico"|ezimage} type="image/x-icon" />
<link rel="Copyright" href={"/ezinfo/copyright"|ezroot} />
<link rel="Author" href={"/ezinfo/about"|ezroot} />

{if and( is_set($pagedesign), $pagedesign.data_map.rss_feed.has_content )}
<link rel="Alternate" type="application/rss+xml" title="RSS" href="{$pagedesign.data_map.rss_feed.data_text|ezroot(no)}" />
{/if}

{if $enable_print}
<link rel="Alternate" href={concat("layout/set/print/", $site.uri.original_uri)|ezroot} media="print" title="{'Printable version'|i18n('design/ezwebin/link')}" />
{/if}

{/default}