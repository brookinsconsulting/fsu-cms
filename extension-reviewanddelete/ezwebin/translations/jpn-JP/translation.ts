<!DOCTYPE TS><TS>
<context>
    <name>design/ezwebin/article/article_index</name>
    <message>
        <source>Article index</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>design/ezwebin/article/comments</name>
    <message>
        <source>Comments</source>
        <translation type="unfinished">コメント</translation>
    </message>
    <message>
        <source>New comment</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>%login_link_startLog in%login_link_end or %create_link_startcreate a user account%create_link_end to comment.</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>design/ezwebin/blog/calendar</name>
    <message>
        <source>Previous month</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Next month</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Calendar</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Mon</source>
        <translation type="unfinished">月</translation>
    </message>
    <message>
        <source>Tue</source>
        <translation type="unfinished">火</translation>
    </message>
    <message>
        <source>Wed</source>
        <translation type="unfinished">水</translation>
    </message>
    <message>
        <source>Thu</source>
        <translation type="unfinished">木</translation>
    </message>
    <message>
        <source>Fri</source>
        <translation type="unfinished">金</translation>
    </message>
    <message>
        <source>Sat</source>
        <translation type="unfinished">土</translation>
    </message>
    <message>
        <source>Sun</source>
        <translation type="unfinished">日</translation>
    </message>
</context>
<context>
    <name>design/ezwebin/blog/extra_info</name>
    <message>
        <source>Tags</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Archive</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>design/ezwebin/collectedinfo/form</name>
    <message>
        <source>Form %formname</source>
        <translation>フォーム%formname</translation>
    </message>
    <message>
        <source>Thank you for your feedback.</source>
        <translation>ご意見・ご感想有難うございました。</translation>
    </message>
    <message>
        <source>Return to site</source>
        <translation>サイトへ戻る</translation>
    </message>
    <message>
        <source>You have already submitted this form. The data you entered was:</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>design/ezwebin/collectedinfo/poll</name>
    <message>
        <source>Poll %pollname</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Results</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Please log in to vote on this poll.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>You have already voted for this poll.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Votes</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>%count total votes</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Back to poll</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>design/ezwebin/collectedinfomail/feedback</name>
    <message>
        <source>Feedback from %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>The following feedback was collected</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>design/ezwebin/collectedinfomail/form</name>
    <message>
        <source>Collected information from %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>The following information was collected</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>design/ezwebin/content/advancedsearch</name>
    <message>
        <source>Advanced search</source>
        <translation>検索オプション</translation>
    </message>
    <message>
        <source>Search all the words</source>
        <translation>全ての文字を検索</translation>
    </message>
    <message>
        <source>Search the exact phrase</source>
        <translation>一致する句を検索</translation>
    </message>
    <message>
        <source>Published</source>
        <translation>公開中</translation>
    </message>
    <message>
        <source>Any time</source>
        <translation>指定無し</translation>
    </message>
    <message>
        <source>Last day</source>
        <translation>昨日</translation>
    </message>
    <message>
        <source>Last week</source>
        <translation>先週</translation>
    </message>
    <message>
        <source>Last three months</source>
        <translation>三ヶ月前から</translation>
    </message>
    <message>
        <source>Last year</source>
        <translation>昨年</translation>
    </message>
    <message>
        <source>Display per page</source>
        <translation>ページあたりの表示件数</translation>
    </message>
    <message>
        <source>5 items</source>
        <translation>5アイテム</translation>
    </message>
    <message>
        <source>10 items</source>
        <translation>10アイテム</translation>
    </message>
    <message>
        <source>20 items</source>
        <translation>20アイテム</translation>
    </message>
    <message>
        <source>30 items</source>
        <translation>30アイテム</translation>
    </message>
    <message>
        <source>50 items</source>
        <translation>50アイテム</translation>
    </message>
    <message>
        <source>Search</source>
        <translation>検索</translation>
    </message>
    <message>
        <source>No results were found when searching for &quot;%1&quot;</source>
        <translation>&quot;%1&quot;に対する検索結果は得られませんでした.</translation>
    </message>
    <message>
        <source>Search for &quot;%1&quot; returned %2 matches</source>
        <translation>&quot;%1&quot; の検索結果は %2 件です。</translation>
    </message>
    <message>
        <source>Last month</source>
        <translation type="unfinished">先月</translation>
    </message>
</context>
<context>
    <name>design/ezwebin/content/browse</name>
    <message>
        <source>Browse</source>
        <translation>ブラウズ</translation>
    </message>
    <message>
        <source>To select objects, choose the appropriate radiobutton or checkbox(es), and click the &quot;Select&quot; button.</source>
        <translation>ラジオボタンもしくはチェックボックスでオブジェクトを選択し、”選択”ボタンをクリックして下さい。</translation>
    </message>
    <message>
        <source>Back</source>
        <translation>戻る</translation>
    </message>
    <message>
        <source>Top level</source>
        <translation>トップレベル</translation>
    </message>
    <message>
        <source>Select</source>
        <translation>選択</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation>キャンセル</translation>
    </message>
    <message>
        <source>To select an object that is a child of one of the displayed objects, click the parent object name to display a list of its children.</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>design/ezwebin/content/browse_mode_list</name>
    <message>
        <source>Name</source>
        <translation>名前</translation>
    </message>
    <message>
        <source>Type</source>
        <translation>タイプ</translation>
    </message>
    <message>
        <source>Invert selection</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>design/ezwebin/content/diff</name>
    <message>
        <source>Versions for &lt;%object_name&gt; [%version_count]</source>
        <translation>&lt;%object_name&gt; [%version_count] 
のバージョン</translation>
    </message>
    <message>
        <source>Version</source>
        <translation>バージョン</translation>
    </message>
    <message>
        <source>Status</source>
        <translation>ステータス</translation>
    </message>
    <message>
        <source>Translations</source>
        <translation>翻訳</translation>
    </message>
    <message>
        <source>Creator</source>
        <translation>作成者</translation>
    </message>
    <message>
        <source>Modified</source>
        <translation>修正日時</translation>
    </message>
    <message>
        <source>Draft</source>
        <translation>ドラフト</translation>
    </message>
    <message>
        <source>Published</source>
        <translation>公開中</translation>
    </message>
    <message>
        <source>Pending</source>
        <translation>保留中</translation>
    </message>
    <message>
        <source>Archived</source>
        <translation>保管</translation>
    </message>
    <message>
        <source>Rejected</source>
        <translation>非承認</translation>
    </message>
    <message>
        <source>Untouched draft</source>
        <translation>未変更のドラフト</translation>
    </message>
    <message>
        <source>This object does not have any versions.</source>
        <translation>このオブジェクトにはバージョンが存在しません。</translation>
    </message>
    <message>
        <source>Show differences</source>
        <translation>違いを表示</translation>
    </message>
    <message>
        <source>Differences between versions %oldVersion and %newVersion</source>
        <translation>%oldVersion と %newVersionの違い</translation>
    </message>
    <message>
        <source>Old version</source>
        <translation>旧バージョン</translation>
    </message>
    <message>
        <source>Inline changes</source>
        <translation>インライン変更</translation>
    </message>
    <message>
        <source>Block changes</source>
        <translation>ブロック変更</translation>
    </message>
    <message>
        <source>New version</source>
        <translation>新バージョン</translation>
    </message>
</context>
<context>
    <name>design/ezwebin/content/draft</name>
    <message>
        <source>Select all</source>
        <translation>全て選択</translation>
    </message>
    <message>
        <source>Deselect all</source>
        <translation>全ての選択を解除</translation>
    </message>
    <message>
        <source>My drafts</source>
        <translation>マイドラフト</translation>
    </message>
    <message>
        <source>These are the current objects you are working on. The drafts are owned by you and can only be seen by you.
      You can either edit the drafts or remove them if you don&apos;t need them any more.</source>
        <translation>現在あなたが編集しているオブジェクトです.このドラフトはあなたの所有で、あなたのみ閲覧可能です.
編集または削除が可能です.</translation>
    </message>
    <message>
        <source>Name</source>
        <translation>名前</translation>
    </message>
    <message>
        <source>Class</source>
        <translation>クラス</translation>
    </message>
    <message>
        <source>Section</source>
        <translation>セクション</translation>
    </message>
    <message>
        <source>Version</source>
        <translation>バージョン</translation>
    </message>
    <message>
        <source>Language</source>
        <translation>言語</translation>
    </message>
    <message>
        <source>Last modified</source>
        <translation>最新の修正</translation>
    </message>
    <message>
        <source>Edit</source>
        <translation>編集</translation>
    </message>
    <message>
        <source>Remove</source>
        <translation>削除</translation>
    </message>
    <message>
        <source>You have no drafts</source>
        <translation>ドラフトがありません</translation>
    </message>
    <message>
        <source>Empty draft</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>design/ezwebin/content/edit</name>
    <message>
        <source>Manage versions</source>
        <translation>バージョン管理</translation>
    </message>
    <message>
        <source>Store and exit</source>
        <translation>保存して編集終了</translation>
    </message>
    <message>
        <source>Preview</source>
        <translation>プレビュー</translation>
    </message>
    <message>
        <source>Translate</source>
        <translation>翻訳</translation>
    </message>
    <message>
        <source>Edit %1 - %2</source>
        <translation>%1 - %2の編集</translation>
    </message>
    <message>
        <source>Send for publishing</source>
        <translation>送信して公開</translation>
    </message>
    <message>
        <source>Store draft</source>
        <translation>ドラフトを保存</translation>
    </message>
    <message>
        <source>Discard draft</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Translate from</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Documentation</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Translating content from %from_lang to %to_lang</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Content in %language</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>design/ezwebin/content/edit_attribute</name>
    <message>
        <source>Not translatable</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Required</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Information collector</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>design/ezwebin/content/edit_draft</name>
    <message>
        <source>The currently published version is %version and was published at %time.</source>
        <translation>現在公開中のバージョンは%versionで、%timeに公開されています.</translation>
    </message>
    <message>
        <source>The last modification was done at %modified.</source>
        <translation>最新の修正日時 %modified.</translation>
    </message>
    <message>
        <source>The object is owned by %owner.</source>
        <translation>オブジェクトは ユーザ %owner の所有です.</translation>
    </message>
    <message>
        <source>This object is already being edited by you.
        You can either continue editing one of your drafts or you can create a new draft.</source>
        <translation>このオブジェクトを編集中です.このまま編集を続けるか、新規ドラフトを作成して下さい.</translation>
    </message>
    <message>
        <source>Current drafts</source>
        <translation>現在のドラフト</translation>
    </message>
    <message>
        <source>Version</source>
        <translation>バージョン</translation>
    </message>
    <message>
        <source>Name</source>
        <translation>名前</translation>
    </message>
    <message>
        <source>Owner</source>
        <translation>所有者</translation>
    </message>
    <message>
        <source>Created</source>
        <translation>作成日時</translation>
    </message>
    <message>
        <source>Last modified</source>
        <translation>最新の修正</translation>
    </message>
    <message>
        <source>Edit</source>
        <translation>編集</translation>
    </message>
    <message>
        <source>New draft</source>
        <translation>新規ドラフト</translation>
    </message>
    <message>
        <source>This object is already being edited by yourself and others.
    You can either continue editing one of your drafts or you can create a new draft.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>This object is already being edited by someone else.
        You should either contact the person about their draft or create a new draft for your own use.</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>design/ezwebin/content/edit_languages</name>
    <message>
        <source>Existing languages</source>
        <translation>使用中の言語</translation>
    </message>
    <message>
        <source>New languages</source>
        <translation>新しい言語</translation>
    </message>
    <message>
        <source>You do not have permission to edit the object in any available languages.</source>
        <translation>言語に関わらず、編集の権限がありません.</translation>
    </message>
    <message>
        <source>Edit</source>
        <translation>編集</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation>キャンセル</translation>
    </message>
    <message>
        <source>Select the language you want to use when editing the object.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Select the language you want to add to the object.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Select the language the new translation will be based on.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Use an empty, untranslated draft</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>You do not have permission to create a translation in another language.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>However, you can select one of the following languages for editing.</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>design/ezwebin/content/history</name>
    <message>
        <source>Version not a draft</source>
        <translation>このバージョンはドラフトではありません。</translation>
    </message>
    <message>
        <source>Version not yours</source>
        <translation>このバージョンはあなたの所有ではありません。</translation>
    </message>
    <message>
        <source>Unable to create new version</source>
        <translation>新規バージョンの作成が出来ません。</translation>
    </message>
    <message>
        <source>Version history limit has been exceeded and no archived version can be removed by the system.</source>
        <translation>バージョン履歴のリミットを超えています。システムは保管されているバージョンを削除することは出来ません。</translation>
    </message>
    <message>
        <source>Versions for &lt;%object_name&gt; [%version_count]</source>
        <translation>&lt;%object_name&gt; [%version_count] 
のバージョン</translation>
    </message>
    <message>
        <source>Version</source>
        <translation>バージョン</translation>
    </message>
    <message>
        <source>Status</source>
        <translation>ステータス</translation>
    </message>
    <message>
        <source>Creator</source>
        <translation>作成者</translation>
    </message>
    <message>
        <source>Created</source>
        <translation>作成日時</translation>
    </message>
    <message>
        <source>Modified</source>
        <translation>修正日時</translation>
    </message>
    <message>
        <source>Select version #%version_number for removal.</source>
        <translation>バージョン#%version_numberを選択し削除</translation>
    </message>
    <message>
        <source>View the contents of version #%version_number. Translation: %translation.</source>
        <translation>バージョン#%version_numberのコンテントを表示。翻訳:%translation</translation>
    </message>
    <message>
        <source>Draft</source>
        <translation>ドラフト</translation>
    </message>
    <message>
        <source>Published</source>
        <translation>公開中</translation>
    </message>
    <message>
        <source>Pending</source>
        <translation>保留</translation>
    </message>
    <message>
        <source>Archived</source>
        <translation>保管</translation>
    </message>
    <message>
        <source>Rejected</source>
        <translation>非承認</translation>
    </message>
    <message>
        <source>Untouched draft</source>
        <translation>未変更のドラフト</translation>
    </message>
    <message>
        <source>Create a copy of version #%version_number.</source>
        <translation>バージョン#%version_numberの複製を作成</translation>
    </message>
    <message>
        <source>Edit the contents of version #%version_number.</source>
        <translation>バージョン#%version_numberのコンテントを編集</translation>
    </message>
    <message>
        <source>This object does not have any versions.</source>
        <translation>このオブジェクトにはバージョンはありません。</translation>
    </message>
    <message>
        <source>Remove selected</source>
        <translation>選択項目の削除</translation>
    </message>
    <message>
        <source>Remove the selected versions from the object.</source>
        <translation>オブジェクトから選択したバージョンを削除する。</translation>
    </message>
    <message>
        <source>Show differences</source>
        <translation>違いを表示</translation>
    </message>
    <message>
        <source>Back</source>
        <translation>戻る</translation>
    </message>
    <message>
        <source>Published version</source>
        <translation>公開バージョン</translation>
    </message>
    <message>
        <source>Translations</source>
        <translation>翻訳</translation>
    </message>
    <message>
        <source>New drafts [%newerDraftCount]</source>
        <translation>新規ドラフト</translation>
    </message>
    <message>
        <source>This object does not have any drafts.</source>
        <translation>このオブジェクトにはドラフトが存在しません。</translation>
    </message>
    <message>
        <source>Differences between versions %oldVersion and %newVersion</source>
        <translation>%oldVersion と %newVersionの違い</translation>
    </message>
    <message>
        <source>Old version</source>
        <translation>旧バージョン</translation>
    </message>
    <message>
        <source>Inline changes</source>
        <translation>インライン変更</translation>
    </message>
    <message>
        <source>Block changes</source>
        <translation>ブロック変更</translation>
    </message>
    <message>
        <source>New version</source>
        <translation>新バージョン</translation>
    </message>
    <message>
        <source>Back to history</source>
        <translation>履歴へ戻る</translation>
    </message>
    <message>
        <source>Version %1 is not available for editing anymore. Only drafts can be edited.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>To edit this version, first create a copy of it.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Version %1 was not created by you. Only your own drafts can be edited.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>You can either change your version history settings in content.ini, remove draft versions or edit existing drafts.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Toggle selection</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Version #%version_number cannot be removed because it is either the published version of the object or because you do not have permission to remove it.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>There is no need to do a copies of untouched drafts.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>You cannot make copies of versions because you do not have permission to edit the object.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>You cannot edit the contents of version #%version_number either because it is not a draft or because you do not have permission to edit the object.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Modified translation</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>design/ezwebin/content/keyword</name>
    <message>
        <source>Keyword: %keyword</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Link</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Type</source>
        <translation type="unfinished">タイプ</translation>
    </message>
</context>
<context>
    <name>design/ezwebin/content/search</name>
    <message>
        <source>Search</source>
        <translation>検索</translation>
    </message>
    <message>
        <source>For more options try the %1Advanced search%2</source>
        <comment>The parameters are link start and end tags.</comment>
        <translation>その他のオプションはこちら%1Advanced search%2</translation>
    </message>
    <message>
        <source>Search tips</source>
        <translation>検索ヒント</translation>
    </message>
    <message>
        <source>Check spelling of keywords.</source>
        <translation>スペリングを確認して下さい。</translation>
    </message>
    <message>
        <source>Search for &quot;%1&quot; returned %2 matches</source>
        <translation>&quot;%1&quot; の検索結果は %2 件です。</translation>
    </message>
    <message>
        <source>The following words were excluded from the search:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>No results were found when searching for &quot;%1&quot;.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Try changing some keywords (eg, &quot;car&quot; instead of &quot;cars&quot;).</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Try searching with less specific keywords.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Reduce number of keywords to get more results.</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>design/ezwebin/content/tipafriend</name>
    <message>
        <source>Tip a friend</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>The message was sent.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Click here to return to the original page.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>The message was not sent.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>The message was not sent due to an unknown error. Please notify the site administrator about this error.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Please correct the following errors:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Your name</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Your email address</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Recipient&apos;s email address</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Comment</source>
        <translation type="unfinished">コメント</translation>
    </message>
    <message>
        <source>Send</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation type="unfinished">キャンセル</translation>
    </message>
</context>
<context>
    <name>design/ezwebin/content/view/versionview</name>
    <message>
        <source>Edit</source>
        <translation>編集</translation>
    </message>
    <message>
        <source>Publish</source>
        <translation>公開</translation>
    </message>
    <message>
        <source>Manage versions</source>
        <translation type="unfinished">バージョン管理</translation>
    </message>
</context>
<context>
    <name>design/ezwebin/edit/comment</name>
    <message>
        <source>Edit %1 - %2</source>
        <translation>%1 - %2の編集</translation>
    </message>
    <message>
        <source>Send for publishing</source>
        <translation>送信して公開</translation>
    </message>
    <message>
        <source>Discard</source>
        <translation>破棄</translation>
    </message>
</context>
<context>
    <name>design/ezwebin/edit/file</name>
    <message>
        <source>Edit %1 - %2</source>
        <translation>%1 - %2の編集</translation>
    </message>
    <message>
        <source>Send for publishing</source>
        <translation>送信して公開</translation>
    </message>
    <message>
        <source>Discard</source>
        <translation>破棄</translation>
    </message>
</context>
<context>
    <name>design/ezwebin/edit/forum_reply</name>
    <message>
        <source>Edit %1 - %2</source>
        <translation>%1 - %2の編集</translation>
    </message>
    <message>
        <source>Send for publishing</source>
        <translation>送信して公開</translation>
    </message>
    <message>
        <source>Discard</source>
        <translation>破棄</translation>
    </message>
</context>
<context>
    <name>design/ezwebin/edit/forum_topic</name>
    <message>
        <source>Edit %1 - %2</source>
        <translation>%1 - %2の編集</translation>
    </message>
    <message>
        <source>Send for publishing</source>
        <translation>送信して公開</translation>
    </message>
    <message>
        <source>Discard</source>
        <translation>破棄</translation>
    </message>
</context>
<context>
    <name>design/ezwebin/embed/forum</name>
    <message>
        <source>Latest from</source>
        <translation>フォーラムからの最新投稿</translation>
    </message>
</context>
<context>
    <name>design/ezwebin/embed/poll</name>
    <message>
        <source>Vote</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>design/ezwebin/ezinfo/about</name>
    <message>
        <source>eZ Publish information: %version</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>What is eZ Publish?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Licence</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Contributors</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Copyright Notice</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Third-Party Software</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Extensions</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>design/ezwebin/ezodf/browse_place</name>
    <message>
        <source>Choose document placement</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Please choose the placement for the OpenOffice.org object.

    Select the placements and click the %buttonname button.
    Using the recent and bookmark items for quick placement is also possible.
    Click on placement names to change the browse listing.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Select</source>
        <translation type="unfinished">選択</translation>
    </message>
</context>
<context>
    <name>design/ezwebin/ezodf/export</name>
    <message>
        <source>OpenOffice.org export</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Export eZ publish content to OpenOffice.org</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Error</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Here you can export any eZ publish content object to an OpenOffice.org Writer document format.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Export Object</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>design/ezwebin/ezodf/import</name>
    <message>
        <source>Document is now imported</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>OpenOffice.org import</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>The object was imported as: %class_name</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Document imported as</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>The images are placed in the media and can be re-used.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Import another document</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Upload file</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Import OpenOffice.org document</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Replace document</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Import to</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>You can import OpenOffice.org Writer documents directly into eZ publish from this page. You are
asked where to place the document and eZ publish does the rest. The document is converted into
the appropriate class during the import, you get a notice about this after the import is done.
Images are placed in the media library so you can re-use them in other articles.</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>design/ezwebin/full/article</name>
    <message>
        <source>Comments</source>
        <translation>コメント</translation>
    </message>
    <message>
        <source>New comment</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>%login_link_startLog in%login_link_end or %create_link_startcreate a user account%create_link_end to comment.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Tip a friend</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>design/ezwebin/full/article_mainpage</name>
    <message>
        <source>Tip a friend</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>design/ezwebin/full/article_subpage</name>
    <message>
        <source>Tip a friend</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>design/ezwebin/full/blog_post</name>
    <message>
        <source>Tags:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Comments</source>
        <translation type="unfinished">コメント</translation>
    </message>
    <message>
        <source>%login_link_startLog in%login_link_end or %create_link_startcreate a user account%create_link_end to comment.</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>design/ezwebin/full/documentation_page</name>
    <message>
        <source>Table of contents</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Created:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Modified:</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>design/ezwebin/full/event</name>
    <message>
        <source>Category</source>
        <translation>カテゴリー</translation>
    </message>
</context>
<context>
    <name>design/ezwebin/full/event_view_calendar</name>
    <message>
        <source>Mon</source>
        <translation>月</translation>
    </message>
    <message>
        <source>Tue</source>
        <translation>火</translation>
    </message>
    <message>
        <source>Wed</source>
        <translation>水</translation>
    </message>
    <message>
        <source>Thu</source>
        <translation>木</translation>
    </message>
    <message>
        <source>Fri</source>
        <translation>金</translation>
    </message>
    <message>
        <source>Sat</source>
        <translation>土</translation>
    </message>
    <message>
        <source>Sun</source>
        <translation>日</translation>
    </message>
    <message>
        <source>Today</source>
        <translation>今日</translation>
    </message>
    <message>
        <source>Category</source>
        <translation>カテゴリー</translation>
    </message>
</context>
<context>
    <name>design/ezwebin/full/event_view_program</name>
    <message>
        <source>Past events</source>
        <translation>過去のイベント</translation>
    </message>
    <message>
        <source>Future events</source>
        <translation>今後のイベント</translation>
    </message>
</context>
<context>
    <name>design/ezwebin/full/feedback_form</name>
    <message>
        <source>Send form</source>
        <translation>フォームの送信</translation>
    </message>
</context>
<context>
    <name>design/ezwebin/full/forum</name>
    <message>
        <source>New topic</source>
        <translation>新規トピック</translation>
    </message>
    <message>
        <source>Keep me updated</source>
        <translation>更新を知らせる</translation>
    </message>
    <message>
        <source>You need to be logged in to get access to the forums. You can do so %login_link_start%here%login_link_end%</source>
        <translation>フォーラムへアクセスするには、以下からログインして下さい.%login_link_start%here%login_link_end%</translation>
    </message>
    <message>
        <source>Topic</source>
        <translation>トピック</translation>
    </message>
    <message>
        <source>Replies</source>
        <translation>返答数</translation>
    </message>
    <message>
        <source>Author</source>
        <translation>投稿者</translation>
    </message>
    <message>
        <source>Last reply</source>
        <translation>最新のコメント</translation>
    </message>
    <message>
        <source>Pages</source>
        <translation>ページ</translation>
    </message>
</context>
<context>
    <name>design/ezwebin/full/forum_reply</name>
    <message>
        <source>Message preview</source>
        <translation>メッセージプレビュー</translation>
    </message>
    <message>
        <source>Author</source>
        <translation>投稿者</translation>
    </message>
    <message>
        <source>Topic</source>
        <translation>トピック</translation>
    </message>
    <message>
        <source>Location</source>
        <translation>配置</translation>
    </message>
    <message>
        <source>Moderated by</source>
        <translation>モデレーター</translation>
    </message>
    <message>
        <source>Edit</source>
        <translation>編集</translation>
    </message>
</context>
<context>
    <name>design/ezwebin/full/forum_topic</name>
    <message>
        <source>Previous topic</source>
        <translation>前トピック</translation>
    </message>
    <message>
        <source>Next topic</source>
        <translation>次トピック</translation>
    </message>
    <message>
        <source>New reply</source>
        <translation>新規返答</translation>
    </message>
    <message>
        <source>Keep me updated</source>
        <translation>更新を知らせる</translation>
    </message>
    <message>
        <source>Author</source>
        <translation>投稿者</translation>
    </message>
    <message>
        <source>Message</source>
        <translation>メッセージ</translation>
    </message>
    <message>
        <source>Location</source>
        <translation>配置</translation>
    </message>
    <message>
        <source>Moderated by</source>
        <translation>モデレーター</translation>
    </message>
    <message>
        <source>Edit</source>
        <translation>編集</translation>
    </message>
    <message>
        <source>Remove</source>
        <translation>削除</translation>
    </message>
    <message>
        <source>Remove this item.</source>
        <translation>このアイテムを削除</translation>
    </message>
    <message>
        <source>You need to be logged in to get access to the forums. You can do so %login_link_start%here%login_link_end%</source>
        <translation type="unfinished">フォーラムへアクセスするには、以下からログインして下さい.%login_link_start%here%login_link_end%</translation>
    </message>
</context>
<context>
    <name>design/ezwebin/full/forums</name>
    <message>
        <source>Topics</source>
        <translation>トピック</translation>
    </message>
    <message>
        <source>Posts</source>
        <translation>ポスト</translation>
    </message>
    <message>
        <source>Last reply</source>
        <translation>最新のコメント</translation>
    </message>
</context>
<context>
    <name>design/ezwebin/full/gallery</name>
    <message>
        <source>View as slideshow</source>
        <translation>スライドショーを表示</translation>
    </message>
</context>
<context>
    <name>design/ezwebin/full/image</name>
    <message>
        <source>Previous image</source>
        <translation>前イメージ</translation>
    </message>
    <message>
        <source>Next image</source>
        <translation>次イメージ</translation>
    </message>
</context>
<context>
    <name>design/ezwebin/full/multicalendar</name>
    <message>
        <source>Event</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Start date</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Category</source>
        <translation type="unfinished">カテゴリー</translation>
    </message>
    <message>
        <source>Description</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>design/ezwebin/full/poll</name>
    <message>
        <source>Vote</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Result</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>design/ezwebin/full/product</name>
    <message>
        <source>Add to basket</source>
        <translation>買い物カゴへ追加</translation>
    </message>
    <message>
        <source>Add to wish list</source>
        <translation>ウイッシュリストへ追加</translation>
    </message>
    <message>
        <source>People who bought this also bought</source>
        <translation>これを購入した人はこんな商品を買っています</translation>
    </message>
</context>
<context>
    <name>design/ezwebin/line/event</name>
    <message>
        <source>Category</source>
        <translation>カテゴリー</translation>
    </message>
</context>
<context>
    <name>design/ezwebin/line/event_calendar</name>
    <message>
        <source>Next events</source>
        <translation>新規イベント</translation>
    </message>
</context>
<context>
    <name>design/ezwebin/line/flash</name>
    <message>
        <source>View flash</source>
        <translation>フラッシュを再生</translation>
    </message>
</context>
<context>
    <name>design/ezwebin/line/forum</name>
    <message>
        <source>Last reply</source>
        <translation>最新のコメント</translation>
    </message>
    <message>
        <source>Enter forum</source>
        <translation>フォーラムへ参加</translation>
    </message>
    <message>
        <source>Number of topics</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Number of posts</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>design/ezwebin/line/forum_reply</name>
    <message>
        <source>Reply to:</source>
        <translation>返信:</translation>
    </message>
</context>
<context>
    <name>design/ezwebin/line/poll</name>
    <message>
        <source>%count votes</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Vote</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>design/ezwebin/line/quicktime</name>
    <message>
        <source>View movie</source>
        <translation>動画を再生</translation>
    </message>
</context>
<context>
    <name>design/ezwebin/line/real_video</name>
    <message>
        <source>View movie</source>
        <translation>動画を再生</translation>
    </message>
</context>
<context>
    <name>design/ezwebin/line/windows_media</name>
    <message>
        <source>View movie</source>
        <translation>動画を再生</translation>
    </message>
</context>
<context>
    <name>design/ezwebin/link</name>
    <message>
        <source>%sitetitle front page</source>
        <translation>%sitetitle フロントページ</translation>
    </message>
    <message>
        <source>Search %sitetitle</source>
        <translation>%sitetitle を検索</translation>
    </message>
    <message>
        <source>Printable version</source>
        <translation>印刷バージョン</translation>
    </message>
</context>
<context>
    <name>design/ezwebin/node/removeobject</name>
    <message>
        <source>Are you sure you want to remove these items?</source>
        <translation>このアイテムを削除してよろしいですか?</translation>
    </message>
    <message>
        <source>%nodename and its %childcount children. %additionalwarning</source>
        <translation>%nodename と %childcount 子アイテム. %additionalwarning</translation>
    </message>
    <message>
        <source>%nodename %additionalwarning</source>
        <translation>%nodename %additionalwarning</translation>
    </message>
    <message>
        <source>Move to trash</source>
        <translation>ごみ箱へ移動</translation>
    </message>
    <message>
        <source>Note</source>
        <translation>備考</translation>
    </message>
    <message>
        <source>Confirm</source>
        <translation>確認</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation>キャンセル</translation>
    </message>
    <message>
        <source>If %trashname is checked, removed items can be found in the trash.</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>design/ezwebin/notification/addingresult</name>
    <message>
        <source>Add to my notifications</source>
        <translation>通知リストに追加</translation>
    </message>
    <message>
        <source>Notification for node &lt;%node_name&gt; already exists.</source>
        <translation>ノード &lt;%node_name&gt;の通知は既に存在します.</translation>
    </message>
    <message>
        <source>Notification for node &lt;%node_name&gt; was added successfully.</source>
        <translation>ノード&lt;%node_name&gt;の通知が加わりました.</translation>
    </message>
    <message>
        <source>OK</source>
        <translation>OK</translation>
    </message>
</context>
<context>
    <name>design/ezwebin/notification/settings</name>
    <message>
        <source>Notification settings</source>
        <translation>通知設定</translation>
    </message>
    <message>
        <source>Store</source>
        <translation>保存</translation>
    </message>
</context>
<context>
    <name>design/ezwebin/pagelayout</name>
    <message>
        <source>Search</source>
        <translation>検索</translation>
    </message>
</context>
<context>
    <name>design/ezwebin/parts/website_toolbar</name>
    <message>
        <source>Create here</source>
        <translation>ここに作成</translation>
    </message>
    <message>
        <source>Move</source>
        <translation>移動</translation>
    </message>
    <message>
        <source>Remove</source>
        <translation>削除</translation>
    </message>
    <message>
        <source>Import</source>
        <translation>インポート</translation>
    </message>
    <message>
        <source>Export</source>
        <translation>エクスポート</translation>
    </message>
    <message>
        <source>Replace</source>
        <translation>リプレイス</translation>
    </message>
    <message>
        <source>About</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Edit: %node_name [%class_name]</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Add locations</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Documentation</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>design/ezwebin/settings/edit</name>
    <message>
        <source>Node notification</source>
        <translation>ノードの通知</translation>
    </message>
    <message>
        <source>Name</source>
        <translation>名前</translation>
    </message>
    <message>
        <source>Class</source>
        <translation>クラス</translation>
    </message>
    <message>
        <source>Section</source>
        <translation>セクション</translation>
    </message>
    <message>
        <source>Select</source>
        <translation>選択</translation>
    </message>
    <message>
        <source>Remove</source>
        <translation>削除</translation>
    </message>
</context>
<context>
    <name>design/ezwebin/shop/basket</name>
    <message>
        <source>Shopping basket</source>
        <translation>買い物カゴ</translation>
    </message>
    <message>
        <source>Account information</source>
        <translation>お客様情報</translation>
    </message>
    <message>
        <source>Confirm order</source>
        <translation>オーダーの確認</translation>
    </message>
    <message>
        <source>Basket</source>
        <translation>買い物カゴ</translation>
    </message>
    <message>
        <source>VAT is unknown</source>
        <translation>税が不明です</translation>
    </message>
    <message>
        <source>VAT percentage is not yet known for some of the items being purchased.</source>
        <translation>購入されるアイテムには税率が不明なものがあます.</translation>
    </message>
    <message>
        <source>This probably means that some information about you is not yet available and will be obtained during checkout.</source>
        <translation>現段階ではあなたの情報の一部は欠けていますが、会計までに全ての情報が補充されることを示しています.</translation>
    </message>
    <message>
        <source>Attempted to add object without price to basket.</source>
        <translation>価格未設定の商品を買い物かごに追加しようとしました.</translation>
    </message>
    <message>
        <source>Your payment was aborted.</source>
        <translation>決済は中止されました.</translation>
    </message>
    <message>
        <source>Count</source>
        <translation>数量</translation>
    </message>
    <message>
        <source>VAT</source>
        <translation>税</translation>
    </message>
    <message>
        <source>Price inc. VAT</source>
        <translation>価格（税込）</translation>
    </message>
    <message>
        <source>Discount</source>
        <translation>割引</translation>
    </message>
    <message>
        <source>Update</source>
        <translation>更新</translation>
    </message>
    <message>
        <source>Remove</source>
        <translation>削除</translation>
    </message>
    <message>
        <source>Selected options</source>
        <translation>選択したオプション</translation>
    </message>
    <message>
        <source>Shipping</source>
        <translation>配送</translation>
    </message>
    <message>
        <source>Order total</source>
        <translation>お買い上げ合計</translation>
    </message>
    <message>
        <source>Continue shopping</source>
        <translation>買い物を続ける</translation>
    </message>
    <message>
        <source>Checkout</source>
        <translation>チェックアウト</translation>
    </message>
    <message>
        <source>The following items were removed from your basket because the products were changed.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Total price ex. VAT</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Total price inc. VAT</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Unknown</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Subtotal ex. VAT</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Subtotal inc. VAT</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>You have no products in your basket.</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>design/ezwebin/shop/confirmorder</name>
    <message>
        <source>Shopping basket</source>
        <translation>買い物カゴ</translation>
    </message>
    <message>
        <source>Account information</source>
        <translation>お客様情報</translation>
    </message>
    <message>
        <source>Confirm order</source>
        <translation>オーダーの確認</translation>
    </message>
    <message>
        <source>Product items</source>
        <translation>お買物明細</translation>
    </message>
    <message>
        <source>Count</source>
        <translation>数量</translation>
    </message>
    <message>
        <source>VAT</source>
        <translation>税</translation>
    </message>
    <message>
        <source>Price inc. VAT</source>
        <translation>価格（税込）</translation>
    </message>
    <message>
        <source>Discount</source>
        <translation>割引</translation>
    </message>
    <message>
        <source>Selected options</source>
        <translation>選択したオプション</translation>
    </message>
    <message>
        <source>Order summary</source>
        <translation>ご注文の要約</translation>
    </message>
    <message>
        <source>Subtotal of items</source>
        <translation>商品小計</translation>
    </message>
    <message>
        <source>Order total</source>
        <translation>お買い上げ合計</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation>キャンセル</translation>
    </message>
    <message>
        <source>Confirm</source>
        <translation>確認</translation>
    </message>
    <message>
        <source>Total price ex. VAT</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Total price inc. VAT</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>design/ezwebin/shop/customerorderview</name>
    <message>
        <source>Order list</source>
        <translation>ご注文リスト</translation>
    </message>
    <message>
        <source>ID</source>
        <translation>ID</translation>
    </message>
    <message>
        <source>Date</source>
        <translation>日付</translation>
    </message>
    <message>
        <source>Total ex. VAT</source>
        <translation>合計（税別）</translation>
    </message>
    <message>
        <source>Total inc. VAT</source>
        <translation>合計（税込）</translation>
    </message>
    <message>
        <source>Purchase list</source>
        <translation>ご購入一覧</translation>
    </message>
    <message>
        <source>Product</source>
        <translation>商品</translation>
    </message>
    <message>
        <source>Amount</source>
        <translation>総数</translation>
    </message>
    <message>
        <source>Customer information</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>design/ezwebin/shop/orderlist</name>
    <message>
        <source>Order list</source>
        <translation type="unfinished">ご注文リスト</translation>
    </message>
    <message>
        <source>Sort result by</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Order time</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>User name</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Order ID</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Ascending</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Sort ascending</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Descending</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Sort descending</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Sort</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>ID</source>
        <translation type="unfinished">ID</translation>
    </message>
    <message>
        <source>Date</source>
        <translation type="unfinished">日付</translation>
    </message>
    <message>
        <source>Customer</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Total ex. VAT</source>
        <translation type="unfinished">合計（税別）</translation>
    </message>
    <message>
        <source>Total inc. VAT</source>
        <translation type="unfinished">合計（税込）</translation>
    </message>
    <message>
        <source>The order list is empty</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Archive</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>design/ezwebin/shop/orderview</name>
    <message>
        <source>Order %order_id [%order_status]</source>
        <translation>注文ID %order_id [%order_status]</translation>
    </message>
    <message>
        <source>Product items</source>
        <translation>お買物明細</translation>
    </message>
    <message>
        <source>Product</source>
        <translation>商品</translation>
    </message>
    <message>
        <source>Count</source>
        <translation>数量</translation>
    </message>
    <message>
        <source>VAT</source>
        <translation>税</translation>
    </message>
    <message>
        <source>Price inc. VAT</source>
        <translation>価格（税込）</translation>
    </message>
    <message>
        <source>Discount</source>
        <translation>割引</translation>
    </message>
    <message>
        <source>Order summary</source>
        <translation>ご注文の要約</translation>
    </message>
    <message>
        <source>Summary</source>
        <translation>要約</translation>
    </message>
    <message>
        <source>Subtotal of items</source>
        <translation>商品小計</translation>
    </message>
    <message>
        <source>Order total</source>
        <translation>お買い上げ合計</translation>
    </message>
    <message>
        <source>Order history</source>
        <translation>お買い上げ履歴</translation>
    </message>
    <message>
        <source>Total price ex. VAT</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Total price inc. VAT</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>design/ezwebin/shop/userregister</name>
    <message>
        <source>Shopping basket</source>
        <translation>買い物カゴ</translation>
    </message>
    <message>
        <source>Account information</source>
        <translation>お客様情報</translation>
    </message>
    <message>
        <source>Confirm order</source>
        <translation>オーダーの確認</translation>
    </message>
    <message>
        <source>Your account information</source>
        <translation>お客様情報</translation>
    </message>
    <message>
        <source>First name</source>
        <translation>名</translation>
    </message>
    <message>
        <source>Last name</source>
        <translation>姓</translation>
    </message>
    <message>
        <source>Company</source>
        <translation>会社・法人名</translation>
    </message>
    <message>
        <source>Street</source>
        <translation>住所</translation>
    </message>
    <message>
        <source>Zip</source>
        <translation>郵便番号</translation>
    </message>
    <message>
        <source>Place</source>
        <translation>市区町村</translation>
    </message>
    <message>
        <source>State</source>
        <translation>都道府県・州</translation>
    </message>
    <message>
        <source>Country</source>
        <translation>国名</translation>
    </message>
    <message>
        <source>Comment</source>
        <translation>コメント</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation>キャンセル</translation>
    </message>
    <message>
        <source>Continue</source>
        <translation>次へ</translation>
    </message>
    <message>
        <source>All fields marked with * must be filled in.</source>
        <translation>* 印の付いた項目は必須入力です.</translation>
    </message>
    <message>
        <source>Input did not validate. All fields marked with * must be filled in.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Email</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>design/ezwebin/shop/wishlist</name>
    <message>
        <source>Wish list</source>
        <translation>ウイッシュリスト</translation>
    </message>
    <message>
        <source>Product</source>
        <translation>商品</translation>
    </message>
    <message>
        <source>Count</source>
        <translation>数量</translation>
    </message>
    <message>
        <source>Selected options</source>
        <translation>選択したオプション</translation>
    </message>
    <message>
        <source>Store</source>
        <translation>保存</translation>
    </message>
    <message>
        <source>Remove items</source>
        <translation>アイテムを削除</translation>
    </message>
    <message>
        <source>Empty wish list</source>
        <translation>ウィッシュリストを空にする</translation>
    </message>
</context>
<context>
    <name>design/ezwebin/simplified_treemenu/show_simplified_menu</name>
    <message>
        <source>Fold/Unfold</source>
        <translation>開く/たたむ</translation>
    </message>
    <message>
        <source>Node ID: %node_id Visibility: %visibility</source>
        <translation>ノードID: %node_id 表示設定:%node_visibility </translation>
    </message>
</context>
<context>
    <name>design/ezwebin/user/activate</name>
    <message>
        <source>Activate account</source>
        <translation>アカウントを有効にする</translation>
    </message>
    <message>
        <source>Your account is now activated.</source>
        <translation>アカウントは有効になりました。</translation>
    </message>
    <message>
        <source>Sorry, the key submitted was not a valid key. Account was not activated.</source>
        <translation>パスワードが無効なためアカウントを有効に出来ません。</translation>
    </message>
    <message>
        <source>Your account is already active.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>OK</source>
        <translation type="unfinished">OK</translation>
    </message>
</context>
<context>
    <name>design/ezwebin/user/edit</name>
    <message>
        <source>User profile</source>
        <translation>ユーザ・プロフィールユーザ・プロフィール</translation>
    </message>
    <message>
        <source>Username</source>
        <translation>ユーザ名</translation>
    </message>
    <message>
        <source>Name</source>
        <translation>名前</translation>
    </message>
    <message>
        <source>My drafts</source>
        <translation>マイドラフト</translation>
    </message>
    <message>
        <source>My orders</source>
        <translation>マイオーダー</translation>
    </message>
    <message>
        <source>My notification settings</source>
        <translation>通知設定</translation>
    </message>
    <message>
        <source>My wish list</source>
        <translation>ウイッシュリスト</translation>
    </message>
    <message>
        <source>Edit profile</source>
        <translation>プロファイルの編集</translation>
    </message>
    <message>
        <source>Change password</source>
        <translation>パスワードの変更</translation>
    </message>
    <message>
        <source>Email</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>design/ezwebin/user/forgotpassword</name>
    <message>
        <source>Password was successfully generated and sent to: %1</source>
        <translation>新しいパスワードは %1 に送信されました.</translation>
    </message>
    <message>
        <source>The key is invalid or has been used. </source>
        <translation>キーが無効またはすでに使用済みです.</translation>
    </message>
    <message>
        <source>Have you forgotten your password?</source>
        <translation>パスワードを忘れましたか?</translation>
    </message>
    <message>
        <source>Generate new password</source>
        <translation>新規パスワードの作成</translation>
    </message>
    <message>
        <source>An email has been sent to the following address: %1. It contains a link you need to click so that we can confirm that the correct user has received the new password.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>There is no registered user with that email address.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>If you have forgotten your password, enter your email address and we will create a new password and send it to you.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Email</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>design/ezwebin/user/login</name>
    <message>
        <source>Login</source>
        <translation>ログイン</translation>
    </message>
    <message>
        <source>Could not login</source>
        <translation>ログインできません</translation>
    </message>
    <message>
        <source>A valid username and password is required to login.</source>
        <translation>正しいユーザ名とパスワードでログインしてください.</translation>
    </message>
    <message>
        <source>Access not allowed</source>
        <translation>アクセスは許可されません</translation>
    </message>
    <message>
        <source>You are not allowed to access %1.</source>
        <translation>%1 へのアクセスはできません.</translation>
    </message>
    <message>
        <source>Username</source>
        <comment>User name</comment>
        <translation>ユーザ名</translation>
    </message>
    <message>
        <source>Password</source>
        <translation>パスワード</translation>
    </message>
    <message>
        <source>Remember me</source>
        <translation>次回からログインを省略</translation>
    </message>
    <message>
        <source>Login</source>
        <comment>Button</comment>
        <translation>ログイン</translation>
    </message>
    <message>
        <source>Forgot your password?</source>
        <translation>パスワードを忘れましたか?</translation>
    </message>
    <message>
        <source>Log in to the eZ Publish Administration Interface</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Sign up</source>
        <comment>Button</comment>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>design/ezwebin/user/password</name>
    <message>
        <source>Change password for user</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Please retype your old password.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Password didn&apos;t match, please retype your new password.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Password successfully updated.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Old password</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>New password</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Retype password</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>OK</source>
        <translation type="unfinished">OK</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation type="unfinished">キャンセル</translation>
    </message>
</context>
<context>
    <name>design/ezwebin/user/register</name>
    <message>
        <source>Register user</source>
        <translation>ユーザー登録</translation>
    </message>
    <message>
        <source>Input did not validate</source>
        <translation>無効な入力です</translation>
    </message>
    <message>
        <source>Input was stored successfully</source>
        <translation>入力の保存に成功しました</translation>
    </message>
    <message>
        <source>Register</source>
        <translation>登録</translation>
    </message>
    <message>
        <source>Discard</source>
        <translation>破棄</translation>
    </message>
    <message>
        <source>Unable to register new user</source>
        <translation>新規登録が出来ません</translation>
    </message>
    <message>
        <source>Back</source>
        <translation>戻る</translation>
    </message>
</context>
<context>
    <name>design/ezwebin/user/success</name>
    <message>
        <source>User registered</source>
        <translation>登録されました</translation>
    </message>
    <message>
        <source>Your account was successfully created.</source>
        <translation>アカウントが作成されました。</translation>
    </message>
    <message>
        <source>Your account was successfully created. An email will be sent to the specified address. Follow the instructions in that email to activate your account.</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>design/ezwebin/view/ezbinaryfile</name>
    <message>
        <source>The file could not be found.</source>
        <translation>ファイルが見つかりません。</translation>
    </message>
</context>
<context>
    <name>design/ezwebin/view/ezmedia</name>
    <message>
        <source>No %link_startFlash player%link_end avaliable!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>No media file is available.</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>design/ezwebin/view/ezprice</name>
    <message>
        <source>Price</source>
        <translation>価格</translation>
    </message>
    <message>
        <source>Your price</source>
        <translation>ご提供価格</translation>
    </message>
    <message>
        <source>You save</source>
        <translation>割引額</translation>
    </message>
</context>
<context>
    <name>design/ezwebin/view/sitemap</name>
    <message>
        <source>Site map</source>
        <translation>サイトマップ</translation>
    </message>
</context>
</TS>
