eZjaxx extension 0.9 README
Ajax extension for eZ Publish 3.6 - 3.9

  

What is the eZjaxx extension?
==============================

eZjaxx is a take on improving the speed of eZ Publish
and include some bells and whistles to improve editing
and admin workflow.
This is done by both template improvements and the use 
of Javascript ($jaxx: mini JavaScript library).

So far this is done in the admin section:
* content structure menu: Ajaxed menu, only loads the nodes you
  display.

* Sidebar Ajax search, searches directly on title names bye 
  default, this can be changed in jaxx.ini.
  This search works in browse mode as well, so you can find stuff faster.
  In edit mode on pages with object relation(s) (browse type) you can 
  drag&drop nodes from search to targets.

* drop down suggestions on keyword entries as you type based on 
  keywords in the system. By default on same class type and 
  limited to 7 hits (change this in jaxx.ini).

* Menu width, menu width is set by JavaScript and a set preferences
  is set behind the scenes.

* add new Roll is ajaxed so you skip one step.

* Copy Here menu entry: a nice function when you want to create lots of nodes fast,
  or want a copy of some node under the same parent node without the browse steps.
  This is in the menu on node icons in the sub elements list.
 
Base:
* An ajaxed gallery example, rename design/base/..../image_.tpl to test it out


What is the $jaxx JavaScript library?
===================================

$jaxx is a mini JavaScript library, inspired by
larger library's like jQuery / Prototype / Moo.fx / Moo.tools.

$jaxx extends the following objects to include some js 1.5 / 1.6 functionality:
*Array
  indexOf: find the index of a array element
  forEach: loop array objects
  filter: filter an array, return a new array with the filtered values
  map: return a new array with the result of work on the original array
  flatten: (not part of js spec) a array function to flatten array of array's
*String:
  trim: (not part of js spec) Trim empty space
*Function:
  bind:(not part of js spec) bind this value and variables to a function for later use.
  bindE:(not part of js spec) same as bind but returns one more value set by the event caller.
*Window:
  XMLHttpRequest: makes a wrapper for xmlhttprequest on ie browsers that don't support 
                  it nativly (ie < ie 7).
                  
$jaxx global functions:
*Array:
  $c: makes sure that an object is returned as a native array.
*Object:
  $ext: extends an object with another object.
*Event:
  $addEvent: a cross browser function to add events to elements
            event added thru this is automaticly removed on unload in
            ie because of memleak problems in ie.
  $removeEvent: same as addEvent, but this one removes it.
*Dom:
  $getPos: get position of an element
           returns an object with pl and pt (position Left and Top)
  $getSize: get size of an element, if no element is defined, document.body is asumed
           returns an object with ow, oh, sw and sh (Offset and Scroll, Height and Width)
  $getScroll: get the current scroll position of the document
           returns an object with sl and st (ScrollTop and Left)
  $: shortcut to document.getElementsById, can take more the one element.
           example: $('my_div_id', 'my_other_dic_id')
           returns: an native array of elements, extended with $jaxx
  $$: this one handels css queryes, you can set element type, class, id and simple attribute match
           definition: $$(String of elements to lookfor, Parentelement(default:document), existing jaxx array to extend (default:create new))
           example: $$('div#my_div_id') or $$('a.my_links') or $$('input[type=checkbox]')
           returns: an native array of elements, extended with $jaxx
*Cookie:
  $getCookie: get the current value of a cookie by name
           returns a empty string if no cookie by that name is found
  $setCookie: set a cookie by cookie name and number of days to expire
           Cookie is deleted if number of days are undefined
*Ajax:
  $ajax: a complete Ajax function for getting xml or html from the server
         takes a object of options as first attribute and the url as the second, options:
           method: GET or POST method, default is GET.
           postBody: if the method is POST, this is for the post value
           onError: function to call on error with status(404) and statusText(Not found)
           onLoad: Function to call on successful load, with request as parameter
           update: element to update with responseText.
           xsl: and xsl request object if you want to xsl transform the response before update 'update' element.
  $xsl: a simple but complete cross browser xslt function
           takes xsl as response object, xml as response object and variables as object
  $json: a micro json function for script retrieval or injection.
           takes a string of an url or script content, and function to call onload.
           if the string is not an url (ending with .js) the onload will not be set.



$jaxx: extending an array with functions for
       slide shows: 						slide, show, hide, toggle, start, stop
       loading html / text:				load
       Mouse or click based navigation: nav
       drag and drop: 					drag
       event: 							addEvent, click
       class: 							addClass, removeClass
       DOM: 							remove, parentIndex, parentPosition
       other:
          fx:  set options for $jaxx, target values for fading fx's 
               and recalculate the element propertie cache
          merge: merge more elements to the array
          check: check / uncheck check boxes
          voidLink: replace href on links with void(0) (do nothing)
       
 !!Arrays created in $ and $$ are automaticly extended with the $jaxx object.
       
 
 This is all for now, ask me on ar [AT] ez.no if something is unclear.  