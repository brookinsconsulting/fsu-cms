<div id="toolbar_newssearch" style="display: none;">

<div class="box-header"><div class="box-tc"><div class="box-ml"><div class="box-mr">{section show=$first}<div class="box-tl"><div class="box-tr">{/section}

<h4>{ezini( 'JaxxSettings', 'SearchBarName', 'jaxx.ini' )}</h4>

</div></div></div></div>{section show=$first}</div></div>{/section}

{section show=$last}
<div class="box-bc"><div class="box-ml"><div class="box-mr"><div class="box-bl"><div class="box-br"><div class="box-content">
{section-else}
<div class="box-ml"><div class="box-mr"><div class="box-content">
{/section}
<div style="position: relative;">


<form id="newssearchform" action={'/content/search/'|ezurl} method="get" onsubmit="SearchAjaxLoad(0);return false;" style="padding:2px;">
<input id="newssearchtext" name="SearchText" type="text" size="12" value="" /><br />
<input id="newssearchbutton" class="button" name="SearchButton" type="submit" value="{'Search'|i18n( 'design/admin/pagelayout' )}" />



<div id="newssearchresult" style="display: none;">
<a class="windowclose" href="JavaScript:searchres.hide();searchform.show();"><img alt="Close search result" border="0" src={"images/close.gif"|ezdesign} /></a>
<ul style="display:block">
<li> Searching... </li>
</ul>
<br />
</div>

</form>
<br />

</div>
</div></div></div>{section show=$last}</div></div></div>{/section}

</div>
<script type="text/javascript">
<!--

function SearchAjaxLoad(o){ldelim}

        var value = searchform[1].value;
        if (value.length < 2) return false;
        var url = {concat("/jaxx/search/", $ui_context)|ezurl} + '/' + o;
        searchtarget[0].innerHTML = "<li> Searching... <\/li>";
        searchtarget.load(url, searchtarget[0], 'SearchStr='+value);
        return false;

{rdelim}

var searchres = $('newssearchresult'), searchtarget= $$( 'ul', searchres ).fx({ldelim}method:'POST'{rdelim});
var searchform = $('newssearchbutton', 'newssearchtext');
$addEvent(searchform[0],'click', function(){ldelim} searchform.hide(); searchres.show() {rdelim});
var searchbox = $('toolbar_newssearch').fx({ldelim} opacity: true, duration: 200 {rdelim});

{if eq( $ui_context, 'edit' )}
{literal}

var searchObjectRelation, formAjaxPostBack = new $ajax({ method: "POST" });

$addEvent(window,'load',function(){
    searchObjectRelation = $$('div.objectRelationDropp', $('maincontent-design'));
    searchtarget.o.onLoad = relationsAjaxLoad;
    if (searchObjectRelation.length != 0) searchbox.show();
});

{/literal}
{else}
{literal}

$addEvent(window,'load',function(){ searchbox.show(); });

{/literal}
{/if}

-->
</script>