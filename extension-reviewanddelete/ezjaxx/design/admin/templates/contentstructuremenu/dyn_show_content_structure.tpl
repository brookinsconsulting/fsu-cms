{section loop=$#currentNodeList}
{def $toolTip        = ""
     $visibility     = 'Visible'
     $itemobject     = $:item.object
     $classes        = false()
     $haveChildren   = false()
}
{if $:itemobject.content_class.is_container}
{set $haveChildren   = fetch( 'content', 'list_count', hash(
     						'parent_node_id', $:item.node_id,
              				'class_filter_type',  'include',
             				'class_filter_array', $#ShowClasses) )|gt(0)
     $classes        = first_set( $:item.classes_js_array, false() )}
{/if}

<li id="n{$:item.node_id}" class="{if $#last_key|eq($:key)}lastli{/if}{if  $:item.node_id|eq($#currentNodeID )} currentnode{/if}">

{if $:haveChildren}
 <a class="openclose" href={$:item.url_alias|ezurl} title="{'Fold/Unfold'|i18n('design/admin/contentstructuremenu')}" onclick="ezpopmenu_hideAll();"><img border="0" src={"images/content_tree-open.gif"|ezdesign()} alt="Open sub menu" /></a>
{else}
 <span class="openclose"><img alt="." border="0" src={"images/1x1.gif"|ezdesign()} /></span>
{/if}

{if $:item.is_hidden}
{set $visibility = 'Hidden'}
{elseif $:item.is_invisible}
{set $visibility = 'Hidden by superior'}
{/if}
{set $toolTip = 'Node ID: %node_id Visibility: %visibility' |
                i18n("contentstructuremenu/show_content_structure", , hash( '%node_id'      , $:item.node_id,
                                                                            '%visibility'   , $:visibility ) )}

{if ne($#ui_context, 'browse')}
 <a class="nodeicon" href="#" onclick="ezpopmenu_showTopLevel( event, 'ContextMenu', ez_createAArray( new Array( '%nodeID%', {$:item.node_id}, '%objectID%', {$:item.contentobject_id}{if is_set($:itemobject.language_js_array)}, '%languages%', {$:itemobject.language_js_array}{/if}{if $:classes}, '%classList%', {$:classes}{/if} ) ) , '{$:item.name|shorten(18)|wash(javascript)}', {$:item.node_id} {cond( eq( $:classes, "''" ), ", 'menu-create-here'","" )} );return false;">{$:item.class_identifier|class_icon( $#classIconsSize, "[%classname] Click on the icon to get a context sensitive menu."|i18n( 'design/admin/contentstructuremenu',, hash( '%classname', $:item.class_name|wash ) ) )}</a>
 <a class="nodetext" href={$:item.url_alias|ezurl} title="{$:toolTip}">
{else}
 <a class="nodeicon" href="#">{$:item.class_identifier|class_icon( $#classIconsSize )}</a>
 <a class="nodetext" href={concat("/content/browse/", $:item.node_id)|ezurl} title="{$:toolTip}">
{/if}
{if $:item.is_hidden}<span class="node-name-hidden">{elseif $:item.is_invisible}<span class="node-name-hiddenbyparent">{else}<span class="node-name-normal">{/if}{$:item.name|wash}</span>{if $:item.is_hidden}<span class="node-hidden">(Hidden)</span>{elseif $:item.is_invisible}<span class="node-hiddenbyparent">(Hidden by parent)</span>{/if}
</a>

{if $:haveChildren}
<ul class="subMenu" style="display:none;">
 <li class="lastli loading">{'Please wait...'|i18n('design/standard/ezjaxx')}</li>
</ul>
{/if}
</li>
{undef	$toolTip
		$visibility
		$classes
		$haveChildren
		$itemobject
}
{/section}