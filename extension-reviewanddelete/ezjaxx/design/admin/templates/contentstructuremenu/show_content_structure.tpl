{def $last_key = $:currentNodeList|count|dec
	 $showlvl = or( $#maxDepth|eq(0), $#maxDepth|gt($:lvl) )}

{section loop=$:currentNodeList}
{def $itemobject       = $:item.object
     $toolTip          = ""
     $visibility       = 'Visible'
     $showchildren     = false()
}
{if $#menuarray|contains($:item.node_id)}
  {set $showchildren = $:showlvl}
{/if}
{if $:itemobject.content_class.is_container|not()}
   {def $haveChildren = false()
        $itemClasses  = false() }
{elseif $:showchildren}
   {def $tempChildren  = fetch( 'content', 'list', hash(
     						'parent_node_id', $:item.node_id,
              				'class_filter_type',  'include',
             				'class_filter_array', $#ShowClasses,
             				'limit', $#maxNodes,
             				'sort_by', $:item.sort_array
             				 ) )
   		$haveChildren = $tempChildren|count|gt(0)
        $itemClasses  = first_set( $:item.classes_js_array, false() )}
{else}
   {def $haveChildren = fetch( 'content', 'list_count', hash(
     						'parent_node_id', $:item.node_id,
              				'class_filter_type',  'include',
             				'class_filter_array', $#ShowClasses ) )|gt(0)
        $itemClasses  = first_set( $:item.classes_js_array, false() )}
{/if}

<li id="n{$:item.node_id}" class="{if $:last_key|eq($:key)}lastli{/if}{if $:item.node_id|eq($#currentNodeID )} currentnode{/if}">

{if $:is_not_root_node|not}
{elseif $:haveChildren}
 <a class="openclose" href={$:item.url_alias|ezurl} title="{'Fold/Unfold'|i18n('design/admin/contentstructuremenu')}" onclick="ezpopmenu_hideAll();"><img border="0" {if $:showchildren}src={"images/content_tree-close.gif"|ezdesign()} alt="Close sub menu"{else}src={"images/content_tree-open.gif"|ezdesign()} alt="Open sub menu"{/if} /></a>
{else}
 <span class="openclose"><img alt="image placeholder" border="0" src={"images/1x1.gif"|ezdesign()} /></span>
{/if}

{if and(is_set($#showToolTips), $#showToolTips|eq('enabled'))}
{if $:item.is_hidden}
    {set $visibility = 'Hidden'}
{elseif $:item.is_invisible}
    {set $visibility = 'Hidden by superior'}
{/if}
{set $toolTip = 'Node ID: %node_id Visibility: %visibility' |
                i18n("contentstructuremenu/show_content_structure", , hash( '%node_id'      , $:item.node_id,
                                                                            '%visibility'   , $:visibility ) )}
{/if}


{if ne($#ui_context, 'browse')}
 <a class="nodeicon" href="#" onclick="ezpopmenu_showTopLevel( event, 'ContextMenu', ez_createAArray( new Array( '%nodeID%', {$:item.node_id}, '%objectID%', {$:item.contentobject_id}{if is_set($:itemobject.language_js_array)}, '%languages%', {$:itemobject.language_js_array}{/if}{if $:itemClasses}, '%classList%', {$:itemClasses}{/if} ) ) , '{$:item.name|shorten(18)|wash(javascript)}', {$:item.node_id}{cond( eq( $:itemClasses, "''" ), ", ' menu-create-here '"," " )}); return false;">{$:item.class_identifier|class_icon( $#classIconsSize, "[%classname] Click on the icon to get a context sensitive menu."|i18n( 'design/admin/contentstructuremenu',, hash( '%classname', $:item.class_name|wash ) ) )}</a>
 <a class="nodetext" href={$:item.url_alias|ezurl} title="{$:toolTip}">
{else}
 <a class="nodeicon" href="#">{$:item.class_identifier|class_icon( $#classIconsSize )}</a>
 <a class="nodetext" href={concat("/content/browse/", $:item.node_id)|ezurl} title="{$:toolTip}">
{/if}
{if $:item.is_hidden}<span class="node-name-hidden">{elseif $:item.is_invisible}<span class="node-name-hiddenbyparent">{else}<span class="node-name-normal">{/if}{$:item.name|wash}</span>{if $:item.is_hidden}<span class="node-hidden">(Hidden)</span>{elseif $:item.is_invisible}<span class="node-hiddenbyparent">(Hidden by parent)</span>{/if}
</a>


{if $:haveChildren}
<ul{if $:is_not_root_node} class="subMenu"{/if}
{if $:showchildren}>
	{include name=SubMenu uri="design:contentstructuremenu/show_content_structure.tpl" currentNodeList=$:tempChildren ui_context=$:ui_context is_not_root_node=true() lvl=$:lvl|inc}
{else} style="display:none;">
	<li class="lastli loading">{'Please wait...'|i18n('design/standard/ezjaxx')}</li>
{/if}
</ul>
{/if}
</li>
{undef	$itemobject
		$toolTip
		$visibility
		$showchildren
		$itemClasses
		$haveChildren
}
{/section}
{undef $last_key $showlvl}