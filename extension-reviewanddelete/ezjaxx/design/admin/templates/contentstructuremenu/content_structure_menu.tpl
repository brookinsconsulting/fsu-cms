{if is_set($module_result.path.0.node_id)}
    {def $rootNodeID       = $module_result.path.0.node_id
         $menuarray        = array( 1 )
         $menucachesubtree = "/" }
	{foreach $module_result.path as $item}
		{if is_set($item.node_id)}
			{set $menuarray = $menuarray|append( $item.node_id )}
			{if $item.url_alias}
				{set $menucachesubtree = concat( $item.url_alias , "/" )}
			{/if}
		{/if}
	{/foreach}
{elseif is_set( $custom_root_node_id )}
    {def $rootNodeID       = $custom_root_node_id
         $menuarray        = array( 1, $:rootNodeID )
         $menucachesubtree = "/" }
{else}
    {def $rootNodeID       = ezini( 'TreeMenu', 'RootNodeID', 'contentstructuremenu.ini' )
         $menuarray        = array( 1, $:rootNodeID )
         $menucachesubtree = "/" }
{/if}

{cache-block keys=array($menuarray|implode( ',' ), $current_user.role_id_list|implode( ',' ), $current_user.limited_assignment_value_list|implode( ',' ), $ui_context) subtree_expiry=$menucachesubtree}
{def $classIconsSize         = ezini( 'TreeMenu', 'ClassIconsSize'   , 'contentstructuremenu.ini' )
     $showToolTips           = ezini( 'TreeMenu', 'ToolTips'         , 'contentstructuremenu.ini' )
     $ShowClasses            = ezini( 'TreeMenu', 'ShowClasses'      , 'contentstructuremenu.ini' )
	 $currentNodeID          = first_set($module_result.node_id, $rootNodeID)
     $maxNodes               = ezini( 'TreeMenu', 'MaxNodes'         , 'contentstructuremenu.ini' )
     $maxDepth               = ezini( 'TreeMenu', 'MaxDepth'         , 'contentstructuremenu.ini' )
     $rootNode               = array( fetch( 'content', 'node',
                                       hash( 'node_id', $rootNodeID
         			                    )))}
<ul id="content_tree_menu">
{include name=SubMenu uri="design:contentstructuremenu/show_content_structure.tpl" currentNodeList=$rootNode ui_context=$ui_context is_not_root_node=false() lvl=1}
</ul>
 
<script language="JavaScript" type="text/javascript">
<!--
var menuAjaxImage = {ldelim}

	open:{"images/content_tree-open.gif"|ezdesign},
	close:{"images/content_tree-close.gif"|ezdesign},
	pre: new Image()

{rdelim};
var menuAjaxRoot = {concat("/jaxx/expand/", $ui_context, "/", $currentNodeID)|ezurl}, menuRoot = $('content_tree_menu')[0];
var menuLinks = $$('a.openclose', menuRoot).voidLink().click(menuAction);
var menuTargets = $$('ul.subMenu', menuRoot).fx({ldelim}onLoad: menuAjaxLoad, opacity: true, duration: 45{rdelim});
menuAjaxImage.pre.src = menuAjaxImage.open;
menuAjaxImage.pre.src = menuAjaxImage.close;

// -->
</script>
{undef $classIconsSize $showToolTips $currentNodeID $rootNode $ShowClasses}
{/cache-block}
{undef $rootNodeID $menuarray $menucachesubtree}