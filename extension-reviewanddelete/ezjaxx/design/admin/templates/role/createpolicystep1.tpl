<form action={concat( $module.functions.edit.uri, '/', $role.id, '/' )|ezurl} method="post" id="roleedit_form">

<div class="context-block">

{* DESIGN: Header START *}<div class="box-header"><div class="box-tc"><div class="box-ml"><div class="box-mr"><div class="box-tl"><div class="box-tr">

<h1 class="context-title">{'Create a new policy for the <%role_name> role'|i18n( 'design/admin/role/createpolicystep1',, hash( '%role_name', $role.name ) )|wash}</h1>

{* DESIGN: Mainline *}<div class="header-mainline"></div>

{* DESIGN: Header END *}</div></div></div></div></div></div>

{* DESIGN: Content START *}<div class="box-ml"><div class="box-mr"><div class="box-content">

<div class="context-attributes">
<p>
{'Welcome to the policy wizard. This three step wizard will help you create a new policy which will be added to the role that is currently being edited. The wizard can be aborted at any stage by using the "Cancel" button.'|i18n( 'design/admin/role/createpolicystep1' )}
</p>

<hr />
<div class="roleedit_slide">
<h2>{'Step one: select module'|i18n( 'design/admin/role/createpolicystep1' )}</h2>
<p>
{'Instructions'|i18n( 'design/admin/role/createpolicystep1' )}:
</p>
<ol>
<li>{'Use the drop-down menu to select the module that you wish to grant access to.'|i18n( 'design/admin/role/createpolicystep1' )}</li>
<li>{'Click one of the "Grant.." buttons (explained below) in order to go to the next step.'|i18n( 'design/admin/role/createpolicystep1' )}</li>
</ol>
<p>
{'The "Grant access to all functions" button will create a policy that grants unlimited access to all functions of the selected module. If you wish to limit the access method to a specific function, use the "Grant access to a function" button. Please note that function limitation is only supported by some modules (the next step will reveal if it works or not).'|i18n( 'design/admin/role/createpolicystep1' )}
</p>
</div>

<div class="roleedit_slide" style="display:none">
<h2>{'Step two: select function'|i18n( 'design/admin/role/createpolicystep2' )}</h2>
<p>
{'Instructions'|i18n( 'design/admin/role/createpolicystep2' )}:
</p>
<ul>
<li>{'Use the drop-down menu to select the function that you wish to grant access to.'|i18n( 'design/admin/role/createpolicystep2' )}</li>
<li>{'Click on one of the "Grant.." buttons (explained below) in order to go to the next step.'|i18n( 'design/admin/role/createpolicystep2' )}</li>
</ul>
<p>
{'The "Grant full access" button will create a policy that grants unlimited access to the selected function within the module that was specified in step one. If you wish to limit the access method in some way, click the "Grant limited access" button. Function limitation is only supported by some functions. If unsupported, eZ publish will simply set up a policy with unlimited access to the selected function.'|i18n( 'design/admin/role/createpolicystep2' )}
</p>
</div>


<div class="block">
    <div class="element">
    <label>{'Module'|i18n( 'design/admin/role/createpolicystep1' )}:</label>
    <select name="Modules" id="roleedit_modules">
    <option value="*">{'Every module'|i18n( 'design/admin/role/createpolicystep1' )}</option>
    {section var=Modules loop=$modules }
    <option value="{$Modules.item}">{$Modules.item}</option>
    {/section}
    </select>
    <input type="hidden" name="CurrentModule" id="CurrentModule" value="" />
    </div>
    
    <div class="element" id="roleedit_functions_div" style="display:none; width: 230px;">
	<label>{'Function'|i18n( 'design/admin/role/createpolicystep2' )}:</label>
	<select name="ModuleFunction" id="roleedit_functions">
	{section name=Functions loop=$functions}
	<option value="{$Functions:item}">{$Functions:item}</option>
	{/section}
	</select>
	</div>

</div>

<div class="block">
<input class="button" type="submit" name="AddModule" value="{'Grant access to all functions'|i18n( 'design/admin/role/createpolicystep1' )}" />
<input class="button" type="submit" name="CustomFunction" value="{'Grant access to one function'|i18n( 'design/admin/role/createpolicystep1' )}" />
<span id="roleedit_buttons" style="display:none">
<input class="button" type="submit" name="AddFunction" value="{'Grant full access'|i18n( 'design/admin/role/createpolicystep2' )}" />
<input class="button" type="submit" name="Limitation" value="{'Grant limited access'|i18n( 'design/admin/role/createpolicystep2' )}" />
</span>
</div>

</div>

{* DESIGN: Content END *}</div></div></div>

<div class="controlbar">
{* DESIGN: Control bar START *}<div class="box-bc"><div class="box-ml"><div class="box-mr"><div class="box-tc"><div class="box-bl"><div class="box-br">
<div class="block">
<input class="button-disabled" type="submit" value="{'OK'|i18n( 'design/admin/role/createpolicystep1' )}" disabled="disabled" />
<input class="button" type="submit" value="{'Cancel'|i18n( 'design/admin/role/createpolicystep1' )}" />
</div>
{* DESIGN: Control bar END *}</div></div></div></div></div></div>
</div>

</div>

</form>
<script type="text/javascript">
<!--
var jxRe_url = {"/jaxx/roleedit/"|ezurl};

{literal}

var jxRe_droppModules = $('roleedit_modules').addEvent('change', roleModuleChange);
var jxRe_droppFunctions = $('roleedit_functions');
var jxRe_buttons_div = $('roleedit_buttons');
var jxRe_button = $$('input[name=Limitation]', jxRe_buttons_div[0]).merge($('roleedit_functions_div'));
var jxRe_slide = $$('div.roleedit_slide');
jxRe_droppFunctions.o.onLoad = roleFunctionsOnLoad;


$$('input[name=CustomFunction]').hide();


function roleModuleChange(e){
	jxRe_buttons_div.show();
	e = e.target || e.srcElement;
	e = e.options[e.selectedIndex].value;
	$('CurrentModule')[0].value = e;
	var url = jxRe_url + '/' + e;
	jxRe_droppFunctions.load(url);
}

function roleFunctionsOnLoad(r){
	var str = r.responseText;
	if (str.indexOf('<option') != -1){
		jxRe_button.show();
		jxRe_slide.slide(1);
		if (jxRe_droppFunctions[0].outerHTML !== undefined) jxRe_droppFunctions[0].outerHTML = '<select name="ModuleFunction" id="roleedit_functions">' + str + '<\/select>';
	} else {
		jxRe_button.hide();
		jxRe_slide.slide(0);
	}
}



{/literal}

-->
</script>
