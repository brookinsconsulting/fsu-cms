{def $toolTip      = ""
     $visibility   = ""
	 $classes        = first_set($:node.classes_js_array, false())
}
{default classIconsSize = ezini( 'TreeMenu', 'ClassIconsSize', 'contentstructuremenu.ini' )
             last_item      = false()
             is_root_node=false()}

<li id="sn{$:node.node_id}" class="draggable">
<a class="nodeicon" href="#"{if eq($ui_context, "navigation")} onclick="ezpopmenu_showTopLevel( event, 'ContextMenu', ez_createAArray( new Array( '%nodeID%', {$:node.node_id}, '%objectID%', {$:node.object.id}{if is_set($:node.object.language_js_array)}, '%languages%', {$:node.object.language_js_array}{/if}{if $:classes}, '%classList%', {$:classes} {/if}) ) , '{$:node.object.name|shorten(18)|wash(javascript)}', {$:node.node_id} {if $:classes}{cond( eq( $:classes, "''" ), ", 'menu-create-here'",'' )} {/if}); return false;"{/if}>{$:node.object.class_identifier|class_icon( $:classIconsSize, "[%classname] Click on the icon to get a context sensitive menu."|i18n( 'design/admin/contentstructuremenu',, hash( '%classname', $:node.object.class_name|wash ) ) )}</a>

{if $:node.is_hidden}
{set $visibility = 'Hidden'}
{elseif $:node.is_invisible}
{set $visibility = 'Hidden by superior'}
{/if}
{set $toolTip = 'Node ID: %node_id Visibility: %visibility'|
                i18n("contentstructuremenu/show_content_structure", , hash( '%node_id'      , $:node.node_id,
                '%visibility'   , $:visibility ) ) }
{if  eq($ui_context, "browse")}
<a class="nodetext" href={concat("/content/browse/", $:node.node_id)|ezurl} title="{$:toolTip}">{elseif  ne($ui_context, "edit")}<a class="nodetext" href={$:node.url_alias|ezurl} title="{$:toolTip}">{/if}<span style="top: 0px; left: 0px;" {if $:node.is_hidden}class="node-name-hidden">{else}{if $:node.is_invisible}class="node-name-hiddenbyparent">{else}class="node-name-normal">{/if}{/if}{$:node.object.name|shorten( 23, '..' )|wash}</span>{if  ne($ui_context, "edit")}</a>{/if}

<input type="hidden" name="object_id_for_node_{$:node.node_id}" value="{$:node.object.id}" />
<input type="hidden" name="class_name_for_node_{$:node.node_id}" value="{$:node.class_name}" />
</li>
{/default}
{undef $toolTip $visibility $classes}