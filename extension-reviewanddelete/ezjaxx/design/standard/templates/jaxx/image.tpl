{def $sort_column_value=cond( $sort_column|eq( 'published' ), $node.object.published,
                             $sort_column|eq( 'modified' ), $node.object.modified,
                             $sort_column|eq( 'name' ), $node.object.name,
                             $sort_column|eq( 'priority' ), $node.priority,
                             $sort_column|eq( 'modified_subnode' ), $node.modified_subnode,
                             false() )
     $previous_image=fetch_alias( subtree, hash( parent_node_id, $node.parent_node_id,
                                                class_filter_type, include,
                                                class_filter_array, array( 'image' ),
                                                limit, 1,
                                                attribute_filter, array( and, array( $sort_column, $sort_order|choose( '>', '<' ), $sort_column_value ) ),
                                                sort_by, array( $sort_column, $sort_order|not ) ) )
     $next_image=fetch_alias( subtree, hash( parent_node_id, $node.parent_node_id,
                                            class_filter_type, include,
                                            class_filter_array, array( 'image' ),
                                            limit, 1,
                                            attribute_filter, array( and, array( $sort_column, $sort_order|choose( '<', '>' ), $sort_column_value ) ),
                                            sort_by, array( $sort_column, $sort_order ) ) ) }

<h1>{$node.name|wash}</h1>

{if is_unset( $versionview_mode )}
<div class="content-navigator">
    {if $previous_image}
        <div class="content-navigator-previous">
            <div class="content-navigator-arrow">&laquo;&nbsp;</div><a href="javascript:ezjaxxImageLoad({$previous_image[0].node_id})" title="{$previous_image[0].name|wash}">{'Previous image'|i18n( 'design/base' )}</a>
        </div>
        <div class="content-navigator-separator">|</div>
    {else}
        <div class="content-navigator-previous-disabled">
            <div class="content-navigator-arrow">&laquo;&nbsp;</div>{'Previous image'|i18n( 'design/base' )}
        </div>
        <div class="content-navigator-separator-disabled">|</div>
    {/if}

        <div class="content-navigator-forum-link"><a href={$node.parent.url_alias|ezurl}>{$node.parent.name|wash}</a></div>


    {if $next_image}
    	<div class="content-navigator-separator">|</div>
        <div class="content-navigator-next">
            <a href="javascript:ezjaxxImageLoad({$next_image[0].node_id})" title="{$next_image[0].name|wash}">{'Next image'|i18n( 'design/base' )}</a><div class="content-navigator-arrow">&nbsp;&raquo;</div>
        </div>
    {else}
        <div class="content-navigator-separator-disabled">|</div>
        <div class="content-navigator-next-disabled">
            {'Next image'|i18n( 'design/base' )}<div class="content-navigator-arrow">&nbsp;&raquo;</div>
        </div>
    {/if}
</div>
{/if}

<div class="attribute-image">
    <p>{attribute_view_gui attribute=$node.data_map.image image_class=imagelarge}</p>
</div>

<div class="attribute-caption">
    {attribute_view_gui attribute=$node.data_map.caption}
</div>