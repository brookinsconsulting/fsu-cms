/* jaxx : javascript asynchronous xml & xslt v0.7
	Legacy code: make jaxx work on older(ie 5.0) browsers
*/


if (!Array.push) Array.prototype.push = function() {//js 1.3 &&  ECMA-262 Edition 3
	for (var i = 0; i < arguments.length; i++) this[this.length] = arguments[i];
	return this.length;
};

if (!Array.shift) Array.prototype.shift = function() {//js 1.2 && ECMA-262 Edition 3
	var r = this[0];
	if (this.length) {
		var a = this.slice(1), i = a.length;
		while (i--) this[i] = a[i];
		this.length--;
	}
	return r;
};

if (!Array.unshift) Array.prototype.unshift = function() {//js 1.2 && ECMA-262 Edition 3
	var a = A.concat.call(A.slice.apply(arguments, [0]), this), i = a.length;
	while (i--) this[i] = a[i];
	return this.length;
};

if ( !Array.pop )Array.prototype.pop = function(){//js 1.2 && ECMA-262 Edition 3
	if ( this.length > 0 ){
		var last = this[this.length - 1 ];
		this.length--;
	}
	return last || null;
};

if ( !Array.splice ) Array.prototype.splice = function(si, c){//js 1.2 && ECMA-262 Edition 3 : ONLY REMOVES !!
	for ( var i = si; i < this.length - c; i++ )
		this[i] = this[i + c];
	this.length = this.length - c;
};


if (!Function.apply) Function.prototype.apply = function(o, a) {//js 1.3 & ECMA-262 Edition 3
	var $ = "__apply__", r;
	if (o == null) o = window;
	o[$] = this;
	switch (a.length) { // unroll for speed
		case 0: r = o[$](); break;
		case 1: r = o[$](a[0]); break;
		case 2: r = o[$](a[0], a[1]); break;
		case 3: r = o[$](a[0], a[1], a[2]); break;
		case 4: r = o[$](a[0], a[1], a[2], a[3]); break;
		default:
			var aa = [], i = a.length - 1;
			do aa[i] = "a[" + i + "]"; while (i--);
			eval("r=o[$](" + aa + ")");
	}
	try delete o[$];
	catch (e) {o[$] = null};
	return r;
};
	
if (!Function.call) Function.prototype.call = function(o) {//js 1.3 & ECMA-262 Edition 3
	return this.apply(o, A.slice.apply(arguments, [1]));
};