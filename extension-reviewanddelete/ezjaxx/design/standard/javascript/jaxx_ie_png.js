/*
jaxx : javascript asynchronous xml & xslt v0.6 alpha (as in not feature complete)
Inspired/based on the work of others:	dean.edwards.name 'IE7'
*/

var pngTest = new RegExp(".png$", "i");

//todo: do not applay in ms ie7 and higher
if (window.attachEvent && !window.opera) $addEvent(window, "load", function(){
	$$(['img','input[type=image]'], document.body).forEach(function(el){
		if (pngTest.test(el.src)) {
			var w = el.width, h = el.height;
			el.runtimeStyle.filter = "progid:DXImageTransform.Microsoft.AlphaImageLoader(src='" + el.src + "', sizingMethod='scale')";
			el.src = "";
			el.height = h;
			el.width = w;
		}
	});
});