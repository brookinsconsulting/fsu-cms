/*
	jaxx : javascript asynchronous xml & xslt
	v0.6 alpha (as in not feature complete)
	
	Copyright (c) 2006 WebEffekt.no
	Licensed under the MIT License:
	http://www.opensource.org/licenses/mit-license.php

	Inspired/based on the work of:
	prototype.conio.net		simon.incutio.com		jquery.com
	moofx.mad4milk.net		dean.edwards.name          erik.eae.ne
	
This code are left out of the main library & legacy for size considerations.
*/

if (!String.stripTags) String.prototype.stripTags = function(){
	return this.replace(/<\/?[^>]+>/gi, '');
};

if (window.HTMLElement && !document.documentElement.innerText){ // adds innerText to Gecko (Moz / firefox)
	HTMLElement.prototype.__defineGetter__("innerText", function(){ return(this.textContent);});
	HTMLElement.prototype.__defineSetter__("innerText", function(txt){ this.textContent = txt;});
	//todo: newline is not threated as in nativ version
}

if (!Array.some) Array.prototype.some = function(cB, tO){
	//Use indexOf instead, more informativ return value
	for (var i=0;i<this.length;i++) if (cB.call(tO,this[i],i,this)) return true;
	return false;
};
if (!Array.containsValue) Array.prototype.containsValue = function(valueToCheck){
	//Use indexOf instead, more informativ return value
	for (var i=0;i<this.length;i++) {if (this[i] == valueToCheck) return true;}
	return false;
};

if (!Array.lastIndexOf) Array.prototype.lastIndexOf = function(sE, sI){
	var x = -1; //todo: for faster excution on large arrays, this should loop backwards
	for (var i = sI || 0;i<this.length;i++) if (this[i] === sE) x = i;
	return x;
};

if (!Array.every) Array.prototype.every = function(cB, tO){
	for (var i=0;i<this.length;i++) if (!cB.call(tO,this[i],i,this)) return false;
	return true;
};

if (!Array.flatten) Array.prototype.flatten = function(){
	//flatten a multidimmensional array
	for(var i=0,r=[],l=this.length;i<l;i++)
	 (this[i].constructor === Array)? r = r.concat(this[i].flatten()): r.push(this[i]);
	return r;
};


function $objectToArray(o){
	//returns a array filled with an objects properties
	var arr = []
	for (prop in o || {}) arr.push(o[prop]);
	return arr;
}

	
/* Some varius code:
http://erik.eae.net/playground/arrayextras/arrayextras.js

function object(o){// from javascript.crockford.com
	function f() {}
	f.prototype = o;
	return new f();
}

window.onerror = function(e, s, x) {
	alert(e + "\n" + x + "\n" + this);
	return true;
}; 
	  
//for mozilla:
var string = (new XMLSerializer()).serializeToString(xmlobject);
var xmlobject = (new DOMParser()).parseFromString(xmlstring, "text/xml");


function wheel(event){
        var delta = 0;
        if (!event) event = window.event;
        if (event.wheelDelta) { 
                delta = event.wheelDelta/120;
                if (window.opera) delta = -delta;
        } else if (event.detail) {

                delta = -event.detail/3;
        }
         // If delta is nonzero, handle it.
         // Basically, delta is now positive if wheel was scrolled up,
         // and negative, if wheel was scrolled down
        if (delta)
                handle(delta);
}
if (window.addEventListener) window.addEventListener('DOMMouseScroll', wheel, false);
else window.onmousewheel = document.onmousewheel = wheel;



var $eventChain = function(){};//The whole idea here is to have a set of events / fx's to fire in a timeline manner.
$eventChain.prototype = {
arr: [],
index: -1,
load: function(arr){
	this.arr = arr;
	return this;
},

start: function(){
	this.arr.forEach(function(el,i){
		if (el.time && i != 0) setTimeout(el.func, el.time);
		if (el.onComplete && !el.relTime && el.func.o)el.func.o.onComplete = el.onComplete;
	});
	this.arr[0].func();
	this.index = 0;
},

call: function(i){
	this.index = (i) ? i : this.index +1;
	if (this.arr.length > this.index){
		if (this.arr[i].relTime) setTimeout(this.arr[i].onComplete, this.arr[i].relTime);
		this.arr[i].func();
	} else this.index = -1;
}
};


function StartSound(soundobj) {//Play embed obcets
	soundobj=$(soundobj)[0];
	if (soundobj.start_at_beginning) soundobj.start_at_beginning();
	else if (soundobj.Play)soundobj.Play();
	else if (soundobj.DoPlay) soundobj.DoPlay();
}

function StopSound(soundobj) {//Stop embed obcets
	soundobj=$(soundobj)[0];
	if (soundobj.Stop)soundobj.Stop();
	else if (soundobj.DoStop) soundobj.DoStop();
}

*/

