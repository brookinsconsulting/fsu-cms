<?php

$Module = array( "name" => "eZaxx Module and Views" );

$ViewList = array();
$ViewList["expand"] = array(
    "script" => "expand.php",
    'params' => array( 'UIcontext', 'CurrentID', 'NodeID', 'CustomCallBack' )
    );
    
$ViewList["expand2"] = array(
    "script" => "expand2.php",
    'params' => array( 'UIcontext', 'CurrentID', 'NodeID', 'CustomCallBack' )
    );
    
$ViewList["search"] = array(
    "script" => "search.php",
    'params' => array( 'UIcontext', 'Offset' )
    );
    
$ViewList["keyword"] = array(
    "script" => "keyword.php",
    'params' => array( 'ClassID' )
    );

$ViewList["preferences"] = array(
    "script" => "preferences.php",
    'params' => array( 'Function', 'Key', 'Value' )
    );

$ViewList["copyobject"] = array(
    "script" => "copyobject.php",
    'params' => array( 'ObjectID' )
    );
    
$ViewList["roleedit"] = array(
    "script" => "roleedit.php",
    'params' => array( 'moduleName' )
    );
    
$ViewList["image"] = array(
    "script" => "image.php",
    'params' => array( 'SortColumn', 'SortOrder', 'NodeID' )
    );
    

?>
