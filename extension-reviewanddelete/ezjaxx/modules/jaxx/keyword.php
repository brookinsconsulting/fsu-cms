<?php
//
// Created on: <10-Oct-2006 00:00:00 ar>
//
// ## BEGIN COPYRIGHT, LICENSE AND WARRANTY NOTICE ##
// SOFTWARE NAME: eZjaxx Ajax extension for eZ Publish
// SOFTWARE RELEASE: 0.9
// COPYRIGHT NOTICE: Copyright (C) 2006-* eZ systems AS
// SOFTWARE LICENSE: GNU General Public License v2.0
// NOTICE: >
//   This program is free software; you can redistribute it and/or
//   modify it under the terms of version 2.0  of the GNU General
//   Public License as published by the Free Software Foundation.
// 
//   This program is distributed in the hope that it will be useful,
//   but WITHOUT ANY WARRANTY; without even the implied warranty of
//   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//   GNU General Public License for more details.
// 
//   You should have received a copy of version 2.0 of the GNU General
//   Public License along with this program; if not, write to the Free
//   Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
//   MA 02110-1301, USA.
// 
// 
// ## END COPYRIGHT, LICENSE AND WARRANTY NOTICE ##
//
// ## BEGIN ABOUT TEXT, SPECS AND DESCRIPTION  ##
//
// MODULE:		JAXX
// VIEW:		KEYWORD
// PARAMS:		'ClassID'
// POST:		Keyword
// OUTPUT:		xhtml
// TEMPLATE(S):	none
// DESCRIPTION: Script for outputting a list of similar keywords, using settings from jaxx.ini
//				Returns http header status 204 'No Content' if turned of or no keywords.
//
// ## END ABOUT TEXT, SPECS AND DESCRIPTION  ##
//


include_once( 'lib/ezutils/classes/ezfunctionhandler.php' );
include_once( 'lib/ezutils/classes/ezini.php' );

$jaxxINI =& eZINI::instance( 'jaxx.ini' );
$http =& eZHTTPTool::instance();

$KeywordClassLimit = $jaxxINI->variable( "JaxxSettings", "KeywordClassLimit");
$KeywordLimit = $jaxxINI->variable( "JaxxSettings", "KeywordLimit"); 

if ($KeywordLimit == "0")
{
    //Keyword suggestion off
    header("HTTP/1.0 204 No Content");
    eZExecution::cleanExit();
}
else $KeywordLimit = (int) $KeywordLimit;

$classID=false;


$keywordStr = $http->postVariable( 'Keyword' );


if ( isset( $Params['ClassID'] ) && $Params['ClassID'] != "")
{
   $classID = $Params['ClassID'];

}

if (!$keywordStr || $keywordStr == "")
{
	header("HTTP/1.0 204 No Content");
    echo "Empty Keyword string!";
    eZExecution::cleanExit();
}
else
{
	$searchParamArray=array(  'alphabet' => $keywordStr, 
	                          'limit'  => $KeywordLimit
                            );
	
	if($KeywordClassLimit == 'true' && $classID != false)
		$searchParamArray['classid'] = $classID;
	
    $searchList = eZFunctionHandler::execute( 'content', 'keyword', $searchParamArray );
}

if (!$searchList) {
    //No Keywords found uses 204, so empty hits don't trigger client onLoad
    header("HTTP/1.0 204 No Content");
    eZExecution::cleanExit();
}

$r = '<ul>';

foreach ($searchList as $node){
	    $r .= '<li onmouseover="ezInputKeywordMouse(this)" onmousedown="ezInputKeywordEnter(ezInputKeyword[ezKeywordIndex])">';
	    $r .= $node["keyword"];
	    $r .= '</li>';
}

echo $r.'</ul>';
eZExecution::cleanExit();

?>