<?php
//
// Created on: <10-Nov-2006 00:00:00 ar>
//
// ## BEGIN COPYRIGHT, LICENSE AND WARRANTY NOTICE ##
// SOFTWARE NAME: eZjaxx Ajax extension for eZ Publish
// SOFTWARE RELEASE: 0.9
// COPYRIGHT NOTICE: Copyright (C) 2006-* eZ systems AS
// SOFTWARE LICENSE: GNU General Public License v2.0
// NOTICE: >
//   This program is free software; you can redistribute it and/or
//   modify it under the terms of version 2.0  of the GNU General
//   Public License as published by the Free Software Foundation.
// 
//   This program is distributed in the hope that it will be useful,
//   but WITHOUT ANY WARRANTY; without even the implied warranty of
//   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//   GNU General Public License for more details.
// 
//   You should have received a copy of version 2.0 of the GNU General
//   Public License along with this program; if not, write to the Free
//   Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
//   MA 02110-1301, USA.
// 
// 
// ## END COPYRIGHT, LICENSE AND WARRANTY NOTICE ##
//
// ## BEGIN ABOUT TEXT, SPECS AND DESCRIPTION  ##
//
// MODULE:		JAXX
// VIEW:		ROLEEDIT
// PARAMS:		'moduleName'
// OUTPUT:		xhtml
// TEMPLATE(S):	none
// DESCRIPTION: Script for getting option list of functions pr moduleName
//
// ## END ABOUT TEXT, SPECS AND DESCRIPTION  ##
//


if ( isset( $Params['moduleName'] ))
{
   $moduleName = $Params['moduleName'];

}

include_once( 'lib/ezutils/classes/ezmodule.php' );
$mod = & eZModule::exists( $moduleName );

if ( $mod )
{
    $functions =& $mod->attribute( 'available_functions' );
    $functionNames = array_keys( $functions );
}
else
{
    echo "";
    eZExecution::cleanExit();
}



foreach( $functionNames as $functionName )
{
    echo "<option value='" . $functionName . "'>" . $functionName . "</option>\n";
}


eZExecution::cleanExit();


?>