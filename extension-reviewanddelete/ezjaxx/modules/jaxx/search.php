<?php
//
// Created on: <15-Oct-2006 00:00:00 ar>
//
// ## BEGIN COPYRIGHT, LICENSE AND WARRANTY NOTICE ##
// SOFTWARE NAME: eZjaxx Ajax extension for eZ Publish
// SOFTWARE RELEASE: 0.9
// COPYRIGHT NOTICE: Copyright (C) 2006-* eZ systems AS
// SOFTWARE LICENSE: GNU General Public License v2.0
// NOTICE: >
//   This program is free software; you can redistribute it and/or
//   modify it under the terms of version 2.0  of the GNU General
//   Public License as published by the Free Software Foundation.
// 
//   This program is distributed in the hope that it will be useful,
//   but WITHOUT ANY WARRANTY; without even the implied warranty of
//   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//   GNU General Public License for more details.
// 
//   You should have received a copy of version 2.0 of the GNU General
//   Public License along with this program; if not, write to the Free
//   Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
//   MA 02110-1301, USA.
// 
// 
// ## END COPYRIGHT, LICENSE AND WARRANTY NOTICE ##
//
// ## BEGIN ABOUT TEXT, SPECS AND DESCRIPTION  ##
//
// MODULE:		JAXX
// VIEW:		SEARCH
// PARAMS:		'UIcontext', 'Offset'
// POST:		SearchStr 
// OUTPUT:		xhtml
// TEMPLATE(S):	standard/templates/jaxx/search.tpl
// DESCRIPTION: Script for getting list of search result to Sidebar Search
//				Fetches 6 nodes, but only displays 5 of them. 
//				The spare node is to determin if there are more nodes.
//
// ## END ABOUT TEXT, SPECS AND DESCRIPTION  ##
//


include_once( 'lib/ezutils/classes/ezfunctionhandler.php' );
include_once( 'kernel/common/template.php' );
include_once( 'lib/ezutils/classes/ezini.php' );

$jaxxINI =& eZINI::instance( 'jaxx.ini' );
$http =& eZHTTPTool::instance();

$SearchClassIdArray = $jaxxINI->variable( "JaxxSettings", "SearchClassIdArray");
$SearchAttributeIdArray = $jaxxINI->variable( "JaxxSettings", "SearchAttributeIdArray");


$searchStr = $http->postVariable( 'SearchStr' );
$searchOffset = 0;

if ( isset( $Params['Offset'] ) && is_numeric($Params['Offset']))
{
   $searchOffset = (int) $Params['Offset'];

}


if (!$searchStr || $searchStr == "") {
    echo "Nothing found!";
    eZExecution::cleanExit();
}
else
{
	$searchParamArray=array(    'text' => $searchStr, 
	                            'limit'  => 6, 
	                            'offset' => $searchOffset
	                        );
	
	if(count($SearchClassIdArray) > 0)
		$searchParamArray['class_id'] = $SearchClassIdArray;
	if(count($SearchAttributeIdArray) > 0)
		$searchParamArray['class_attribute_id'] = $SearchAttributeIdArray;
	
    $searchList = eZFunctionHandler::execute( 'content', 'search', $searchParamArray );
}

if (!$searchList  || count($searchList["SearchResult"]) == 0) {
    echo "Empty Search result!";
    eZExecution::cleanExit();
}

$r = '';
$tpl =& templateInit();

$menuINI =& eZINI::instance( 'contentstructuremenu.ini' );

$classIconsSize = $menuINI->variable( 'TreeMenu', 'ClassIconsSize' );
$tpl->setVariable( 'classIconsSize', $classIconsSize );

if ( isset( $Params['UIcontext'] ) )
{
     $UIcontext = $Params['UIcontext'];
     $tpl->setVariable( 'ui_context', $UIcontext );
}


foreach ($searchList["SearchResult"] as $key => $node){
	
	if ($key != 5){
        $tpl->setVariable( 'node', $node );
	    $r .= $tpl->fetch( 'design:jaxx/search.tpl' );
	}
}

$r .= "<li style='position:relative;'>";
if ($searchOffset != 0)
	$r .= "<a style='cursor:pointer;text-decoration:none' onclick='SearchAjaxLoad(". ($searchOffset-5) . ");'>&lt;&lt; Back</a> ";
if (count($searchList["SearchResult"]) == 6)
	$r .= " <a style='cursor: pointer;margin: 0; padding: 0;position: absolute;right: 7px;top: 0px; z-index: 8; text-decoration: none' onclick='SearchAjaxLoad(". ($searchOffset+5) . ");'>Next &gt;&gt;</a>";
$r .= "</li>"; 
echo $r;

eZExecution::cleanExit();

?>