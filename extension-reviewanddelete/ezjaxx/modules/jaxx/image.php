<?php
//
// Created on: <10-Nov-2006 00:00:00 ar>
//
// ## BEGIN COPYRIGHT, LICENSE AND WARRANTY NOTICE ##
// SOFTWARE NAME: eZjaxx Ajax extension for eZ Publish
// SOFTWARE RELEASE: 0.9
// COPYRIGHT NOTICE: Copyright (C) 2006-* eZ systems AS
// SOFTWARE LICENSE: GNU General Public License v2.0
// NOTICE: >
//   This program is free software; you can redistribute it and/or
//   modify it under the terms of version 2.0  of the GNU General
//   Public License as published by the Free Software Foundation.
// 
//   This program is distributed in the hope that it will be useful,
//   but WITHOUT ANY WARRANTY; without even the implied warranty of
//   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//   GNU General Public License for more details.
// 
//   You should have received a copy of version 2.0 of the GNU General
//   Public License along with this program; if not, write to the Free
//   Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
//   MA 02110-1301, USA.
// 
// 
// ## END COPYRIGHT, LICENSE AND WARRANTY NOTICE ##
//
// ## BEGIN ABOUT TEXT, SPECS AND DESCRIPTION  ##
//
// MODULE:		JAXX
// VIEW:		IMAGE
// PARAMS:		'SortColumn', 'SortOrder', 'NodeID'
// OUTPUT:		xhtml
// TEMPLATE(S):	standard/templates/jaxx/image.tpl
// DESCRIPTION: Script for outputting node template part of image in gallery rendering.
//				This should be converted to json output, and not use template.
//
// ## END ABOUT TEXT, SPECS AND DESCRIPTION  ##
//


include_once( 'kernel/classes/ezcontentobjecttreenode.php' );


$nodeID=false;
$sortOrder=0;
$sortColumn="published";

if ( isset( $Params['NodeID'] ) && is_numeric($Params['NodeID']))
{
   $nodeID = (int) $Params['NodeID'];

}

if (!$nodeID) {
    echo "No NodeID!";
    eZExecution::cleanExit();
}

if ( isset( $Params['SortOrder'] ) && is_numeric($Params['SortOrder']))
{
   $sortOrder = (int) $Params['SortOrder'];

}

if ( isset( $Params['SortColumn'] ) )
{
   $sortColumn = $Params['SortColumn'];

}

$node =& eZContentObjectTreeNode::fetch( $nodeID );


if (!$node) {
    echo "No Node!";
    eZExecution::cleanExit();
}

/*
// {$node.object.data_map}
$object =& $node->object();
$dataMap =& $object->dataMap();
 
// {$node.object.data_map.short_description.content.output.output_text}
$shortDescription =& $dataMap['short_description']->content();
$shortDescriptionOutput =& $shortDescription->attribute('output');
$shortDescriptionOutputText = $shortDescriptionOutput->attribute('output_text');
 */

eZExecution::cleanExit();

?>