<?php
//
// Created on: <1-Oct-2006 00:00:00 ar>
//
// ## BEGIN COPYRIGHT, LICENSE AND WARRANTY NOTICE ##
// SOFTWARE NAME: eZjaxx Ajax extension for eZ Publish
// SOFTWARE RELEASE: 0.9
// COPYRIGHT NOTICE: Copyright (C) 2006-* eZ systems AS
// SOFTWARE LICENSE: GNU General Public License v2.0
// NOTICE: >
//   This program is free software; you can redistribute it and/or
//   modify it under the terms of version 2.0  of the GNU General
//   Public License as published by the Free Software Foundation.
// 
//   This program is distributed in the hope that it will be useful,
//   but WITHOUT ANY WARRANTY; without even the implied warranty of
//   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//   GNU General Public License for more details.
// 
//   You should have received a copy of version 2.0 of the GNU General
//   Public License along with this program; if not, write to the Free
//   Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
//   MA 02110-1301, USA.
// 
// 
// ## END COPYRIGHT, LICENSE AND WARRANTY NOTICE ##
//
// ## BEGIN ABOUT TEXT, SPECS AND DESCRIPTION  ##
//
// MODULE:		JAXX
// VIEW:		EXPAND
// PARAMS:		'UIcontext', 'CurrentID', 'NodeID'
// OUTPUT:		xHtml
// TEMPLATE(S):	admin/templates/contentstructuremenu/dyn_show_content_structure.tpl
// DESCRIPTION: Script for getting parts of the content structure menu ondemand,
// 				is requested from javascript XmlhttpRequest in client browser.
//				Inspired by ezodcsm (eZ ondemand content structure menu)
//
// ## END ABOUT TEXT, SPECS AND DESCRIPTION  ##
//


include_once( 'lib/ezutils/classes/ezfunctionhandler.php' );
include_once( 'kernel/common/template.php' );

$nodeID=false;

if ( isset( $Params['NodeID'] ) && is_numeric($Params['NodeID']))
{
   $nodeID = (int) $Params['NodeID'];

}


if (!$nodeID) {
    echo "No NodeID!";
    eZExecution::cleanExit();
}

$menuINI     =& eZINI::instance( 'contentstructuremenu.ini' );
$showClasses = $menuINI->variable( 'TreeMenu', 'ShowClasses' );
$maxNodes    = $menuINI->variable( 'TreeMenu', 'MaxNodes' );
$node        = eZFunctionHandler::execute( 'content', 'node', 
                array( 'node_id' => $nodeID
                        ) );

if (!$node) {
    echo "No Node!";
    eZExecution::cleanExit();
}


$sort_array = $node->attribute( 'sort_array' );
$nodeList   = eZFunctionHandler::execute( 'content', 'list', 
                array( 'parent_node_id' => $nodeID, 
                        'class_filter_type'  => 'include', 
                        'class_filter_array' => $showClasses,
                        'limit' => $maxNodes,
                        'sort_by' => $sort_array[0]
                    ) );


if (!$nodeList) {
    echo "No Nodes!";
    eZExecution::cleanExit();
}

$tpl =& templateInit();
$UIcontext = "navigation";

if ( isset( $Params['UIcontext'] ) )
{
     $UIcontext = $Params['UIcontext'];
}

$tpl->setVariable( 'ui_context', $UIcontext );
$tpl->setVariable( 'ShowClasses', $showClasses );
$tpl->setVariable( 'currentNodeList', $nodeList );
$tpl->setVariable( 'last_key', count($nodeList) -1 );
$tpl->setVariable( 'currentNodeID', (int) $Params['CurrentID'] );
$tpl->setVariable( 'classIconsSize', $menuINI->variable( 'TreeMenu', 'ClassIconsSize' ) );

echo $tpl->fetch( 'design:contentstructuremenu/dyn_show_content_structure.tpl' );

eZExecution::cleanExit();

?>