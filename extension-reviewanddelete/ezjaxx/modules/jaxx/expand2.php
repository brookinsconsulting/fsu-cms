<?php
//
// Created on: <1-Des-2006 00:00:00 ar>
//
// ## BEGIN COPYRIGHT, LICENSE AND WARRANTY NOTICE ##
// SOFTWARE NAME: eZjaxx Ajax extension for eZ Publish
// SOFTWARE RELEASE: 0.9
// COPYRIGHT NOTICE: Copyright (C) 2006-* eZ systems AS
// SOFTWARE LICENSE: GNU General Public License v2.0
// NOTICE: >
//   This program is free software; you can redistribute it and/or
//   modify it under the terms of version 2.0  of the GNU General
//   Public License as published by the Free Software Foundation.
// 
//   This program is distributed in the hope that it will be useful,
//   but WITHOUT ANY WARRANTY; without even the implied warranty of
//   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//   GNU General Public License for more details.
// 
//   You should have received a copy of version 2.0 of the GNU General
//   Public License along with this program; if not, write to the Free
//   Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
//   MA 02110-1301, USA.
// 
// 
// ## END COPYRIGHT, LICENSE AND WARRANTY NOTICE ##
//
// ## BEGIN ABOUT TEXT, SPECS AND DESCRIPTION  ##
//
// MODULE:		JAXX
// VIEW:		EXPAND2
// PARAMS:		'UIcontext', 'CurrentID', 'NodeID'
// OUTPUT:		xHtml
// TEMPLATE(S):	admin/templates/contentstructuremenu/dyn_show_content_structure.tpl
// DESCRIPTION: This is an experimental work in progress version of expand.php witch
//              aims to speed it up by at removing the need for using the template
//              system and also does the needed feching directly against the db.
//
// ## END ABOUT TEXT, SPECS AND DESCRIPTION  ##
//


include_once( "kernel/classes/ezcontentobject.php" );
//include_once( 'kernel/classes/ezcontentobjecttreenode.php' );

  function fetchNodeSort($node_id)
  {
    $db =& eZDB::instance();
	$query = "SELECT     sort_field,
                         sort_order
                  FROM ezcontentobject_tree
                  WHERE node_id=".$node_id;

	$node_array = $db->arrayQuery( $query );
	return $node_array;
  }
  
function fetchNodeList($parent_node_id, $parent_sort_array, $node_classes, $max_nodes )
  {
	//based on code from Paul Forsyth @ VisionWT
	$node_information = array();

    $db =& eZDB::instance();
	$query = "SELECT contentobject_id,
                         contentobject_version,
                         sort_order,
                         is_hidden,
                         is_invisible,
                         sort_field,
                         path_identification_string
                  FROM ezcontentobject_tree
                  WHERE node_id=".$parent_node_id;

	$node_array = $db->arrayQuery( $query );

	$node_information['node_sort_field'] = $node_array[0]['sort_field'];
	$node_information['node_sort_order'] = $node_array[0]['sort_order'];
	$node_information['node_is_hidden'] = $node_array[0]['is_hidden'];
	$node_information['node_is_invisible'] = $node_array[0]['is_invisible'];
	$node_information['node_path_identification_string'] = $node_array[0]['path_identification_string'];
	$node_information['node_id'] = $parent_node_id;
	$node_information['object_id'] = $node_array[0]['contentobject_id'];
	$node_information['object_version'] = $node_array[0]['contentobject_version'];

	$query = "SELECT name FROM ezcontentobject_name 
                  WHERE contentobject_id=" .  $node_information['object_id'] . "
	          AND content_version=" . $node_information['object_version'];
	$object_name_array = $db->arrayQuery( $query );

	$node_information['object_name'] = $object_name_array[0]['name'];

	$query = "SELECT ezcontentclass.id as id,
                         ezcontentclass.identifier as identifier,
                         ezcontentclass.is_container as is_container,
                         ezcontentclass.name as name
                  FROM ezcontentclass, ezcontentobject
                  WHERE ezcontentobject.id=".$node_information['object_id']."
                  AND ezcontentobject.contentclass_id = ezcontentclass.id";

	$class_array = $db->arrayQuery( $query );
	$node_information['class_id'] = $class_array[0]['id'];
	$node_information['class_identifier'] = $class_array[0]['identifier'];
	$node_information['class_is_container'] = $class_array[0]['is_container'];
	$node_information['class_name'] = $class_array[0]['name'];

	return $node_information;
  }





$nodeID=false;

if ( isset( $Params['NodeID'] ) && is_numeric($Params['NodeID']))
{
   $nodeID = (int) $Params['NodeID'];
}


if (!$nodeID) {
    echo "No NodeID!";
    eZExecution::cleanExit();
}

$currentNodeID = (int) $Params['CurrentID'];
$menuINI       =& eZINI::instance( 'contentstructuremenu.ini' );
$showClasses   = $menuINI->variable( 'TreeMenu', 'ShowClasses' );
$maxNodes      = $menuINI->variable( 'TreeMenu', 'MaxNodes' );
$node = fetchNodeSort($nodeID);

if (!$node) {
    echo "No Node!";
    eZExecution::cleanExit();
}

$sort_array = array($node[0]["sort_field"], $node[0]["sort_order"]);

var_dump($sort_array);



eZExecution::cleanExit();
 
 
 
/* 
$node          =& eZContentObjectTreeNode::fetch( $nodeID );



$sort_array = $node->attribute( 'sort_array' );
$nodeList =& eZContentObjectTreeNode::subTree(  array( 'Depth' => 1,
        'Limit'            => $maxNodes,
        'Offset'           => 0,
        'SortBy'           => $sort_array[0],
		'DepthOperator'    => 'eq',
        'ClassFilterType'  => 'include',
        'ClassFilterArray' => $showClasses
		), $nodeID );


if (!$nodeList) {
    echo "No Nodes!";
    eZExecution::cleanExit();
}


$UIcontext = "navigation";
$nodeListCount = count($nodeList) -1;

if ( isset( $Params['UIcontext'] ) )
{
     $UIcontext = $Params['UIcontext'];
}

foreach ($nodeList as $key => $item){
	$itemObject =& $item->object();
	$itemNodeID = $item->attribute('node_id');
	$itemName = $item->attribute('name');
	$itemObjectID = $item->attribute('contentobject_id');
	$countChildren =& eZContentObjectTreeNode::subTreeCount(  array( 'Depth' => 1,
        'Limit'            => $maxNodes,
        'Offset'           => 0,
        'SortBy'           => $sort_array[0],
		'DepthOperator'    => 'eq',
        'ClassFilterType'  => 'include',
        'ClassFilterArray' => $showClasses
		), $itemNodeID );

	$liClass = "";
	if ($key == $nodeListCount) $liClass = "lastli ";
	if ($itemNodeID == $currentNodeID) $liClass .= "currentnode";
	if ($liClass != "") $liClass = ' class="' . $liClass . '"';
	
	echo '<li id="n' . $itemNodeID . '"' . $liClass . '> ';
	if ($countChildren == 0) echo '<span class="openclose"><img alt="." border="0" src="" /></span>';
	else echo '<a class="openclose" href="/gallery_site_admin/galleries_is_my_friend_my_friend" title="Fold/Unfold" onclick="ezpopmenu_hideAll();"><img border="0" src="/design/admin/images/content_tree-open.gif" alt="Open sub menu" /></a>';

	if ($UIcontext == 'browse'){
	echo ' <a class="nodeicon" href="#"><img alt="." src="" border="0" height="16" width="16" /></a>';
	echo ' <a class="nodetext" href="/content/browse/' . $itemNodeID . '">';
	} else {
 	echo ' <a class="nodeicon" href="#" onclick="ezpopmenu_showTopLevel( event, ' . "'ContextMenu'" . ", ez_createAArray( new Array( '%nodeID%', " . $itemNodeID . ", '%objectID%', " . $itemObjectID . " ) ) , '" . $itemName . "', " . $itemNodeID . ");return false;" . '"><img alt="." src="" border="0" height="16" width="16" /></a>';
	echo ' <a class="nodetext" href="' . $item->attribute('url_alias') . '">';
	}
	echo $itemName. '</a>';
	echo '<ul class="subMenu" style="display:none;"><li class="lastli loading">Please wait...</li></ul>';
	echo '</li>';

	
}
*/

eZExecution::cleanExit();

?>