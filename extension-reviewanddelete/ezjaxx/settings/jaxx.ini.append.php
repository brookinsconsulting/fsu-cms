<?php /* #?ini charset="utf-8"?

[JaxxSettings]
# Class ID's to include in search, will search on all classes if left empty
SearchClassIdArray[]

# Attribute ID's to include in search, will search on all if left empty
SearchAttributeIdArray[]
SearchAttributeIdArray[]=1
SearchAttributeIdArray[]=4

# Sets the name of the search toolbar
SearchBarName=Sidebar search

# Only search for keywords whithin same class type
KeywordClassLimit=true

# Limit number of keywords to suggest
KeywordLimit=7

*/
?>