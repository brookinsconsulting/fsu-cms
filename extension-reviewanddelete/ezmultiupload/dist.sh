#!/bin/bash
EXTENSION_NAME="eZ Multiupload Extension"
EXTENSION_IDENTIFIER="ezmultiupload"
EXTENSION_SUMMARY="This extension enables users to upload multiple files at once via the front-end of an eZ Publish website."
EXTENSION_LICENSE="GNU General Public License v2.0"
EXTENSION_VERSION="1.0.0"
EXTENSION_PUBLISH_VERSION="4.0 and higher"
EXTENSION_ARCHIVE_NAME="ezmultiupload"
EXTENSION_PHP_VERSION="5.1"

# EXTENSION_FILTER_FILES=""
