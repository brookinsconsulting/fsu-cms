<?php /* #?ini charset="utf-8"?

[ExtensionSettings]
DesignExtensions[]=cyclerotation

[JavaScriptSettings]
FrontendJavaScriptList[]=framework/objects.js
FrontendJavaScriptList[]=framework/utils.js
FrontendJavaScriptList[]=jquery.cycle.all.min.js
FrontendJavaScriptList[]=jquery.cyclerotation.js

*/ ?>