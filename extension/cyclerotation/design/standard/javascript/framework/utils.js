var $utils=(function($){
	if(!$){return;}
	return{
		'attributes':function(o,eh){
			if(typeof(o)=='object'){
//alert('NamedNodeMap: '+this.is(o,'NamedNodeMap')+'\nObject Class: '+this.getClass(o));
				if(this.is(o,'NamedNodeMap')){// NOT IE Compatible
					var map={},ehre=/^on/i;
					for(var i=0;i<o.length;i++){
						if(eh || !eh && !ehre.test(o[i].nodeName)){
							map[o[i].nodeName]=this.typecast(o[i].nodeValue);
						}
					}
					return map;
				}
//alert('Is Object Not NamedNodeMap\n'+o);
//alert(o+'\nnodeType:'+o.nodeType+'\no.toString: '+o.toString()+'\nObject toString: '+Object.prototype.toString.call(o));
//alert('test\n'+(o==Object.prototype.toString.call(o))+'\no.match'+String(o).match(/^\[object\s(.*)\]$/));

				// convert to a DOM Node object and rerun as the attributes
				return this.attributes((o.nodeType===1?o:o[0]).attributes,eh);
			}
			return this.attributes($(o)[0].attributes,eh);
		},
		'getClass':function(o){
			var s=Object.prototype.toString.call(o).match(/^\[object\s(.*)\]$/);
//			if(s[1]=='Object' && o.nodeType===1){
//				return String(o).match(/^\[object\s(.*)\]$/)[1];
//			}
			return s[1];
		},
		'init':function(object,data,fn){
			var instance=$.extend(true,{},object);
			if(!fn){fn='init'}
			if(data && instance[fn]){
				instance[fn].apply(instance,data);
			}
			return instance;
		},
		'is':function(o,c){
			if(this.getClass(c)=='Array'){
				return $.inArray(this.getClass(o),c)>=0;
			}
			return this.getClass(o)==c;
		},
		'log':function(){
			if(window.console&&window.console.log&&window.console.log.apply){
				window.console.log.apply(this,arguments);
			}
		},
		'typecast':function(o){
			if((/(\d*)\.{0,1}(\d+)/).test(o)){
				return isNaN(o)?o:Number(o);
			}else if((/true|false/i).test(o)){
				return o=='true';
			}else if(o=='null' || o=='undefined'){
				return o=='null'?null:undefined;
			}
			return o;
		},
		'timeAgo':function(date1, date2, granularity){
			var self = this;

			periods = [];
			periods['week'] = 604800;
			periods['day'] = 86400;
			periods['hour'] = 3600;
			periods['minute'] = 60;
			periods['second'] = 1;

			if(!granularity){
				granularity = 5;
			}

			(typeof(date1) == 'string') ? date1 = new Date(date1).getTime() / 1000 : date1 = new Date().getTime() / 1000;
			(typeof(date2) == 'string') ? date2 = new Date(date2).getTime() / 1000 : date2 = new Date().getTime() / 1000;

			if(date1 > date2){
				difference = date1 - date2;
			}else{
				difference = date2 - date1;
			}

			output = '';

			for(var period in periods){
				var value = periods[period];

				if(difference >= value){
					time = Math.floor(difference / value);
					difference %= value;

					output = output +  time + ' ';

					if(time > 1){
						output = output + period + 's ';
					}else{
						output = output + period + ' ';
					}
				}

				granularity--;
				if(granularity == 0){
					break;
				}	
			}

			return output + ' ago';
		}
	}
})(typeof(jQuery)!=='undefined'?jQuery:null);