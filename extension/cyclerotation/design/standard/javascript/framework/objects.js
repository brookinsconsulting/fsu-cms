_function={
	'apply':function(as,arg){
		if(!this.arguments && !arg){
			return this.own.call(as?as:this.as);
		}
		if(this.arguments && arg){
			for(var i in arg){
				this.arguments.push(arg[i]);
			}
			return this.own.apply(as?as:this.as,this.arguments);
		}
		return this.own.apply(as?as:this.as,arg?arg:this.arguments);
	},
	'arguments':false,
	'as':false,
	'call':function(as){
		return this.own.call(as?as:this.as);
	},
	'init':function(f,as,args){
		this.own=f;
		this.as=as;
		this.arguments=args;
	},
	'own':false
}
_event={
	'add':function(f){
		this.queue[this.queue.length]=f;
	},
	'init':function(n){
		if(n){
			this.name=n;
		}
	},
	'name':false,
	'queue':[],
	'run':function(as,arg){
		$.each(this.queue,function(q,f){
			f.apply(as,arg);
		});
	}
}
