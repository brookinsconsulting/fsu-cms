<?php
$eZTemplateOperatorArray=array();
$eZTemplateOperatorArray[]=array(
	'script' => 'extension/sitelink/operators/sitelinkoperator.php',
	'class' => 'SiteLinkOperator',
	'operator_names' => array('sitelink', 'sitelink_path')
);

$eZTemplateOperatorArray[]=array(
	'script' => 'extension/sitelink/operators/pagedataoperator.php',
	'class' => 'PageDataOperator',
	'operator_names' => array('pagedata','pagedata_set','pagedata_merge')
);

$eZTemplateOperatorArray[]=array(
	'script' => 'extension/sitelink/operators/ezservervarsoperator.php',
	'class' => 'eZServerVarsOperator',
	'operator_names' => array('ezservervars', 'ezpostvars', 'ezgetvars')
);

$eZTemplateOperatorArray[]=array(
	'script' => 'extension/sitelink/operators/getfooteroperator.php',
	'class' => 'GetFooterOperator',
	'operator_names' => array('get_footer')
);

?>