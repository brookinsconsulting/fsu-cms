<?php /* #?ini charset="utf-8"?

# This is an example.
# See doc/readme.txt.php for usage.
[calendar]
name=The name of the calendar appears in navigation, as the page title, and on links from other calendars.
short_description=Short description appears when this item is displayed in line view. 
description=Description appears below the calendar on the full view of the calendar page
cal_color=Each calendar tags its events with a color so that when multiple calendars are displayed together the events from each calendar can be identified. 
master_cal=If a calendar is marked as 'Show Relations' it will ONLY show events that have been RELATED to it (whenever a new event is added, the option to related the event to all such calendars is made available). Otherwise, the calendar will show only events that have been position directly inside the calendar object in the content tree.

[folder]
name=The name appears in the page title, and at the top of full view of the page. 
short_name=The short name is used to identify the page in the navigation. 
short_description=Displays under the name in the line view.
description=This is the main part of this content, and is only displayed in its full view. 
show_children=When checked, the template will display all of the sub-items in line view, and this page will act as a directory of that content. 
keywords=Include words here that are not present in the content, but should trigger this page appearing in search results. 
ors_related=Related objects can be used to add images and files into the content of the page. 

[info_page]
short_name=The short name is used to identify the page in the navigation. 
name=The name appears in the page title, and at the top of full view of the page. 
short_description=Displays under the name in the line view.
description=This is the main part of this content, and is only displayed in its full view. 
keywords=Include words here that are not present in the content, but should trigger this page appearing in search results. 

[page_note]
name=The note name appears at the top of the page note box 
note=Note text appears in a note box on the right side of the page. Add images, links, lists and other formatting here. 

[article]
title=Please insert here the title of your article.
short_title=If you want insert here a short title. It's commonly used in the menu.
author=The list of authors.
intro=The following text it's commonly used in line view.
body=Place here your article text.
enable_comments=Check the following checkbox to enable comments on this article.
ors_related=To add help for related object use the key 'ors_related'.

[user]
user_account=Your username will be shown on your posts throughout the site. Please enter a valid email address to ensure you receive your account verification email. <b>The email address will not be shown anywhere on the site.</b> 
signature=Your signature will appear on forum posts
image=Optional - if added it will show on your profile page. 

[forum_topic]
message=Please note...any urls (starting with 'http://') and email addresses you type in the text of your message will be automatically turned into links.
image=Maximum image size is 1MB.
[forum_reply]
message=Please note...any urls (starting with 'http://') and email addresses you type in the text of your message will be automatically turned into links.
image=Maximum image size is 1MB.

[event]
name=The name appears on the calendar(s), in the page title, and at the top of full view of the page. 
location=Describe the place where the event will take place. Displays on the "hover" view of events on the calendar as well as the full view of the event. 
short_description=Displays under the name in the line view.
description=This is the main part of this content, and is only displayed in its full view. 
related_cals=Some calendars are set up for particular reader audiences (eg students)...if you related your event to any of these calendars (listed here) the event will show up on the calendars selected.
date_from=The date the event starts. 
time_from=The time the event starts. Leave blank for all day.
date_to=The date the event stops. Leave blank for all day. NB - for repeat events (see last input field on form) this should be the date of the last repetition.
time_to=The time the event stops. Leave blank for all day.
repeat_events=If your event repeats on a regular basis as is not more than one full day in length, you can manage the repetition using this field. NB - for repeat events, remember to set the Date To field to indicate the date of the last repetition.


[gallery]
name=The gallery name should be descriptive so that users browsing all of the uploaded galleries can decide if they want to view the images.
short_description=This will be shown in the main listing of galleries, and should be 1-3 sentences of introduction to your gallery. 
description=When users are viewing your gallery, they will also see this descriptive text. You can include a link to your blog or other galleries by using the Link tool (looks like a chain link). 
image=Choose an image (from your hard drive) that is representative of your gallery. It will show in the list of galleries. Once you have created your gallery, you will be able to add the rest of your images in it. 

[image]
name=The name of your image will be displayed in the gallery view, and when viewing your image. In gallery view, only the first 26 characters will be displayed. 
url=If you would like to provide a link to your website, other photo gallery, or blog, please include it here. The name your provide in the author field will be linked to the URL you provide. If the URL contains objectionable content, your posts and links may be removed by the site administrators. 

[comment]
subject=Provide a quick headline for your comment
author=Enter your name (or alias) and email address for attribution. Your email address will not be used for any purpose other than for a representative of Banff to contact you if there is a problem with your submission. Please see our <a href='/privcay'>Privacy Policy</a> for more details. 
url=If you would like to provide a link to your website, other photo gallery, or blog, please include it here. The name your provide in the author field will be linked to the URL you provide. If the URL contains objectionable content, your posts and links may be removed by the site administrators. 
message=Add the body of your comment here. If your comment includes objectionable, marketing-oriented, or off-topic content it will be removed by site administrators. 

[faq_list]
name=The name appears in the page title, and at the top of full view of the page. 
short_description=Displays under the name in the line view.
description=This is the main part of this content, and is only displayed in its full view, just above the list of FAQ Questions that are subitems of the List. 
footer=Displays below the FAQ Questions on the FAQ List full view. Use this to direct users what to do if their question is not answered. 

[faq_question]
name=The content of the question.
answer=The answer can include images, links, files, lists, and other formatting. 

[feedback_form]
name=The name appears in the navigation, page title, and at the top of full view of the page. 
description=This is the main part of this content, and is only displayed in its full view above the form fields. 
subject=
message=
email=
recipient=The contents of the form will be emailed to this address upon completion. It will also be stored in the database.

*/ ?>
