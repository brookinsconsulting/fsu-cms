<?php /*

Attribute Help Text


ABOUT

This extension allow you to add help text to the attribute of your classes.
It's a workaround for the bug:
http://ez.no/community/bugs/add_help_text_or_description_as_a_standard_field_for_datatypes

You have to edit a ini file and add your own help text.


INSTALL

Read the following documentation:
http://ez.no/doc/ez_publish/technical_manual/3_6/installation/extensions
and install this extension only for your admin site.

Tested only with eZ publish 3.7, but should work in 3.5 and 3.6 too.


CONFIG

Add your text help for your attributes in the settings/orsaht.ini.php file.
The file format is simple:

[<class_identifier>]
<attribute_identifier>=<attribute_help_text>

For related object use "ors_related" as <attribute_identifier>.

You can also edit the css file.


EXAMPLE

See the settings/orsaht.ini.php for the standard article class.


BUGS

No i18n.
Workaround: You can support multiple language changing the name of the
extension and some settings and enable it only in you localized site.

Poor style design.

Missing friendly admin interface.


MORE INFO

Nothing special: basically override templates are standard templates
with two lines to include the templates that read the ini file.


AUTHOR

Daniele Pizzolli <ez tovel it>


LICENSE

Gnu GPL 2
See LICENSE file.

*/ ?>
