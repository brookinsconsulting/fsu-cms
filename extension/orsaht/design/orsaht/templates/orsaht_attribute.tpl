{if ezini_hasvariable($class.identifier, $attributes.item.contentclass_attribute.identifier, 'orsaht.ini' )}
{let attributeHelpText=ezini( $class.identifier, $attributes.item.contentclass_attribute.identifier, 'orsaht.ini' )}
{section show=$attributeHelpText}
    <label><span class="ors-attribute-help-text">{$attributeHelpText}</span></label>
{/section}
{/let}
{/if}