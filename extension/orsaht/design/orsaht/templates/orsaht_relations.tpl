{if ezini_hasvariable($class.identifier, 'ors_related', 'orsaht.ini' )}
{let attributeHelpText=ezini( $class.identifier, 'ors_related', 'orsaht.ini' )}
{section show=$attributeHelpText}
    <label><span class="ors-relations-help-text">{$attributeHelpText}</span></label>
{/section}
{/let}
{/if}