{def $audioFile=$node.data_map.file
     $audioURL = concat('http://',ezsys('hostname'),'/content/download/',$audioFile.contentobject_id,'/',$audioFile.content.contentobject_attribute_id,'/file/',$audioFile.content.original_filename)}

<div class="content-view-line float-break">
   <div class="class-mp3-audio">

	<h2><span class="attribute-date">{$node.data_map.date.data_int|l10n(shortdate)}<span> - <a href="{$node.url_alias|ezroot(no)}">{$node.data_map.name.content|wash}</a></h2>
		
	<div class="attribute-short">
		{attribute_view_gui attribute=$node.data_map.summary}
	</div>
{*
	<div class="mp3-download">
		<a href="{$audioURL}" title="{$audioFile.content.original_filename} ({$audioFile.content.filesize|si(byte)})"><img alt="Download mp3" src="{'images/mp3-download.png'|ezdesign(no)}" width="144" height"24" /></a>
	</div>
*}
   </div>
</div>
