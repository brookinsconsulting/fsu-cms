{* XML Audio - Full View *}
{def $audioFile=$node.data_map.file}

<data-root>
	<mp3URL value="{concat("content/download/",$audioFile.contentobject_id,"/",$audioFile.content.contentobject_attribute_id,"/",$audioFile.content.original_filename)|ezroot('no')}" />
	<mp3Title value="{$node.data_map.name.data_text|wash()}" />
	<autoStart value="true" />
	<loopCount value="0" />
</data-root>