{* Podcast Audio - Full View *}
{'Test'|debug()}
{def $audioFile=$node.data_map.file
     $audioURL = concat('http://',ezsys('hostname'),'/content/download/',$audioFile.contentobject_id,'/',$audioFile.content.contentobject_attribute_id,'/',$audioFile.content.original_filename)
     $itunes = 'Subscribe to FSU Headlines podcast via iTunes. Just click<br /><a href="http://phobos.apple.com/WebObjects/MZStore.woa/wa/viewPodcast?s=143441&id=79537523"><img height="15" width="61" alt="University Communications Group - FSU Headlines - FSU Headlines" src="http://ax.phobos.apple.com.edgesuite.net/images/badgeitunes61x15dark.gif" /></a>'}

<item>
	<title>{$node.data_map.name.data_text|wash()}</title>
	<link>{$audioURL}</link>
	<description><![CDATA[{$node.data_map.description.content.output.output_text} {$itunes}]]></description>
	<pubDate>{$node.data_map.date.data_int|datetime('custom','%D, %d %M %Y %h:%i:%s %O')}</pubDate>
	<enclosure url="{$audioURL}" length="3362816" type="audio/x-mpeg"/>
	<guid isPermaLink="false"></guid>
	<itunes:author>Florida State University</itunes:author>
	<itunes:subtitle>Current news about Florida State University</itunes:subtitle>
	<itunes:summary>{$node.data_map.description.content.output.output_text}</itunes:summary>
	<itunes:explicit>no</itunes:explicit>
	<itunes:duration>3:30</itunes:duration>
</item>