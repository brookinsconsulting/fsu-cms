{def $audioFile=$node.data_map.file
     $audioURL = concat('http://',ezsys('hostname'),'/content/download/',$audioFile.contentobject_id,'/',$audioFile.content.contentobject_attribute_id,'/file/',$audioFile.content.original_filename)}

<div class="content-view-full">
   <div class="class-mp3-audio">

	<div class="attribute-title">
		<div class="mp3-download"><a href="{$audioURL}" title="{$audioFile.content.original_filename} ({$audioFile.content.filesize|si(byte)})"><img alt="Download mp3" src="{'images/mp3-download.png'|ezdesign(no)}" width="144" height"24" /></a></div>
		<h1>{$node.name|wash()}</h1>
	</div>

	<div class="attribute-long">
		<span class="attribute-date">{$node.data_map.date.data_int|l10n(shortdate)}<span>
		{attribute_view_gui attribute=$node.data_map.description}
	</div>

	<div class="content-media">
	<div id="flashcontent"></div>
{run-once}<script type="text/javascript" src="{'javascript/swfobject.js'|ezdesign('no')}"></script>{/run-once}
<script type="text/javascript">
	var so = new SWFObject("{'flash/mediaplayer.swf'|ezdesign('no')}", "media-player", "651", "109", "8.0.0.0"); 
	so.addParam("quality", "high"); 
	so.addParam("scale", "noscale"); 
	so.addParam("menu", "false"); 
	so.addParam("salign", "lt"); 
	so.addParam("wmode", "transparent");
	so.addParam("flashvars", "xmlFilename=/layout/set/podcast/content/view/full/{$node.node_id}");
	so.write("flashcontent");
</script>
	</div>


   </div>
</div>


