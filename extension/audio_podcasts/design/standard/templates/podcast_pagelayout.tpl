<?xml version="1.0" encoding="utf-8"?> 
{def $node = fetch('content','node',hash('node_id', $module_result.node_id))}

<rss xmlns:atom="http://www.w3.org/2005/Atom"{if eq(ezini('iTunesSettings','AllowUse','podcast.ini'), 'enabled')} xmlns:itunes="http://www.itunes.com/dtds/podcast-1.0.dtd"{/if} version="2.0">


<channel>
	<title>{ezini('GeneralSettings','Title','podcast.ini')}</title>
	<link>{ezini('GeneralSettings','Link','podcast.ini')}</link>
	<description>{ezini('GeneralSettings','Description','podcast.ini')}</description>
	<generator>eZ Publish</generator>
	<docs>http://blogs.law.harvard.edu/tech/rss</docs>
	<language>{ezini('RegionalSettings','Locale','site.ini')|downcase()}</language>
	<pubDate>{$node.modified_subnode|datetime('custom','%D, %d %M %Y %h:%i:%s %O')}</pubDate>
	<lastBuildDate>{$node.modified_subnode|datetime('custom','%D, %d %M %Y %h:%i:%s %O')}</lastBuildDate>

	<atom:link href="{concat('http://', ezsys('hostname'), $module_result.uri)}" rel="self" type="application/rss+xml"/>

{if eq(ezini('iTunesSettings','AllowUse','podcast.ini'), 'enabled')}
{def $iTunesChannel = ezini('iTunesSettings','Channel','podcast.ini')
     $iTunesCategories = $iTunesChannel['Category']|explode(',')}
	<itunes:author>{$iTunesChannel['Author']}</itunes:author>
	<itunes:keywords>{$iTunesChannel['Keywords']}</itunes:keywords>
	<itunes:explicit>{$iTunesChannel['Explicit']}</itunes:explicit>
	<itunes:image href="{$iTunesChannel['Image']}"/>
	<itunes:block>{$iTunesChannel['Block']}</itunes:block>

   {foreach $iTunesCategories as $category}
      {if ezini_hasvariable('iTunesSettings',$category|trim(),'podcast.ini')}
<itunes:category text="{$category|trim()|wash()}">
	<itunes:cateogory text="{ezini('iTunesSettings',$category|trim(),'podcast.ini')|trim()|wash()}" />
</itunes:category>
      {else}
	<itunes:category text="{$category|trim()|wash()}" />
      {/if}
   {/foreach}
{/if}

{if is_set($module_result.view_parameters['iscast'])}
{def $episodeList = fetch('content','list',hash('parent_node_id', ezini('GeneralSettings','NodeId','podcast.ini'),
						'sort_by', array('published',false()),
						'class_filter_type','include',
						'class_filter_array',array('audio') ))}
{foreach $episodeList as $episode}
	{node_view_gui content_node=$episode view='podcast'}
{/foreach}

{else}
{$module_result.content}
{/if}

</channel>

</rss>

{kill_debug()}