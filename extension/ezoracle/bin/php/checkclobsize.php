#!/usr/bin/env php
<?php

require 'autoload.php';

$cli = eZCLI::instance();
$script = eZScript::instance( array(
    'description' => "Lists the maximum size of all CLOB fields in the database",
    'use-session'    => false,
    'use-modules'    => false,
    'use-extensions' => true )
);
$script->startup();
$options = $script->getOptions(
    "[a]",
    "",
    array( 'a' => 'List all tables with CLOB fields, not only the onew with data in them' )
);
$script->initialize();

$cli->output( 'Retrieving database schema definition...' );
// retrieve live db schema, without transforming it to "generic" version
$db = eZDB::instance();
$dbSchema = eZDbSchema::instance( $db );
$liveSchema = $dbSchema->schema( array( 'format' => 'local' ) );

$cli->output( 'Retrieving CLOB lengths (in characters)' );

// for every blob col, get the max length of data stored
// NB: results are in chars, NOT in bytes! This means a 4000 chars blob would not fit in a 4000 bytes varchar2
foreach( $liveSchema as $tableName => $tableDef )
{
    if ( isset( $tableDef['fields'] ) )
    {
        foreach( $tableDef['fields'] as $colName => $colDef )
        {
            if ( $colDef['type'] == 'longtext' )
            {
                $max = $db->arrayquery( "SELECT MAX( DBMS_LOB.GETLENGTH( $colName ) ) AS maxsize FROM $tableName" );
                if ( $options['a'] || $max[0]['maxsize'] )
                {
                    $cli->output( str_pad( "$tableName.$colName:", 62 ) . $max[0]['maxsize'] );
                }
            }
        }
    }
}

$script->shutdown();

