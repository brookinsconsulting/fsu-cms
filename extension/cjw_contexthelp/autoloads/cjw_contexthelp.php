<?php
include 'extension/cjw_contexthelp/autoloads/_operators.php';

/**
 * File containing cjw_contexthelp class.
 *
 * @package cjw_contexthelp
 * @author Felix Woldt - www.jac-systeme.de
 * @copyright Copyright (C) 2007-2008 Coolscreen & JAC Systeme & Webmanufaktur'
 * @license GNU General Public License v2.0
 */
class cjw_contexthelp extends Autocjw_contexthelp
{
    function cjw_contexthelp()
    {
        cjw_contexthelp::Autocjw_contexthelp();
    }

    /**
     * template operator cjw_contexthelp_class
     * @return
     * @example  cjw_contexthelp_class( $classIdentifier, $asObject )}
     * {def $help_for_article = cjw_contexthelp_class( 'article', 'true' )}
     * {def $help_for_all = cjw_contexthelp_class( )}
     */
    function cjw_contexthelp_class( $searchClassIdentifier, $asObject, $containerNodeId )
    {
        // nodes mit zurückgeben für debugging
        if( $asObject === 'true' or $asObject === true )
            $getNode = true;
        else
            $getNode = false;

        if( (int) $containerNodeId > 0 )
        {
            $contextHelpContainerNodeId = (int) $containerNodeId;
        }
        else
        {
            $cjwContextHelpINI = eZINI::instance( 'cjwcontexthelp.ini' );
            $contextHelpContainerNodeId = $cjwContextHelpINI->variable('ContextHelpSettings','ContainerNodeId');
        }

        // fetch only a helptext for a class
        if( $searchClassIdentifier !== 'all' )
        {
            $classAttributeFilter = array( array( 'cjw_contexthelp_class/ez_class_identifier',
                                                       'like',
                                                       $searchClassIdentifier ) );
        }
        // fetch helptexts of all classes
        else
        {
            $classAttributeFilter = null;
        }

        $classNodes = eZContentObjectTreeNode::subTreeByNodeID(
               array (
                      'ClassFilterType' => 'include',
                      'ClassFilterArray' =>
                                              array (
                                                0 => 'cjw_contexthelp_class',
                                              ),
                      'AttributeFilter' => $classAttributeFilter,
                      'IgnoreVisibility' => true,
                      'Limitation' => array ( ),
                      'Offset' => 0,
                      'Limit' => 0,
                    ), $contextHelpContainerNodeId );

        $returnArr = array();

        foreach ( $classNodes as $classNode )
        {
            $dataMap = $classNode->attribute('data_map');
            $ezClassIdentifier = $dataMap['ez_class_identifier']->attribute('content');

            $helpTextEzXml = $dataMap['help_text']->attribute('content');
            $helpTextEzXmlOutput = $helpTextEzXml->attribute('output');
            $classArray = array( 'node_id' => $classNode->attribute('node_id'),
                                 'help_text' => $helpTextEzXmlOutput->attribute('output_text'),
                                 'node' => null );
            if( $getNode )
            {
                    $classArray['node'] = $classNode;
            }
            $returnArr[ $ezClassIdentifier ]['class'] = $classArray;

            $attributeNodes = eZContentObjectTreeNode::subTreeByNodeID(
                                                           array (
                                                                  'ClassFilterType' => 'include',
                                                                  'ClassFilterArray' =>
                                                                                          array (
                                                                                            0 => 'cjw_contexthelp_class_attribute',
                                                                                          ),
                                                                  'IgnoreVisibility' => true,
                                                                  'Limitation' => array ( ),
                                                                  'Offset' => 0,
                                                                  'Limit' => 0,
                                                                ), $classNode->attribute('node_id') );
            $returnArr[ $ezClassIdentifier ]['available_help_attributes'] = array();
            foreach( $attributeNodes as $attributeNode )
            {
                $attributeDataMap = $attributeNode->attribute('data_map');
                $attributeIdentifer = $attributeDataMap['ez_class_attribute_identifier']->attribute('content');

                $helpTextEzXml = $attributeDataMap['help_text']->attribute('content');
                $helpTextEzXmlOutput = $helpTextEzXml->attribute('output');
                $attributeArray = array( 'node_id' => $attributeNode->attribute('node_id'),
                                         'help_text' => $helpTextEzXmlOutput->attribute('output_text'),
                                         'node' => null );
                if( $getNode )
                {
                    $attributeArray['node'] = $attributeNode;
                }
                array_push( $returnArr[ $ezClassIdentifier ]['available_help_attributes'] , $attributeIdentifer );
                $returnArr[ $ezClassIdentifier ]['attributes'][ $attributeIdentifer ] = $attributeArray;
            }
        }

        if( $searchClassIdentifier == 'all' )
        {
            return $returnArr;
        }
        else
        {
            // nur 1 element zurückgeben wenn nach klasse gesucht wurde
            if( array_key_exists( $searchClassIdentifier, $returnArr ) )
            {
                return array_merge( array('search_class_identifier' => $searchClassIdentifier ), $returnArr[ $searchClassIdentifier ] );
            }
            else
                false;
        }
    }

    /**
     * @param $help_class_node_id - node_id of main help class
     * @return array with attributes which has a helptext
    */
    function cjw_contexthelp_available_attributes( $help_class_node_id )
    {

            $attributeNodes = eZContentObjectTreeNode::subTreeByNodeID(
                                                           array (
                                                                  'ClassFilterType' => 'include',
                                                                  'ClassFilterArray' =>
                                                                                          array (
                                                                                            0 => 'cjw_contexthelp_class_attribute',
                                                                                          ),
                                                                  'IgnoreVisibility' => true,
                                                                  'Limitation' => array ( ),
                                                                  'Offset' => 0,
                                                                  'Limit' => 0,
                                                                ), $help_class_node_id );

            $availableAttributeArray = array();
            foreach( $attributeNodes as $attributeNode )
            {

                $attributeDataMap = $attributeNode->attribute('data_map');
                $attributeIdentifer = $attributeDataMap['ez_class_attribute_identifier']->attribute('content');
                $availableAttributeArray[] = $attributeIdentifer;

            }

            return $availableAttributeArray;

    }

    function cjw_contexthelp_available_classes( $help_container_node_id )
    {
            $attributeClasses = eZContentObjectTreeNode::subTreeByNodeID(
                                                           array (
                                                                  'ClassFilterType' => 'include',
                                                                  'ClassFilterArray' =>
                                                                                          array (
                                                                                            0 => 'cjw_contexthelp_class',
                                                                                          ),
                                                                  'IgnoreVisibility' => true,
                                                                  'Limitation' => array ( ),
                                                                  'Offset' => 0,
                                                                  'Limit' => 0,
                                                                ), $help_container_node_id );

            $availableClassArray = array();
            foreach( $attributeClasses as $classNode )
            {
                $dataMap = $classNode->attribute('data_map');
                $classIdentifer = $dataMap['ez_class_identifier']->attribute('content');
                $availableClassArray[] = $classIdentifer;
            }
            return $availableClassArray;

    }

}
?>