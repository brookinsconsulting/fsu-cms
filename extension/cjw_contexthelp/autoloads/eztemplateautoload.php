<?php

$eZTemplateOperatorArray = array();
$eZTemplateOperatorArray[] = array(
    'script' => 'extension/cjw_contexthelp/autoloads/cjw_contexthelp.php',
    'class' => 'cjw_contexthelp',
    'operator_names' => array(
        'cjw_contexthelp_class',
        'cjw_contexthelp_available_attributes',
        'cjw_contexthelp_available_classes'
    )
);
?>