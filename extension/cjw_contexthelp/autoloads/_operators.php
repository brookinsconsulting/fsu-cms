<?php
class Autocjw_contexthelp
{
    function Autocjw_contexthelp(){
        $this->Operators = array(
            'cjw_contexthelp_class',
            'cjw_contexthelp_available_attributes',
            'cjw_contexthelp_available_classes' );
    }

    function & operatorList()   {
        return $this->Operators;
    }

    function namedParameterPerOperator()
    {
        return true;
    }

    function namedParameterList()
    {
        return array(
            'cjw_contexthelp_class' => array(
                'classIdentifier' => array('type' => 'string', 'required' => false, 'default' => 'all'),
'asObject' => array('type' => 'string', 'required' => false, 'default' => 'false'),
'containerNodeId' => array('type' => 'string', 'required' => false, 'default' => 'false')),
            'cjw_contexthelp_available_attributes' => array(
                'help_class_node_id' => array('type' => 'string', 'required' => true, 'default' => '')),
            'cjw_contexthelp_available_classes' => array(
                'help_container_node_id' => array('type' => 'string', 'required' => true, 'default' => '')));
    }

    function modify(& $tpl, & $operatorName, & $operatorParameters, & $rootNamespace, & $currentNamespace, & $operatorValue, & $namedParameters)
    {
        switch ( $operatorName )
        {
            case 'cjw_contexthelp_class':
                $operatorValue = $this->cjw_contexthelp_class($namedParameters['classIdentifier'], $namedParameters['asObject'], $namedParameters['containerNodeId']);
            break;

            case 'cjw_contexthelp_available_attributes':
                $operatorValue = $this->cjw_contexthelp_available_attributes($namedParameters['help_class_node_id']);
            break;

            case 'cjw_contexthelp_available_classes':
                $operatorValue = $this->cjw_contexthelp_available_classes($namedParameters['help_container_node_id']);
            break;

        }
    }

}
?>