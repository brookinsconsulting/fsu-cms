<?php /* #?ini charset="utf-8"?

[ExtensionSettings]
DesignExtensions[]=cjw_contexthelp

[StylesheetSettings]

# A list of CSS file to always include in the pagelayout
# This can be filled in by extensions to provide styles
# that are not in the standard themes.

CSSFileList[]=contexthelp.css

[JavaScriptSettings]
#JavaScriptList[]=jquery-1.3.2.min.js

*/ ?>
