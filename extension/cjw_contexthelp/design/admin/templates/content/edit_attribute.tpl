{* # context_help - content/edit_attribute.tpl # *}
{default $view_parameters=array()}

{* cjw_contexthelp *}

<script type="text/javascript" language="javascript" src={"javascript/contexthelp.js"|ezdesign()}></script>

{* to get no validation error - include this into head *}
{* <link rel="stylesheet" type="text/css" href={"stylesheets/contexthelp.css"|ezdesign()} /> *}

{def $search_class_identifier = $object.class_identifier
     $context_help = cjw_contexthelp_class( $search_class_identifier )}

{* global help for class *}
{if and( is_array( $context_help ), is_set( $context_help.class.help_text ) )}
<div id="class_help">{$context_help.class.help_text}</div>

    {* help for attributes *}
    {if is_array( $context_help.attributes ) }
        {foreach $context_help.attributes as $attribute_identifier_name => $attribute}
        <div class="tooltip" id="tt{$attribute.node_id}">{$attribute.help_text}</div>
        {/foreach}
    {/if}

{/if}

{*$context_help|attribute(show,3)*}

{foreach $content_attributes as $item}
    {def $input_required = $item.contentclass_attribute.is_required|choose( '', '*' )
         $attribute_identifier = $item.contentclass_attribute_identifier }

    {if is_array($context_help)}
        {def $is_tooltip_available = $context_help.available_help_attributes|contains( $attribute_identifier )}
    {else}
        {def $is_tooltip_available = false()}
    {/if}

<div class="block {$object.class_identifier} {$item.contentclass_attribute_identifier}">
{* only show edit GUI if we can edit *}
{if and(eq($item.contentclass_attribute.can_translate,0),
                  ne($object.initial_language_code,$item.language_code) ) }
    <label>{$item.contentclass_attribute.name|wash}{$input_required}{if $is_tooltip_available} <span class="tooltiplink" onmouseover="showWMTT('tt{$context_help.attributes[ $attribute_identifier ].node_id}')" onmouseout="hideWMTT()"><img src={'images/icon_attention.gif'|ezdesign()} alt="{'helptext'|i18n('cjw_newsletter/edit_attribute')}" /></span>{/if}</label><div class="labelbreak"></div>
    <input type="hidden" name="ContentObjectAttribute_id[]" value="{$item.id}" />
        {attribute_view_gui attribute_base=$attribute_base attribute=$item view_parameters=$view_parameters}
    </div>
{else}
    <label{if $item.has_validation_error} class="validation-error"{/if}>{$item.contentclass_attribute.name|wash}{$input_required}{if $is_tooltip_available} <span class="tooltiplink" onmouseover="showWMTT('tt{$context_help.attributes[ $attribute_identifier ].node_id}')" onmouseout="hideWMTT()"><img src={'images/icon_attention.gif'|ezdesign()} alt="{'helptext'|i18n('cjw_newsletter/edit_attribute')}" /></span>{/if}</label><div class="labelbreak"></div>
    <input type="hidden" name="ContentObjectAttribute_id[]" value="{$item.id}" />

        {attribute_edit_gui attribute_base=$attribute_base attribute=$item view_parameters=$view_parameters}

    </div>
{/if}
{undef $input_required $attribute_identifier $is_tooltip_available}
{/foreach}
{/default}