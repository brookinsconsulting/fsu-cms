{* # ezstring - contexthelp # *}

{* TODO - nur classen anzeigen die noch keinen hilfe haben *}
{def $parent_node_id = $attribute.object.current.main_parent_node_id}
<br>

{def $available_classes = cjw_contexthelp_available_classes( $parent_node_id ) }

{*$available_classes|attribute(show)*}


{default attribute_base='ContentObjectAttribute'
         html_class='full'}
{*<input id="ezcoa-{if ne( $attribute_base, 'ContentObjectAttribute' )}{$attribute_base}-{/if}{$attribute.contentclassattribute_id}_{$attribute.contentclass_attribute_identifier}" class="{eq( $html_class, 'half' )|choose( 'box', 'halfbox' )} ezcc-{$attribute.object.content_class.identifier} ezcca-{$attribute.object.content_class.identifier}_{$attribute.contentclass_attribute_identifier}" type="text" size="70" name="{$attribute_base}_ezstring_data_text_{$attribute.id}" value="{$attribute.data_text|wash( xhtml )}" />*}



{def $class_array = fetch( 'class', 'list' )}

{* Always set the .._selected_array_.. variable, this circumvents the problem when nothing is selected. *}
{*<input type="hidden" name="{$attribute_base}_ezselect_selected_array_{$attribute.id}" value="" />*}

<input type="hidden" name="{$attribute_base}_ezstring_data_text_{$attribute.id}" value="{$attribute.data_text|wash( xhtml )}" />


<select id="ezcoa-{if ne( $attribute_base, 'ContentObjectAttribute' )}{$attribute_base}-{/if}{$attribute.contentclassattribute_id}_{$attribute.contentclass_attribute_identifier}" class="ezcc-{$attribute.object.content_class.identifier} ezcca-{$attribute.object.content_class.identifier}_{$attribute.contentclass_attribute_identifier}" name="{$attribute_base}_ezstring_data_text_{$attribute.id}">
<option value=""> - </option>
{foreach $class_array as $class}
{def $help_text_already_exists = $available_classes|contains( $class.identifier )|choose('', '***')}
    {if or( $help_text_already_exists|eq(''), $class.identifier|eq($attribute.data_text))}
        <option value="{$class.identifier}" {if $class.identifier|eq( $attribute.data_text ) }selected="selected"{/if}>{$help_text_already_exists} {$class.identifier} ( {$class.name|wash()} ) {$help_text_already_exists}</option>
    {/if}
{undef $help_text_already_exists}
{/foreach}
</select>
{undef $class_array $parent_node_id}
{/default}
