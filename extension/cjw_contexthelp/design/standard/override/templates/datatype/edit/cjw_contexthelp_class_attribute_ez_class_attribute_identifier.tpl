 {*# override ezstring - contexthelp #*}
{default attribute_base='ContentObjectAttribute'
         html_class='full'}

{* TODO - nur attribute anzeigen die noch keinen hilfe haben *}


<br>



{*<input id="ezcoa-{if ne( $attribute_base, 'ContentObjectAttribute' )}{$attribute_base}-{/if}{$attribute.contentclassattribute_id}_{$attribute.contentclass_attribute_identifier}" class="{eq( $html_class, 'half' )|choose( 'box', 'halfbox' )} ezcc-{$attribute.object.content_class.identifier} ezcca-{$attribute.object.content_class.identifier}_{$attribute.contentclass_attribute_identifier}" type="text" size="70" name="{$attribute_base}_ezstring_data_text_{$attribute.id}" value="{$attribute.data_text|wash( xhtml )}" />*}

{def $help_class_main_node_id = $attribute.object.current.main_parent_node_id
     $help_class_identifier = fetch('content','node', hash('node_id', $help_class_main_node_id )).data_map['ez_class_identifier'].content
     $available_attributes = cjw_contexthelp_available_attributes( $help_class_main_node_id ) }
{*$available_attributes|attribute(show)*}
Attribute of Class >> {$help_class_identifier} << <br />
{def $class_list=fetch( 'class', 'list', hash( 'class_filter', array( $help_class_identifier ) ) )
     $class_attribute_data_map = array()
}
{if $class_list|count|gt( 0 )}
    {set $class_attribute_data_map = $class_list.0.data_map}
{/if}

{*$class_attribute_data_map|attribute(show)*}

{* Always set the .._selected_array_.. variable, this circumvents the problem when nothing is selected. *}
{*<input type="hidden" name="{$attribute_base}_ezselect_selected_array_{$attribute.id}" value="" />*}

<input type="hidden" name="{$attribute_base}_ezstring_data_text_{$attribute.id}" value="{$attribute.data_text|wash( xhtml )}" />


<select id="ezcoa-{if ne( $attribute_base, 'ContentObjectAttribute' )}{$attribute_base}-{/if}{$attribute.contentclassattribute_id}_{$attribute.contentclass_attribute_identifier}" class="ezcc-{$attribute.object.content_class.identifier} ezcca-{$attribute.object.content_class.identifier}_{$attribute.contentclass_attribute_identifier}" name="{$attribute_base}_ezstring_data_text_{$attribute.id}">
<option value=""> - </option>
{foreach $class_attribute_data_map as $index => $class_attribute}
{def $help_text_already_exists = $available_attributes|contains($index)|choose('', '***')}
    {if or( $help_text_already_exists|eq(''), $class_attribute.identifier|eq($attribute.data_text))}
        <option value="{$class_attribute.identifier}" {if $class_attribute.identifier|eq( $attribute.data_text ) }selected="selected"{/if}>{$help_text_already_exists} {$class_attribute.identifier} ( {$class_attribute.name|wash( )} ) {$help_text_already_exists}</option>
    {/if}
{undef $help_text_already_exists}
{/foreach}
</select>
{undef $class_attribute_data_map $class_list}
{/default}
