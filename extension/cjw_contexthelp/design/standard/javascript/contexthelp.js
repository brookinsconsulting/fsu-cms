// Tooltips
// nach einer Vorlage aus www.webmatze.de
// korrigiert f�r ie6 aufgrund http://www.quirksmode.org/js/doctypes.html
// Anwendung
// onMouseOver="showWMTT('id')" onMouseOut="hideWMTT()">
// <d�v id="id123">Inhalt</div>
wmtt = null;
document.onmousemove = updateWMTT;
function updateWMTT(e) {
     x = (document.all) ? $(window.event.srcElement).offset().left : e.target.offsetLeft;
     y = (document.all) ? $(window.event.srcElement).offset().top -170 : e.target.offsetTop;

     if (wmtt != null) {
         wmtt.style.left = (x + 20) + "px";
         wmtt.style.top  = (y + 20) + "px";
     }
}
function showWMTT(id) {
    wmtt = document.getElementById(id);
    wmtt.style.display = "block"
}
function hideWMTT() {
    wmtt.style.display = "none";
}
