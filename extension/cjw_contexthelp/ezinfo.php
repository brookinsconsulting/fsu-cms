<?php
class cjw_contexthelpInfo
{
    static function info()
    {
        return array( 'Name' => 'cjw_contexthelp -Context Sensitive Help in Frontside Editing',
                      'Version' => '2.0.0',
                      'eZ version' => '4.x',
                      'Copyright' => '(C) 2007-2008 <a href="http://www.coolscreen.de">Coolscreen</a> &amp; <a href="http://www.jac-systeme.de">JAC Systeme</a> &amp; <a href="http://www.webmanufaktur.ch">Webmanufaktur</a>',
                      'License' => "GNU General Public License v2.0",
                      'More Information' => '<a href="http://projects.ez.no/cjw_contexthelp">http://projects.ez.no/cjw_contexthelp</a>'
                    );
    }
}
?>
