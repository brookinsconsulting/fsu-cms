#!/usr/bin/env php
<?php

require 'autoload.php';

set_time_limit( 0 );

$script = eZScript::instance( array( 'description'    => ( "eZ Publish content object publisher in an loop\n" ),
                                     'use-session'    => false,
                                     'use-modules'    => true,
                                     'use-extensions' => false ) );

$script->startup();

$optionList = $script->getOptions(
    "[iterations:]",
    "",
    array(
        'iterations' => 'The number of iterations to loop over',
    )
);

$sys = eZSys::instance();

$script->initialize();

$iterationNumber = $optionList['iterations'];

$out = new ezcConsoleOutput();

$progress = new ezcConsoleProgressbar(
    $out, (int) $iterationNumber, array('fractionFormat' => "%01d")
);

for($i = 0; $i <= $iterationNumber; $i++) {
	//eZCache::clearContentCache();
	file_get_contents('http://146.201.5.231');
	$progress->advance();
}

$progress->finish();

//$out->outputText("\nok");

$script->shutdown();

?>
