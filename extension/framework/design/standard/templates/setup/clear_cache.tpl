{def $selected_cache_type=ezpreference('admin_clearcache_type')
	 $caches_list=array(array('All','All caches'|i18n('design/standard/pagelayout')|wash,false),
						array('Content','Content'|i18n('design/standard/pagelayout')|wash,false),
						array('ContentNode','Content - node'|i18n('design/standard/pagelayout')|wash,true),
						array('ContentSubtree','Content - subtree'|i18n('design/standard/pagelayout')|wash,true),
						array('Template','Template'|i18n('design/standard/pagelayout')|wash,false),
						array('TemplateContent','Template & content'|i18n('design/standard/pagelayout')|wash,false),
						array('Ini','Ini settings'|i18n('design/standard/pagelayout')|wash,false),
						array('Static','Static'|i18n('design/standard/pagelayout')|wash,false))
}
{if is_unset($ui_context)}{def $ui_context=''}{/if}
<form id="clearcache" action={'setup/cachetoolbar'|sitelink()} method="post">
{if is_set($module_result.content_info.node_id)}
	<input type="hidden" name="NodeID" value="{$module_result.content_info.node_id}" />
	<input type="hidden" name="ObjectID" value="{$module_result.content_info.object_id}" />
{/if}
	<select{eq($ui_context,'edit')|choose('',' disabled="disabled"')} name="CacheTypeValue">
	{foreach $caches_list as $item}
		{if or(eq($item[2],false),is_set($module_result.content_info))}<option{eq($selected_cache_type,$item[0])|choose('',' selected="selected"')} value="{$item[0]}">{$item[1]}</option>{/if}
	{/foreach}
	</select>
	<input {eq($ui_context,'edit')|choose('class="button"','class="button-disabled"')}{eq($ui_context,'edit')|choose('',' disabled="disabled"')} type="submit" name="ClearCacheButton" value="{'Clear'|i18n('design/standard/pagelayout')}" />
</form>
{undef $selected_cache_type $caches_list}