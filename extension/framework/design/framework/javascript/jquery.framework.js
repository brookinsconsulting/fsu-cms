var $framework=(function($){
	var $lib={
		'console':function(){
			if(window.console&&window.console.log){
				window.console.log.apply(this,arguments);
			}
		},
		'cycle':function(o,p){
			if(p.images && p.images.length){
				if(p.images.length>1){
					$('#banner-container .next').css({'display':'block'});
				}
				$.each(p.images,function(k,v){
					var img='<img alt="" src="'+v.src+'" width="'+p.size.width+'" height="'+p.size.height+'" />';
					if(v.link){
						img='<a href="'+v.link+'"'+(v.new_window?' target="_blank"':'')+'>'+img+'</a>';
					}
					if(p.data){
						img=$(img);
						if(img.is('a')){
							p.images[k]=img.find('img').data('cycle.data',v.data).end();
							return true;
						}
						p.images[k]=img.data('cycle.data',v.data);
						return true;
					}
					p.images[k]=img;
				});
				if(p.data){
					$.each(p.images,function(k,v){
						v.appendTo(o);
					});
					return o.cycle(p.animation);
				}
				return o.append(p.images.join('\n')).cycle(p.animation);
			}
			return false;
		},
		'menu':function(o,p,s){
			var $menu='<ul class="menu '+(p.orientation?p.orientation:'vertical')+'">';
			$.each(p.menu,function(k,v){
				$menu+='<li><a'+(v.id?' id="'+v.id+'"':'')+' href="'+v.link+'">'+v.label+'</a>';
				if(k<p.menu.length-1 && p.delimiter){
					$menu+='<span class="delimiter">'+p.delimiter+'</span>';
				}
				if(v.menu){
					$menu+=$lib.menu(o,$.extend(v,{'delimiter':p.delimiter}),true);
				}
				$menu+='</li>';
			});
			$menu+='</ul>';
			if(s){
				return $menu;
			}
			if(p.header){
				$menu='<'+p.header.tag+'>'+p.header.text+'</'+p.header.tag+'>'+$menu;
			}
			o.append($menu);
		}
	};

	$.fn.framework=function(f,o){
		var o=$.extend({
		}, o);

		$lib[f].apply(null,[this,o]);

		this.each(function(k,v){
		});

		return this;
	};

	return {
		'console':$lib.console,
		'cycle':function($selector,$settings,$controls){
			var $events={
					'after':null,
					'before':null,
					'end':null,
					'onPagerEvent':null,
					'onPrevNextEvent':null,
					'pagerAnchorBuilder':null,
					'timeoutFn':null,
					'updateActivePagerLink':null
				};
			$settings=$.extend(true,{
				'animation':{
					'speed':1000,
					'fx':'fade',
					'prev':false,
					'next':false
				},
				'images':false,
				'size':false,
				'frame':false,
				'events':false
			},$settings);

			if($settings.frame){
				$($settings.frame).wrap('<a id="banner-frame-link"></a>');
				$events.before=function(c,n,o,f){
					var $framelink=$('#banner-frame-link'),
						$obj=$(this),
						$href=$obj.attr('href'),
						$target=$obj.attr('target');
					if($href){
						$framelink.attr('href',$href);
					}else{
						$framelink.removeAttr('href');
					}
					if($target){
						$framelink.attr('target',$target);
					}else{
						$framelink.removeAttr('target');
					}
				}
			}

			$.each($events,function(k,v){
				if($events[k] && $settings.events[k]){
					$settings.animation[k]=function(){
						if($events[k]){
							$events[k].apply(this,arguments);
						}
						if($settings.events[k]){
							$settings.events[k].apply(this,arguments);
						}
					}
				}else if($events[k]){
					$settings.animation[k]=$events[k];
				}else if($settings.events[k]){
					$settings.animation[k]=$settings.events[k];
				}
			});

			var $object=$($selector),
				$cycle=$object.empty().framework('cycle',$settings);
			if($cycle){
				if($settings.frame){
					$object.parent().bind('mouseenter mouseleave',function(){
						$cycle.cycle('toggle');
					});
				}
				if($controls){
					var $cs=$($controls.selector);
					$($controls.bind).bind({
						'mouseenter':function(){
							if($.browser.msie){
								$cs.show();
							}else{
								$cs.fadeIn();
							}
						},
						'mouseleave':function(){
							if($.browser.msie){
								$cs.hide();
							}else{
								$cs.fadeOut();
							}
						}
					});
				}
				return true;
			}
			return false;
		},
		'fade':function($selector,$fade,$hover){
			var $item=$($selector).fadeTo($fade[0],$fade[1]);
			if($hover){
				$item.hover(function(){
					$(this).fadeTo($hover[0],1);
				},function(){
					$(this).fadeTo($hover[1],$fade[1]);
				});
			}
			return $item;
		},
		'log':$lib.console,
		'menu':function($selector,$settings){
			$settings=$.extend(true,{
				'animation':{
					'speed':[100,100],
					'fx':['slideDown','slideUp']
				},
				'delimiter':'|',
				'equalize':false,
				'header':false,
				'menu':false,
				'vertical':false
			},$settings);
			var $w=0,$l=0,
				$menu=$($selector).empty().framework('menu',{
					'delimiter':($settings.delimiter || $settings.delimiter===false)?$settings.delimiter:'|',
					'header':$settings.header,
					'menu':$settings.menu,
					'orientation':($settings.vertical || $settings.vertical==null)?'vertical':'horizontal'
				}).find('>ul.menu>li').each(function(k,v){
					var $v=$(v),
						$ul=$v.find('ul');
					if($settings.equalize){$w+=$v.outerWidth();$l=k;}
					if($ul.length>0){
						$v.hover(function(){
							$ul.stop(true,true).slideDown($settings.animation.speed[0]);
						},function(){
							$ul.stop(true,true).slideUp($settings.animation.speed[1]);
						});
					}
				});
			if($settings.equalize){
				$p=Math.round(($menu.end().width()-$w)/(++$l*2));
				$menu.find('>a').css({'paddingLeft':$p+'px','paddingRight':$p+'px'});
			}
			return $menu.end();
		}
	};

})(jQuery);