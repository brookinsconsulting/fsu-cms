{def $linkClass = array()}
{if ne($classification|trim,'')}
	{set $linkClass = $linkClass|append($classification|wash())}
{/if}
<a{if count($linkClass)} class="{$linkClass|implode(' ')}"{/if} href={$href|sitelink()} {if and(is_set($id),ne($id,''))} id="{$id}"{/if}{if and(is_set($title),ne($title,''))} title="{$title}"{/if}{if and(is_set($target),ne($target,'_self'))} target="{$target}"{/if}>{$content}</a>