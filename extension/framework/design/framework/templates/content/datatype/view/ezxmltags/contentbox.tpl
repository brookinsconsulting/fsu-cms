{* Content Box - Custom Tag *}
<section class="module {first_set($display_type,'customtag')}{if first_set($class,false())} {$class|implode(' ')}{/if}">
	{if and(is_set($header),ne($header,''))}<header>{cond(eq($header_type,'text'),concat('<h1>',$header,'</h1>'),html_entity_decode($header))}</header>{/if}
	<section class="{first_set($display_type,'customtag')}-content">{$content}</section>
</section>
