{def $path_array=$current_node|sitelink_path()}
{if and(is_set($reverse),$reverse)}{set $path_array=$path_array|reverse()}{/if}
{if and(is_set($text_only),$text_only)}
{foreach $path_array as $key=>$path}
	{if not($path.current)}{$path.text|wash()}{else}{$path.text|wash()}{/if}{delimiter}{first_set($delimiter,' / ')}{/delimiter}
{/foreach}
{else}
<div id="path">
	<ul class="menu horizontal">
	{foreach $path_array as $key=>$path}
		{if not($path.current)}
			<li{if eq($path.node_id,$indexpage)} class="first"{/if}><a href="{$path.url_alias}">{$path.text|wash()}</a><span class="delimiter">{first_set($delimiter,'/')}</span></li>
		{else}
			<li class="last">{$path.text|wash()}</li>
		{/if}
	{/foreach}
	</ul>
</div>
{/if}