<?php

$ModuleTools = ModuleTools::instance();

$Module=array(
	'name'=>'Site Information',
	'variable_params'=>true
	);

$ViewList = array();

$ViewList['updates'] = array(
	'name'=>'Recent Site Updates',
	'script'=>'updates.php',
	'params'=>array('SortBy','Class'),
	'unordered_params'=>array('offset'=>'Offset')
	);


?>