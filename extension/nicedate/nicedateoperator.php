<?php

class nicedateOperator
{
    var $Operators;

    function custom_relative_date($time) {
	 $today = strtotime(date('M j, Y'));
	 $reldays = ($time - $today)/86400;
	 if ($reldays >= 0 && $reldays < 1) {
		return 'today';
       } else if ($reldays >= 1 && $reldays < 2) {
		return 'tomorrow';
       } else if ($reldays >= -1 && $reldays < 0) {
		return 'yesterday';
       }
       if (abs($reldays) < 7) {
		if ($reldays > 0) {
		   $reldays = floor($reldays);
		   return 'in ' . $reldays . ' day' . ($reldays != 1 ? 's' : '');
		} else {
		   $reldays = abs(floor($reldays));
		   return $reldays . ' day'  . ($reldays != 1 ? 's' : '') . ' ago';
		}
       }
       if (abs($reldays) < 182) {
		return date('l, F j',$time ? $time : time());
       } else {
		return date('l, F j, Y',$time ? $time : time());
       }
    }
  
    function nicedateOperator( $name = array() )
    {
	$this->Operators = array( 'nicedate', 'minsandsecs' );
    }


    function &operatorList()
    {
	return $this->Operators;
    }


    function namedParameterPerOperator()
    {
        return true;
    }   

    function namedParameterList()
    {
        return array( 'nicedate' => array() );
    }


    function modify( $tpl, &$operatorName, &$operatorParameters, &$rootNamespace, &$currentNamespace, &$operatorValue, &$namedParameters )
    {
	  
	  if ($operatorName == 'nicedate') {
		$operatorValue = $this->custom_relative_date($operatorValue);
	       return true; 
	  } elseif ($operatorName == 'minsandsecs') {
		$mins = floor($operatorValue/60);
		$secs = $mins == 0 ? $operatorValue : $operatorValue % ($mins * 60);
		$secs = $secs < 10 ? "0".$secs : $secs;
		$operatorValue = $mins.":".$secs;
	       return true; 
	  }
	  return false;
    }
}

?>
