<!DOCTYPE TS><TS>
<context>
    <name>design/ezwebin/article/article_index</name>
    <message>
        <source>Article index</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>design/ezwebin/article/comments</name>
    <message>
        <source>Comments</source>
        <translation type="unfinished">Komentarze</translation>
    </message>
    <message>
        <source>New comment</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>%login_link_startLog in%login_link_end or %create_link_startcreate a user account%create_link_end to comment.</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>design/ezwebin/blog/calendar</name>
    <message>
        <source>Previous month</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Next month</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Calendar</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Mon</source>
        <translation type="unfinished">Pon</translation>
    </message>
    <message>
        <source>Tue</source>
        <translation type="unfinished">Wt</translation>
    </message>
    <message>
        <source>Wed</source>
        <translation type="unfinished">Śr</translation>
    </message>
    <message>
        <source>Thu</source>
        <translation type="unfinished">Czw</translation>
    </message>
    <message>
        <source>Fri</source>
        <translation type="unfinished">Pt</translation>
    </message>
    <message>
        <source>Sat</source>
        <translation type="unfinished">So</translation>
    </message>
    <message>
        <source>Sun</source>
        <translation type="unfinished">N</translation>
    </message>
</context>
<context>
    <name>design/ezwebin/blog/extra_info</name>
    <message>
        <source>Tags</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Archive</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>design/ezwebin/collectedinfo/form</name>
    <message>
        <source>Form %formname</source>
        <translation>Formularz %formname</translation>
    </message>
    <message>
        <source>Thank you for your feedback.</source>
        <translation>Dziękujemy za kontakt z nami.</translation>
    </message>
    <message>
        <source>Return to site</source>
        <translation>Powrót na stronę</translation>
    </message>
    <message>
        <source>You have already submitted this form. The data you entered was:</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>design/ezwebin/collectedinfo/poll</name>
    <message>
        <source>Poll %pollname</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Results</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Please log in to vote on this poll.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>You have already voted for this poll.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Votes</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>%count total votes</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Back to poll</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>design/ezwebin/collectedinfomail/feedback</name>
    <message>
        <source>Feedback from %1</source>
        <translation type="unfinished">Odpowiedź od %1
<byte value="x9"/><byte value="x9"/></translation>
    </message>
    <message>
        <source>The following feedback was collected</source>
        <translation type="unfinished">Zebrano następujące informacje</translation>
    </message>
</context>
<context>
    <name>design/ezwebin/collectedinfomail/form</name>
    <message>
        <source>Collected information from %1</source>
        <translation type="unfinished">Zbieranie informacji z %1</translation>
    </message>
    <message>
        <source>The following information was collected</source>
        <translation type="unfinished">Zebrano następujące informacje</translation>
    </message>
</context>
<context>
    <name>design/ezwebin/content/advancedsearch</name>
    <message>
        <source>Advanced search</source>
        <translation>Szukanie zaawansowane</translation>
    </message>
    <message>
        <source>Search all the words</source>
        <translation>Szukaj wszystkie słowa</translation>
    </message>
    <message>
        <source>Search the exact phrase</source>
        <translation>Szukaj dokładnej frazy</translation>
    </message>
    <message>
        <source>Published</source>
        <translation>Opublikowany</translation>
    </message>
    <message>
        <source>Any time</source>
        <translation>Dowolny czas</translation>
    </message>
    <message>
        <source>Last day</source>
        <translation>Wczoraj</translation>
    </message>
    <message>
        <source>Last week</source>
        <translation>Ostatni tydzień</translation>
    </message>
    <message>
        <source>Last three months</source>
        <translation>Ostatni miesiąc</translation>
    </message>
    <message>
        <source>Last year</source>
        <translation>Ostatni rok</translation>
    </message>
    <message>
        <source>Display per page</source>
        <translation>Wyników na stronie</translation>
    </message>
    <message>
        <source>5 items</source>
        <translation>5 pozycji</translation>
    </message>
    <message>
        <source>10 items</source>
        <translation>10 pozycji</translation>
    </message>
    <message>
        <source>20 items</source>
        <translation>20 pozycji</translation>
    </message>
    <message>
        <source>30 items</source>
        <translation>30 pozycji</translation>
    </message>
    <message>
        <source>50 items</source>
        <translation>50 pozycji</translation>
    </message>
    <message>
        <source>Search</source>
        <translation>Szukaj</translation>
    </message>
    <message>
        <source>No results were found when searching for &quot;%1&quot;</source>
        <translation>Wyszukiwanie &quot;%1&quot; nie dało żadnych wyników</translation>
    </message>
    <message>
        <source>Search for &quot;%1&quot; returned %2 matches</source>
        <translation>Wyszukiwanie &quot;%1&quot; zwróciło %2 wyników</translation>
    </message>
    <message>
        <source>Last month</source>
        <translation type="unfinished">Ostatni miesiąc</translation>
    </message>
</context>
<context>
    <name>design/ezwebin/content/browse</name>
    <message>
        <source>Browse</source>
        <translation>Przeglądaj</translation>
    </message>
    <message>
        <source>To select objects, choose the appropriate radiobutton or checkbox(es), and click the &quot;Select&quot; button.</source>
        <translation>Aby zaznaczyć obiekty, wybierz odpowiednie przyciski radio lub checkbox i kliknij przecisk &quot;Zaznacz&quot;.</translation>
    </message>
    <message>
        <source>Back</source>
        <translation>Wstecz</translation>
    </message>
    <message>
        <source>Top level</source>
        <translation>Najwyższy poziom</translation>
    </message>
    <message>
        <source>Select</source>
        <translation>Wybierz</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation>Anuluj</translation>
    </message>
    <message>
        <source>To select an object that is a child of one of the displayed objects, click the parent object name to display a list of its children.</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>design/ezwebin/content/browse_mode_list</name>
    <message>
        <source>Name</source>
        <translation>Nazwa</translation>
    </message>
    <message>
        <source>Type</source>
        <translation>Typ</translation>
    </message>
    <message>
        <source>Invert selection</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>design/ezwebin/content/diff</name>
    <message>
        <source>Versions for &lt;%object_name&gt; [%version_count]</source>
        <translation>Liczba wersji &lt;%object_name&gt; [%version_count]</translation>
    </message>
    <message>
        <source>Version</source>
        <translation>Wersja</translation>
    </message>
    <message>
        <source>Status</source>
        <translation>Status</translation>
    </message>
    <message>
        <source>Translations</source>
        <translation>Tłumaczenia</translation>
    </message>
    <message>
        <source>Creator</source>
        <translation>Twórca</translation>
    </message>
    <message>
        <source>Modified</source>
        <translation>Zmodyfikowany</translation>
    </message>
    <message>
        <source>Draft</source>
        <translation>Szkic</translation>
    </message>
    <message>
        <source>Published</source>
        <translation>Opublikowany</translation>
    </message>
    <message>
        <source>Pending</source>
        <translation>Oczekujący</translation>
    </message>
    <message>
        <source>Archived</source>
        <translation>Zarchiwizowany</translation>
    </message>
    <message>
        <source>Rejected</source>
        <translation>Odrzucony</translation>
    </message>
    <message>
        <source>Untouched draft</source>
        <translation>Nieużywany szkic</translation>
    </message>
    <message>
        <source>This object does not have any versions.</source>
        <translation>Ten obiekt nie ma żadnej wersji.</translation>
    </message>
    <message>
        <source>Show differences</source>
        <translation>Pokaż różnice</translation>
    </message>
    <message>
        <source>Differences between versions %oldVersion and %newVersion</source>
        <translation>Różnice pomiędzy wersjami %oldVersion i %newVersion</translation>
    </message>
    <message>
        <source>Old version</source>
        <translation>Stara wersja</translation>
    </message>
    <message>
        <source>Inline changes</source>
        <translation>Zmiany w linii</translation>
    </message>
    <message>
        <source>Block changes</source>
        <translation>Zmiany blokowe</translation>
    </message>
    <message>
        <source>New version</source>
        <translation>Nowa wersja</translation>
    </message>
</context>
<context>
    <name>design/ezwebin/content/draft</name>
    <message>
        <source>Select all</source>
        <translation>Wybierz wszystko</translation>
    </message>
    <message>
        <source>Deselect all</source>
        <translation>Odznacz wszystykie</translation>
    </message>
    <message>
        <source>My drafts</source>
        <translation>Moje szkice</translation>
    </message>
    <message>
        <source>These are the current objects you are working on. The drafts are owned by you and can only be seen by you.
      You can either edit the drafts or remove them if you don&apos;t need them any more.</source>
        <translation>To są aktualne obiekty, na których właśnie pracujesz. Szkice są Twoją własnoścą i moą być edytowane tylko przez Ciebie.
      Możesz albo edytować szkice, albo je usunąć, jeżeli ich już nie potrzebujesz.</translation>
    </message>
    <message>
        <source>Name</source>
        <translation>Nazwa</translation>
    </message>
    <message>
        <source>Class</source>
        <translation>Klasa</translation>
    </message>
    <message>
        <source>Section</source>
        <translation>Sekcja</translation>
    </message>
    <message>
        <source>Version</source>
        <translation>Wersja</translation>
    </message>
    <message>
        <source>Language</source>
        <translation>Język</translation>
    </message>
    <message>
        <source>Last modified</source>
        <translation>Ostatnia modyfikacja</translation>
    </message>
    <message>
        <source>Edit</source>
        <translation>Edytuj</translation>
    </message>
    <message>
        <source>Remove</source>
        <translation>Usuń</translation>
    </message>
    <message>
        <source>You have no drafts</source>
        <translation>Nie masz żadnych szkiców</translation>
    </message>
    <message>
        <source>Empty draft</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>design/ezwebin/content/edit</name>
    <message>
        <source>Manage versions</source>
        <translation>Zarządzaj wersjami</translation>
    </message>
    <message>
        <source>Store and exit</source>
        <translation>Zapisz i zakończ</translation>
    </message>
    <message>
        <source>Preview</source>
        <translation>Podgląd</translation>
    </message>
    <message>
        <source>Translate</source>
        <translation>Przetłumacz</translation>
    </message>
    <message>
        <source>Edit %1 - %2</source>
        <translation>Edycja %1 - %2</translation>
    </message>
    <message>
        <source>Send for publishing</source>
        <translation>Wyślij do publikacji</translation>
    </message>
    <message>
        <source>Store draft</source>
        <translation>Zapisz szkic</translation>
    </message>
    <message>
        <source>Discard draft</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Translate from</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Documentation</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Translating content from %from_lang to %to_lang</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Content in %language</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>design/ezwebin/content/edit_attribute</name>
    <message>
        <source>Not translatable</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Required</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Information collector</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>design/ezwebin/content/edit_draft</name>
    <message>
        <source>The currently published version is %version and was published at %time.</source>
        <translation>Obecna prezentowana jest %version wersja i została opublikowana %time.</translation>
    </message>
    <message>
        <source>The last modification was done at %modified.</source>
        <translation>Ostatnia modyfikacja %modified.</translation>
    </message>
    <message>
        <source>The object is owned by %owner.</source>
        <translation>Właścicielem obiektu jest %owner.</translation>
    </message>
    <message>
        <source>This object is already being edited by you.
        You can either continue editing one of your drafts or you can create a new draft.</source>
        <translation>Obiekt jest obecnie modyfikowany przez ciebie.
   Możesz kontynuować edycję swojego szkicu lub stworzyć nowy.</translation>
    </message>
    <message>
        <source>Current drafts</source>
        <translation>Obecne szkice</translation>
    </message>
    <message>
        <source>Version</source>
        <translation>Wersja</translation>
    </message>
    <message>
        <source>Name</source>
        <translation>Nazwa</translation>
    </message>
    <message>
        <source>Owner</source>
        <translation>Właściciel</translation>
    </message>
    <message>
        <source>Created</source>
        <translation>Utworzono</translation>
    </message>
    <message>
        <source>Last modified</source>
        <translation>Ostatnia modyfikacja</translation>
    </message>
    <message>
        <source>Edit</source>
        <translation>Edytuj</translation>
    </message>
    <message>
        <source>New draft</source>
        <translation>Nowy szkic</translation>
    </message>
    <message>
        <source>This object is already being edited by yourself and others.
    You can either continue editing one of your drafts or you can create a new draft.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>This object is already being edited by someone else.
        You should either contact the person about their draft or create a new draft for your own use.</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>design/ezwebin/content/edit_languages</name>
    <message>
        <source>Existing languages</source>
        <translation>Dostępne wersje językowe</translation>
    </message>
    <message>
        <source>New languages</source>
        <translation>New languages</translation>
    </message>
    <message>
        <source>You do not have permission to edit the object in any available languages.</source>
        <translation>Nie posiadasz uprawnień do edycji obiektu w żadnym z dostępnych języków.</translation>
    </message>
    <message>
        <source>Edit</source>
        <translation>Edytuj</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation>Anuluj</translation>
    </message>
    <message>
        <source>Select the language you want to use when editing the object.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Select the language you want to add to the object.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Select the language the new translation will be based on.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Use an empty, untranslated draft</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>You do not have permission to create a translation in another language.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>However, you can select one of the following languages for editing.</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>design/ezwebin/content/history</name>
    <message>
        <source>Version not a draft</source>
        <translation>Wersja nie jest szkicem</translation>
    </message>
    <message>
        <source>Version not yours</source>
        <translation>Wersja nie należy do Ciebie</translation>
    </message>
    <message>
        <source>Unable to create new version</source>
        <translation>Nie można utworzyć nowej wersji</translation>
    </message>
    <message>
        <source>Version history limit has been exceeded and no archived version can be removed by the system.</source>
        <translation>Limit historii wersji został przekroczony. Żadna zarchiwizowana wersja nie może być usunięta przez system.</translation>
    </message>
    <message>
        <source>Versions for &lt;%object_name&gt; [%version_count]</source>
        <translation>Wersje dla &lt;%object_name&gt; [%version_count]</translation>
    </message>
    <message>
        <source>Version</source>
        <translation>Wersja</translation>
    </message>
    <message>
        <source>Status</source>
        <translation>Status</translation>
    </message>
    <message>
        <source>Creator</source>
        <translation>Twórca</translation>
    </message>
    <message>
        <source>Created</source>
        <translation>Utworzono</translation>
    </message>
    <message>
        <source>Modified</source>
        <translation>Zmodyfikowany</translation>
    </message>
    <message>
        <source>Select version #%version_number for removal.</source>
        <translation>Wybierz wersję #%version_number do usunięcia.</translation>
    </message>
    <message>
        <source>View the contents of version #%version_number. Translation: %translation.</source>
        <translation>Wyświetl zawartość wersji #%version_number. Wersja językowa: %translation.</translation>
    </message>
    <message>
        <source>Draft</source>
        <translation>Szkic</translation>
    </message>
    <message>
        <source>Published</source>
        <translation>Opublikowany</translation>
    </message>
    <message>
        <source>Pending</source>
        <translation>Oczekujący</translation>
    </message>
    <message>
        <source>Archived</source>
        <translation>Zarchiwizowany</translation>
    </message>
    <message>
        <source>Rejected</source>
        <translation>Odrzucony</translation>
    </message>
    <message>
        <source>Untouched draft</source>
        <translation>Nieużywany szkic</translation>
    </message>
    <message>
        <source>Create a copy of version #%version_number.</source>
        <translation>Utwórz kopię wersji #%version_number.</translation>
    </message>
    <message>
        <source>Edit the contents of version #%version_number.</source>
        <translation>Edytuj treść wersji #%version_number.</translation>
    </message>
    <message>
        <source>This object does not have any versions.</source>
        <translation>Ten obiekt nie ma żadnej wersji.</translation>
    </message>
    <message>
        <source>Remove selected</source>
        <translation>Usuń zaznaczone</translation>
    </message>
    <message>
        <source>Remove the selected versions from the object.</source>
        <translation>Usuń zaznaczone wersje obiektu.</translation>
    </message>
    <message>
        <source>Show differences</source>
        <translation>Pokaż różnice</translation>
    </message>
    <message>
        <source>Back</source>
        <translation>Wstecz</translation>
    </message>
    <message>
        <source>Published version</source>
        <translation>Opublikowany</translation>
    </message>
    <message>
        <source>Translations</source>
        <translation>Tłumaczenia</translation>
    </message>
    <message>
        <source>New drafts [%newerDraftCount]</source>
        <translation>Nowy szkic [%newerDraftCount]</translation>
    </message>
    <message>
        <source>This object does not have any drafts.</source>
        <translation>Ten obiekt nie posiada żadnych szkiców.</translation>
    </message>
    <message>
        <source>Differences between versions %oldVersion and %newVersion</source>
        <translation>Różnice pomiędzy wersjami %oldVersion i %newVersion</translation>
    </message>
    <message>
        <source>Old version</source>
        <translation>Stara wersja</translation>
    </message>
    <message>
        <source>Inline changes</source>
        <translation>Zmiany w linii</translation>
    </message>
    <message>
        <source>Block changes</source>
        <translation>Zmiany blokowe</translation>
    </message>
    <message>
        <source>New version</source>
        <translation>Nowa wersja</translation>
    </message>
    <message>
        <source>Back to history</source>
        <translation>Powrót do historii</translation>
    </message>
    <message>
        <source>Version %1 is not available for editing anymore. Only drafts can be edited.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>To edit this version, first create a copy of it.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Version %1 was not created by you. Only your own drafts can be edited.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>You can either change your version history settings in content.ini, remove draft versions or edit existing drafts.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Toggle selection</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Version #%version_number cannot be removed because it is either the published version of the object or because you do not have permission to remove it.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>There is no need to do a copies of untouched drafts.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>You cannot make copies of versions because you do not have permission to edit the object.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>You cannot edit the contents of version #%version_number either because it is not a draft or because you do not have permission to edit the object.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Modified translation</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>design/ezwebin/content/keyword</name>
    <message>
        <source>Keyword: %keyword</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Link</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Type</source>
        <translation type="unfinished">Typ</translation>
    </message>
</context>
<context>
    <name>design/ezwebin/content/search</name>
    <message>
        <source>Search</source>
        <translation>Szukaj</translation>
    </message>
    <message>
        <source>For more options try the %1Advanced search%2</source>
        <comment>The parameters are link start and end tags.</comment>
        <translation>Jeśli potrzebujesz więcej opcji, wykorzystaj %1Wyszukiwanie zaawansowane%2</translation>
    </message>
    <message>
        <source>Search tips</source>
        <translation>Porady</translation>
    </message>
    <message>
        <source>Check spelling of keywords.</source>
        <translation>Sprawdź poprawność pisowni.</translation>
    </message>
    <message>
        <source>Search for &quot;%1&quot; returned %2 matches</source>
        <translation>Wyszukiwanie &quot;%1&quot; zwróciło %2 wyników</translation>
    </message>
    <message>
        <source>The following words were excluded from the search:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>No results were found when searching for &quot;%1&quot;.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Try changing some keywords (eg, &quot;car&quot; instead of &quot;cars&quot;).</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Try searching with less specific keywords.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Reduce number of keywords to get more results.</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>design/ezwebin/content/tipafriend</name>
    <message>
        <source>Tip a friend</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>The message was sent.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Click here to return to the original page.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>The message was not sent.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>The message was not sent due to an unknown error. Please notify the site administrator about this error.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Please correct the following errors:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Your name</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Your email address</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Recipient&apos;s email address</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Comment</source>
        <translation type="unfinished">Komentarz</translation>
    </message>
    <message>
        <source>Send</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation type="unfinished">Anuluj</translation>
    </message>
</context>
<context>
    <name>design/ezwebin/content/view/versionview</name>
    <message>
        <source>Edit</source>
        <translation>Edytuj</translation>
    </message>
    <message>
        <source>Publish</source>
        <translation>Opublikuj</translation>
    </message>
    <message>
        <source>Manage versions</source>
        <translation type="unfinished">Zarządzaj wersjami</translation>
    </message>
</context>
<context>
    <name>design/ezwebin/edit/comment</name>
    <message>
        <source>Edit %1 - %2</source>
        <translation>Edycja %1 - %2</translation>
    </message>
    <message>
        <source>Send for publishing</source>
        <translation>Wyślij do publikacji</translation>
    </message>
    <message>
        <source>Discard</source>
        <translation>Odrzuć</translation>
    </message>
</context>
<context>
    <name>design/ezwebin/edit/file</name>
    <message>
        <source>Edit %1 - %2</source>
        <translation>Edycja %1 - %2</translation>
    </message>
    <message>
        <source>Send for publishing</source>
        <translation>Wyślij do publikacji</translation>
    </message>
    <message>
        <source>Discard</source>
        <translation>Odrzuć</translation>
    </message>
</context>
<context>
    <name>design/ezwebin/edit/forum_reply</name>
    <message>
        <source>Edit %1 - %2</source>
        <translation>Edycja %1 - %2</translation>
    </message>
    <message>
        <source>Send for publishing</source>
        <translation>Wyślij do publikacji</translation>
    </message>
    <message>
        <source>Discard</source>
        <translation>Odrzuć</translation>
    </message>
</context>
<context>
    <name>design/ezwebin/edit/forum_topic</name>
    <message>
        <source>Edit %1 - %2</source>
        <translation>Edycja %1 - %2</translation>
    </message>
    <message>
        <source>Send for publishing</source>
        <translation>Wyślij do publikacji</translation>
    </message>
    <message>
        <source>Discard</source>
        <translation>Odrzuć</translation>
    </message>
</context>
<context>
    <name>design/ezwebin/embed/forum</name>
    <message>
        <source>Latest from</source>
        <translation>Najnowsze z</translation>
    </message>
</context>
<context>
    <name>design/ezwebin/embed/poll</name>
    <message>
        <source>Vote</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>design/ezwebin/ezinfo/about</name>
    <message>
        <source>eZ Publish information: %version</source>
        <translation>Informacje o eZ Publish: %version</translation>
    </message>
    <message>
        <source>Licence</source>
        <translation>Licencja</translation>
    </message>
    <message>
        <source>Contributors</source>
        <translation>Dostawca</translation>
    </message>
    <message>
        <source>Copyright Notice</source>
        <translation>Nota Copyright</translation>
    </message>
    <message>
        <source>Third-Party Software</source>
        <translation>Zewnętrzne oprogramowanie</translation>
    </message>
    <message>
        <source>Extensions</source>
        <translation>Rozszerzenia</translation>
    </message>
    <message>
        <source>What is eZ Publish?</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>design/ezwebin/ezodf/browse_place</name>
    <message>
        <source>Choose document placement</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Please choose the placement for the OpenOffice.org object.

    Select the placements and click the %buttonname button.
    Using the recent and bookmark items for quick placement is also possible.
    Click on placement names to change the browse listing.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Select</source>
        <translation type="unfinished">Wybierz</translation>
    </message>
</context>
<context>
    <name>design/ezwebin/ezodf/export</name>
    <message>
        <source>OpenOffice.org export</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Export eZ publish content to OpenOffice.org</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Error</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Here you can export any eZ publish content object to an OpenOffice.org Writer document format.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Export Object</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>design/ezwebin/ezodf/import</name>
    <message>
        <source>Document is now imported</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>OpenOffice.org import</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>The object was imported as: %class_name</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Document imported as</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>The images are placed in the media and can be re-used.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Import another document</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Upload file</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Import OpenOffice.org document</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Replace document</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Import to</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>You can import OpenOffice.org Writer documents directly into eZ publish from this page. You are
asked where to place the document and eZ publish does the rest. The document is converted into
the appropriate class during the import, you get a notice about this after the import is done.
Images are placed in the media library so you can re-use them in other articles.</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>design/ezwebin/full/article</name>
    <message>
        <source>Comments</source>
        <translation>Komentarze</translation>
    </message>
    <message>
        <source>New comment</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>%login_link_startLog in%login_link_end or %create_link_startcreate a user account%create_link_end to comment.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Tip a friend</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>design/ezwebin/full/article_mainpage</name>
    <message>
        <source>Tip a friend</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>design/ezwebin/full/article_subpage</name>
    <message>
        <source>Tip a friend</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>design/ezwebin/full/blog_post</name>
    <message>
        <source>Tags:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Comments</source>
        <translation type="unfinished">Komentarze</translation>
    </message>
    <message>
        <source>%login_link_startLog in%login_link_end or %create_link_startcreate a user account%create_link_end to comment.</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>design/ezwebin/full/documentation_page</name>
    <message>
        <source>Created:</source>
        <translation>Utworzono</translation>
    </message>
    <message>
        <source>Modified:</source>
        <translation>Zmodyfikowany</translation>
    </message>
    <message>
        <source>Table of contents</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>design/ezwebin/full/event</name>
    <message>
        <source>Category</source>
        <translation>Kategoria</translation>
    </message>
</context>
<context>
    <name>design/ezwebin/full/event_view_calendar</name>
    <message>
        <source>Mon</source>
        <translation>Pon</translation>
    </message>
    <message>
        <source>Tue</source>
        <translation>Wt</translation>
    </message>
    <message>
        <source>Wed</source>
        <translation>Śr</translation>
    </message>
    <message>
        <source>Thu</source>
        <translation>Czw</translation>
    </message>
    <message>
        <source>Fri</source>
        <translation>Pt</translation>
    </message>
    <message>
        <source>Sat</source>
        <translation>So</translation>
    </message>
    <message>
        <source>Sun</source>
        <translation>N</translation>
    </message>
    <message>
        <source>Today</source>
        <translation>Dziś</translation>
    </message>
    <message>
        <source>Category</source>
        <translation>Kategoria</translation>
    </message>
</context>
<context>
    <name>design/ezwebin/full/event_view_program</name>
    <message>
        <source>Past events</source>
        <translation>Wydarzenia przeszłe</translation>
    </message>
    <message>
        <source>Future events</source>
        <translation>Wydarzenia przyszłe</translation>
    </message>
</context>
<context>
    <name>design/ezwebin/full/feedback_form</name>
    <message>
        <source>Send form</source>
        <translation>Wyślij</translation>
    </message>
</context>
<context>
    <name>design/ezwebin/full/forum</name>
    <message>
        <source>New topic</source>
        <translation>Nowy temat</translation>
    </message>
    <message>
        <source>Keep me updated</source>
        <translation>Powiadamiaj mnie o zmianach</translation>
    </message>
    <message>
        <source>You need to be logged in to get access to the forums. You can do so %login_link_start%here%login_link_end%</source>
        <translation>Musisz być zalogowany aby uzyskać dostęp do forum. Możesz się zalogować %login_link_start%tutaj%login_link_end%</translation>
    </message>
    <message>
        <source>Topic</source>
        <translation>Temat</translation>
    </message>
    <message>
        <source>Replies</source>
        <translation>Odpowiedzi</translation>
    </message>
    <message>
        <source>Author</source>
        <translation>Autor</translation>
    </message>
    <message>
        <source>Last reply</source>
        <translation>Ostatnia odpowiedź</translation>
    </message>
    <message>
        <source>Pages</source>
        <translation>Strony</translation>
    </message>
</context>
<context>
    <name>design/ezwebin/full/forum_reply</name>
    <message>
        <source>Message preview</source>
        <translation>Podgląd wiadomości</translation>
    </message>
    <message>
        <source>Author</source>
        <translation>Autor</translation>
    </message>
    <message>
        <source>Topic</source>
        <translation>Temat</translation>
    </message>
    <message>
        <source>Location</source>
        <translation>Umiejscowienie</translation>
    </message>
    <message>
        <source>Moderated by</source>
        <translation>Zmoderowany przez</translation>
    </message>
    <message>
        <source>Edit</source>
        <translation>Edytuj</translation>
    </message>
</context>
<context>
    <name>design/ezwebin/full/forum_topic</name>
    <message>
        <source>Previous topic</source>
        <translation>Poprzedni temat</translation>
    </message>
    <message>
        <source>Next topic</source>
        <translation>Następny temat</translation>
    </message>
    <message>
        <source>New reply</source>
        <translation>Nowa odpowiedź</translation>
    </message>
    <message>
        <source>Keep me updated</source>
        <translation>Powiadamiaj mnie o zmianach</translation>
    </message>
    <message>
        <source>You need to be logged in to get access to the forums. You can do so %login_link_start%here%login_link_end%</source>
        <translation>Musisz być zalogowany aby uzyskać dostęp do forum. Możesz się zalogować %login_link_start%tutaj%login_link_end%</translation>
    </message>
    <message>
        <source>Author</source>
        <translation>Autor</translation>
    </message>
    <message>
        <source>Message</source>
        <translation>Wiadomość</translation>
    </message>
    <message>
        <source>Location</source>
        <translation>Umiejscowienie</translation>
    </message>
    <message>
        <source>Moderated by</source>
        <translation>Zmoderowany przez</translation>
    </message>
    <message>
        <source>Edit</source>
        <translation>Edytuj</translation>
    </message>
    <message>
        <source>Remove</source>
        <translation>Usuń</translation>
    </message>
    <message>
        <source>Remove this item.</source>
        <translation>Usuń ten element.</translation>
    </message>
</context>
<context>
    <name>design/ezwebin/full/forums</name>
    <message>
        <source>Topics</source>
        <translation>Tematy</translation>
    </message>
    <message>
        <source>Posts</source>
        <translation>Posty</translation>
    </message>
    <message>
        <source>Last reply</source>
        <translation>Ostatnia odpowiedź</translation>
    </message>
</context>
<context>
    <name>design/ezwebin/full/gallery</name>
    <message>
        <source>View as slideshow</source>
        <translation>Wyświetl jako pokaz slajdów</translation>
    </message>
</context>
<context>
    <name>design/ezwebin/full/image</name>
    <message>
        <source>Previous image</source>
        <translation>Poprzedni obraz</translation>
    </message>
    <message>
        <source>Next image</source>
        <translation>Następny obraz</translation>
    </message>
</context>
<context>
    <name>design/ezwebin/full/multicalendar</name>
    <message>
        <source>Event</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Start date</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Category</source>
        <translation type="unfinished">Kategoria</translation>
    </message>
    <message>
        <source>Description</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>design/ezwebin/full/poll</name>
    <message>
        <source>Vote</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Result</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>design/ezwebin/full/product</name>
    <message>
        <source>Add to basket</source>
        <translation>Dodaj do koszyka</translation>
    </message>
    <message>
        <source>Add to wish list</source>
        <translation>Dodaj do listy życzeń</translation>
    </message>
    <message>
        <source>People who bought this also bought</source>
        <translation>Osoby, które to kupiły najczęściej kupowały również</translation>
    </message>
</context>
<context>
    <name>design/ezwebin/line/event</name>
    <message>
        <source>Category</source>
        <translation>Kategoria</translation>
    </message>
</context>
<context>
    <name>design/ezwebin/line/event_calendar</name>
    <message>
        <source>Next events</source>
        <translation>Następne wydarzenia</translation>
    </message>
</context>
<context>
    <name>design/ezwebin/line/flash</name>
    <message>
        <source>View flash</source>
        <translation>Pokaż animację flash</translation>
    </message>
</context>
<context>
    <name>design/ezwebin/line/forum</name>
    <message>
        <source>Last reply</source>
        <translation>Ostatnia odpowiedź</translation>
    </message>
    <message>
        <source>Enter forum</source>
        <translation>Wejdź na forum</translation>
    </message>
    <message>
        <source>Number of topics</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Number of posts</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>design/ezwebin/line/forum_reply</name>
    <message>
        <source>Reply to:</source>
        <translation>Odpowiedz do:</translation>
    </message>
</context>
<context>
    <name>design/ezwebin/line/poll</name>
    <message>
        <source>%count votes</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Vote</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>design/ezwebin/line/quicktime</name>
    <message>
        <source>View movie</source>
        <translation>Pokaż film</translation>
    </message>
</context>
<context>
    <name>design/ezwebin/line/real_video</name>
    <message>
        <source>View movie</source>
        <translation>Pokaż film</translation>
    </message>
</context>
<context>
    <name>design/ezwebin/line/windows_media</name>
    <message>
        <source>View movie</source>
        <translation>Pokaż film</translation>
    </message>
</context>
<context>
    <name>design/ezwebin/link</name>
    <message>
        <source>%sitetitle front page</source>
        <translation>%sitetitle strona główna</translation>
    </message>
    <message>
        <source>Search %sitetitle</source>
        <translation>Szukaj %sitetitle</translation>
    </message>
    <message>
        <source>Printable version</source>
        <translation>Wersja do druku</translation>
    </message>
</context>
<context>
    <name>design/ezwebin/node/removeobject</name>
    <message>
        <source>Are you sure you want to remove these items?</source>
        <translation>Czy jesteś pewien, że chcesz usunąć te obiekty?</translation>
    </message>
    <message>
        <source>%nodename and its %childcount children. %additionalwarning</source>
        <translation>%nodename i jego %childcount elementów podrzędnych. %additionalwarning</translation>
    </message>
    <message>
        <source>%nodename %additionalwarning</source>
        <translation>%nodename %additionalwarning</translation>
    </message>
    <message>
        <source>Move to trash</source>
        <translation>Wyrzuć do śmietnika</translation>
    </message>
    <message>
        <source>Note</source>
        <translation>Notatka</translation>
    </message>
    <message>
        <source>Confirm</source>
        <translation>Potwierdź</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation>Anuluj</translation>
    </message>
    <message>
        <source>If %trashname is checked, removed items can be found in the trash.</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>design/ezwebin/notification/addingresult</name>
    <message>
        <source>Add to my notifications</source>
        <translation></translation>
    </message>
    <message>
        <source>Notification for node &lt;%node_name&gt; already exists.</source>
        <translation></translation>
    </message>
    <message>
        <source>Notification for node &lt;%node_name&gt; was added successfully.</source>
        <translation></translation>
    </message>
    <message>
        <source>OK</source>
        <translation>OK</translation>
    </message>
</context>
<context>
    <name>design/ezwebin/notification/settings</name>
    <message>
        <source>Notification settings</source>
        <translation>Ustawienia powiadomień</translation>
    </message>
    <message>
        <source>Store</source>
        <translation>Zapisz</translation>
    </message>
</context>
<context>
    <name>design/ezwebin/pagelayout</name>
    <message>
        <source>Search</source>
        <translation>Szukaj</translation>
    </message>
</context>
<context>
    <name>design/ezwebin/parts/website_toolbar</name>
    <message>
        <source>Create here</source>
        <translation>Utwórz tutaj</translation>
    </message>
    <message>
        <source>Move</source>
        <translation>Przenieś</translation>
    </message>
    <message>
        <source>Remove</source>
        <translation>Usuń</translation>
    </message>
    <message>
        <source>Import</source>
        <translation>Importuj</translation>
    </message>
    <message>
        <source>Export</source>
        <translation>Exportuj</translation>
    </message>
    <message>
        <source>Replace</source>
        <translation>Zastąp</translation>
    </message>
    <message>
        <source>About</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Edit: %node_name [%class_name]</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Add locations</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Documentation</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>design/ezwebin/settings/edit</name>
    <message>
        <source>Node notification</source>
        <translation>Powiadomienia dotyczące węzłów</translation>
    </message>
    <message>
        <source>Name</source>
        <translation>Nazwa</translation>
    </message>
    <message>
        <source>Class</source>
        <translation>Klasa</translation>
    </message>
    <message>
        <source>Section</source>
        <translation>Sekcja</translation>
    </message>
    <message>
        <source>Select</source>
        <translation>Wybierz</translation>
    </message>
    <message>
        <source>Remove</source>
        <translation>Usuń</translation>
    </message>
</context>
<context>
    <name>design/ezwebin/shop/basket</name>
    <message>
        <source>Shopping basket</source>
        <translation>Twój koszyk</translation>
    </message>
    <message>
        <source>Account information</source>
        <translation>Informacje o koncie</translation>
    </message>
    <message>
        <source>Confirm order</source>
        <translation>Potwierdź zamówienie</translation>
    </message>
    <message>
        <source>Basket</source>
        <translation>Koszyk</translation>
    </message>
    <message>
        <source>VAT is unknown</source>
        <translation>Nieznany VAT</translation>
    </message>
    <message>
        <source>VAT percentage is not yet known for some of the items being purchased.</source>
        <translation>Stawka VAT dla niektórych z zamawianych produktów nie jest jeszcze znana.</translation>
    </message>
    <message>
        <source>This probably means that some information about you is not yet available and will be obtained during checkout.</source>
        <translation>Oznacza to prawdopodobnie, że niektóre z informacji o Tobie nie są jeszcze dostępne i będą udostępnione w trakcie potwierdzenia zamówienia.</translation>
    </message>
    <message>
        <source>Attempted to add object without price to basket.</source>
        <translation>Próba dodania do koszyka produktu bez wprowadzonej ceny.</translation>
    </message>
    <message>
        <source>Your payment was aborted.</source>
        <translation>Płatność została anulowana.</translation>
    </message>
    <message>
        <source>Count</source>
        <translation>Ilość</translation>
    </message>
    <message>
        <source>VAT</source>
        <translation>VAT</translation>
    </message>
    <message>
        <source>Price inc. VAT</source>
        <translation>Cena z VAT</translation>
    </message>
    <message>
        <source>Discount</source>
        <translation>Rabat</translation>
    </message>
    <message>
        <source>Update</source>
        <translation>Aktualizacja</translation>
    </message>
    <message>
        <source>Remove</source>
        <translation>Usuń</translation>
    </message>
    <message>
        <source>Selected options</source>
        <translation>Wybrane opcje</translation>
    </message>
    <message>
        <source>Shipping</source>
        <translation>Dostawa</translation>
    </message>
    <message>
        <source>Order total</source>
        <translation>Do zapłaty</translation>
    </message>
    <message>
        <source>Continue shopping</source>
        <translation>Kontynuuj zakupy</translation>
    </message>
    <message>
        <source>Checkout</source>
        <translation>Złóż zamówienie</translation>
    </message>
    <message>
        <source>The following items were removed from your basket because the products were changed.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Total price ex. VAT</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Total price inc. VAT</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Unknown</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Subtotal ex. VAT</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Subtotal inc. VAT</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>You have no products in your basket.</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>design/ezwebin/shop/confirmorder</name>
    <message>
        <source>Shopping basket</source>
        <translation>Twój koszyk</translation>
    </message>
    <message>
        <source>Account information</source>
        <translation>Informacje o koncie</translation>
    </message>
    <message>
        <source>Confirm order</source>
        <translation>Potwierdź zamówienie</translation>
    </message>
    <message>
        <source>Product items</source>
        <translation>Lista produktów</translation>
    </message>
    <message>
        <source>Count</source>
        <translation>Ilość</translation>
    </message>
    <message>
        <source>VAT</source>
        <translation>VAT</translation>
    </message>
    <message>
        <source>Price inc. VAT</source>
        <translation>Cena z VAT</translation>
    </message>
    <message>
        <source>Discount</source>
        <translation>Rabat</translation>
    </message>
    <message>
        <source>Selected options</source>
        <translation>Wybrane opcje</translation>
    </message>
    <message>
        <source>Order summary</source>
        <translation>Zamówienie łącznie</translation>
    </message>
    <message>
        <source>Subtotal of items</source>
        <translation>Suma pozycji</translation>
    </message>
    <message>
        <source>Order total</source>
        <translation>Do zapłaty</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation>Anuluj</translation>
    </message>
    <message>
        <source>Confirm</source>
        <translation>Potwierdź</translation>
    </message>
    <message>
        <source>Total price ex. VAT</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Total price inc. VAT</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>design/ezwebin/shop/customerorderview</name>
    <message>
        <source>Order list</source>
        <translation>Lista zamówień</translation>
    </message>
    <message>
        <source>ID</source>
        <translation>ID</translation>
    </message>
    <message>
        <source>Date</source>
        <translation>Data</translation>
    </message>
    <message>
        <source>Total ex. VAT</source>
        <translation>Suma bez VAT</translation>
    </message>
    <message>
        <source>Total inc. VAT</source>
        <translation>Suma z VAT</translation>
    </message>
    <message>
        <source>Purchase list</source>
        <translation>Lista zakupów</translation>
    </message>
    <message>
        <source>Product</source>
        <translation>Produkt</translation>
    </message>
    <message>
        <source>Amount</source>
        <translation>Kwota</translation>
    </message>
    <message>
        <source>Customer information</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>design/ezwebin/shop/orderlist</name>
    <message>
        <source>Order list</source>
        <translation type="unfinished">Lista zamówień</translation>
    </message>
    <message>
        <source>Sort result by</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Order time</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>User name</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Order ID</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Ascending</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Sort ascending</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Descending</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Sort descending</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Sort</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>ID</source>
        <translation type="unfinished">ID</translation>
    </message>
    <message>
        <source>Date</source>
        <translation type="unfinished">Data</translation>
    </message>
    <message>
        <source>Customer</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Total ex. VAT</source>
        <translation type="unfinished">Suma bez VAT</translation>
    </message>
    <message>
        <source>Total inc. VAT</source>
        <translation type="unfinished">Suma z VAT</translation>
    </message>
    <message>
        <source>The order list is empty</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Archive</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>design/ezwebin/shop/orderview</name>
    <message>
        <source>Order %order_id [%order_status]</source>
        <translation>Zamówienie %order_id [%order_status]</translation>
    </message>
    <message>
        <source>Product items</source>
        <translation>Lista produktów</translation>
    </message>
    <message>
        <source>Product</source>
        <translation>Produkt</translation>
    </message>
    <message>
        <source>Count</source>
        <translation>Ilość</translation>
    </message>
    <message>
        <source>VAT</source>
        <translation>VAT</translation>
    </message>
    <message>
        <source>Price inc. VAT</source>
        <translation>Cena z VAT</translation>
    </message>
    <message>
        <source>Discount</source>
        <translation>Rabat</translation>
    </message>
    <message>
        <source>Order summary</source>
        <translation>Zamówienie łącznie</translation>
    </message>
    <message>
        <source>Summary</source>
        <translation>Podsumowanie</translation>
    </message>
    <message>
        <source>Subtotal of items</source>
        <translation>Suma pozycji</translation>
    </message>
    <message>
        <source>Order total</source>
        <translation>Do zapłaty</translation>
    </message>
    <message>
        <source>Order history</source>
        <translation>Historia zamówienia</translation>
    </message>
    <message>
        <source>Total price ex. VAT</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Total price inc. VAT</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>design/ezwebin/shop/userregister</name>
    <message>
        <source>Shopping basket</source>
        <translation>Twój koszyk</translation>
    </message>
    <message>
        <source>Account information</source>
        <translation>Informacje o koncie</translation>
    </message>
    <message>
        <source>Confirm order</source>
        <translation>Potwierdź zamówienie</translation>
    </message>
    <message>
        <source>Your account information</source>
        <translation>Informacje o Twoim koncie</translation>
    </message>
    <message>
        <source>First name</source>
        <translation>Imię</translation>
    </message>
    <message>
        <source>Last name</source>
        <translation>Nazwisko</translation>
    </message>
    <message>
        <source>Company</source>
        <translation>Nazwa firmy</translation>
    </message>
    <message>
        <source>Street</source>
        <translation>Ulica</translation>
    </message>
    <message>
        <source>Zip</source>
        <translation>Kod pocztowy</translation>
    </message>
    <message>
        <source>Place</source>
        <translation>Miejsce</translation>
    </message>
    <message>
        <source>State</source>
        <translation>Województwo</translation>
    </message>
    <message>
        <source>Country</source>
        <translation>Kraj</translation>
    </message>
    <message>
        <source>Comment</source>
        <translation>Komentarz</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation>Anuluj</translation>
    </message>
    <message>
        <source>Continue</source>
        <translation>Kontynuuj</translation>
    </message>
    <message>
        <source>All fields marked with * must be filled in.</source>
        <translation>Wszystkie pola oznaczone * muszą zostać wypełnione.</translation>
    </message>
    <message>
        <source>Input did not validate. All fields marked with * must be filled in.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Email</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>design/ezwebin/shop/wishlist</name>
    <message>
        <source>Wish list</source>
        <translation>Lista życzeń</translation>
    </message>
    <message>
        <source>Product</source>
        <translation>Produkt</translation>
    </message>
    <message>
        <source>Count</source>
        <translation>Ilość</translation>
    </message>
    <message>
        <source>Selected options</source>
        <translation>Wybrane opcje</translation>
    </message>
    <message>
        <source>Store</source>
        <translation>Zapisz</translation>
    </message>
    <message>
        <source>Remove items</source>
        <translation>Usuń pozycje</translation>
    </message>
    <message>
        <source>Empty wish list</source>
        <translation>Wyczyść listę życzeń</translation>
    </message>
</context>
<context>
    <name>design/ezwebin/simplified_treemenu/show_simplified_menu</name>
    <message>
        <source>Fold/Unfold</source>
        <translation>Zwiń/Rozwiń</translation>
    </message>
    <message>
        <source>Node ID: %node_id Visibility: %visibility</source>
        <translation>ID węzła: %node_id Widoczność: %visibility</translation>
    </message>
</context>
<context>
    <name>design/ezwebin/user/activate</name>
    <message>
        <source>Activate account</source>
        <translation>Aktywuj konto</translation>
    </message>
    <message>
        <source>Your account is now activated.</source>
        <translation>Twoje konto zostało aktywowane.</translation>
    </message>
    <message>
        <source>Sorry, the key submitted was not a valid key. Account was not activated.</source>
        <translation>Niestety wprowadzony klucz nie jest właściwy. Konto nie zostało aktywowane.</translation>
    </message>
    <message>
        <source>Your account is already active.</source>
        <translation>Twoje konto jest już aktywne.</translation>
    </message>
    <message>
        <source>OK</source>
        <translation>OK</translation>
    </message>
</context>
<context>
    <name>design/ezwebin/user/edit</name>
    <message>
        <source>User profile</source>
        <translation>Profil użytkownika</translation>
    </message>
    <message>
        <source>Username</source>
        <translation>Nazwa użytkownika</translation>
    </message>
    <message>
        <source>Name</source>
        <translation>Nazwa</translation>
    </message>
    <message>
        <source>My drafts</source>
        <translation>Moje szkice</translation>
    </message>
    <message>
        <source>My orders</source>
        <translation>Moje zamówienia</translation>
    </message>
    <message>
        <source>My notification settings</source>
        <translation>Ustawienia moich powiadomień</translation>
    </message>
    <message>
        <source>My wish list</source>
        <translation>Moja lista życzeń</translation>
    </message>
    <message>
        <source>Edit profile</source>
        <translation>Edycja profilu</translation>
    </message>
    <message>
        <source>Change password</source>
        <translation>Zmień hasło</translation>
    </message>
    <message>
        <source>Email</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>design/ezwebin/user/forgotpassword</name>
    <message>
        <source>Password was successfully generated and sent to: %1</source>
        <translation>Hasło zostało utworzone i wysłane na adres %1</translation>
    </message>
    <message>
        <source>The key is invalid or has been used. </source>
        <translation>Klucz jest niewłaściwy lub został już użyty.</translation>
    </message>
    <message>
        <source>Have you forgotten your password?</source>
        <translation>Czy zapomniałeś swoje hasło?</translation>
    </message>
    <message>
        <source>Generate new password</source>
        <translation>Wygeneruj nowe hasło</translation>
    </message>
    <message>
        <source>An email has been sent to the following address: %1. It contains a link you need to click so that we can confirm that the correct user has received the new password.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>There is no registered user with that email address.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>If you have forgotten your password, enter your email address and we will create a new password and send it to you.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Email</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>design/ezwebin/user/login</name>
    <message>
        <source>Login</source>
        <translation>Logowanie</translation>
    </message>
    <message>
        <source>Could not login</source>
        <translation>Nie można zalogować</translation>
    </message>
    <message>
        <source>A valid username and password is required to login.</source>
        <translation>Do zalogowania wymagana jest prawidłowa nazwa użytkownika z hasłem.</translation>
    </message>
    <message>
        <source>Access not allowed</source>
        <translation>Dostęp wzbroniony</translation>
    </message>
    <message>
        <source>You are not allowed to access %1.</source>
        <translation>Nie masz dostępu do %1.</translation>
    </message>
    <message>
        <source>Username</source>
        <comment>User name</comment>
        <translation>Nazwa użytkownika</translation>
    </message>
    <message>
        <source>Password</source>
        <translation>Hasło</translation>
    </message>
    <message>
        <source>Remember me</source>
        <translation>Zapamiętaj mnie</translation>
    </message>
    <message>
        <source>Login</source>
        <comment>Button</comment>
        <translation>Zaloguj</translation>
    </message>
    <message>
        <source>Forgot your password?</source>
        <translation>Nie pamiętasz hasła?</translation>
    </message>
    <message>
        <source>Log in to the eZ Publish Administration Interface</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Sign up</source>
        <comment>Button</comment>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>design/ezwebin/user/password</name>
    <message>
        <source>Change password for user</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Please retype your old password.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Password didn&apos;t match, please retype your new password.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Password successfully updated.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Old password</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>New password</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Retype password</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>OK</source>
        <translation type="unfinished">OK</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation type="unfinished">Anuluj</translation>
    </message>
</context>
<context>
    <name>design/ezwebin/user/register</name>
    <message>
        <source>Register user</source>
        <translation>Zarejestruj nowego użytkownika</translation>
    </message>
    <message>
        <source>Input did not validate</source>
        <translation>Błędnie wprowadzona wartość</translation>
    </message>
    <message>
        <source>Input was stored successfully</source>
        <translation>Podane dane zostały przyjęte</translation>
    </message>
    <message>
        <source>Register</source>
        <translation>Rejestruj</translation>
    </message>
    <message>
        <source>Discard</source>
        <translation>Odrzuć</translation>
    </message>
    <message>
        <source>Unable to register new user</source>
        <translation>Nie można zarejestrować nowego użytkownika</translation>
    </message>
    <message>
        <source>Back</source>
        <translation>Wstecz</translation>
    </message>
</context>
<context>
    <name>design/ezwebin/user/success</name>
    <message>
        <source>User registered</source>
        <translation>Użytkownik zarejestrowany</translation>
    </message>
    <message>
        <source>Your account was successfully created.</source>
        <translation>Twoje konto zostało pomyślnie utworzone.</translation>
    </message>
    <message>
        <source>Your account was successfully created. An email will be sent to the specified address. Follow the instructions in that email to activate your account.</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>design/ezwebin/view/ezbinaryfile</name>
    <message>
        <source>The file could not be found.</source>
        <translation>Plik nie został odnaleziony.</translation>
    </message>
</context>
<context>
    <name>design/ezwebin/view/ezmedia</name>
    <message>
        <source>No %link_startFlash player%link_end avaliable!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>No media file is available.</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>design/ezwebin/view/ezprice</name>
    <message>
        <source>Price</source>
        <translation>Cena</translation>
    </message>
    <message>
        <source>Your price</source>
        <translation>Twoja cena</translation>
    </message>
    <message>
        <source>You save</source>
        <translation>Zaoszczędzisz</translation>
    </message>
</context>
<context>
    <name>design/ezwebin/view/sitemap</name>
    <message>
        <source>Site map</source>
        <translation>Mapa serwisu</translation>
    </message>
</context>
</TS>
