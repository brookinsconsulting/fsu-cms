<div class="content-view-line">
    <div class="class-infobox">
        <div class="attribute-header">
            <h2>{attribute_view_gui attribute=$node.object.data_map.header}</h2>
        </div>

        <div class="attribute-content">
      {if $node.object.data_map.image.has_content}

            <div class="attribute-image"> 
				{attribute_view_gui attribute=$node.object.data_map.image href=$node.object.data_map.image_url.data_text image_class=articlethumbnail} 
			</div>
			{/if}            
			{attribute_view_gui attribute=$node.object.data_map.content}
      {if $node.object.data_map.url.has_content}
            
			<div class="attribute-url">
				<p>{attribute_view_gui attribute=$node.object.data_map.url}</p>
			</div>
			{/if}

{if $node.data_map.source.content.relation_browse|count} 
{def	$selected_id_array=$node.data_map.sort_by.content 
	$sort_string='priority'
	$new_date=true()
	$todaystart=maketime( '00','00', '00', currentdate()|datetime( custom, '%n'), currentdate()|datetime( custom, '%j'), currentdate()|datetime( custom, '%Y') )} 
{section var=Options loop=$node.data_map.sort_by.class_content.options} 
{section-exclude match=$selected_id_array|contains( $Options.item.id )|not} 
{set sort_string = $Options.item.name|wash( xhtml )} 
{/section} 
{let source_children=fetch(content, $node.data_map.search_type.data_int|choose('list', 'tree'), hash(parent_node_id, $node.data_map.source.content.relation_browse.0.node_id, sort_by, array( array( $sort_string, false() ) ), limit, $node.data_map.max_child.has_content|choose(99,$node.data_map.max_child.data_int|wash), class_filter_type, 'exclude', class_filter_array, array('quicklink', 'page_note', 'comment') ))} 
{section var=source_child loop=$source_children last-value}
{if eq($source_child.class_identifier, 'event')}
{set new_date=and(ne($source_child.data_map.from_time.data_int, $source_child.last.data_map.from_time.data_int),gt($source_child.data_map.from_time.data_int,$todaystart))|choose(false(),true())}
<div class='event'>{content_view_gui view=event_home_view content_object=$source_child.object new_date=$new_date}</div>
{else}
<h5><a href={$source_child.url_alias|ezurl}>{$source_child.name|wash}</a></h5> 
{if is_set($source_child.data_map.short_description)}{$source_child.data_map.short_description.content.output.output_text}{/if}
{/if}
{/section} 
{/let} 
{/if} 
{undef}
			
			{if or( $node.object.can_edit, $node.object.can_remove)}
			<div class="controls">
			<form action={"/content/action"|ezurl} method="post">
				{if $node.object.can_edit}
					<input type="image" name="EditButton" src={"edit_ico.png"|ezimage} alt="Edit" />
					<input type="hidden" name="ContentObjectLanguageCode" value="{$node.object.current_language}" />
				{/if}
				 
				{if $node.object.can_remove}
					<input type="image" name="ActionRemove" src={"trash.png"|ezimage} alt="Remove" />
				{/if}
				  	<input type="hidden" name="ContentObjectID" value="{$node.object.id}" />
  					<input type="hidden" name="NodeID" value="{$node.node_id}" />
  					<input type="hidden" name="ContentNodeID" value="{$node.node_id}" />
			</form>
			</div>
			{/if}
			
        </div>
    </div>
</div>
