<div class="view-embed">
    <div class="content-media">
    {let attribute=$object.data_map.file}
        <object width="{$attribute.content.width}" height="{$attribute.content.height}">
        <param name="movie" value={concat("content/download/",$attribute.contentobject_id,"/",$attribute.content.contentobject_attribute_id,"/",$attribute.content.original_filename)|ezurl} />
        <param name="controller" value="{section show=$attribute.content.has_controller}true{section-else}false{/section}" />
        <param name="autoplay" value="{section show=$attribute.content.is_autoplay}true{section-else}false{/section}" />
        <param name="loop" value="{section show=$attribute.content.is_loop}true{section-else}false{/section}" />
        <embed src={concat("content/download/",$attribute.contentobject_id,"/",$attribute.content.contentobject_attribute_id,"/",$attribute.content.original_filename)|ezurl}
               type="video/quicktime"
               pluginspage="{$attribute.content.pluginspage}"
               width="{$attribute.content.width}" height="{$attribute.content.height}" autoplay="{section show=$attribute.content.is_autoplay}true{section-else}false{/section}"
               loop="{section show=$attribute.content.is_loop}true{section-else}false{/section}" controller="{section show=$attribute.content.has_controller}true{section-else}false{/section}" >
        </embed>
        </object>
    {/let}
    </div>
</div>