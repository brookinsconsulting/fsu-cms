<?php
//
// Definition of eZNetClientInfo class
//
// Created on: <21-Sep-2006 14:12:49 hovik>
//
// Copyright (C) 1999-2005 eZ systems as. All rights reserved.
//
// This source file is part of the eZ publish (tm) Open Source Content
// Management System.
//
// This file may be distributed and/or modified under the terms of the
// "GNU General Public License" version 2 as published by the Free
// Software Foundation and appearing in the file LICENSE included in
// the packaging of this file.
//
// Licencees holding a valid "eZ publish professional licence" version 2
// may use this file in accordance with the "eZ publish professional licence"
// version 2 Agreement provided with the Software.
//
// This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING
// THE WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR
// PURPOSE.
//
// The "eZ publish professional licence" version 2 is available at
// http://ez.no/ez_publish/licences/professional/ and in the file
// PROFESSIONAL_LICENCE included in the packaging of this file.
// For pricing of this licence please contact us via e-mail to licence@ez.no.
// Further contact information is available at http://ez.no/company/contact/.
//
// The "GNU General Public License" (GPL) is available at
// http://www.gnu.org/copyleft/gpl.html.
//
// Contact licence@ez.no if any conditions of this licencing isn't clear to
// you.
//

/*! \file eznetclientinfo.php
*/

/*!
  \class eZNetClientInfo eznetclientinfo.php
  \brief The class eZNetClientInfo does

  - Checks if DB Update is needed.
    - Creates cronjob lock if DB update is on progress. ( uses ezsite_data table for this operation )

  - Stores version information about the network client.
*/

class eZNetClientInfo
{
    /*!
     Constructor
    */
    function eZNetClientInfo()
    {
        $this->DB = eZDB::instance();
    }

    /*!
     \static

     Get instance of the eZNetClientInfo class.
    */
    static function instance()
    {
        return new eZNetClientInfo();
    }

    /*!
     Check if version is validated. This will check the installed Network version with the
     one indicated in the database. In case there is a missmatch, it'll check the version path
     for required DB updates, and perform them.

     While performing the updates, this function will act as a cronjob lock. If locked, the
     function will return false.

     If the lock has been present for more than 4 hours, the lock will be removed.

     \return true if everything is ok.
             false if locked.
    */
    function validate()
    {
        if ( $this->locked() )
        {
            return false;
        }

        if ( !$this->correctVersion() )
        {
            if ( !$this->lock() )
            {
                eZNetUtils::log( 'ERROR : Could not create lock.' );
                return false;
            }

            eZNetUtils::log( 'Incorrect version. ' . $this->currentVersion() . ' should be: ' . max( array_keys( $this->VersionArray ) ) );

            $this->DB->begin();
            $this->upgrade();
            $this->updateVersion();
            $this->unLock();
            $this->DB->commit();

            eZNetUtils::log( 'Updated to version: ' . max( array_keys( $this->VersionArray ) ) );
        }

        return true;
    }

    /*!
     Update Network version
    */
    function updateVersion()
    {
        if ( $this->currentVersion() )
        {
            $sql = 'UPDATE ezsite_data
                    SET value=\'' . $this->DB->escapeString( max( array_keys( $this->VersionArray ) ) ) . '\'
                    WHERE name=\'' . $this->DB->escapeString( eZNetClientInfo::VERSION_NAME ) . '\'';
        }
        else
        {
            $sql = 'INSERT INTO ezsite_data ( name, value )
                    VALUES ( \'' . $this->DB->escapeString( eZNetClientInfo::VERSION_NAME ) . '\',
                             \'' . $this->DB->escapeString( max( array_keys( $this->VersionArray ) ) ) . '\' )';
        }

        $this->DB->query( $sql );
    }

    /*!
     \public

     Checks if current execution is the first run after an update.

     \return true if DB has been cleared recently.
    */
    function isFirstRun()
    {
        $sql = 'SELECT count(*) count FROM ezsite_data
                WHERE name=\'' . $this->DB->escapeString( eZNetClientInfo::EZNET_DB ) . '\'';

        $result = $this->DB->arrayQuery( $sql );

        return $result[0]['count'] > 0;
    }

    /*!
     \private

     Stores a value to DB it means DB has been cleared

    */
    function setEmptyDB()
    {
        // If empty DB value already exists
        if ( $this->isFirstRun() )
        {
            return;
        }

        $this->DB->begin();
        $sql = 'INSERT INTO ezsite_data ( name, value )
                VALUES ( \'' . $this->DB->escapeString( eZNetClientInfo::EZNET_DB ) . '\',
                         \'' . $this->DB->escapeString( eZNetClientInfo::EMPTY_DB ) . '\')';

        $this->DB->query( $sql );
        $this->DB->commit();
    }

    /*!
     \public

     Drops notice from DB. It means that the execution is not already the first.

    */
    function dropFirstRun()
    {
        $this->DB->begin();
        $sql = 'DELETE FROM ezsite_data
                WHERE name=\'' . $this->DB->escapeString( eZNetClientInfo::EZNET_DB ) . '\'';

        $this->DB->query( $sql );
        $this->DB->commit();
    }

    /*!
     Run functions required to upgrade to latest version.
     */
    function upgrade()
    {
        foreach( $this->upgradeFunctionList() as $function )
        {
            call_user_func( array( $this, $function ) );
        }
    }

    /*!
     Generate list of functions required to run to upgrade version.

     \return array of functions to run
    */
    function upgradeFunctionList()
    {
        $functionList = array();
        $currentVersion = $this->currentVersion();

        foreach( $this->VersionArray as $version => $upgradeDefinition )
        {
            if ( $version <= $currentVersion )
            {
                continue;
            }
            if ( isset( $upgradeDefinition['functionList'] ) )
            {
                $functionList = array_merge( $functionList, $upgradeDefinition['functionList'] );
            }
        }

        return $functionList;
    }

    /*!
     Check that the correct version is installed.

     \return true if the version is correct, false if not.
    */
    function correctVersion()
    {
        $currentVersion = $this->currentVersion();

        if ( !$currentVersion )
        {
            return false;
        }

        $correctVersion = max( array_keys( $this->VersionArray ) );
        return ( $currentVersion == $correctVersion );
    }

    /*!
     Get currect version number

     \return current version, false if not set.
    */
    function currentVersion()
    {
        $sql = 'SELECT value FROM ezsite_data
                WHERE name=\'' . $this->DB->escapeString( eZNetClientInfo::VERSION_NAME ) . '\'';
        $resultSet = $this->DB->arrayQuery( $sql );

        if ( count( $resultSet ) == 0 )
        {
            return false;
        }

        return $resultSet[0]['value'];
    }

    /*!
     Check if installation is locked. If the installation is locked, check if the lock is oder than
     the specified lock time.

     \retrun true if locked, false if not locked.
     */
    function locked()
    {
        $sql = 'SELECT value FROM ezsite_data
                WHERE name=\'' . $this->DB->escapeString( eZNetClientInfo::LOCK_NAME ) . '\'';
        $resultSet = $this->DB->arrayQuery( $sql );

        if ( count( $resultSet ) == 0 )
        {
            return false;
        }

        if ( $resultSet[0]['value'] + eZNetClientInfo::LOCK_TIMEOUT < mktime() )
        {
            return $this->unLock();
        }

        return true;
    }

    /*!
     Remove lock.

     \return true if successfull.
     */
    function unLock()
    {
        $this->DB->begin();
        $sql = 'DELETE FROM ezsite_data WHERE name=\'' . $this->DB->escapeString( eZNetClientInfo::LOCK_NAME ) . '\'';
        $this->DB->query( $sql );
        return $this->DB->commit();
    }

    /*!
     Create lock.

     \return true if successfull.
    */
    function lock()
    {
        $this->DB->begin();
        $sql = 'INSERT INTO ezsite_data ( name, value )
                VALUES ( \'' . $this->DB->escapeString( eZNetClientInfo::LOCK_NAME ) . '\', \'' . mktime() . '\' )';
        $this->DB->query( $sql );
        return $this->DB->commit();
    }

    /*!
     Update DB Schema
    */
    function updateSchema()
    {
        $schemaDir = eZExtension::baseDirectory() . '/' . eZINI::instance( 'network.ini' )->variable( 'NetworkSettings', 'ExtensionPath' ) . '/scripts/';
        $schemaFile = 'db.sql';
        $dbContents = eZFile::getContents( $schemaDir . '/' . $schemaFile );
        $schema = unserialize( eZNetCrypt::decrypt( $dbContents ) );
        eZNetUtils::createTable( $schema );
    }

    /*!
     Clear DB
    */
    function clearDB()
    {
        $clearDBArray = array( 'ezx_ezpnet_branch',
                               'ezx_ezpnet_installation',
                               'ezx_ezpnet_mon_group',
                               'ezx_ezpnet_mon_item',
                               'ezx_ezpnet_mon_result',
                               'ezx_ezpnet_mon_value',
                               'ezx_ezpnet_soap_log',
                               'ezx_ezpnet_storage',
                               'ezx_ezpnet_patch',
                               'ezx_ezpnet_large_store',
                               'ezx_ezpnet_patch_item',
                               'ezx_ezpnet_module_branch',
                               'ezx_ezpnet_patch_sql_st',
                               'ezx_ezpnet_module_inst',
                               'ezx_ezpnet_module_patch',
                               'ezx_ezpnet_mod_patch_item' );

        $this->DB->begin();
        foreach( $clearDBArray as $dbName )
        {
            $sql = 'DELETE FROM ' . $dbName;
            $this->DB->query( $sql );
        }

        // We notice that it will be the first run after clearing of DB.
        $this->setEmptyDB();

        $this->DB->commit();
    }

    var $DB = null;
    var $VersionArray = array( '0.1' => array( 'functionList' => array( 'updateSchema', 'clearDB' ) ),
                               '1.0.1' => array( 'functionList' => array( 'clearDB' ) ),
                               '1.1.0' => array( 'functionList' => array( 'updateSchema', 'clearDB' ) ),
                               '1.1.1' => array( 'functionList' => array( 'updateSchema', 'clearDB' ) ),
                               );

    /// Public constants
    const LOCK_TIMEOUT = 14400;
    const VERSION_NAME = 'ezxnet_version';
    const LOCK_NAME = 'ezxnet_lock';
    const EZNET_DB = 'ezxnet_db';
    const EMPTY_DB = 'empty';
}

?>
