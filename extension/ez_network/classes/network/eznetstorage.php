<?php
//
// Definition of eZNetStorage class
//
// Created on: <29-Sep-2005 10:01:32 hovik>
//
// Copyright (C) 1999-2005 eZ systems as. All rights reserved.
//
// This source file is part of the eZ publish (tm) Open Source Content
// Management System.
//
// This file may be distributed and/or modified under the terms of the
// "GNU General Public License" version 2 as published by the Free
// Software Foundation and appearing in the file LICENSE included in
// the packaging of this file.
//
// Licencees holding a valid "eZ publish professional licence" version 2
// may use this file in accordance with the "eZ publish professional licence"
// version 2 Agreement provided with the Software.
//
// This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING
// THE WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR
// PURPOSE.
//
// The "eZ publish professional licence" version 2 is available at
// http://ez.no/ez_publish/licences/professional/ and in the file
// PROFESSIONAL_LICENCE included in the packaging of this file.
// For pricing of this licence please contact us via e-mail to licence@ez.no.
// Further contact information is available at http://ez.no/company/contact/.
//
// The "GNU General Public License" (GPL) is available at
// http://www.gnu.org/copyleft/gpl.html.
//
// Contact licence@ez.no if any conditions of this licencing isn't clear to
// you.
//

/*! \file eznetstorage.php
*/

/*!
  \class eZNetStorage eznetstorage.php
  \brief The class eZNetStorage does

*/


class eZNetStorage extends eZPersistentObject
{
    /*!
     Constructor
    */
    function eZNetStorage($row )
    {
        $this->eZPersistentObject( $row );
    }

    static function definition()
    {
        return array( "fields" => array( "id" => array( 'name' => 'ID',
                                                        'datatype' => 'string',
                                                        'default' => '',
                                                        'required' => true ),
                                         "id2" => array( 'name' => 'ID2',
                                                         'datatype' => 'string',
                                                         'default' => '',
                                                         'required' => true ),
                                         "value" => array( 'name' => 'Value',
                                                           'datatype' => 'longtext',
                                                           'default' => '',
                                                           'required' => true ) ),
                      "keys" => array( "id" ),
                      "function_attributes" => array(),
//                       "increment_key" => "id",
                      "class_name" => "eZNetStorage",
                      "sort" => array( "id" => "asc" ),
                      "name" => "ezx_ezpnet_storage" );
    }

    /*!
     \static
     Get value based on key

     \param Key
     \param Additional Key.
     \return value
    */
    function get( $key, $key2 = false )
    {
        if ( is_array( $key ) )
        {
            $key = serialize( $key );
        }
        $key = md5( $key );

        $matchArray = array( 'id' => $key );

        if ( $key2 === false )
        {
            if ( is_array( $key2 ) )
            {
                $key2 = serialize( $key2 );
            }
            $key2 = md5( $key2 );
            $matchArray['id2'] = $key2;
        }

        $result = eZPersistentObject::fetchObject( eZNetStorage::definition(),
                                                   array( 'value' ),
                                                   $matchArray,
                                                   false );
        if ( !$result )
        {
            return false;
        }

        return unserialize( eZNetCrypt::decrypt( $result['value'] ) );
    }

    /*!
     \static
     Get value array based on key. Will return all instances stored by key, but with different key2.

     \param Key.
     \return Array of values
    */
    function getArray( $key  )
    {
        if ( is_array( $key ) )
        {
            $key = serialize( $key );
        }
        $key = md5( $key );
        $matchArray = array( 'id' => $key );

        $resultArray = eZPersistentObject::fetchObjectList( eZNetStorage::definition(),
                                                            array( 'value' ),
                                                            $matchArray );
        if ( !$resultArray ||
             !count( $resultArray ) )
        {
            return false;
        }

        $returnArray = array();

        foreach( $resultArray as $result )
        {
            $returnArray[] = unserialize( eZNetCrypt::decrypt( $result['value'] ) );
        }

        return $returnArray;
    }

    /*!
     \static
     Set a value to the DB storage.

     \param key
     \param value
     \param key2 ( additional key )
    */
    function set( $key, $value, $key2 = '' )
    {
        if ( is_array( $key ) )
        {
            $key = serialize( $key );
        }
        if ( is_array( $key2 ) )
        {
            $key2 = serialize( $key2 );
        }

        $key = md5( $key );
        $key2 = md5( $key2 );

        $db = eZDB::instance();

        // Need to check for type of db instance.
        // If it's oracle we should use special SQL to store a big text to clob field
        // due to via simple sql insert it's not allowed to pass strings more than 4000 characters.
        // Simple solution: need to use binded variables.
        if ( $db->databaseName() == 'oracle' )
        {
            $content = eZNetCrypt::encrypt( serialize( $value ) );

            $def = eZNetStorage::definition();
            $filedList = implode( ", ", array_keys( $def['fields'] ) );
            $values = "'$key', '$key2', :value";
            $clobData = array( 'value' => $content );

            eZNetUtils::insertOracleCLOBData( $db->DBConnection, $def['name'], $filedList, $values, $clobData );
        }
        else
        {
            $storage = eZPersistentObject::fetchObject( eZNetStorage::definition(),
                                                        null,
                                                        array( 'id' => $key,
                                                               'id2' => $key2 ) );
            if ( !$storage )
            {
                $storage = new eZNetStorage( array( 'id' => $key,
                                                    'id2' => $key2 ) );
            }

            $storage->setAttribute( 'value', eZNetCrypt::encrypt( serialize( $value ) ) );
            $storage->store();
        }
    }
}

?>
