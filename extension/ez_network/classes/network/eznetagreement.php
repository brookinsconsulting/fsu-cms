<?php
//
// Definition of eZNetAgreement class
//
// Created on: <27-Mar-2006 16:51:09 hovik>
//
// Copyright (C) 1999-2005 eZ systems as. All rights reserved.
//
// This source file is part of the eZ publish (tm) Open Source Content
// Management System.
//
// This file may be distributed and/or modified under the terms of the
// "GNU General Public License" version 2 as published by the Free
// Software Foundation and appearing in the file LICENSE included in
// the packaging of this file.
//
// Licencees holding a valid "eZ publish professional licence" version 2
// may use this file in accordance with the "eZ publish professional licence"
// version 2 Agreement provided with the Software.
//
// This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING
// THE WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR
// PURPOSE.
//
// The "eZ publish professional licence" version 2 is available at
// http://ez.no/ez_publish/licences/professional/ and in the file
// PROFESSIONAL_LICENCE included in the packaging of this file.
// For pricing of this licence please contact us via e-mail to licence@ez.no.
// Further contact information is available at http://ez.no/company/contact/.
//
// The "GNU General Public License" (GPL) is available at
// http://www.gnu.org/copyleft/gpl.html.
//
// Contact licence@ez.no if any conditions of this licencing isn't clear to
// you.
//

/*! \file eznetagreement.php
*/

/*!
  \class eZNetAgreement eznetagreement.php
  \brief The class eZNetAgreement does

*/


class eZNetAgreement extends eZPersistentObject
{
    // Consts
    const LevelAccessToPatch = 300;
    const LevelSupport = 250;
    const LevelInternalTesting = 200;
    const LevelStarter = 125;
    const LevelOnDemandStarter = 115;
    const LevelOnDemandPlus =    110;
    const LevelBasic =   100;
    const LevelSilver =   75;
    const LevelGold =     50;
    const LevelPlatinum = 25;
    const LevelNow      = 20;
    const LevelNowPlus  = 18;
    const LevelPremiumBasic =  17;
    const LevelPremiumSilver = 16;
    const LevelPremiumGold = 14;
    const LevelPremiumPlatinum = 12;

    const ContentClassID = 52;


    /*!
     Constructor
    */
    function eZNetAgreement( $row = array() )
    {
        $this->eZPersistentObject( $row );
    }

    /*!
     \reimp
    */
    static function definition()
    {
        return array( "fields" => array( "id" => array( 'name' => 'ID',
                                                        'datatype' => 'integer',
                                                        'default' => 0,
                                                        'required' => false,
                                                        'foreign_override_class' => 'eZContentObject',
                                                        'foreign_override_attribute' => 'id' ),
                                         "name" => array( 'name' => 'Name',
                                                          'datatype' => 'string',
                                                          'default' => '',
                                                          'required' => true ),
                                         "description" => array( 'name' => 'Description',
                                                                 'datatype' => 'string',
                                                                 'default' => '',
                                                                 'required' => true ),
                                         "email" => array( 'name' => 'Email',
                                                           'datatype' => 'string',
                                                           'default' => '',
                                                           'required' => true ),
                                         "identifier" => array( 'name' => 'Identifier',
                                                                'datatype' => 'string',
                                                                'default' => '',
                                                                'required' => true ) ),
                      "keys" => array( 'id' ),
                      "function_attributes" => array(),
                      "increment_key" => "id",
                      "class_name" => "eZNetAgreement",
                      'soap_custom_handler' => true,
                      "sort" => array( "Name" => "asc" ),
                      "name" => "ezx_ezpnet_agreement" );
    }

    /*!
     \static
    */
    function fetch( $id, $asObject = true )
    {
        return eZPersistentObject::fetchObject( eZNetAgreement::definition(),
                                                null,
                                                array( 'id' => $id ),
                                                $asObject );
    }

    /*!
     \static

     Get specification for custom datamapping. ( to use with SOAP sync. )

     Syntax :
     array( <attribute> => array( <function 1> => array( <function1 param1>, <function 1 param2>, ...), <function 2> => array( ... ) )| <index>, )
     */
    function customDataMapDefinition()
    {
        return array( 'fields' => array( 'id' => array( 'attribute' => array( 'id' ) ),
                                         'name' => array( 'dataMap' => array(),
                                                          'name',
                                                          'attribute' => array( 'data_text' ) ),
                                         'description' => array( 'dataMap' => array(),
                                                                 'description',
                                                                 'attribute' => array( 'data_text' ) ),
                                         'email' => array( 'dataMap' => array(),
                                                           'email',
                                                           'attribute' => array( 'data_text' ) ),
                                         'identifier' => array( 'dataMap' => array(),
                                                                'identifier',
                                                                'attribute' => array( 'data_text' ) ) ) );
    }

    /*!
     \static
     Get key name

     \return key name
    */
    function customKeyName()
    {
        return 'id';
    }

    /*!
     \static
     Get custom max from current class. \sa eZNetSOAPSync::getCustomMax()

     \return max id for the eZCRMCustomer data.
    */
    function getCustomMax()
    {
        $resultSet = eZContentObject::fetchObjectList( eZContentObject::definition(),
                                                       array(),
                                                       array( 'contentclass_id' => eZNetAgreement::ContentClassID,
                                                              'status' => EZ_CONTENT_OBJECT_STATUS_PUBLISHED ),
                                                       null,
                                                       null,
                                                       false,
                                                       false,
                                                       array( array( 'operation' => 'max( id )',
                                                                     'name' => 'max' ) ) );
        return isset( $resultSet[0]['max'] ) ? $resultSet[0]['max'] : false;
    }

    /*!
     \static
     Get custom data fetch definition
    */
    function customDataFetchDefinition()
    {
        return array( 'class_name' => 'eZContentObject',
                      'conditions' => array( 'contentclass_id' => eZNetAgreement::ContentClassID,
                                             'status' => EZ_CONTENT_OBJECT_STATUS_PUBLISHED ),
                      'include_file' => 'kernel/classes/ezcontentobject.php' );
    }

    /*!
     \static
     Get custom data filter from for sync log.
    */
    function customDataFilter()
    {
        return 'eZNetAgreement';
    }

    /*!
     Get latest ID ( soap custom handler )

     \param remote host
    */
    function getLatestID( $remoteHost )
    {
        $db = eZDB::instance();

        $sql = "SELECT max( remote_value ) as max FROM ezx_ezpnet_soap_log
                  WHERE remote_host = '" . $db->escapeString( $remoteHost ) . "' AND
                        class_name = 'eZContentObject' AND
                        extended_filter = '" . $db->escapeString( eZNetAgreement::customDataFilter() ) . "' AND
                        key_name = 'id'";

        $result = $db->arrayQuery( $sql );
        if ( $result )
        {
            return max( 0, $result[0]['max'] );
        }

        return 0;
    }

    /*!
     \static

     Get Agreement ranking list. Agreement identifier as array key

     Lowest number is highest priority
    */
    function agreementPriorityList()
    {
        return array( 'access_to_patch' => eZNetAgreement::LevelAccessToPatch,
                      'support' => eZNetAgreement::LevelSupport,
                      'internal_testing' => eZNetAgreement::LevelInternalTesting,
                      'starter' => eZNetAgreement::LevelStarter,
                      'on_demand_starter' => eZNetAgreement::LevelOnDemandStarter,
                      'on_demand_plus' => eZNetAgreement::LevelOnDemandPlus,
                      'basic' => eZNetAgreement::LevelBasic,
                      'silver' => eZNetAgreement::LevelSilver,
                      'gold' => eZNetAgreement::LevelGold,
                      'platinum' => eZNetAgreement::LevelPlatinum,
                      'now' => eZNetAgreement::LevelNow,
                      'now_plus' => eZNetAgreement::LevelNowPlus,
                      'premium_basic' => eZNetAgreement::LevelPremiumBasic,
                      'premium_silver' => eZNetAgreement::LevelPremiumSilver,
                      'premium_gold' => eZNetAgreement::LevelPlatinumGold,
                      'premium_platinum' => eZNetAgreement::LevelPlatinumPlatinum );
    }

    /*!
     \static
     Get agreements which should have network issue trackers.

     \return array of agreement identifiers
    */
    function issueAgreementIdentifierList()
    {
        return array( 'access_to_patch',
                      'support',
                      'internal_testing',
                      'on_demand_starter',
                      'on_demand_plus',
                      'basic',
                      'silver',
                      'gold',
                      'platinum',
                      'now',
                      'now_plus',
                      'premium_basic',
                      'premium_silver',
                      'premium_gold',
                      'premium_platinum' );
    }
}

?>
