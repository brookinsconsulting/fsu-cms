<?php
//
// Definition of eZNetBranch class
//
// Created on: <03-Feb-2006 10:58:45 hovik>
//
// Copyright (C) 1999-2005 eZ systems as. All rights reserved.
//
// This source file is part of the eZ publish (tm) Open Source Content
// Management System.
//
// This file may be distributed and/or modified under the terms of the
// "GNU General Public License" version 2 as published by the Free
// Software Foundation and appearing in the file LICENSE included in
// the packaging of this file.
//
// Licencees holding a valid "eZ publish professional licence" version 2
// may use this file in accordance with the "eZ publish professional licence"
// version 2 Agreement provided with the Software.
//
// This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING
// THE WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR
// PURPOSE.
//
// The "eZ publish professional licence" version 2 is available at
// http://ez.no/ez_publish/licences/professional/ and in the file
// PROFESSIONAL_LICENCE included in the packaging of this file.
// For pricing of this licence please contact us via e-mail to licence@ez.no.
// Further contact information is available at http://ez.no/company/contact/.
//
// The "GNU General Public License" (GPL) is available at
// http://www.gnu.org/copyleft/gpl.html.
//
// Contact licence@ez.no if any conditions of this licencing isn't clear to
// you.
//

/*! \file eznetbranch.php
*/

/*!
  \class eZNetBranch eznetbranch.php
  \brief The class eZNetBranch does

*/

class eZNetBranch extends eZPersistentObject
{
    /// Consts
    const StatusDraft = 0;
    const StatusPublished = 1;

    /*!
     Constructor
    */
    function eZNetBranch($row )
    {
        $this->NetUtils = new eZNetUtils();
        $this->eZPersistentObject( $row );
    }

    static function definition()
    {
        return array( "fields" => array( "id" => array( 'name' => 'ID',
                                                        'datatype' => 'integer',
                                                        'default' => 0,
                                                        'required' => true ),
                                         "name" => array( 'name' => 'Name',
                                                          'datatype' => 'string',
                                                          'default' => '',
                                                          'required' => true ),
                                         'creator_id' => array( 'name' => 'CreatorID',
                                                                'datatype' => 'integer',
                                                                'default' => 0,
                                                                'required' => true ),
                                         'created' => array( 'name' => 'Created',
                                                             'datatype' => 'integer',
                                                             'default' => 0,
                                                             'required' => true ),
                                         'modified' => array( 'name' => 'Modified',
                                                              'datatype' => 'integer',
                                                             'default' => 0,
                                                             'required' => true ),
                                         "description" => array( 'name' => 'Description',
                                                                 'datatype' => 'string',
                                                                 'default' => '',
                                                                 'required' => true ),
                                         'status' => array( 'name' => 'Status',
                                                            'datatype' => 'integer',
                                                            'default' => 0,
                                                            'required' => true,
                                                            'keep_key' => true ) ),
                      "keys" => array( "id", 'status' ),
                      "function_attributes" => array( 'creator' => 'creator' ),
                      "increment_key" => "id",
                      "class_name" => "eZNetBranch",
                      "sort" => array( "name" => "asc" ),
                      "name" => "ezx_ezpnet_branch" );
    }

    /*!
     \static

     Fetch a list of branches based on installation remote ID.

    */
    function fetchListByRemoteIDAndLatestID( $installationSiteID,
                                             $latestID,
                                             $offset = 0,
                                             $limit = 100,
                                             $asObject = true,
                                             $status = eZNetBranch::StatusPublished )
    {
        $installation = eZNetInstallation::fetchBySiteID( $installationSiteID );
        if ( $installation->attribute( 'branch_id' ) == $latestID )
        {
            return array();
        }
        return eZPersistentObject::fetchObjectList( eZNetBranch::definition(),
                                                    array( 'id' ),
                                                    array( 'id' => $installation->attribute( 'branch_id' ),
                                                           'status' => $status ),
                                                    array( 'id' => 'asc' ),
                                                    array( 'limit' => $limit,
                                                           'offset' => $offset ),
                                                    $asObject );
    }

    /*!
     \static

     Fetch a list of branches based on installation remote ID.

    */
    function fetchListByRemoteIDAndLatestModified( $installationSiteID,
                                                   $latestModified,
                                                   $offset = 0,
                                                   $limit = 100,
                                                   $asObject = true,
                                                   $status = eZNetBranch::StatusPublished )
    {
        $installation = eZNetInstallation::fetchBySiteID( $installationSiteID );
        if ( !$installation )
        {
            return false;
        }

        return eZPersistentObject::fetchObjectList( eZNetBranch::definition(),
                                                    array( 'id' ),
                                                    array( 'id' => $installation->attribute( 'branch_id' ),
                                                           'modified' => array( '>', $latestModified ),
                                                           'status' => $status ),
                                                    array( 'modified' => 'asc' ),
                                                    array( 'limit' => $limit,
                                                           'offset' => $offset ),
                                                    $asObject );
    }

    /*!
     \static

     Create branch element
    */
    function create()
    {
        $branch = new eZNetBranch( array( 'status' => eZNetBranch::StatusDraft,
                                          'created' => mktime(),
                                          'creator_id' => eZUser::currentUserID() ) );
        $branch->store();

        return $branch;
    }

    /*!
     \static
    */
    function fetch( $id,
                    $status = eZNetBranch::StatusPublished,
                    $asObject = true )
    {
        return eZPersistentObject::fetchObject( eZNetBranch::definition(),
                                                null,
                                                array( 'id' => $id,
                                                       'status' => $status ),
                                                $asObject );
    }

    /*!
     \static

     Fetch draft

     \param Branch ID
     \param force, if force creation of draft.
     \param $asObject
    */
    function fetchDraft( $id, $force = true, $asObject = true )
    {
        $branch = eZNetBranch::fetch( $id, eZNetBranch::StatusDraft, $asObject );
        if ( !$branch &&
             $force )
        {
            $branch = eZNetBranch::fetch( $id, eZNetBranch::StatusPublished, $asObject );
            if ( $branch )
            {
                $branch->setAttribute( 'status', eZNetBranch::StatusDraft );
                $branch->store();
            }
        }

        if ( !$branch )
        {
            return false;
        }
        return $branch;
    }

    /*!
     \reimp
    */
    function attribute( $attr, $noFunction = false )
    {
        $retVal = null;
        switch( $attr )
        {
            case 'creator':
            {
                $retVal = eZUser::fetch( $this->attribute( 'creator_id' ) );
            } break;

            default:
            {
                $retVal = eZPersistentObject::attribute( $attr );
            } break;
        }

        return $retVal;
    }

    /*!
     Publish current object
    */
    function publish()
    {
        $this->setAttribute( 'status', eZNetBranch::StatusPublished );
        $this->setAttribute( 'modified', mktime() );
        $this->store();
        $this->removeDraft();
    }

    /*!
     Remove draft.
    */
    function removeDraft()
    {
        $draft = eZNetBranch::fetchDraft( $this->attribute( 'id' ),
                                          false );
        if ( $draft )
        {
            $draft->remove();
        }
    }

    /*!
      \static
      Remove all objects of \a id
    */
    function removeAll( $id )
    {
        eZPersistentObject::removeObject( eZNetBranch::definition(),
                                          array( 'id' => $id ) );
    }

    /*!
     \static

     Fetch branch list
    */
    function fetchList( $offset = 0,
                        $limit = 20,
                        $status = eZNetBranch::StatusPublished,
                        $asObject = true )
    {
        return eZPersistentObject::fetchObjectList( eZNetBranch::definition(),
                                                    null,
                                                    array( 'status' => $status ),
                                                    null,
                                                    array( 'limit' => $limit,
                                                           'offset' => $offset ),
                                                    $asObject );
    }
}
?>
