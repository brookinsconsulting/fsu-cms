<?php
//
// Definition of eZNetEventResult class
//
// Created on: <07-Sep-2006 11:27:25 hovik>
//
// Copyright (C) 1999-2005 eZ systems as. All rights reserved.
//
// This source file is part of the eZ publish (tm) Open Source Content
// Management System.
//
// This file may be distributed and/or modified under the terms of the
// "GNU General Public License" version 2 as published by the Free
// Software Foundation and appearing in the file LICENSE included in
// the packaging of this file.
//
// Licencees holding a valid "eZ publish professional licence" version 2
// may use this file in accordance with the "eZ publish professional licence"
// version 2 Agreement provided with the Software.
//
// This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING
// THE WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR
// PURPOSE.
//
// The "eZ publish professional licence" version 2 is available at
// http://ez.no/ez_publish/licences/professional/ and in the file
// PROFESSIONAL_LICENCE included in the packaging of this file.
// For pricing of this licence please contact us via e-mail to licence@ez.no.
// Further contact information is available at http://ez.no/company/contact/.
//
// The "GNU General Public License" (GPL) is available at
// http://www.gnu.org/copyleft/gpl.html.
//
// Contact licence@ez.no if any conditions of this licencing isn't clear to
// you.
//

/*! \file ezneteventresult.php
*/

/*!
  \class eZNetEventResult ezneteventresult.php
  \brief The class eZNetEventResult does

*/

class eZNetEventResult extends eZPersistentObject
{
    /// Consts
    const Success = 1;
    const Failed = 0;


    /*!
     Constructor
    */
    function eZNetEventResult( $row = array() )
    {
        $this->eZPersistentObject( $row );
    }

    static function definition()
    {
        return array( "fields" => array( "id" => array( 'name' => 'ID',
                                                        'datatype' => 'integer',
                                                        'default' => 0,
                                                        'required' => true ),
                                         "event_id" => array( 'name' => 'EventID',
                                                              'datatype' => 'integer',
                                                              'default' => 0,
                                                              'required' => true,
                                                              'foreign_class' => 'eZNetEvent',
                                                              'foreign_attribute' => 'id',
                                                              'multiplicity' => '0..*' ),
                                         "run_id" => array( 'name' => 'MonitorItemID',
                                                            'datatype' => 'string',
                                                            'default' => 0,
                                                            'required' => true ),
                                         'value' => array( 'name' => 'Value',
                                                           'datatype' => 'string',
                                                           'default' => '',
                                                           'required' => true ),
                                         'success' => array( 'name' => 'Success',
                                                             'datatype' => 'integer',
                                                             'default' => 1,
                                                             'required' => true ),
                                         'modified' => array( 'name' => 'Modified',
                                                              'datatype' => 'integer',
                                                              'default' => 0,
                                                              'required' => true ),
                                         'created' => array( 'name' => 'Created',
                                                             'datatype' => 'integer',
                                                             'default' => 0,
                                                             'required' => true ),
                                         'description' => array( 'name' => 'Description',
                                                                 'datatype' => 'string',
                                                                 'default' => '',
                                                                 'required' => true ) ),
                      "keys" => array( "id" ),
                      "function_attributes" => array( 'event' => 'event' ),
                      "increment_key" => "id",
                      "class_name" => "eZNetEventResult",
                      "sort" => array( "id" => "asc" ),
                      "name" => "ezx_ezpnet_event_result" );
    }

    /*!
     \reimp
    */
    function attribute( $attr, $noFunction = false )
    {
        $retVal = null;
        switch( $attr )
        {
            case 'event':
            {
                $retVal = eZNetEvent::fetch( $this->attribute( 'event_id' ) );
            } break;

            default:
            {
                $retVal =& eZPersistentObject::attribute( $attr );
            } break;
        }

        return $retVal;
    }

    /*!
     \static

     Fetch result item value by ID
    */
    function fetch( $id, $asObject = true )
    {
        return eZNetEventResult::fetchObject( eZNetEventResult::definition(),
                                              null,
                                              array( 'id' => $id ),
                                              $asObject );
    }

    /*!
     \static

     Fetch list by event id
    */
    function fetchListByEventID( $eventID,
                                 $offset = 0,
                                 $limit = 10,
                                 $asObject = true )
    {
        return eZNetEventResult::fetchObjectList( eZNetEventResult::definition(),
                                                  null,
                                                  array( 'event_id' => $eventID ),
                                                  array( 'created' => 'desc' ),
                                                  null,
                                                  $asObject );
    }

    /*!
     \static

     Fetch list by event id
    */
    function fetchListByRunID( $runID,
                               $offset = 0,
                               $limit = 10,
                               $asObject = true )
    {
        return eZNetEventResult::fetchObjectList( eZNetEventResult::definition(),
                                                  null,
                                                  array( 'run_id' => $runID ),
                                                  array( 'created' => 'desc' ),
                                                  null,
                                                  $asObject );
    }

    /*!
     \abstract

     Event function. This function is called each time the event is run

     \param $cli object
     \param $script object

     \return true if event should be spawned, false if not.
    */
    function run( $cli, $script )
    {
    }

}

?>
