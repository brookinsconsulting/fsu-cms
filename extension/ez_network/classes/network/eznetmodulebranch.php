<?php
//
// Definition of eZNetModuleBranch class
//
// Created on: <26-Sep-2006 11:43:53 hovik>
//
// Copyright (C) 1999-2005 eZ systems as. All rights reserved.
//
// This source file is part of the eZ publish (tm) Open Source Content
// Management System.
//
// This file may be distributed and/or modified under the terms of the
// "GNU General Public License" version 2 as published by the Free
// Software Foundation and appearing in the file LICENSE included in
// the packaging of this file.
//
// Licencees holding a valid "eZ publish professional licence" version 2
// may use this file in accordance with the "eZ publish professional licence"
// version 2 Agreement provided with the Software.
//
// This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING
// THE WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR
// PURPOSE.
//
// The "eZ publish professional licence" version 2 is available at
// http://ez.no/ez_publish/licences/professional/ and in the file
// PROFESSIONAL_LICENCE included in the packaging of this file.
// For pricing of this licence please contact us via e-mail to licence@ez.no.
// Further contact information is available at http://ez.no/company/contact/.
//
// The "GNU General Public License" (GPL) is available at
// http://www.gnu.org/copyleft/gpl.html.
//
// Contact licence@ez.no if any conditions of this licencing isn't clear to
// you.
//

/*! \file eznetmodulebranch.php
*/

/*!
  \class eZNetModuleBranch eznetmodulebranch.php
  \brief The class eZNetModuleBranch does

*/


class eZNetModuleBranch extends eZPersistentObject
{
    /// Consts
    const StatusDraft = 0;
    const StatusPublished = 1;


    /*!
     Constructor
    */
    function eZNetModuleBranch($row )
    {
        $this->NetUtils = new eZNetUtils();
        $this->eZPersistentObject( $row );
    }

    static function definition()
    {
        return array( "fields" => array( "id" => array( 'name' => 'ID',
                                                        'datatype' => 'integer',
                                                        'default' => 0,
                                                        'required' => true ),
                                         "name" => array( 'name' => 'Name',
                                                          'datatype' => 'string',
                                                          'default' => '',
                                                          'required' => true ),
                                         'creator_id' => array( 'name' => 'CreatorID',
                                                                'datatype' => 'integer',
                                                                'default' => 0,
                                                                'required' => true ),
                                         'created' => array( 'name' => 'Created',
                                                             'datatype' => 'integer',
                                                             'default' => 0,
                                                             'required' => true ),
                                         'version_identifier' => array( 'name' => 'VersionIdentifier',
                                                                        'datatype' => 'string',
                                                                        'default' => '',
                                                                        'required' => true ),
                                         'modified' => array( 'name' => 'Modified',
                                                              'datatype' => 'integer',
                                                              'default' => 0,
                                                              'required' => true ),
                                         "description" => array( 'name' => 'Description',
                                                                 'datatype' => 'string',
                                                                 'default' => '',
                                                                 'required' => true ),
                                         'status' => array( 'name' => 'Status',
                                                            'datatype' => 'integer',
                                                            'default' => 0,
                                                            'required' => true,
                                                            'keep_key' => true ) ),
                      "keys" => array( "id", 'status' ),
                      "function_attributes" => array( 'creator' => 'creator',
                                                      'version_value' => 'versionValue' ),
                      "increment_key" => "id",
                      "class_name" => "eZNetModuleBranch",
                      "sort" => array( "name" => "asc" ),
                      "name" => "ezx_ezpnet_module_branch" );
    }

    /*!
     \static
     Fetch eZNetModuleBranch list by installation site ID

     \param installation site ID
     \param asObject

     \return eZNetModuleBranch list
    */
    function fetchListBySiteID( $siteID,
                                $asObject = true )
    {
        $moduleInstallationList = eZNetModuleInstallation::fetchListBySiteID( $siteID );
        $moduleBranchList = array();

        foreach( $moduleInstallationList as $moduleInstallation )
        {
            $moduleBranchList[] = $moduleInstallation->attribute( 'module_branch' );
        }

        return $moduleBranchList;
    }

    /*!
     \static

     Fetch a list of branches based on installation remote ID.

    */
    function fetchListByRemoteIDAndLatestModified( $installationSiteID,
                                                   $latestModified,
                                                   $offset = 0,
                                                   $limit = 100,
                                                   $asObject = true,
                                                   $status = eZNetModuleBranch::StatusPublished )
    {
        $moduleList = eZNetModuleInstallation::fetchListBySiteID( $installationSiteID );
        $moduleBranchIDList = array();
        foreach( $moduleList as $module )
        {
            $moduleBranchIDList[] = $module->attribute( 'module_branch_id' );
        }

        return eZNetModuleBranch::fetchObjectList( eZNetModuleBranch::definition(),
                                                   array( 'id' ),
                                                   array( 'id' => array( $moduleBranchIDList ),
                                                          'modified' => array( '>', $latestModified ),
                                                          'status' => $status ),
                                                   array( 'modified' => 'asc' ),
                                                   array( 'limit' => $limit,
                                                          'offset' => $offset ),
                                                   $asObject );
    }

    /*!
     \static

     Create branch element
    */
    function create()
    {
        $branch = new eZNetModuleBranch( array( 'status' => eZNetModuleBranch::StatusDraft,
                                                'created' => mktime(),
                                                'creator_id' => eZUser::currentUserID() ) );
        $branch->store();

        return $branch;
    }

    /*!
     \static
    */
    function fetch( $id,
                    $status = eZNetModuleBranch::StatusPublished,
                    $asObject = true )
    {
        return eZNetModuleBranch::fetchObject( eZNetModuleBranch::definition(),
                                               null,
                                               array( 'id' => $id,
                                                      'status' => $status ),
                                               $asObject );
    }

    /*!
     \static

     Fetch draft

     \param Branch ID
     \param force, if force creation of draft.
     \param $asObject
    */
    function fetchDraft( $id, $force = true, $asObject = true )
    {
        $branch = eZNetModuleBranch::fetch( $id, eZNetModuleBranch::StatusDraft, $asObject );
        if ( !$branch &&
             $force )
        {
            $branch = eZNetModuleBranch::fetch( $id, eZNetModuleBranch::StatusPublished, $asObject );
            if ( $branch )
            {
                $branch->setAttribute( 'status', eZNetModuleBranch::StatusDraft );
                $branch->store();
            }
        }

        if ( !$branch )
        {
            return false;
        }
        return $branch;

    }

    /*!
     \reimp
    */
    function attribute( $attr, $noFunction = false )
    {
        $retVal = null;
        switch( $attr )
        {
            case 'version_value':
            {
                // First check extension for version information
                $extensionInfo = eZNetUtils::extensionInfo( $this->attribute( 'version_identifier' ) );
                if ( $extensionInfo !== null )
                {
                    $retVal = $extensionInfo['version'];
                }
                // If it does not exsist, check extension for information.
                else
                {
                    $db = eZDB::instance();
                    $sql = 'SELECT value FROM ezsite_data WHERE name=\'' . $db->escapeString( $this->attribute( 'version_identifier' ) ) . '\'';
                    $result = $db->arrayQuery( $sql );
                    if ( count( $result ) )
                    {
                        $retVal = $result[0]['value'];
                    }
                }
            } break;

            case 'creator':
            {
                $retVal = eZUser::fetch( $this->attribute( 'creator_id' ) );
            } break;

            default:
            {
                $retVal = eZPersistentObject::attribute( $attr );
            } break;
        }

        return $retVal;
    }

    /*!
     Publish current object
    */
    function publish()
    {
        $this->setAttribute( 'status', eZNetModuleBranch::StatusPublished );
        $this->setAttribute( 'modified', mktime() );
        $this->store();
        $this->removeDraft();
    }

    /*!
     Remove draft.
    */
    function removeDraft()
    {
        $draft = eZNetModuleBranch::fetchDraft( $this->attribute( 'id' ),
                                                false );
        if ( $draft )
        {
            $draft->remove();
        }
    }

    /*!
      \static
      Remove all objects of \a id
    */
    function removeAll( $id )
    {
        eZNetModuleBranch::removeObject( eZNetModuleBranch::definition(),
                                         array( 'id' => $id ) );
    }

    /*!
     \static

     Fetch branch list
    */
    function fetchList( $offset = 0,
                        $limit = 20,
                        $status = eZNetModuleBranch::StatusPublished,
                        $asObject = true )
    {
        return eZNetModuleBranch::fetchObjectList( eZNetModuleBranch::definition(),
                                                   null,
                                                   array( 'status' => $status ),
                                                   null,
                                                   array( 'limit' => $limit,
                                                          'offset' => $offset ),
                                                   $asObject );
    }
}

?>
