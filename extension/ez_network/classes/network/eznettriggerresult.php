<?php
//
// Definition of eZNetTriggerResult class
//
// Created on: <06-Sep-2006 10:55:48 hovik>
//
// Copyright (C) 1999-2005 eZ systems as. All rights reserved.
//
// This source file is part of the eZ publish (tm) Open Source Content
// Management System.
//
// This file may be distributed and/or modified under the terms of the
// "GNU General Public License" version 2 as published by the Free
// Software Foundation and appearing in the file LICENSE included in
// the packaging of this file.
//
// Licencees holding a valid "eZ publish professional licence" version 2
// may use this file in accordance with the "eZ publish professional licence"
// version 2 Agreement provided with the Software.
//
// This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING
// THE WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR
// PURPOSE.
//
// The "eZ publish professional licence" version 2 is available at
// http://ez.no/ez_publish/licences/professional/ and in the file
// PROFESSIONAL_LICENCE included in the packaging of this file.
// For pricing of this licence please contact us via e-mail to licence@ez.no.
// Further contact information is available at http://ez.no/company/contact/.
//
// The "GNU General Public License" (GPL) is available at
// http://www.gnu.org/copyleft/gpl.html.
//
// Contact licence@ez.no if any conditions of this licencing isn't clear to
// you.
//

/*! \file eznettriggerresult.php
*/

/*!
  \class eZNetTriggerResult eznettriggerresult.php
  \brief The class eZNetTriggerResult does

*/

class eZNetTriggerResult extends eZPersistentObject
{
    /// Consts
    const Success = 1;
    const Failed = 0;


    /*!
     Constructor
    */
    function eZNetTriggerResult( $row = array() )
    {
        $this->eZPersistentObject( $row );
    }

        static function definition()
    {
        return array( "fields" => array( "id" => array( 'name' => 'ID',
                                                        'datatype' => 'integer',
                                                        'default' => 0,
                                                        'required' => true ),
                                         "trigger_id" => array( 'name' => 'TriggerID',
                                                                'datatype' => 'integer',
                                                                'default' => 0,
                                                                'required' => true,
                                                                'foreign_class' => 'eZNetTrigger',
                                                                'foreign_attribute' => 'id',
                                                                'multiplicity' => '0..*' ),
                                         "run_id" => array( 'name' => 'MonitorItemID',
                                                            'datatype' => 'string',
                                                            'default' => 0,
                                                            'required' => true ),
                                         'value' => array( 'name' => 'Value',
                                                           'datatype' => 'string',
                                                           'default' => '',
                                                           'required' => true ),
                                         'success' => array( 'name' => 'Success',
                                                             'datatype' => 'integer',
                                                             'default' => 1,
                                                             'required' => true ),
                                         'modified' => array( 'name' => 'Modified',
                                                              'datatype' => 'integer',
                                                              'default' => 0,
                                                              'required' => true ),
                                         'created' => array( 'name' => 'Created',
                                                             'datatype' => 'integer',
                                                             'default' => 0,
                                                             'required' => true ),
                                         'description' => array( 'name' => 'Description',
                                                                 'datatype' => 'string',
                                                                 'default' => '',
                                                                 'required' => true ) ),
                      "keys" => array( "id" ),
                      "function_attributes" => array( 'trigger' => 'trigger' ),
                      "increment_key" => "id",
                      "class_name" => "eZNetTriggerResult",
                      "sort" => array( "id" => "asc" ),
                      "name" => "ezx_ezpnet_trigger_result" );
    }

    /*!
     \reimp
    */
    function attribute( $attr, $noFunction = false )
    {
        $retVal = null;
        switch( $attr )
        {
            case 'trigger':
            {
                $retVal = eZNetTrigger::fetch( $this->attribute( 'trigger_id' ) );
            } break;

            default:
            {
                $retVal =& eZPersistentObject::attribute( $attr );
            } break;
        }

        return $retVal;
    }

    /*!
     \static

     Fetch result item value by ID
    */
    function fetch( $id, $asObject = true )
    {
        return eZNetTriggerResult::fetchObject( eZNetTriggerResult::definition(),
                                                null,
                                                array( 'id' => $id ),
                                                $asObject );
    }

    /*!
     \static

     Fetch list by trigger id
    */
    function fetchListByTriggerID( $triggerID,
                                   $offset = 0,
                                   $limit = 10,
                                   $asObject = true )
    {
        return eZNetTriggerResult::fetchObjectList( eZNetTriggerResult::definition(),
                                                    null,
                                                    array( 'trigger_id' => $triggerID ),
                                                    array( 'created' => 'desc' ),
                                                    null,
                                                    $asObject );
    }

    /*!
     \static

     Fetch list by trigger id
    */
    function fetchListByRunID( $runID,
                               $offset = 0,
                               $limit = 10,
                               $asObject = true )
    {
        return eZNetTriggerResult::fetchObjectList( eZNetTriggerResult::definition(),
                                                    null,
                                                    array( 'run_id' => $runID ),
                                                    array( 'created' => 'desc' ),
                                                    null,
                                                    $asObject );
    }

    /*!
     \abstract

     Trigger function. This function is called each time the trigger is run

     \param $cli object
     \param $script object

     \return true if event should be spawned, false if not.
    */
    function run( $cli, $script )
    {
    }

}

?>
