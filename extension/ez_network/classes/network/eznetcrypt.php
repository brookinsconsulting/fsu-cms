<?php
//
// Definition of eZNetCrypt class
//
// Created on: <06-Jul-2005 14:07:28 hovik>
//
// Copyright (C) 1999-2005 eZ systems as. All rights reserved.
//
// This source file is part of the eZ publish (tm) Open Source Content
// Management System.
//
// This file may be distributed and/or modified under the terms of the
// "GNU General Public License" version 2 as published by the Free
// Software Foundation and appearing in the file LICENSE included in
// the packaging of this file.
//
// Licencees holding a valid "eZ publish professional licence" version 2
// may use this file in accordance with the "eZ publish professional licence"
// version 2 Agreement provided with the Software.
//
// This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING
// THE WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR
// PURPOSE.
//
// The "eZ publish professional licence" version 2 is available at
// http://ez.no/ez_publish/licences/professional/ and in the file
// PROFESSIONAL_LICENCE included in the packaging of this file.
// For pricing of this licence please contact us via e-mail to licence@ez.no.
// Further contact information is available at http://ez.no/company/contact/.
//
// The "GNU General Public License" (GPL) is available at
// http://www.gnu.org/copyleft/gpl.html.
//
// Contact licence@ez.no if any conditions of this licencing isn't clear to
// you.
//

/*! \file eznetcrypt.php
*/

/*!
  \class eZNetCrypt eznetcrypt.php
  \brief The class eZNetCrypt does

*/

class eZNetCrypt
{

    /*!
     \constructor
    */
    function eZNetCrypt()
    {
//        $this->IVSize = mcrypt_get_iv_size( MCRYPT_XTEA, MCRYPT_MODE_ECB );
//        $this->IV = mcrypt_create_iv( $this->IVSize, MCRYPT_RAND );
    }

    /*!
     Encrypt specified text using DES

     \param text
    */
    function encrypt( $text )
    {
        if ( !isset( $this ) ||
             get_class( $this ) != 'eZNetCrypt' )
        {
            $crypt = new eZNetCrypt();
        }
        else
        {
            $crypt = $this;
        }

        if ( !$crypt->Enabled )
        {
            return $text;
        }

        return mcrypt_encrypt( MCRYPT_3DES, $crypt->Key, $text, MCRYPT_MODE_CFB, $crypt->IV );
    }

    /*!
     Decrypt specified text using DES

     \param Encrypted text
    */
    function decrypt( $enc )
    {
        if ( !isset( $this ) ||
             get_class( $this ) != 'eZNetCrypt' )
        {
            $crypt = new eZNetCrypt();
        }
        else
        {
            $crypt = $this;
        }

        if ( !$crypt->Enabled )
        {
            return $enc;
        }

        return mcrypt_decrypt( MCRYPT_3DES, $crypt->Key, $enc, MCRYPT_MODE_CFB, $crypt->IV );
    }

    var $IVSize;
    var $IV;
    var $Key = '*321 regninger 123*';

    var $Enabled = false;
}

?>
