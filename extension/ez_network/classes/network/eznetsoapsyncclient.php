<?php
//
// Definition of eZNetSOAPSyncClient class
//
// Created on: <27-Feb-2006 16:57:33 hovik>
//
// Copyright (C) 1999-2005 eZ systems as. All rights reserved.
//
// This source file is part of the eZ publish (tm) Open Source Content
// Management System.
//
// This file may be distributed and/or modified under the terms of the
// "GNU General Public License" version 2 as published by the Free
// Software Foundation and appearing in the file LICENSE included in
// the packaging of this file.
//
// Licencees holding a valid "eZ publish professional licence" version 2
// may use this file in accordance with the "eZ publish professional licence"
// version 2 Agreement provided with the Software.
//
// This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING
// THE WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR
// PURPOSE.
//
// The "eZ publish professional licence" version 2 is available at
// http://ez.no/ez_publish/licences/professional/ and in the file
// PROFESSIONAL_LICENCE included in the packaging of this file.
// For pricing of this licence please contact us via e-mail to licence@ez.no.
// Further contact information is available at http://ez.no/company/contact/.
//
// The "GNU General Public License" (GPL) is available at
// http://www.gnu.org/copyleft/gpl.html.
//
// Contact licence@ez.no if any conditions of this licencing isn't clear to
// you.
//

/*! \file eznetsoapsyncclient.php
*/

/*!
  \class eZNetSOAPSyncClient eznetsoapsyncclient.php
  \brief The class eZNetSOAPSyncClient does

*/


class eZNetSOAPSyncClient extends eZNetSOAPSync
{
    /*!
     Constructor

     \param eZPersistenceObject definition
    */
    function eZNetSOAPSyncClient( $definition = false , $remoteHost = false )
    {
        $this->eZNetSOAPSync( $definition, $remoteHost );
    }

    /*!
     Syncronize - push. Push client data to server.
    */
    function syncronizePushClient( $soapClient )
    {
        $sendCount = 0;

        // 1. Get remote eZ Publish hostID
        $request = new eZSOAPRequest( 'hostID', eZNetSOAPSync::SYNC_NAMESPACE );
        $response = $soapClient->send( $request );

        if( !$request ||
            $response->isFault() )
        {
            eZDebug::writeError( 'Did not get valid result running SOAP method : hostID, on class : ' . $this->ClassName );
            return false;
        }

        $this->RemoteHost = $response->value(); // Missing message IDs

        if ( !$this->RemoteHost )
        {
            eZDebug::writeError( 'RemoteHost not set: ' . var_export( $this->RemoteHost, 1 ),
                                 'eZNetSOAPSync::syncronize()' );
            return false;
        }

        $sendList = $this->createSendArrayList( $soapClient );

        while( $sendList &&
               count( $sendList  ) > 0 )
        {
            $request = new eZSOAPRequest( 'importElements', eZNetSOAPSync::SYNC_NAMESPACE );
            $request->addParameter( 'className', $this->ClassName );
            $request->addParameter( 'hostID', eZNetUtils::hostID() );
            $request->addParameter( 'data', $sendList );
            $response = $soapClient->send( $request );

            if( $response->isFault() )
            {
                eZDebug::writeNotice( 'Did not get valid result running SOAP method : importElements, on class : ' . $this->ClassName );
                return false;
            }

            $sendCount += count( $sendList );

            $sendList = $this->createSendArrayList( $soapClient );
        }

        return array( 'class_name' => $this->ClassName,
                      'export_count' => $sendCount );
    }

    /*!
     Get list of objects to send.

     \param soap client.

     \return list of objects to send.
    */
    function createSendArrayList( $soapClient )
    {
        $useModified = isset( $this->Fields['modified'] );

        if ( $useModified )
        {
            $request = new eZSOAPRequest( 'getLatestModified', eZNetSOAPSync::SYNC_NAMESPACE );
            $request->addParameter( 'className', $this->ClassName );
            $request->addParameter( 'hostID', eZNetUtils::hostID() );
            $response = $soapClient->send( $request );

            if( $response->isFault() )
            {
                eZDebug::writeNotice( 'Did not get valid result running SOAP method : getLatestModified, on class : ' . $this->ClassName );
                return false;
            }

            $latestModified = $response->value();

            $latestList = $this->fetchListByLatestModified( $latestModified );
        }
        else
        {
            $request = new eZSOAPRequest( 'getLatestID', eZNetSOAPSync::SYNC_NAMESPACE );
            $request->addParameter( 'className', $this->ClassName );
            $request->addParameter( 'hostID', eZNetUtils::hostID() );
            $response = $soapClient->send( $request );

            if( $response->isFault() )
            {
                eZDebug::writeNotice( 'Did not get valid result running SOAP method : getLatestID, on class : ' . $this->ClassName );
                return false;
            }

            $latestID = $response->value(); // Missing message IDs

            $latestList = $this->fetchListByLatestID( $latestID );
        }

        return $latestList;
    }

    /*!
     Create standard soap request for "Fetch by latest"

     \param optional number of objects to be fetched at one time, default 100

     \return Soap request
    */
    function createFetchListSoapRequest( $limit = 100 )
    {
        $useModified = isset( $this->Fields['modified'] );
        $cli = eZCLI::instance();

        if ( $useModified )
        {
            $request = new eZSOAPRequest( 'fetchListByHostIDLatestModified', eZNetSOAPSync::SYNC_NAMESPACE );
            $request->addParameter( 'className', $this->ClassName );
            $request->addParameter( 'hostID', eZNetUtils::hostID() );
            $request->addParameter( 'latestModified', $this->getLatestModified() );
            $cli->output( 'Synchronizing: ' . $this->ClassName . ', latest updated: ' . $this->getLatestModified() );
        }
        else
        {
            $request = new eZSOAPRequest( 'fetchListByHostIDLatestID', eZNetSOAPSync::SYNC_NAMESPACE );
            $request->addParameter( 'className', $this->ClassName );
            $request->addParameter( 'hostID', eZNetUtils::hostID() );
            $request->addParameter( 'latestID', $this->getLatestID() );
            $cli->output( 'Synchronizing: ' . $this->ClassName . ', id: ' . $this->getLatestID() );
        }

        return $request;
    }

}

?>
