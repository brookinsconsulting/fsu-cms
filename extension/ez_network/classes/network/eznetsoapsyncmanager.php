<?php
//
// Definition of eZNetSOAPSyncManager class
//
// Created on: <05-Jul-2007 14:12:31 hovik>
//
// Copyright (C) 1999-2005 eZ systems as. All rights reserved.
//
// This source file is part of the eZ publish (tm) Open Source Content
// Management System.
//
// This file may be distributed and/or modified under the terms of the
// "GNU General Public License" version 2 as published by the Free
// Software Foundation and appearing in the file LICENSE included in
// the packaging of this file.
//
// Licencees holding a valid "eZ publish professional licence" version 2
// may use this file in accordance with the "eZ publish professional licence"
// version 2 Agreement provided with the Software.
//
// This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING
// THE WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR
// PURPOSE.
//
// The "eZ publish professional licence" version 2 is available at
// http://ez.no/ez_publish/licences/professional/ and in the file
// PROFESSIONAL_LICENCE included in the packaging of this file.
// For pricing of this licence please contact us via e-mail to licence@ez.no.
// Further contact information is available at http://ez.no/company/contact/.
//
// The "GNU General Public License" (GPL) is available at
// http://www.gnu.org/copyleft/gpl.html.
//
// Contact licence@ez.no if any conditions of this licencing isn't clear to
// you.
//

/*! \file eznetsoapsyncmanager.php
*/

/*!
  \class eZNetSOAPSyncManager eznetsoapsyncmanager.php
  \brief The class eZNetSOAPSyncManager manages SOAP syncronizations.
         The class is used by the client to handle SOAP syncronizations.

*/

class eZNetSOAPSyncManager
{
    /// Const
    const DefaultFetchLimit = 100;

    /*!
     Constructor

     \param eZSoapClient
     \param Class syncronization list
     \param CLI
    */
    function eZNetSOAPSyncManager( $soapClient,
                                   $classList,
                                   $cli )
    {
        $this->SOAPClient = $soapClient;
        $this->CLI =& $cli;
        $this->ClassList = $classList;
    }

    /*!
     Syncronize the class list provided in the constructor, using the SOAP client provided.
     */
    function syncronize()
    {
        $orderedClassList = eZNetSOAPSyncAdvanced::orderClassListByDependencies( $this->ClassList );
        $reversedClassList = array_reverse( $orderedClassList );

        $db = eZDB::instance();
        $db->begin();

        // Fetch max modified/ID for all classes which should be syncronized.
        $this->CLI->output( 'Fetching max remote values' );
        $maxValueList = array();
        foreach( $reversedClassList as $className )
        {
            $soapSync = new eZNetSOAPSync( call_user_func( array( $className, 'definition' ) ) );
            $maxValueList[$className] = $soapSync->maxRemoteValue( $this->SOAPClient );
        }

        foreach( $orderedClassList as $className )
        {
            $transferCount = 0;
            $transferSuccess = false;

            while( !$transferSuccess &&
                   $transferCount < 3 )
            {
                $messageSync = new eZNetSOAPSync( call_user_func( array( $className, 'definition' ) ) );
                $result = $messageSync->syncronize( $this->SOAPClient,
                                                    $this->fetchLimit( $className ),
                                                    $maxValueList[$className] );
                if ( $result )
                {
                    $transferSuccess = true;
                    $this->CLI->output( 'Imported : ' . $result['import_count'] . ' elements to Class : ' . $result['class_name'] );
                }
                else
                {
                    ++$transferCount;
                }
            }
            if ( !$transferSuccess )
            {
                $this->CLI->error( 'Syncronization of: ' . $className . ' failed. Aborting syncronization.' );
                break;
            }
        }

        $db->commit();
    }

    /*!
     Syncronize the client class list provided in the constructor, using the SOAP client provided.
     */
    function syncronizeClient()
    {
        $orderedClassList = eZNetSOAPSyncAdvanced::orderClassListByDependencies( $this->ClassList );
        $reversedClassList = array_reverse( $orderedClassList );

        $db = eZDB::instance();
        $db->begin();

        // Fetch max modified/ID for all classes which should be syncronized.
        $this->CLI->output( 'Fetching max remote values' );
        $maxValueList = array();
        foreach( $reversedClassList as $className )
        {
            $soapSync = new eZNetSOAPSyncClient( call_user_func( array( $className, 'definition' ) ) );
            $maxValueList[$className] = $soapSync->maxRemoteValue( $this->SOAPClient );
        }

        foreach( $orderedClassList as $className )
        {
            $transferCount = 0;
            $transferSuccess = false;

            while( !$transferSuccess &&
                   $transferCount < 3 )
            {
                $messageSync = new eZNetSOAPSyncClient( call_user_func( array( $className, 'definition' ) ) );
                $result = $messageSync->syncronize( $this->SOAPClient,
                                                    $this->fetchLimit( $className ),
                                                    $maxValueList[$className] );
                if ( $result )
                {
                    $transferSuccess = true;
                    $this->CLI->output( 'Imported : ' . $result['import_count'] . ' elements to Class : ' . $result['class_name'] );
                }
                else
                {
                    ++$transferCount;
                }
            }
            if ( !$transferSuccess )
            {
                $this->CLI->error( 'Syncronization of: ' . $className . ' failed. Aborting syncronization.' );
                break;
            }
        }

        $db->commit();
    }

    /*!
     \private
     Get list of custom class fetch limits

     \return custom class fetch limits
     */
    function customClassFetchLimit()
    {
        return array( 'eZNetPatch' => 1 );
    }

    /*!
     \private

     \param class name

     \return fetch limit
    */
    function fetchLimit( $className )
    {
        $customFetchList = $this->customClassFetchLimit();
        return isset( $customFetchList[$className] ) ?
            $customFetchList[$className] :
            eZNetSOAPSyncManager::DefaultFetchLimit;
    }

    /// Class variables
    var $SOAPClient;
    var $CLI;
    var $ClassList;
}

?>
