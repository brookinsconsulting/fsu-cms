<?php
//
// Definition of eZNetSOAPSyncAdvanced class
//
// Created on: <14-Nov-2005 12:53:18 hovik>
//
// Copyright (C) 1999-2005 eZ systems as. All rights reserved.
//
// This source file is part of the eZ publish (tm) Open Source Content
// Management System.
//
// This file may be distributed and/or modified under the terms of the
// "GNU General Public License" version 2 as published by the Free
// Software Foundation and appearing in the file LICENSE included in
// the packaging of this file.
//
// Licencees holding a valid "eZ publish professional licence" version 2
// may use this file in accordance with the "eZ publish professional licence"
// version 2 Agreement provided with the Software.
//
// This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING
// THE WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR
// PURPOSE.
//
// The "eZ publish professional licence" version 2 is available at
// http://ez.no/ez_publish/licences/professional/ and in the file
// PROFESSIONAL_LICENCE included in the packaging of this file.
// For pricing of this licence please contact us via e-mail to licence@ez.no.
// Further contact information is available at http://ez.no/company/contact/.
//
// The "GNU General Public License" (GPL) is available at
// http://www.gnu.org/copyleft/gpl.html.
//
// Contact licence@ez.no if any conditions of this licencing isn't clear to
// you.
//

/*! \file eznetsoapsyncadvanced.php
*/

/*!
  \class eZNetSOAPSyncAdvanced eznetsoapsyncadvanced.php
  \brief The class eZNetSOAPSyncAdvanced does

*/


class eZNetSOAPSyncAdvanced extends eZNetSOAPSync
{

    /*!
     \reimp
    */
    function functionDefinitionList()
    {
        return array_merge( eZNetSOAPSync::functionDefinitionList() );
    }


    /*!
     \static
     Calculate dependencies of list classes to syncronize. TODO : use more efficient algo than brute force.

     \param list of classes to syncronize.

     \return Ordered list of classes to syncronize
    */
    function orderClassListByDependencies( $classList )
    {
        $tmpDependencyList = array();

        // build rough depency list
        foreach( $classList as $className )
        {
            $tmpDependencyList[$className] = array();
            
            // this causes a seg fault: (see issue #001996 in network-project issue tracker)
            //$definition = call_user_func_array( array( $className, "definition" ),
            //                                    array() );
            
            $definition = call_user_func( array( $className, "definition" ) );
            
            foreach( $definition['fields'] as $attributeDefinition )
            {
                if ( isset( $attributeDefinition['foreign_class'] ) )
                {
                    $tmpDependencyList[$className][] = $attributeDefinition['foreign_class'];
                }
                if ( isset( $attributeDefinition['foreign_override_class'] ) )
                {
                    $tmpDependencyList[$attributeDefinition['foreign_override_class']][] = $className;
                }
            }
        }

        // cleanup rough list
        foreach( $tmpDependencyList as $className => $classDependencyList )
        {
            $tmpDependencyList[$className] = array_unique( $classDependencyList );
            foreach( $tmpDependencyList[$className] as $key => $value )
            {
                if ( !in_array( $value, array_keys( $tmpDependencyList ) ) )
                {
                    unset( $tmpDependencyList[$className][$key] );
                }
                if ( $className == $value )
                {
                    unset( $tmpDependencyList[$className][$key] );
                }
            }
        }

        $dependencyList = array();
        // Build complete list
        for ( $i = 0 ; $i <= count( $tmpDependencyList ); $i++ )
        {
            $classList2 = array();
            foreach( $tmpDependencyList as $className => $classDependencyList )
            {
                if ( count( $classDependencyList) == 0 )
                {
                    $classList2[] = $className;
                }
            }

            foreach( $classList2 as $className )
            {
                unset( $tmpDependencyList[$className] );
            }

            foreach( $tmpDependencyList as $className => $classDependencyList )
            {
                foreach( $classDependencyList as $innerKey => $innerValue )
                {
                    if ( in_array( $innerValue, $classList2 ) )
                    {
                        unset( $tmpDependencyList[$className][$innerKey] );
                    }
                }
            }

            $dependencyList = array_merge( $dependencyList, $classList2 );
        }

        // Append list of failed dependencies
        foreach( $tmpDependencyList as $className => $value )
        {
            $dependencyList[] = $className;
        }

        foreach( $dependencyList as $key => $className )
        {
            if ( !in_array( $className, $classList ) )
            {
                unset( $dependencyList[$key] );
            }
        }

        return $dependencyList;
    }
}

?>
