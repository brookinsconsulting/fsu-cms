<?php
//
// Definition of eZNetInstallationInfo class
//
// Created on: <24-Aug-2006 15:45:56 hovik>
//
// Copyright (C) 1999-2005 eZ systems as. All rights reserved.
//
// This source file is part of the eZ publish (tm) Open Source Content
// Management System.
//
// This file may be distributed and/or modified under the terms of the
// "GNU General Public License" version 2 as published by the Free
// Software Foundation and appearing in the file LICENSE included in
// the packaging of this file.
//
// Licencees holding a valid "eZ publish professional licence" version 2
// may use this file in accordance with the "eZ publish professional licence"
// version 2 Agreement provided with the Software.
//
// This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING
// THE WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR
// PURPOSE.
//
// The "eZ publish professional licence" version 2 is available at
// http://ez.no/ez_publish/licences/professional/ and in the file
// PROFESSIONAL_LICENCE included in the packaging of this file.
// For pricing of this licence please contact us via e-mail to licence@ez.no.
// Further contact information is available at http://ez.no/company/contact/.
//
// The "GNU General Public License" (GPL) is available at
// http://www.gnu.org/copyleft/gpl.html.
//
// Contact licence@ez.no if any conditions of this licencing isn't clear to
// you.
//

/*! \file eznetlicenseinfo.php
*/

/*!
  \class eZNetInstallationInfo eznetinstallationinfo.php
  \brief The class eZNetInstallationInfo does

*/


class eZNetInstallationInfo extends eZPersistentObject
{
    /// Consts
    const StatusDraft = 0;
    const StatusPublished = 1;

    const CreateProjectDisbled = 0;
    const CreateProjectEnabled = 1;

    const ProjectCreatedFalse = 0;
    const ProjectCreatedTrue = 1;

    const PatchRetrievalModeAuto = 0;
    const PatchRetrievalModeeZPatch = 1;
    const PatchRetrievalModeTextDiff = 2;


    /*!
     Constructor
    */
    function eZNetInstallationInfo( $row = array() )
    {
        $this->NetUtils = new eZNetUtils();
        $this->eZPersistentObject( $row );
    }

    static function definition()
    {
        return array( "fields" => array( "id" => array( 'name' => 'ID',
                                                        'datatype' => 'integer',
                                                        'default' => 0,
                                                        'required' => true ),
                                         "installation_id" => array( 'name' => 'Name',
                                                                     'datatype' => 'string',
                                                                     'default' => '',
                                                                     'required' => true,
                                                                     'foreign_class' => 'eZNetInstallation',
                                                                     'foreign_attribute' => 'id',
                                                                     'multiplicity' => '1..*' ),
                                         "status" => array( 'name' => 'Status',
                                                            'datatype' => 'integer',
                                                            'default' => 0,
                                                            'required' => true,
                                                            'keep_key' => true ),
                                         "local_ip" => array( 'name' => 'LocalIP',
                                                              'datatype' => 'string',
                                                              'default' => '',
                                                              'required' => true ),
                                         "public_ip" => array( 'name' => 'PublicIP',
                                                               'datatype' => 'string',
                                                               'default' => '',
                                                               'required' => true ),
                                         'created' => array( 'name' => 'Created',
                                                             'datatype' => 'integer',
                                                             'default' => 0,
                                                             'required' => true ),
                                         'creator_id' => array( 'name' => 'CreatorID',
                                                                'datatype' => 'integer',
                                                                'default' => 0,
                                                                'required' => true ),
                                         'modified' => array( 'name' => 'Modified',
                                                              'datatype' => 'integer',
                                                              'default' => 0,
                                                              'required' => true ),
                                         "modifier_id" => array( 'name' => 'ModifierID',
                                                                 'datatype' => 'string',
                                                                 'default' => '',
                                                                 'required' => true ),
                                         'options' => array( 'name' => 'Options',
                                                             'datatype' => 'string',
                                                             'default' => '',
                                                             'required' => true ),
                                         "extension_version" => array( 'name' => 'ExtensionVersion',
                                                                       'datatype' => 'string',
                                                                       'default' => '',
                                                                       'required' => true ) ),
                      "keys" => array( "id", 'status' ),
                      "function_attributes" => array( 'creator' => 'creator',
                                                      'modifier' => 'modifier',
                                                      'additional_ip_list' => 'additionalIPList',
                                                      'node_ip_map' => 'nodeIPMap',
                                                      'option_array' => 'optionArray',
                                                      'node_id_list' => 'nodeIDList',
                                                      'installation' => 'installation' ),
                      "increment_key" => "id",
                      "class_name" => "eZNetInstallationInfo",
                      "sort" => array( "id" => "asc" ),
                      "name" => "ezx_ezpnet_installation_info" );
    }

    /*!
    \reimp
    */
    function attribute( $attr, $noFunction = false )
    {
        $retVal = null;
        switch( $attr )
        {
            case 'node_ip_map':
            {
                $retVal = array( '' => $this->attribute( 'local_ip' ) );
                foreach( $this->attribute( 'additional_ip_list' ) as $additionalIP )
                {
                    $retVal[$this->nodeID( $additionalIP )] = $additionalIP;
                }
            } break;

            case 'installation':
            {
                $retVal = eZNetInstallation::fetch( $this->attribute( 'installation_id' ) );
            } break;

            case 'node_id_list':
            {
                $retVal = array( $this->nodeID( $this->attribute( 'local_ip' ) ) );
                foreach( $this->attribute( 'additional_ip_list' ) as $extraIP )
                {
                    $retVal[] = $this->nodeID( $extraIP );
                }
            } break;

            case 'creator':
            {
                $retVal = eZUser::fetch( $this->attribute( 'creator_id' ) );
            } break;

            case 'modifier':
            {
                $retVal = eZUser::fetch( $this->attribute( 'modifier_id' ) );
            } break;

            case 'option_array':
            {
                $optionDef = $this->attribute( 'options' );
                $retVal = $optionDef == '' ? array() : unserialize( $optionDef );
            } break;

            case 'additional_ip_list':
            {
                $retVal = explode( ';', $this->option( 'additional_ip_list' ) );
                foreach( $retVal as $key => $value )
                {
                    if ( !$value )
                    {
                        unset( $retVal[$key] );
                    }
                }
            } break;

            default:
            {
                $retVal = eZPersistentObject::attribute( $attr );
            } break;
        }

        return $retVal;
    }

    /*!
     \static

     Create new eZNetInstallationInfo info
    */
    function create( $installationID )
    {
        $installationInfo = new eZNetInstallationInfo( array( 'status' => eZNetInstallationInfo::StatusDraft,
                                                              'installation_id' => $installationID,
                                                              'created' => mktime(),
                                                              'creator_id' => eZUser::currentUserID() ) );
        $installationInfo->setOption( 'create_project', eZNetInstallationInfo::CreateProjectEnabled );
        return $installationInfo;
    }

    /*!
     Publish current object
    */
    function publish()
    {
        $this->setAttribute( 'status', eZNetInstallationInfo::StatusPublished );
        $this->setAttribute( 'modified', mktime() );
        $this->setAttribute( 'modifier_id', eZUser::currentUserID() );
        $this->store();
        $this->removeDraft();
    }

    /*!
     Fetch by installation ID
    */
    function fetchByInstallationID( $installationID,
                                    $status = eZNetInstallationInfo::StatusPublished,
                                    $modifierSet = false,
                                    $asObject = true )
    {
        $condArray = array( 'installation_id' => $installationID,
                            'status' => $status );
        if ( $modifierSet )
        {
            $condArray['modifier_id'] = array( '!=', 0 );
        }

        return eZNetInstallationInfo::fetchObject( eZNetInstallationInfo::definition(),
                                                   null,
                                                   $condArray,
                                                   $asObject );
    }

    /*!
     \static

     Fetch draft. If no draft exist, create draft from existing published object
    */
    function fetchDraftByInstallationID( $installationID,
                                         $force = true,
                                         $asObject = true )
    {
        $draft = eZNetInstallationInfo::fetchByInstallationID( $installationID,
                                                               eZNetInstallationInfo::StatusDraft,
                                                               false,
                                                               $asObject );
        if ( !$draft &&
             $force )
        {
            $draft = eZNetInstallationInfo::fetchByInstallationID( $installationID,
                                                                   eZNetInstallationInfo::StatusPublished,
                                                                   true,
                                                                   $asObject );

            if ( $draft )
            {
                $draft->setAttribute( 'status', eZNetInstallationInfo::StatusDraft );
                $draft->sync();
            }
        }

        return $draft;
    }

    /*!
     Remove draft.
    */
    function removeDraft()
    {
        $draft = eZNetInstallationInfo::fetchDraft( $this->attribute( 'id' ),
                                                    false );
        if ( $draft )
        {
            $draft->remove();
        }
    }

    /*!
     \reimp
    */
    function fetch( $id,
                    $status = eZNetInstallationInfo::StatusPublished,
                    $asObject = true )
    {
        return eZNetInstallationInfo::fetchObject( eZNetInstallationInfo::definition(),
                                                   null,
                                                   array( 'id' => $id,
                                                          'status' => $status ),
                                                   $asObject );
    }

    /*!
     \static

     Fetch draft. If no draft exist, create draft from existing published object
    */
    function fetchDraft( $id,
                         $force = true,
                         $asObject = true )
    {
        $draft = eZNetInstallationInfo::fetch( $id,
                                               eZNetInstallationInfo::StatusDraft,
                                               $asObject );
        if ( !$draft &&
             $force )
        {
            $draft = eZNetInstallationInfo::fetch( $id,
                                                   eZNetInstallationInfo::StatusPublished,
                                                   $asObject );

            if ( $draft )
            {
                $draft->setAttribute( 'status', eZNetInstallationInfo::StatusDraft );
                $draft->sync();
            }
        }

        return $draft;
    }

    /*!
     Set option

     \param option name
     \param option value
    */
    function setOption( $attr, $value )
    {
        $optionArray = $this->attribute( 'option_array' );
        $optionArray[$attr] = $value;
        $this->setAttribute( 'options', serialize( $optionArray ) );
    }

    /*!
     Check if option is set.

     \param option name
    */
    function hasOption( $attr )
    {
        $optionArray = $this->attribute( 'option_array' );
        return isset( $optionArray[$attr] );
    }

    /*
     Get option

     \param option name

     \return option value
    */
    function option( $attr )
    {
        $optionArray = $this->attribute( 'option_array' );
        return isset( $optionArray[$attr] ) ? $optionArray[$attr] : false;
    }

    /*!
     Get node name from node id

     \param node ID

     \return node name
    */
    function nodeIP( $nodeID )
    {
        $nodeIPMap = $this->attribute( 'node_ip_map' );
        return $nodeIPMap[$nodeID];
    }

    /*!
     Get node name from node id

     \param node ID

     \return node name
    */
    function nodeName( $nodeID )
    {
        if ( $this->option( 'server_name_map' ) )
        {
            $nodeIPMap = $this->attribute( 'node_ip_map' );
            if ( $nodeIP = $this->nodeIP( $nodeID ) )
            {
                foreach( explode( ';', $this->option( 'server_name_map' ) ) as $nameMap )
                {
                    list( $ip, $name ) = explode( ':', $nameMap );
                    if ( $ip == $nodeIP )
                    {
                        return $name;
                    }
                }
            }
        }

        if ( $nodeID == '' )
        {
            return 'Main - ' . $this->attribute( 'local_ip' );
        }
        foreach( $this->attribute( 'additional_ip_list' ) as $additionalIP )
        {
            if ( $nodeID == $this->nodeID( $additionalIP ) )
            {
                return 'Node - ' . $additionalIP;
            }
        }

        return 'Unknown';
    }

    /*!
     Generate NodeID. Node ID for main local ip widll be an empty string.
     For additional IPs, it'll be the md5sum of the additional IP.

     \param local IP

     \return NodeID ( md5 hash )
    */
    function nodeID( $localIP )
    {
        if ( !in_array( $localIP, $this->attribute( 'additional_ip_list' ) ) )
        {
            return '';
        }

        return md5( $localIP );
    }

    /*!
     \static

     Fetch a list of branches based on installation remote ID.

    */
    function fetchListByRemoteIDAndLatestModified( $installationSiteID,
                                                   $latestModified,
                                                   $offset = 0,
                                                   $limit = 100,
                                                   $asObject = true,
                                                   $status = eZNetInstallation::StatusPublished )
    {
        $installation = eZNetInstallation::fetchBySiteID( $installationSiteID );
        if ( !$installation )
        {
            return false;
        }

        return eZNetInstallationInfo::fetchObjectList( eZNetInstallationInfo::definition(),
                                                       array( 'id' ),
                                                       array( 'installation_id' => $installation->attribute( 'id' ),
                                                              'modified' => array( '>', $latestModified ) ),
                                                       array( 'modified' => 'asc' ),
                                                       array( 'limit' => $limit,
                                                              'offset' => $offset ),
                                                       $asObject );
    }

    /*!
     \static

     Get patch mode name map
    */
    function createProjectNameMap()
    {
        return array( eZNetInstallationInfo::CreateProjectDisbled => ezi18n( 'crm', 'No' ),
                      eZNetInstallationInfo::CreateProjectEnabled => ezi18n( 'crm', 'Yes' ) );
    }

    /*!
     Return if project should be created

     \return true if project should be created, false of not.
    */
    function shouldCreateProject()
    {

        if ( $this->hasOption( 'project_created' ) &&
             $this->option( 'project_created' ) == eZNetInstallationInfo::ProjectCreatedTrue )
        {
            return false;
        }

        // $returnFalse used to make sure all function calls are successfull.
        $returnFalse = true;
        if ( $installation = $this->attribute( 'installation' ) )
        {
            if ( $topAgreementLink = $installation->attribute( 'top_level_agreement_link' ) )
            {
                if ( $agreement = $topAgreementLink->attribute( 'agreement' ) )
                {
                    $returnFalse = false;
                    if ( !in_array( $agreement->attribute( 'identifier' ),
                                    eZNetAgreement::issueAgreementIdentifierList() ) )
                    {
                        return false;
                    }
                }
            }
        }
        if ( $returnFalse )
        {
            return false;
        }

        return ( $this->hasOption( 'create_project' ) &&
                 $this->option( 'create_project' ) == eZNetInstallationInfo::CreateProjectEnabled );
    }

    /*!
     \static
     Retrieval mode name map

     \return retrieval mode name map.
    */
    function patchRetrievalModeNameMap()
    {
        return array( eZNetInstallationInfo::PatchRetrievalModeAuto => ezi18n( 'crm', 'Automatic' ),
                      eZNetInstallationInfo::PatchRetrievalModeeZPatch => ezi18n( 'crm', 'Manual - eZ Patch' ),
                      eZNetInstallationInfo::PatchRetrievalModeTextDiff => ezi18n( 'crm', 'Manual - Text diff' ) );
    }

    /*!
     \static
     Send email notification to branch/patch subscribers.

     \param eZNetPatch object
    */
    function sendPatchNotification( $patch )
    {

        $nwINI = eZINI::instance( 'network.ini' );
        $mail = new eZMail();
        $mail->setSender( $nwINI->variable( 'NotificationSettings', 'Sender' ) );
        $branchID = $patch->attribute( 'branch_id' );
        $offset = 0;
        $limit = 50;
        while( $installationList = eZNetInstallation::fetchListByBranchID( $branchID,
                                                                           $offset,
                                                                           $limit ) )
        {
            foreach( $installationList as $installation )
            {
                if ( $installationInfo = $installation->attribute( 'installation_info' ) )
                {
                    if ( $installationInfo->hasOption( 'email_patch_notification_list' ) )
                    {
                        if( $emailNotificationString = trim( $installationInfo->option( 'email_patch_notification_list' ) ) )
                        {
                            $branch = $patch->attribute( 'branch' );
                            $customer = $installation->attribute( 'customer' );
                            $subject = '[eZ Network - Patch notification] ' . $branch->attribute( 'name' ) .
                                ' - ' . $installation->attribute( 'name' ) .
                                ' - ' . $patch->attribute( 'name' );
                            $body = 'New update is available from the eZ Network team. ' .
                                'For more information, access your eZ Network portal on http://ez.no ' . "\n\n" .
                                'Installation: ' . $installation->attribute( 'name' ) . "\n" .
                                'Customer:     ' . $customer->attribute( 'name' ) . "\n" .
                                'Branch:       ' . $branch->attribute( 'name' ) . "\n" .
                                'Patch:        ' . $patch->attribute( 'name' ) . "\n" .
                                'Description:  ' . $patch->attribute( 'description' );
                            $mail->setSubject( $subject );
                            $mail->setBody( $body );

                            foreach( explode( ',', $emailNotificationString ) as $receiver )
                            {
                                eZDebug::writeNotice( 'Sending email ( ' . $subject . ' ) to: ' . $receiver,
                                                      'eZNetInstallationInfo::sendPatchNotification()' );
                                $mail->setReceiver( $receiver );
                                eZMailTransport::send( $mail );
                            }
                        }
                    }
                }
            }
            $offset += $limit;
        }
    }
}

?>
