<?php
//
// Definition of eZNetNWTools class
//
// Created on: <06-Sep-2007 09:58:59 hovik>
//
// Copyright (C) 1999-2005 eZ systems as. All rights reserved.
//
// This source file is part of the eZ publish (tm) Open Source Content
// Management System.
//
// This file may be distributed and/or modified under the terms of the
// "GNU General Public License" version 2 as published by the Free
// Software Foundation and appearing in the file LICENSE included in
// the packaging of this file.
//
// Licencees holding a valid "eZ publish professional licence" version 2
// may use this file in accordance with the "eZ publish professional licence"
// version 2 Agreement provided with the Software.
//
// This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING
// THE WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR
// PURPOSE.
//
// The "eZ publish professional licence" version 2 is available at
// http://ez.no/ez_publish/licences/professional/ and in the file
// PROFESSIONAL_LICENCE included in the packaging of this file.
// For pricing of this licence please contact us via e-mail to licence@ez.no.
// Further contact information is available at http://ez.no/company/contact/.
//
// The "GNU General Public License" (GPL) is available at
// http://www.gnu.org/copyleft/gpl.html.
//
// Contact licence@ez.no if any conditions of this licencing isn't clear to
// you.
//

/*! \file eznetnwtools.php
*/

/*!
  \class eZNetNWTools eznetnwtools.php
  \brief The class eZNetNWTools does

*/

class eZNetNWTools
{

    /*!
     \constructor
    */
    function eZNetNWTools()
    {
        $this->DB = eZDB::instance();
        $this->CLI = eZCLI::instance();
    }

    /*!
     Fetch SOAPLog for patch item

     \param eZNetPatchItem
     \param $remoteHost ( optional )

     \return eZNetSOAPLog
     */
    function fetchSoapLogForPatchItem( $patchItem, $remoteHost = false )
    {
        $condArray = array( 'class_name' => 'eZNetPatchItem',
                            'key_name' => 'id',
                            'local_value' => $patchItem->attribute( 'id' ) );

        if ( $remoteHost )
        {
            $condArray['remote_host'] = $remoteHost;
        }
        return eZNetSOAPLog::fetchObject( eZNetSOAPLog::definition(),
                                          null,
                                          $condArray,
                                          true );
    }

    /*!
     Check if patch item belongs to installation by connecting to the
     intranet, and check it's status there.

     \param patchItemID
     \param installationID

     \return TRUE if patch item belongs to installtion.
    */
    function patchItemBelongsToInstallation( $patchItemID, $installationID, $remoteHostID = false )
    {
        $installation = eZNetInstallation::fetch( $installationID );
        if ( !$installation )
        {
            return array( 'success' => false,
                          'description' => 'Could not fetch eZNetInstallation( ' . $installationID . ' )' );
        }

        $patchItem = eZNetPatchItem::fetch( $patchItemID);
        if ( !$patchItem )
        {
            return array( 'success' => false,
                          'description' => 'Could not fetch eZNetPatchItem( ' . $patchItemID . ' )' );
        }

        $soapLog = $this->fetchSoapLogForPatchItem( $patchItem, $remoteHostID );
        // If it can not find the eZNetSOAPLog, remove the item.
        if ( !$soapLog )
        {
            $this->removePatchItem( $patchItem );
            return array( 'success' => true,
                          'operation' => $this->opRemove );
        }

        // Ask server if eZNetPatchItem belongs to installation.
        $ini = eZINI::instance( 'sync.ini' );
        $Server = $ini->variable( 'NetworkSettings', 'Server' );
        $Port = $ini->variable( 'NetworkSettings', 'Port' );
        $Path = $ini->variable( 'NetworkSettings', 'Path' );

        $request = $this->createPatchItemBelongsToSOAPRequest( $soapLog->attribute( 'remote_value' ),
                                                               $installation->attribute( 'remote_id' ) );
        // If use of SSL fails the client must attempt to use HTTP
        $Port = eZNetSoapSync::getPort( $Server, $Path, $Port );

        $client = new eZSOAPClient( $Server, $Path, $Port );
        $response = $client->send( $request );

        if ( !$response ||
             $response->isFault() )
        {
            return array( 'success' => false,
                          'description' => 'Could not send SOAP request for eZNetPatchItem ( ' . $patchItem->attribute( 'id' ) . ' )' );
        }

        // Check server responce if the eZNetPatchItem belongs to the installation.
        if ( !$response->value() )
        {
            $this->removePatchItem( $patchItem );
            return array( 'success' => true,
                          'operation' => $this->opRemove );
        }

        return array( 'success' => true,
                      'operation' => $this->opKeep );

    }

    /*!
     Remove eZNetPatchItem result
    */
    function removePatchItem( $patchItem )
    {
        echo '.';
        $this->DB->begin();
        $this->cleanupSOAPLog( $patchItem );
        $patchItem->remove();
        $this->DB->commit();
    }

    /*!
     Clean up eZNetSOAPLog
    */
    function cleanupSOAPLog( $persistentObject )
    {
        $definition = $persistentObject->definition();
        foreach ( eZNetSOAPLog::fetchObjectList( eZNetSOAPLog::definition(),
                                                 null,
                                                 array( 'class_name' => $definition['class_name'],
                                                        'key_name' => $definition['keys'][0],
                                                        'local_value' => $persistentObject->attribute( $definition['keys'][0] ) ) )
                  as $soapLog )
        {
            $soapLog->remove();
        }
    }

    /*!
     Clean up obsolete patch items from previous bug in syncronization.
     This function will remove all patch items with missing patches.
     ( patch_id in eZNetPatchItem does not match any eZNetPatch entries )
    */
    function cleanupPatchItem()
    {
        $this->CLI->output( 'Cleaning up eZNetPatchItem missing patches or installation.' );
        $sql = 'DELETE FROM ezx_ezpnet_patch_item ' .
            'WHERE ezx_ezpnet_patch_item.patch_id <> ALL ( SELECT id FROM ezx_ezpnet_patch )';
        $this->DB->query( $sql );
        $sql = 'DELETE FROM ezx_ezpnet_patch_item ' .
            'WHERE ezx_ezpnet_patch_item.installation_id <> ALL ( SELECT id FROM ezx_ezpnet_installation )';
        $this->DB->query( $sql );
    }

    /*!
     Remove patch items where patch branch does not match installation branch
    */
    function cleanupInvalidPatchItems()
    {
        $this->CLI->output( 'Remove patch items with invalid branch ID' );
        $installationOffset = 0;
        $installationLimit = 20;
        while( $installationList = eZNetInstallation::fetchList( false,
                                                                 $installationOffset,
                                                                 $installationLimit ) )
        {
            foreach( $installationList as $installation )
            {
                $patchItemOffset = 0;
                $patchItemLimit = 50;
                while( $patchItemList = eZNetPatchItem::fetchListByInstallationID( $installation->attribute( 'id' ),
                                                                                   false,
                                                                                   $patchItemOffset,
                                                                                   $patchItemLimit ) )
                {
                    foreach( $patchItemList as $patchItem )
                    {
                        if ( $patchItem->attribute( 'branch_id' ) != $installation->attribute( 'branch_id' ) )
                        {
                            $this->removePatchItem( $patchItem );
                            --$patchItemOffset;
                        }
                    }
                    $patchItemOffset += $patchItemLimit;
                }
            }
            $installationOffset += $installationLimit;
        }
        echo "\n";
    }

    //// Vars

    var $DB;
    var $CLI;

    var $opRemove = 1;
    var $opKeep = 2;
}

?>
