<?php
//
// Definition of eZNetSOAPObject class
//
// Created on: <15-Jul-2005 00:38:44 hovik>
//
// Copyright (C) 1999-2005 eZ systems as. All rights reserved.
//
// This source file is part of the eZ publish (tm) Open Source Content
// Management System.
//
// This file may be distributed and/or modified under the terms of the
// "GNU General Public License" version 2 as published by the Free
// Software Foundation and appearing in the file LICENSE included in
// the packaging of this file.
//
// Licencees holding a valid "eZ publish professional licence" version 2
// may use this file in accordance with the "eZ publish professional licence"
// version 2 Agreement provided with the Software.
//
// This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING
// THE WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR
// PURPOSE.
//
// The "eZ publish professional licence" version 2 is available at
// http://ez.no/ez_publish/licences/professional/ and in the file
// PROFESSIONAL_LICENCE included in the packaging of this file.
// For pricing of this licence please contact us via e-mail to licence@ez.no.
// Further contact information is available at http://ez.no/company/contact/.
//
// The "GNU General Public License" (GPL) is available at
// http://www.gnu.org/copyleft/gpl.html.
//
// Contact licence@ez.no if any conditions of this licencing isn't clear to
// you.
//

/*! \file eznetsoapobject.php
*/

/*!
  \class eZNetSOAPObject eznetsoapobject.php
  \brief The class eZNetSOAPObject does

*/

class eZNetSOAPObject extends eZPersistentObject
{
    /*!
     Constructor
    */
    function eZNetSOAPObject( $row )
    {
        $this->eZPersistentObject( $row );
    }

    /*!
     Fetch missing object list specified by eZNetSOAP object ( using the eZNetSOAPLog )
    */
    function fetchMissingObjectList( $eZNetSOAP )
    {
        $latestLogEntry = $eZNetSOAP->latestLogEntry();
        $lastEventTS = 0;

        if ( $latestLogEntry )
        {
            $lastEventTS = $latestLogEntry->attribute( 'timestamp' );
        }

        $className = $eZNetSOAP->attribute( 'local_class' );
        $classDefinition = call_user_func( array( $className, 'definition' ) );

        return eZPersistentObject::fetchObjectList( $classDefinition,
                                                    null,
                                                    array( 'timestamp' => array( '>', $lastEventTS ) ) );
    }

    /*!
     Create string representation of current object
    */
    function soapString()
    {
        $definition = $this->definition();

        $objectArray = array();
        foreach( array_keys( $definition['fields'] ) as $attributeName )
        {
            $objectArray[$attributeName] = $this->attribute( $attributeName );
        }

        return serialize( $objectArray );
    }

    /*!
      Get local ID
    */
    function soapLocalID()
    {
        return $this->attribute( 'id' );
    }
}

?>
