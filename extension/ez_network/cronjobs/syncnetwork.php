<?php
//
// Created on: <23-Feb-2006 12:33:41 hovik>
//
// Copyright (C) 1999-2005 eZ systems as. All rights reserved.
//
// This source file is part of the eZ publish (tm) Open Source Content
// Management System.
//
// This file may be distributed and/or modified under the terms of the
// "GNU General Public License" version 2 as published by the Free
// Software Foundation and appearing in the file LICENSE included in
// the packaging of this file.
//
// Licencees holding a valid "eZ publish professional licence" version 2
// may use this file in accordance with the "eZ publish professional licence"
// version 2 Agreement provided with the Software.
//
// This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING
// THE WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR
// PURPOSE.
//
// The "eZ publish professional licence" version 2 is available at
// http://ez.no/ez_publish/licences/professional/ and in the file
// PROFESSIONAL_LICENCE included in the packaging of this file.
// For pricing of this licence please contact us via e-mail to licence@ez.no.
// Further contact information is available at http://ez.no/company/contact/.
//
// The "GNU General Public License" (GPL) is available at
// http://www.gnu.org/copyleft/gpl.html.
//
// Contact licence@ez.no if any conditions of this licencing isn't clear to
// you.
//

/*! \file syncnetwork.php
*/

@ini_set( 'memory_limit', '512M' );
$GLOBALS['eZDebugEnabled'] = false;

require 'extension/ez_network/classes/include_all.php';

if ( !$isQuiet )
{
    $cli->output( 'Starting eZ Network syncronization.' . "\n" .
                  'Use the --clear-all to reset client side data.' );
}

$clearAll = false;
foreach( $GLOBALS['argv'] as $argument )
{
    if ( $argument === '--clear-all' )
    {
        $clearAll = true;
    }
}

// Make sure network extensions is up to date.
$clientInfo = eZNetClientInfo::instance();
if ( !$clientInfo->validate() )
{
    return;
}

if ( $clearAll )
{
    $cli->output( 'Clearing existing data.' );
    $clientInfo->clearDB();
}

$classList = array( 'eZNetBranch',
                    'eZNetPatch',
                    'eZNetPatchItem',
                    'eZNetInstallation',
                    'eZNetModuleInstallation',
                    'eZNetModuleBranch',
                    'eZNetModulePatch',
                    'eZNetModulePatchItem',
                    'eZNetMonitorItem',
                    'eZNetMonitorGroup' );

$syncINI = eZINI::instance( 'sync.ini' );
$Server = $syncINI->variable( 'NetworkSettings', 'Server' );
$Port = $syncINI->variable( 'NetworkSettings', 'Port' );
$Path = $syncINI->variable( 'NetworkSettings', 'Path' );

// If use of SSL fails the client must attempt to use HTTP
$Port = eZNetSoapSync::getPort( $Server, $Path, $Port );

$client = new eZSOAPClient( $Server, $Path, $Port );

$networkInfo = eZNetUtils::extensionInfo( 'ez_network' );
$cli->output( $networkInfo['name'] . ' client ' . $networkInfo['version'] );
$cli->output( '' );

// Initialize Soap sync manager, and start syncronization.
$syncManager = new eZNetSOAPSyncManager( $client,
                                         $classList,
                                         $cli );
$syncManager->syncronizeClient();

/* Check existence of eZNetInstallation object in current DB.
 * If the first synchronizing was not successful then eZNetIntsllation doesn't exist.
 * We should notify the user about that.
 * NOTE: Failed synchronizing can be caused through incorrect installation key.
 */

// If eZNetInstallation should not be synchronized We should not check the existence.
if ( !in_array( 'eZNetInstallation', $classList ) )
    return;

$hostID = eZNetSOAPSync::hostID();
$installation = eZNetInstallation::fetchBySiteID( $hostID );
if ( !$installation )
{
    $cli->output( "\n".
                  'eZNetInstallation object with the installation key (' . $hostID . ') was not found in the database.' . "\n" .
                  'Make sure the installation key is correct and has been sent to eZ Systems' . "\n" .
                  'or contact your system administrator.' );
}


?>
