{section show=$is_upload}
<script language="Javascript" for="window" event="onload">
<!--
// -->
</script>

<script language="Javascript" for="uploadButton" event="onclick">
    document.getElementById("warningText").style.visibility="visible";
</script>

<div class="onlineeditor">

<h1>{"Upload local file"|i18n("design/standard/ezdhtml")}</h1>
<form enctype="multipart/form-data" action={concat("ezdhtml/insertobject","/",$object_id,"/",$object_version,"/")|ezurl} method="post">

<div class="block">
<label>{"Location"|i18n("design/standard/ezdhtml")}:</label>
<select name="location">
<option value="auto">{"Automatic"|i18n("design/standard/ezdhtml")}</option>
    {let root_node_value=ezini( 'LocationSettings', 'RootNode', 'upload.ini' )
         root_node=cond( $root_node_value|is_numeric, fetch( content, node, hash( node_id, $root_node_value ) ),
                         fetch( content, node, hash( node_path, $root_node_value ) ) )}
    {section var=node loop=fetch( content, tree,
                                  hash( parent_node_id, $root_node.node_id,
                                        class_filter_type, include,
                                        class_filter_array, ezini( 'LocationSettings', 'ClassList', 'upload.ini' ),
                                        depth, ezini( 'LocationSettings', 'MaxDepth', 'upload.ini' ),
                                        limit, ezini( 'LocationSettings', 'MaxItems', 'upload.ini' ) ) )}
        <option value="{$node.node_id}">{'&nbsp;'|repeat( sub( $node.depth, $root_node.depth, 1 ) )}{$node.name|wash}</option>
    {/section}
    {/let}
</select>
</div>

<div class="block">
<label>{"File"|i18n("design/standard/ezdhtml")}:</label>
<input class="box" size="48" name="fileName" type="file" />
<input type="hidden" name="MAX_FILE_SIZE" value="50000000" />
</div>

<input class="button" type="submit" name="uploadButton" id="uploadButton" value="{'OK'|i18n('design/standard/ezdhtml')}" />
<input class="button" type="submit" name="cancelButton" id="cancelButton" value="{'Cancel'|i18n('design/standard/ezdhtml')}" />

<div class="message-warning" style="visibility: hidden" id="warningText">
<h2>{"Upload is in progress, it may take a few seconds ..."|i18n("design/standard/ezdhtml")}</h2>
</div>

</form>

<div style="height:400px;">
</div>

{section-else}
<script language="Javascript" for="window" event="onload">
    <!--
    // When window gets created, set some default values
    var objectID = "";
    var customAttributes = -1;
    var objectSrc = "";
    var imageExist = false;
    try
    {ldelim}
        objectID = window.dialogArguments["objectID"];
        ContentObjectAlignment.value = window.dialogArguments["objectAlign"];
        ContentObjectSize.value = window.dialogArguments["objectSize"];
        ObjectClass.value = window.dialogArguments["objectClass"];
        ObjectView.value = window.dialogArguments["objectView"];
        HtmlID.value = window.dialogArguments["htmlID"];
        customAttributes = window.dialogArguments["customAttributes"];
        objectSrc = window.dialogArguments["objectSrc"];
        imageExist  = window.dialogArguments["imageExist"];
    {rdelim}
    catch( e )
    {ldelim}
    {rdelim}
    var objectIDString = "";
    var imageSrc = "";
    if ( objectID != "")
        objectIDString = "id='" + objectID;
    else
        objectIDString = "none";
    var isObject = objectSrc.match(/object_insert/g);
    if ( customAttributes != -1 )
         getAttributes( customAttributes );
    try
    {ldelim}
        for (var index=0;index<ContentObjectIDString.length;index++)
        {ldelim}
            if (ContentObjectIDString[index].value.match(objectIDString) )
            {ldelim}
                if ( objectID != "" )
                    ContentObjectIDString[index].checked = true;
                ContentObjectIDString[index].className = "selected";
                imageSrc = ContentObjectIDString[index].value;
            {rdelim}
        {rdelim}
    {rdelim}
    catch( e )
    {ldelim}
    {rdelim}
    if ( isObject )
         ContentObjectSize.disabled=true;
    else
         ContentObjectSize.disabled=false;
// -->
</script>

<script language="Javascript" for="ok" event="onclick">
    <!--
    var arr = new Array();

    // Contains object id and object icon path or image src path. The source has to be a proper "http://" URL
    // otherwise the editor doesn' show the image!
    var objectIDString = "";
    var ContentObjectIDString = document.getElementsByName("ContentObjectIDString");
    try
        {ldelim}
        for (var i=0;i<ContentObjectIDString.length;i++)
        {ldelim}
            if ( ContentObjectIDString[i].checked == true)
            {ldelim}
                objectIDString = ContentObjectIDString[i].value;
            {rdelim}
        {rdelim}
    {rdelim}
    catch( e )
    {ldelim}
    {rdelim}

    if (CustomAttributeValue.length > 0)
    {ldelim}
    var myAltValue = CustomAttributeValue[0].value;
    {rdelim}
    else
    {ldelim}
    var myAltValue = CustomAttributeValue.value;
    {rdelim}

    if (myAltValue  == '' && objectIDString.indexOf('object_insert') == -1) 
    {ldelim}
        alert( "You must enter a text alternative for this image!" );
	  return;
    {rdelim}
    if ( objectIDString != "" )
    {ldelim}
        arr["objectIDString"] = objectIDString;
        arr["objectAlign"] = ContentObjectAlignment.value;
        arr["objectSize"] = ContentObjectSize.value;
        if ( ContentObjectSize.disabled )
            arr["objectSize"] = "";
        arr["objectClass"] = ObjectClass.value;
        arr["objectView"] = ObjectView.value;
        arr["htmlID"] = HtmlID.value;
        arr["customAttributes"] = new Array();
        if ( typeof CustomAttributeName != 'undefined' && typeof CustomAttributeName != 'unknown' )
        {ldelim}
            var customAttributes = new Array();
        if ( CustomAttributeName.length != null )
        {ldelim}
                for (var i=0;i<CustomAttributeName.length;i++)
            {ldelim}
                if ( CustomAttributeValue.length != null )
                customAttributes[i]=CustomAttributeName[i].value + "|" + CustomAttributeValue[i].value;
            else
                customAttributes[i]=CustomAttributeName[i].value + "|" + CustomAttributeValue.value
            {rdelim}
        {rdelim}
        else
        {ldelim}
                customAttributes[0]=CustomAttributeName.value + "|" + CustomAttributeValue.value;
        {rdelim}
        arr["customAttributes"] = customAttributes;
        {rdelim}
        window.returnValue = arr;
        window.close();
    {rdelim}
    else
    {ldelim}
        alert( "You must select an object to insert" );
    {rdelim}
// -->
</script>

<script language="Javascript" for="ContentObjectIDString" event="onclick">
    var objectIDString = "";
    for (var i=0;i<ContentObjectIDString.length;i++)
    {ldelim}
        if ( ContentObjectIDString[i].checked == true)
        {ldelim}
            objectIDString = ContentObjectIDString[i].value;
            ContentObjectIDString[i].className = "selected";
        {rdelim}
        else
            ContentObjectIDString[i].className = "unselected";
    {rdelim}
    var isObject = objectIDString.match(/object_insert/g);
    if ( objectIDString != "none" )
        ok.disabled=false;
    if ( isObject )
    {ldelim}
        ContentObjectSize.disabled=true;
        sizeRow.disabled=true;
    {rdelim}
    else
    {ldelim}
        ContentObjectSize.disabled=false;
        sizeRow.disabled=false;
    {rdelim}
</script>

<script language="Javascript" for="ObjectClass" event="onclick">
    ok.disabled=false;
</script>

<script language="Javascript">
function getAttributes( customAttributes )
{ldelim}
    var attributeArray = customAttributes.split("attribute_separation");
    for (var i=0;i<attributeArray.length;i++)
    {ldelim}
        var attributePair = attributeArray[i].split("|");
	  if (attributePair[0].toLowerCase() == 'alt')
	  {ldelim}
		if (CustomAttributeValue.length > 0)
	     {ldelim}
		   CustomAttributeValue[0].value = attributePair[1];
	     {rdelim}
    	     else
	     {ldelim}
		   CustomAttributeValue.value = attributePair[1];
	     {rdelim}
	  {rdelim}
    	  else
	  {ldelim}
		addAttribute( attributePair[0], attributePair[1] );
	  {rdelim}
    {rdelim}
{rdelim}

function addNew()
{ldelim}
    var tbody = document.getElementById("attributes").getElementsByTagName("tbody")[0];
    var row1 = document.createElement("tr");
    var td1 = document.createElement("td");
    var checkbox1 = document.createElement("<input type='checkbox' id='Selected_attribute' />");
    td1.appendChild(checkbox1);
    var td2 = document.createElement("td");
    var input1 = document.createElement("<input id='CustomAttributeName' type='text' size='10' />");
    td2.appendChild(input1);
    var td3 = document.createElement("td");
    var input2 = document.createElement("<input id='CustomAttributeValue' type='text' size='10' />");
    td3.appendChild(input2);
    row1.appendChild(td1);
    row1.appendChild(td2);
    row1.appendChild(td3);
    tbody.insertBefore(row1,lastPropertyRow);
{rdelim}

function addAttribute( attrName, attrValue )
{ldelim}
    var tbody = document.getElementById("attributes").getElementsByTagName("tbody")[0];
    var row1 = document.createElement("tr");
    var td1 = document.createElement("td");
    var checkbox1 = document.createElement("<input type='checkbox' id='Selected_attribute' />");
    td1.appendChild(checkbox1);
    var td2 = document.createElement("td");
    var input1 = document.createElement("<input id='CustomAttributeName' type='text' size='10' />");
    input1.value = attrName;
    td2.appendChild(input1);
    var td3 = document.createElement("td");
    var input2 = document.createElement("<input id='CustomAttributeValue' type='text' size='10' />");
    input2.value = attrValue;
    td3.appendChild(input2);
    row1.appendChild(td1);
    row1.appendChild(td2);
    row1.appendChild(td3);
    tbody.insertBefore(row1,lastPropertyRow);
{rdelim}

function removeSelected()
{ldelim}
if ( typeof Selected_attribute != 'undefined' )
{ldelim}
	if ( Selected_attribute.length != null )
	{ldelim}
        var deleteArray = new Array();
	    var deleteArrayIndex = 0;
	    for (var i=0;i<Selected_attribute.length;i++)
	    {ldelim}
            if ( Selected_attribute[i].checked == true )
		    {ldelim}
	            deleteArray[deleteArrayIndex] = i;
		      deleteArrayIndex++;
	           {rdelim}
        {rdelim}
        var baseIndex = 6;
        for (var j=0;j<deleteArray.length;j++)
	    {ldelim}
	        rowIndex = baseIndex + deleteArray[j];
	        document.all.attributes.deleteRow(rowIndex);
	        baseIndex--;
	    {rdelim}
    {rdelim}
    else
	{ldelim}
        if ( Selected_attribute.checked == true )
	        document.all.attributes.deleteRow(6);
    {rdelim}
{rdelim}
{rdelim}
</script>

<div class="onlineeditor">
<h1>{"Select from related objects or upload local files"|i18n("design/standard/ezdhtml")}</h1>

<fieldset>
<legend>{'Related objects'|i18n('design/standard/ezdhtml')}</legend>

{section show=$related_contentobjects|count|gt( 0 )}

    {* Related images *}
    {section show=$grouped_related_contentobjects.images|count|gt( 0 )}
    <h2>{'Related images'|i18n('design/standard/ezdhtml')}</h2>
    <table class="list-thumbnails" cellspacing="0" width="100%">
    <tr>
        {section var=RelatedImageObjects loop=$grouped_related_contentobjects.images}
        <td>
        <div class="image-thumbnail-item">
            {attribute_view_gui attribute=$RelatedImageObjects.RelatedObject.data_map.image image_class=small}
            <p>
                <input type="radio" {section show=$RelatedImageObjects.ObjectIsSelected}class="selected"{/section} name="ContentObjectIDString" value="{$RelatedImageObjects.ObjectIDString}" {section show=$RelatedImageObjects.ObjectIsSelected}checked{/section}/>
                {$RelatedImageObjects.RelatedObject.name|wash}
           </p>
        </div>
        </td>
        {delimiter modulo=4}
        </tr><tr>
        {/delimiter}
        {/section}
    </tr>
    </table>
    {/section}

    {* Related files *}
    {section show=$grouped_related_contentobjects.files|count|gt( 0 )}
    <h2>{'Related files'|i18n('design/standard/ezdhtml')}</h2>
            <table class="list" cellspacing="0">
            <tr>
                <th class="tight">&nbsp;</th>
                <th class="name">{'Name'|i18n( 'design/admin/content/edit' )}</th>
                <th class="class">{'File type'|i18n( 'design/admin/content/edit' )}</th>
                <th class="filesize">{'Size'|i18n( 'design/admin/content/edit' )}</th>
            </tr>

            {section var=RelatedFileObjects loop=$grouped_related_contentobjects.files sequence=array( bglight, bgdark )}
                <tr class="{$RelatedFileObjects.sequence|wash}">
                    <td><input type="radio" {section show=$RelatedFileObjects.ObjectIsSelected}class="selected"{/section} name="ContentObjectIDString" value="{$RelatedFileObjects.ObjectIDString}" {section show=$RelatedFileObjects.ObjectIsSelected}checked{/section} /></td>
                    <td class="name">{$RelatedFileObjects.RelatedObject.class_name|class_icon( small, $RelatedFileObjects.RelatedObject.class_name )}&nbsp;{$RelatedFileObjects.RelatedObject.name|wash}</td>
                    <td class="filetype">{$RelatedFileObjects.RelatedObject.data_map.file.content.mime_type|wash}</td>
                    <td class="filesize">{$RelatedFileObjects.RelatedObject.data_map.file.content.filesize|si( byte )}</td>
                </tr>
            {/section}
            </table>
    {/section}

    {* Related objects *}
    {section show=$grouped_related_contentobjects.objects|count|gt( 0 )}
    <h2>{'Related content'|i18n('design/standard/ezdhtml')}</h2>
            <table class="list" cellspacing="0">
            <tr>
                <th class="tight">&nbsp;</th>
                <th class="name">{'Name'|i18n( 'design/admin/content/edit' )}</th>
                <th class="class">{'Type'|i18n( 'design/admin/content/edit' )}</th>
            </tr>
            {section var=RelatedObjects loop=$grouped_related_contentobjects.objects sequence=array( bglight, bgdark )}
                <tr class="{$RelatedObjects.sequence|wash}">
                    <td class="checkbox"><input type="radio" {section show=$RelatedObjects.ObjectIsSelected}class="selected"{/section} name="ContentObjectIDString" value="{$RelatedObjects.ObjectIDString}" {section show=$RelatedObjects.ObjectIsSelected}checked{/section} /></td>
                    <td class="name">{$RelatedObjects.RelatedObject.class_name|class_icon( small, $RelatedObjects.RelatedObject.class_name )}&nbsp;{$RelatedObjects.RelatedObject.name|wash}</td>
                    <td class="class">{$RelatedObjects.RelatedObject.class_name|wash}</td>
                </tr>
            {/section}
            </table>
    {/section}
{section-else}
<div class="block">
<p>{"There are no related objects."|i18n("design/standard/ezdhtml")}</p>
</div>
{/section}

<form enctype="multipart/form-data" action={concat("ezdhtml/insertobject","/",$object_id,"/",$object_version,"/")|ezurl} method="post">

<input type="submit" class="button" name="uploadFile" id="uploadfile" value="{'Upload new'|i18n('design/standard/ezdhtml')}" />
<input type="submit" class="button" name="BrowseButton" id="BrowseButton" value="{'Add existing'|i18n('design/standard/ezdhtml')}" />

</form>

</fieldset>

<fieldset>
<legend>{'Properties'|i18n('design/standard/ezdhtml')}</legend>

    <table id="attributes" class="list" cellspacing="0">
    <tr>
        <th class="tight">&nbsp;</th>
        <th width="30%">{'Attribute name'|i18n('design/standard/ezdhtml')}</th>
        <th width="60%">{'Value'|i18n('design/standard/ezdhtml')}</th>
    </tr>
    <tr id="AltRow" class="bgdark">
        <td><input id='Selected_attribute' type='checkbox' disabled="true" /></td>
        <td class="name">Alt text</td>
	  <input type="hidden" id="CustomAttributeName" value="alt">
        <td class="value">
        <input id="CustomAttributeValue" type="text" size="20"/>
        </td>
    </tr>
    <tr id="sizeRow" class="bglight">
        <td><input type='checkbox' disabled="true" /></td>
        <td class="name">Size</td>
        <td class="value">
        <select id="ContentObjectSize" onClick="ok.disabled=false">
            {section name=SizeArray loop=$size_type_list}
                <option value="{$SizeArray:item}">{$SizeArray:item}</option>
	        {/section}
        </select>
        </td>
    </tr>
    <tr id="alignmentRow" class="bgdark">
        <td><input type='checkbox' disabled="true" /></td>
        <td class="name">Alignment</td>
        <td class="value">
        <select id="ContentObjectAlignment" onClick="ok.disabled=false">
            <option value="center" selected>{"Center"|i18n("design/standard/ezdhtml")}</option>
            <option value="left">{"Left"|i18n("design/standard/ezdhtml")}</option>
            <option value="right">{"Right"|i18n("design/standard/ezdhtml")}</option>
        </select>
        </td>
    </tr>
    <tr id="classRow" class="bglight">
        <td><input type='checkbox' disabled="true" /></td>
        <td class="name">Class</td>
        <td class="value">
        <select id ="ObjectClass">
            <option value="-1">[{"none"|i18n("design/standard/ezdhtml")}]</option>
            {section name=ClassArray loop=$class_list}
                <option value="{$ClassArray:item}">{$ClassArray:item}</option>
	        {/section}
	     </select>
         </td>
    </tr>
        <input type="hidden" id ="ObjectView" value="embed"> 
    <tr id="idRow" class="bglight">
        <td><input type='checkbox' disabled="true" /></td>
        <td class="name">ID</td>
        <td class="value">
        <input id="HtmlID" type="text" size="10"/>
        </td>
    </tr>
    <tr id="lastPropertyRow" class="bgdark" style="display:none">
    </tr>
    </table>

<div class="block"">
        <input type="button" class="button" id="remove_attribute" value="{'Remove selected'|i18n('design/standard/ezdhtml')}" onclick='removeSelected()' />
	    <input type="button" class="button" id="new_attribute" value="{'New attribute'|i18n('design/standard/ezdhtml')}" onclick='addNew()' />
</div>

</fieldset>

<div class="block"">
	    <input class="button" id="ok" type="submit" value={"OK"|i18n("design/standard/ezdhtml")} />
        <input type="button" class="button" onclick="window.close();" value={"Cancel"|i18n("design/standard/ezdhtml")} />
</div>
{/section}
</div>

<div style="height:200px;">
</div>
