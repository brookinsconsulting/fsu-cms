<?php /* #?ini charset="iso-8859-1"?

[CustomTagSettings]
AvailableCustomTags[]=fvplaylist
AvailableCustomTags[]=faplaylist
IsInline[fvplaylist]=true
IsInline[faplaylist]=true

[fvplaylist]
CustomAttributes[]=feed_url
CustomAttributes[]=feed_nodeid
CustomAttributes[]=width
CustomAttributes[]=height
CustomAttributes[]=depth
CustomAttributesDefaults[feed_url]=eg. http://www.examlpe.com/rss.xml
CustomAttributesDefaults[feed_nodeid]=Parent which contains flv videos
CustomAttributesDefaults[width]=382
CustomAttributesDefaults[height]=212
CustomAttributesDefaults[depth]=1

[faplaylist]
CustomAttributes[]=feed_url
CustomAttributes[]=feed_nodeid
CustomAttributes[]=width
CustomAttributes[]=height
CustomAttributes[]=depth
CustomAttributesDefaults[feed_url]=eg. http://www.examlpe.com/rss.xml
CustomAttributesDefaults[feed_nodeid]=Parent which contains mp3 audios
CustomAttributesDefaults[width]=400
CustomAttributesDefaults[height]=310
CustomAttributesDefaults[depth]=1

[embed]
AvailableClasses[]=ngflashvideo
ClassDescription[ngflashvideo]=Flash Video

*/ ?>
