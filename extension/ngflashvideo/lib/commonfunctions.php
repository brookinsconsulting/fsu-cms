<?php
//
// Created on: <29-apr-2007 23:0:12 >
//
// Netgen d.o.o. www.netgen.hr
//

function getXspf( $NodeID, $classesToInclude, $MaxTracksInXspf, $Depth )
{

	$parentnode = eZContentObjectTreeNode::fetch($NodeID);

	
	if ($Depth==1)
	{
		$sortBy = array( 'published' , false );
	}else{
		$sortBy = $parentnode->sortArray();
		$sortBy = $sortBy[0];
	}

	$my_params = 	array( 'MainNodeOnly' => false,
				'IgnoreVisibility' => true,
				'Depth' => $Depth,
				'ClassFilterType' => 'include',
				'Limit' => $MaxTracksInXspf,
				'SortBy' => $sortBy,
				'ClassFilterArray' => $classesToInclude 
				);
	$nodes = $parentnode->subTree( $my_params, $NodeID );

       $xspfContent = "";
	$xspfContent .= '<?xml version="1.0" encoding="UTF-8"?>';
	$xspfContent .= '<playlist version="1" xmlns="http://xspf.org/ns/0/">';
	$xspfContent .= '<trackList>';

	foreach($nodes as $node)
	{

		// get the current object version of a node
		$object = $node->attribute('object');
		$version = $object->version( $object->attribute('current_version') );
		$contentObjectAttributes = $version->contentObjectAttributes();


		$mainnode = eZContentObjectTreeNode::fetch($object->attribute("main_node_id"));
		$nodeMainLink = $mainnode->attribute( 'url_alias' );

		foreach ($contentObjectAttributes as $attribute)
		{	


			if ($attribute->attribute("contentclass_attribute_identifier") == "name") {
				$nodeName = $attribute->attribute("data_text"); 
			}
			
			if ($attribute->attribute("contentclass_attribute_identifier") == "date") {
				$nodeDate = date("d.m.y.", $attribute->attribute("data_int"));
			}

			if ($attribute->attribute("contentclass_attribute_identifier") == "file") {
				$attributecontent = $attribute->attribute("content");
				
				
				
				$contentLocation = '/' . ($attributecontent->attribute("filepath"));

				// this can also be used if you removed index.php:
				//$contentLocation = '/content/download/' . $attribute->attribute("contentobject_id") . '/' . $attribute->attribute("id") . '/version/' . $attribute->attribute("version") . '/file/' . urlencode($attributecontent->attribute("original_filename"));
			}		

		}
		
		$xspfContent .= "<track>\n";
		$xspfContent .= "<title>".$nodeName."</title>\n";
		$xspfContent .= "<creator>".$nodeDate."</creator>\n";
		$xspfContent .= "<location>".$contentLocation."</location>\n";
		$xspfContent .= "<link>".$nodeMainLink."</link>\n";

		//$xspfContent .= "<image>/extension/ngflashvideo/design/standard/images/poster.jpg</image>\n";
		$xspfContent .= "</track>\n";
	}

	$xspfContent .= '</trackList>';
	$xspfContent .= '</playlist>';

	return $xspfContent;
}

?>