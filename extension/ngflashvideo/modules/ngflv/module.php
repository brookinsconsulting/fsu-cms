<?php

$Module = array( 'name' => 'getxspf' );

// Define module views.
$ViewList = array();

$ViewList['flvxspf'] = array(
    'script' => 'flvxspf.php',
    'functions' => array( 'flvxspf' ),
    'params' => array( 'NodeID', 'Depth' )
);

$ViewList['mp3xspf'] = array(
    'script' => 'mp3xspf.php',
    'functions' => array( 'mp3xspf' ),
    'params' => array( 'NodeID', 'Depth' )
);

// Define module functions (used by permission system).
//$FunctionList['flvxspf'] = array( );
//$FunctionList['mp3xspf'] = array( );

?>
