<?php

	$Module =& $Params['Module'];

// NodeID parameter

	$NodeID=false;

	if ( isset( $Params['NodeID'] ) && is_numeric($Params['NodeID']))
	{
	   $NodeID = (int) $Params['NodeID'];
	}
	if (!$NodeID) {
		eZDebug::writeError( 'No Node specified' );
		eZExecution::cleanExit();
	}

// Depth parameter

	if ( isset( $Params['Depth'] ) && is_numeric($Params['Depth']))
	{
		$Depth = (int) $Params['Depth'];
	}
	else
	{
		$Depth=1;
	}


	include_once( 'kernel/classes/ezcontentobjecttreenode.php' );
	include_once( 'kernel/classes/ezcontentobject.php' );

	$config =& eZINI::instance( 'ngflashvideo.ini' );
	$MaxTracksInXspf = intval( $config->variable( 'xspfSettings', 'MaxMp3TracksInXspf' ) );
	$cacheTime = intval( $config->variable( 'xspfSettings', 'CacheTime' ) );



	if($cacheTime <= 0)
	{
		include_once( 'extension/ngflashvideo/lib/commonfunctions.php' );
		$xspfContent = getXspf( $NodeID, array( 'audio'), $MaxTracksInXspf, $Depth );
	}
	else
	{
		$cacheDir = eZSys::cacheDirectory();
		$cacheFilePath = $cacheDir . '/xspf/' . 'mp3-' . md5( $NodeID .'-'. $Depth ) . '.xml';

		// If cache directory does not exist, create it. Get permissions settings from site.ini
		if ( !is_dir( $cacheDir.'/xspf' ) )
		{
			$siteconfig =& eZINI::instance( 'site.ini' );
			$mode = $siteconfig->variable( 'FileSettings', 'TemporaryPermissions' );
			if ( !is_dir( $cacheDir ) )
			{
				mkdir( $cacheDir );
				chmod( $cacheDir, octdec( $mode ) );
			}
			mkdir( $cacheDir.'/xspf' );
			chmod( $cacheDir.'/xspf', octdec( $mode ) );
		}

		// VS-DBFILE

		require_once( 'kernel/classes/ezclusterfilehandler.php' );
		$cacheFile = eZClusterFileHandler::instance( $cacheFilePath );

		if ( !$cacheFile->exists() or ( time() - $cacheFile->mtime() > $cacheTime ) )
		{
			include_once( 'extension/ngflashvideo/lib/commonfunctions.php' );
			$xspfContent = getXspf( $NodeID, array('audio'), $MaxTracksInXspf, $Depth );
			$cacheFile->storeContents( $xspfContent, false, 'xml' );
		}
		else
		{
			$xspfContent = $cacheFile->fetchContents();
		}
	}

	// Set header settings
	$httpCharset = eZTextCodec::httpCharset();

	header( 'Content-Type: text/xml; charset=' . $httpCharset );
	header( 'Content-Length: '.strlen($xspfContent) );
	header( 'X-Powered-By: eZ publish' );

	echo $xspfContent;

	eZExecution::cleanExit();

?>
