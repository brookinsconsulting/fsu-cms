ngflashvideo extension readme for eZ Publish

Version: 1.0.0


What is the ngflashvideo extension?
===================================

NgFlashvideo extension enables you to publish flv videos and mp3 audio with embedded flash player on your website. It is based on Jeroen Wijering's Flash Video Player (http://www.jeroenwijering.com/?item=Flash_Video_Player)
It also supports automated generation of playlists, based on subtree fetch (depth can be controled).


Embeding flash player with playlist from subree
===============================================

Inside ezxml attribute, you can insert the custom tag fvplaylist or faplaylist. Both of them have this settings:

feed_url   : use only if you wish to feed your player form some external xspf source, for example http://www.examlpe.com/rss.xml
feed_nodeid: enter the id of the node which contains flv videos or mp3 audios
width      : width of the player
height     : height of the player
depth      : set the depth of fetch. If you want only child elements set to 1. If you want unlimited fetch set to 0.


Credits:

Netgen d.o.o. http://www.netgen.hr