﻿{default $image_class='articleimage'}

<div class="content-view-full">

    {include uri='design:parts/right_column.tpl' node=$node show_parent_right_column curdate=$node.object.published}

    <div class="fix">
    <div class="class-article class-file">

	<div class="attribute-title">
		<h1>{$node.name|wash()}</h1>
	</div>

	<div class="attribute-date">
		<p>{$node.object.published|l10n(shortdate)}</p>
	</div>


	{if $node.data_map.description.content.is_empty|not}
	    <div class="attribute-long">
		{attribute_view_gui attribute=$node.data_map.description}
	    </div>
	{/if}

	<div class="content-media">
	{def $attribute=$node.data_map.file}
	<p id="ngaudio"><a href="http://www.macromedia.com/go/getflashplayer">Get the Flash Player</a> to see this player.</p>
	{literal}
	<script type="text/javascript">
		var FO = {	movie:"/extension/ngflashvideo/design/standard/images/mp3player.swf",width:"340",height:"20",majorversion:"7",build:"0",bgcolor:"#FFFFFF",wmode:"transparent",
			flashvars:"file={/literal}{concat("content/download/",$attribute.contentobject_id,"/",$attribute.content.contentobject_attribute_id,"/",$attribute.content.original_filename)|ezurl('no')}{literal}&autostart=false&lightcolor=0x6e788c&backcolor=0xffffff&frontcolor=0x42719E" };
		UFO.create(	FO, "ngaudio");
	</script>
	{/literal}
	</div>


    </div>
    </div>
</div>


