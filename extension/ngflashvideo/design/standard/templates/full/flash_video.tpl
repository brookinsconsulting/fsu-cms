{if $node.data_map.fullscreen_dozvoljen.data_int}
	{def $allowfullscreen="true"}
{else}
	{def $allowfullscreen="false"}
{/if}

{if $node.data_map.autoplay.data_int}
	{def $autoplay="true"}
{else}
	{def $autoplay="false"}
{/if}

{if $node.data_map.image.has_content}
	{def $image=concat('&image=/',$node.data_map.image.content.original.url)}
{else}
	{def $image=''}
{/if}

<div class="content-view-full">

    {include uri='design:parts/right_column.tpl' node=$node show_parent_right_column curdate=$node.object.published}

	<div class="fix">
	<div class="class-article flash_video">


		<div class="attribute-title">
			<h1>{$node.name|wash()}</h1>
		</div>

		{section show=$node.data_map.description.content.is_empty|not}
		<div class="attribute-intro">
		{attribute_view_gui attribute=$node.data_map.description}
		</div>
		{/section}

		{if $node.data_map.image.has_content}
			{def $image=concat('&image=/',$node.data_map.image.content.original.url)}
		{else}
			{def $image=''}
		{/if}

		<div class="content-media">
		{def $attribute=$node.data_map.file}

		<p id="ngfvp"><a href="http://www.macromedia.com/go/getflashplayer">Get the Flash Player</a> to see this player.</p>
		<script type="text/javascript">
			var FO = {ldelim}	movie:"/extension/ngflashvideo/design/standard/images/flvplayer.swf",width:"{$node.data_map.width.content}",height:"{$node.data_map.height.content}",majorversion:"7",build:"0",bgcolor:"#FFFFFF",allowfullscreen:"{$allowfullscreen}",
				flashvars:"file={concat("content/download/",$attribute.contentobject_id,"/",$attribute.content.contentobject_attribute_id,"/",$attribute.content.original_filename)|ezurl('no')}&autostart={$autoplay}{$image}" {rdelim};
			UFO.create( FO, "ngfvp");
		</script>


		</div>

    </div>
    </div>
</div>


