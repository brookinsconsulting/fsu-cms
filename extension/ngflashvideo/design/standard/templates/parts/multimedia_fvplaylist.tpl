{default
	playerid="ngfvpp1"
	width="382"
	height="341"
	image="/extension/ngflashvideo/design/standard/images/poster.jpg"
	displayheight="272"
}

{if is_set($feed_nodeid)}
	{def $spiff = concat("ngflv/flvxspf/",$feed_nodeid)}
	{if is_set($depth)}
		{set $spiff = concat($spiff,"/",$depth)}
	{/if}
	{set $spiff = $spiff|ezurl(no)}
{else}
	{def $spiff = $feed_url}
{/if}

<p id="{$playerid}"><a href="http://www.macromedia.com/go/getflashplayer">Install Flash Player</a></p>

{literal}
<script type="text/javascript">
	var FO = {	movie:"/extension/ngflashvideo/design/standard/images/flvplayer.swf",width:"{/literal}{$width}{literal}",height:"{/literal}{$height}{literal}",majorversion:"8",build:"0",bgcolor:"#FFFFFF",allowfullscreen:"true",
	flashvars:"file={/literal}{$spiff}&image={$image}{literal}&displayheight={/literal}{$displayheight}{literal}&lightcolor=0x6e788c&backcolor=0xffffff&frontcolor=0x42719E" };
	UFO.create(	FO, "{/literal}{$playerid}{literal}");
</script>
{/literal}
