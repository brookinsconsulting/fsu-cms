﻿<div class="content-view-line float-break">
<div class="class-article">

	<div class="in2">
	<div class="floater">

		<h2><a href={$node.url_alias|ezurl}>{$node.data_map.name.content|wash}</a></h2>
		
		<div class="attribute-date"><p>{$node.object.published|l10n(shortdate)}</p></div>

		{section show=$node.data_map.description.content.is_empty|not}
		<div class="attribute-short">
		{attribute_view_gui attribute=$node.data_map.description}
		</div>
		{/section}

	</div>
	</div>

</div>
</div>
