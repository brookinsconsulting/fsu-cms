{* Modified Pages *}

{def $User = fetch('user','current_user')
     $AllChildren = fetch('content','list',hash('parent_node_id',$object.main_node_id,
						'sort_by', array('modified', false()),
						'depth',100,
						'limit',400))
}

{if $User.is_logged_in}

<div class="content-view-full">

<table cellpadding="2" cellspacing="4" style="width: 100%;">
  <tr>
    <th>Name</th>
    <th>Created</th>
    <th>Modified</th>
  </tr>
{foreach $AllChildren as $Index=>$Child  sequence array('bglight', 'bgdark') as $RowClass}
  <tr class="{$RowClass}">
    <td><a href={$Child.url_alias|ezroot()}>{$Child.name|wash()}</a></td>
    <td>{$Child.object.published|l10n(shortdatetime)}</td>
    <td><a href={concat("/content/history/",$Child.object.id)}>{$Child.object.modified|l10n(shortdatetime)}</a></td>
  </tr>
{/foreach}
</table>

</div>

{/if}
