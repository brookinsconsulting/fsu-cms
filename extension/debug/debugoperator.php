<?php

class debugOperator
{
    var $Operators;

    function debugOperator()
    {
	$this->Operators = array( "debug", "kill_debug", "debug_on" );
    }


    function operatorList()
    {
	return $this->Operators;
    }


    function namedParameterPerOperator()
    {
        return true;
    }   

    function namedParameterList()
    {
        return array( 'debug' => array(), 'kill_debug' => array(), 'debug_on' => array() );
    }


    function modify( $tpl, &$operatorName, &$operatorParameters, &$rootNamespace, &$currentNamespace, &$operatorValue, &$namedParameters )
    {

        switch ( $operatorName )
        {
            case 'kill_debug':
            {
		$GLOBALS['eZDebugEnabled'] =0;
		$operatorValue = '';
		return true;
            } 
            break;

            case 'debug_on':
            {
		$GLOBALS['eZDebugEnabled'] =1;
		$GLOBALS['DebugByIP']=0;
		$GLOBALS['DebugByUser']=0;
		$operatorValue = '';
		return true;
            } 
            break;

            case 'debug':
            {
		eZDebug::writeDebug( $operatorValue );
		$operatorValue = '';
		return true;
            } 
            break;

	}

    }
}

?>
