var new_req; 
var new_ajax_url='/basicajax/dynamic/';
var TargetDiv;
var funcParams = 0;

function basicajax_get_html(TargetUrl, temp_TargetDiv, PipedParamString) 
{

    funcParams = 0;
    if (typeof PipedParamString == 'function') {
	funcParams = PipedParamString;
	PipedParamString = '';
    }
    if (TargetUrl.match(/^http/) != null) {
	TargetUrl = new_ajax_url + TargetUrl;
	TargetUrl = TargetUrl.replace(/\//g,'|');
	TargetUrl = TargetUrl.replace(/\?/g,'|questionmark|');
    } else {
	if (PipedParamString) PipedParamString = PipedParamString.replace(/\|/g, '/');
    }
    TargetDiv = temp_TargetDiv;
	if (PipedParamString != '') TargetUrl = TargetUrl + "/" + PipedParamString;
    // branch for native XMLHttpRequest object
    if (window.XMLHttpRequest) {
        new_req = new XMLHttpRequest();
        new_req.onreadystatechange = basicajax_processReqChange;
        new_req.open("GET", TargetUrl, true);
        new_req.send(null);
    // branch for IE/Windows ActiveX version
    } else if (window.ActiveXObject) {
        new_req = new ActiveXObject("Microsoft.XMLHTTP");
        if (new_req) {
            new_req.onreadystatechange = basicajax_processReqChange;
            new_req.open("GET", TargetUrl, true);
            new_req.send();
        }
    }
}

function basicajax_processReqChange() 
{
    if (TargetDiv != '') {

    if (new_req.readyState == 4) {
        // only if "OK"
        if (new_req.status == 200) {
	    document.getElementById(TargetDiv).innerHTML = new_req.responseText;
	    if (typeof funcParams == 'function') {
		funcParams.call();
	    }
 	 } else if(new_req.status == 404) {
	   // Add a custom message or redirect the user to another page
	   document.getElementById(TargetDiv).innerHTML = "File not found";
        } else {
            alert("There was a problem retrieving the XML data:\n" + new_req.statusText);
        }
    }
    }
}