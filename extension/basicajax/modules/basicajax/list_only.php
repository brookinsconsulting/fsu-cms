<?php

include_once( 'kernel/common/template.php' );

$nodeID = $Params['ParentNodeId'];

$tpl =& templateInit();

$tpl->setVariable( 'nodeID', $nodeID );

$contents =  $tpl->fetch( 'design:node/view/ajax_select.tpl' );

echo $contents;

eZExecution::cleanExit(); 

?>