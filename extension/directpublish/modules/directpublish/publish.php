<?php


$http = eZHTTPTool::instance();
$Module = $Params['Module'];
$Module->setCurrentAction('Publish');
$userName = $http->postVariable( 'publisher_name' );
$userObject = eZUser::fetchByName( $userName, true );
if (!is_object($userObject)) {
    $userName = 'admin';
    $userObject = eZUser::fetchByName( $userName, true );
}
eZDebug::writeDebug($userObject);
$userID = $userObject->attribute('contentobject_id');

eZUser::setCurrentlyLoggedInUser( $userObject, $userID );

if ( $http->hasPostVariable( 'DirectPublishButton' ) )
{
if ( ( $http->hasPostVariable( 'ClassID' ) && $http->hasPostVariable( 'NodeID' ) ))
{

    $hasClassInformation = true;
    $contentClassID = false;
    $contentClassIdentifier = false;
    $languageCode = false;
    $class = false;

    $contentClassID = $http->postVariable( 'ClassID' );


        //include_once( 'kernel/classes/ezcontentlanguage.php' );
        $allLanguages = eZContentLanguage::prioritizedLanguages();
        // Only show language selection if there are more than 1 languages.
        if ( count( $allLanguages ) > 1 &&
             $hasClassInformation )
        {
	      $allLanguages = array($allLanguages[0]);
        }


    if ( $hasClassInformation && $http->hasPostVariable( 'NodeID' )  )
    {

        $node = eZContentObjectTreeNode::fetch( $http->postVariable( 'NodeID' ) );

        if ( is_object( $node ) )
        {
            $contentObject = eZContentObject::createWithNodeAssignment( $node,
                                                                        $contentClassID,
                                                                        $languageCode,
                                                                        ( $http->hasPostVariable( 'AssignmentRemoteID' ) ?
                                                                              $http->postVariable( 'AssignmentRemoteID' ) : false ) );
            if ( $contentObject )
            {

                if ( $http->hasPostVariable( 'RedirectURIAfterPublish' ) )
                {
                    $http->setSessionVariable( 'RedirectURIAfterPublish', $http->postVariable( 'RedirectURIAfterPublish' ) );
                }
                $http->setPostVariable( 'MainNodeID', $node->attribute( 'node_id' ) );
                $http->setPostVariable( 'ContentObjectAttribute_id', true );
                $EditVersion = $contentObject->attribute( 'current_version' ) ;

            }
            else
            {
                // If ACCESS DENIED save current post variables for using after login
                $http->setSessionVariable( '$_POST_BeforeLogin', $http->attribute( 'post' ) );
                return $Module->handleError( eZError::KERNEL_ACCESS_DENIED, 'kernel' );
            }
        }
        else
        {
            return $Module->handleError( eZError::KERNEL_NOT_AVAILABLE, 'kernel' );
        }
    }

} elseif ( $http->hasPostVariable( 'ObjectID' ) ) {

$ObjectID = $http->PostVariable( 'ObjectID' );
$contentObject = eZContentObject::fetch( $ObjectID );
$db = eZDB::instance();
$db->begin();
$version = $contentObject->createNewVersionIn( $EditLanguage, $FromLanguage );
$version->setAttribute( 'status', eZContentObjectVersion::STATUS_INTERNAL_DRAFT );
$version->store();
$db->commit();
$EditVersion = $version->attribute('version');

}

}

$attribute_list = $contentObject->contentObjectAttributes(true);
$attribute_id_list = array();


foreach ($attribute_list as $attribute)
{
    $varkey = 'ContentObjectAttribute_';
    if ($attribute->DataTypeString == 'ezobjectrelation') {
        $varkey .= "data_object_relation_id_";
    } elseif ($attribute->DataTypeString == 'ezkeyword') {
        $varkey .= "ezkeyword_data_text_";
    } elseif ($attribute->DataTypeString == 'ezstring') {
        $varkey .= "ezstring_data_text_";
    } elseif ($attribute->DataTypeString == 'ezfloat') {
        $varkey .= "data_float_";
    } elseif ($attribute->DataTypeString == 'ezimage') {
        $varkey .= "data_imagealttext_";
        $http->setPostVariable( 'MAX_FILE_SIZE', '0');
    } else {
        $varkey .= "data_text_";
    }
    $varkey .= $attribute->ID;
    $varname = 'dp_attribute_'.$attribute->contentClassAttributeIdentifier();
    $var = ($http->hasPostVariable( $varname )) ? $http->postVariable( $varname ) : '';
    $http->setPostVariable( $varkey, $var );

    $attribute_id_list[] = $attribute->ID;
}
$http->setPostVariable('ContentObjectAttribute_id', $attribute_id_list);

//eZDebug::writeDebug($_POST);

require 'kernel/content/node_edit.php';
initializeNodeEdit( $Module );
require 'kernel/content/relation_edit.php';
initializeRelationEdit( $Module );
require 'kernel/content/section_edit.php';
initializeSectionEdit( $Module );
$obj = $contentObject;
$ObjectID  = $contentObject->attribute( 'id' );
$Params['ObjectID'] = $ObjectID;
$isAccessChecked = false;
$classID = $obj->attribute( 'contentclass_id' );
$class = eZContentClass::fetch( $classID );
$languages = eZContentLanguage::fetchList();
$firstLanguage = array_shift( $languages );
$EditLanguage = $firstLanguage->attribute( 'locale' );


$ini = eZINI::instance();

if ( is_numeric( $EditVersion ) )
{
    $version = $obj->version( $EditVersion );
    if ( !$version )
    {
        return $Module->handleError( eZError::KERNEL_NOT_AVAILABLE, 'kernel' );
    }
}

if ( is_numeric( $EditVersion ) )
{
    // Fetch version
    $version = eZContentObjectVersion::fetchVersion( $EditVersion, $obj->attribute( 'id' ) );
    if ( !is_object( $version ) )
    {
        return $Module->handleError( eZError::KERNEL_NOT_AVAILABLE, 'kernel' );
    }
    $user = eZUser::currentUser();
    // Check if $user can edit the current version.
    // We should not allow to edit content without creating a new version.
    if ( ( $version->attribute( 'status' ) != eZContentObjectVersion::STATUS_INTERNAL_DRAFT and
           $version->attribute( 'status' ) != eZContentObjectVersion::STATUS_DRAFT and
           $version->attribute( 'status' ) != eZContentObjectVersion::STATUS_PENDING ) or
           $version->attribute( 'creator_id' ) != $user->id() )
    {
        return $Module->redirectToView( 'history', array( $ObjectID, $version->attribute( "version" ), $EditLanguage ) );
    }
}

// If $isAccessChecked is still false we need to check access ourselves.
if ( !$isAccessChecked )
{
    // Check permission for object and version in specified language.
    if ( !$obj->canEdit( false, false, false, $EditLanguage ) )
    {
        return $Module->handleError( eZError::KERNEL_ACCESS_DENIED, 'kernel',
                                     array( 'AccessList' => $obj->accessList( 'edit' ) ) );
    }
}



if ( !function_exists( 'checkContentActions' ) )
{
    function checkContentActions( $module, $class, $object, $version, $contentObjectAttributes, $EditVersion, $EditLanguage, $FromLanguage, &$Result )
    {
        if ( $module->isCurrentAction( 'Preview' ) )
        {
            $module->redirectToView( 'versionview', array( $object->attribute('id'), $EditVersion, $EditLanguage, $FromLanguage ) );
            return eZModule::HOOK_STATUS_CANCEL_RUN;
        }

        if ( $module->isCurrentAction( 'Translate' ) )
        {
            $module->redirectToView( 'translate', array( $object->attribute( 'id' ), $EditVersion, $EditLanguage, $FromLanguage ) );
            return eZModule::HOOK_STATUS_CANCEL_RUN;
        }

        if ( $module->isCurrentAction( 'VersionEdit' ) )
        {
            if ( isset( $GLOBALS['eZRequestedURI'] ) and is_object( $GLOBALS['eZRequestedURI'] ) )
            {
                $uri = $GLOBALS['eZRequestedURI'];
                $uri = $uri->originalURIString();
                $http = eZHTTPTool::instance();
                $http->setSessionVariable( 'LastAccessesVersionURI', $uri );
            }
            $module->redirectToView( 'history', array( $object->attribute( 'id' ), $EditVersion, $EditLanguage ) );
            return eZModule::HOOK_STATUS_CANCEL_RUN;
        }

        if ( $module->isCurrentAction( 'EditLanguage' ) )
        {
            if ( $module->hasActionParameter( 'SelectedLanguage' ) )
            {
                $EditLanguage = $module->actionParameter( 'SelectedLanguage' );
                // We reset the from language to disable the translation look
                $FromLanguage = false;
                $module->redirectToView( 'edit', array( $object->attribute('id'), $EditVersion, $EditLanguage, $FromLanguage ) );
                return eZModule::HOOK_STATUS_CANCEL_RUN;
            }
        }

        if ( $module->isCurrentAction( 'TranslateLanguage' ) )
        {
            if ( $module->hasActionParameter( 'SelectedLanguage' ) )
            {
                $FromLanguage = $EditLanguage;
                $EditLanguage = $module->actionParameter( 'SelectedLanguage' );
                $module->redirectToView( 'edit', array( $object->attribute('id'), $EditVersion, $EditLanguage, $FromLanguage ) );
                return eZModule::HOOK_STATUS_CANCEL_RUN;
            }
        }

        if ( $module->isCurrentAction( 'FromLanguage' ) )
        {
            $FromLanguage = $module->actionParameter( 'FromLanguage' );
            $module->redirectToView( 'edit', array( $object->attribute('id'), $EditVersion, $EditLanguage, $FromLanguage ) );
            return eZModule::HOOK_STATUS_CANCEL_RUN;
        }

        if ( $module->isCurrentAction( 'Discard' ) )
        {
            $http = eZHTTPTool::instance();
            $objectID = $object->attribute( 'id' );
            $discardConfirm = true;
            if ( $http->hasPostVariable( 'DiscardConfirm' ) )
                $discardConfirm = $http->postVariable( 'DiscardConfirm' );
            if ( $http->hasPostVariable( 'RedirectIfDiscarded' ) )
                $http->setSessionVariable( 'RedirectIfDiscarded', $http->postVariable( 'RedirectIfDiscarded' ) );
            $http->setSessionVariable( 'DiscardObjectID', $objectID );
            $http->setSessionVariable( 'DiscardObjectVersion', $EditVersion );
            $http->setSessionVariable( 'DiscardObjectLanguage', $EditLanguage );
            $http->setSessionVariable( 'DiscardConfirm', $discardConfirm );
            $module->redirectTo( $module->functionURI( 'removeeditversion' ) . '/' );
            return eZModule::HOOK_STATUS_CANCEL_RUN;
        }

        // helper function which computes the redirect after
        // publishing and final store of a draft.
        function computeRedirect( $module, $object, $version, $EditLanguage = false )
        {
            $http = eZHTTPTool::instance();

            $node = $object->mainNode();

            $hasRedirected = false;
            if ( $http->hasSessionVariable( 'ParentObject' ) && $http->sessionVariable( 'NewObjectID' ) == $object->attribute( 'id' ) )
            {
                $parentArray = $http->sessionVariable( 'ParentObject' );
                $parentURL = $module->redirectionURI( 'content', 'edit', $parentArray );
                $parentObject = eZContentObject::fetch( $parentArray[0] );
                $db = eZDB::instance();
                $db->begin();
                $parentObject->addContentObjectRelation( $object->attribute( 'id' ), $parentArray[1] );
                $db->commit();
                $http->removeSessionVariable( 'ParentObject' );
                $http->removeSessionVariable( 'NewObjectID' );
                $module->redirectTo( $parentURL );
                $hasRedirected = true;
            }
            if ( $http->hasSessionVariable( 'RedirectURIAfterPublish' ) && !$hasRedirected )
            {
                $uri = $http->sessionVariable( 'RedirectURIAfterPublish' );
                $http->removeSessionVariable( 'RedirectURIAfterPublish' );
                $module->redirectTo( $uri );
                $hasRedirected = true;
            }
            if ( $http->hasPostVariable( 'RedirectURIAfterPublish' )  && !$hasRedirected )
            {
                $uri = $http->postVariable( 'RedirectURIAfterPublish' );
                $module->redirectTo( $uri );
                $hasRedirected = true;
            }
            if ( $http->hasPostVariable( "BackToEdit") && $http->postVariable( "BackToEdit") )
            {
                $uri = $module->redirectionURI( 'content', 'edit', array( $object->attribute( 'id'), 'f', $EditLanguage ) );
                $module->redirectTo( $uri );
                eZDebug::writeDebug( $uri, "uri  " .  $object->attribute( 'id')  );
                $hasRedirected = true;
            }

            if ( !$hasRedirected )
            {
                if ( $http->hasPostVariable( 'RedirectURI' ) )
                {
                    $uri = $http->postVariable( 'RedirectURI' );
                    $module->redirectTo( $uri );
                }
                else if ( $node !== null )
                {
                    $parentNode = $node->attribute( 'parent_node_id' );
                    if ( $parentNode == 1 )
                    {
                        $parentNode = $node->attribute( 'node_id' );
                    }
                    $module->redirectToView( 'view', array( 'full', $parentNode ) );
                }
                else
                {
                    $module->redirectToView( 'view', array( 'full', $version->attribute( 'main_parent_node_id' ) ) );
                }
            }

        }

        if( $module->isCurrentAction( 'StoreExit' ) )
        {
            computeRedirect( $module, $object, $version, $EditLanguage );
            return eZModule::HOOK_STATUS_CANCEL_RUN;
        }

        if ( $module->isCurrentAction( 'Publish' ) )
        {
            // Checking the source and destination language from the url,
            // if they are the same no confirmation is needed.
            if ( $EditLanguage != $FromLanguage )
            {
                $conflictingVersions = $version->hasConflicts( $EditLanguage );
                if ( $conflictingVersions )
                {
                    require_once( 'kernel/common/template.php' );
                    $tpl = templateInit();

                    $res = eZTemplateDesignResource::instance();
                    $res->setKeys( array( array( 'object', $object->attribute( 'id' ) ),
                                        array( 'class', $class->attribute( 'id' ) ),
                                        array( 'class_identifier', $class->attribute( 'identifier' ) ),
                                        array( 'class_group', $class->attribute( 'match_ingroup_id_list' ) ) ) );

                    $tpl->setVariable( 'edit_language', $EditLanguage );
                    $tpl->setVariable( 'current_version', $version->attribute( 'version' ) );
                    $tpl->setVariable( 'object', $object );
                    $tpl->setVariable( 'draft_versions', $conflictingVersions );

                    $Result = array();
                    $Result['content'] = $tpl->fetch( 'design:content/edit_conflict.tpl' );
                    return eZModule::HOOK_STATUS_CANCEL_RUN;
                }
            }

            //include_once( 'lib/ezutils/classes/ezoperationhandler.php' );
            eZDebug::accumulatorStart( 'publish', '', 'publish' );
            $oldObjectName = $object->name();
            $operationResult = eZOperationHandler::execute( 'content', 'publish', array( 'object_id' => $object->attribute( 'id' ),
                                                                                         'version' => $version->attribute( 'version' ) ) );
            eZDebug::accumulatorStop( 'publish' );

            if ( ( array_key_exists( 'status', $operationResult ) && $operationResult['status'] != eZModuleOperationInfo::STATUS_CONTINUE ) )
            {
                switch( $operationResult['status'] )
                {
                    case eZModuleOperationInfo::STATUS_HALTED:
                    {
                        if ( isset( $operationResult['redirect_url'] ) )
                        {
                            $module->redirectTo( $operationResult['redirect_url'] );
                            return;
                        }
                        else if ( isset( $operationResult['result'] ) )
                        {
                            $result = $operationResult['result'];
                            $resultContent = false;
                            if ( is_array( $result ) )
                            {
                                if ( isset( $result['content'] ) )
                                {
                                    $resultContent = $result['content'];
                                }
                                if ( isset( $result['path'] ) )
                                {
                                    $Result['path'] = $result['path'];
                                }
                            }
                            else
                            {
                                $resultContent = $result;
                            }
                            // Temporary fix to make approval workflow work with edit.
                            if ( strpos( $resultContent, 'Deffered to cron' ) === 0 )
                            {
                                $Result = null;
                            }
                            else
                            {
                                $Result['content'] = $resultContent;
                            }
                        }
                    }break;
                    case eZModuleOperationInfo::STATUS_CANCELLED:
                    {
                        $Result = array();
                        $Result['content'] = "Content publish cancelled<br/>";
                    }
                }

                /* If we already have a correct module result
                 * we don't need to continue module execution.
                 */
                if ( is_array( $Result ) )
                    return eZModule::HOOK_STATUS_CANCEL_RUN;
            }

            // update content object attributes array by refetching them from database
            $object = eZContentObject::fetch( $object->attribute( 'id' ) );
            $contentObjectAttributes = $object->attribute( 'contentobject_attributes' );

            // set chosen hidden/invisible attributes for object nodes
            $http          = eZHTTPTool::instance();
            $assignedNodes = $object->assignedNodes( true );
            foreach ( $assignedNodes as $node )
            {
                $nodeID               = $node->attribute( 'node_id' );
                $parentNodeID         = $node->attribute( 'parent_node_id' );
                $updateNodeVisibility =  false;
                $postVarName          = "FutureNodeHiddenState_$parentNodeID";

                if ( !$http->hasPostVariable( $postVarName ) )
                    $updateNodeVisibility = true;
                else
                {
                    $futureNodeHiddenState = $http->postVariable( $postVarName );
                    $db = eZDB::instance();
                    $db->begin();
                    if ( $futureNodeHiddenState == 'hidden' )
                        eZContentObjectTreeNode::hideSubTree( $node );
                    else if ( $futureNodeHiddenState == 'visible' )
                        eZContentObjectTreeNode::unhideSubTree( $node );
                    else if ( $futureNodeHiddenState == 'unchanged' )
                        $updateNodeVisibility = true;
                    else
                        eZDebug::writeWarning( "Unknown value for the future node hidden state: '$futureNodeHiddenState'" );
                    $db->commit();
                }

                if ( $updateNodeVisibility )
                {
                    // this might be redundant
                    $db = eZDB::instance();
                    $db->begin();
                    $parentNode = eZContentObjectTreeNode::fetch( $parentNodeID );
                    eZContentObjectTreeNode::updateNodeVisibility( $node, $parentNode, /* $recursive = */ false );
                    $db->commit();
                    unset( $node, $parentNode );
                }
            }
            unset( $assignedNodes );

            $object = eZContentObject::fetch( $object->attribute( 'id' ) );

            $newObjectName = $object->name();

            $http = eZHTTPTool::instance();

            computeRedirect( $module, $object, $version, $EditLanguage );
            // we have set redirection URI for module so we don't need to continue module execution
            return eZModule::HOOK_STATUS_CANCEL_RUN;
        }
    }
}


$Module->addHook( 'action_check', 'checkContentActions' );

$includeResult = include( 'kernel/content/attribute_edit.php' );

if ( $includeResult != 1 )
    return $includeResult;


?>
