<?php
// SOFTWARE NAME: eZ publish
// SOFTWARE RELEASE: 3.9.x
// BUILD VERSION: Objectrelationbrowse 2.0
// COPYRIGHT NOTICE: Copyright (C) 1999-2007 Contactivity bv (info@contactivity.com)
// SOFTWARE LICENSE: GNU General Public License v2.0
// NOTICE: >
//   This program is free software; you can redistribute it and/or
//   modify it under the terms of version 2.0  of the GNU General
//   Public License as published by the Free Software Foundation.
//
//   This program is distributed in the hope that it will be useful,
//   but WITHOUT ANY WARRANTY; without even the implied warranty of
//   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//   GNU General Public License for more details.
//
//   You should have received a copy of version 2.0 of the GNU General
//   Public License along with this program; if not, write to the Free
//   Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
//   MA 02110-1301, USA.
//
//
// ## END COPYRIGHT, LICENSE AND WARRANTY NOTICE ##
//

class ObjectRelationBrowseFilter
{
	function ObjectRelationBrowseFilter()
	{
	}

	function createSqlParts($params)
	{
		$sqlTables= ", ezcontentobject_link AS t0";
		$sqlJoins = " ezcontentobject_tree.contentobject_id = t0.from_contentobject_id AND ezcontentobject_tree.contentobject_version = t0.from_contentobject_version AND t0.to_contentobject_id =".$params[0]." AND ";

		return array('tables' => $sqlTables, 'joins'  => $sqlJoins);
	}
}
?>