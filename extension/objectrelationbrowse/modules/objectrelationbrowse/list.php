<?php
// SOFTWARE NAME: eZ publish
// SOFTWARE RELEASE: 3.9.x
// BUILD VERSION: Objectrelationbrowse 2.0
// COPYRIGHT NOTICE: Copyright (C) 1999-2007 Contactivity bv (info@contactivity.com)
// SOFTWARE LICENSE: GNU General Public License v2.0
// NOTICE: >
//   This program is free software; you can redistribute it and/or
//   modify it under the terms of version 2.0  of the GNU General
//   Public License as published by the Free Software Foundation.
//
//   This program is distributed in the hope that it will be useful,
//   but WITHOUT ANY WARRANTY; without even the implied warranty of
//   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//   GNU General Public License for more details.
//
//   You should have received a copy of version 2.0 of the GNU General
//   Public License along with this program; if not, write to the Free
//   Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
//   MA 02110-1301, USA.
//
//
// ## END COPYRIGHT, LICENSE AND WARRANTY NOTICE ##
//

$http =& eZHTTPTool::instance();
$Result = array();
$Result["pagelayout"] = false;
$ini =& eZINI::instance();
$Module =& $Params['Module'];
$moduleINI =& eZINI::instance( 'module.ini' );
$return_value = array();
$return_value[] = "<?xml version='1.0' standalone='yes'?><objects>";
$sqlcheck="";

$attributeID = $Params['AttributeID'];
if ( !$attributeID OR !is_numeric( $attributeID ) )
    $attributeID = 0;
$classAttribute =& eZContentClassAttribute::fetch( $attributeID );
if ($classAttribute )
{
	$class_content = $classAttribute->content();
	$classContraintlist = $class_content['class_constraint_list'];

	foreach ( $classContraintlist as $classContraint) {
		$chklist[] = "ezcontentclass.identifier = '".$classContraint."'";
	}

	$nodeID = $class_content['default_selection']['node_id'];
}

$limit = $Params['Limit'];
if ( !$limit OR !is_numeric( $limit ) )
    $limit = $moduleINI->variable( 'ModuleSettings', 'DynamicListLimit' );

$phrase = urlencode(trim($Params['Phrase']));


//set the locale
$locale= $ini->variable( 'RegionalSettings', 'ContentObjectLocale' );

//only query the database if search term(s) and nodeID have been passed
if ( ( $phrase AND $classAttribute ) )
{

  	//find the keywords
  	$keywords= explode (" ", $phrase);
	$firstWord =& $keywords[0];
	foreach( $keywords as $word ) {
		$word = str_replace( '*', '.*', $word );
		$chklist[] = sprintf( "ezcontentobject_name.name REGEXP '[[:<:]]%s'", $word );
	}

	// Only allow certain sections
	$allowedSections = $moduleINI->variable( 'ModuleSettings', 'AllowedSections' );
	foreach ( $allowedSections as $allowedSection) {
		$sectionchklist[] = "ezcontentobject.section_id = ".$allowedSection;
	}
	$chklist[] = " (".join( ' OR ', $sectionchklist ).") ";
	$sqlcheck = join( ' AND ', $chklist );

	$db =& eZDB::instance();
	$pathArray = $db->arrayQuery( "SELECT path_string FROM ezcontentobject_tree WHERE node_id=" . $nodeID );
    $path=$pathArray[0]['path_string'];

	$query = "SELECT ezcontentobject.id as object_id, ezcontentobject_name.name AS name, ezcontentobject_tree.node_id AS node_id, ezcontentobject.section_id as section_id
	FROM ezcontentobject_tree, ezcontentobject, ezcontentclass, ezcontentobject_name
	WHERE path_string LIKE '".$path."_%'
	AND $sqlcheck
	AND ezcontentclass.version = 0
	AND ezcontentobject_tree.contentobject_id = ezcontentobject.id
	AND ezcontentclass.id = ezcontentobject.contentclass_id
	AND ezcontentobject_tree.contentobject_is_published = 1
	AND ezcontentobject_tree.contentobject_id = ezcontentobject_name.contentobject_id
	AND ezcontentobject_tree.contentobject_version = ezcontentobject_name.content_version
	AND ezcontentobject_name.content_translation =  '$locale'
	ORDER BY INSTR( ezcontentobject_name.name, '$firstWord' ), ezcontentobject_name.name ASC LIMIT ".$limit;

	$resultArray = $db->arrayQuery( $query );

  	$count = count($resultArray);
 	$return_value[] = "<count>".$count."</count>";
  	if ($count > 0) {
  		foreach ($resultArray as $row) {
  			$return_value[]="<ezobject><objectid>".$row[object_id]."</objectid><objectname><![CDATA[".htmlentities( str_replace("", "", str_replace("\"", "'", $row[name] ) ) )."]]></objectname><nodeid>".$row[node_id]."</nodeid></ezobject>";
  		}
  	}

  	mysql_free_result($result);
}
else
{
	$return_value[] = "<count>0</count><ezobject/>";  //nodeID or keyword parameters empty
}
$return_value[] = "</objects>";
header('Content-Type: text/xml');
echo implode("", $return_value);
eZExecution::cleanExit();
?>