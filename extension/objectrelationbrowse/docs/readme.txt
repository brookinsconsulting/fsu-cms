
Object Relation Browse Datatype
-------------------------------

/*
    Object Relation Browse extension for eZ publish 3.9
    Developed by Contactivity bv, Leiden the Netherlands
    http://www.contactivity.com, info@contactivity.com
    

    This file may be distributed and/or modified under the terms of the
    GNU General Public License" version 2 as published by the Free
    Software Foundation and appearing in the file LICENSE.GPL included in
    the packaging of this file.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.
    
    The "GNU General Public License" (GPL) is available at
    http://www.gnu.org/copyleft/gpl.html.
    
    
*/


1. Context
----------
One of the most powerful features of eZ publish 3 is the option to create relationships between many kinds of objects. However, the standard 'browse' method used to relate objects proved to be too cumbersome when working with a large number of objects and many relations per object.


We needed a datatype that would:
- display all available objects for relation in a single list;
- provide functionality to dynamically filter objects from the list on basis of the name of the object and wildcards;
- allow the selection of multiple objects from a list without having to 'browse'; 

The main purpose of the objectrelationbrowse datatype is to allow the creation of all types of lists (AJAX, listbox, dropdown, etc.) on basis of objects. These lists may then be used to establish establish object relations.


2. Features
-----------
We have created the objectrelationbrowse datatype to:
- option to display all available objects in a single,dynamically filterable list embedded in the edit form (see:ajax_edit.png);
- option to create new and/or edit embedded objects in the edit form; and
- option to display listboxes, dropdowns and checkboxes on basis of list/tree.


3. Settings (see class_edit.png)
-----------
a. Type of listing
Available options are: 'Default browse' (see default_browse.png), 'AJAX' (see ajax_edit.png), 'Listbox', 'Dropdown' and 'Checkbox'. The 'default browse' option uses the standard eZ publish method to add related objects. In addition, it also allows adding new related objects through an embedded form. The 'AJAX' option displays an inline form through which existing objects can be added. 'Listbox', 'dropdown' and 'checkbox' allow you to select related objects the standard way. 

b. Depth
Only applicable to list types 'Listbox', 'Dropdown' and 'Checkbox'. Available options are 'list' and 'tree'. 'Tree' will create a list of all available objects underneath a certain node (see list_tree.png), 'list' will only fetch one level.

c. Type
Only applicable to list type 'Default browse'. Allows the user to add 'only new objects', 'only existing objects' or 'new and existing objects'. If 'only new objects' is selected, the content/browse option is disabled. If 'only existing objects' is selected, the "Create new object" option is disabled.

d. Allowed classes
Applicable to all list types. Indicates which class(es) the user can add, or, in case of the 'default browse' option, which class(es) the user is allowed to create. 

e. Allow edit
Only applicable to list type 'Default browse'. Indicates if the user should be allowed to edit the related objects. 

f. Default selection node
Indicates where the user starts browsing for related objects, or the node from which the listbox, dropdown or checkboxed will be created.

g. Default placement
Only applicable to list type 'Default browse'. Indicates where newly created objects will be placed in the content tree. If no default node has been specified, newly created objects will not appear in the content tree. 


4. 'AJAX' list type (see ajax_edit.png)
-----------
In the edit interface, you can start using the AJAX interface by clicking on the "search" button. Start typing in the textfield that appears, and the box below it will automatically show all objects with matching object names. Then select the object that you want to create a relationship to, and click the "add selected" button.


5. Feedback
-----------
Please send all remarks, comments and suggestions for improvement to info@contactivity.com.


6. Disclaimer
-----------
This program is free software; you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation; either version 2 of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.


7. Credits
-----------
Thanks to Pike who developed the user interface for the AJAX.