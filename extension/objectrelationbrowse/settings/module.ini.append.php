<?php /* #?ini charset="iso-8859-1"?

[ModuleSettings]
ExtensionRepositories[]=objectrelationbrowse

#By default the AJAX list shows a maximum of 100 objects
DynamicListLimit=100

#By default the AJAX list only shows objects from the standard section (1) and media section (3)
AllowedSections[]
AllowedSections[]=1
AllowedSections[]=3

?>
