<?php

#The name of the filter.
[ObjectRelationBrowseFilter]

#The name of the extension where the filtering code is defined.
ExtensionName=objectrelationbrowse

#The name of the filter class.
ClassName=ObjectRelationBrowseFilter

#The name of the method which is called to generate the SQL parts.
MethodName=createSqlParts

#The file which should be included (extension/myextension will automatically be prepended).
FileName=datatypes/ezobjectrelationbrowse/ezobjectrelationbrowsefilter.php

?>



