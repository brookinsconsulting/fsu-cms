<?php

class get_link_objectOperator
{
    var $Operators;

    function get_link_objectOperator( $name = "get_link_object" )
    {
	$this->Operators = array( $name );
    }


    function operatorList()
    {
	return $this->Operators;
    }


    function namedParameterPerOperator()
    {
        return true;
    }   

    function namedParameterList()
    {
        return array( 'get_link_object' => array() );
    }


    function modify( $tpl, $operatorName, $operatorParameters, $rootNamespace, $currentNamespace, &$operatorValue, $namedParameters )
    {

         $my_node_id = eZURLAliasML::fetchNodeIDByPath($operatorValue);

         if (is_numeric($my_node_id) && $my_node = eZContentObjectTreeNode::fetch( $my_node_id ) ) {

	     $operatorValue = $my_node;	
	     return true;
	  } else {
	     $operatorValue = false;
	     return false;
	  }

    }
}

?>
