<?php
 
$hex = $_GET['col'];
$red   = hexdec( $hex[0] . $hex[1] );
$green = hexdec( $hex[2] . $hex[3] );
$blue  = hexdec( $hex[4] . $hex[5] );


// set the HTTP header type to PNG
header("Content-type: image/png"); 

// set the width and height of the new image in pixels
$width = 14;
$height = 14;

// create a pointer to a new true colour image
$im = ImageCreateTrueColor($width, $height); 

// switch on image antialising if it is available
ImageAntiAlias($im, true);

// sets background to white
$white = ImageColorAllocate($im, 255, 255, 255); 
ImageFillToBorder($im, 0, 0, $white, $white);

// define blue colour



$blue = ImageColorAllocate($im, $red, $green, $blue);

// draw an empty circle
ImageFilledEllipse($im, 7, 7, 8, 8, $blue);

imagecolortransparent($im, $white);


// send the new PNG image to the browser
ImagePNG($im); 

// destroy the reference pointer to the image in memory to free up resources
ImageDestroy($im);

 
?>