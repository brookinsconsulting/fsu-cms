<?php

class clearAgendaCacheType extends eZWorkflowEventType
{
	const ID = "clearagendacache";

    function __construct()
    {
        $this->eZWorkflowEventType( clearAgendaCacheType::ID, "Clear agenda cache" );
        $this->setTriggerTypes( array( 'content' => array( 'publish' => array( 'after' ) ) ) );
    }

    function execute( $process, $event )
    {
		$calINI = eZINI::instance( 'ezphpicalendar.ini' );
        $class_ar = $calINI->variable( 'ClassSettings', 'CalClassIds' );
        $parameters = $process->attribute( 'parameter_list' );
        $objectId= $parameters['object_id'];
        $object = eZContentObject::fetch( $parameters['object_id'] );	
        foreach ($object->assignedNodes() as $start_node) {
			foreach ($start_node->fetchPath() as $path_node) {
				$path_object = $path_node->object();
		        $content_class = $path_object->contentClass();
			 	$class_id = $content_class->attribute('id');
				if (in_array($class_id,$class_ar) ) {
					$this_node_id = $path_node->attribute('node_id');
					$mycal = new eZPhpical($this_node_id);
					$mycal->regenerate();
				}
			}
		}
        return eZWorkflowType::STATUS_ACCEPTED;
    }
}

eZWorkflowEventType::registerEventType( clearAgendaCacheType::ID, "clearagendacachetype" );

?>