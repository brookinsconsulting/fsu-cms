<?php

$cal_ids =  $Params['NodeID'];

if (!is_array($cal_ids)) $cal_ids = array($cal_ids);

if ($cal_ids[0]=='all') {
	$ezphpicalendarini = eZINI::instance( 'ezphpicalendar.ini' );
	$calclasses = $ezphpicalendarini->variable( "ClassSettings", "CalClassIds" );
	if (!is_array($calclasses)) $calclasses = array($calclasses);
	$params = array('ClassFilterType' => 'include', 'ClassFilterArray' => $calclasses );
	$cal_ids = eZContentObjectTreeNode::subTreeByNodeID( $params, 2 );
}

foreach ($cal_ids as $cal_id) {

	if (is_object($cal_id)) $cal_id = $cal_id->attribute('node_id');

	$mycal = new eZPhpical($cal_id);

	if ($mycal->regenerate() ) {
		echo 'Success';
	} else {
		echo 'Failure';
	}
}

eZExecution::cleanExit(); 

?> 