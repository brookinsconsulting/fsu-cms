<?php

$url = explode('/', $_SERVER['SERVER_PROTOCOL']);
$url = $url[0];
$url .= '://'.$_SERVER['HTTP_HOST'].'/extension/phpicalendar/'.$Params['Url'];
if ($_SERVER['QUERY_STRING'] != '') $url .= "?". $_SERVER['QUERY_STRING'];

if ($Params['Url'] != 'search.php') { 

	$extraPath = eZDir::filenamePath( "" );
	$ini = eZINI::instance();
	$currentSiteAccess = $GLOBALS['eZCurrentAccess']['name'];


	$UserParameters = array();
	$res = eZTemplateDesignResource::instance();
	$keys = $res->keys();
	if ( isset( $keys['layout'] ) )
	    $layout = $keys['layout'];
	else
	    $layout = false;

	$user = eZUser::currentUser();

	$LanguageCode = "eng-US";
	$StartNodes = $_GET['cal'];
	$startnodes_ar = explode(',', $StartNodes);
	$root_id = str_replace("cal_node", "", $startnodes_ar[0]);

	$getdate = $_GET['getdate'];

	if ($getdate == '')
		$getdate = date('Ymd');
		
	$Day = substr($getdate, 6, 2 );
	$Month = substr($getdate, 4, 2 );
	$Year = substr($getdate, 0, 4 );
	$View = $Params['View'];
	$ViewMode = $Params['Url'];
	$Parent_ID = 2;
	$Offset = $Parent_ID;

	$viewParameters = array( 'offset' => $Offset,
	                     'year' => $Year,
	                     'month' => $Month,
	                     'day' => $Day,
	                     'namefilter' => implode('|',$startnodes_ar) );


	$viewParameters = array_merge( $viewParameters, $UserParameters );

	$cacheFileArray = eZNodeviewfunctions::generateViewCacheFile( $user, $root_id, $Offset, $layout, $LanguageCode, $ViewMode, $viewParameters );

	$cacheDir = $cacheFileArray['cache_dir'];

	if ( !file_exists( $cacheDir ) )
	{
		$perm = octdec( $ini->variable( 'FileSettings', 'StorageDirPermissions' ) );
		eZDir::mkdir( $cacheDir, $perm, true );
	}

	$cacheFilePath = $cacheFileArray['cache_path'];

	if ( file_exists( $cacheFilePath ) ) {
		$last_write = @filemtime( $cacheFilePath );
		if (eZExpiryHandler::getTimestamp('content-view-cache',-1) < $last_write && (time() - $last_write) < 60*60 ) {
			$contents = file_get_contents( $cacheFilePath );
			echo $contents;
			echo "<!-- Loaded from cache $cacheFilePath -->";
			eZExecution::cleanExit(); 
			return true;
		}
	} 

}

$ch = curl_init( $url );

ob_start();
curl_exec( $ch );
echo "<!-- Generated from cache $url -->";
$big_cal_output = ob_get_contents();
ob_end_flush();

if ($Params['Url'] != 'search.php') { 

	require_once( 'kernel/classes/ezclusterfilehandler.php' );
	$file = eZClusterFileHandler::instance( "$cacheFilePath" );
	$file->storeContents( $big_cal_output, 'viewcache', 'pdf' );
	$file->move( $cacheFilePath );

}

eZExecution::cleanExit();

?> 
