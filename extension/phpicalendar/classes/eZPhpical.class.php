<?php

class eZPhpical {

	function eZPhpical($node_id) {
		$this->node_id = $node_id;
	}
	
	function regenerate() {
		$cal_id = $this->node_id;

		$cal_node = eZContentObjectTreeNode::fetch($cal_id);

		$cal_node_data = $cal_node->dataMap();
		
		
		if (array_key_exists('url',$cal_node_data)) {
			$FilePath = 'extension/phpicalendar/calendars/cal_node'.$cal_id.'.inc';
			if (file_exists($FilePath)) unlink($FilePath) or die('Could not unlink .inc!');
			$url=$cal_node_data['url']->content();
			$output = $url;
			$calcol = $cal_node_data['cal_color']->content();
			$output .= "||".$calcol;
			if (!$fh = fopen($FilePath, 'w')) return false;
			fwrite($fh, $output);
			fclose($fh);
			return true;
		}
		
		$FilePath = 'extension/phpicalendar/calendars/cal_node'.$cal_id.'.data';
		if (file_exists($FilePath)) unlink($FilePath) or die('Could not unlink .data!');
		$output = $cal_node->Name;
		$calcol = $cal_node_data['cal_color']->content();
		$output .= "||".$calcol;
		$url_alias = $cal_node->urlAlias();
		$output .= "||".$url_alias;

		
		if (!$fh = fopen($FilePath, 'w')) return false;
		fwrite($fh, $output);
		fclose($fh);
		
		$v = new vcalendar(); // create a new calendar instance
		
		$ini = eZINI::instance();
		$myhost = $ini->variable('SiteSettings', 'SiteURL');

		$v->setConfig( $cal_id, $myhost ); // set Your unique id
		$v->setProperty( 'method', 'PUBLISH' ); // required of some calendar software
		
		$ezphpicalendarini = eZINI::instance( 'ezphpicalendar.ini' );

		$eventclasses = $ezphpicalendarini->variable( "ClassSettings", "EventClassIds" );
		
		if (!is_array($eventclasses)) $eventclasses = array($eventclasses);

		$params = array('ClassFilterType' => 'include', 'ClassFilterArray' => $eventclasses, 'Depth' => 1, 'DepthOperator' => 'eq');
		$events = eZContentObjectTreeNode::subTreeByNodeID( $params, $cal_id );

		foreach ($events as $key => $this_event ) {
			$vevent = new vevent(); // create an event calendar component
			$event_id = $this_event->attribute('node_id');
			$objData = $this_event->dataMap();
			$location = '';
			foreach (array('location','city','region') as $at ) {
				if (array_key_exists($at, $objData)) {
					$addme = $objData[$at]->content(); 
					if ($addme != '' && $location == '') { $location = $addme; }
					elseif ($addme != '' && $location != '') {$location .= ", ".$addme; }
				}
			}

			$time_from = $objData['from_time']->attribute('data_int');
			$time_to = $objData['to_time']->attribute('data_int');
			
			if ($time_to == 0) {
				$time_to = mktime (23, 59, 59, date('n',$time_from), date('j',$time_from), date('Y',$time_from) );
			}
			
			if (date('His', $time_to) == '000000') {
				$time_to = mktime (23, 59, 59, date('n',$time_to), date('j',$time_to), date('Y',$time_to) );
			}
			

			$conc_time_from  = date('Y',$time_from).date('m',$time_from).date('d',$time_from);


			$start_ar =  array( 'year'=>date('Y',$time_from),
		 						'month'=>date('m',$time_from), 
								'day'=>date('d',$time_from), 
								'hour'=>date('H',$time_from), 
								'min'=>date('i',$time_from),  
								'sec'=>date('s',$time_from) );
			$vevent->setProperty( 'dtstart', $start_ar );

			$end_ar = array('year'=>date('Y',$time_to),
							'month'=>date('m',$time_to), 
							'day'=>date('d',$time_to), 
							'hour'=>date('H',$time_to), 
							'min'=>date('i',$time_to),  
							'sec'=>date('s',$time_to) );
							
			$vevent->setProperty( 'dtend', $end_ar );

			$vevent->setProperty( 'summary', $this_event->object()->name()  );
			$addme = $objData['text']->content();
			$addme = $addme->attribute( 'output' );
			$addme = $addme->attribute( 'output_text' );
			
			$first_image = false;
			

			if (array_key_exists('image', $objData) && is_array( $objData['image']->content()->imageAlias('small') ) ) {
				
				$evimage_r = $objData['image']->content()->imageAlias('small');
				$first_image = "/". $evimage_r['url'];
			} elseif (stripos($addme, '<img') !== false) {
	            $imgsrc_regex = '#<\s*img [^\>]*src\s*=\s*(["\'])(.*?)\1#im';
	            preg_match($imgsrc_regex, $addme, $matches);
				$first_image=$matches[2];
			}
			
			$addme = trim(strip_tags($addme));

			if (array_key_exists('price', $objData)) {
				$evprice = $objData['price']->content();
				if ($evprice) {
				$addme .= "\r\n\r\n<table class='ev_atts_tab'><tr><td class='label'><b>Price:</b></td><td>";
				$addme .= $objData['price']->content();
			    $addme .= "</td></tr></table>";
				}
	    	}
			$vevent->setProperty( 'description', $addme);

			$vevent->setProperty( 'location', $location );
			if (array_key_exists('url', $objData)) {
				$evul = $objData['url']->content() ;
				if ($evul) {
					$vevent->setProperty( 'URL', $evul );
				}
	    	}
			$organizer = "";
			$orphone = "";
			$orgmail = "";
			if (array_key_exists('contact', $objData)) $organizer = $objData['contact']->content();
			if (array_key_exists('phone', $objData)) $orphone = $objData['phone']->content();
			if (strlen(trim($organizer)) > 0 && strlen(trim($orphone)) > 0) $organizer .= " - ";
			if (strlen($orphone) > 0) $organizer .= $orphone;
			if (array_key_exists('email', $objData)) $orgemail = $objData['email']->content();
			if (strlen(trim($orgemail)) == 0) $orgemail = "noreply@$myhost";
			$vevent->setOrganizer( $orgemail, array('CN' => $organizer) );
			
			$my_id = $this_event->object()->ID;
			if ($first_image) {
				$my_id .= "-i".base64_encode($first_image);
			}
			$uid = $time_from.$time_to.$my_id."@".$myhost."-".$event_id;
			$vevent->setProperty( 'uid', $uid);

		    $repeat_options = array('DAILY','WEEKLY','MONTHLY','YEARLY');
		    $rind = $objData['repeat']->attribute('data_text');
			$rcount = $objData['repeat_count']->attribute('data_int');
			$rint = $objData['repeat_interval']->attribute('data_int');
			if ($rind != '' && $rcount > 1 && $rint > 0 ) {
				$repeat = $repeat_options[$rind];
				$vevent->setProperty( 'rrule', array( 'FREQ' => $repeat, 'count' => $rcount, 'interval' => $rint));
			}

			$v->setComponent ( $vevent ); // add event to calendar

		}
		$calpath = "extension/phpicalendar/calendars";
		$calfile = 'cal_node'.$cal_id.'.ics';
		
		if (file_exists($calpath.'/'.$calfile)) unlink($calpath.'/'.$calfile) or die('Could not unlink .ics!');

		$v->setConfig( 'directory', $calpath); // set directory
		$v->setConfig( 'filename',  $calfile); // set file name
//

		if (!$v->saveCalendar()) {
			return false;
		} else {
			return true;
		}
		; // save calendar to file
	}
	
	var $node_id;

}
?>
