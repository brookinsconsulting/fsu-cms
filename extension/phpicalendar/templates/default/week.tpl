<div id='cal_wrapper'>
	
{TOP_MONTHMAJORS}

<div id='cal_nav'>
<div id='cal_nav_tabs'>{HEADER}</div>
<div id='display_date_wrap'>
<div id='cal_prev_month'><a class="cal_nav_month" href="javascript: get_callendar_xml('week.php?cal={CAL}&amp;getdate={PREV_WEEK}')"><< Previous Week</a></div>
<div id='cal_next_month'><a class="cal_nav_month" href="javascript: get_callendar_xml('week.php?cal={CAL}&amp;getdate={NEXT_WEEK}')">Next Week >></a></div>
<h2>{DISPLAY_DATE}</h2> 
</div>
</div>			

<div id='main_cal'>

	<table width="100%" border="0" cellspacing="0" cellpadding="0">
		<tr>
			<td></td>
			<td style="width: 1px;"></td>
			<!-- loop daysofweek on -->
			<td style="width: 11%;" {COLSPAN} align="center" class="{ROW1}" onmouseover="this.className='{ROW2}'" onmouseout="this.className='{ROW3}'" onclick="get_callendar_xml('week.php?cal={CAL}&amp;getdate={DAYLINK}')">
				<a class="ps3" href="javascript: get_callendar_xml('day.php?cal={CAL}&amp;getdate={DAYLINK}')"><span class="V9BOLD">{DAY}</span></a> 
			</td>
			<!-- loop daysofweek off -->
		</tr>
		<tr valign="top" id="allday">
			<td class="rowOff2"><img src="images/spacer.gif" height="1" alt=" " /></td>
			<td style="width: 1px;"></td>
			<!-- loop alldaysofweek on -->
			<td style="width: 11%;" {COLSPAN} class="rowOff">
				<!-- loop allday on -->
				<div class="alldaybg_{CALNO}">
					{ALLDAY}
					<img src="images/spacer.gif" style="width: 11%;" height="1" alt=" " />
				</div>
				<!-- loop allday off -->
			</td>
			<!-- loop alldaysofweek off -->
		</tr>
		<!-- loop row on -->
		<tr>
			<td rowspan="4" align="center" valign="top" width="auto" class="timeborder">9:00 AM</td>
			<td width="1" height="15"></td>
			<td class="dayborder">&nbsp;</td>
		</tr>
		<tr>
			<td width="1" height="15"></td>
			<td class="dayborder2">&nbsp;</td>
		</tr>
		<tr>
			<td width="1" height="15"></td>
			<td class="dayborder">&nbsp;</td>
		</tr>
		<tr>
			<td width="1" height="15"></td>
			<td class="dayborder2">&nbsp;</td>
		</tr>
		<!-- loop row off -->
		<!-- loop event on -->
		<div class="eventfont">
			<div class="eventbg_{EVENT_CALNO}">{CONFIRMED}<b>{EVENT_START}</b></div>
			<div class="padd">{EVENT}</div>
		</div>
		<!-- loop event off -->
	</table>	
	
</div>
{BOTTOM_MONTHMAJORS}
</div>
{FOOTER}