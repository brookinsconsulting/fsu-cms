<div id='cal_wrapper'>

{TOP_MONTHMAJORS}

<div id='cal_legend'>
{LEGEND}
</div>

<div id='cal_nav'>
<div id='cal_nav_tabs'>{HEADER}</div>
<div id='display_date_wrap'>
<div id='cal_next_month'><a class="cal_nav_month" href="javascript: get_callendar_xml('month.php?cal={CAL}&amp;getdate={NEXT_MONTH}')">Next Month >></a></div>
<div id='cal_prev_month'><a class="cal_nav_month" href="javascript: get_callendar_xml('month.php?cal={CAL}&amp;getdate={PREV_MONTH}')"><< Previous Month</a></div>
<h2>{DISPLAY_DATE}</h2>

</div>	
</div>			

<div id='main_cal'>
{MONTH_LARGE|+0}
</div>

{BOTTOM_MONTHMAJORS}

</div>
{FOOTER}




