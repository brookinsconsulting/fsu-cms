<div id='cal_wrapper'>

<div id='cal_legend'>
{LEGEND}
</div>

<div id='cal_nav'>

<div id='cal_nav_tabs'>{HEADER}</div>
<div id='display_date_wrap'>
<h2>SEARCH RESULTS</h2> 
</div>			
</div>		

<div id='search_string'><p class="V12">{L_QUERY}: {FORMATTED_SEARCH}</p></div>	

<div id='main_cal_back'>
	<div id='main_cal' class='hideright'>
	<table border="0" cellspacing="0" cellpadding="0" class="calborder">
		<tr>
			<td align="left">
				<div style="padding: 10px;">
				
					<!-- switch results on -->
					<font class="V12"><b>{EVENT_TEXT}</b></font><br />
					<div style="margin-left: 10px; margin-bottom: 10px;">
						<table width="100%" border="0" cellspacing="1" cellpadding="1">
							<tr>
								<td width="120" class="G10BOLD">{L_TIME}:</td>
								<td align="left" class="G10B"><a class="ps3" href="javascript: get_callendar_xml('day.php?cal={CAL}&amp;getdate={KEY}')">{DAYOFMONTH}</a> {EVENT_START}</td>
							</tr><tr>
								<td width="120" class="G10BOLD">{L_LOCATION}:</td>
								<td align="left" class="G10B">{LOCATION}</td>
							</tr>
							<!-- switch recur on -->
							<tr>
								<td valign="top" width="100" class="G10BOLD">{L_RECURRING_EVENT}:</td>
								<td valign="top" align="left" class="G10B">{RECUR}</td>
							</tr>
							<!-- switch recur off -->
							<!-- switch description on -->
							<tr>
								<td valign="top" width="100" class="G10BOLD">{L_DESCRIPTION}:</td>
								<td valign="top" align="left" class="G10B">{DESCRIPTION}</td>
							</tr>
							<!-- switch description off -->
						</table>
					</div>
					<!-- switch exceptions on -->		
					<font class="V10"><i>{L_EXCEPTION}</i>: {EVENT_TEXT}</font><br />
					<div style="margin-left: 10px;">
						<table width="100%" border="0" cellspacing="1" cellpadding="1">
							<tr>
								<td width="100" class="V10">{L_TIME}:</td>
								<td align="left" class="V10"><a class="ps3" href="day.php?cal={CAL}&amp;getdate={KEY}">{DAYOFMONTH}</a> {EVENT_START}</td>
							</tr>
							<!-- switch except_recur on -->
							<tr>
								<td valign="top" width="100" class="V10">{L_RECURRING_EVENT}:</td>
								<td valign="top" align="left" class="V10">{EXCEPT_RECUR}</td>
							</tr>
							<!-- switch except_recur off -->
							<!-- switch except_description on -->
							<tr>
								<td valign="top" width="100" class="V10">{L_DESCRIPTION}:</td>
								<td valign="top" align="left" class="V10">{EXCEPT_DESCRIPTION}</td>
							</tr>
							<!-- switch except_description off -->
						</table>
					</div>
					<!-- switch exceptions off -->		

					<!-- switch results off -->
				
					<!-- switch no_results on -->
					<div align="center">
						<p class="V12">{L_NO_RESULTS}</p>
					</div>
					<!-- switch no_results off -->
				</div>
			</td>
		</tr>
	</table>
	</div>

	<div id='News-Result'></div>

	<div id='pag_wrapper'>
		<div id='News-Pagination' class='pagination'></div>
	</div>

</div>

</div>

{FOOTER}



