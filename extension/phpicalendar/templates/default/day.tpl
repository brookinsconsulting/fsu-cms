<div id='cal_wrapper'>
	
{TOP_MONTHMAJORS}

<div id='cal_nav'>
<div id='cal_nav_tabs'>{HEADER}</div>
<div id='display_date_wrap'>
<div id='cal_next_month'><a class="cal_nav_month" href="javascript: get_callendar_xml('day.php?cal={CAL}&amp;getdate={NEXT_DAY}')">Next Day >></a></div>
<div id='cal_prev_month'><a class="cal_nav_month" href="javascript: get_callendar_xml('day.php?cal={CAL}&amp;getdate={PREV_DAY}')"><< Previous Day</a></div>
<h2>{DISPLAY_DATE}</h2> 
</div>	
</div>			

<div id='main_cal'>
<table width="100%" border="0" cellspacing="0" cellpadding="0" class="calborder">
	<tr id="allday">
		<td>
			<!-- loop allday on -->
			<div class="alldaybg_{CALNO}">
				{ALLDAY}
			</div>
			<!-- loop allday off -->
		</td>
	</tr>
	<tr>
		<td align="center" valign="top" colspan="3">
			<table width="100%" border="0" cellspacing="0" cellpadding="0">
				<!-- loop row on -->
				<tr>
					<td rowspan="4" align="center" valign="top" width="60" class="timeborder">9:00 AM</td>
					<td width="1" height="15"></td>
					<td class="dayborder">&nbsp;</td>
				</tr>
				<tr>
					<td width="1" height="15"></td>
					<td class="dayborder2">&nbsp;</td>
				</tr>
				<tr>
					<td width="1" height="15"></td>
					<td class="dayborder">&nbsp;</td>
				</tr>
				<tr>
					<td width="1" height="15"></td>
					<td class="dayborder2">&nbsp;</td>
				</tr>
				<!-- loop row off -->
				<!-- loop event on -->
				<div class="eventfont">
					<div class="eventbg_{EVENT_CALNO}">{CONFIRMED}<b>{EVENT_START}</b> - {EVENT_END}</div>
					<div class="padd">{EVENT}</div>
				</div>
				<!-- loop event off -->
			</table>
		</td>
	</tr>
</table>
</div>
{BOTTOM_MONTHMAJORS}
</div>
{FOOTER}