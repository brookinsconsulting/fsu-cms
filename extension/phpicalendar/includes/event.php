<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
		"http://www.w3.org/TR/1999/REC-html401-19991224/loose.dtd">
<html>
<head>
	<meta http-equiv="content-type" content="text/html;charset=utf-8">
	<title>Phpical Event</title>
	<link rel="stylesheet" type="text/css" href="/extension/phpicalendar/templates/default/default.css">
	<link rel="stylesheet" type="text/css" href="/extension/ezwebin/design/ezwebin/stylesheets/core.css">
	
	<script src="/extension/banff/design/banff/javascript/sifr_js.php" type="text/javascript"></script>
	<script src="/extension/banff/design/banff/javascript/sifr_config.js" type="text/javascript"></script>
	
	<script>
	
	var dateFormat = function () {
		var	token = /d{1,4}|m{1,4}|yy(?:yy)?|([HhMsTt])\1?|[LloSZ]|"[^"]*"|'[^']*'/g,
			timezone = /\b(?:[PMCEA][SDP]T|(?:Pacific|Mountain|Central|Eastern|Atlantic) (?:Standard|Daylight|Prevailing) Time|(?:GMT|UTC)(?:[-+]\d{4})?)\b/g,
			timezoneClip = /[^-+\dA-Z]/g,
			pad = function (val, len) {
				val = String(val);
				len = len || 2;
				while (val.length < len) val = "0" + val;
				return val;
			};

		// Regexes and supporting functions are cached through closure
		return function (date, mask, utc) {
			var dF = dateFormat;

			// You can't provide utc if you skip other args (use the "UTC:" mask prefix)
			if (arguments.length == 1 && Object.prototype.toString.call(date) == "[object String]" && !/\d/.test(date)) {
				mask = date;
				date = undefined;
			}

			// Passing date through Date applies Date.parse, if necessary
			date = date ? new Date(date) : new Date;
			if (isNaN(date)) throw SyntaxError("invalid date");

			mask = String(dF.masks[mask] || mask || dF.masks["default"]);

			// Allow setting the utc argument via the mask
			if (mask.slice(0, 4) == "UTC:") {
				mask = mask.slice(4);
				utc = true;
			}

			var	_ = utc ? "getUTC" : "get",
				d = date[_ + "Date"](),
				D = date[_ + "Day"](),
				m = date[_ + "Month"](),
				y = date[_ + "FullYear"](),
				H = date[_ + "Hours"](),
				M = date[_ + "Minutes"](),
				s = date[_ + "Seconds"](),
				L = date[_ + "Milliseconds"](),
				o = utc ? 0 : date.getTimezoneOffset(),
				flags = {
					d:    d,
					dd:   pad(d),
					ddd:  dF.i18n.dayNames[D],
					dddd: dF.i18n.dayNames[D + 7],
					m:    m + 1,
					mm:   pad(m + 1),
					mmm:  dF.i18n.monthNames[m],
					mmmm: dF.i18n.monthNames[m + 12],
					yy:   String(y).slice(2),
					yyyy: y,
					h:    H % 12 || 12,
					hh:   pad(H % 12 || 12),
					H:    H,
					HH:   pad(H),
					M:    M,
					MM:   pad(M),
					s:    s,
					ss:   pad(s),
					l:    pad(L, 3),
					L:    pad(L > 99 ? Math.round(L / 10) : L),
					t:    H < 12 ? "a"  : "p",
					tt:   H < 12 ? "am" : "pm",
					T:    H < 12 ? "A"  : "P",
					TT:   H < 12 ? "AM" : "PM",
					Z:    utc ? "UTC" : (String(date).match(timezone) || [""]).pop().replace(timezoneClip, ""),
					o:    (o > 0 ? "-" : "+") + pad(Math.floor(Math.abs(o) / 60) * 100 + Math.abs(o) % 60, 4),
					S:    ["th", "st", "nd", "rd"][d % 10 > 3 ? 0 : (d % 100 - d % 10 != 10) * d % 10]
				};

			return mask.replace(token, function ($0) {
				return $0 in flags ? flags[$0] : $0.slice(1, $0.length - 1);
			});
		};
	}();

	// Some common format strings
	dateFormat.masks = {
		"default":      "ddd mmm dd yyyy HH:MM:ss",
		shortDate:      "m/d/yy",
		mediumDate:     "mmm d, yyyy",
		longDate:       "mmmm d, yyyy",
		fullDate:       "dddd, mmmm d, yyyy",
		shortTime:      "h:MM TT",
		mediumTime:     "h:MM:ss TT",
		longTime:       "h:MM:ss TT Z",
		isoDate:        "yyyy-mm-dd",
		isoTime:        "HH:MM:ss",
		isoDateTime:    "yyyy-mm-dd'T'HH:MM:ss",
		isoUtcDateTime: "UTC:yyyy-mm-dd'T'HH:MM:ss'Z'"
	};

	// Internationalization strings
	dateFormat.i18n = {
		dayNames: [
			"Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat",
			"Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"
		],
		monthNames: [
			"Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec",
			"January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"
		]
	};

	// For convenience...
	Date.prototype.format = function (mask, utc) {
		return dateFormat(this, mask, utc);
	};

	function trim (str, charlist) {
	    // http://kevin.vanzonneveld.net
	    // +   original by: Kevin van Zonneveld (http://kevin.vanzonneveld.net)
	    // +   improved by: mdsjack (http://www.mdsjack.bo.it)
	    // +   improved by: Alexander Ermolaev (http://snippets.dzone.com/user/AlexanderErmolaev)
	    // +      input by: Erkekjetter
	    // +   improved by: Kevin van Zonneveld (http://kevin.vanzonneveld.net)
	    // +      input by: DxGx
	    // +   improved by: Steven Levithan (http://blog.stevenlevithan.com)
	    // +    tweaked by: Jack
	    // +   bugfixed by: Onno Marsman
	    // *     example 1: trim('    Kevin van Zonneveld    ');
	    // *     returns 1: 'Kevin van Zonneveld'
	    // *     example 2: trim('Hello World', 'Hdle');
	    // *     returns 2: 'o Wor'
	    // *     example 3: trim(16, 1);
	    // *     returns 3: 6

	    var whitespace, l = 0, i = 0;
	    str += '';

	    if (!charlist) {
	        // default list
	        whitespace = " \n\r\t\f\x0b\xa0\u2000\u2001\u2002\u2003\u2004\u2005\u2006\u2007\u2008\u2009\u200a\u200b\u2028\u2029\u3000";
	    } else {
	        // preg_quote custom list
	        charlist += '';
	        whitespace = charlist.replace(/([\[\]\(\)\.\?\/\*\{\}\+\$\^\:])/g, '\$1');
	    }

	    l = str.length;
	    for (i = 0; i < l; i++) {
	        if (whitespace.indexOf(str.charAt(i)) === -1) {
	            str = str.substring(i);
	            break;
	        }
	    }

	    l = str.length;
	    for (i = l - 1; i >= 0; i--) {
	        if (whitespace.indexOf(str.charAt(i)) === -1) {
	            str = str.substring(0, i + 1);
	            break;
	        }
	    }

	    return whitespace.indexOf(str.charAt(0)) === -1 ? str : '';
	}
	
	
	var Base64 = {

	    // private property
	    _keyStr : "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=",

	    // public method for encoding
	    encode : function (input) {
	        var output = "";
	        var chr1, chr2, chr3, enc1, enc2, enc3, enc4;
	        var i = 0;

	        input = Base64._utf8_encode(input);

	        while (i < input.length) {

	            chr1 = input.charCodeAt(i++);
	            chr2 = input.charCodeAt(i++);
	            chr3 = input.charCodeAt(i++);

	            enc1 = chr1 >> 2;
	            enc2 = ((chr1 & 3) << 4) | (chr2 >> 4);
	            enc3 = ((chr2 & 15) << 2) | (chr3 >> 6);
	            enc4 = chr3 & 63;

	            if (isNaN(chr2)) {
	                enc3 = enc4 = 64;
	            } else if (isNaN(chr3)) {
	                enc4 = 64;
	            }

	            output = output +
	            this._keyStr.charAt(enc1) + this._keyStr.charAt(enc2) +
	            this._keyStr.charAt(enc3) + this._keyStr.charAt(enc4);

	        }

	        return output;
	    },

	    // public method for decoding
	    decode : function (input) {
	        var output = "";
	        var chr1, chr2, chr3;
	        var enc1, enc2, enc3, enc4;
	        var i = 0;

	        input = input.replace(/[^A-Za-z0-9\+\/\=]/g, "");

	        while (i < input.length) {

	            enc1 = this._keyStr.indexOf(input.charAt(i++));
	            enc2 = this._keyStr.indexOf(input.charAt(i++));
	            enc3 = this._keyStr.indexOf(input.charAt(i++));
	            enc4 = this._keyStr.indexOf(input.charAt(i++));

	            chr1 = (enc1 << 2) | (enc2 >> 4);
	            chr2 = ((enc2 & 15) << 4) | (enc3 >> 2);
	            chr3 = ((enc3 & 3) << 6) | enc4;

	            output = output + String.fromCharCode(chr1);

	            if (enc3 != 64) {
	                output = output + String.fromCharCode(chr2);
	            }
	            if (enc4 != 64) {
	                output = output + String.fromCharCode(chr3);
	            }

	        }

	        output = Base64._utf8_decode(output);

	        return output;

	    },

	    // private method for UTF-8 encoding
	    _utf8_encode : function (string) {
	        string = string.replace(/\r\n/g,"\n");
	        var utftext = "";

	        for (var n = 0; n < string.length; n++) {

	            var c = string.charCodeAt(n);

	            if (c < 128) {
	                utftext += String.fromCharCode(c);
	            }
	            else if((c > 127) && (c < 2048)) {
	                utftext += String.fromCharCode((c >> 6) | 192);
	                utftext += String.fromCharCode((c & 63) | 128);
	            }
	            else {
	                utftext += String.fromCharCode((c >> 12) | 224);
	                utftext += String.fromCharCode(((c >> 6) & 63) | 128);
	                utftext += String.fromCharCode((c & 63) | 128);
	            }

	        }

	        return utftext;
	    },

	    // private method for UTF-8 decoding
	    _utf8_decode : function (utftext) {
	        var string = "";
	        var i = 0;
	        var c = c1 = c2 = 0;

	        while ( i < utftext.length ) {

	            c = utftext.charCodeAt(i);

	            if (c < 128) {
	                string += String.fromCharCode(c);
	                i++;
	            }
	            else if((c > 191) && (c < 224)) {
	                c2 = utftext.charCodeAt(i+1);
	                string += String.fromCharCode(((c & 31) << 6) | (c2 & 63));
	                i += 2;
	            }
	            else {
	                c2 = utftext.charCodeAt(i+1);
	                c3 = utftext.charCodeAt(i+2);
	                string += String.fromCharCode(((c & 15) << 12) | ((c2 & 63) << 6) | (c3 & 63));
	                i += 3;
	            }

	        }

	        return string;
	    }

	}
	
	 	eventData = parent.window.eventData;

		myuid = eventData.uid;
		
		var firstimage = false;
		
		if (myuid.indexOf('-i') != -1) {
			var temp = myuid.split('-i')[1];
			var firstimage = temp.split('@')[0];
			firstimage = Base64.decode(firstimage);
		}
				
		var ev_node_id = myuid.split('-').reverse()[0];
		
		var myhost = myuid.split('@').reverse()[0].split('-')[0];
	
		myJSONtext = eventData.event_data;

		myJSONtext = myJSONtext.replace(/qqq/g, "\\\"")

		var my_event = eval('(' + myJSONtext + ')');
		

		
		var my_time = my_event.event_start;
		var my_time_end = my_event.event_end;
		
		var my_date = my_event.start_unixtime;
		var my_date_end = my_event.end_unixtime;
		
		<?
		
		$cal_name = $_GET['cal_name'];
		$cal_path = $_SERVER['DOCUMENT_ROOT'].'/extension/phpicalendar/calendars/'.$cal_name .'.data';

		if ($cal_path) 
			$calname = file_get_contents($cal_path);

		if ($calname) echo "my_event.calname = '$calname'";
		
		?>
		
		function intodate(i) {
			hr = (i.charAt(0) == "0") ? i.charAt(1) : i.charAt(0) + i.charAt(1);
			min = i.charAt(2) + i.charAt(3);

			hr = parseInt(hr);

			am = 'AM';
			if (hr >= 12) {
				am = 'PM';
			}
			if (hr > 12) {
				hr = hr - 12;
			}
			myout = hr + ":" + min + " " + am;
			return myout;
		}
		
		function urldecode( str ) {
		    // http://kevin.vanzonneveld.net
		    // +   original by: Philip Peterson
		    // +   improved by: Kevin van Zonneveld (http://kevin.vanzonneveld.net)
		    // +      input by: AJ
		    // +   improved by: Kevin van Zonneveld (http://kevin.vanzonneveld.net)
		    // %          note: info on what encoding functions to use from: http://xkr.us/articles/javascript/encode-compare/
		    // *     example 1: urldecode('Kevin+van+Zonneveld%21');
		    // *     returns 1: 'Kevin van Zonneveld!'
		    // *     example 2: urldecode('http%3A%2F%2Fkevin.vanzonneveld.net%2F');
		    // *     returns 2: 'http://kevin.vanzonneveld.net/'
		    // *     example 3: urldecode('http%3A%2F%2Fwww.google.nl%2Fsearch%3Fq%3Dphp.js%26ie%3Dutf-8%26oe%3Dutf-8%26aq%3Dt%26rls%3Dcom.ubuntu%3Aen-US%3Aunofficial%26client%3Dfirefox-a');
		    // *     returns 3: 'http://www.google.nl/search?q=php.js&ie=utf-8&oe=utf-8&aq=t&rls=com.ubuntu:en-US:unofficial&client=firefox-a'

		    var histogram = {}, histogram_r = {}, code = 0, str_tmp = [];
		    var ret = str.toString();

		    var replacer = function(search, replace, str) {
		        var tmp_arr = [];
		        tmp_arr = str.split(search);
		        return tmp_arr.join(replace);
		    };

		    // The histogram is identical to the one in urlencode.
		    histogram['!']   = '%21';
		    histogram['%20'] = '+';

		    for (replace in histogram) {
		        search = histogram[replace]; // Switch order when decoding
		        ret = replacer(search, replace, ret) // Custom replace. No regexing   
		    }

		    // End with decodeURIComponent, which most resembles PHP's encoding functions
		    ret = decodeURIComponent(ret);

		    return ret;
		}

		sd_r = String(new Date(my_date*1000).format("ddd mmm dd yyyy HH:MM:ss")).split(' ');
		ed_r = String(new Date(my_date_end*1000).format("ddd mmm dd yyyy HH:MM:ss")).split(' ');
		
		calc_event_times = intodate(sd_r[4].replace(/:00/, '').replace(/:/, '')) + ' - ' + intodate(ed_r[4].replace(/:00/, '').replace(/:/, ''));
	
		out_s = sd_r[1] + " " + sd_r[2] + ", " + intodate(sd_r[4].replace(/:00/, '').replace(/:/, ''));
	
		if ((ed_r[1] + " " + ed_r[2]) != out_s || sd_r[3] != ed_r[3]) {
			out_s += " - " + (ed_r[1] + " " + ed_r[2]);
			if (sd_r[3] != ed_r[3]) out_s += " " + ed_r[3];

			out_s += ", " + intodate(ed_r[4].replace(/:00/, '').replace(/:/, ''));
		} else {
			out_s += " - " + intodate(ed_r[4].replace(/:00/, '').replace(/:/, ''));
		}
		
		event_times = out_s
		
		event_times = event_times.replace(/, 0:00 AM/, '');
		event_times = event_times.replace(/, 0:00 PM/, '');
		event_times = event_times.replace(/, 11:59 PM/, '');
		
		if (event_times.indexOf('-')) {
			split_check = event_times.split(' - ');
			if (split_check[0] == split_check[1]) event_times = split_check[0];
		}
		

		calname_ar = my_event.calname.split("||");
		
		my_event.calname = calname_ar[0];
		if (my_event.event_text == '') my_event.event_text = 'Unnamed Event';
			
		var output = "<p>" + urldecode(my_event.event_text) + " - <span class='V9'>(<i>" + event_times + "</i>)</span></p>";

		desc_out = urldecode(my_event.description);

		if (firstimage == false) {
			output += "<p>" + desc_out + "</p><p>";
		} else {
			output += "<p><img class='event_image' src='" + firstimage + "'>" + desc_out + "</p><p>";	
		}
		
		var atts = "<table class='ev_atts_tab'>";
		if (my_event.location ) atts += "<tr><td class='label'><b>Location:</b></td><td>" + urldecode(my_event.location) + "</td></tr>";

		if (my_event.url) atts += "<tr><td class='label'><b>Visit Website:</b></td><td><a href='javascript: doit = function() {window.open(\"" + my_event.url + "\");}; doit();'>" + my_event.url + "</a></td></tr>";
		
		
		if (my_event.organizer && my_event.organizer.length) {
			var myorg = my_event.organizer[0];
			var outname = myorg.name;
			if (myorg.name == '""' && my_event.organizer[0].email && my_event.organizer[0].email.indexOf('noreply') == -1) {
				myorg.name = myorg.email.split('MAILTO:')[1];
				atts += "<tr><td class='label'><b>Email:</b></td><td>" + trim(myorg.name, '"') + "</td></tr>";
			} else if (my_event.organizer[0].email && my_event.organizer[0].email.indexOf('noreply') == -1) {
				atts += "<tr><td class='label'><b>Contact:</b></td><td>" + trim(myorg.name, '"') + "</td></tr>";
				atts += "<tr><td class='label'><b>Email:</b></td><td><a href='" + myorg.email + "'>" + myorg.email.split('MAILTO:')[1] + "</a></td></tr>";
			}
			
		}

		
		atts += '<tr><td colspan=2><p><b><a href="#" onclick = "document.getElementById(\'tipafriend\').style.display=\'block\'; return false;">Send to a friend</a></b></p></td></tr>';
				
		atts += "</table>";
		
		output += atts;
		


//		if (my_event.location ) output += "<b>Location</b>: " + my_event.location + "<br /></p>";
		
	</script>
</head>
<body>
<center>
	<table border="0" width="430" cellspacing="0" cellpadding="0" class="calborder">
		<tr>
			<td align="center" class="sideback"><div style="height: 17px; margin-top: 3px;" class="G10BOLD"><script>document.write(my_event.calname);</script></div></td>
		</tr>
		<tr>
			<td align="left" class="V12">
				<div style="margin-left: 10px; margin-bottom:10px;">
					
					<script>
					
					document.write(output);
					
					</script>
					
					<form target = "_parent" action="/content/tipafriend/" method="post" id="tipafriend">

					<div class="block">
					<label>Your name</label><div class="labelbreak"></div>
					<input class="box" type="text" size="40" name="YourName" value="" />
					</div>

					<div class="block">
					<label>Your email address</label><div class="labelbreak"></div>
					<input class="box" type="text" size="40" name="YourEmail" value="" />
					</div>

					<div class="block">
					<label>Recipient's email address</label><div class="labelbreak"></div>
					<input class="box" type="text" size="40" name="ReceiversEmail" value="" />
					</div>

					<div class="block">
					<label>Comment</label><div class="labelbreak"></div>
					<textarea class="box" cols="40" rows="10" name="Comment"></textarea>
					</div>

					<div class="buttonblock">
					<input class="button" type="submit" name="SendButton" value="Send"/>
					<input class="button" type="submit" name="CancelButton" value="Cancel"/>
					</div>

					<script>document.write('<input type="hidden" name="NodeID" value="' +  ev_node_id  + '" />')</script>

					</form>
				</div>
			</td>
		</tr>
	</table>
</center>
</body>
</html>
