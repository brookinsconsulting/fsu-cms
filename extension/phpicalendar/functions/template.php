<?php

//print_r($master_array);

class Page {
	var $page;
	var $majors;
	function draw_subscribe($template_p) {
		global $template, $getdate, $cal, $ALL_CALENDARS_COMBINED, $subscribe_path, $download_filename;
		if ($cal != $ALL_CALENDARS_COMBINED && $subscribe_path != '' && $download_filename != '') {
			$this->page 	= str_replace('{SUBSCRIBE_PATH}', $subscribe_path, $this->page);
			$this->page 	= str_replace('{DOWNLOAD_FILENAME}', $download_filename, $this->page);
		} else {
			$this->page = preg_replace('!<\!-- switch display_download on -->(.*)<\!-- switch display_download off -->!is', '', $this->page);
		}
	}
	
	function num_abbrev_str($n)
	{

	 $date_r = explode(' ', $n);
	 $saveme = '';
	 if (count($date_r) > 1) {
		$saveme = $date_r[0] . " ";
		$n = (int)$date_r[1];
		$savemetoo = " " . $date_r[2];
	 } else {
	 	$n = (int)$date_r[0];
	 }
	 $dates_ar = array("th", "st", "nd", "rd", "th", "th", "th", "th", "th", "th");
	 $addon  = ($n == 11)? "th" : $dates_ar[$n%10];
	 return $saveme . $n . $addon . $savemetoo;
	}
	
	function draw_admin() {
		global $template, $getdate, $cal, $master_array, $is_loged_in;
		
		if ($is_loged_in == TRUE) { 
			// Print Calendar Checkboxes
			$COLUMNS_TO_PRINT = 3;
			$column = 1;
			$filelist = availableCalendars('', '', '', true);
			foreach ($filelist as $file) {
				if ($column > $COLUMNS_TO_PRINT) {
					$delete_table 	.= '</tr>';
					$column 	= 1;
				}
				if ($column == 1) {
					$delete_table .= '<tr>';
				}
				
				$cal_filename_tmp = getCalendarName($file);
				$cal_tmp = urlencode($file);
				$cal_displayname_tmp = str_replace("32", " ", $cal_filename_tmp);
				
				$delete_table .= '<td align="left"><input name="delete_calendar[]" value="'.$cal_tmp.'" type="checkbox" />'.$cal_displayname_tmp.'</td>';
				
				$column++;
			}
			// Print remaining empty columns if necessary
			$number_of_columns = count($filelist);
			while (gettype($number_of_columns/$COLUMNS_TO_PRINT) != "integer") {
				$delete_table .= '<td>&nbsp;</td>';
				$number_of_columns++;
			}
			
			$this->page 	= str_replace('{DELETE_TABLE}', $delete_table, $this->page);
		} else {
			$this->page 	= preg_replace('!<\!-- switch logged_in on -->(.*)<\!-- switch logged_in off -->!is', '', $this->page);
			$this->page 	= preg_replace('!<\!-- switch logged_in2 on -->(.*)<\!-- switch logged_in2 off -->!is', '', $this->page);
		}
	
	}
	
	function draw_print($template_p) {
		global $template, $getdate, $cal, $master_array, $daysofweek_lang, $week_start_day, $printview, $dateFormat_day, $timeFormat, $week_start, $week_end, $lang;
		preg_match("!<\!-- loop events on -->(.*)<\!-- loop events off -->!is", $this->page, $match1);
		preg_match("!<\!-- switch some_events on -->(.*)<\!-- loop events on -->!is", $this->page, $match3);
		$loop_event		= trim($match1[1]);
		$loop_day 		= trim($match3[1]);
		$parse_month 	= date ("Ym", strtotime($getdate));
		$parse_year 	= date ("Y", strtotime($getdate));

		$seen_events = array();
		foreach($master_array as $key => $val) {
			preg_match ('/([0-9]{6})([0-9]{2})/', $key, $regs);
			if ((($regs[1] == $parse_month) && ($printview == 'month')) || (($key == $getdate) && ($printview == 'day')) || ((($key >= $week_start) && ($key <= $week_end)) && ($printview == 'week')) || ((substr($regs[1],0,4) == $parse_year) && ($printview == 'year'))) {
				$events_week++;
				$dayofmonth = strtotime ($key);
				$dayofmonth = localizeDate ($dateFormat_day, $dayofmonth);
				$events_tmp = $loop_event;
				$day_tmp	= $loop_day;
				$day_events = 0;
				// Pull out each day
				foreach ($val as $new_val) {
					foreach ($new_val as $new_key2 => $new_val2) {
						if (isset($seen_events["$new_key2"])){
							$new_val2['event_text'] .= " second instance of ".$new_key2;
							continue;
						}
						$seen_events["$new_key2"] = 1;
						$day_events++;
					if ($new_val2['event_text']) {	
						$event_text 	= stripslashes(urldecode($new_val2['event_text']));
						$location 	= stripslashes(urldecode($new_val2['location']));
						$description 	= stripslashes(urldecode($new_val2['description']));
						$event_start 	= $new_val2['event_start'];
						$event_end 		= $new_val2['event_end'];
						if (isset($new_val2['display_end'])) $event_end = $new_val2['display_end'];
							if (!$new_val2['event_start']) { 
								$event_start = $lang['l_all_day'];
								$event_start2 = '';
								$event_end = '';
							} else {
								$event_start 	= date ($timeFormat, strtotime ($event_start));
								$event_end 		= date ($timeFormat, strtotime ($event_end));
								$event_start 	= $event_start .' - '.$event_end;
								if (date("Ymd", $new_val2['start_unixtime']) != date("Ymd", $new_val2['end_unixtime'])) $event_start .= " ".localizeDate($dateFormat_day, $new_val2['end_unixtime']);
							}
						}
						
						if ($description == '') {
							$events_tmp = preg_replace('!<\!-- switch description_events on -->(.*)<\!-- switch description_events off -->!is', '', $events_tmp);
						}

						if ($location == '') {
							$events_tmp = preg_replace('!<\!-- switch location_events on -->(.*)<\!-- switch location_events off -->!is', '', $events_tmp);
						}
						
						$search		= array('{EVENT_START}', '{EVENT_TEXT}', '{DESCRIPTION}', '{LOCATION}');
						$replace	= array($event_start, $event_text, $description, $location);
						$events_tmp = str_replace($search, $replace, $events_tmp);
						$some_events .= $events_tmp;
						$events_tmp	= $loop_event;
					}
				}
				if ($day_events == 0) continue;
				$day_tmp  = str_replace('{DAYOFMONTH}', $dayofmonth, $day_tmp);
				$final   .= $day_tmp.$some_events;
				unset ($day_tmp, $some_events);
			}
		}
		
		if ($events_week < 1) {
			$this->page = preg_replace('!<\!-- switch some_events on -->(.*)<\!-- switch some_events off -->!is', '', $this->page);
		} else {
			$this->page = preg_replace('!<\!-- switch some_events on -->(.*)<\!-- switch some_events off -->!is', $final, $this->page);
			$this->page = preg_replace('!<\!-- switch no_events on -->(.*)<\!-- switch no_events off -->!is', '', $this->page);
		}
	}	
	
	function draw_search($template_p) {
		global $template, $getdate, $cal, $the_arr, $daysofweek_lang, $week_start_day, $printview, $dateFormat_day, $timeFormat, $week_start, $week_end, $lang, $unique_colors, $coldata;

		preg_match("!<\!-- switch results on -->(.*)<\!-- switch results off -->!is", $this->page, $match1);
		preg_match("!<\!-- switch recur on -->(.*)<\!-- loop recur off -->!is", $this->page, $match2);
		preg_match("!<\!-- switch exceptions on -->(.*)<\!-- switch exceptions off -->!is", $this->page, $match3);
		$loop_event		= trim($match1[1]);
		$loop_recur 	= trim($match2[1]);
		$loop_except 	= trim($match3[1]);
		$parse_month 	= date ("Ym", strtotime($getdate));
		
		if (isset($the_arr)){
		
			// Pull out each event
			$done_r = array();
			
			foreach($the_arr as $key => $val) {
				
				
				if (!array_key_exists('event_text',$val)) continue;

				if ($val[start_unixtime] < time()) continue;

				$this_key = crc32($val[start_unixtime] . $val[event_text]);
				if (!in_array($this_key, $done_r)) {
				$done_r[] = $this_key;
				$events_found++;
				$dayofmonth = strtotime($val['date']);
				$dayofmonth = localizeDate ('%A, %B %e %Y', $dayofmonth);
				$events_tmp = $loop_event;
				$recur_tmp	= $loop_recur;

				if (array_key_exists('event_text',$val) && $val['event_text']) {

					
					$location 	= stripslashes(urldecode($val['location']));
					$event_start 	= $val['event_start'];
					$event_end 		= $val['event_end'];
					$time_link = $val['event_start'];
					$daylink 		= $val['date'];
					if (isset($val['display_end'])) $event_end = $val['display_end'];
						if (!$val['event_start']) { 
							$time_link = "-1";
							$event_start = $lang['l_all_day'];
							$event_start2 = '';
							$event_end = '';
						} else {
								$event_start    = date ($timeFormat, strtotime ($event_start));
								$event_end      = date ($timeFormat, strtotime ($event_end));
								$event_start    = $event_start .' - '.$event_end;
						}							
					}

					$event_text = openeventfull($daylink, $time_link, $key, $val);

					$link_html = str_replace(urldecode($val['event_text'])."</a>", "", $event_text);
					$description 	= html_shorten(stripslashes(urldecode($val['description'])), 50, "...".$link_html."more</a>" );
					$description 	= preg_replace('/(<br\s?\/>)*<p>(<br\s?\/>)*/', '<p>', $description );
				
					if ($description == '') {
						$events_tmp = preg_replace('!<\!-- switch description on -->(.*)<\!-- switch description off -->!is', '', $events_tmp);
					}
					if (!isset($val['exceptions'])) {
						$events_tmp = preg_replace('!<\!-- switch exceptions on -->(.*)<\!-- switch exceptions off -->!is', '', $events_tmp);
					}else{
						$some_exceptions = "";
						foreach ($val['exceptions'] as $except_val){
							$except_tmp	= $loop_except;
						
							$except_date = strtotime($except_val['date']);
							$except_date = localizeDate ('%A, %B %e %Y', $except_date);
							$except_tmp = str_replace('{DAYOFMONTH}', $except_date, $except_tmp);

							$except_event_start    	= date ($timeFormat, strtotime ($except_val['event_start']));
							$except_event_end    	= date ($timeFormat, strtotime ($except_val['event_end']));
							$except_event_start    	= $except_event_start .' - '.$except_event_end;

							$except_tmp = str_replace('{EVENT_START}', $except_event_start, $except_tmp);

							$except_event_text 	= stripslashes(urldecode($except_val['event_text']));
							$except_tmp = str_replace('{EVENT_TEXT}', $except_event_text, $except_tmp);

							#is there a recur in the exception?
							if (!$except_val['recur']) {
								$except_tmp = preg_replace('!<\!-- switch except_recur on -->(.*)<\!-- switch except_recur off -->!is', '', $except_tmp);
							}else{
								$except_tmp = str_replace('{EXCEPT_RECUR}', $except_val['recur'], $except_tmp);
							}
							#is there a description in the exception?
							if (!$except_val['description']) {
								$except_tmp = preg_replace('!<\!-- switch except_description on -->(.*)<\!-- switch except_description off -->!is', '', $except_tmp);
							}else{
								$except_description = stripslashes(urldecode($except_val['description']));
								$except_tmp = str_replace('{EXCEPT_DESCRIPTION}', $except_description, $except_tmp);
							}
							$some_exceptions .= $except_tmp;

						}
						$events_tmp = preg_replace('!<\!-- switch exceptions on -->(.*)<\!-- switch exceptions off -->!is', $some_exceptions,$events_tmp );

	
					}
					if (!preg_replace("/[A-Za-z 0]/", "", $val['recur'])) {
						$events_tmp = preg_replace('!<\!-- switch recur on -->(.*)<\!-- switch recur off -->!is', '', $events_tmp);
					}else{
						$events_tmp = str_replace('{RECUR}', $val['recur'], $events_tmp);
					}
					
					$event_calno 	= $val['categories'] ? array_search($val['categories'], array_values($coldata)) : $val['calnumber'];
					$event_text = caldot($template, $event_calno).$event_text;

					$extra_info_tmp_array = explode('-', $key);
					$extra_info = json_decode(base64_decode($extra_info_tmp_array[1]));
					$type = $extra_info->type;
					$regions = $extra_info->regions;
					$state = $extra_info->state;
					$scope = $extra_info->audience;
					$format = $extra_info->format;

					$search		= array('{EVENT_START}', '{EVENT_TEXT}', '{DESCRIPTION}','{LOCATION}', '{TYPE}', '{REGIONS}', '{STATE}', '{SCOPE}', '{FORMAT}');
					$replace	= array($event_start, $event_text, $description, $location, $type, $regions, $state, $scope, $format);

					$events_tmp = str_replace($search, $replace, $events_tmp);
					$some_events .= $events_tmp;
					$events_tmp	= $loop_event;
				

					$event_calno	= (($event_calno - 1) % $unique_colors) + 1;
					$dayofmonth = $dayofmonth;
				
			
					$some_events  = str_replace('{KEY}', $val['date'], $some_events);
					$some_events  = str_replace('{DAYOFMONTH}', $dayofmonth, $some_events);

					$pre = '<div class="upcoming cal_'.$event_calno.'">';
					$final   .= $pre.$day_tmp.$some_events."</div>";
					unset ($day_tmp, $some_events);
				}
			}
			foreach($the_arr as $key => $val) {
				
				
				if (!array_key_exists('event_text',$val)) continue;

				if ($val[start_unixtime] > time()) continue;

				$this_key = crc32($val[start_unixtime] . $val[event_text]);
				if (!in_array($this_key, $done_r)) {
				$done_r[] = $this_key;
				$events_found++;
				$dayofmonth = strtotime($val['date']);
				$dayofmonth = localizeDate ('%A, %B %e %Y', $dayofmonth);
				$events_tmp = $loop_event;
				$recur_tmp	= $loop_recur;

				if (array_key_exists('event_text',$val) && $val['event_text']) {

					
					$location 	= stripslashes(urldecode($val['location']));
					$event_start 	= $val['event_start'];
					$event_end 		= $val['event_end'];
					$time_link = $val['event_start'];
					$daylink 		= $val['date'];
					if (isset($val['display_end'])) $event_end = $val['display_end'];
						if (!$val['event_start']) { 
							$time_link = "-1";
							$event_start = $lang['l_all_day'];
							$event_start2 = '';
							$event_end = '';
						} else {
								$event_start    = date ($timeFormat, strtotime ($event_start));
								$event_end      = date ($timeFormat, strtotime ($event_end));
								$event_start    = $event_start .' - '.$event_end;
						}							
					}

					$event_text = openeventfull($daylink, $time_link, $key, $val);

					$link_html = str_replace(urldecode($val['event_text'])."</a>", "", $event_text);
					$description 	= html_shorten(stripslashes(urldecode($val['description'])), 50, "...".$link_html."more</a>" );
					$description 	= preg_replace('/(<br\s?\/>)*<p>(<br\s?\/>)*/', '<p>', $description );
				
					if ($description == '') {
						$events_tmp = preg_replace('!<\!-- switch description on -->(.*)<\!-- switch description off -->!is', '', $events_tmp);
					}
					if (!isset($val['exceptions'])) {
						$events_tmp = preg_replace('!<\!-- switch exceptions on -->(.*)<\!-- switch exceptions off -->!is', '', $events_tmp);
					}else{
						$some_exceptions = "";
						foreach ($val['exceptions'] as $except_val){
							$except_tmp	= $loop_except;
						
							$except_date = strtotime($except_val['date']);
							$except_date = localizeDate ('%A, %B %e %Y', $except_date);
							$except_tmp = str_replace('{DAYOFMONTH}', $except_date, $except_tmp);

							$except_event_start    	= date ($timeFormat, strtotime ($except_val['event_start']));
							$except_event_end    	= date ($timeFormat, strtotime ($except_val['event_end']));
							$except_event_start    	= $except_event_start .' - '.$except_event_end;

							$except_tmp = str_replace('{EVENT_START}', $except_event_start, $except_tmp);

							$except_event_text 	= stripslashes(urldecode($except_val['event_text']));
							$except_tmp = str_replace('{EVENT_TEXT}', $except_event_text, $except_tmp);

							#is there a recur in the exception?
							if (!$except_val['recur']) {
								$except_tmp = preg_replace('!<\!-- switch except_recur on -->(.*)<\!-- switch except_recur off -->!is', '', $except_tmp);
							}else{
								$except_tmp = str_replace('{EXCEPT_RECUR}', $except_val['recur'], $except_tmp);
							}
							#is there a description in the exception?
							if (!$except_val['description']) {
								$except_tmp = preg_replace('!<\!-- switch except_description on -->(.*)<\!-- switch except_description off -->!is', '', $except_tmp);
							}else{
								$except_description = stripslashes(urldecode($except_val['description']));
								$except_tmp = str_replace('{EXCEPT_DESCRIPTION}', $except_description, $except_tmp);
							}
							$some_exceptions .= $except_tmp;

						}
						$events_tmp = preg_replace('!<\!-- switch exceptions on -->(.*)<\!-- switch exceptions off -->!is', $some_exceptions,$events_tmp );

	
					}
					if (!preg_replace("/[A-Za-z 0]/", "", $val['recur'])) {
						$events_tmp = preg_replace('!<\!-- switch recur on -->(.*)<\!-- switch recur off -->!is', '', $events_tmp);
					}else{
						$readable_recur = preg_replace('/1 (week|day)/', '$1', $val['recur']);
						$readable_recur = preg_replace('/(week|day) for a count of ([0-9]*)/', '$1 for $2 $1s', $readable_recur);
						$events_tmp = str_replace('{RECUR}', $readable_recur, $events_tmp);
					//	$events_tmp = str_replace('{RECUR}', $val['recur'], $events_tmp);
					}
					
					$event_calno 	= $val['categories'] ? array_search($val['categories'], array_values($coldata)) : $val['calnumber'];
					$event_text = caldot($template, $event_calno).$event_text;
				

					$extra_info_tmp_array = explode('-', $key);
					$extra_info = json_decode(base64_decode($extra_info_tmp_array[1]));
					$type = $extra_info->type;
					$regions = $extra_info->regions;
					$state = $extra_info->state;
					$scope = $extra_info->audience;
					$format = $extra_info->format;

					$search		= array('{EVENT_START}', '{EVENT_TEXT}', '{DESCRIPTION}','{LOCATION}', '{TYPE}', '{REGIONS}', '{STATE}', '{SCOPE}', '{FORMAT}');
					$replace	= array($event_start, $event_text, $description, $location, $type, $regions, $state, $scope, $format);

					$events_tmp = str_replace($search, $replace, $events_tmp);
					$some_events .= $events_tmp;
					$events_tmp	= $loop_event;
				

					$event_calno	= (($event_calno - 1) % $unique_colors) + 1;
					$dayofmonth = $dayofmonth;
				
			
					$some_events  = str_replace('{KEY}', $val['date'], $some_events);
					$some_events  = str_replace('{DAYOFMONTH}', $dayofmonth, $some_events);

					//$pre = '<div class="upcoming cal_'.$event_calno.'">';
					//$final   .= $pre.$day_tmp.$some_events."</div>";
					$final   .= $day_tmp.$some_events; // datatables is now happy
					unset ($day_tmp, $some_events);
				}
			}
		}		
		if ($events_found < 1) {
			$this->page = preg_replace('!<\!-- switch results on -->(.*)<\!-- switch results off -->!is', '', $this->page);
		} else {
			$this->page = preg_replace('!<\!-- switch results on -->(.*)<\!-- switch results off -->!is', $final, $this->page);
			$this->page = preg_replace('!<\!-- switch no_results on -->(.*)<\!-- switch no_results off -->!is', '', $this->page);
			#echo "<hr>this->page: $this->page<br><hr>";

		}
	}#end draw_search
	
	function draw_week($template_p) {
		global $coldata, $majors_config, $unique_colors, $start_week_time, $template, $getdate, $cal, $master_array, $daysofweek_lang, $week_start_day, $dateFormat_week_list, $current_view, $day_array, $timeFormat, $gridLength, $timeFormat_small, $day_start, $week_length;
		
		// Figure out colspans
		$dayborder 	= 0;
		$thisdate  	= $start_week_time;
		$swt	   	= $start_week_time;
		$temp_master = array();
		for ($i=0;$i<7;$i++) {
			$thisday = date("Ymd", $thisdate);
			$nbrGridCols[$thisday] = 1;
			if (isset($master_array[$thisday])) {
				foreach($master_array[($thisday)] as $ovlKey => $ovlValue) {
					if ($ovlKey != "-1") {
						foreach($ovlValue as $ovl2Value) {
							$me = $ovl2Value;
							$mekey = $me['event_text'] . $me['start_unixtime'];
							if (($me['event_length'] >= $majors_config['time_limit'] || ($me['recur']['FREQ'] == 'daily' and $me['recur']['COUNT'] >=$majors_config['day_limit'] ))) {
								$majors = $this->majors;
								if (! $majors[$mekey]) {
									$majors[$mekey] = $me;
									$this->majors = $majors;
								}
							} else {
								$tmp  = $temp_master[($thisday)][$ovlKey];
								if (is_array($tmp)) {
									$tmp[] = $ovl2Value;
								} else {
									$tmp = array();
									$tmp[] = $ovl2Value;
								}

								
								$temp_master[($thisday)][$ovlKey] = $tmp;
								$nbrGridCols[($thisday)] = kgv($nbrGridCols[($thisday)], ($ovl2Value["event_overlap"] + 1));
							}
						}
					}
				} 
			}
			$thisdate = ($thisdate + (25 * 60 * 60));
		}

		for ($i=0; $i<$week_length; $i++) {
			$thisdate 			= date ('Ymd', $start_week_time); 
			$weekarray[$i] 		= $thisdate;
			$start_week_time 	= strtotime('+1 day', $start_week_time);
		}
				
		// Replaces the allday events
		preg_match("!<\!-- loop allday on -->(.*)<\!-- loop allday off -->!is", $this->page, $match1);
		preg_match("!<\!-- loop alldaysofweek on -->(.*)<\!-- loop allday on -->!is", $this->page, $match2);
		preg_match("!<\!-- loop allday off -->(.*)<\!-- loop alldaysofweek off -->!is", $this->page, $match3);
		$loop_ad 	= trim($match1[1]);
		$loop_begin = trim($match2[1]);
		$loop_end 	= trim($match3[1]);
		foreach ($weekarray as $get_date) {
			$replace 	= $loop_begin;
			$colspan	= 'colspan="'.$nbrGridCols[$get_date].'"';
			$replace 	= str_replace('{COLSPAN}', $colspan, $replace);
			if (is_array($temp_master[$get_date]['-1']) && !empty($temp_master[$get_date]['-1']) ) {
				foreach ($temp_master[$get_date]['-1'] as $uid => $allday) {
					$event_calno  	= $allday['calnumber'];
					$event_calno	= (($event_calno - 1) % $unique_colors) + 1;
 					$event 			= openevent($get_date, -1, $uid, $allday, 1, 11, 'psf');
					$loop_tmp 		= str_replace('{ALLDAY}', $event, $loop_ad);
					$loop_tmp 		= str_replace('{CALNO}', $event_calno, $loop_tmp);
					$replace		.= $loop_tmp;
				}
			}
			$replace .= $loop_end;
			$weekreplace .= $replace;
		}
		$this->page = preg_replace('!<\!-- loop alldaysofweek on -->(.*)<\!-- loop alldaysofweek off -->!is', $weekreplace, $this->page);
		
		// Replaces the daysofweek
		preg_match("!<\!-- loop daysofweek on -->(.*)<\!-- loop daysofweek off -->!is", $this->page, $match1);
		$loop_dof = trim($match1[1]);
		$start_wt		 	= strtotime(dateOfWeek($getdate, $week_start_day));
		$start_day 			= strtotime($week_start_day);
		for ($i=0; $i<$week_length; $i++) {
			$day_num 		= date("w", $start_day);
			$daylink		= date('Ymd', $start_wt);
			if ($current_view == 'day') {
				$weekday 		= $daysofweek_lang[$day_num];
			} else {
				$weekday = localizeDate($dateFormat_week_list, strtotime($daylink));
			}	
			$mytoday = date('Ymd');

			if ($daylink == $mytoday ) {
				$row1 = 'rowToday';
				$row2 = 'rowOn';
				$row3 = 'rowToday';
			} else {
				$row1 = 'rowOff';
				$row2 = 'rowOn';
				$row3 = 'rowOff';
			}
			$start_day 		= strtotime("+1 day", $start_day);
			$start_wt 		= strtotime("+1 day", $start_wt);
			$colspan		= 'colspan="'.$nbrGridCols[$daylink].'"';
			$search			= array('{DAY}', '{DAYLINK}', '{ROW1}', '{ROW2}', '{ROW3}', '{COLSPAN}');
			$replace		= array($weekday, $daylink, $row1, $row2, $row3, $colspan);
			$loop_tmp 		= str_replace($search, $replace, $loop_dof);
			$weekday_loop  .= $loop_tmp;
		}
		$this->page = preg_replace('!<\!-- loop daysofweek on -->(.*)<\!-- loop daysofweek off -->!is', $weekday_loop, $this->page);
		
		// Build the body
		preg_match("!<\!-- loop row on -->(.*)<\!-- loop row off -->!is", $this->page, $match2);
		preg_match("!<\!-- loop event on -->(.*)<\!-- loop event off -->!is", $this->page, $match3);
		$loop_hours = trim($match2[1]);
		$loop_event = trim($match3[1]);

		$event_length = array ();
		$border = 0;
		preg_match ('/([0-9]{4})([0-9]{2})([0-9]{2})/', $getdate, $day_array2);
		$this_day = $day_array2[3]; 
		$this_month = $day_array2[2];
		$this_year = $day_array2[1];
		$thisdate = $swt;
		for ($i=0;$i<7;$i++) {
			$thisday = date("Ymd", $thisdate);
			$event_length[$thisday] = array ();
			$thisdate = ($thisdate + (25 * 60 * 60));
		}
		foreach ($day_array as $key) {
			$cal_time = $key;	
			preg_match('/([0-9]{2})([0-9]{2})/', $key, $regs_tmp);
			$key = mktime($regs_tmp[1],$regs_tmp[2],0,$this_month,$this_day,$this_year);
			$key = date ($timeFormat, $key);
												

			if (ereg("([0-9]{1,2}):00", $key)) {
				$weekdisplay .= '<tr>';
				$weekdisplay .= '<td rowspan="' . (60 / $gridLength) . '" align="center" valign="top" class="timeborder">'.$key.'</td>';
				$weekdisplay .= '<td bgcolor="#a1a5a9" width="1" height="' . $gridLength . '"></td>';
			} elseif ($cal_time == $day_start) {
				$size_tmp = 60 - (int)substr($cal_time,2,2);
				$weekdisplay .= '<tr>';
				$weekdisplay .= '<td rowspan="' . ($size_tmp / $gridLength) . '" align="center" valign="top" class="timeborder">'.$key.'</td>';
				$weekdisplay .= '<td bgcolor="#a1a5a9" width="1" height="' . $gridLength . '"></td>';
			} else {
				$weekdisplay .= '<tr>';
				$weekdisplay .= '<td bgcolor="#a1a5a9" width="1" height="' . $gridLength . '"></td>';
			}
			
			// initialize $thisdate again
			$thisdate = $swt;
			
			// loop this part 7 times, one for each day
			for ($week_loop=0; $week_loop<$week_length; $week_loop++) {
				$thisday = date("Ymd", $thisdate);
				$dayborder = 0;
				unset($this_time_arr);

				if ($day_start == $cal_time && isset($temp_master[$thisday]) && is_array($temp_master[$thisday])) {
					foreach($temp_master[$thisday] as $time_key => $time_arr) {
						if ((int)$time_key <= (int)$cal_time) {
							if (is_array($time_arr) && $time_key != '-1') {
								foreach($time_arr as $uid => $event_tmp) {
									if ((int)$event_tmp['event_end'] > (int)$cal_time) {
										$this_time_arr[$uid] = $event_tmp;
									}
								}
							}
						} else {
							break;
						}
					}
				} else {
					if (isset($temp_master[$thisday][$cal_time]) && sizeof($temp_master[$thisday][$cal_time]) > 0) {
						$this_time_arr = $temp_master[$thisday][$cal_time];
					}
				}
				
				// check for eventstart 
				if (isset($this_time_arr) && sizeof($this_time_arr) > 0) {
					#print "<pre>";
					#print_r ($this_time_arr);
					#print "</pre>";

					foreach ($this_time_arr as $eventKey => $loopevent) {
						$drawEvent = drawEventTimes ($cal_time, $loopevent["event_end"]);
						$j = 0;
						while (isset($event_length[$thisday][$j])) {
							if ($event_length[$thisday][$j]["state"] == "ended") {
								$event_length[$thisday][$j] = array ("length" => ($drawEvent["draw_length"] / $gridLength), "key" => $eventKey, "overlap" => $loopevent["event_overlap"],"state" => "begin");
								break;
							}
							$j++;
						}
						if ($j == sizeof($event_length[$thisday])) {
							array_push ($event_length[$thisday], array ("length" => ($drawEvent["draw_length"] / $gridLength), "key" => $eventKey, "overlap" => $loopevent["event_overlap"],"state" => "begin"));
						}
					}
				}

				if (sizeof($event_length[$thisday]) == 0) {
					if ($dayborder == 0) {
						$class = ' class="weekborder"';
						$dayborder++;
					} else {
						$class = '';
						$dayborder = 0;
					}
					
					$drawWidth = 1;
					$colspan_width = round((11 / $nbrGridCols[$thisday]) * $drawWidth);
					$weekdisplay .= '<td width="' . $colspan_width . '%" colspan="' . $nbrGridCols[$thisday] . '" ' . $class . '>&nbsp;</td>'."\n";
					
				} else {
					$emptyWidth = $nbrGridCols[$thisday];
					// Used to "join" ended events, so the ended case below results in one colspan'd td instead of multiple tds.
					$ended_counter = 0;
					for ($i=0;$i<sizeof($event_length[$thisday]);$i++) {

						$drawWidth = $nbrGridCols[$thisday] / ($event_length[$thisday][$i]["overlap"] + 1);
						$emptyWidth = $emptyWidth - $drawWidth;
								
							switch ($event_length[$thisday][$i]["state"]) {
								case "begin":
									if ($ended_counter) {
										$weekdisplay .= '<td colspan="' . $ended_counter . '" '.$class.'>&nbsp;</td>';
										$ended_counter = 0;
									}
									$event_length[$thisday][$i]["state"] = "started";
	 								$uid = $event_length[$thisday][$i]["key"];
	 								$event_start 	= $this_time_arr[$uid]['start_unixtime'];
									$event_start 	= date ($timeFormat_small, $event_start);
	 								$event_calno  	= $this_time_arr[$uid]['calnumber'];
	 								$event_status	= strtolower($this_time_arr[$uid]['status']);
	 								$event_recur = $this_time_arr[$uid]['recur'];
									$event_calno 	= $this_time_arr[$uid]['categories'] ? array_search($this_time_arr[$uid]['categories'], array_values($coldata)) + 1 : $this_time_arr[$uid]['calnumber'];
									$event_calno = (($event_calno - 1) % $unique_colors) + 1;
									$confirmed = '';
							
									
							  		if (is_array($event_recur)) $confirmed .= '<img src="images/recurring.gif" width="9" height="9" alt="" border="0" hspace="0" vspace="0" />&nbsp;';
									if ($event_status != '') {
							  			$confirmed .= '<img src="images/'.$event_status.'.gif" width="9" height="9" alt="" border="0" hspace="0" vspace="0" />&nbsp;';
							  		}
									$colspan_width = round((11 / $nbrGridCols[$thisday]) * $drawWidth);
									//print_r($this_time_arr[$uid]['categories']);
									list($bgc, $bgcl) = calcols($event_calno);
									if ($bgc) {
										$weekdisplay .= '<td width="'.$colspan_width.'%" rowspan="' . $event_length[$thisday][$i]['length'] . '" colspan="' . $drawWidth . '" align="left" valign="top" style="background: '.$bgcl.';">'."\n";									
									} else {
										$weekdisplay .= '<td width="'.$colspan_width.'%" rowspan="' . $event_length[$thisday][$i]['length'] . '" colspan="' . $drawWidth . '" align="left" valign="top" class="eventbg2_'.$event_calno.'">'."\n";
									}
									// Start drawing the event
									$event_temp   = $loop_event;
									$event 		  = openevent($thisday, $cal_time, $uid, $this_time_arr[$uid], $week_events_lines, 25, 'ps');
									$event_temp   = str_replace('{EVENT}', $event, $event_temp);
									$event_temp   = str_replace('{EVENT_START}', $event_start, $event_temp);
									$event_temp   = str_replace('{CONFIRMED}', $confirmed, $event_temp);
									if ( $bgc ) {
										$event_temp   = str_replace('{EVENTHEAD}',"style='background: $bgc;'", $event_temp);
									} else {
										$event_temp   = str_replace('{EVENTHEAD}',"class='eventbg_$event_calno'", $event_temp);
									}
									$weekdisplay .= $event_temp;
									$weekdisplay .= '</td>';
								
									$putmein = $event_calno;
									if ($bgc) $putmein .= '" style="background-color: '.$bgc;
								
									$weekdisplay  = str_replace('{EVENT_CALNO}', $putmein, $weekdisplay);
									// End event drawing

									break;
								case "started":
									if ($ended_counter) {
										$weekdisplay .= '<td colspan="' . $ended_counter . '" '.$class.'>&nbsp;</td>';
										$ended_counter = 0;
									}
									break;
								case "ended":
									$ended_counter += $drawWidth;
									break;
							}
							$event_length[$thisday][$i]["length"]--;
							if ($event_length[$thisday][$i]["length"] == 0) {
								$event_length[$thisday][$i]["state"] = "ended";
							}
						

					}
				

					// Clean up
					$emptyWidth += $ended_counter;
					//fill empty space on the right
					if ($emptyWidth > 0) {
						$weekdisplay .= "<td colspan=\"" . $emptyWidth . "\" $class>&nbsp;</td>\n";
					}
					while (isset($event_length[$thisday][(sizeof($event_length[$thisday]) - 1)]["state"]) && $event_length[$thisday][(sizeof($event_length[$thisday]) - 1)]["state"] == "ended") {
						array_pop($event_length[$thisday]);
					}
				}
				$thisdate = ($thisdate + (25 * 60 * 60));
			}
			$weekdisplay .= "</tr>\n";
		}

		$this->page = preg_replace('!<\!-- loop row on -->(.*)<\!-- loop event off -->!is', $weekdisplay, $this->page);
	}

	function draw_day($template_p) {
		global $coldata, $majors_config, $template, $getdate, $cal, $master_array, $unique_colors, $daysofweek_lang, $week_start_day, $dateFormat_week_list, $current_view, $day_array, $timeFormat, $gridLength, $day_start;

		// Replaces the allday events
		$replace = '';
		if (is_array($master_array[$getdate]['-1'])) {
			preg_match("!<\!-- loop allday on -->(.*)<\!-- loop allday off -->!is", $this->page, $match1);
			$loop_ad = trim($match1[1]);
			foreach ($master_array[$getdate]['-1'] as $uid => $allday) {
				$event_calno  	= $allday['calnumber'];
				$event_calno	= (($event_calno - 1) % $unique_colors) + 1;
 				$event 			= openevent($getdate, -1, $uid, $allday);
				$loop_tmp 		= str_replace('{ALLDAY}', $event, $loop_ad);
				$loop_tmp 		= str_replace('{CALNO}', $event_calno, $loop_tmp);
				$replace		.= $loop_tmp;
			}
		}
		$this->page = preg_replace('!<\!-- loop allday on -->(.*)<\!-- loop allday off -->!is', $replace, $this->page);


		// Replaces the daysofweek
		preg_match("!<\!-- loop daysofweek on -->(.*)<\!-- loop daysofweek off -->!is", $this->page, $match1);
		$loop_dof = trim($match1[1]);
		$start_wt		 	= strtotime(dateOfWeek($getdate, $week_start_day));
		$start_day 			= strtotime(dateOfWeek($getdate, $week_start_day));
		for ($i=0; $i<7; $i++) {
			$day_num 		= date("w", $start_day);
			$daylink		= date('Ymd', $start_wt);
			if ($current_view == 'day') {
				$weekday 		= $daysofweek_lang[$day_num];
			} else {
				$weekday = localizeDate($dateFormat_week_list, strtotime($daylink));
			}	
			if ($daylink == $getdate) {
				$row1 = 'rowToday';
				$row2 = 'rowOn';
				$row3 = 'rowToday';
			} else {
				$row1 = 'rowOff';
				$row2 = 'rowOn';
				$row3 = 'rowOff';
			}
			$start_day 		= strtotime("+1 day", $start_day);
			$start_wt 		= strtotime("+1 day", $start_wt);
			$search			= array('{DAY}', '{DAYLINK}', '{ROW1}', '{ROW2}', '{ROW3}');
			$replace		= array($weekday, $daylink, $row1, $row2, $row3);
			$loop_tmp 		= str_replace($search, $replace, $loop_dof);
			$weekday_loop  .= $loop_tmp;
		}
		$this->page = preg_replace('!<\!-- loop daysofweek on -->(.*)<\!-- loop daysofweek off -->!is', $weekday_loop, $this->page);
		
		// Build the body
		$dayborder = 0;

		$nbrGridCols = 1;
		if (isset($master_array[($getdate)])) {
			foreach($master_array[($getdate)] as $ovlKey => $ovlValue) {
				if ($ovlKey != '-1') {
					foreach($ovlValue as $ovl2Value) {
						$nbrGridCols = kgv($nbrGridCols, ($ovl2Value['event_overlap'] + 1));
					}
				}
			} 
		}
		preg_match("!<\!-- loop row on -->(.*)<\!-- loop row off -->!is", $this->page, $match2);
		preg_match("!<\!-- loop event on -->(.*)<\!-- loop event off -->!is", $this->page, $match3);
		$loop_hours = trim($match2[1]);
		$loop_event = trim($match3[1]);

		$event_length = array ();
		$border = 0;
		preg_match('/([0-9]{4})([0-9]{2})([0-9]{2})/', $getdate, $day_array2);
		$this_day = $day_array2[3]; 
		$this_month = $day_array2[2];
		$this_year = $day_array2[1];
		
		foreach ($day_array as $key) {
			preg_match('/([0-9]{2})([0-9]{2})/', $key, $regs_tmp);
			$cal_time = $key;
			$key = mktime($regs_tmp[1],$regs_tmp[2],0,$this_month,$this_day,$this_year);
			$key = date ($timeFormat, $key);

			unset($this_time_arr);
			
			// add events that overlap $day_start instead of cutting them out completely
			if (($day_start == $cal_time) && (isset($master_array[$getdate]))) {
				foreach($master_array[$getdate] as $time_key => $time_arr) {
					if ((int)$time_key <= (int)$cal_time) {
						if (is_array($time_arr) && $time_key != '-1') {
							foreach($time_arr as $uid => $event_tmp) {
								if ((int)$event_tmp['event_end'] > (int)$cal_time) {
									$this_time_arr[$uid] = $event_tmp;
								}
							}
						}
					} else {
						break;
					}
				}
			} else {
				// add events that overlap the start time
				if (isset($master_array[$getdate][$cal_time]) && sizeof($master_array[$getdate][$cal_time]) > 0) {
					$this_time_arr = $master_array[$getdate][$cal_time];
				}
			}
			

			// check for eventstart 
			if (isset($this_time_arr) && sizeof($this_time_arr) > 0) {
				foreach ($this_time_arr as $eventKey => $loopevent) {

					$mekey = $loopevent['event_text'] . $loopevent['start_unixtime'];
					
					if (!($loopevent['event_length'] >= $majors_config['time_limit'] || ($loopevent['recur']['FREQ'] == 'daily' and $loopevent['recur']['COUNT'] >=$majors_config['day_limit'] ))) {
						
						$drawEvent = drawEventTimes ($cal_time, $loopevent['event_end']);
						$j = 0;
						while (isset($event_length[$j])) {
							if ($event_length[$j]['state'] == 'ended') {
								$event_length[$j] = array ('length' => ($drawEvent['draw_length'] / $gridLength), 'key' => $eventKey, 'overlap' => $loopevent['event_overlap'],'state' => 'begin');
								break;
							}
							$j++;
						}
						if ($j == sizeof($event_length)) {
							array_push ($event_length, array ('length' => ($drawEvent['draw_length'] / $gridLength), 'key' => $eventKey, 'overlap' => $loopevent['event_overlap'],'state' => 'begin'));
						}
					} else {
						
						$majors = $this->majors;
						if (! $majors[$mekey]) {
							$majors[$mekey] = $loopevent;
							$this->majors = $majors;
						}
						
					}
				}
			}
			if (preg_match('/([0-9]{1,2}):00/', $key)) {
				$daydisplay .= '<tr>'."\n";
				$daydisplay .= '<td rowspan="' . (60 / $gridLength) . '" align="center" valign="top" width="60" class="timeborder">'.$key.'</td>'."\n";
				$daydisplay .= '<td bgcolor="#a1a5a9" width="1" height="' . $gridLength . '"></td>'."\n";
			} elseif($cal_time == $day_start) {
				$size_tmp = 60 - (int)substr($cal_time,2,2);
				$daydisplay .= '<tr>'."\n";
				$daydisplay .= "<td rowspan=\"" . ($size_tmp / $gridLength) . "\" align=\"center\" valign=\"top\" width=\"60\" class=\"timeborder\">$key</td>\n";
				$daydisplay .= '<td bgcolor="#a1a5a9" width="1" height="' . $gridLength . '"></td>'."\n";
			} else {
				$daydisplay .= '<tr>'."\n";
				$daydisplay .= '<td bgcolor="#a1a5a9" width="1" height="' . $gridLength . '"></td>'."\n";
			}
			if ($dayborder == 0) {
				$class = ' class="dayborder"';
				$dayborder++;
			} else {
				$class = ' class="dayborder2"';
				$dayborder = 0;
			}
			if (sizeof($event_length) == 0) {
				$daydisplay .= '<td colspan="' . $nbrGridCols . '" '.$class.'>&nbsp;</td>'."\n";
				
			} else {
				$emptyWidth = $nbrGridCols;
				// Used to "join" ended events, so the ended case below results in one colspan'd td instead of multiple tds.
				$ended_counter = 0;
				for ($i=0;$i<sizeof($event_length);$i++) {
					$drawWidth = $nbrGridCols / ($event_length[$i]['overlap'] + 1 - count($this->majors));

					$emptyWidth = $emptyWidth - $drawWidth;
									
						switch ($event_length[$i]['state']) {
							case 'begin':
							  if ($ended_counter) {
								$daydisplay .= '<td colspan="' . $ended_counter . '" '.$class.'>&nbsp;</td>';
								$ended_counter = 0;
							  }
							  $event_length[$i]['state'] = 'started';
	 						  $uid = $event_length[$i]['key'];
	 						  $event_start 	= strtotime ($this_time_arr[$uid]['event_start']);
	 						  $event_end	= strtotime ($this_time_arr[$uid]['event_end']);
	 						  if (isset($this_time_arr[$uid]['display_end'])) $event_end = strtotime ($this_time_arr[$uid]['display_end']);
							  $event_start 	= date ($timeFormat, $event_start);
							  $event_end	= date ($timeFormat, $event_end);
	 						  $event_recur  = $this_time_arr[$uid]['recur'];
	 						  $event_status = strtolower($this_time_arr[$uid]['status']);
							  $event_calno 	= $this_time_arr[$uid]['categories'] ? array_search($this_time_arr[$uid]['categories'], array_values($coldata)) + 1 : $this_time_arr[$uid]['calnumber'];
							  $event_calno  = (($event_calno - 1) % $unique_colors) + 1;
							  $confirmed = '';
							  if (is_array($event_recur)) $confirmed .= '<img src="images/recurring.gif" width="9" height="9" alt="" border="0" hspace="0" vspace="0" />&nbsp;';
							  if ($event_status != '') $confirmed .= '<img src="images/'.$event_status.'.gif" width="9" height="9" alt="" border="0" hspace="0" vspace="0" />&nbsp;';
							  $colspan_width = round((460 / $nbrGridCols) * $drawWidth);
							  list($bgc, $bgcl) = calcols($event_calno);
						      if ($bgc) {
							  	$daydisplay .= '<td rowspan="' . $event_length[$i]['length'] . '" width="'.$colspan_width.'" colspan="' . $drawWidth . '" align="left" valign="top" style="background: '.$bgcl.';">'."\n";
							  } else {
							  	$daydisplay .= '<td rowspan="' . $event_length[$i]['length'] . '" width="'.$colspan_width.'" colspan="' . $drawWidth . '" align="left" valign="top" class="eventbg2_'.$event_calno.'">'."\n";
							  }
							  // Start drawing the event
							  $event_temp  = $loop_event;

							  $event 	   = openevent($getdate, $cal_time, $uid, $this_time_arr[$uid], 0, 0, 'ps');
							  $event_temp  = str_replace('{EVENT}', $event, $event_temp);
							  $event_temp  = str_replace('{EVENT_START}', $event_start, $event_temp);
							  $event_temp  = str_replace('{EVENT_END}', $event_end, $event_temp);
							  $event_temp  = str_replace('{CONFIRMED}', $confirmed, $event_temp);

							if ( $bgc ) {
								$event_temp   = str_replace('{EVENTHEAD}',"style='background: $bgc;'", $event_temp);
							} else {
								$event_temp   = str_replace('{EVENTHEAD}',"class='eventbg_$event_calno'", $event_temp);
							}
						
							  $daydisplay .= $event_temp;
							  $daydisplay .= '</td>';
						
						      $putmein = $event_calno;
						      if ($bgc) $putmein .= '" style="background-color: '.$bgc;
						

							  $daydisplay  = str_replace('{EVENT_CALNO}', $putmein, $daydisplay);
														
							  // End event drawing
						  
							  break;
							case 'started':
								if ($ended_counter) {
									$daydisplay .= '<td colspan="' . $ended_counter . '" '.$class.'>&nbsp;</td>';
									$ended_counter = 0;
								}
								break;
							case 'ended':
								$daydisplay .= '<td colspan="' . $drawWidth . '" ' . $class . '>&nbsp;</td>'."\n";
								break;
						}
						$event_length[$i]['length']--;
						if ($event_length[$i]['length'] == 0) {
							$event_length[$i]['state'] = 'ended';
						}
										
				}

				// Clean up.
				$emptyWidth += $ended_counter;
				//fill empty space on the right
				if ($emptyWidth > 0) {
					$daydisplay .= '<td colspan="' . $emptyWidth . '" ' . $class . '>&nbsp;</td>'."\n";
				}
				while (isset($event_length[(sizeof($event_length) - 1)]) && $event_length[(sizeof($event_length) - 1)]['state'] == 'ended') {
					array_pop($event_length);
				}

				
			}
			$daydisplay .= '</tr>'."\n";
		}
		
		$this->page = preg_replace('!<\!-- loop row on -->(.*)<\!-- loop event off -->!is', $daydisplay, $this->page);
	
	
	}
	
	function tomorrows_events() {
		global $template, $getdate, $master_array, $next_day, $timeFormat, $tomorrows_events_lines;
		
		preg_match("!<\!-- switch t_allday on -->(.*)<\!-- switch t_allday off -->!is", $this->page, $match1);
		preg_match("!<\!-- switch t_event on -->(.*)<\!-- switch t_event off -->!is", $this->page, $match2);
		$loop_t_ad 	= trim($match1[1]);
		$loop_t_e 	= trim($match2[1]);
		$return_adtmp	= '';
		$return_etmp	= '';

		if (is_array($master_array[$next_day]) && sizeof($master_array[$next_day]) > 0) {
			foreach ($master_array[$next_day] as $cal_time => $event_times) {
				foreach ($event_times as $uid => $val) {
					$event_text = stripslashes(urldecode($val["event_text"]));
					$event_text = strip_tags($event_text, '<b><i><u>');
					if ($event_text != "") {
						if (!isset($val["event_start"])) {
							$return_adtmp = openevent($next_day, $cal_time, $uid, $val, $tomorrows_events_lines, 21, 'psf');
							$replace_ad  .= str_replace('{T_ALLDAY}', $return_adtmp, $loop_t_ad);
						} else {
							$return_etmp  = openevent($next_day, $cal_time, $uid, $val, $tomorrows_events_lines, 21, 'ps3');
							$replace_e   .= str_replace('{T_EVENT}', $return_etmp, $loop_t_e);
						}
					}
				}
			}

			$this->page = preg_replace('!<\!-- switch t_allday on -->(.*)<\!-- switch t_allday off -->!is', $replace_ad, $this->page);
			$this->page = preg_replace('!<\!-- switch t_event on -->(.*)<\!-- switch t_event off -->!is', $replace_e, $this->page);		

		} else {

			$this->page = preg_replace('!<\!-- switch tomorrows_events on -->(.*)<\!-- switch tomorrows_events off -->!is', '', $this->page);

		}
	}

	function get_vtodo() {
		global $template, $getdate, $master_array, $next_day, $timeFormat, $tomorrows_events_lines, $show_completed, $show_todos;
		
		preg_match("!<\!-- switch show_completed on -->(.*)<\!-- switch show_completed off -->!is", $this->page, $match1);
		preg_match("!<\!-- switch show_important on -->(.*)<\!-- switch show_important off -->!is", $this->page, $match2);
		preg_match("!<\!-- switch show_normal on -->(.*)<\!-- switch show_normal off -->!is", $this->page, $match3);
		$completed 	= trim($match1[1]);
		$important 	= trim($match2[1]);
		$normal 	= trim($match3[1]);
		$nugget2	= '';
		$todo_popup_data_index = 0;
		if (is_array($master_array['-2'])) {
			foreach ($master_array['-2'] as $vtodo_times) {
				foreach ($vtodo_times as $val) {
					$vtodo_text = stripslashes(urldecode($val["vtodo_text"]));
					if ($vtodo_text != "") {	
						if (isset($val["description"])) { 
							$description 	= stripslashes(urldecode($val["description"]));
						} else {
							$description = ""; 
						}
						$completed_date = $val['completed_date'];
						$event_calna 	= $val['calname'];
						$status 		= $val["status"];
						$priority 		= $val['priority'];
						$start_date 	= $val["start_date"];
						$due_date 		= $val['due_date'];
						$vtodo_array 	= array(
							'cal'			=> $event_calna,
							'completed_date'=> $completed_date,
							'description'	=> $description,
							'due_date'		=> $due_date,
							'priority'		=> $priority,
							'start_date'	=> $start_date,
							'status'		=> $status,
							'vtodo_text' 	=> $vtodo_text);
						$vtodo_array 	= base64_encode(urlencode(serialize($vtodo_array)));
						$todo = "
						<script language=\"Javascript\" type=\"text/javascript\"><!--
						var todoData = new TodoData('$vtodo_array','$vtodo_text');
						document.todo_popup_data[$todo_popup_data_index] = todoData;
						// --></script>";

						$todo .= '<a class="psf" title="'.$title.'" href="#" onclick="openTodoInfo('.$todo_popup_data_index.'); return false;">';
						$todo_popup_data_index++;
						$vtodo_array = $todo;
						
						$vtodo_text 	= word_wrap(strip_tags(str_replace('<br />',' ',$vtodo_text), '<b><i><u>'), 21, $tomorrows_events_lines);
						$data 			= array ('{VTODO_TEXT}', '{VTODO_ARRAY}');
						$rep			= array ($vtodo_text, $vtodo_array);
						
						// Reset this TODO's category.
						$temp = '';
						if ($status == 'COMPLETED' || (isset($val['completed_date']) && isset($val['completed_time']))) {
							if ($show_completed == 'yes') {
								$temp = $completed;
							}
						} elseif (isset($val['priority']) && ($val['priority'] != 0) && ($val['priority'] <= 5)) {
							$temp = $important;
						} else {
							$temp = $normal;
						}
						
						// Do not include TODOs which do not have the
						// category set.
						if ($temp != '') {
							$nugget1 = str_replace($data, $rep, $temp);
							$nugget2 .= $nugget1;
						}
					}
				}
			}	
		}
		
		// If there are no TODO items, completely hide the TODO list.
		if (($nugget2 == '') || ($show_todos != 'yes')) {
			$this->page = preg_replace('!<\!-- switch vtodo on -->(.*)<\!-- switch vtodo off -->!is', '', $this->page);
		}
		
		// Otherwise display the list of TODOs.
		else {
			$this->page = preg_replace('!<\!-- switch show_completed on -->(.*)<\!-- switch show_normal off -->!is', $nugget2, $this->page);
		}
	}
	
	function draw_month($template_p, $offset = '+0', $type) {
		global $coldata, $majors_config, $template, $getdate, $master_array, $this_year, $this_month, $unique_colors, $dateFormat_month, $week_start_day, $cal, $minical_view, $month_event_lines, $daysofweekreallyshort_lang, $daysofweekshort_lang, $daysofweek_lang, $timeFormat_small, $timeFormat;
		preg_match("!<\!-- loop weekday on -->(.*)<\!-- loop weekday off -->!is", $template_p, $match1);
		preg_match("!<\!-- loop monthdays on -->(.*)<\!-- loop monthdays off -->!is", $template_p, $match2);
		preg_match("!<\!-- switch notthismonth on -->(.*)<\!-- switch notthismonth off -->!is", $template_p, $match3);
		preg_match("!<\!-- switch istoday on -->(.*)<\!-- switch istoday off -->!is", $template_p, $match4);
		preg_match("!<\!-- switch ismonth on -->(.*)<\!-- switch ismonth off -->!is", $template_p, $match5);
		preg_match("!<\!-- loop monthweeks on -->(.*)<\!-- loop monthdays on -->!is", $template_p, $match6);
		preg_match("!<\!-- loop monthdays off -->(.*)<\!-- loop monthweeks off -->!is", $template_p, $match7);		
				
		$loop_wd 			= trim($match1[1]);
		$loop_md 			= trim($match2[1]);
		$t_month[0]			= trim($match3[1]);
		$t_month[1]			= trim($match4[1]);
		$t_month[2] 		= trim($match5[1]);
		$startweek 			= trim($match6[1]);
		$endweek 			= trim($match7[1]);
		if ($type != 'medium') {
			$fake_getdate_time 	= strtotime($this_year.'-'.$this_month.'-15');
			$fake_getdate_time	= strtotime("$offset month", $fake_getdate_time);
		} else {
			$fake_getdate_time 	= strtotime($this_year.'-'.$offset.'-15');
		}
		
		$minical_month 		= date("m", $fake_getdate_time);
		$minical_year 		= date("Y", $fake_getdate_time);
		$first_of_month 	= $minical_year.$minical_month."01";
		$first_of_year 		= $minical_year."0101";

		// Add links in to the month/year views.
		$dateFormat_month_local = str_replace("%B", "<a class=\"ps3\" href=\"javascript: get_callendar_xml('month.php?cal=$cal&amp;getdate=$first_of_month')\">%B</a>", $dateFormat_month);
		$dateFormat_month_local = str_replace("%Y", "%Y", $dateFormat_month_local);

		//$start_day 			= strtotime($week_start_day);
		$start_day			= strtotime(dateOfWeek($getdate, $week_start_day));
		$month_title 		= localizeDate ($dateFormat_month_local, $fake_getdate_time);
		$month_date 		= date ('Ymd', $fake_getdate_time);

		if ($type == 'small') {
			$langtype = $daysofweekreallyshort_lang;
		} elseif ($type == 'medium') {
			$langtype = $daysofweekshort_lang;
		} elseif ($type == 'large') {
			$langtype = $daysofweek_lang;	
		}
		
		for ($i=0; $i<7; $i++) {
			$day_num 		= date("w", $start_day);
			$weekday 		= $langtype[$day_num];
			$start_day 		= strtotime("+1 day", $start_day);
			$loop_tmp 		= str_replace('{LOOP_WEEKDAY}', $weekday, $loop_wd);
			$weekday_loop  .= $loop_tmp;
		}
		
		$start_day 			= strtotime(dateOfWeek($first_of_month, $week_start_day));
		$i 					= 0;
		$whole_month 		= TRUE;

		do {
			if ($i == 0) $middle .= $startweek; $i++;
			#$temp_middle			= $loop_md;
			$switch					= array('ALLDAY' => '', 'CAL' => $cal, 'MINICAL_VIEW' => $minical_view);
			$check_month 			= date ("m", $start_day);
			$daylink 				= date ("Ymd", $start_day);	
			
			$switch['DAY']	 		= date ("j", $start_day);
			$switch['DAYLINK'] 		= date ("Ymd", $start_day);
			if ($check_month != $minical_month) {
				$temp = $t_month[0];
			} elseif ($daylink == $getdate && date("n", $fake_getdate_time) == date('n')) {
				$temp = $t_month[1];
			} else {
				$temp = $t_month[2];
			}
			if (isset($master_array[$daylink])) {
				if ($type != 'small') {
					foreach ($master_array[$daylink] as $cal_time => $event_times) {
						foreach ($event_times as $uid => $val) {
							
							$me = $master_array[$daylink][$cal_time][$uid];
							if (!($me['event_length'] >= $majors_config['time_limit'] || ($me['recur']['FREQ'] == 'daily' and $me['recur']['COUNT'] >=$majors_config['day_limit'] ))) {


								$event_calno 	= $val['categories'] ? array_search($val['categories'], array_values($coldata)) + 1 : $val['calnumber'];

								$event_calno	= (($event_calno - 1) % $unique_colors) + 1;
								if (!isset($val['event_start'])) {
									if ($type == 'large') {
										$switch['ALLDAY'] .= '<div class="V10 cal_'.$event_calno.'">'.caldot($template, $event_calno);
	 									$addme = openevent($daylink, $cal_time, $uid, $val, $month_event_lines, 12, 'psf');
		 								$switch['ALLDAY'] .= $addme;
										//if (! strpos($addme,"<a") === false ) {
										//	$switch['ALLDAY'] .= (isset($val['location']) && $val['location'] != '') ? $val['location']."<br />" : '';
										//} else {
										//	$switch['ALLDAY'] .= (isset($val['location']) && $val['location'] != '') ? $val['location'] : '';											
										//}
										$switch['ALLDAY'] .= '</div>';
									} else {
										$switch['ALLDAY'] .= caldot($template, $event_calno);
									}
								} else {	
									$start2		 = date($timeFormat_small, $val['start_unixtime']);
									if ($type == 'large') {
										$switch['EVENT'] .= '<div class="V9 cal_'.$event_calno.'">'.caldot($template, $event_calno);
	 									$addme = openevent($daylink, $cal_time, $uid, $val, $month_event_lines, 12, 'ps3', "$start2 ").'';
	 									$switch['EVENT'] .= $addme;
										//if (! strpos($addme,"<a") === false ) {
	 									//	$switch['EVENT'] .= (isset($val['location']) && $val['location'] != '') ? "<br />".$val['location']."<br />" : '';
										//} else {
	 									//	$switch['EVENT'] .= (isset($val['location']) && $val['location'] != '') ? $val['location'] : '';											
										//}
										$switch['EVENT'] .= '</div>';
									} else {
										$switch['EVENT'] = caldot($template, $event_calno);
									}
								}
							} else {
								$majors = $this->majors;
								if (! $majors[$uid]) {
									$majors[$uid] = $me;
									$this->majors = $majors;
								}
							}
						}
					}
				}
			}
			
			
			$switch['EVENT'] = (isset($switch['EVENT'])) ? $switch['EVENT'] : '';
			$switch['ALLDAY'] = (isset($switch['ALLDAY'])) ? $switch['ALLDAY'] : '';
			
			#echo "<pre>";print_r($switch);echo "</pre>";
			
			foreach ($switch as $tag => $data) {
				$temp = str_replace('{'.$tag.'}', $data, $temp);
			}
			$middle .= $temp;
			
			$start_day = strtotime("+1 day", $start_day); 
			if ($i == 7) { 
				$i = 0;
				$middle .= $endweek;
				$checkagain = date ("m", $start_day);
				if ($checkagain != $minical_month) $whole_month = FALSE;	
			}
		} while ($whole_month == TRUE); 

		$return = str_replace('<!-- loop weekday on -->'.$match1[1].'<!-- loop weekday off -->', $weekday_loop, $template_p);
		$return = preg_replace('!<\!-- loop monthweeks on -->(.*)<\!-- loop monthweeks off -->!is', $middle, $return);
		$return = str_replace('{MONTH_TITLE}', $month_title, $return);
		$return = str_replace('{CAL}', $cal, $return);
		$return = str_replace('{MONTH_DATE}', $month_date, $return);
		return $return;	
	}
	
	
	
	
	function draw_upcomings($template_p) {
		global $template, $getdate, $master_array, $this_year, $this_month, $unique_colors, $dateFormat_month, $week_start_day, $cal, $minical_view, $month_event_lines, $daysofweekreallyshort_lang, $daysofweekshort_lang, $daysofweek_lang, $timeFormat_small, $timeFormat;

		preg_match("!<\!-- switch results on -->(.*)<\!-- switch results off -->!is", $template_p, $match1);
		$loop_event		= trim($match1[1]);
		$parse_month 	= date ("Ym", time());
		$final = "";
		$i = 0;
		$start_day = time();
		$done_us = array();
		do {

			$daylink = date ("Ymd", $start_day);

			if (isset($master_array[$daylink])) {
				foreach ($master_array[$daylink] as $cal_time => $event_times) {
					foreach ($event_times as $key => $val) {

						//if (in_array($key, $done_us)) continue;

						$done_us[] = $key;
						$event_calno 	= $val['categories'] ? array_search($val['categories'], array_values($coldata)) : $val['calnumber'];
						$event_calno	= (($event_calno - 1) % $unique_colors) + 1;
						$dayofmonth = localizeDate ('%A, %B %e %Y', $start_day);
						$dayofmonth = $dayofmonth;
						$events_tmp = $loop_event;

						if ($val['event_text']) {	
							$link_text = openeventfull($daylink, $cal_time, $key, $val);
							$event_text = caldot($template, $event_calno).$link_text;

							$link_html = str_replace(urldecode($val['event_text'])."</a>", "", $link_text);
							$description 	= html_shorten(stripslashes(urldecode($val['description'])), 50, "...".$link_html."more</a>" );
							$description 	= preg_replace('/(<br\s?\/>)*<p>(<br\s?\/>)*/', '<p>', $description );
							$location 	= stripslashes(urldecode($val['location']));
							$event_start 	= $val['event_start'];
							$event_end 		= $val['event_end'];

							if (isset($val['display_end'])) $event_end = $val['display_end'];

							if (!$event_start) { 
								$event_start = "All day.";
								$event_start2 = '';
								$event_end = '';
							} else {
									$event_start    = date ($timeFormat, strtotime ($event_start));
									$event_end      = date ($timeFormat, strtotime ($event_end));
									$event_start    = $event_start .' - '.$event_end;
							}							


							if ($description == '') {
								$events_tmp = preg_replace('!<\!-- switch description on -->(.*)<\!-- switch description off -->!is', '', $events_tmp);
							}
							if ($location == '') {
								$events_tmp = preg_replace('!<\!-- switch location on -->(.*)<\!-- switch location off -->!is', '', $events_tmp);
							}
							
							$event_calno_s = ' cal_' . $event_calno . ' ';
							
							$search		= array('{EVENT_START}', '{EVENT_TEXT}', '{DESCRIPTION}','{LOCATION}','{REGIONS}');
							$replace	= array($event_start, $event_text, $description, $location, $event_calno_s);
							$events_tmp = str_replace($search, $replace, $events_tmp);

							$some_events .= $events_tmp;
							$events_tmp	= $loop_event;

							$some_events  = str_replace('{KEY}', $daylink, $some_events);
							$some_events  = str_replace('{DAYOFMONTH}', $dayofmonth, $some_events);

							$pre = "<div class='upcoming cal_".$event_calno."'>";

							$final   .= $pre.$day_tmp.$some_events."</div>";
							unset ($day_tmp, $some_events);
							
						}

					}
				}
			}
			
			$start_day = strtotime("+1 day", $start_day); 
			
			$i++;

		} while ($i < 60);
		if (count($done_us) == 0) $final = "<p class='upcoming'>There are no events listed for the next 60 days.</p>";
		$return = str_replace('{CAL}', $cal, $final);
		$return = str_replace('{MONTH_DATE}', $month_date, $return);
		return $return;
	}
	
	
	
	
	function draw_month_majors() {
		global $template, $getdate, $master_array, $this_year, $this_month, $unique_colors, $dateFormat_month, $week_start_day, $cal, $minical_view, $month_event_lines, $daysofweekreallyshort_lang, $daysofweekshort_lang, $daysofweek_lang, $timeFormat_small, $timeFormat;
		return $return;
	}
	
	function nomonthbottom() {
		$this->page = preg_replace('!<\!-- switch showbottom on -->(.*)<\!-- switch showbottom off -->!is','', $this->page);
	}

	function nosearch() {
		$this->page = preg_replace('!<\!-- switch show_search on -->(.*)<\!-- switch show_search off -->!is','', $this->page);
	}
	
	function monthbottom() {
		global $template, $getdate, $master_array, $this_year, $this_month, $cal, $timeFormat, $timeFormat_small, $dateFormat_week_list, $lang;
		preg_match("!<\!-- loop showbottomevents_odd on -->(.*)<\!-- loop showbottomevents_odd off -->!is", $this->page, $match1);
		preg_match("!<\!-- loop showbottomevents_even on -->(.*)<\!-- loop showbottomevents_even off -->!is", $this->page, $match2);
		
		$loop[0] 	= trim($match1[1]);
		$loop[1] 	= trim($match2[1]);
		
		$m_start = $this_year.$this_month.'01';
		$u_start = strtotime($m_start);
		$i=0;
		$seen_events = array();
		do {
			if (isset($master_array[$m_start])) {
				foreach ($master_array[$m_start] as $cal_time => $event_times) {
				#	$switch['CAL'] 			= $cal;
				#	$switch['START_DATE'] 	= localizeDate ($dateFormat_week_list, $u_start);
					$start_date 	= localizeDate ($dateFormat_week_list, $u_start);
					foreach ($event_times as $uid => $val) {
						if (isset($seen_events[$uid])) continue;
						$seen_events[$uid] = 1;
						$switch['CAL'] 			= $cal;
						$switch['START_DATE'] 	= $start_date;
						$switch['CALNAME'] 	= $val['calname'];
						if (!isset($val['event_start'])) {
							$switch['START_TIME'] 	= $lang['l_all_day'];
							$switch['EVENT_TEXT'] 	= openevent($m_start, $cal_time, $uid, $val, $month_event_lines, 12, 'psf');
							$switch['DESCRIPTION'] 	= urldecode($val['description']);
						} else {
							$event_start = $val['start_unixtime'];
							$event_end 	 = (isset($val['display_end'])) ? $val['display_end'] : $val["event_end"];
							$event_start = date($timeFormat, $val['start_unixtime']);
							$event_end   = date($timeFormat, @strtotime ($event_end));
							$switch['START_TIME'] 	= $event_start . ' - ' . $event_end;
							$switch['EVENT_TEXT'] 	= openevent($m_start, $cal_time, $uid, $val, 0, 12, 'psf');
							$switch['DESCRIPTION'] 	= urldecode($val['description']);
						}

						if ($switch['EVENT_TEXT'] != '') {
							$switch['DAYLINK'] = $m_start;
							$temp = $loop[$i];
							foreach ($switch as $tag => $data) {
								$temp = str_replace('{'.$tag.'}', $data, $temp);
							}
							$middle .= $temp;
							$i = ($i == 1) ? 0 : 1;
						}
						unset ($switch);
					}
				}
			}
			$u_start 	 = strtotime("+1 day", $u_start);
			$m_start 	 = date('Ymd', $u_start);
			$check_month = date('m', $u_start);
		#	unset ($switch);
		} while ($this_month == $check_month);

		$this->page = preg_replace('!<\!-- loop showbottomevents_odd on -->(.*)<\!-- loop showbottomevents_even off -->!is', $middle, $this->page);

	}

	function Page($file = 'std.tpl') {
		global $template;
		if (!file_exists($file)){
			#look for it in default if not found
			$file = str_replace("templates/$template","templates/default",$file); 
			if (!file_exists($file)) die("Template file $file not found.");
		}	
		$this->page = join('', file($file));
	}

	function parse($file) {
		global $template; $lang;
		if (basename(dirname($file)) == "$template" || $file =='./functions/event.js'){
			if (!is_file($file)){
				#look for it in default if not found
				$file = str_replace("templates/$template","templates/default",$file); 
			}
			if (!is_file($file)){
				exit(error($lang['l_error_path'], $file));
			}
			ob_start();
			include($file);
			$buffer = ob_get_contents();
			ob_end_clean();
			return $buffer;
		}
	}
	
	function replace_tags($tags = array()) {
		if (sizeof($tags) > 0)
			foreach ($tags as $tag => $data) {
				
				// This removes any unfilled tags
				if (!$data) {
					$this->page = preg_replace('!<\!-- switch ' . $tag . ' on -->(.*)<\!-- switch ' . $tag . ' off -->!is', '', $this->page);
				}
				
				// This replaces any tags
				$this->page = str_replace('{' . strtoupper($tag) . '}', $data, $this->page);
			}
			
		else
			die('No tags designated for replacement.');
		}
		
	function replace_files($tags = array()) {
		if (sizeof($tags) > 0)
			foreach ($tags as $tag => $data) {
				
				// This opens up another template and parses it as well.
				$data = $this->parse($data);
				
				// This removes any unfilled tags
				if (!$data) {
					$this->page = preg_replace('!<\!-- switch ' . $tag . ' on -->(.*)<\!-- switch ' . $tag . ' off -->!is', '', $this->page);
				}
				
				// This replaces any tags
				$this->page = str_replace('{' . strtoupper($tag) . '}', $data, $this->page);
			}
			
		else
			die('No tags designated for replacement.');
		}
	
	function output() {
		global $majors_config, $ongoing_position, $template, $php_started, $lang, $enable_rss, $template_started, $unix_time, $next_month_year, $next_month_month, $custom_view, $cpath, $unique_colors;

		$next_month_start = mktime(0,0,0,$next_month_month,1,$next_month_year);
		// Looks for {MONTH} before sending page out
		preg_match_all ('!\{MONTH_([A-Z]*)\|?([+|-])([0-9]{1,2})\}!is', $this->page, $match);
			
		if (sizeof($match) > 0) {
			$i=0;
			foreach ($match[1] as $key => $val) {
				
				if ($match[1][$i] == 'SMALL') {
					$template_file 	= $this->parse(BASE.'templates/'.$template.'/month_small.tpl');
					$type 			= 'small';
					$offset 		= $match[2][$i].$match[3][$i];
				} elseif ($match[1][$i] == 'MEDIUM') {
					$template_file 	= $this->parse(BASE.'templates/'.$template.'/month_medium.tpl');
					$type 			= 'medium';
					$offset 		= $match[3][$i];
				} else {
					if ($custom_view == 'upcoming') {
						$template_file 	= $this->parse(BASE.'templates/'.$template.'/month_upcoming.tpl');
						$type 			= 'upcoming';
						$offset 		= 0;
					} else {
						$template_file 	= $this->parse(BASE.'templates/'.$template.'/month_large.tpl');
						$type 			= 'large';
						$offset 		= $match[2][$i].$match[3][$i];
					}
				}
				if ($custom_view == 'upcoming') {
					$data = $this->draw_upcomings($template_file);	

				} else {
					$data = $this->draw_month($template_file, $offset, $type);				
				}
				$this->page = str_replace($match[0][$i], $data, $this->page);
				$i++;
			}
		}
		

		

		if (count($this->majors)) {
			$majors_out = '<div id="major_box" class="multi-day"><h2>Ongoing Events</h2><a id="view_majors" href="javascript: toggle_majors(1);"></a><a id="close_majors" href="javascript: toggle_majors(0);">Close</a><a id="close_majors2" href="javascript: toggle_majors(0);">Close</a></div>';
			foreach ($this->majors as $uid => $val) {
				$eomd = $this->num_abbrev_str(date('d', $next_month_start-(60*60*24)));
				$real_start = ( $val['orig_start'] > 0 ) ? $val['orig_start'] : $val['start_unixtime'];
				$event_calno 	= $val['categories'] ? array_search($val['categories'], array_values($coldata)) : $val['calnumber'];
				$event_calno	= (($event_calno - 1) % $unique_colors) + 1;	

				$event_text = stripslashes(urldecode($val["event_text"]));
				$event_date = date('Ymd', $val['start_unixtime']);
				$calc_end = ($val['event_length'] >= $majors_config['time_limit']) ? $val['end_unixtime'] : $val['recur']['COUNT']*60*60*24 + $real_start;

				$start_day_s = $this->num_abbrev_str(date('M d Y',$val['start_unixtime']));
				$end_day_s = $this->num_abbrev_str(date('M d Y', $calc_end ));
				$title = $event_text;

				// make events that have a past end date expire
				// issue 6632 rw
				if ($calc_end < time()) {
					continue;
				}

				# for iCal pseudo tag <http> comptability
				if (ereg("<([[:alpha:]]+://)([^<>[:space:]]+)>",$event_text,$matches)) {
					$full_event_text = $matches[1] . $matches[2];
					$event_text = $matches[2];
				} else {
					$full_event_text = $event_text;
					$event_text = strip_tags($event_text, '<b><i><u><img>');
				}
				
				$majors_out .= '<div class="major_e">'.caldot($template, $event_calno)."<div>";
				
				$date_rage_s = $start_day_s . " - " . $end_day_s;

				if (!($val['event_length'] >= $majors_config['time_limit'])) {
					$date_rage_s .= ", daily";
				}
				
				if ($start_day_s == $end_day_s) $date_rage_s = $start_day_s;
				
				$majors_out .= $date_rage_s . ": ";
				
	
				if ($val['description'] || 1==1) {
					$cal_name = $val['calname'];
					$event_data = addslashes(json_encode($val));
					// fix for URL-length bug in IE: populate and submit a hidden form on click
					$majors_out .= "<a class='$link_class' onclick='window.eventData = new EventData(\"$event_date\",\"0\",\"$uid\",\"$cpath\",\"$event_data\");var el=this;var t = el.title || el.name || null;var a=\"/extension/phpicalendar/includes/event.php?cal_name=$cal_name&KeepThis=true&TB_iframe=true&height=275&width=460\";var g = el.rel || false;tb_show(t,a,g);el.blur();return false;' title='View more details - $title' href='#'>";
				} else {
					$majors_out .= '<a class="'.$link_class.'" title="View more details - '.$title.'" href="'.$res[1].'">';
				}

				$majors_out .= "$event_text</a>";
				

				$majors_out .= (isset($val['location']) && $val['location'] != '') ? ", ".$val['location'] : '';
				
				$majors_out .= "</div></div>\n";
			}
			$majors_out .= "<br/>";
		} else {
			$majors_out = '<div class="multi-day"><h2>Ongoing Events</h2><span><!--Multi-day events will be listed here.--></span></div>';
		}
		$outcount = count($this->majors);
		$majors_out = "<div id='cal_month_majors' class='majcount$outcount'>" . $majors_out . "</div>";
		if ($ongoing_position == "BOTTOM") {
			$this->page = str_replace('{TOP_MONTHMAJORS}', "", $this->page);
			$this->page = str_replace('{BOTTOM_MONTHMAJORS}', $majors_out, $this->page);
		} else {
			$this->page = str_replace('{TOP_MONTHMAJORS}', $majors_out, $this->page);
			$this->page = str_replace('{BOTTOM_MONTHMAJORS}', "", $this->page);
		}
		$php_ended = @getmicrotime();
		$generated1 = number_format(($php_ended-$php_started),3);
		$generated2 = number_format(($php_ended-$template_started),3);
		$this->page = str_replace('{GENERATED1}', $generated1, $this->page);
		$this->page = str_replace('{GENERATED2}', $generated2, $this->page);
		if ($enable_rss != 'yes') {
			$this->page = preg_replace('!<\!-- switch rss_powered on -->(.*)<\!-- switch rss_powered off -->!is', '', $this->page);
		} else {
			$this->page = str_replace('{BASE}', BASE, $this->page);
		}
		if ($cpath){
//			$this->page = str_replace('&amp;getdate', "&amp;cpath=$cpath&amp;getdate", $this->page);
		}
		print($this->page);
	}
}

function html_shorten($operatorValue, $max_length, $endstring) {
	$save_temp = $operatorValue;
    $save_temp2 = $operatorValue;
    preg_match_all("/<[^>]*?>/", $save_temp, $tag_matches);

    if ($max_length == 0) {
		if(count($tag_matches[0])) {
	    	$save_temp3 = preg_replace("/<p>(&nbsp;|\s)*?<\/p>/", "", $save_temp2);
	    	$out = preg_replace("/(<[^>]*?>)(\s*)?$/", $endstring.'$1$2', $save_temp3);
		} else {
	    	$out = preg_replace("/(\S)(\s*?)$/", '$1'.$endstring.'$2', $save_temp2);
		}
		$operatorValue = $out;
		return true;
    } else {
    	$resultstring='';
    	$opentags = array();
    	$cropped=false;
    	foreach ($tag_matches[0] as $key => $this_tag_match) {
	    	$splitarray = split($this_tag_match, $save_temp, 2);

			$tagtype = preg_replace("/ .*\/?>/", ">", $this_tag_match);

			$tagtypeopen = preg_replace("/\//", "", $tagtype);
			if (strlen($splitarray[0])  > $max_length && !$cropped) {
			   $word_end = strpos($splitarray[0], ' ', $max_length);
			   if (!$word_end) $word_end = strlen($splitarray[0]);
			   $resultstring .= substr($splitarray[0], 0, $word_end); 
			   $cropped=true;
			   $resultstring .= $endstring;
			} else {
		   		if (!$cropped) {
					$resultstring .= $splitarray[0];
					if ( strpos($tagtype, '/') === false ) {$directional = 1;} else {$directional = -1;}
					$tagtypeopen = preg_replace("/\//", "", $tagtype);
					if (array_key_exists($tagtypeopen, $opentags) ) {
						$opentags[$tagtypeopen] = $opentags[$tagtypeopen] + $directional;
					} else {
						$opentags[$tagtypeopen] = $directional;
					}
				}
		   		$max_length = $max_length - strlen($splitarray[0]);
			}
			if (!$cropped) {
		   		$resultstring .= $this_tag_match;
			} else {
		   		if (strpos($tagtype, '/') == 1 && array_key_exists($tagtypeopen, $opentags) && $opentags[$tagtypeopen] > 0) {
					$resultstring .= $tagtype;
					$opentags[$tagtypeopen] = $opentags[$tagtypeopen] -1;
		   		}
			}

			$save_temp = $splitarray[1];
		 }
	    	
		if (count($tag_matches[0]) == 0) {
			if (strlen($save_temp2)  > $max_length) {
		   		$word_end = strpos($save_temp2, ' ', $max_length);
		   		$resultstring .= substr($save_temp2, 0, $word_end); 
		   		$cropped=true;
		   		$resultstring .= $endstring;
	    	} else {
		   		$resultstring = $save_temp2;
	    	}
    	}

    	return $resultstring;
	}
	
}
	
?>