<?php

function caldot($template, $event_calno = false) {
	global $coldata;
	$my_image = "event_dot.gif";
	$myclass = "";
	if ($event_calno) $my_image = "monthdot_$event_calno.gif";
	if (array_key_exists($event_calno, $coldata)) {
		$my_image = "/extension/phpicalendar/images/colbul.php?col=". trim($coldata[$event_calno], "#");
	}
	$out = "<img src='$my_image' alt='' width='14' height='14' border='0' />";
	return $out;
}

function calcols($event_calno = false) {
	global $coldata;
	if (array_key_exists($event_calno, $coldata)) {
		$bgc = $coldata[$event_calno];
		$bgcl = changeBrightness ( trim($bgc,"#"), .9 );
		return array($bgc, $bgcl);
	}
	return false;
}

function list_jumps() {
	global $second_offset, $lang, $cal;
	$calName = join(',', array_map("getCalendarName", split(',', $cal)));
	$today = date('Ymd', time() + $second_offset);
	$return = '<option value="#">'.$lang['l_jump'].'</option>';
	$return .= '<option value="day.php?cal='.$calName.'&amp;getdate='.$today.'">'.$lang['l_goday'].'</option>';
	$return .= '<option value="week.php?cal='.$calName.'&amp;getdate='.$today.'">'.$lang['l_goweek'].'</option>';
	$return .= '<option value="month.php?cal='.$calName.'&amp;getdate='.$today.'">'.$lang['l_gomonth'].'</option>';
	$return .= '<option value="year.php?cal='.$calName.'&amp;getdate='.$today.'">'.$lang['l_goyear'].'</option>';
	return $return;
}

function list_calcolors() {
	global $linkedmonths, $all_event_icon, $caltogs, $template, $master_array, $unique_colors, $coldata;
	$i = 1;
	if (is_array($master_array['-3'])) {
		$checkstring = ($caltogs["caltog_999"] == 'true' || array_key_exists(0,$caltogs)) ? "CHECKED" : "";
		if (count($master_array['-3']) > 1) {
			$idblockstr = "";
			if ($all_event_icon != "") $idblockstr = "<img src='" . $all_event_icon . "' alt='' />";
			$return .= '<div class="cal_legend"><input id="caltog_999" class="caltoggle" type="checkbox" '.$checkstring.' onclick ="togglecals(999, this.checked)" />';
			$return .= $idblockstr.'Show All Events</div>';
		}
		$following = '';
		$boxheight = (count($master_array['-3']) * 50) + 50;
		foreach ($master_array['-3'] as $key => $val) {
			$valsave = $val;
			$filepath = str_replace("./calendars","webcal://".$_SERVER['HTTP_HOST']."/phpicalendar/ezpublishicals",$master_array['-4'][$key]['filename']);
			$filepath = str_replace("http://", "webcal://", $filepath);
			if ($i > $unique_colors) $i = 1;
			$val = str_replace ("\,", ",", $val);
			$idblockstr = "<img src='/extension/phpicalendar/templates/$template/images/monthdot_$i.gif' alt='' />";
			$href='';
			$multi = 1;
			if (file_exists(BASE.'calendars/'.$val.'.data')) {
				$val = file_get_contents(BASE.'calendars/'.$val.'.data');
				$temp_ar = explode("||",$val);
				$val = $temp_ar[0];
				$coldata[$i] = $temp_ar[1];
				$href = "/".$temp_ar[2];
				$my_image = "/extension/phpicalendar/images/colbul.php?col=". trim($temp_ar[1], "#");
				$idblockstr = "<img src='$my_image' alt='' width='14' height='14' border='0' />";
			}
						
			if (array_key_exists($master_array['-4'][$key]['filename'], $coldata)) {
				$mycol = $coldata[$master_array['-4'][$key]['filename']];
				$coldata[$i] = $mycol;
				$my_image = "/extension/phpicalendar/images/colbul.php?col=". trim($mycol, "#");
				$idblockstr = "<img src='$my_image' alt='' width='14' height='14' border='0' />";
			}
			$checkstring = ($caltogs["caltog_".$i] == 'true') ? "CHECKED" : ""; 
			if (count($master_array['-3']) > 1) {
				$return .= '<div class="cal_legend"><input id="caltog_'.$i.'" class="caltoggle" type="checkbox" '.$checkstring.' onclick="togglecals('.$i.', this.checked)" />';
				if ($href != '' && (isset($linkedmonths) == false || $linkedmonths == '1' )) {
					$return .= $idblockstr.'<a href="'.$href.'">'.$val.'</a></div>';
				} else {
					$return .= $idblockstr.$val.'</div>';
				}
				
			}
			
			$following .= '<h3>'.$val.'</h3><p><input type="button" class="downloadcal" value = "download" onclick="document.location.href=\''.$filepath.'\'"/>&nbsp;&nbsp;'.$filepath.'</p>';

			if (($i+1) % 3 == 0) $return .= "<div class='clear_legend'></div>";
			
			$i++;
		}
	}
	$return .=  '<div id="subscribe_link" style="display: none"><h2>Add These Calendars</h2><p>The following calendars are available for download / subscription:</p>'.$following.'</div>';
	$return .= '<a id = "download_cal" href="#" onclick="var el=this; var t = el.title || el.name || null; var a=\'#TB_inline?height='.$boxheight.'&width=700&inlineId=subscribe_link\'; var g = el.rel || false; tb_show(t,a,g); el.blur(); return false;">Calendar download / subscription</a></div>';		
	$return .= "<div class='clear_legend'></div>";
	return $return;
}

function list_months() {
	global $getdate, $this_year, $cal, $dateFormat_month;
	$month_time 	= strtotime("$this_year-01-01");
	$getdate_month 	= date("m", strtotime($getdate));
	for ($i=0; $i<12; $i++) {
		$monthdate 		= date ("Ymd", $month_time);
		$month_month 	= date("m", $month_time);
		$select_month 	= localizeDate($dateFormat_month, $month_time);
		if ($month_month == $getdate_month) {
			$return .= "<option value=\"month.php?cal=$cal&amp;getdate=$monthdate\" selected=\"selected\">$select_month</option>\n";
		} else {
			$return .= "<option value=\"month.php?cal=$cal&amp;getdate=$monthdate\">$select_month</option>\n";
		}
		$month_time = strtotime ("+1 month", $month_time);
	}
	return $return;
}


function list_years() {
	global $getdate, $this_year, $cal, $num_years;
	$year_time = strtotime($getdate);
	for ($i=0; $i < $num_years; $i++) {
		$offset = $num_years - $i;
		$prev_time = strtotime("-$offset year", $year_time);
		$prev_date = date("Ymd", $prev_time);
		$prev_year = date("Y", $prev_time);
		$return .= "<option value=\"year.php?cal=$cal&amp;getdate=$prev_date\">$prev_year</option>\n";
	}
	
	$getdate_date = date("Ymd", $year_time);
	$getdate_year = date("Y", $year_time);
	$return .= "<option value=\"year.php?cal=$cal&amp;getdate=$getdate_date\" selected=\"selected\">$getdate_year</option>\n";
	
	for ($i=0; $i < $num_years; $i++) {
		$offset = $i + 1;
		$next_time = strtotime("+$offset year", $year_time);
		$next_date = date("Ymd", $next_time);
		$next_year = date("Y", $next_time);
		$return .=  "<option value=\"year.php?cal=$cal&amp;getdate=$next_date\">$next_year</option>\n";
	}
	
	return $return;
}


function list_weeks() {
	global $getdate, $this_year, $cal, $dateFormat_week_jump, $week_start_day;
	ereg ("([0-9]{4})([0-9]{2})([0-9]{2})", $getdate, $day_array2);
	$this_day 			= $day_array2[3]; 
	$this_month 		= $day_array2[2];
	$this_year 			= $day_array2[1];
	$check_week 		= strtotime($getdate);
	$start_week_time 	= strtotime(dateOfWeek(date("Ymd", strtotime("$this_year-01-01")), $week_start_day));
	$end_week_time 		= $start_week_time + (6 * 25 * 60 * 60);
		
	do {
		$weekdate 		= date ("Ymd", $start_week_time);
		$select_week1 	= localizeDate($dateFormat_week_jump, $start_week_time);
		$select_week2 	= localizeDate($dateFormat_week_jump, $end_week_time);
	
		if (($check_week >= $start_week_time) && ($check_week <= $end_week_time)) {
			$return .= "<option value=\"week.php?cal=$cal&amp;getdate=$weekdate\" selected=\"selected\">$select_week1 - $select_week2</option>\n";
		} else {
			$return .= "<option value=\"week.php?cal=$cal&amp;getdate=$weekdate\">$select_week1 - $select_week2</option>\n";
		}
		$start_week_time =  strtotime ("+1 week", $start_week_time);
		$end_week_time = $start_week_time + (6 * 25 * 60 * 60);
	} while (date("Y", $start_week_time) <= $this_year);

	return $return;
}

function list_languages() {
	global $getdate, $cal, $current_view;
	$dir_handle = @opendir(BASE.'languages/');
	$tmp_pref_language = urlencode(ucfirst($language));
	while ($file = readdir($dir_handle)) {
		if (substr($file, -8) == ".inc.php") {
			$language_tmp = urlencode(ucfirst(substr($file, 0, -8)));
			if ($language_tmp == $tmp_pref_language) {
				$return .= "<option value=\"$current_view.php?chlang=$language_tmp\" selected=\"selected\">in $language_tmp</option>\n";
			} else {
				$return .= "<option value=\"$current_view.php?chlang=$language_tmp\">in $language_tmp</option>\n";
			}
		}
	}
	closedir($dir_handle);
	
	return $return;
}

$cal_file_ar = array_reverse(explode('/', $_SERVER['PHP_SELF']));
$cal_file_ar = explode(".php",$cal_file_ar[0]);
$cal_file = $cal_file_ar[0];

?>
