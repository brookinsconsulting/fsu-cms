<?php

// date_functions.php
// functions for returning or comparing dates

// not a date function, but I didn't know where to put it
// for backwards compatibility


if (phpversion() < '4.1') {
	function array_key_exists($key, $arr) {
		if (!is_array($arr)) return false;
		foreach (array_keys($arr) as $k) {
			if ("$k" == "$key") return true;
		}
		return false;
	}
}

// takes iCalendar 2 day format and makes it into 3 characters
// if $txt is true, it returns the 3 letters, otherwise it returns the
// integer of that day; 0=Sun, 1=Mon, etc.
function two2threeCharDays($day, $txt=true) {
	switch($day) {
		case 'SU': return ($txt ? 'sun' : '0');
		case 'MO': return ($txt ? 'mon' : '1');
		case 'TU': return ($txt ? 'tue' : '2');
		case 'WE': return ($txt ? 'wed' : '3');
		case 'TH': return ($txt ? 'thu' : '4');
		case 'FR': return ($txt ? 'fri' : '5');
		case 'SA': return ($txt ? 'sat' : '6');
	}
}

// dateOfWeek() takes a date in Ymd and a day of week in 3 letters or more
// and returns the date of that day. (ie: "sun" or "sunday" would be acceptable values of $day but not "su")
function dateOfWeek($Ymd, $day) {
	global $week_start_day;
	if (!isset($week_start_day)) $week_start_day = 'Sunday';
	$timestamp = strtotime($Ymd);
	$num = date('w', strtotime($week_start_day));
	$start_day_time = strtotime((date('w',$timestamp)==$num ? "$week_start_day" : "last $week_start_day"), $timestamp);
	$ret_unixtime = strtotime($day,$start_day_time);
	// Fix for 992744
	// $ret_unixtime = strtotime('+12 hours', $ret_unixtime);
	$ret_unixtime += (12 * 60 * 60);
	$ret = date('Ymd',$ret_unixtime);
	return $ret;
}

// function to compare to dates in Ymd and return the number of weeks 
// that differ between them. requires dateOfWeek()
function weekCompare($now, $then) {
	global $week_start_day;
	$sun_now = dateOfWeek($now, "Sunday");
	$sun_then = dateOfWeek($then, "Sunday");
	$seconds_now = strtotime($sun_now);
	$seconds_then =  strtotime($sun_then);
	$diff_weeks = round(($seconds_now - $seconds_then)/(60*60*24*7));
	return $diff_weeks;
}

// function to compare to dates in Ymd and return the number of days 
// that differ between them.
function dayCompare($now, $then) {
	$seconds_now = strtotime($now);
	$seconds_then =  strtotime($then);
	$diff_seconds = $seconds_now - $seconds_then;
	$diff_minutes = $diff_seconds/60;
	$diff_hours = $diff_minutes/60;
	$diff_days = round($diff_hours/24);
	
	return $diff_days;
}

// function to compare to dates in Ymd and return the number of months 
// that differ between them.
function monthCompare($now, $then) {
	ereg ("([0-9]{4})([0-9]{2})([0-9]{2})", $now, $date_now);
	ereg ("([0-9]{4})([0-9]{2})([0-9]{2})", $then, $date_then);
	$diff_years = $date_now[1] - $date_then[1];
	$diff_months = $date_now[2] - $date_then[2];
	if ($date_now[2] < $date_then[2]) {
		$diff_years -= 1;
		$diff_months = ($diff_months + 12) % 12;
	}
	$diff_months = ($diff_years * 12) + $diff_months;

	return $diff_months;
}

function yearCompare($now, $then) {
	ereg ("([0-9]{4})([0-9]{2})([0-9]{2})", $now, $date_now);
	ereg ("([0-9]{4})([0-9]{2})([0-9]{2})", $then, $date_then);
	$diff_years = $date_now[1] - $date_then[1];
	return $diff_years;
}

// localizeDate() - similar to strftime but uses our preset arrays of localized
// months and week days and only supports %A, %a, %B, %b, %e, and %Y
// more can be added as needed but trying to keep it small while we can
function localizeDate($format, $timestamp) {
	global $daysofweek_lang, $daysofweekshort_lang, $daysofweekreallyshort_lang, $monthsofyear_lang, $monthsofyear_lang, $monthsofyearshort_lang;
	$year = date("Y", $timestamp);
	$month = date("n", $timestamp)-1;
	$day = date("j", $timestamp);
	$dayofweek = date("w", $timestamp);
	
	$date = str_replace('%Y', $year, $format);
	$date = str_replace('%e', $day, $date);
	$date = str_replace('%B', $monthsofyear_lang[$month], $date);
	$date = str_replace('%b', $monthsofyearshort_lang[$month], $date);
	$date = str_replace('%A', $daysofweek_lang[$dayofweek], $date);
	$date = str_replace('%a', $daysofweekshort_lang[$dayofweek], $date);
	
	return $date;	
	
}
// calcOffset takes an offset (ie, -0500) and returns it in the number of seconds
function calcOffset($offset_str) {
	$sign = substr($offset_str, 0, 1);
	$hours = substr($offset_str, 1, 2);
	$mins = substr($offset_str, 3, 2);
	$secs = ((int)$hours * 3600) + ((int)$mins * 60);
	if ($sign == '-') $secs = 0 - $secs;
	return $secs;
}

// calcTime calculates the unixtime of a new offset by comparing it to the current offset
// $have is the current offset (ie, '-0500')
// $want is the wanted offset (ie, '-0700')
// $time is the unixtime relative to $have
function calcTime($have, $want, $time) {
	if ($have == 'none' || $want == 'none') return $time;
	$have_secs = calcOffset($have);
	$want_secs = calcOffset($want);
	$diff = $want_secs - $have_secs;
	$time += $diff;
	return $time;
}

function chooseOffset($time) {
	global $timezone, $tz_array;
	if (!isset($timezone)) $timezone = '';
	switch ($timezone) {
		case '':
			$offset = 'none';
			break;
		case 'Same as Server':
			$offset = date('O', $time);
			break;
		default:
			if (is_array($tz_array) && array_key_exists($timezone, $tz_array)) {
				$dlst = date('I', $time);
				$offset = $tz_array[$timezone][$dlst];
			} else {
				$offset = '+0000';
			}
	}
	return $offset;
}

function rgb2hsl($clrR, $clrG, $clrB){
     
    $clrMin = min($clrR, $clrG, $clrB);
    $clrMax = max($clrR, $clrG, $clrB);
    $deltaMax = $clrMax - $clrMin;
     
    $L = ($clrMax + $clrMin) / 510;
     
    if (0 == $deltaMax){
        $H = 0;
        $S = 0;
    }
    else{
        if (0.5 > $L){
            $S = $deltaMax / ($clrMax + $clrMin);
        }
        else{
            $S = $deltaMax / (510 - $clrMax - $clrMin);
        }

        if ($clrMax == $clrR) {
            $H = ($clrG - $clrB) / (6.0 * $deltaMax);
        }
        else if ($clrMax == $clrG) {
            $H = 1/3 + ($clrB - $clrR) / (6.0 * $deltaMax);
        }
        else {
            $H = 2 / 3 + ($clrR - $clrG) / (6.0 * $deltaMax);
        }

        if (0 > $H) $H += 1;
        if (1 < $H) $H -= 1;
    }
    return array($H, $S,$L);
}

function hsl2rgb($h, $s, $l) { 

	$m2 = ($l<=0.5) ? $m2 = $l*($s+1) : $l+$s-($l*$s); 

	$m1 = $l*2 - $m2;
	$r = hue($m1, $m2, ($h+1/3));
	$g = hue($m1, $m2, $h);
	$b = hue($m1, $m2, ($h-1/3));

	$h = sprintf('%02x%02x%02x', round(255*$r), round(255*$g), round(255*$b) );
	$hex3 = $h{0}.$h{2}.$h{4};
	$out = ($hex3 == $h{1}.$h{3}.$h{5}) ? '#'.$hex3 : '#'.$h;
	
	return (strlen($out) == 4) ? '#'.$out[1].$out[1].$out[2].$out[2].$out[3].$out[3] : $out; 
}

function hue($m1, $m2, $h) {
	if($h<0) ++$h;
	if($h>1) --$h;

	if($h < 1/6) return $m1+($m2-$m1)*6*$h;
	if($h < 1/2) return $m2;
	if($h < 2/3) return $m1+($m2-$m1)*6*(2/3-$h);
	return $m1;
}


function changeBrightness ( $hex, $lightness )
{
    $red   = hexdec( $hex[0] . $hex[1] );
    $green = hexdec( $hex[2] . $hex[3] );
    $blue  = hexdec( $hex[4] . $hex[5] );

    $hsl = rgb2hsl($red, $green, $blue);

    $rgb = hsl2rgb($hsl[0], $hsl[1], $lightness);
	return $rgb;

}

function intodate($i) {
	$hr = $i[0] . $i[1];
	$min = $i[2] . $i[3];
	$hr = $hr + 0;
	$am = 'AM';
	if ($hr > 12) {
		$am = 'PM';
		$hr = $hr - 12;
	}
	$myout = $hr . ":" . $min . " " . $am;
	return $myout;
}

function openevent($event_date, $time, $uid, $arr, $lines = 0, $length = 0, $link_class = '', $pre_text = '', $post_text = '') {
	global $cpath, $master_array, $unique_colors, $coldata;	

	$event_text = stripslashes(urldecode($arr["event_text"]));
	if ($event_text=='') $event_text = stripslashes(urldecode($arr["location"]));
	if (empty($start)) {
		$title = $event_text;
	} else {
		$title = $arr['event_start'].' - '.$arr['event_end'].': '.$event_text;
	}
	
	# for iCal pseudo tag <http> comptability
	if (ereg("<([[:alpha:]]+://)([^<>[:space:]]+)>",$event_text,$matches)) {
		$full_event_text = $matches[1] . $matches[2];
		$event_text = $matches[2];
	} else {
		$full_event_text = $event_text;
		$event_text = strip_tags($event_text, '<b><i><u><img>');
	}

	if (!empty($event_text)) {
		if ($lines > 0) {
			$event_text = word_wrap($event_text, $length, $lines);
		}

		if ((!(ereg("([[:alpha:]]+://[^<>[:space:]]+)", $full_event_text, $res))) || ($arr['description'])) {

			$event_calno 	= $arr['calnumber'];
			$event_calno	= (($event_calno - 1) % $unique_colors) + 1;
			
			$bgc = "#3E85B6";
			
			if (array_key_exists($event_calno, $coldata)) {
				$bgc = $coldata[$event_calno];
			}
			
			$bgcl = changeBrightness ( trim($bgc,"#"), .9 );
			
			$cal_name = $arr['calname'];
			$cal_class_name = str_replace(" ","_", $cal_name);
            $my_event_description = urldecode($arr['event_text']);

			if ($arr['event_start'] == "" || ($arr['event_start'] == "0000" && $arr['event_end'] == "2400")) {
				$my_event_start ="All day";
			} else {
				$my_event_start = intodate($arr['event_start']);
			}
			$caption = "Start time: ".$my_event_start."<br/>Location: ".$arr['location'];

			$arr['organizer'] = unserialize($arr['organizer']);	
						
			$event_data = addslashes(json_encode($arr));
			
			$event_data = str_replace('\\\\\"', 'qqq', $event_data);
			
			$event_data = str_replace("'", '%27', $event_data); 
			
			// fix for URL-length bug in IE: populate and submit a hidden form on click
			$return = "";
			
			$caption = str_replace("'", "`",$caption);
			
			$my_event_description = str_replace("'", "`",$my_event_description);

			$return .= "<a class='$link_class' onmouseover='return overlib(\"".$caption."\", LEFT, FGCOLOR, \"".$bgcl."\", BGCOLOR, \"".$bgc."\", CAPTION, \"".$my_event_description."\", TEXTSIZE, \"12px\");' onmouseout='return nd();' onclick='nd_quiet(); window.eventData = new EventData(\"$event_date\",\"$my_event_start\",\"$uid\",\"$cpath\",\"$event_data\");var el=this;var t = el.title || el.name || null;var a=\"/extension/phpicalendar/includes/event.php?cal_name=$cal_name&KeepThis=true&TB_iframe=true&height=425&width=460\";var g = el.rel || false;tb_show(t,a,g);el.blur();return false;' title=".chr(34).$title.chr(34)." href='#'>";
		} else {
			$return .= '<a class="'.$link_class.'" title="'.$title.'" href="'.$res[1].'">';
		}
		$return .= $event_text.$post_text.'</a>'."\n";
	}

	return $return;
}

function openeventfull($event_date, $time, $uid, $arr) {

	global $cpath, $master_array;	
	$link_class='custom';
	$event_text = stripslashes(urldecode($arr["event_text"]));
	if ($event_text=='') $event_text = stripslashes(urldecode($arr["location"]));

	$title = $event_text;

	$full_event_text = $event_text;

	if (!empty($event_text)) {
		if ((!(ereg("([[:alpha:]]+://[^<>[:space:]]+)", $full_event_text, $res))) || ($arr['description'])) {
			
			$cal_name = $arr['calname'];
		
			$event_data = addslashes(json_encode($arr));
			
			$event_data = str_replace('\\\\\"', 'qqq', $event_data);
			
			$event_data = str_replace("'", '%27', $event_data); 

			// fix for URL-length bug in IE: populate and submit a hidden form on click
			$return = "";
			$return .= "<a class='$link_class' onclick='window.eventData = new EventData(\"$event_date\",\"$my_event_start\",\"$uid\",\"$cpath\",\"$event_data\");var el=this;var t = el.title || el.name || null;var a=\"/extension/phpicalendar/includes/event.php?cal_name=$cal_name&KeepThis=true&TB_iframe=true&height=425&width=460\";var g = el.rel || false;tb_show(t,a,g);el.blur();return false;' title='$title' href='#'>";
		} else {
			$return .= '<a class="'.$link_class.'" title="'.$title.'" href="'.$res[1].'">';
		}
		$return .= $pre_text.$event_text.$post_text.'</a>'."\n";
	}
	return $return;
}

// Returns an array of the date and time extracted from the data
// passed in. This array contains (unixtime, date, time, allday).
//
// $data		= A string representing a date-time per RFC2445.
// $property	= The property being examined, e.g. DTSTART, DTEND.
// $field		= The full field being examined, e.g. DTSTART;TZID=US/Pacific
function extractDateTime($data, $property, $field) {
	global $tz_array;
	
	// Initialize values.
	unset($unixtime, $date, $time, $allday);
	
	$allday =''; #suppress error on returning undef.
	// Check for zulu time.
	$zulu_time = false;
	if (substr($data,-1) == 'Z') $zulu_time = true;
	$data = str_replace('Z', '', $data);
	
	// Remove some substrings we don't want to look at.
	$data = str_replace('T', '', $data);
	$field = str_replace(';VALUE=DATE-TIME', '', $field); 
	
	// Extract date-only values.
	if ((preg_match('/^'.$property.';VALUE=DATE/i', $field)) || (ereg ('^([0-9]{4})([0-9]{2})([0-9]{2})$', $data)))  {
		// Pull out the date value. Minimum year is 1970.
		ereg ('([0-9]{4})([0-9]{2})([0-9]{2})', $data, $dt_check);
		if ($dt_check[1] < 1970) { 
			$data = '1971'.$dt_check[2].$dt_check[3];
		}
		
		// Set the values.
		$unixtime = strtotime($data);
		$date = date('Ymd', $unixtime);
		$allday = $data;
	}
	
	// Extract date-time values.
	else {
		// Pull out the timezone, or use GMT if zulu time was indicated.
		if (preg_match('/^'.$property.';TZID=/i', $field)) {
			$tz_tmp = explode('=', $field);
			$tz_dt = parse_tz($tz_tmp[1]);
			unset($tz_tmp);
		} elseif ($zulu_time) {
			$tz_dt = 'GMT';
		}

		// Pull out the date and time values. Minimum year is 1970.
		preg_match ('/([0-9]{4})([0-9]{2})([0-9]{2})([0-9]{0,2})([0-9]{0,2})/', $data, $regs);
		if ($regs[1] < 1970) { 
			$regs[1] = '1971';
		}
		$date = $regs[1] . $regs[2] . $regs[3];
		$time = $regs[4] . $regs[5];
		$unixtime = mktime($regs[4], $regs[5], 0, $regs[2], $regs[3], $regs[1]);

		// Check for daylight savings time.
		$dlst = date('I', $unixtime);
		$server_offset_tmp = chooseOffset($unixtime);
		if (isset($tz_dt)) {
			if (array_key_exists($tz_dt, $tz_array)) {
				$offset_tmp = $tz_array[$tz_dt][$dlst];
			} else {
				$offset_tmp = '+0000';
			}
		} elseif (isset($calendar_tz)) {
			if (array_key_exists($calendar_tz, $tz_array)) {
				$offset_tmp = $tz_array[$calendar_tz][$dlst];
			} else {
				$offset_tmp = '+0000';
			}
		} else {
			$offset_tmp = $server_offset_tmp;
		}
		
		// Set the values.
		$unixtime = calcTime($offset_tmp, $server_offset_tmp, $unixtime);
		$date = date('Ymd', $unixtime);
		$time = date('Hi', $unixtime);
	}
	
	// Return the results.
	return array($unixtime, $date, $time, $allday);
}

//TZIDs in calendars often contain leading information that should be stripped
//Example: TZID=/mozilla.org/20050126_1/Europe/Berlin
//Need to return the last part only
function parse_tz($data){
	$fields = explode("/",$data);
	$tz = array_pop($fields);
	$tmp = array_pop($fields);
	if (isset($tmp) && $tmp != "") $tz = "$tmp/$tz";
	return $tz;
}


?>

