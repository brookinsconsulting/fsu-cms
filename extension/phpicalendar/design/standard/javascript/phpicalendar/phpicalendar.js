if (typeof jQuery.fn.pagination != 'function') {
	dhtmlLoadScript('/extension/phpicalendar/design/standard/javascript/phpicalendar/jquery.pagination.js');
	dhtmlLoadLink('/extension/phpicalendar/design/standard/stylesheets/pagination.css');
}

if (typeof jQuery.fn.setValue != 'function') {
	dhtmlLoadScript('/extension/phpicalendar/design/standard/javascript/phpicalendar/jquery.field.min.js');
}


if (typeof tb_show != 'function') {
	dhtmlLoadScript('/extension/phpicalendar/design/standard/javascript/phpicalendar/thickbox-compressed.js');
	dhtmlLoadLink('/extension/phpicalendar/design/standard/stylesheets/thickbox.css');
	var tb_pathToImage = "/extension/phpicalendar/design/standard/images/loadingAnimation.gif";
}

if (typeof YAHOO != 'function') {
	dhtmlLoadScript('/extension/phpicalendar/design/standard/javascript/yui/build/yahoo-dom-event/yahoo-dom-event.js');
	dhtmlLoadScript('/extension/phpicalendar/design/standard/javascript/yui/build/calendar/calendar-min.js');
	dhtmlLoadLink('/extension/phpicalendar/design/standard/stylesheets/yui/build/calendar/assets/calendar.css');
}

if (typeof getElementsByClass != 'function') {
	var getElementsByClass=function(node,searchClass,tag) {
	  var classElements = new Array();
	  var els = node.getElementsByTagName(tag); // use "*" for all elements
	  var elsLen = els.length;
	  var pattern = new RegExp("\\b"+searchClass+"\\b");
	  for (i = 0, j = 0; i < elsLen; i++) {
	    if ( pattern.test(els[i].className) ) {
	      classElements[j] = els[i];
	      j++;
	    }
	  }
	  return classElements;
	}
}

function dhtmlLoadScript(url)
{
   var e = document.createElement("script");
   e.src = url;
   e.type="text/javascript";
   document.getElementsByTagName("head")[0].appendChild(e); 
}

function dhtmlLoadLink(url)
{
   var e = document.createElement("link");
   e.href = url;
   e.type="text/css";
   e.rel = 'stylesheet';
   e.media = 'screen';
   document.getElementsByTagName("head")[0].appendChild(e); 
}

var req; 
var ajax_url='/phpicalendar/ezdisplayicals/';
var israwload = true;


function trim (str, charlist) {
    // http://kevin.vanzonneveld.net
    // +   original by: Kevin van Zonneveld (http://kevin.vanzonneveld.net)
    // +   improved by: mdsjack (http://www.mdsjack.bo.it)
    // +   improved by: Alexander Ermolaev (http://snippets.dzone.com/user/AlexanderErmolaev)
    // +      input by: Erkekjetter
    // +   improved by: Kevin van Zonneveld (http://kevin.vanzonneveld.net)
    // +      input by: DxGx
    // +   improved by: Steven Levithan (http://blog.stevenlevithan.com)
    // +    tweaked by: Jack
    // +   bugfixed by: Onno Marsman
    // *     example 1: trim('    Kevin van Zonneveld    ');
    // *     returns 1: 'Kevin van Zonneveld'
    // *     example 2: trim('Hello World', 'Hdle');
    // *     returns 2: 'o Wor'
    // *     example 3: trim(16, 1);
    // *     returns 3: 6

    var whitespace, l = 0, i = 0;
    str += '';

    if (!charlist) {
        // default list
        whitespace = " \n\r\t\f\x0b\xa0\u2000\u2001\u2002\u2003\u2004\u2005\u2006\u2007\u2008\u2009\u200a\u200b\u2028\u2029\u3000";
    } else {
        // preg_quote custom list
        charlist += '';
        whitespace = charlist.replace(/([\[\]\(\)\.\?\/\*\{\}\+\$\^\:])/g, '\$1');
    }

    l = str.length;
    for (i = 0; i < l; i++) {
        if (whitespace.indexOf(str.charAt(i)) === -1) {
            str = str.substring(i);
            break;
        }
    }

    l = str.length;
    for (i = l - 1; i >= 0; i--) {
        if (whitespace.indexOf(str.charAt(i)) === -1) {
            str = str.substring(0, i + 1);
            break;
        }
    }

    return whitespace.indexOf(str.charAt(0)) === -1 ? str : '';
}

$(document).ready(function() {
	dhtmlHistory.initialize();
	dhtmlHistory.addListener(history_change);
});

function history_change(newLocation, historyData) {
	if (historyData && israwload == false) {
		decoded = base64_decode(historyData, true);
		if (decoded.indexOf('/') == 0) return false;
		get_callendar_xml(decoded, true);
	}
}

function ajax_history_processor(ajaxProcessor) {
	var ajaxHash = base64_encode(ajaxProcessor);
	var ajaxHistoryData = base64_encode(ajaxProcessor);
	dhtmlHistory.add(ajaxHash, ajaxHistoryData);
}

function get_callendar_xml(addme, isHistoryChange) {
	
	var uri_r = (document.location + "").split('#');
	if (israwload && uri_r.length > 1) {
		addme = base64_decode(uri_r[1]);
	}
	
	israwload = false;
	
	if (!isHistoryChange || isHistoryChange == 'undefined') {
		ajax_history_processor(addme);
       }

    // branch for native XMLHttpRequest object
    if (window.XMLHttpRequest) {
        req = new XMLHttpRequest();
        req.onreadystatechange = processReqChange;
        req.open("GET", ajax_url+addme, true);
        req.send(null);
    // branch for IE/Windows ActiveX version
    } else if (window.ActiveXObject) {
        req = new ActiveXObject("Microsoft.XMLHTTP");
        if (req) {
            req.onreadystatechange = processReqChange;
            req.open("GET", ajax_url+addme, true);
            req.send();
        }
    }
    if (document.getElementById('loading')) document.getElementById('loading').innerHTML="<img src='/extension/phpicalendar/images/ajax-loader.gif'/>";
}

function processReqChange() 
{
    // only if req shows "complete"

    if (req.readyState == 4) {
        // only if "OK"
        if (req.status == 200) {
	     var calhtml = document.getElementById('calendar').innerHTML;
	     document.getElementById('calendar').innerHTML = req.responseText;
	     if (typeof sifr_stuff == 'function') sifr_stuff();
		 reset_toggles();
		 if ($("#News-Pagination")) add_pagination();
 	 } else if(req.status == 404) {
	   // Add a custom message or redirect the user to another page
	     document.getElementById('calendar').innerHTML = "File not found";
        } else {
            alert("There was a problem retrieving the XML data:\n" + req.statusText);
        }
    }
}


var members = 0;
var page_index = 0;

function handlePaginationClick(new_page_index, pagination_container) {
	page_index = new_page_index;
    var max_elem = Math.min((page_index+1) * 10, members.length);
    var newcontent = $('#News-Result').get(0);
  
 	newcontent.innerHTML = '';
    // Iterate through a selection of the content and build an HTML string
    for(var i=page_index*10;i<max_elem;i++)
    {
        newcontent.appendChild( members[i] );
    }
    
    // Prevent click eventpropagation
    return false;
}

function add_pagination() {
	members = $('.upcoming');
	$("#News-Pagination").pagination(members.length, {
		items_per_page:10, 
		callback:handlePaginationClick
	});
}


function EventData(date, time, uid, cpath, event_data) {
	this.date = date;
	this.time = time;
	this.uid = uid;
	this.cpath = cpath;
	this.event_data = event_data;
}

function togglecals() {
	var cal = togglecals.arguments[0];
	var val = togglecals.arguments[1];
	

	var sess_string = "";
	var mycals = getElementsByClass(document, "caltoggle","INPUT"); 
	var allclear = true;
	var master = document.getElementById('caltog_999');
	for (tt=0;tt<mycals.length;tt++) {
		if (mycals[tt].checked) 
		{
			allclear = false;
			break;
		}
	}
	if (allclear) master.checked = true;
	for (tt=0;tt<mycals.length;tt++) {
		if (mycals[tt].id != 'caltog_999' && val != 0 && cal == 999) mycals[tt].checked = 0;
		if (mycals[tt].id == 'caltog_999' && val != 0 && cal != 999) mycals[tt].checked = 0;
		var myevs = getElementsByClass(document, "cal_"+tt,"DIV"); 
		for (evc=0;evc<myevs.length;evc++) {
			myevs[evc].style.display = (mycals[tt].checked || master.checked) ? 'block' : 'none';
		}
		sess_string += mycals[tt].id + " : " + mycals[tt].checked + ", ";
	}
	if (togglecals.arguments[2] == undefined) {
		sess_string = "sessvars.toggleObj = {" + trim(sess_string, "'") + "}";
		eval(sess_string);
	}
}

function reset_toggles() {
	if (sessvars.toggleObj) {
		for (toggle in sessvars.toggleObj) {

			thistog = document.getElementById(toggle);
			
			if (thistog.checked != sessvars.toggleObj[toggle]) {
				thistog.checked = sessvars.toggleObj[toggle];
				togglecals(toggle.split('_')[1], sessvars.toggleObj[toggle], 'noreset');
			}
		}
	}
}

function toggle_majors(n) {
	document.getElementById("view_majors").style.color = (n) ? "#cccccc" : "#ffffff";
	document.getElementById("close_majors").style.color = (n) ? "#ffffff" : "#cccccc";
	document.getElementById('cal_month_majors').style.height = (n) ? 'auto' : '40px';
}

function showSimpleDatePicker( id, target ) {
	var calIconID = 'ev_fi_icon' + id;
    var calContainer = document.getElementById( "cal1Container" );

    var xy = YAHOO.util.Dom.getXY( calIconID );
    calContainer.style.position = 'absolute';
    calContainer.style.left = '470px';
    calContainer.style.top = ( xy[1] -250 ) + 'px';
    calContainer.style.display = 'block';
    calContainer.style.zIndex = '2000';

    window['cal'+id] = new YAHOO.widget.Calendar( target + '_' + id, "cal1Container", { close: true, 
                                                                                              mindate: "1/1/1970",
                                                                                              LOCALE_WEEKDAYS: "medium" } );
    window['cal'+id].render();
    window['cal'+id].selectEvent.subscribe( handleSimpleSelect, window['cal'+id], true );
}

function handleSimpleSelect(type,args,obj) {
    var dates = args[0];
    var date = dates[0];
    var year = date[0], month = date[1], day = date[2];

    var objIDArray = obj.id.split( '_' );
    var id = objIDArray[2];
	var target = objIDArray[0] + "_" + objIDArray[1];
	$('#'+target).setValue(month+'/'+day+'/'+year);
    window['cal'+id].hide();
}