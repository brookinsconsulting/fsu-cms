{set-block scope=global variable=cache_ttl}0{/set-block}

<script language="javascript" type="text/javascript" src={"javascript/phpicalendar/base64.js"|ezdesign}></script>
<script language="javascript" type="text/javascript" src={"javascript/phpicalendar/json2007.js"|ezdesign}></script>
<script language="javascript" type="text/javascript" src={"javascript/phpicalendar/overlib.js"|ezdesign}></script>
<script language="javascript" type="text/javascript" src={"javascript/phpicalendar/rsh.js"|ezdesign}></script>
<script language="javascript" type="text/javascript" src={"javascript/phpicalendar/phpicalendar.js"|ezdesign}></script>
<script language="javascript" type="text/javascript" src={"javascript/phpicalendar/sessvars.js"|ezdesign}></script>

{def 	$promote_events = ezini( 'GeneralSettings', 'PromoteEvents', 'ezphpicalendar.ini' )}

<div id='cal_outer_wrapper'>

{if eq('enabled', $promote_events)}
<form name='promote_event' id='promote_event' method="post" action="/content/action/">
    <a id='promote_event_link' href="javascript: document.promote_event.submit()">Submit an event</a>
    <input type="hidden" name="ContentNodeID" value="{$node.node_id}" />
    <input type="hidden" name="ContentObjectID" value="{$node.contentobject_id.}" />
    <input type="hidden" name="ContentLanguageCode" value="{ezini( 'RegionalSettings', 'Locale', 'site.ini')}" />
    <input type="hidden" name="NodeID" value="{$node.node_id}" />
    <input type="hidden" name="ClassIdentifier" value="event" />
    <input type="hidden" name="NewButton" value="pressed" />
</form>
{/if}

{def $siteNodeId = first_set(ezini('NodeSettings', 'RootNode', 'content.ini'), 2)
     $othercals = fetch(content, tree, hash(parent_node_id, $siteNodeId, class_filter_type, 'include', class_filter_array, array('event_calendar'), main_node_only, true(), sort_by, array('name', true())))
     $othermultis = fetch(content, tree, hash(parent_node_id, $siteNodeId, class_filter_type, 'include', class_filter_array, array('multicalendar'), main_node_only, true(), sort_by, array('name', true())))}

{if or($othercals|count|gt(1), $othermultis|count)}
<div id='jumptocal'>Jump to another calendar: <select id = 'cal_jump' name = 'cal_jump' onchange="if (this.selectedIndex) {ldelim}javascrpt: window.document.location.href=this.options[this.selectedIndex].value;{rdelim}">
	<option value="-- Select a Calendar --"></option>
	{foreach $othermultis as $thiscal}
		<option value={$thiscal.url_alias|ezroot}>{$thiscal.name|wash}</option>
	{/foreach}
	<optgroup label="-----------">
	{foreach $othercals as $thiscal}
		{if eq($node.node_id, $thiscal.node_id)}{skip}{/if}
		<option value={$thiscal.url_alias|ezroot}>{$thiscal.name|wash}</option>
	{/foreach}
	</optgroup>
</select></div>
{/if}

<h1>{$node.name|wash}</h1>

        {if and(is_set($node.object.data_map.description),$node.object.data_map.description.has_content)}
            <div class="attribute-long">
                {attribute_view_gui attribute=$node.object.data_map.description}
            </div>
        {/if}

<div id='calendar'>
<div id='cal_loading'><p>Loading...</p></div>
</div>

{def 	$cal_class_ids = ezini( 'ClassSettings', 'CalClassIds', 'ezphpicalendar.ini' )
		$sub_cals=fetch(content, tree, hash(parent_node_id, $node.node_id, class_filter_type, 'include', class_filter_array, $cal_class_ids))
		$cal_name_ar = array(concat('cal_node',$node.node_id))
		$cal_names = '?cal=0'}
		
		
{foreach $sub_cals as $this_cal}
	{set cal_name_ar = $cal_name_ar|append(concat('cal_node',$this_cal.node_id))}
{/foreach}

{if $cal_name_ar|count}
	{set cal_names = $cal_name_ar|implode(',')}
{/if}

<script language="javascript" type="text/javascript">

$(document).ready(function() {ldelim}
    get_callendar_xml('month.php?cal={$cal_names}');
{rdelim});

</script>

</div>

