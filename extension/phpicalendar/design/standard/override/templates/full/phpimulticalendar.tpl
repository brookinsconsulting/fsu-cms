<script language="javascript" type="text/javascript" src={"javascript/phpicalendar/base64.js"|ezdesign}></script>
<script language="javascript" type="text/javascript" src={"javascript/phpicalendar/json2007.js"|ezdesign}></script>
<script language="javascript" type="text/javascript" src={"javascript/phpicalendar/overlib.js"|ezdesign}></script>
<script language="javascript" type="text/javascript" src={"javascript/phpicalendar/rsh.js"|ezdesign}></script>
<script language="javascript" type="text/javascript" src={"javascript/phpicalendar/phpicalendar.js"|ezdesign}></script>
<script language="javascript" type="text/javascript" src={"javascript/phpicalendar/sessvars.js"|ezdesign}></script>

{def 	$promote_events = ezini( 'GeneralSettings', 'PromoteEvents', 'ezphpicalendar.ini' )}

<div id='cal_outer_wrapper' class="module contentbox standard-radius">

{def $othercals = fetch(content, list, hash(parent_node_id, $node.node_id, class_filter_type, 'include', class_filter_array, array('event_calendar'), main_node_only, true(), sort_by, array('name', true())))}

{if eq('enabled', $promote_events)}
<div id="widget-bar" class="module standard-top-radius">
<form name='promote_event' autocomplete = 'off' id='promote_event' method="post" action="/content/action/">
    <span id='promote_event_link'>Submit an event: <select id = 'promo_select' name = 'promo_select' onchange="javascript: if (this.selectedIndex) {ldelim}var stuff = this.options[this.selectedIndex].value.split('/'); document.promote_event.ContentNodeID.value = stuff[0]; document.promote_event.ContentObjectID.value = stuff[1]; document.promote_event.NodeID.value = stuff[0]; document.promote_event.submit();{rdelim}">
	<option value = '-999' >-- Select a Category --</option>
    {foreach $othercals as $thiscal}
		<option value="{$thiscal.node_id}/{$thiscal.contentobject_id}">{$thiscal.name|wash}</option>
	{/foreach}
	</select></span>
    <input type="hidden" name="ContentNodeID" value="" />
    <input type="hidden" name="ContentObjectID" value="" />
    <input type="hidden" name="ContentLanguageCode" value="{ezini( 'RegionalSettings', 'Locale', 'site.ini')}" />
    <input type="hidden" name="NodeID" value="" />
    <input type="hidden" name="ClassIdentifier" value="event" />
    <input type="hidden" name="NewButton" value="pressed" />
    <input type="hidden" name="RedirectURIAfterPublish" value="{$node.url_alias|ezroot(no)}" />

</form>
</div>
{/if}
<div id="cal_inner_wrapper">
<h1>{$node.name|wash}</h1>

        {if and(is_set($node.object.data_map.description),$node.object.data_map.description.has_content)}
            <div class="attribute-long">
                {attribute_view_gui attribute=$node.object.data_map.description}
            </div>
        {/if}
<hr />
<div id='calendar'>
<div id='cal_loading'><p>Loading...</p></div>
</div>

{def 	$cal_class_ids = ezini( 'ClassSettings', 'CalClassIds', 'ezphpicalendar.ini' )
		$sub_cals=fetch(content, tree, hash(parent_node_id, $node.node_id, class_filter_type, 'include', class_filter_array, $cal_class_ids))
		$cal_name_ar = array()
		$cal_names = '?cal=0'}
		
		
{foreach $sub_cals as $this_cal}
	{set cal_name_ar = $cal_name_ar|append(concat('cal_node',$this_cal.node_id))}
{/foreach}

{if $cal_name_ar|count}
	{set cal_names = $cal_name_ar|implode(',')}
{/if}

<script language="javascript" type="text/javascript">

$(document).ready(function() {ldelim}
    get_callendar_xml('month.php?cal={$cal_names},multi');
{rdelim});

</script>

</div>
</div>
