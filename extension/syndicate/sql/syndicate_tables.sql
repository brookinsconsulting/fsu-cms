-- phpMyAdmin SQL Dump
-- version 2.6.4-pl1-Debian-1ubuntu1.1
-- http://www.phpmyadmin.net
-- 
-- Host: localhost
-- Erstellungszeit: 08. März 2006 um 10:45
-- Server Version: 4.1.12
-- PHP-Version: 4.4.0-3ubuntu1
-- 
-- Datenbank: `virus`
-- 

-- --------------------------------------------------------

-- 
-- Tabellenstruktur für Tabelle `ymcrss_export`
-- 

CREATE TABLE `ymcrss_export` (
  `access_url` varchar(255) default NULL,
  `active` int(11) default NULL,
  `created` int(11) default NULL,
  `creator_id` int(11) default NULL,
  `description` longtext,
  `id` int(11) NOT NULL auto_increment,
  `image_id` int(11) default NULL,
  `modified` int(11) default NULL,
  `modifier_id` int(11) default NULL,
  `rss_version` varchar(255) default NULL,
  `site_access` varchar(255) default NULL,
  `status` int(11) NOT NULL default '0',
  `title` varchar(255) default NULL,
  `number_of_objects` int(11) unsigned NOT NULL default '0',
  `main_node_only` int(11) unsigned NOT NULL default '1',
  PRIMARY KEY  (`id`,`status`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

-- 
-- Tabellenstruktur für Tabelle `ymcrss_export_item`
-- 

CREATE TABLE `ymcrss_export_item` (
  `class_id` int(11) default NULL,
  `description` varchar(255) default NULL,
  `id` int(11) NOT NULL auto_increment,
  `rssexport_id` int(11) default NULL,
  `source_node_id` int(11) default NULL,
  `status` int(11) NOT NULL default '0',
  `title` varchar(255) default NULL,
  `subnodes` int(11) unsigned NOT NULL default '0',
  `enclosure` varchar(255) default NULL,
  PRIMARY KEY  (`id`,`status`),
  KEY `ezrss_export_rsseid` (`rssexport_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
