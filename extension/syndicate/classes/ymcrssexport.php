<?php

define( "EZ_RSSEXPORT_STATUS_VALID", 1 );
define( "EZ_RSSEXPORT_STATUS_DRAFT", 0 );

class ymcRSSExport extends eZPersistentObject
{
	
    function ymcRSSExport( $row )
    {
        $this->eZPersistentObject( $row );
    }

    static function definition()
    {
        return array( "fields" => array( "id" => array( 'name' => 'ID',
                                                        'datatype' => 'integer',
                                                        'default' => 0,
                                                        'required' => true ),
                                         'title' => array( 'name' => 'Title',
                                                           'datatype' => 'string',
                                                           'default' => ezi18n( 'syndicate', 'New RSS Export' ),
                                                           'required' => true ),
                                         'site_access' => array( 'name' => 'SiteAccess',
                                                                 'datatype' => 'string',
                                                                 'default' => '',
                                                                 'required' => true ),
                                         'modified' => array( 'name' => 'Modified',
                                                              'datatype' => 'integer',
                                                              'default' => 0,
                                                              'required' => true ),
                                         'modifier_id' => array( 'name' => 'ModifierID',
                                                              'datatype' => 'integer',
                                                              'default' => 0,
                                                              'required' => true ),
                                         'created' => array( 'name' => 'Created',
                                                              'datatype' => 'integer',
                                                              'default' => 0,
                                                              'required' => true ),
                                         'creator_id' => array( 'name' => 'CreatorID',
                                                              'datatype' => 'integer',
                                                              'default' => 0,
                                                              'required' => true ),
                                         'description' => array( 'name' => 'Description',
                                                                 'datatype' => 'string',
                                                                 'default' => '',
                                                                 'required' => false ),
                                         'image_id' => array( 'name' => 'ImageID',
                                                              'datatype' => 'integer',
                                                              'default' => 0,
                                                              'required' => false ),
                                         'rss_version' => array( 'name' => 'RSSVersion',
                                                                 'datatype' => 'string',
                                                                 'default' => 0,
                                                                 'required' => true ),
                                         'active' => array( 'name' => 'Active',
                                                            'datatype' => 'integer',
                                                            'default' => 0,
                                                            'required' => true ),
                                         'status' => array( 'name' => 'Status',
                                                            'datatype' => 'integer',
                                                            'default' => 0,
                                                            'required' => true ),
                                         'access_url' => array( 'name' => 'AccessURL',
                                                                'datatype' => 'string',
                                                                'default' => 'rss_feed',
                                                                'required' => false ),
                                         'number_of_objects' => array( 'name' => 'NumberOfObjects',
                                                            'datatype' => 'integer',
                                                            'default' => 0,
                                                            'required' => true ),
                                         'main_node_only' => array( 'name' => 'MainNodeOnly',
                                                            'datatype' => 'integer',
                                                            'default' => 0,
                                                            'required' => true ) ),
                      "keys" => array( "id", 'status' ),
                      "increment_key" => "id",
                      "sort" => array( "title" => "asc" ),
                      "class_name" => "ymcRSSExport",
                      "name" => "ymcrss_export" );

    }


	    /*!
	     \static
	     Creates a new RSS Export with the new RSS Export
	     \param User ID

	     \return the URL alias object
	    */
	    static function create( $user_id )
	    {
	        //getting Site URL

	        //getting Syndicate-Settings
	        $SyndicateConfig = eZINI::instance( 'syndicate.ini' );
	        //getting User
	        include_once( "kernel/classes/datatypes/ezuser/ezuser.php" );
	        $user = eZUser::fetch($user_id);


	        $dateTime = time();
	        $row = array( 'id' => null,
	                      'title' => ezi18n( 'kernel/classes', 'New RSS Export' ),
	                      'site_access' => $SyndicateConfig->variable( 'SyndicateSettings', 'DefaultSiteAccess' ),
	                      'modifier_id' => $user_id,
	                      'modified' => $dateTime,
	                      'creator_id' => $user_id,
	                      'created' => $dateTime,
	                      'status' => 0,
	                      'description' => '',
	                      'image_id' => 0,
	                      'active' => 1,
	                      'rss_version' => $SyndicateConfig->variable( 'SyndicateSettings', 'DefaultVersion' ),
	                      'number_of_objects' => $SyndicateConfig->variable( 'SyndicateSettings', 'NumberOfObjectsDefault' ),
	                      'access_url' => $user->attribute('login') );
	        return new ymcRSSExport( $row );
	    }

	    /*!
	     Store Object to database
	     \note Transaction unsafe. If you call several transaction unsafe methods you must enclose
	     the calls within a db transaction; thus within db->begin and db->commit.
	    */
	    function store( $storeAsValid = false )
	    {
	        include_once( "kernel/classes/datatypes/ezuser/ezuser.php" );
	        $dateTime = time();
	        $user = eZUser::currentUser();
	        if (  $this->ID == null )
	        {
	            eZPersistentObject::store();
	            return;
	        }

	        $db = eZDB::instance();
	        $db->begin();
	        if ( $storeAsValid )
	        {
	            $oldStatus = $this->attribute( 'status' );
	            $this->setAttribute( 'status', EZ_RSSEXPORT_STATUS_VALID );
	        }
	        $this->setAttribute( 'modified', $dateTime );
	        $this->setAttribute( 'modifier_id', $user->attribute( "contentobject_id" ) );
	        eZPersistentObject::store();
	        $db->commit();
	        if ( $storeAsValid )
	        {
	            $this->setAttribute( 'status', $oldStatus );
	        }
	    }

	    /*!
	     Remove the RSS Export.
	     \note Transaction unsafe. If you call several transaction unsafe methods you must enclose
	     the calls within a db transaction; thus within db->begin and db->commit.
	    */
	    function remove($conditions = null, $extraConditions = null)
	    {
	        $exportItems = $this->fetchItems();

	        $db = eZDB::instance();
	        $db->begin();
	        foreach ( $exportItems as $item )
	        {
	            $item->remove();
	        }
	        eZPersistentObject::remove();
	        $db->commit();
	    }

	    /*!
	     \static
	      Fetches the RSS Export by ID.

	     \param RSS Export ID
	    */
	    static function fetch( $id, $asObject = true, $status = EZ_RSSEXPORT_STATUS_VALID )
	    {
	        return eZPersistentObject::fetchObject( ymcRSSExport::definition(),
	                                                null,
	                                                array( "id" => $id,
	                                                       'status' => $status ),
	                                                $asObject );
	    }

	    /*!
	     \static
	      Fetches the RSS Export by feed access url and is active.

	     \param RSS Export access url
	    */
	    static function fetchByName( $access_url, $asObject = true )
	    {
	        return eZPersistentObject::fetchObject( ymcRSSExport::definition(),
	                                                null,
	                                                array( 'access_url' => $access_url,
	                                                       'active' => 1,
	                                                       'status' => 1 ),
	                                                $asObject );
	    }

	    /*!
	     \static
	      Fetches complete list of RSS Exports.
	    */
	    static function fetchList( $asObject = true )
	    {
	        return eZPersistentObject::fetchObjectList( ymcRSSExport::definition(),
	                                                    null, array( 'status' => 1 ), null, null,
	                                                    $asObject );
	    }

	    /*!
	     \static
	      Fetches complete list of RSS Exports.
	    */
	    static function fetchFeedList( $conds, $asObject = true )
	    {
	        return eZPersistentObject::fetchObjectList( ymcRSSExport::definition(),
	                                                    null, $conds, null, null,
	                                                    $asObject );
	    }


	    /*!
	     \reimp
	    */
	    function attributes()
	    {
	        return array_merge( eZPersistentObject::attributes(),
	                            'item_list',
	                            'modifier',
	                            'owner',
	                            'rss-xml',
	                            'image_path',
	                            'image_node' );
	    }

	    /*!
	     \reimp
	    */
	    function hasAttribute( $attr )
	    {
	        return ( $attr == 'item_list' or $attr == 'modifier' or $attr == 'rss-xml' or $attr == 'image_path' or $attr == 'image_node' or
	                 eZPersistentObject::hasAttribute( $attr ) );
	    }

	    /*!
	     \reimp
	    */
	    function attribute( $attr, $noFunction = false )
	    {
	        switch( $attr )
	        {
	            case 'item_list':
	            {
	                return $this->fetchItems();
	            } break;

	            case 'image_node':
	            {
	                if ( !$this->ImageID > 0 )
	                    return null;
	                include_once( "kernel/classes/ezcontentobjecttreenode.php" );
	                return eZContentObjectTreeNode::fetch( $this->ImageID );
	            }

	            case 'image_path':
	            {
	                if ( !$this->ImageID > 0 )
	                    return null;
	                include_once( "kernel/classes/ezcontentobjecttreenode.php" );
	                $objectNode = eZContentObjectTreeNode::fetch( $this->ImageID );
	                if ( !isset( $objectNode ) )
	                    return null;
	                $path_array = $objectNode->attribute( 'path_array' );
	                for ( $i = 0; $i < count( $path_array ); $i++ )
	                {
	                    $treenode = eZContentObjectTreeNode::fetch( $path_array[$i] );
	                    if( $i == 0 )
	                        $return = $treenode->attribute( 'name' );
	                    else
	                        $return .= '/'.$treenode->attribute( 'name' );
	                }
	                return $return;

	            } break;

	            case 'modifier':
	            {
	                include_once( "kernel/classes/datatypes/ezuser/ezuser.php" );
	                return eZUser::fetch( $this->ModifierID );
	            } break;

	            case 'owner':
	             {
	                include_once( "kernel/classes/datatypes/ezuser/ezuser.php" );
	                return eZUser::fetch( $this->CreatorID );
	            } break;

	            case 'rss-xml':
	            {
	                switch( $this->attribute( 'rss_version' ) )
	                {
	                    case '1.0':
	                    {
	                        return null;
	                    } break;

	                    case '2.0':
	                    {
	                        return $this->fetchPodcast();
	                    } break;

	                    case 'podcast':
	                    {
	                        return $this->fetchPodcast();
	                    } break;
	                }
	            } break;

	            default:
	                return eZPersistentObject::attribute( $attr );
	        }

	        return null;
	    }

	    /*!
	      Fetches RSS Items related to this RSS Export. The RSS Export Items contain information about which nodes to export information from

	      \param RSSExport ID (optional). Uses current RSSExport's ID as default

	      \return RSSExportItem list. null if no RSS Export items found
	    */
	    function fetchItems( $id = false, $status = EZ_RSSEXPORT_STATUS_VALID )
	    {
	        if ( $id === false )
	        {
	            if ( isset( $this ) )
	            {
	                $id = $this->ID;
	                $status = $this->Status;
	            }
	            else
	                return null;
	        }
	        if ( $id !== null )
	        {
	            return ymcRSSExportItem::fetchFilteredList( array( 'rssexport_id' => $id, 'status' => $status ) );
	        }
	        else
	        {
	            $items = array();
	            return array();
	        }
	    }

	    function getObjectListFilter()
	    {
	        if ( $this->MainNodeOnly == 1 )
	        {
	            $this->MainNodeOnly = true;
	        }
	        else
	        {
	            $this->MainNodeOnly = false;
	        }

	        return array(
	                    'number_of_objects' => intval($this->NumberOfObjects),
	                    'main_node_only'    => $this->MainNodeOnly
	                    );
	    }

	    /*!
	     Get a RSS xml document based on the RSS 2.0 standard based on the RSS Export settings defined by this object

	     \param

	     \return RSS 2.0 XML document
	    */

	function fetchPodcast( $id = null )
	    {
	        if ( $id != null )
	        {
	            $rssExport = ymcRSSExport::fetch( $id );
	            return $rssExport->fetchPodcast();
	        }

	        include_once( 'lib/ezxml/classes/ezdomdocument.php' );

	        // Get URL Translation settings.
	        $config = eZINI::instance( 'site.ini' );
		    $pod_config = eZINI::instance( 'podcast.ini' );
		
		
		    $cond = array(
	                    'rssexport_id'  => $this->ID,
	                    'status'        => $this->Status
	                    );

	        $rssSources = ymcRSSExportItem::fetchFilteredList( $cond );

	        $nodeArray = ymcRSSExportItem::fetchNodeList( $rssSources, $this->getObjectListFilter() );

	        $useURLAlias = true;
	
			$rss_ar = array( 'version' => '2.0', 'xmlns:atom' => 'http://www.w3.org/2005/Atom' );
			
	        $baseItemURL = 'http://'.eZSys::hostname() . eZSys::indexDir().'/'; 
	
			if ($pod_config->variable('iTunesSettings','AllowUse') == 'enabled') $rss_ar['xmlns:itunes'] = "http://www.itunes.com/dtds/podcast-1.0.dtd";
			
	        $doc = new eZDOMDocument();
	        $doc->setName( 'eZ publish RSS Export' );
	        $root = $doc->createElementNode( 'rss', $rss_ar);
	        $doc->setRoot( $root );

	        $channel = $doc->createElementNode( 'channel' );
	        $root->appendChild( $channel );

	        $channelTitle = $doc->createElementTextNode( 'title', $this->attribute( 'title' ) );
	        $channel->appendChild( $channelTitle );

	        $channelLink = $doc->createElementTextNode( 'link', $baseItemURL );
	        $channel->appendChild( $channelLink );


	        $channelDescription = $doc->createElementTextNode( 'description', $this->attribute( 'description' ) );
	        $channel->appendChild( $channelDescription );

	        $channelLanguage = $doc->createElementTextNode( 'language', $config->variable('RegionalSettings','Locale') );
	        $channel->appendChild( $channelLanguage );
	
	        $channelGenerator= $doc->createElementTextNode( 'generator', 'eZ Publish' );
	        $channel->appendChild( $channelGenerator );
	
	        $channelDocs = $doc->createElementTextNode( 'docs', 'http://blogs.law.harvard.edu/tech/rss' );
	        $channel->appendChild( $channelDocs );
	
			$last_mod = 0;
	
			foreach ($nodeArray as $feednode) {
				if ($feednode->object()->attribute('modified') > $last_mod) $last_mod = $feednode->object()->attribute('modified');
			}
	
	        $channelPubDate = $doc->createElementTextNode( 'pubDate', gmdate( 'D, d M Y H:i:s', $last_mod ) .' GMT' );
	        $channel->appendChild( $channelPubDate );
	
	        $channellastBuildDate = $doc->createElementTextNode( 'lastBuildDate', gmdate( 'D, d M Y H:i:s', $last_mod ) .' GMT'  );
	        $channel->appendChild( $channellastBuildDate );

			$atom_str = "http://". $_SERVER['HTTP_HOST'] . "/" . $_SERVER['REQUEST_URI'];
	
		    $channelatomlink = $doc->createElementNode( 'atom:link', array('href' => $atom_str, 'rel' => 'self', 'type' => 'application/rss+xml') );
	        $channel->appendChild( $channelatomlink);

	        $imageURL = $this->fetchImageURL();
	        if ( $imageURL !== false )
	        {
	            $image = $doc->createElementNode( 'image' );

	            $imageUrlNode = $doc->createElementTextNode( 'url', $imageURL );
	            $image->appendChild( $imageUrlNode );

	            $imageTitle = $doc->createElementTextNode( 'title', $this->attribute( 'title' ) );
	            $image->appendChild( $imageTitle );

	            $imageLink = $doc->createElementTextNode( 'link', $this->attribute( 'url' ) );
	            $image->appendChild( $imageLink );

	            $channel->appendChild( $image );
	        }
	
			if ($pod_config->variable('iTunesSettings','AllowUse') == 'enabled') {
				
				$iTunesChannel = $pod_config->variable('iTunesSettings','Channel');
				$iTunesCategories = explode(',', $iTunesChannel['Category']);	
				$channelitunesauthor = $doc->createElementTextNode( 'itunes:author',  $iTunesChannel['Author'] );	
				$channel->appendChild( $channelitunesauthor );		
				
				$channelituneskeywords = $doc->createElementTextNode( 'itunes:keywords',  $iTunesChannel['Keywords'] );	
				$channel->appendChild( $channelituneskeywords );
				
				$channelitunesexplicit= $doc->createElementTextNode( 'itunes:explicit',  $iTunesChannel['Explicit'] );	
				$channel->appendChild( $channelitunesexplicit );
				
				$channelitunesimage = $doc->createElementNode( 'itunes:image',  array('href' => $iTunesChannel['Image'] ) );	
				$channel->appendChild( $channelitunesimage);
				
				$channelitunesblock = $doc->createElementTextNode( 'itunes:block',  $iTunesChannel['Block'] );	
				$channel->appendChild( $channelitunesblock);

				foreach ($iTunesCategories as $category) {
					$this->addCategory(&$channel, $doc, $pod_config, $category);
				}
				
			}


	        if( is_array( $nodeArray ) && count( $nodeArray ) )
	        {
	            $attributeMappings = ymcRSSExportItem::getAttributeMappings( $rssSources );
	        }

	        foreach ( $nodeArray as $node )
	        {
	            $object = $node->attribute( 'object' );
	            $dataMap = $object->dataMap();
	            if ( $useURLAlias === true )
	            {
	                $nodeURL = $baseItemURL.$node->urlAlias();
	            }
	            else
	            {
	                $nodeURL = $baseItemURL.'content/view/full/'.$node->attribute( 'node_id' );
	            }

	            // keep track if there's any match
	            $doesMatch = false;
	            // start mapping the class attribute to the respective RSS field
	            foreach ( $attributeMappings as $attributeMapping )
	            {
	                // search for correct mapping by path
	                if ( $attributeMapping[0]->attribute( 'class_id' ) == $object->attribute( 'contentclass_id' ) and
	                     in_array( $attributeMapping[0]->attribute( 'source_node_id' ), $node->attribute( 'path_array' ) ) )
	                {
	                    // found it

	                    $doesMatch = true;
	                    // now fetch the attributes
	                    $title = $dataMap[$attributeMapping[0]->attribute( 'title' )];
	                    //$description = $dataMap[$attributeMapping[0]->attribute( 'description' )];
	                    $attribArray = explode(",", $attributeMapping[0]->attribute( 'description' ));
	                    $description = array();
	                    foreach($attribArray as $attribute)
	                    {
	                    $description[] = $dataMap[$attribute];
	                    }


	                    $enclosure = $dataMap[$attributeMapping[0]->attribute( 'enclosure' )];
	                    break;
	                }
	            }

	            if( !$doesMatch )
	            {
	                // no match
	                eZDebug::writeWarning( __CLASS__.'::'.__FUNCTION__.': Cannot find matching RSS source node for content object in '.__FILE__.', Line '.__LINE__ );
	                $retValue = null;
	                return $retValue;
	            }

	            // title RSS element with respective class attribute content
	            unset( $itemTitle );
	            $itemTitle = $doc->createElementNode( 'title' );
	            $titleContent = $title->attribute( 'content' );
	            if ( get_class( $titleContent ) == 'ezxmltext' )
	            {
	                $outputHandler = $titleContent->attribute( 'output' );
	                unset( $itemTitleText );
	                $itemTitleText = $doc->createTextNode( $outputHandler->attribute( 'output_text' ) );
	                $itemTitle->appendChild( $itemTitleText );
	            }
	            else
	            {
	                unset( $itemTitleText );
	                $itemTitleText = $doc->createTextNode( $titleContent );
	                $itemTitle->appendChild( $itemTitleText );
	            }

	            // title RSS element with respective class attribute content

	            unset( $itemDescription );
	            $itemDescription = $doc->createElementNode( 'description' );
				$itunesSummary = '';
				$itunesImage = '';
	            foreach ($description as $descItem)
	            {
	                $descriptionContent = $descItem->attribute( 'content' );
	                if ( strtolower(get_class( $descriptionContent )) == 'ezxmltext' )
	                {
	                    $outputHandler = $descriptionContent->attribute( 'output' );
	                    unset( $itemDescriptionText );
						$itunesSummary = $outputHandler->attribute( 'output_text' );
			   if($pod_config->variable('iTunesSettings','AllowUse') == 'enabled') {$itunesImage = $pod_config->variable('iTunesSettings','iTunesImage');}
	                    $itemDescriptionText = $doc->createTextNode( $itunesSummary . $itunesImage );
	                    $itemDescription->appendChild( $itemDescriptionText );
	                }
	                 else if (strtolower(get_class( $descContent )) == 'ezimagealiashandler')
	                {
	                    $original = $descContent->attribute('original');
	                    $alt = $descContent->attribute('alternative_text');
	                    $MainConfig = eZINI::instance( 'site.ini' );
	                    $url = $baseItemURL.$original['url'];
	                    unset($imageTag);
	                    $imageTag = "<img src=\"".$url."\" alt=\"".$alt."\" />";

	                    $itemDescription->appendChild( $doc->createTextNode( $imageTag ) );
	                }
	                else
	                {
	                    include_once( "kernel/common/template.php" );
	                    $tpl = templateInit();
	                    $tpl->setVariable( 'templateAttribute', $descItem );
	                    unset($tempRes);
	                    $tempRes= $tpl->fetch( "design:feed/output.tpl" );
	                    $itemDescription->appendChild( $doc->createTextNode( $tempRes ) );
	                }
	            }
	             if(is_object($enclosure))
	            {
	            unset( $itemEnclosure );
	            $itemEnclosure = $doc->createElementNode( 'enclosure' );

	            $enclosureContent = $enclosure->attribute( 'content' );
	
		
	
	            if ( get_class( $enclosureContent ) == 'eZBinaryFile' || get_class( $enclosureContent ) == 'ezmedia' )
	            {
	                $filename = $enclosureContent->attribute( 'original_filename' );
	                $contentobject_id = $enclosure->attribute( 'contentobject_id' );
	                $attribute_id = $enclosure->attribute( 'id' );

	                $downloadLink = $baseItemURL.'content/download/'.$contentobject_id."/".$attribute_id."/file/".rawurlencode($filename);



	                $enclosureLength = $enclosureContent->attribute('filesize');
	                $enclosureMime = $enclosureContent->attribute('mime_type');

	            $definition = array( 'url' => 'url',
	                           'length' => 'length',
	                           'type' => 'type');
	             $values = array( 'url' => $downloadLink,
	                       'length' => $enclosureLength,
	                       'type' => $enclosureMime);
	            $itemEnclosure->appendAttributes( $values, $definition );

	            }

	            }

	            unset( $itemLink );
	            $itemLink = $doc->createElementNode( 'link' );

	            unset( $itemLinkUrl );
	            $itemLinkUrl = $doc->createTextNode( $nodeURL );
	            $itemLink->appendChild( $itemLinkUrl );




	            unset( $item );
	            $item = $doc->createElementNode( 'item' );

	            unset( $itemPubDate );
	            $itemPubDate = $doc->createElementTextNode( 'pubDate', gmdate( 'D, d M Y H:i:s', $object->attribute( 'published' ) ) .' GMT' );


	            unset( $guidLink );
	            $guidLink = $doc->createElementNode( 'guid' );

	            unset( $guidLinkURL );
	            $guidLinkURL = $doc->createTextNode( $nodeURL );
	            $guidLink->appendChild( $guidLinkURL );
	

				unset( $itemItunesSumary  );
	                  $itemItunesSumary = $doc->createElementTextNode( 'itunes:summary', $itunesSummary );
	                  $item->appendChild( $itemItunesSumary );


				unset( $itemItunesAuthor );
                $itemItunesAuthor = $doc->createElementTextNode( 'itunes:author', $pod_config->variable('iTunesSettings','ItemAuthor') );
                $item->appendChild( $itemItunesAuthor );
				unset( $itemItunesSubtitle );
                $itemItunesSubtitle = $doc->createElementTextNode( 'itunes:subtitle', $pod_config->variable('iTunesSettings','ItemSubtitle') );
                $item->appendChild( $itemItunesSubtitle );
				unset( $itemItunesExplicit );
                $itemItunesExplicit = $doc->createElementTextNode( 'itunes:explicit', $pod_config->variable('iTunesSettings','ItemExplicit') );
                $item->appendChild( $itemItunesExplicit );
				unset( $itemItunesDuration );
                $itemItunesDuration = $doc->createElementTextNode( 'itunes:duration', $this->getTimeFromSeconds($dataMap['duration']->content()) );
                $item->appendChild( $itemItunesDuration );

	            $item->appendChilD( $guidLink);
	            $item->appendChild( $itemPubDate );
	            $item->appendChild( $itemTitle );
	            $item->appendChild( $itemLink );
	            $item->appendChild( $itemDescription );
	            $item->appendChild( $itemEnclosure );
	


	            $channel->appendChild( $item );
	        }

	        return $doc;
	    }


	    /*!
	     \private

	     Fetch Image from current ezrss export object. If non exist, or invalid, return false

	     \return valid image url
	    */
	    function fetchImageURL()
	    {

	        $imageNode = $this->attribute( 'image_node' );
	        if ( !$imageNode )
	            return false;

	        $imageObject = $imageNode->attribute( 'object' );
	        if ( !$imageObject )
	            return false;

	        $dataMap = $imageObject->attribute( 'data_map' );
	        if ( !$dataMap )
	            return false;

	        $imageAttribute = $dataMap['image'];
	        if ( !$imageAttribute )
	            return false;

	        $imageHandler = $imageAttribute->attribute( 'content' );
	        if ( !$imageHandler )
	            return false;

	        $imageAlias = $imageHandler->imageAlias( 'rss' );
	        if( !$imageAlias )
	            return false;

	        $url = eZSys::hostname() . eZSys::wwwDir() .'/'. $imageAlias['url'];
	        $url = preg_replace( "#^(//)#", "/", $url );

	        return 'http://'.$url;
	    }
	
		function addCategory(&$channel, $doc, $pod_config, $category, $subcats = array()) {
			if ($pod_config->hasVariable('iTunesSettings', trim($category) ) ) {	
				$cat_cat = $pod_config->variable('iTunesSettings', trim($category) );
				unset($addme);
				$addme = $doc->createElementNode( 'itunes:category', array('text' => $category) );															
				$subcats[] = $addme ;
				$this->addCategory(&$channel, $doc, $pod_config, $cat_cat , $subcats);
				return false;
			} else {
				$subcats[] = $doc->createElementNode( 'itunes:category',  array('text' => trim($category) ) );	
			}

			$lastone = 0;
			
			foreach (array_reverse($subcats) as $thiscat) {

				if ($lastone == 0) {
					$lastone = $thiscat;
					continue;
				}

				$thiscat->appendChild($lastone);
				
				$lastone = $thiscat;
			}

			if ($lastone != 0) $channel->appendChild($lastone);
			
			$subcats = array();

		}

		function getTimeFromSeconds($seconds=0) {
			return floor($seconds/60) . ':' . (int)fmod($seconds, 60);
		}

}
?>
