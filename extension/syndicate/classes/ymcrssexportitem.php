<?php


class ymcRSSExportItem extends eZPersistentObject
{

    /*!
     Initializes a new RSSExportItem.
    */
    function ymcRSSExportItem( $row )
    {
        $this->eZPersistentObject( $row );
    }

    /*!
     \reimp
    */
    static function definition()
    {
        return array( "fields" => array( "id" => array( 'name' => 'ID',
                                                        'datatype' => 'integer',
                                                        'default' => 0,
                                                        'required' => true ),
                                         'rssexport_id' => array( 'name' => 'RSSExportID',
                                                                  'datatype' => 'integer',
                                                                  'default' => '',
                                                                  'required' => true ),
                                         'source_node_id' => array( 'name' => 'SourceNodeID',
                                                                    'datatype' => 'integer',
                                                                    'default' => '',
                                                                    'required' => true ),
                                         'class_id' => array( 'name' => 'ClassID',
                                                              'datatype' => 'integer',
                                                              'default' => '',
                                                              'required' => true ),
                                         'description' => array( 'name' => 'Description',
                                                                 'datatype' => 'string',
                                                                 'default' => '',
                                                                 'required' => true ),
                                         'title' => array( 'name' => 'Title',
                                                           'datatype' => 'string',
                                                           'default' => '',
                                                           'required' => true ),
                                         'enclosure' => array( 'name' => 'Enclosure',
                                                           'datatype' => 'string',
                                                           'default' => '',
                                                           'required' => true ),
                                         'status' => array( 'name' => 'Status',
                                                            'datatype' => 'integer',
                                                            'default' => 0,
                                                            'required' => true ),
                                         'subnodes' => array( 'name' => 'Subnodes',
                                                            'datatype' => 'integer',
                                                            'default' => 0,
                                                            'required' => true ) ),
                      "keys" => array( "id", 'status' ),
                      'function_attributes' => array( 'class_attributes' => 'classAttributes',
                                                      'source_node' => 'sourceNode',
                                                      'source_path' => 'sourcePath' ),
                      "increment_key" => "id",
                      "class_name" => "ymcRSSExportItem",
                      "name" => "ymcrss_export_item" );
    }

    /*!
     \static
     Creates a new RSS Export Item
     \param ymcRSSExport objcted id. (The RSSExport this item belongs to)

     \return the URL alias object
    */
    static function create( $rssexport_id )
    {
        $SyndicateConfig = eZINI::instance( 'syndicate.ini' );
        $defaultClass = eZContentClass::fetchByIdentifier($SyndicateConfig->variable( 'SyndicateSettings', 'DefaultClass' ));
        $row = array( 'id' => null,
                      'rssexport_id' => $rssexport_id,
                      'source_node_id' => 0,
                      'class_id' =>  $defaultClass->attribute('id'),
                      'url_id' => '',
                      'description' => '',
                      'title' => '',
                      'enclosure' => '',
                      'status' => 0,
                      'subnodes' => 0);
        return new ymcRSSExportItem( $row );
    }

    function classAttributes()
    {
        if ( isset( $this->ClassID ) and $this->ClassID )
        {
            include_once( 'kernel/classes/ezcontentclass.php' );
            $contentClass = eZContentClass::fetch( $this->ClassID );
            if ( $contentClass )
                $attributes = $contentClass->fetchAttributes();
            else
                $attributes = null;
        }
        else
            $attributes = null;
        return $attributes;
    }

    function sourcePath()
    {
        if ( isset( $this->SourceNodeID ) and $this->SourceNodeID )
        {
            include_once( "kernel/classes/ezcontentobjecttreenode.php" );
            $objectNode = eZContentObjectTreeNode::fetch( $this->SourceNodeID );
            if ( isset( $objectNode ) )
            {
                $path_array = $objectNode->attribute( 'path_array' );
                for ( $i = 0; $i < count( $path_array ); $i++ )
                {
                    $treenode = eZContentObjectTreeNode::fetch( $path_array[$i] );
                    if( $i == 0 )
                        $retValue = $treenode->attribute( 'name' );
                    else
                        $retValue .= '/'.$treenode->attribute( 'name' );
                }
            }
            else
                $retValue = null;
        }
        else
            $retValue = null;
        return $retValue;
    }

    function sourceNode()
    {
        if ( isset( $this->SourceNodeID ) and $this->SourceNodeID )
        {
            include_once( "kernel/classes/ezcontentobjecttreenode.php" );
            $sourceNode = eZContentObjectTreeNode::fetch( $this->SourceNodeID );
        }
        else
            $sourceNode = null;
        return $sourceNode;
    }

    /*!
     \static
      Fetches the RSS Export by ID.

     \param RSS Export ID
    */
    static function fetch( $id, $asObject = true, $status = EZ_RSSEXPORT_STATUS_VALID )
    {
        return eZPersistentObject::fetchObject( ymcRSSExportItem::definition(),
                                                null,
                                                array( "id" => $id,
                                                       'status' => $status ),
                                                $asObject );
    }

    /*
     Fetches the items belonging to the specified RSSExport
     example: fetchFilteredList( array( 'rssexport_id' => 24 ) )

     \param array, example: array( 'rssexport_id' => 24 )

     \return array containing RSSExport Items
    */
    static function fetchFilteredList( $cond, $asObject = true, $status = EZ_RSSEXPORT_STATUS_VALID )
    {
        return eZPersistentObject::fetchObjectList( ymcRSSExportItem::definition(),
                                                    null, $cond, array( 'id' => 'asc',
                                                                        'status' => $status ), null,
                                                    $asObject );
    }

    static function getAttributeMappings( $rssSources )
    {
        if( is_array( $rssSources ) && count( $rssSources ) )
        {
            foreach( $rssSources as $rssSource )
            {
                // fetch path for class attribute to RSS field mapping
                $node = eZContentObjectTreeNode::fetch( $rssSource->SourceNodeID );
                if ( is_object( $node ) )
                {
                    $attributeMappings[] = array( $rssSource, $node );
                }
            }

            // sort the array so nodes with deeper path are first
            // for class attribute to RSS field mapping
            usort( $attributeMappings,
                   create_function( '$a, $b',
                                    '$a_cnt = count( $a[1]->attribute( \'path_array\' ) );' .
                                    '$b_cnt = count( $b[1]->attribute( \'path_array\' ) );' .
                                    'return ( $a_cnt == $b_cnt ) ? 0 : ( ( $a_cnt > $b_cnt ) ? 1 : -1 );' ) );
        }

        return $attributeMappings;
    }

    /*!
     Get the N last published nodes matching the specifications of this RSS Export item

     \param number of objects to fetch

     \return list of Nodes
    */
    static function fetchNodeList( $rssSources, $objectListFilter )
    {
        // compose parameters for several subtrees
        if( is_array( $rssSources ) && count( $rssSources ) )
        {
            foreach( $rssSources as $rssSource )
            {
                // Do not include subnodes
                if ( !intval( $rssSource->Subnodes ) )
                {
                    $depth = 1;
                }
                else // Fetch objects even from subnodes
                {
                    $depth = 0;
                }

                $nodesParams[] = array( 'ParentNodeID' => $rssSource->SourceNodeID,
                                        'ResultID' => $rssSource->ID,
                                        'Depth' => $depth,
                                        'DepthOperator' => 'eq',
                                        'MainNodeOnly' => $objectListFilter['main_node_only'],
                                        'ClassFilterType' => 'include',
                                        'ClassFilterArray' => array( intval( $rssSource->ClassID ) )
                                       );
            }
            $listParams = array( 'Limit' => $objectListFilter['number_of_objects'],
                                 'SortBy' => array( 'published', false )
                                );

            include_once( "kernel/classes/ezcontentobjecttreenode.php" );

            return eZContentObjectTreeNode::subTreeMultiPaths( $nodesParams, $listParams );
        }
    }

}

?>
