<?php
// Syndicate: an RSS Extension - Function definition for fetch functions
//
// Created on: <18-Sep-2003 14:49:54 kk>
// Modified on: <10-Feb-2006 14:00:00 ymc-pabu>
//
// Copyright (C) 1999-2006 eZ systems as. All rights reserved.
//
// This source file is part of the eZ publish (tm) Open Source Content
// Management System.
//
// This file may be distributed and/or modified under the terms of the
// "GNU General Public License" version 2 as published by the Free
// Software Foundation and appearing in the file LICENSE included in
// the packaging of this file.
//
// Licencees holding a valid "eZ publish professional licence" version 2
// may use this file in accordance with the "eZ publish professional licence"
// version 2 Agreement provided with the Software.
//
// This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING
// THE WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR
// PURPOSE.
//
// The "GNU General Public License" (GPL) is available at
// http://www.gnu.org/copyleft/gpl.html.
//
// Contact licence@ymc.ch if any conditions of this licencing isn't clear to
// you.
//
$FunctionList = array();

$FunctionList['feed_by_source'] = array( 'name' => 'feed_by_source',
                                            'operation_types' => array(),
                                            'call_method' => array( 'include_file' => 'extension/syndicate/modules/syndicate/syndicate_functioncollection.php',
                                                                    'class' => 'ymcSyndicateFunctionCollection',
                                                                    'method' => 'fetchFeedBySource' ),
                                            'parameter_type' => 'standard',
                                            'parameters' => array( array('name' => 'SourceNodeID',
                                                                          'type' => 'integer',
                                                                          'required' => false,
                                                                          'default' => false),
                                                            array('name' => 'ClassIdentifier',
                                                                          'type' => 'string',
                                                                          'required' => false,
                                                                          'default' => false),
                                                           array('name' => 'AccessURL',
                                                                          'type' => 'string',
                                                                          'required' => false,
                                                                          'default' => false)) );
$FunctionList['feed_by_version'] = array( 'name' => 'feed_by_version',
                                            'operation_types' => array(),
                                            'call_method' => array( 'include_file' => 'extension/syndicate/modules/syndicate/syndicate_functioncollection.php',
                                                                    'class' => 'ymcSyndicateFunctionCollection',
                                                                    'method' => 'fetchFeedByVersion' ),
                                            'parameter_type' => 'standard',
                                            'parameters' => array( array('name' => 'Version',
                                                                          'type' => 'string',
                                                                          'required' => false,
                                                                          'default' => false)) );                                                                          
                                                                          
?>
