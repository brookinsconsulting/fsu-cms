<?php
//
// Syndicate: an RSS Extension - Export removed
//
// Created on: <18-Sep-2003 14:49:54 kk>
// Modified on: <10-Feb-2006 14:00:00 ymc-pabu>
//
// Copyright (C) 1999-2006 eZ systems as. All rights reserved.
//
// This source file is part of the eZ publish (tm) Open Source Content
// Management System.
//
// This file may be distributed and/or modified under the terms of the
// "GNU General Public License" version 2 as published by the Free
// Software Foundation and appearing in the file LICENSE included in
// the packaging of this file.
//
// Licencees holding a valid "eZ publish professional licence" version 2
// may use this file in accordance with the "eZ publish professional licence"
// version 2 Agreement provided with the Software.
//
// This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING
// THE WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR
// PURPOSE.
//
// The "GNU General Public License" (GPL) is available at
// http://www.gnu.org/copyleft/gpl.html.
//
// Contact licence@ymc.ch if any conditions of this licencing isn't clear to
// you.
//

$Module = $Params["Module"];

include_once( "kernel/common/template.php" );
$tpl = templateInit();
$Result = array();
$Result['content'] = $tpl->fetch( "design:feed/removed.tpl" );
$Result['path'] = array( array( 'url' => false,
                                'text' => ezi18n( 'syndicate', 'Syndication removed' ) ) );


?>
