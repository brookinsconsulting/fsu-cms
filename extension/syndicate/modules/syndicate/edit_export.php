<?php


$Module = $Params["Module"];

include_once( 'extension/syndicate/modules/syndicate/edit_functions.php' );
include_once( 'kernel/common/template.php' );


$http = eZHTTPTool::instance();

$validated = false;
if ( isset( $Params['RSSExportID'] ) )
    $RSSExportID = $Params['RSSExportID'];
else
    $RSSExportID = false;
if ( $http->hasPostVariable( 'RSSExport_ID' ) )
    $RSSExportID = $http->postVariable( 'RSSExport_ID' );


if ( $Module->isCurrentAction( 'Store' ) )
{
    if( $_POST['active'] == "on" and strlen( trim( $_POST['Access_URL'] ) ) == 0 )
    {
             storeRSSExport( $Module, $http );
             $validated = true;
    }
    else
    {
        return storeRSSExport( $Module, $http, true );
    }
}
else if ( $Module->isCurrentAction( 'UpdateItem' ) )
{
   
    storeRSSExport( $Module, $http );
}
else if ( $Module->isCurrentAction( 'AddItem' ) )
{
    $rssExportItem = ymcRSSExportItem::create( $RSSExportID );
    $rssExportItem->store();
    storeRSSExport( $Module, $http );
}
else if ( $Module->isCurrentAction( 'Cancel' ) )
{
    
    $rssExport = ymcRSSExport::fetch( $RSSExportID, true, EZ_RSSEXPORT_STATUS_DRAFT );
    if($rssExport)
        $rssExport->remove(); 
    $rssExport = ymcRSSExport::fetch( $RSSExportID, true, EZ_RSSEXPORT_STATUS_VALID );
    if($rssExport)
        $rssExport->remove(); 
 return $Module->redirectTo("/syndicate/removed");
}
else if ( $Module->isCurrentAction( 'BrowseImage' ) )
{
    storeRSSExport( $Module, $http );
    include_once( 'kernel/classes/ezcontentbrowse.php' );
    eZContentBrowse::browse( array( 'action_name' => 'RSSExportImageBrowse',
                                    'description_template' => 'design:rss/browse_image.tpl',
                                    'from_page' => '/syndicate/edit_export/'. $RSSExportID .'/0/ImageSource' ),
                             $Module );
}
else if ( $Module->isCurrentAction( 'RemoveImage' ) )
{
    $rssExport = ymcRSSExport::fetch( $RSSExportID, true, EZ_RSSEXPORT_STATUS_DRAFT );
    $rssExport->setAttribute( 'image_id', 0 );
    $rssExport->store();
}


if ( $http->hasPostVariable( 'Item_Count' ) )
{

    $db = eZDB::instance();
    $db->begin();
    for ( $itemCount = 0; $itemCount < $http->postVariable( 'Item_Count' ); $itemCount++ )
    {
        if ( $http->hasPostVariable( 'SourceBrowse_'.$itemCount ) )
        {
            storeRSSExport( $Module, $http );
            include_once( 'kernel/classes/ezcontentbrowse.php' );
            eZContentBrowse::browse( array( 'action_name' => 'RSSObjectBrowse',
                                            'description_template' => 'design:feed/browse_source.tpl',
                                            'from_page' => '/syndicate/edit_export/'. $RSSExportID .'/'. $http->postVariable( 'Item_ID_'.$itemCount ) .'/NodeSource' ),
                                     $Module );
            break;
        }

        // remove selected source (if any)
        if ( $http->hasPostVariable( 'RemoveSource_'.$itemCount ) )
        {
            $itemID = $http->postVariable( 'Item_ID_'.$itemCount );
            if ( ( $rssExportItem = ymcRSSExportItem::fetch( $itemID, true, EZ_RSSEXPORT_STATUS_DRAFT ) ) )
            {
                // remove the draft version
                $rssExportItem->remove();
                // remove the published version
                $rssExportItem->setAttribute( 'status', EZ_RSSEXPORT_STATUS_VALID );
                $rssExportItem->remove();
                storeRSSExport( $Module, $http );
            }

            break;
        }
    }
    $db->commit();
}


if ( is_numeric( $RSSExportID ) )
{
    
    $rssExportID = $RSSExportID;
    $rssExport = ymcRSSExport::fetch( $RSSExportID, true, EZ_RSSEXPORT_STATUS_DRAFT );
    if ( $rssExport )
    {
        include_once( 'lib/ezlocale/classes/ezdatetime.php' );
        $user = eZUser::currentUser();
       
        //Check for Owner-Only Limitation
        $limitation = $user->hasAccessTo('syndicate', 'edit');
        if ($limitation["accessWord"]=="limited")
        {
            foreach($limitation['policies'] as $policy)
            {
                foreach($policy as $key => $value)
                {
                    if($key=='Owner'&&$value[0]==1)
                    {
                        if (!$rssExport->attribute('creator_id')==$user->attribute( "contentobject_id" ))
                        {
                            return $Module->handleError( EZ_ERROR_KERNEL_ACCESS_DENIED, 'kernel' );
                        }
                    }
                }
            }
        }

        $contentIni = eZIni::instance( 'content.ini' );
        $timeOut = $contentIni->variable( 'RSSExportSettings', 'DraftTimeout' );
        if ( $rssExport->attribute( 'modifier_id' ) != $user->attribute( 'contentobject_id' ) &&
             $rssExport->attribute( 'modified' ) + $timeOut > time() )
        {
            // locked editing
            $tpl = templateInit();

            $tpl->setVariable( 'rss_export', $rssExport );
            $tpl->setVariable( 'rss_export_id', $rssExportID );
            $tpl->setVariable( 'lock_timeout', $timeOut );

            $Result = array();
            $Result['content'] = $tpl->fetch( 'design:feed/edit_export_denied.tpl' );
            $Result['path'] = array( array( 'url' => false,
                                            'text' => ezi18n( 'syndication', 'Syndication' ) ) );
            return $Result;
        }
        else if ( $timeOut > 0 && $rssExport->attribute( 'modified' ) + $timeOut < time() )
        {
            $rssExport->remove();
            $rssExport = false;
        }
    }
    if ( !$rssExport )
    {
        $rssExport = ymcRSSExport::fetch( $RSSExportID, true, EZ_RSSEXPORT_STATUS_VALID );
        if ( $rssExport )
        {
            $db = eZDB::instance();
            $db->begin();
            $rssItems = $rssExport->fetchItems();
            $rssExport->setAttribute( 'status', EZ_RSSEXPORT_STATUS_DRAFT );
            $rssExport->store();
            foreach( array_keys( $rssItems ) as $key )
            {
                $rssItem = $rssItems[$key];
                $rssItem->setAttribute( 'status', EZ_RSSEXPORT_STATUS_DRAFT );
                $rssItem->store();
            }
            $db->commit();
        }
        else
        {
            return $Module->handleError( EZ_ERROR_KERNEL_NOT_AVAILABLE, 'kernel' );
        }
    }

    include_once( 'kernel/classes/ezcontentbrowse.php' );

    switch ( $Params['BrowseType'] )
    {
        case 'NodeSource':
        {
            $nodeIDArray = $http->postVariable( 'SelectedNodeIDArray' );
            if ( isset( $nodeIDArray ) && !$http->hasPostVariable( 'BrowseCancelButton' ) )
            {
                $rssExportItem = ymcRSSExportItem::fetch( $Params['RSSExportItemID'], true, EZ_RSSEXPORT_STATUS_DRAFT );
                $rssExportItem->setAttribute( 'source_node_id', $nodeIDArray[0] );
                $rssExportItem->store();
            }
        } break;

        case 'ImageSource':
        {
            $imageNodeIDArray = $http->postVariable( 'SelectedNodeIDArray' );
            if ( isset( $imageNodeIDArray ) && !$http->hasPostVariable( 'BrowseCancelButton' ) )
            {
                $rssExport->setAttribute( 'image_id', $imageNodeIDArray[0] );
            }
        } break;
    }
}
else // New RSSExport
{
    $user = eZUser::currentUser();
    $user_id = $user->attribute( "contentobject_id" );

       
    $db = eZDB::instance();
    $db->begin();

    // Create default rssExport object to use
    $rssExport = ymcRSSExport::create( $user_id );
    $rssExport->store();
    $rssExportID = $rssExport->attribute( 'id' );

    // Create One empty export item
    $rssExportItem = ymcRSSExportItem::create( $rssExportID );
//    $rssExportItem->store();

//    $db->commit();
}

$tpl = templateInit();

// Populate site access list
$config = eZINI::instance( 'syndicate.ini' );
$siteAccess = $config->variable( 'SyndicateSettings', 'AvailableSiteAccessList' );
$rssVersionArray = $config->variable( 'SyndicateSettings', 'AvailableVersionList' );
$rssDefaultVersion = $config->variable( 'SyndicateSettings', 'DefaultVersion' );
$numberOfObjectsArray = $config->variable( 'SyndicateSettings', 'NumberOfObjectsList' );
$numberOfObjectsDefault = $config->variable( 'SyndicateSettings', 'NumberOfObjectsDefault' );

// Get Classes and class attributes
$classArray = array();
$allowedClasses = $config->variable( 'SyndicateSettings', 'AvailableClassesList' );

foreach ($allowedClasses as $class)
{
    $classArray[] = eZContentClass::fetchByIdentifier($class);
}
$tpl->setVariable( 'rss_version_array', $rssVersionArray );
$tpl->setVariable( 'rss_version_default', $rssDefaultVersion );
$tpl->setVariable( 'number_of_objects_array', $numberOfObjectsArray );
$tpl->setVariable( 'number_of_objects_default', $numberOfObjectsDefault );
$tpl->setVariable( 'rss_site_access', $siteAccess );
$tpl->setVariable( 'rss_class_array', $classArray );
$tpl->setVariable( 'rss_export', $rssExport );
$tpl->setVariable( 'rss_export_id', $rssExportID );

$tpl->setVariable( 'validaton', $validated );
$Result = array();
$Result['content'] = $tpl->fetch( "design:feed/edit_export.tpl" );
$Result['path'] = array( array( 'url' => false,
                                'text' => ezi18n( 'syndicate', 'Syndication' ) ) );


?>
