<?php

include_once( 'kernel/common/template.php' );

$Module = $Params["Module"];

$http = eZHTTPTool::instance();

if ( $http->hasPostVariable( 'NewExportButton' ) )
{
    return $Module->run( 'edit_export', array() );
}
else if ( $http->hasPostVariable( 'RemoveExportButton' ) )
{
    $deleteArray = $http->postVariable( 'DeleteIDArray' );
    foreach ( $deleteArray as $deleteID )
    {
        $rssExport = ymcRSSExport::fetch( $deleteID, true, EZ_RSSEXPORT_STATUS_DRAFT );
        if ( $rssExport )
        {
            $rssExport->remove();
        }
        $rssExport = ymcRSSExport::fetch( $deleteID, true, EZ_RSSEXPORT_STATUS_VALID );
        if ( $rssExport )
        {
            $rssExport->remove();
        }
    }
}


// Get all RSS Exports
$exportArray = ymcRSSExport::fetchList();
$exportList = array();
foreach( array_keys( $exportArray ) as $exportID )
{
    $export = $exportArray[$exportID];
    $exportList[$export->attribute( 'id' )] = $export;
}


$tpl = templateInit();

$tpl->setVariable( 'rssexport_list', $exportList );

$Result = array();
$Result['content'] = $tpl->fetch( "design:feed/list.tpl" );
$Result['path'] = array( array( 'url' => 'syndicate/list',
                                'text' => ezi18n( 'syndicate', 'Syndication/List' ) ) );


?>
