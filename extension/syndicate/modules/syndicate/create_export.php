<?php
//
// Syndicate: an RSS Extension - Export Creation on User Side
//
// Created on: <18-Sep-2003 14:49:54 kk>
// Modified on: <10-Feb-2006 14:00:00 ymc-pabu>
//
// Copyright (C) 1999-2006 eZ systems as. All rights reserved.
//
// This source file is part of the eZ publish (tm) Open Source Content
// Management System.
//
// This file may be distributed and/or modified under the terms of the
// "GNU General Public License" version 2 as published by the Free
// Software Foundation and appearing in the file LICENSE included in
// the packaging of this file.
//
// Licencees holding a valid "eZ publish professional licence" version 2
// may use this file in accordance with the "eZ publish professional licence"
// version 2 Agreement provided with the Software.
//
// This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING
// THE WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR
// PURPOSE.
//
// The "GNU General Public License" (GPL) is available at
// http://www.gnu.org/copyleft/gpl.html.
//
// Contact licence@ymc.ch if any conditions of this licencing isn't clear to
// you.
//

$Module = $Params["Module"];

include_once( 'extension/syndicate/modules/syndicate/edit_functions.php' );
include_once( 'kernel/common/template.php' );
include_once( 'extension/syndicate/classes/ymcrssexport.php' );
include_once( 'extension/syndicate/classes/ymcrssexportitem.php' );
include_once( 'lib/ezutils/classes/ezhttppersistence.php' );

$http = eZHTTPTool::instance();
$SourceID = $Params["SourceID"];
$ClassIdentifier = $Params["ClassIdentifier"];
if(!is_numeric($SourceID))
{
    return $Module->handleError( EZ_ERROR_KERNEL_NOT_AVAILABLE, 'kernel' );
}

    $validated = false;
    include_once( "kernel/classes/datatypes/ezuser/ezuser.php" );
    $user = eZUser::currentUser();
    $user_id = $user->attribute( "contentobject_id" );
    
    include_once( "kernel/classes/ezcontentobjecttreenode.php" );
    $objectNode = eZContentObjectTreeNode::fetch( $SourceID );
    $SourceObject = $objectNode->ContentObject;
    
    
    $limitation = $user->hasAccessTo('syndicate', 'create');
        if ($limitation["accessWord"]=="limited")
        {
            foreach($limitation[policies] as $policy)
            {
                foreach($policy as $key => $value)
                {
                    if($key=='SourceOwner'&&$value[0]==1)
                    {
                        if ($SourceObject->attribute('owner_id')!=$user_id)
                        {
                            return $Module->handleError( EZ_ERROR_KERNEL_ACCESS_DENIED, 'kernel' );
                        }
                        
                    }
                }
            }
        }

    $class = eZContentClass::fetchByIdentifier($ClassIdentifier);
    if (is_object($class))
    {
        $config = eZINI::instance( 'syndicate.ini' );
        $title = $config->variable( $ClassIdentifier.'_mapping', 'title' );
        $description = $config->variable( $ClassIdentifier.'_mapping', 'description' );
        $enclosure = $config->variable( $ClassIdentifier.'_mapping', 'enclosure' );
        $version = $config->variable( $ClassIdentifier.'_mapping', 'version' );
    }
    else
    {
        return $Module->handleError( EZ_ERROR_KERNEL_ACCESS_DENIED, 'kernel' );
    }
    
    $db = eZDB::instance();
    $db->begin();

    // Create default rssExport object to use
    $rssExport = ymcRSSExport::create( $user_id );
    //Here we generate the Access URL based on the user's name and class identifier
        $uo = $user->attribute('contentobject');
        $mn = $uo->attribute('main_node');
        $path = substr($mn->attribute('path_identification_string'), 
                        strrpos($mn->attribute('path_identification_string'), "/")+1);
    $access_url = $path."_".$ClassIdentifier; 
    $rssExport->setAttribute('access_url', $access_url);
    $rssExport->setAttribute('rss_version', $version);
    $rssExport->store();
    $rssExportID = $rssExport->attribute( 'id' );

    // Create One empty export item
    $rssExportItem = ymcRSSExportItem::create( $rssExportID );
    $rssExportItem->setAttribute( 'source_node_id', $SourceID );
    $rssExportItem->setAttribute( 'subnodes', 1 );
    if (is_object($class))
    {
        $rssExportItem->setAttribute( 'class_id', $class->attribute('id') );
        $rssExportItem->setAttribute( 'title', $title );
        $rssExportItem->setAttribute( 'description', $description );
        if ($enclosure!="")
        {
        $rssExportItem->setAttribute( 'enclosure', $enclosure );        
        }

    }
    $rssExportItem->store();

    $db->commit();


return $Module->redirectTo( '/syndicate/edit_export/'.$rssExportID );


?>
