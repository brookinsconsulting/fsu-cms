<?php
// Syndicate: an RSS Extension - Feed handling
//
// Created on: <18-Sep-2003 14:49:54 kk>
// Modified on: <10-Feb-2006 14:00:00 ymc-pabu>
//
// Copyright (C) 1999-2006 eZ systems as. All rights reserved.
//
// This source file is part of the eZ publish (tm) Open Source Content
// Management System.
//
// This file may be distributed and/or modified under the terms of the
// "GNU General Public License" version 2 as published by the Free
// Software Foundation and appearing in the file LICENSE included in
// the packaging of this file.
//
// Licencees holding a valid "eZ publish professional licence" version 2
// may use this file in accordance with the "eZ publish professional licence"
// version 2 Agreement provided with the Software.
//
// This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING
// THE WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR
// PURPOSE.
//
// The "GNU General Public License" (GPL) is available at
// http://www.gnu.org/copyleft/gpl.html.
//
// Contact licence@ymc.ch if any conditions of this licencing isn't clear to
// you.
//

$Module = $Params["Module"];
if ( !isset ( $Params['RSSFeed'] ) )
{
    eZDebug::writeError( 'No RSS feed specified' );
    return $Module->handleError( eZError::KERNEL_NOT_AVAILABLE, 'kernel' );
}

$feedName = $Params['RSSFeed'];
$RSSExport = ymcRSSExport::fetchByName( $feedName );

// Get and check if RSS Feed exists
if ( !$RSSExport )
{
    eZDebug::writeError( 'Could not find RSSExport : ' . $Params['RSSFeed'] );
    return $Module->handleError( eZError::KERNEL_NOT_AVAILABLE, 'kernel' );
}

if( $RSSExport->attribute('site_access') != $GLOBALS['eZCurrentAccess']['name'])
{
    eZDebug::writeError( 'Could not find RSSExport : ' . $Params['RSSFeed'] );
    return $Module->handleError( eZError::KERNEL_NOT_AVAILABLE, 'kernel' );
}


$config = eZINI::instance( 'site.ini' );
$cacheTime = intval( $config->variable( 'RSSSettings', 'CacheTime' ) );

if($cacheTime <= 0)
{
    $xmlDoc = $RSSExport->attribute( 'rss-xml' );
    $rssContent = $xmlDoc->toString();
}
else
{
    $cacheDir = eZSys::cacheDirectory();
    $cacheFile = $cacheDir . '/ymcrss/' . md5( $feedName ) . '.xml';

    // If cache directory does not exist, create it. Get permissions settings from site.ini
    if ( !is_dir( $cacheDir.'/ymcrss' ) )
    {
        $mode = $config->variable( 'FileSettings', 'TemporaryPermissions' );
        if ( !is_dir( $cacheDir ) )
        {
            mkdir( $cacheDir );
            chmod( $cacheDir, octdec( $mode ) );
        }
        mkdir( $cacheDir.'/ymcrss' );
        chmod( $cacheDir.'/ymcrss', octdec( $mode ) );
    }

    if ( !file_exists( $cacheFile ) or ( time() - filemtime( $cacheFile ) > $cacheTime ) or 1 == 1)
    {
        $xmlDoc =& $RSSExport->attribute( 'rss-xml' );

        $fid = @fopen( $cacheFile, 'w' );

        // If opening file for write access fails, write debug error
        if ( $fid === false )
        {
            eZDebug::writeError( 'Failed to open cache file for RSS export: '.$cacheFile );
        }
        else
        {
            // write, flush, close and change file access mode
            $mode = $config->variable( 'FileSettings', 'TemporaryPermissions' );
            $rssContent = $xmlDoc->toString();
            $length = fwrite( $fid, $rssContent );
            fflush( $fid );
            fclose( $fid );
            chmod( $cacheFile, octdec( $mode ) );

            if ( $length === false )
            {
                eZDebug::writeError( 'Failed to write to cache file for RSS export: '.$cacheFile );
            }
        }
    }
    else
    {
        $rssContent = file_get_contents( $cacheFile );
    }
}

// Set header settings
$httpCharset = eZTextCodec::httpCharset();
header( 'Content-Type: text/xml; charset=utf-8' );
header( 'Content-Length: '.strlen($rssContent) );
header( 'X-Powered-By: eZ publish' );

print_r($rssContent);

eZExecution::cleanExit();

?>
