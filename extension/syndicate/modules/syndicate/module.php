<?php
//
// Syndicate: an RSS Extension - Main module
//
// Created on: <18-Sep-2003 14:49:54 kk>
// Modified on: <10-Feb-2006 14:00:00 ymc-pabu>
//
// Copyright (C) 1999-2006 eZ systems as. All rights reserved.
//
// This source file is part of the eZ publish (tm) Open Source Content
// Management System.
//
// This file may be distributed and/or modified under the terms of the
// "GNU General Public License" version 2 as published by the Free
// Software Foundation and appearing in the file LICENSE included in
// the packaging of this file.
//
// Licencees holding a valid "eZ publish professional licence" version 2
// may use this file in accordance with the "eZ publish professional licence"
// version 2 Agreement provided with the Software.
//
// This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING
// THE WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR
// PURPOSE.
//
// The "GNU General Public License" (GPL) is available at
// http://www.gnu.org/copyleft/gpl.html.
//
// Contact licence@ymc.ch if any conditions of this licencing isn't clear to
// you.
//

$Module = array( 'name' => 'syndicate' );

$ViewList = array();
$ViewList['list'] = array(
    'script' => 'list.php',
    'functions' => array( 'list' ),
    'default_navigation_part' => 'ezsetupnavigationpart',
    'unordered_params' => array( 'language' => 'Language' ) );
    
$ViewList['success'] = array(
    'script' => 'success.php',
    'functions' => array( 'feed' ),
    'default_navigation_part' => 'ezsetupnavigationpart',
    'unordered_params' => array( 'feed' => 'Feed' ) );
    
$ViewList['removed'] = array(
    'script' => 'removed.php',
    'functions' => array( 'feed' ),
    'default_navigation_part' => 'ezsetupnavigationpart',
    'unordered_params' => array( 'feed' => 'Feed' ) );

$ViewList['edit_export'] = array(
    'script' => 'edit_export.php',
    'functions' => array( 'edit' ),
    'ui_context' => 'edit',
    'default_navigation_part' => 'ezsetupnavigationpart',
    'single_post_actions' => array( 'StoreButton' => 'Store',
                                    'Update_Item_Class' => 'UpdateItem',
                                    'AddSourceButton' => 'AddItem',
                                    'RemoveButton' => 'Cancel',
                                    'BrowseImageButton' => 'BrowseImage',
                                    'RemoveImageButton' => 'RemoveImage' ),                                    
    'params' => array( 'RSSExportID', 'RSSExportItemID', 'BrowseType' ) );

$ViewList['create'] = array(
    'script' => 'create_export.php',
    'functions' => array( 'create' ),
    'ui_context' => 'edit',
    'default_navigation_part' => 'ezsetupnavigationpart',
    'single_post_actions' => array( 'StoreButton' => 'Store',
                                    'Update_Item_Class' => 'UpdateItem',
                                    'AddSourceButton' => 'AddItem',
                                    'RemoveButton' => 'Cancel',
                                    'BrowseImageButton' => 'BrowseImage',
                                    'RemoveImageButton' => 'RemoveImage' ),                                    
    'params' => array( 'SourceID', 'ClassIdentifier' ) );



$ViewList['feed'] = array(
    'script' => 'feed.php',
    'functions' => array( 'feed' ),
    'params' => array ( 'RSSFeed' ) );

$Assigned = array(
    'name'=> 'Owner',
    'values'=> array(
        array(
            'Name' => 'Self',
            'value' => '1')
        )
    );
$ParentAssigned = array(
    'name'=> 'SourceOwner',
    'values'=> array(
        array(
            'Name' => 'Self',
            'value' => '1')
        )
    );
    
$FunctionList = array();
$FunctionList['feed'] = array();
$FunctionList['list'] = array();
$FunctionList['edit'] = array('Owner' => $Assigned);
$FunctionList['create'] = array('SourceOwner' => $ParentAssigned);

?>
