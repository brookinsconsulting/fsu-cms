<?php
//
// Syndicate: an RSS Extension - Export Edit Functions
//
// Created on: <18-Sep-2003 14:49:54 kk>
// Modified on: <10-Feb-2006 14:00:00 ymc-pabu>
//
// Copyright (C) 1999-2006 eZ systems as. All rights reserved.
//
// This source file is part of the eZ publish (tm) Open Source Content
// Management System.
//
// This file may be distributed and/or modified under the terms of the
// "GNU General Public License" version 2 as published by the Free
// Software Foundation and appearing in the file LICENSE included in
// the packaging of this file.
//
// Licencees holding a valid "eZ publish professional licence" version 2
// may use this file in accordance with the "eZ publish professional licence"
// version 2 Agreement provided with the Software.
//
// This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING
// THE WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR
// PURPOSE.
//
// The "GNU General Public License" (GPL) is available at
// http://www.gnu.org/copyleft/gpl.html.
//
// Contact licence@ymc.ch if any conditions of this licencing isn't clear to
// you.
//

include_once( 'extension/syndicate/classes/ymcrssexport.php' );
include_once( 'extension/syndicate/classes/ymcrssexportitem.php' );
include_once( 'kernel/classes/ezrssimport.php' );
include_once( 'lib/ezutils/classes/ezhttppersistence.php' );

/*!
 Store RSSExport

 \param Module
 \param HTTP
 \param publish ( true/false )
*/
function storeRSSExport( $Module, $http, $publish = false )
{
    /* Kill the RSS cache */
    $config = eZINI::instance( 'site.ini' );
    $cacheDir = eZSys::cacheDirectory();
    $cacheFile = $cacheDir . '/ymcrss/' . md5( $http->postVariable( 'Access_URL' ) ) . '.xml';
    if ( file_exists( $cacheFile ) )
        unlink( $cacheFile );

    $db = eZDB::instance();
    $db->begin();
    /* Create the new RSS feed */
    for ( $itemCount = 0; $itemCount < $http->postVariable( 'Item_Count' ); $itemCount++ )
    {
        $rssExportItem = ymcRSSExportItem::fetch( $http->postVariable( 'Item_ID_'.$itemCount ), true, EZ_RSSEXPORT_STATUS_DRAFT );
        if( $rssExportItem == null )
        {
            continue;
        }

        // RSS is supposed to feed certain objects from the subnodes
        if ( $http->hasPostVariable( 'Item_Subnodes_'.$itemCount ) )
        {
            $rssExportItem->setAttribute( 'subnodes', 1 );
        }
        else // Do not include subnodes
        {
            $rssExportItem->setAttribute( 'subnodes', 0 );
        }

        $rssExportItem->setAttribute( 'class_id', $http->postVariable( 'Item_Class_'.$itemCount ) );
        $rssExportItem->setAttribute( 'title', $http->postVariable( 'Item_Class_Attribute_Title_'.$itemCount ) );
        $rssExportItem->setAttribute( 'description', implode(",", $http->postVariable( 'Item_Class_Attribute_Description_'.$itemCount ) ));
        $rssExportItem->setAttribute( 'enclosure', implode(",", $http->postVariable( 'Item_Class_Attribute_Enclosure_'.$itemCount ) ));

        if( $publish )
        {
            $rssExportItem->setAttribute( 'status', 1 );
            $rssExportItem->store();
            // delete drafts
            $rssExportItem->setAttribute( 'status', 0 );
            $rssExportItem->remove();
        }
        else
        {

            $rssExportItem->store();
        }
    }

    
    $rssExport = ymcRSSExport::fetch( $http->postVariable( 'RSSExport_ID' ), true, EZ_RSSEXPORT_STATUS_DRAFT );
    $items = $rssExport->fetchItems();
    foreach ($items as $rssExportItem)
    {
        if( $publish )
        {
            $rssExportItem->setAttribute( 'status', 1 );
            $rssExportItem->store();
            // delete drafts
            $rssExportItem->setAttribute( 'status', 0 );
            $rssExportItem->remove();
        }
        else
        {
            $rssExportItem->store();
        }
    }
    
    if ($http->hasPostVariable('title'))
        $rssExport->setAttribute( 'title', $http->postVariable( 'title' ) );
    if ($http->hasPostVariable('SiteAccess'))
        $rssExport->setAttribute( 'site_access', $http->postVariable( 'SiteAccess' ) );
    if ($http->hasPostVariable('Description'))
        $rssExport->setAttribute( 'description', $http->postVariable( 'Description' ) );
    if ($http->hasPostVariable('RSSVersion'))
        $rssExport->setAttribute( 'rss_version', $http->postVariable( 'RSSVersion' ) );
    if ($http->hasPostVariable('NumberOfObjects'))
        $rssExport->setAttribute( 'number_of_objects', $http->postVariable( 'NumberOfObjects' ) );
    if ($http->hasPostVariable('RSSImageID'))
        $rssExport->setAttribute( 'image_id', $http->postVariable( 'RSSImageID' ) );
    if ( $http->hasPostVariable( 'active' ) )
    {
        $rssExport->setAttribute( 'active', 1 );
    }
    
    if ($http->hasPostVariable( 'Access_URL' ))
    $rssExport->setAttribute( 'access_url', str_replace( array( '/', '?', '&', '>', '<' ), '',  $http->postVariable( 'Access_URL' ) ) );
    if ( $http->hasPostVariable( 'MainNodeOnly' ) )
    {
        $rssExport->setAttribute( 'main_node_only', 1 );
    }
    else
    {
        $rssExport->setAttribute( 'main_node_only', 0 );
    }

    if ( $publish )
    {
        $rssExport->store( true );
        // remove draft
        $rssExport->remove();
        $db->commit();
        return $Module->redirectTo( '/syndicate/success' );
    }
    else
    {
        $rssExport->store();
    }
    $db->commit();
}



?>
