<?php

class tclinkOperator
{
var $Operators;

	function tclinkOperator()
   {
	$this->Operators = array("tclink");
   }


	function operatorList()
   {
	return $this->Operators;
   }


	function namedParameterPerOperator()
   {
        return true;
   }   

	function namedParameterList()
   {
        return array('tclink' => array( 'quotes' => array('type'=>'number', 'required'=>false, 'default'=>0),
					'virtual'=> array('type'=>'number', 'required'=>false, 'default'=>0) ));
   }

	function modify($tpl, &$operatorName, &$operatorParameters, &$rootNamespace, &$currentNamespace, &$operatorValue, &$namedParameters)
   {
eZDebug::writeDebug($operatorValue,'PreRun');
	      if(is_string($operatorValue)) {
		$PathNodeID = eZURLAliasML::fetchNodeIDByPath($operatorValue);
//eZDebug::writeDebug((int)$PathNodeID,'PathNodeID: '.$PathNodeID);
		      if(is_numeric($PathNodeID) && $PathNode = eZContentObjectTreeNode::fetch($PathNodeID)){
			$operatorValue = $PathNode;
		   } else {$operatorValue = "/$operatorValue"; return true;}
	   }
//eZDebug::writeDebug('PostRun','PostRun');
	$tcLink = new tclink($operatorValue, $namedParameters['quotes'], $namedParameters['virtual']);
/*
	$addQuotes = (bool) $namedParameters['href'][0];
	$addRoot = (bool) $namedParameters['href'][1];
*/
if(is_object($operatorValue)){
eZDebug::writeDebug($operatorValue->ClassIdentifier,'ClassIdentifier');}
	      if (is_object($operatorValue) && $operatorValue->ClassIdentifier=='link') {
		$dataMap = $operatorValue->dataMap();
		$operatorValue = $dataMap['location']->content();
	   }
	      else {
		$pathPrefix=tclink::getIniSetting('SiteAccessSettings','PathPrefix','site.ini');
		      if (tclink::getIniSetting('SiteAccessSettings','MatchOrder','site.ini')=='host') {
			$linkSiteaccessHost=tclink::getSiteaccessHost($operatorValue->pathArray(), $pathPrefix);
			$operatorValue = tclink::getHyperlink($operatorValue->pathWithNames(), $pathPrefix, $linkSiteaccessHost);
		   }
		      else {$operatorValue = tclink::getHyperlink($operatorValue->pathWithNames());}
	   }
eZDebug::writeDebug($operatorValue,'Final URL');
	return true;
   }
}

?>
