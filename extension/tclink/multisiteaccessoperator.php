<?php

class multiSiteaccessOperator
{
var $Operators;

	function multiSiteaccessOperator()
   {
	$this->Operators = array("multi_siteaccess");
   }


	function operatorList()
   {
	return $this->Operators;
   }


	function namedParameterPerOperator()
   {
        return true;
   }   

	function namedParameterList()
   {
        return array('multi_siteaccess' => array( 'node_id' => array('type'=>'boolean', 'required'=>false, 'default'=>true) ));
   }

	function modify($tpl, &$operatorName, &$operatorParameters, &$rootNamespace, &$currentNamespace, &$operatorValue, &$namedParameters)
   {
	$node = $namedParameters['node_id'] ? ezContentObjectTreeNode::fetch($operatorValue) : eZContentObjectTreeNode::findMainNode($operatorValue,true);
	if (!is_object($node)) return true; 
	      if(tclink::getIniSetting('SiteAccessSettings','MatchOrder','site.ini')=='host') {
		$siteaccesses=array();
		      foreach(tclink::getIniSetting('SiteAccessSettings','HostMatchMapItems','site.ini') as $hostMatchItem) {
			$hostItem = explode(';',$hostMatchItem);
			      if(in_array($hostItem[1], tclink::getIniSetting('SiteAccessSettings','RelatedSiteAccessList','site.ini'))) {
				$siteaccesses[$hostItem[1]]=$hostItem[0];
			   }
		   }
		      foreach ($siteaccesses as $name => $host) {
			$rootNodeID = tclink::getIniSetting('NodeSettings','RootNode','content.ini','settings/siteaccess/'.$name);
			   if(in_array($rootNodeID, $node->pathArray())) {$operatorValue = $name; break;}
		   }
	   }
	return true;
   }
}

?>
