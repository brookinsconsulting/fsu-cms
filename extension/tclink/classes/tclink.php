<?php

class tclink
{

	function tclink($linkNode, $useQuotes=false, $virtual=true, $useRelative=true)
   {
	$this->linkNode = $linkNode;
	$this->useQuotes = (bool) $useQuotes;
	$this->virtual = (bool) $virtual;
	$this->useRelative = (bool) $useRelative;
   }

	static function getSiteaccessHost($linkNodePath, &$pathPrefix=false) {
eZDebug::writeDebug(func_get_args(),'getSiteaccessHost() Arguments');
	$siteaccesses=array();
	      foreach (tclink::getIniSetting('SiteAccessSettings','HostMatchMapItems','site.ini') as $hostMatchItem) {
		$hostItem = explode(';',$hostMatchItem);
		$siteaccesses[$hostItem[1]]=$hostItem[0];
	   }
	      foreach ($siteaccesses as $name => $host) {
		$rootNodeID = tclink::getIniSetting('NodeSettings','RootNode','content.ini','settings/siteaccess/'.$name);
		$siteaccessPathPrefix = tclink::getIniSetting('SiteAccessSettings','PathPrefix','site.ini','settings/siteaccess/'.$name);
eZDebug::writeDebug($rootNodeID.'|'.$siteaccessPathPrefix,"Site Access Item: $name ($host)");
		      if (in_array($rootNodeID,$linkNodePath)) {
			   if ($pathPrefix) {$pathPrefix=$siteaccessPathPrefix;}
			return $host;
		   }
	   }
	return false;
   }

	static function getIniSetting($blockName, $varName, $fileName, $rootDir='settings') {
	$ini = eZINI::instance($fileName, $rootDir, null, null, null, true);
	      if ($ini->hasSection($blockName) && $ini->hasVariable($blockName, $varName)) {
		return $ini->variable($blockName,$varName);
	   }
	return false;
   }

	static function getHyperlink($urlPath, $pathPrefix=false, $linkHost=false) {
eZDebug::writeDebug(func_get_args(),'getHyperlink() Arguments');
	$linkPath = ($pathPrefix)?preg_replace('/^'.$pathPrefix.'/','',$urlPath):$urlPath;
	   if ($linkHost) {return 'http://' . $linkHost . $linkPath;}
		else {return "/$linkPath";}
   }

}

?>