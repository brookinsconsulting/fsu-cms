<?php

include_once( "extension/video/json.php" );

if ($_GET['testmode'] == 1) $_POST = array('vid'=>'5081740a95b58', 'action'=>'create' );

if (!isset($_POST['vid'])) die('No vid id provided!');

//ob_start();
//print_r($_POST);
//$out = ob_get_contents();
//ob_end_flush();
//mail('david@thinkcreative.com', 'Test', $out);




class twistageVideo {
	
	function twistageVideo($vid) {
		$this->VidID = $vid;
		$json = new Services_JSON();
		$key = $this->authenticate('1f4e97f28c7f5', false);
		$data_url = 'http://service.twistage.com/videos/'.$vid.'.json?signature='.$key;
	   	$resource = fopen($data_url, "r");
	    if ($resource) {
			$json_data = '';
			while (!feof($resource)) {
		   		$json_data .= fread($resource, 8192);
			}
			$vid_data = $json->decode($json_data);
			$publisher = $vid_data->publisher_name; 
			if ($publisher == '') {$this->PublisherName = 'admin';} else {$this->PublisherName = $publisher;}
			$this->Title = $vid_data->title; 
			$this->Description = $vid_data->description; 
			$this->add_tags($vid_data->tags);
	   	} else {
			$this->Error = "No data resource!";
	   	}

	}
	
	function add_tags($tags) {
		$addme = array();
		foreach ($tags as $t) {
			$addme[]=$t->name;
		}
		$this->Tags = implode(', ', $addme);
	}
	
	
	function authenticate($licenseKey, $publishUser) {
	   $url = "http://service.twistage.com/api/";
	   $url .= $publishUser ? "publish_key" : "view_key";
	   $url .= "?authKey=" . urlencode($licenseKey);
	   if ($publishUser) $url .= "&userID=" . urlencode($publishUser);

	   $handle = fopen($url, "r");
	   if ($handle) {
		$signature = fread($handle, 40);
		fclose($handle);
	   } else {
		$signature = "";
	   }
	   return $signature;
	}
	
	
	public $VidID;
	public $PublisherName;
	public $Title;
	public $Description;
	public $Tags;
	public $Error;
	
}





function setEZXMLAttribute( $attribute, $attributeValue, $link = false )
{
    $contentObjectID = $attribute->attribute( "contentobject_id" );
    $parser = new eZSimplifiedXMLInputParser( $contentObjectID, false, 0, false );

    $attributeValue = str_replace( "\r", '', $attributeValue );
    $attributeValue = str_replace( "\n", '', $attributeValue );
    $attributeValue = str_replace( "\t", ' ', $attributeValue );

    $document = $parser->process( $attributeValue );
    if ( !is_object( $document ) )
    {
        $cli = eZCLI::instance();
        $cli->output( 'Error in xml parsing' );
        return;
    }
    $domString = eZXMLTextType::domString( $document );

    $attribute->setAttribute( 'data_text', $domString );
    $attribute->store();
}









function setVersionData($version, $data_hash) {
	$dataMap = $version->dataMap();

	foreach ($data_hash as $setme => $value) {
	
		$objectAttribute = $dataMap[$setme];

		$dataType = $objectAttribute->attribute( 'data_type_string' );

		switch( $dataType )
		{
		  case 'ezxmltext':
		  {
		      setEZXMLAttribute( $objectAttribute, $value );
		  } break;

		  case 'ezurl':
		  {
		      $objectAttribute->setContent( $value );
		  } break;

		  case 'ezkeyword':
		  {
		      $keyword = new eZKeyword();
		      $keyword->initializeKeyword( $value );
		      $objectAttribute->setContent( $keyword );
		  } break;

		  case 'ezdate':
		  {
		      $timestamp = strtotime( $value );
		      if ( $timestamp )
		          $objectAttribute->setAttribute( 'data_int', $timestamp );
		  } break;

		  case 'ezdatetime':
		  {
		      $objectAttribute->setAttribute( 'data_int', strtotime($value) );
		  } break;

		  default:
		  {
		      $objectAttribute->setAttribute( 'data_text', $value );
		  } break;
		}

		$objectAttribute->store();
	}

}




if (array_key_exists('action', $_POST)) switch($_POST['action']) {
	
case "update":

   $twist = new twistageVideo($_POST['vid']);

   if (isset($_POST['target_node'])) break;

   if (isset($_POST['asset_action'])) break;

   if (isset($_POST['status'])) break;

   $editor = eZUser::fetch($_POST['user_id']);

   $editor->loginCurrent();

   $db = eZDB::instance();
   $sql = "SELECT distinct contentobject_id FROM ezcontentobject_attribute, ezcontentobject WHERE ezcontentobject.status =1 AND ezcontentobject.id = ezcontentobject_attribute.contentobject_id AND data_text = '".$_POST['vid']."'";
   $rows = $db->arrayQuery($sql );
   $rows = is_array($rows) ? $rows[0] : $rows;

   $ex_object = eZContentObject::fetch($rows['contentobject_id']);
   $my_node_id = $ex_object->mainNodeID();

   $curNode = eZContentObjectTreeNode::fetch( $my_node_id );

   if (array_key_exists('status', $_POST) && $_POST['status'] == 'trash' && !$curNode->attribute( 'is_hidden' ) ) {
	eZContentObjectTreeNode::hideSubTree( $curNode );
	break;
   }
   if (array_key_exists('status', $_POST) && $_POST['status'] == 'available' && $curNode->attribute( 'is_hidden' ) ) {
	eZContentObjectTreeNode::unhideSubTree( $curNode );
	break;
   }
	$contentObject = $ex_object;
	
	$current_version = $ex_object->nextVersion();
	
	$version = $ex_object->createNewVersion();
	
	$contentObjectID = $ex_object->attribute( 'id' );
	
	$data_hash = array('title' => $page_name, 'description' => $twist->Description, 'tags' => $twist->Tags);
	
	setVersionData($version, $data_hash);

	$operationResult = eZOperationHandler::execute( 'content', 'publish', array( 'object_id' => $contentObjectID,
	                                                                             'version' => $current_version ) );
	$assignedNodes = $contentObject->assignedNodes();

break;




case "create":

	$class_id = 51;

	$twist = new twistageVideo($_POST['vid']);

	$page_name = $twist->Title;
	
	$parent_id = isset($_POST['target_node']) ? $_POST['target_node'] : 12351;
	$userID = isset($_POST['user_id']) ? $_POST['user_id'] : 195;
	
	$editor = eZUser::fetch($userID);

    $editor->loginCurrent();
   
	$parent = eZContentObjectTreeNode::fetch($parent_id);

	$object = $parent->object();

	$remoteID = "twistagehookadded_".$page_name;

	$class = eZContentClass::fetch( $class_id );

	// Create object by user id in the section of the parent object
	$contentObject = $class->instantiate( $userID, $object->attribute( 'section_id' ) );
	$contentObject->setAttribute('remote_id', $remoteID );
	$contentObject->setAttribute( 'name', $page_name );

	$nodeAssignment = eZNodeAssignment::create( array(
	                                                 'contentobject_id' => $contentObject->attribute( 'id' ),
	                                                 'contentobject_version' => $contentObject->attribute( 'current_version' ),
	                                                 'parent_node' => $parent->attribute( 'node_id' ),
	                                                 'sort_field' => 2,
	                                                 'sort_order' => 0,
	                                                 'is_main' => 1
	                                                 )
	                                             );
	$nodeAssignment->store();
	
	$version = $contentObject->version( 1 );
	
	$contentObjectID = $contentObject->attribute( 'id' );
	
	$current_version = 1;
	
	$data_hash = array('title' => $page_name, 'vid' => $_POST['vid'], 'description' => $twist->Description, 'tags' => $twist->Tags);

	setVersionData($version, $data_hash);

	$operationResult = eZOperationHandler::execute( 'content', 'publish', array( 'object_id' => $contentObjectID,
	                                                                             'version' => $current_version ) );
	$assignedNodes = $contentObject->assignedNodes();
	
	print_r('...created.');

break;





case "delete":
   $db = eZDB::instance();
   $sql = "SELECT distinct contentobject_id FROM ezcontentobject_attribute WHERE data_text = '".$_POST['vid']."'";
   $rows = $db->arrayQuery($sql );
   $rows = is_array($rows) ? $rows[0] : $rows;
   $my_object = eZContentObject::fetch($rows['contentobject_id']);
   $my_node_id = $my_object->mainNodeID();
   eZContentObjectTreeNode::removeSubtrees( array($my_node_id), $moveToTrash );
break;



case "error":

   mail("david@thinkcreative.com", "Twistage error", "An error with Twistage video id".$_POST['vid']);

break;




default:
   Print 'Unknown POST action specified';

eZCache::clearByTag( 'content' );

} else {
   Print 'No POST action specified';
}

eZExecution::cleanExit();

?>
