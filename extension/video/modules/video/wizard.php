<?php 

$target_node = $_POST['NodeID'];
$referer = $_SERVER['HTTP_REFERER'];

include_once( 'kernel/classes/datatypes/ezuser/ezuser.php' );

$user = eZUser::currentUser();
$userObject = $user->contentObject();
$userName = $user->Login;
$userID = $user->attribute( 'contentobject_id' );

function authenticate($licenseKey, $publishUser) {
    $url = "http://service.twistage.com/api/";
    $url .= $publishUser ? "publish_key" : "view_key";
    $url .= "?authKey=" . urlencode($licenseKey);
    if ($publishUser) $url .= "&userID=" . urlencode($publishUser);

    $handle = fopen($url, "r");

    if ($handle) {
        $signature = fread($handle, 40);
        fclose($handle);
    } else {
        $signature = "";
    }
    return $signature;
}

$out = "";
  	 
$signature = authenticate("1f4e97f28c7f5", $userName);
if ($signature) {
    $out .= "<script type='text/javascript' src='http://service.twistage.com/api/publish_script'></script>";
    $out .= "<script type='text/javascript'>";
    $out .= "publishWizard('$signature', '$referer', { project_id : '1', user_id : $userID, target_node : $target_node } );";
    $out .= "</script>";
} else {
    $out .= "Error: authentication failed for $userName using $signature as signature";
}


$Result = array();
$Result['content'] = $out;
$Result['path'] = array( array( 'url' => '/video/wizard',
                                'text' => 'Add new video' ) );
$Result['pagelayout'] = 'pagelayout_upload.tpl';




?>

