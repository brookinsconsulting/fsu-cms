<?php

include_once('json.php');

class videoOperator
{
    var $Operators;

    function authenticate($licenseKey, $publishUser, $keyFilePath) {
    $url = "http://service.twistage.com/api/";
    $url .= $publishUser ? "publish_key" : "view_key";
    $url .= "?authKey=" . urlencode($licenseKey);
    if ($publishUser) $url .= "&userID=" . urlencode($publishUser);

    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    $data = curl_exec($ch);
    curl_close($ch);
	$data = $data ? $data : "";
	$file = eZClusterFileHandler::instance( "$keyFilePath" );
	$file->storeContents( $data, 'twistkey', 'txt' );
	$file->move( $keyFilePath );
    return $data;
    }

    function get_vid_data($vid, $key, $json) {
       $data_url = 'http://service.twistage.com/videos/'.$vid.'.json?signature='.$key;
       $resource = fopen($data_url, "r");

       if ($resource) {
	   $json_data = '';
	   while (!feof($resource)) {
	     $json_data .= fread($resource, 8192);
	   }
          return $json->decode($json_data);
       } else {
          return array("error" => "No data!");
       }
    }

    function videoOperator( $name = "video" )
    {
	$this->Operators = array( $name );
    }


    function &operatorList()
    {
	return $this->Operators;
    }


    function namedParameterPerOperator()
    {
        return true;
    }   

    function namedParameterList()
    {
        return array( 'video' => array() );
    }


    function modify( $tpl, $operatorName, $operatorParameters, &$rootNamespace, &$currentNamespace, &$operatorValue, &$namedParameters )
    {
	
		$ini = eZINI::instance();
		$User = eZUser::currentUser();
		$cacheFileArray = eZNodeviewfunctions::generateViewCacheFile($User, 0, 0, false, 'eng-US', false, array('vid'=>$operatorValue));
		$cacheDir = $cacheFileArray['cache_dir'];
		$stillneedvid = true;
		$keyFilePath = $cacheDir . "/twistkey.txt";
		
		if ( file_exists( $keyFilePath ) ) {
			$key = file_get_contents( $keyFilePath );
			if ($key == '') unset($key);
		}

		if ( !file_exists( $cacheDir ) )
		{
			$perm = octdec( $ini->variable( 'FileSettings', 'StorageDirPermissions' ) );
			eZDir::mkdir( $cacheDir, $perm, true );
		}

		$cacheFilePath = $cacheFileArray['cache_path'];

		if ( file_exists( $cacheFilePath ) ) {
			$last_write = @filemtime( $cacheFilePath );
			if (eZExpiryHandler::getTimestamp('content-view-cache',-1) < $last_write && (time() - $last_write) < 60*60 ) {
				$contents = file_get_contents( $cacheFilePath );
				$arr = unserialize($contents);
				$stillneedvid = false;
			}
		} 
		
		if ($stillneedvid) {
		
			$json = new Services_JSON();
			if (!isset($key)) {
				$key = $this->authenticate('1f4e97f28c7f5', false, $keyFilePath);
			}

			if ($key=='') {
				$operatorValue = array('description' => 'error', 'tags' => array('error'), 'total_size' => 0, 'hits_count' => 0, 'duration' => 0, 'screenshots' => array('error'));
				return false;
			}
			$vid_data = $this->get_vid_data($operatorValue,$key,$json);
			if (is_array($vid_data) && $vid_data['error'] == 'No data!') {
				$key = $this->authenticate('1f4e97f28c7f5', false, $keyFilePath);
				$vid_data = $this->get_vid_data($operatorValue,$key,$json);
			}
			$arr = (array) $vid_data;
			
		}

		foreach ($arr as $key => $bit) {
			$out = array();
			if (is_array($bit)) {
			  foreach ($bit as $k => $b) {
				if (is_object($b)) $out[$k] = (array) $b;
			  }
			  if ($out != array()) $arr[$key] = $out;
			}
		}
		
		if ($stillneedvid) {
			if (array_key_exists("main_asset_url", $arr) && $arr["main_asset_url"]) {
			$file = eZClusterFileHandler::instance( "$cacheFilePath" );
			$file->storeContents( serialize($arr), 'vidcache', 'txt' );
			$file->move( $cacheFilePath );
			}
		}

		$operatorValue = $arr;
    }


}

?>
