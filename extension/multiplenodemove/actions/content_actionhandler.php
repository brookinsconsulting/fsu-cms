<?php

$Module =& $module;

function multiplenodemove_ContentActionHandler(&$module, $http, $objectID) {

   $Module =& $Params["Module"];
   $this_node = eZContentObject::fetch($objectID, true);
   $nodeID = $this_node->mainNodeID();
   $selected_nodes = $http->PostVariable('DeleteIDArray' );
   $http->setSessionVariable( 'multiplenodemovenodes', $selected_nodes);
   $http->setSessionVariable( 'multiplenodemovehome', $nodeID);

   if ( $http->hasPostVariable( "multiplenodemoveButton" ) ) {
	$module->redirectTo( '/multiplenodemove/select' );
   } elseif ($http->hasPostVariable( "multiaddlocationsButton" )) {
	$module->redirectTo( '/multiplenodemove/add_locs_select' );
   } else {
	return false;
   }
   return true;
}

?>

