<?php

include_once( "kernel/classes/ezcontentbrowse.php" );
include_once( "lib/ezutils/classes/ezhttptool.php" );

$module = $Params["Module"];

$http = eZHTTPTool::instance();


if ( $http->hasPostVariable( 'BrowseCancelButton' ) || $http->hasPostVariable( 'CancelButton' ) ) {
    if ( $http->hasPostVariable( 'BrowseCancelURI' ) ) {
        return $module->redirectTo( $http->postVariable( 'BrowseCancelURI' ) );
    } else if ( $http->hasPostVariable( 'CancelURI' ) ) {
        return $module->redirectTo( $http->postVariable( 'CancelURI' ) );
    }
}

$multiplenodemovenodes = $http->SessionVariable( 'multiplenodemovenodes' );

foreach ($multiplenodemovenodes as $nodeID) {

    $node = eZContentObjectTreeNode::fetch( $nodeID );
    if ( !$node )
        return $module->handleError( EZ_ERROR_KERNEL_NOT_AVAILABLE, 'kernel', array() );

    if ( !$node->canMoveFrom() )
        return $module->handleError( EZ_ERROR_KERNEL_ACCESS_DENIED, 'kernel', array() );

    $object = $node->object();
    if ( !$object )
        return $module->handleError( EZ_ERROR_KERNEL_NOT_AVAILABLE, 'kernel', array() );
    $objectID = $object->attribute( 'id' );
    $class = $object->contentClass();
    $classID = $class->attribute( 'id' );

    $selectedNodeIDArray = eZContentBrowse::result( 'MoveNode' );
    $selectedNodeID = $selectedNodeIDArray[0];

    $selectedNode = eZContentObjectTreeNode::fetch( $selectedNodeID );

    if ( !$selectedNode )
    {
        eZDebug::writeWarning( "Content node with ID $selectedNodeID does not exist, cannot use that as parent node for node $nodeID",
                               'content/action' );
        return $module->redirectToView( 'view', array( 'full', 2 ) );
    }
    // check if the object can be moved to (under) the selected node
    if ( !$selectedNode->canMoveTo( $classID ) )
    {
        eZDebug::writeError( "Cannot move node $nodeID as child of parent node $selectedNodeID, the current user does not have create permission for class ID $classID",
                             'content/action' );
        return $module->redirectToView( 'view', array( 'full', 2 ) );
    }

    // Check if we try to move the node as child of itself or one of its children
    if ( in_array( $node->attribute( 'node_id' ), $selectedNode->pathArray()  ) )
    {
        eZDebug::writeError( "Cannot move node $nodeID as child of itself or one of its own children (node $selectedNodeID).",
                             'content/action' );
        return $module->redirectToView( 'view', array( 'full', $node->attribute( 'node_id' ) ) );
    }

    include_once( 'kernel/classes/ezcontentobjecttreenodeoperations.php' );
    if( !eZContentObjectTreeNodeOperations::move( $nodeID, $selectedNodeID ) )
    {
        eZDebug::writeError( "Failed to move node $nodeID as child of parent node $selectedNodeID",
                             'content/action' );
    }

}

return $module->redirectToView( 'view', array( 'full', $nodeID, '' ) ); 

?>