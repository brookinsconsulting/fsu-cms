<?php

include_once( "kernel/classes/ezcontentbrowse.php" );
include_once( "lib/ezutils/classes/ezhttptool.php" );

$module = $Params["Module"];

$http = eZHTTPTool::instance();


if ( $http->hasPostVariable( 'BrowseCancelButton' ) || $http->hasPostVariable( 'CancelButton' ) ) {
    if ( $http->hasPostVariable( 'BrowseCancelURI' ) ) {
        return $module->redirectTo( $http->postVariable( 'BrowseCancelURI' ) );
    } else if ( $http->hasPostVariable( 'CancelURI' ) ) {
        return $module->redirectTo( $http->postVariable( 'CancelURI' ) );
    }
}

$multiplenodemovenodes = $http->SessionVariable( 'multiplenodemovenodes' );

foreach ($multiplenodemovenodes as $nodeID) {

	 $node = eZContentObjectTreeNode::fetch( $nodeID );

	 $object = $node->object();

	 $objectID = $object->attribute( 'id' );

	 if ( !$object )
	 {
           return $module->handleError( EZ_ERROR_KERNEL_NOT_AVAILABLE, 'kernel' );
	 }

	 $user = eZUser::currentUser();
	 if ( !$object->checkAccess( 'edit' ) && !$user->attribute( 'has_manage_locations' ) )
	 {
           return $module->handleError( EZ_ERROR_KERNEL_ACCESS_DENIED, 'kernel' );
	 }

	 $existingNode = eZContentObjectTreeNode::fetch( $nodeID );
	 if ( !$existingNode )
	 {
           return $module->handleError( EZ_ERROR_KERNEL_NOT_AVAILABLE, 'kernel' );
	 }

	 $class = $object->contentClass();

        $selectedNodeIDArray = eZContentBrowse::result( 'AddNodeAssignment' );
        if ( !is_array( $selectedNodeIDArray ) )
            $selectedNodeIDArray = array();

        $nodeAssignmentList = eZNodeAssignment::fetchForObject( $objectID, $object->attribute( 'current_version' ), 0, false );
        $assignedNodes = $object->assignedNodes();

        $parentNodeIDArray = array();
        $setMainNode = false;
        $hasMainNode = false;
        foreach ( $assignedNodes as $assignedNode )
        {
            if ( $assignedNode->attribute( 'is_main' ) )
                $hasMainNode = true;

            $append = false;
            foreach ( $nodeAssignmentList as $nodeAssignment )
            {
                if ( $nodeAssignment['parent_node'] == $assignedNode->attribute( 'parent_node_id' ) )
                {
                    $append = true;
                    break;
                }
            }
            if ( $append )
            {
                $parentNodeIDArray[] = $assignedNode->attribute( 'parent_node_id' );
            }
        }
        if ( !$hasMainNode )
            $setMainNode = true;

        $mainNodeID = $existingNode->attribute( 'main_node_id' );
        $objectName = $object->attribute( 'name' );

        $db = eZDB::instance();
        $db->begin();
        $locationAdded = false;
        $node = eZContentObjectTreeNode::fetch( $nodeID );
        foreach ( $selectedNodeIDArray as $selectedNodeID )
        {
            if ( !in_array( $selectedNodeID, $parentNodeIDArray ) )
            {
                $parentNode = eZContentObjectTreeNode::fetch( $selectedNodeID );
                $parentNodeObject = $parentNode->attribute( 'object' );

                $canCreate = ( ( $parentNode->checkAccess( 'create', $class->attribute( 'id' ), $parentNodeObject->attribute( 'contentclass_id' ) ) == 1 ) ||
                               ( $parentNode->canAddLocation() && $node->canRead() ) );

                if ( $canCreate )
                {
                    $insertedNode = $object->addLocation( $selectedNodeID, true );

                    // Now set is as published and fix main_node_id
                    $insertedNode->setAttribute( 'contentobject_is_published', 1 );
                    $insertedNode->setAttribute( 'main_node_id', $node->attribute( 'main_node_id' ) );
                    $insertedNode->setAttribute( 'contentobject_version', $node->attribute( 'contentobject_version' ) );
                    // Make sure the path_identification_string is set correctly.
                    $insertedNode->updateSubTreePath();
                    $insertedNode->sync();

                    $locationAdded = true;
                }
            }
        }
}

if ( $locationAdded )
{
   if ( $object->attribute( 'contentclass_id' ) == $userClassID )
   {
	eZUser::cleanupCache();
   }
}
$db->commit();

include_once( 'kernel/classes/ezcontentcachemanager.php' );
eZContentCacheManager::clearContentCacheIfNeeded( $objectID );

return $module->redirectToView( 'view', array( 'full', $nodeID, '' ) ); 

?>

