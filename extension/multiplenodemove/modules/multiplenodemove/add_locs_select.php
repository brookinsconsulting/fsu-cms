<?php

    include_once( "lib/ezutils/classes/ezhttptool.php" );
    include_once( "kernel/classes/ezcontentbrowse.php" );
    $http = eZHTTPTool::instance();

    $multiplenodemovehome = $http->SessionVariable( 'multiplenodemovehome' );
    $multiplenodemovenodes = $http->SessionVariable( 'multiplenodemovenodes' );

    $Module = $Params["Module"];

    $ignoreNodesSelect = array();
    $ignoreNodesClick  = array();

    foreach ($multiplenodemovenodes as $nodeID) {

	 $node = eZContentObjectTreeNode::fetch( $nodeID );

	 $object = $node->object();

	 $objectID = $object->attribute( 'id' );

        $assigned = eZNodeAssignment::fetchForObject( $objectID, $object->attribute( 'current_version' ), 0, false );
        $publishedAssigned = $object->assignedNodes( false );
        $isTopLevel = false;

        foreach ( $publishedAssigned as $element )
        {
            $append = false;
            if ( $element['parent_node_id'] == 1 )
                $isTopLevel = true;
            foreach ( $assigned as $ass )
            {
                if ( $ass['parent_node'] == $element['parent_node_id'] )
                {
                    $append = true;
                    break;
                }
            }
            if ( $append )
            {
                $ignoreNodesSelect[] = $element['node_id'];
                $ignoreNodesClick[]  = $element['node_id'];
                $ignoreNodesSelect[] = $element['parent_node_id'];
            }
        } 

    }



    if ( !$isTopLevel )
    {
            $ignoreNodesSelect = array_unique( $ignoreNodesSelect );
            eZContentBrowse::browse( array( 'action_name' => 'AddNodeAssignment',
                                            'description_template' => 'design:multiplenodemove/browse_add_alt_location_node.tpl',
                                            'ignore_nodes_select' => $ignoreNodesSelect,
                                            'ignore_nodes_click'  => $ignoreNodesClick,
                                    'start_node' => 2,
                                    'cancel_page' => "/content/view/full/$multiplenodemovehome",
                                    'from_page' => "/multiplenodemove/addlocations" ),
                                    $Module );

            return;
    }


    return;

?>