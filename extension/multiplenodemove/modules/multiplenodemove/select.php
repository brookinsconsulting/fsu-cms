<?php

    include_once( "lib/ezutils/classes/ezhttptool.php" );
    include_once( "kernel/classes/ezcontentbrowse.php" );
    $http = eZHTTPTool::instance();

    $multiplenodemovehome = $http->SessionVariable( 'multiplenodemovehome' );
    $multiplenodemovenodes = $http->SessionVariable( 'multiplenodemovenodes' );

    $Module = $Params["Module"];

    $ignoreNodesSelect = array();
    $ignoreNodesSelectSubtree = array();
    $ignoreNodesClick = array();

    foreach ($multiplenodemovenodes  as $nodeID) {

    $node = eZContentObjectTreeNode::fetch( $nodeID );
    if ( !$node )
        return $module->handleError( EZ_ERROR_KERNEL_NOT_AVAILABLE, 'kernel', array() );

    if ( !$node->canMoveFrom() )
        return $module->handleError( EZ_ERROR_KERNEL_ACCESS_DENIED, 'kernel', array() );

    $object = $node->object();
    if ( !$object )
        return $module->handleError( EZ_ERROR_KERNEL_NOT_AVAILABLE, 'kernel', array() );
    $objectID = $object->attribute( 'id' );
    $class = $object->contentClass();

    $publishedAssigned = $object->assignedNodes( false );
    foreach ( $publishedAssigned as $element )
    {
        $ignoreNodesSelect[] = $element['node_id'];
        $ignoreNodesSelectSubtree[] = $element['node_id'];
        $ignoreNodesClick[]  = $element['node_id'];
        $ignoreNodesSelect[] = $element['parent_node_id'];
    }

    }

    $ignoreNodesSelect = array_unique( $ignoreNodesSelect );
    $ignoreNodesSelectSubtree = array_unique( $ignoreNodesSelectSubtree );
    $ignoreNodesClick = array_unique( $ignoreNodesClick );

    eZContentBrowse::browse( array( 'action_name' => 'MoveNode',
                                    'description_template' => 'design:multiplenodemove/browse_move_node.tpl',
                                    'ignore_nodes_select' => $ignoreNodesSelect,
                                    'ignore_nodes_select_subtree' => $ignoreNodesSelectSubtree,
                                    'ignore_nodes_click'  => $ignoreNodesClick,
                                    'start_node' => 2,
                                    'cancel_page' => "/content/view/full/$multiplenodemovehome",
                                    'from_page' => "/multiplenodemove/move" ),
                             $Module );

    return;

?>