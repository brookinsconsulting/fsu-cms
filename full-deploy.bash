#!/bin/bash -e

# script to make life a little easier when deploying changes for fsu
# intended to be run in ez.fsu.edu:/var/www/html
# this is a "catch-all" deploy; normally you won't need to do everything this does for every change
# i'm not an expert in bash, if you are reading this and you have suggestions please email me
# rw@thinkcreative.com

deploy="\
git pull --rebase origin master && \
chown -R apache:apache . || true && \
chmod -R 770 . || true && \
sudo -u apache php bin/php/ezpgenerateautoloads.php && \
sudo -u apache php bin/php/ezcache.php --clear-all \
"

read -p "This script assumes you are root and this repository ($(pwd)) has a clean working tree.
It will do the following:

$deploy

Here is the current git status:

$(git status)

Go for it? (y/N) " confirm

if [[ $confirm = y* ]]
then
	eval $deploy
else
	echo "Doing nothing."
fi
