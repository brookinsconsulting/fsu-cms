/*

=================================================================================================
>> EASYMENU - XHTML/CSS/DHTML Semantically correct drop down menu 
=================================================================================================
Author: 	Sam Hampton-Smith
Site: 		http://www.hampton-smith.com

Description:	This script takes a nested set of <ul>s and turns it into a fully functional
		DHTML menu. All that is required is the correct use of class names, and
		the application of some CSS. For the latest version of this script, go to
		http://www.easymenu.co.uk
		
Use:		Use of this code is free for all non-commercial applications. If you would like to 
		use the code on a commercial website, a link back to http://www.easymenu.co.uk is 
		required. You are free to make alterations to the code as required.
		

Copyright:	This script is copyright 2004-2005 hampton-smith limited. All rights reserved.


--------------------------------------------------------------------------------------------------
* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
--------------------------------------------------------------------------------------------------
Credits: 	Inspiration/Code borrowed from Dave Lindquist (http://www.gazingus.org)
		Menu hide functionality was aided by some code I found on http://www.jessett.com/
		Konqueror functionality was aided by some code Scott Wehrenberg emailed over
--------------------------------------------------------------------------------------------------
* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
--------------------------------------------------------------------------------------------------

==================================================================================================
>> REVISION HISTORY
==================================================================================================
Date		Version		Changes/Bug Fixes/New functionality added
--------------------------------------------------------------------------------------------------
10/01/2005	1.01		Fixed known issue where hovering over a sub-menu, then returning to 
				its parent menu not via the "starter" item on that menu, where the 
				new item moused over is a "starter" caused the two sub-menus 
				to overlay each other until a further movement of the mouse. 
				Fixed known issue where returning to parent menu via a non-"starter"
				menu item on the parent menu would not hide the child menu
08/01/2005	1.00		First full release of easymenu.
				Stylesheets now loaded in through JavaScript to improve accessibility.
01/12/2004	0.9		Base code inherited from client project
				Added suggestion from Scott Wehrenberg enabling Konqueror functionality
--------------------------------------------------------------------------------------------------



*/



	var currentMenu = null;
	var lastMenuStarter = null;
	var mytimer = null;
	var timerOn = false;
	var opera = window.opera ? true : false;

	if (!document.getElementById)
		document.getElementById = function() { return null; }

	function initialiseDummy(dummy, root) {
		dummy.onmouseover = function() {
			containingMenu = this.parentNode.parentNode;
			for (var x=0;x<containingMenu.childNodes.length;x++) {
				if (containingMenu.childNodes[x].nodeName.toUpperCase()=="LI") {
					if (containingMenu.childNodes[x].getElementsByTagName("ul").length>0) {
						containingMenu.childNodes[x].getElementsByTagName("UL").item(0).style.visibility = 'hidden';
					}
				}
			}
		}
		dummy.onfocus = function() {
			dummy.onmouseover();
		}
	}
	
	function initialiseMenu(menu, starter, root) {
		var leftstarter = false;
	
		if (menu == null || starter == null) return;
			currentMenu = menu;
	
		starter.onmouseover = function() {
			if (currentMenu) {
				if (this.parentNode.parentNode!=currentMenu) {
					currentMenu.style.visibility = "hidden";
					hideAllMenus(currentMenu, root);

				}
				if (this.parentNode.parentNode==root) {
					while (currentMenu.parentNode.parentNode!=root) {
						currentMenu.parentNode.parentNode.style.visibility = "hidden";
						currentMenu = currentMenu.parentNode.parentNode;
					}
				}
				currentMenu = null;
				this.showMenu();

	        	}
		}
	
		menu.onmouseover = function() {
			if (currentMenu) {
				currentMenu = null;
				this.showMenu();
	        	}
		}	
	
		starter.showMenu = function() {

			menu.style.visibility = "visible";
			currentMenu = menu;
		}

		starter.onfocus	 = function() {
			starter.onmouseover();
		}
	
		menu.onfocus	 = function() {
//			currentMenu.style.visibility="hidden";
		}

		menu.showMenu = function() {
			menu.style.visibility = "visible";
			currentMenu = menu;
			stopTime();
		}

		menu.hideMenu = function()  {
			if (!timerOn) {
				mytimer = setInterval("killMenu('" + this.id + "', '" + root.id + "');", 1000);
				timerOn = true;
				for (var x=0;x<menu.childNodes.length;x++) {
					if (menu.childNodes[x].nodeName=="LI") {
						if (menu.childNodes[x].getElementsByTagName("UL").length>0) {
							menuItem = menu.childNodes[x].getElementsByTagName("UL").item(0);
							menuItem.style.visibility = "hidden";
						}
					}
				}
			}
		}

		menu.onmouseout = function(event) {
			this.hideMenu();
		}
		menu.onblur = function() {
			this.hideMenu();
		}
		starter.onmouseout = function() {
			for (var x=0;x<menu.childNodes.length;x++) {
				if (menu.childNodes[x].nodeName=="LI") {
					if (menu.childNodes[x].getElementsByTagName("UL").length>0) {
						menuItem = menu.childNodes[x].getElementsByTagName("UL").item(0);
						menuItem.style.visibility = "hidden";
					}
				}
			}
			menu.style.visibility = "hidden";
		}
}
	function killMenu(menu, root) {
		var menu = document.getElementById(menu);
		var root = document.getElementById(root);
		menu.style.visibility = "hidden";
		for (var x=0;x<menu.childNodes.length;x++) {
			if (menu.childNodes[x].nodeName=="LI") {
				if (menu.childNodes[x].getElementsByTagName("UL").length>0) {
					menuItem = menu.childNodes[x].getElementsByTagName("UL").item(0);
					menuItem.style.visibility = "hidden";
				}
			}
		}
		while (menu.parentNode.parentNode!=root) {
			menu.parentNode.parentNode.style.visibility = "hidden";
			menu = menu.parentNode.parentNode;
		}
		stopTime();
	}
	function stopTime() {
		if (mytimer) {
		 	 clearInterval(mytimer);
			 mytimer = null;
			 timerOn = false;
		}
	} 

	function top_men_start() {
		var root = document.getElementById("menuList");
		var rootOffset = root.offsetLeft;
		getMenus(root, root);
	}



function getMenus(elementItem, root) {
	var selectedItem;
	var menuStarter;
	var menuItem;
	for (var x=1;x<elementItem.childNodes.length;x++) {
		if (elementItem.childNodes[x].nodeName.toUpperCase()=="LI") {
			if (elementItem.childNodes[x].getElementsByTagName("ul").length>0) {
				menuStarter = elementItem.childNodes[x].getElementsByTagName("A").item(0);
				menuItem = elementItem.childNodes[x].getElementsByTagName("UL").item(0);

			if (!opera) {
				if (menuStarter.parentNode.parentNode==root) {
					menuItem.style.left = menuStarter.offsetLeft + "px";
					menuItem.style.top = menuStarter.offsetTop + menuStarter.offsetHeight + "px";
					if (menuItem.offsetWidth < menuStarter.offsetWidth) menuItem.style.width = menuStarter.offsetWidth;
				}
				else {
				 	menuItem.style.left = menuStarter.offsetLeft + menuStarter.offsetWidth + "px";
				 	menuItem.style.top = menuStarter.offsetTop + "px";
				}
			}
			else {
				var rootOffset = root.offsetLeft;
				if (menuStarter.parentNode.parentNode==root) {
					menuItem.style.left = menuStarter.offsetLeft - rootOffset + "px";
					menuItem.style.width = menuStarter.offsetWidth;
					menuItem.style.top = menuStarter.offsetHeight + "px";
				}
				else {
				 	menuItem.style.left = menuStarter.offsetWidth - rootOffset + "px";
				 	menuItem.style.top = menuStarter.offsetTop + "px"; //menuItem.style.top - menuItem.style.offsetHeight + "px";
				}

			}

			/* ****** Hide form elements behind iframes ****** */

if (navigator.appVersion.indexOf("MSIE")!=-1) {

			menuItem.innerHTML = ('<iframe src="about:blank" scrolling="no" frameborder="0"></iframe>' + menuItem.innerHTML);
	var ieMat = menuItem.firstChild;
		ieMat.style.width=menuItem.offsetWidth+"px";
		ieMat.style.height=menuItem.offsetHeight+"px";	
		menuItem.style.zIndex="99";

}
			/* ****** End hide ****** */

				getMenus(menuItem, root);
				initialiseMenu(menuItem, menuStarter, root);
			}
			else {
			    if (elementItem.childNodes[x].getElementsByTagName("A").length) 
				initialiseDummy(elementItem.childNodes[x].getElementsByTagName("A").item(0), root);
			}
		}
	}
}
function hideAllMenus(elementItem, root) {
	for (var x=0;x<elementItem.childNodes.length;x++) {
		if (elementItem.childNodes[x].nodeName.toUpperCase()=="LI") {
			if (elementItem.childNodes[x].getElementsByTagName("ul").length>0) {
				elementItem.childNodes[x].getElementsByTagName("UL").item(0).style.visibility = 'hidden';
				hideAllMenus(elementItem.childNodes[x].getElementsByTagName("UL").item(0), root);
			}
		}
	}
}

EventWorker.addHandler("window.onload", top_men_start);