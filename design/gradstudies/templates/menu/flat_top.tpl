    {def $flat_top_loop = fetch( content, list, hash('parent_node_id', 1149) )}
    <ul id='menuList'>
	<li class='topmenuintro menubar'>Information for:</li>
    {foreach $flat_top_loop as $key_root => $this_menu}


{* turn off related object linking
	{if $this_menu.object.related_contentobject_array|count}
       <li class="menubar"><a class='starter' id='{concat('a',$this_menu.number)}' href={fetch('content', 'node', hash('node_id', $this_menu.object.related_contentobject_array[0].main_node_id)).url_alias|ezroot}>{$this_menu.object.data_map.short_name.has_content|choose($this_menu.name|wash, $this_menu.object.data_map.short_name.content|wash)}</a>
    {else}
    
    *}
       <li class="menubar{if eq(sum($key_root,1), $flat_top_loop|count)} last_one{/if}"><a class='starter' id='{concat('a',$this_menu.number)}' href={$this_menu.url_alias|ezroot}>{$this_menu.object.data_map.short_name.has_content|choose($this_menu.name|wash, $this_menu.object.data_map.short_name.content|wash)}</a>
    {* /if  *}

	{include name=SubFlat uri="design:menu/flat_top_loop.tpl" this_menu=$this_menu pass_class=concat('a',$this_menu.number) orig_class=concat('a',$this_menu.number)}

    {/foreach}

    </ul>
