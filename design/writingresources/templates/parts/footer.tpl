{let basenode = first_set($module_result.node_id,1710)
     patharray=fetch( 'content', 'node', hash( 'node_id', $basenode )).path_array|reverse
     nodeinpathfooterkids=array()
     myfooternode=array()}


{foreach $patharray as $thisnodeinpathid}

	{set nodeinpathfooterkids = fetch('content', 'list', hash('parent_node_id', $thisnodeinpathid, class_filter_type, 'include', class_filter_array, array('footer') ))}

	{if $nodeinpathfooterkids|count}
		{set myfooternode=$nodeinpathfooterkids.0}
	      {break}
	{/if}

{/foreach}


<div class="footer">

		
<div class="footerText">

{if $myfooternode.object.data_map.footer_text.has_content}
<div class='footernode_text'>
{$myfooternode.object.data_map.footer_text.content}
</div>
{/if}

{if $myfooternode|count}

{foreach $myfooternode.object.related_contentobject_array as $relatednode}

    {if $relatednode.data_map.location.content}

<a href={$relatednode.data_map.location.content}>{$relatednode.name|wash}</a>

    {elseif $relatednode.data_map.url.content}

<a href={$relatednode.data_map.url.content}>{$relatednode.name|wash}</a>

    {elseif $relatednode.data_map.internal_link.has_content}

<a href={fetch(content,object,hash(object_id,$this_menu.object.data_map.internal_link.content.relation_browse.0.contentobject_id ,object_version,$this_menu.object.data_map.internal_link.content.relation_browse.0.contentobject_version)).main_node.url_alias|ezroot}>{$relatednode.name|wash}</a>

    {else}

<a href={$relatednode.main_node.url_alias|ezroot}>{$relatednode.name|wash}</a>

    {/if}

    {delimiter}
    &nbsp;|&nbsp;
    {/delimiter}

{/foreach}

{/if}

</div>

</div>

{/let}

{if eq(module_params()['parameters']['ViewMode'], 'error')}
{literal}
<script type="text/javascript">
var gaJsHost = (("https:" == document.location.protocol) ? " https://ssl ." : "http://www.");
document.write(unescape("%3Cscript src='" + gaJsHost + " google-analytics.com/ga.js' type='text/javascript'%3E%3C/script%3E")); 
</script>

<script type="text/javascript">
var pageTracker = _gat._getTracker("UA-5213390-1");
pageTracker._trackPageview("/404.html?page=" + document.location.pathname + document.location.search + "&from=" + document.referrer);
</script>
{/literal}
{else}
{literal}
<script type="text/javascript">
var gaJsHost = (("https:" == document.location.protocol) ? "https://ssl." : "http://www.");
document.write(unescape("%3Cscript src='" + gaJsHost + "google-analytics.com/ga.js' type='text/javascript'%3E%3C/script%3E"));
</script>
<script type="text/javascript">
try {
var pageTracker = _gat._getTracker("UA-5213390-1");
pageTracker._trackPageview();
} catch(err) {}</script>
{/literal}{/if}