{def $targets = fetch(content, tree, hash(parent_node_id, $node.node_id, attribute_filter, array(array('article/flash_include', '=', 1)) ))}
<!--rw-->
<news>
	<data>
{foreach $targets as $this_target}
	{if $this_target.data_map.flash_image.content.is_valid}
	 <pic>
		<image><![CDATA[{$this_target.data_map.flash_image.content.supergraphic.url|ezroot('no')}]]></image>
		<caption><![CDATA[<p class='flashheadline'>{$this_target.data_map.flash_head.content}</p><br/><p class='flashsubtitle'>{$this_target.data_map.flash_sub_head.content}</p>]]></caption>
		<link><![CDATA[{fetch('content', 'node', hash('node_id', $this_target.main_node_id)).url_alias|ezroot('no')}]]></link> 
	 </pic>
	 {/if}
{/foreach}
	</data>
</news>
{undef}

