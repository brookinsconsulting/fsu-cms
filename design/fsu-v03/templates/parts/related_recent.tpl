{set-block scope=global variable=cache_ttl}0{/set-block}
<div class="related_recent" id='tabs'>
<ul>
	<li><a href='#related_items'>Related Items</a></li>
	<li><a href='#recent_items'>Recent Articles</a></li>
</ul>

<div id='related_items' class='tabdiv'>
<h4>Related Articles</h4>
{def $related = array()
     $limit = 3
     $offset = 0
	$site_root_node = ezini( 'NodeSettings', 'RootNode', 'content.ini' ) 
		}
{set $related=fetch( 'ezfind', 'moreLikeThis', hash( 'query_type', 'nid', 'query', $node.node_id, limit, 3, class_id, array(16), subtree_array, array($site_root_node) )) }
{def $relatedNodes = $node.data_map.tags.content.related_nodes|shuffle()}

	{if or( count($related.SearchResult), count($relatedNodes) )}
	   {if not(count($related.SearchResult))}{set $related = hash('SearchResult',$relatedNodes)}{/if}
			  <div class="split float-break">
			          <div class="three-left">
			              <div class="split-content">
			                  <!-- Content: START -->
			                  {if is_set( $related.SearchResult.0 )}
			                  {node_view_gui view=node_feed content_node=$related.SearchResult.0 expanded=true() image_class='articlefeed' summary=false() datetime='%F %j, %Y'}
			                  {/if}
			                  <!-- Content: END -->
			              </div>
			          </div>
			          <div class="three-right">
			              <div class="split-content">
			                  <!-- Content: START -->
			                  {if is_set( $related.SearchResult.2 )}
			                  {node_view_gui view=node_feed content_node=$related.SearchResult.2 expanded=true() image_class='articlefeed' summary=false() datetime='%F %j, %Y'}
			                  {/if}
			                  <!-- Content: END -->
			              </div>
			          </div>
			          <div class="three-center float-break">
			              <div class="split-content">
			                  <!-- Content: START -->
			                  {if is_set( $related.SearchResult.1 )}
			                  {node_view_gui view=node_feed content_node=$related.SearchResult.1 expanded=true() image_class='articlefeed' summary=false() datetime='%F %j, %Y'}
			                  {/if}
			                  <!-- Content: END -->
			              </div>
			          </div>
			 </div>
	{else}
					<div class="center"><h4>No Related Stories</h4></div>
	{/if}
			          <div class="break"></div>
</div>
<div id='recent_items' class='tabdiv'>
<h4>Recent Articles</h4>
{def	$recent = array() }

{if is_set( $object_parameters.limit )}
    {set $limit = $object_parameters.limit}
{/if}

{if is_set( $object_parameters.offset )}
    {set $offset = $object_parameters.offset}
{/if}

{set $recent=fetch( 'content', 'tree', hash( 'parent_node_id', $site_root_node, 
                                           'limit', $limit,
                                           'offset', $offset, 
                                           'class_filter_type', 'include',
                                           'class_filter_array', array( 'article' ),
                                           'sort_by', array(published,false()) ) ) }

     <div class="split float-break">
          <div class="three-left">
              <div class="split-content">
                  <!-- Content: START -->
                  {if is_set( $recent.0 )}
                  {node_view_gui view=node_feed content_node=$recent.0 expanded=true() image_class='articlefeed' summary=false() datetime='%F %j, %Y'}
                  {/if}
                  <!-- Content: END -->
              </div>
          </div>
          <div class="three-right">
              <div class="split-content">
                  <!-- Content: START -->
                  {if is_set( $recent.2 )}
                  {node_view_gui view=node_feed content_node=$recent.2 expanded=true() image_class='articlefeed' summary=false() datetime='%F %j, %Y'}
                  {/if}
                  <!-- Content: END -->
              </div>
          </div>
          <div class="three-center float-break">
              <div class="split-content">
                  <!-- Content: START -->
                  {if is_set( $recent.1 )}
                  {node_view_gui view=node_feed content_node=$recent.1 expanded=true() image_class='articlefeed' summary=false() datetime='%F %j, %Y'}
                  {/if}
                  <!-- Content: END -->
              </div>
          </div>
          <div class="break"></div>
      </div>

</div>
</div>