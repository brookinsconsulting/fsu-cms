<head>

{include uri='design:page_head.tpl'}
{$indexpage|debug}
<style type="text/css">
    @import url({"stylesheets/core.css"|ezdesign(double)});
    @import url({"stylesheets/pagelayout.css"|ezdesign(double)});
    @import url({"stylesheets/content.css"|ezdesign(double)});
    @import url({"stylesheets/websitetoolbar.css"|ezdesign(double)});
</style>
	{def $ini_css = cond(is_unset($load_css_file_list),ezini('StylesheetSettings', 'CSSFileList', 'design.ini'),false())
		 $frontend_css = cond(ezini_hasvariable('StylesheetSettings', 'FrontendCSSFileList', 'design.ini'),ezini('StylesheetSettings', 'FrontendCSSFileList', 'design.ini'),false())
		 $stylesheets_list = cond($module_result.node_id|ne($indexpage), array('styleLevel2.css', 'filmdev.css'), array('fsustyles.css', 'filmdev.css'))
	}
	{if $frontend_css}{set $ini_css=cond($ini_css,$ini_css|merge($frontend_css),$frontend_css)}{/if}
	{ezcss_load(cond($ini_css,$stylesheets_list|merge($ini_css),$stylesheets_list))}
	

<link rel="stylesheet" type="text/css" href={"stylesheets/print.css"|ezdesign} media="print" />
<!-- IE conditional comments; for bug fixes for different IE versions -->
<!--[if IE 5]>     <style type="text/css"> @import url({"stylesheets/browsers/ie5.css"|ezdesign(no)});    </style> <![endif]-->
<!--[if lte IE 7]> <style type="text/css"> @import url({"stylesheets/browsers/ie7lte.css"|ezdesign(no)}); </style> <![endif]-->

{literal}
<script>
sfHover = function() {
	if (document.getElementById("leftsidebar") != null) {
		var sfEls = document.getElementById("leftsidebar").getElementsByTagName("LI");
		for (var i=0; i<sfEls.length; i++) { 
			sfEls[i].onmouseover=function() {
				this.className+=" sfhover";
			}
			sfEls[i].onmouseout=function() {
				this.className=this.className.replace(new RegExp(" sfhover\\b"), "");
			}
		}
	}
}
if (window.attachEvent) window.attachEvent("onload", sfHover);

function EventWorker() {
  this.addHandler = EventWorker.addHandler;
}


EventWorker.addHandler = function (eventRef, func) {
 var eventHandlers = eval(eventRef);
 if (typeof eventHandlers == 'function') {
  eval(eventRef + " = function(event) {eventHandlers(event); func(event);}");  
 } else {
  eval(eventRef + " = func;");
 }
}

function getElementsByClass(node,searchClass,tag) {
  var classElements = new Array();
  var els = node.getElementsByTagName(tag); // use "*" for all elements
  var elsLen = els.length;
  var pattern = new RegExp("\\b"+searchClass+"\\b");
  for (i = 0, j = 0; i < elsLen; i++) {
    if ( pattern.test(els[i].className) ) {
      classElements[j] = els[i];
      j++;
    }
  }
  return classElements;
}
</script>
{/literal}

{def $ini_scripts = cond(is_unset($load_script_list),ezini('JavaScriptSettings','JavaScriptList','design.ini'),false())
	 $frontend_scripts = cond(ezini_hasvariable('JavaScriptSettings', 'FrontendJavaScriptList', 'design.ini'),ezini('JavaScriptSettings', 'FrontendJavaScriptList', 'design.ini'),false())
	 $javascript_list = array()
}
{if $frontend_scripts}{set $ini_scripts=cond($ini_scripts,$ini_scripts|merge($frontend_scripts),$frontend_scripts)}{/if}
{ezscript_load(cond($ini_scripts,$javascript_list|merge($ini_scripts),$javascript_list))}

<!--[if IE]>
<link rel="stylesheet" type="text/css" href="{'stylesheets/ie.css'|ezdesign(no)}"  />
<![endif]-->




</head>