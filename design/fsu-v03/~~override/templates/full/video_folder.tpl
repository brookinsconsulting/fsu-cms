{* Video Folder *}

{def  $tabs=fetch(content, list, hash(parent_node_id, $node.node_id, class_filter_type, 'include', class_filter_array, array('folder'), sort_by, $node.sort_array))
      $my_vids = array()
      $limit=first_set($object_parameters.limit,$limit,5)
      $my_videos=fetch(content, tree, hash(parent_node_id, $node.node_id, limit, 1, class_filter_type, 'include', class_filter_array, array('video','botr'), sort_by, array('published', 0)))
      $roundedImages = hash('outerTop','expert_topcorner.jpg',
			    'innerTop','Ask_the_Experts_subtop.gif',
			    'outerBottom','expert_botcorner.jpg',
			    'innerBottom','expertssub_bot.gif')
      $wideViewPage = true()}
{if $wideViewPage}
{set $roundedImages = hash('outerTop','videowide_topcorner.jpg',
			   'innerTop','videowide_subtop.gif',
			   'outerBottom','videowide_botcorner.jpg',
			   'innerBottom','videowide_subbot.gif')}
{/if}

<div class="content-view-full">
    <div class="class-folder multitabvideo">

        <div class="experttab">
		 <div class="expertvideos" id="expertvideos">
		 <h2><a name="three" id="three"></a></h2>
		 {node_view_gui content_node=$my_videos.0 view='full' wide_view=$wideView player_profile="nonautoplay_large"}
		 </div>
	        <div class="ask_subcontent">
	           <div class='vidbytab_box'>
				{foreach $tabs as $key => $this_tab}
					{set my_vids=fetch(content, tree, hash(parent_node_id, $this_tab.node_id, class_filter_type, 'include', class_filter_array, array('video', 'videoembed','botr'), sort_by, $this_tab.sort_array))}
				    {if $my_vids|count}
				    <div class='vidbytab_tab_holder node_id_{$this_tab.node_id}{if eq($key,0)} selected{/if}'>
					    <div class='vidbytab_body'>
					    	{foreach $my_vids as $this_vid}
					    	{node_view_gui content_node=$this_vid view='inline_vid' wide_view=$wideView}
					    	{/foreach}
					    </div>
					    <h3 class='vidbytab_tab' id='vbn_tab_{$key}'><a>{$this_tab.name|wash}</a></h3>{* <a href="$this_tab.url_alias|ezroot(no)"></a> *}
				    </div>
				    {/if}
				{/foreach}
					<script language="JavaScript">
					    set_vidbynode_tabs();
					</script>
			</div>
	            <div class="clear"></div>
	        </div>
	        <div class="clear"></div>

    </div>

   </div>
</div>

