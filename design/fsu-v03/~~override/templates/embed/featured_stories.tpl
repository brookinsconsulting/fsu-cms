{* Featured Stories - Embed *}
{def $featuredStories = fetch('content', 'list', hash('parent_node_id', $object.main_node_id,
						      'sort_by', array('priority', true()),
						      'class_filter_type', 'include',
						      'class_filter_array', array('article'),
						      'limit', $limit))
}


{def $useNodeID = $featuredStories[0].node_id
	 $currentNode = first_set($current_node, fetch('content','node',hash('node_id',$useNodeID)))
	 $bannerClass = first_set($banner_class, 'original')
	 $banners = fetch('content', 'list', hash('parent_node_id', $currentNode.node_id,
											  'sort_by', $currentNode.sort_array,
											  'class_filter_type', 'include',
											  'class_filter_array', array('image')))
	 $bannerItem = cond(count($banners),$banners[0].data_map.image.content[$bannerClass],false())
	 $bannerData=array()
	 $bannerItemData=false()
	 $bannerID=array()
}

{if count($banners)}
{foreach $banners as $key=>$banner}
{set $bannerItemData=hash('src', $banner.data_map.image.content[$bannerClass].url|sitelink('no'),
						 'alt', cond(ne($banner.data_map.image.content.alternative_text,''),$banner.data_map.image.content.alternative_text|wash(),$banner.name|wash()),
						 'link', $banner|sitelink('no'),
						 'size', hash('width',$banner.data_map.image.content[$bannerClass].width,'height',$banner.data_map.image.content[$bannerClass].height),
						 'new_window',cond(is_set($banner.data_map.new_window),$banner.data_map.new_window.data_int|choose(false(),true()),false())
						)
	 $bannerID=$bannerID|append($banner.name|wash()|md5())
}
{set-block variable="bannerdata"}
	{attribute_view_gui attribute=$banner.data_map.caption}
{/set-block}
{set $bannerItemData=$bannerItemData|merge(hash('data',$bannerdata))}
{set $bannerData=$bannerData|append($bannerItemData)}
{/foreach}

<div class="featured-stories">
<div class='tag_header'><h2>FEATURES</h2><h4>{currentdate()|datetime('custom', '%l, %F %j, %Y')}</h4></div>
<div id="banner-container">
<div id="banner-wrapper">
	<div id="banner-content"><div class="banner-content-wrap">{$bannerData[0].data}</div></div>
	<div id="banners">
		{foreach $bannerData as $key=>$banner}
		<img alt="{$banner.data}" src="{$banner.src}" width="{$banner.size.width}" height="{$banner.size.height}" />
		{/foreach}
	</div>
	<nav id="banner-controls"><ul class="menu horizontal"></ul></nav>
{if gt(count($bannerData),1)}
	{literal}
	<script type="text/javascript">
	(function(){
			var bannerContent=$('.banner-content-wrap');
			var first_time = true;
			$CycleRotation.register('banners',{
				'type':'slideshow',
				'selector':'#banners',
				'animation':{
					'timeout':5000,
					'pause':true,
					'pager':'#banner-controls .menu',
					'pagerAnchorBuilder':function(i,s){
						return '<li class="jump-control-'+i+'"><a href="#">'+(++i)+'</a></li>';
					}
				},
				'events':{
					'before':function(){
						if (!first_time) {
							$('#banner-content .banner-content-wrap').css('display', 'none').html($(this).attr('alt')).fadeIn();
						}
						first_time = false;
					}
				}
			});
	{/literal}{rdelim})();
	</script>
{/if}
</div>
</div>
{/if}

{node_view_gui content_node=$featuredStories[0] view='node_feed' expanded=true() length=400 image_class='featuredstory'}

</div>
