<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">

{def $basket_is_empty   = cond($current_user.is_logged_in, fetch( shop, basket ).is_empty, 1)
     $current_node_id   = first_set($module_result.node_id, 0)
     $user_hash         = concat($current_user.role_id_list|implode( ',' ), ',', $current_user.limited_assignment_value_list|implode( ',' ))}

{def $pagestyle       = 'nosidemenu noextrainfo'
     $infobox_count   = 0
     $locales         = fetch( 'content', 'translation_list' )
     $pagerootdepth   = ezini( 'SiteSettings', 'RootNodeDepth', 'site.ini' )
     $indexpage       = ezini('NodeSettings','RootNode','content.ini')
     $path_normalized = ''
     $pagedesign      = fetch( 'content', 'object', hash( 'object_id', '54' ) )
}

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en">

{include uri='design:parts/page_head.tpl'}

<body id='fsubody'>
<div class="accessibleText">Screenreader Navigation - [<a href="#tof">Skip to Content</a>
 
	&nbsp;|&nbsp;<a href="#mainNav">Skip to Main Navigation</a>
 
	]<hr />
</div>
<!--<div class="topBar"></div>-->
<div class="pageContainer">
  <div class="header">
  <a class='logotagwrap' href="/"><img src="/design/fsu-v03/images/film_logo.png" alt="Florida State University College of Motion Picture Arts" /><!--<div id='logotag'>COLLEGE OF MOTION PICTURE ARTS</div>--></a>
  
  <form class="headerSearchForm" method="get" action="http://www.google.com/search">
	<input type="hidden" name="cof" value="GIMP:#000000;T:#000000;ALC:#cc0000;GFNT:#666666;LC:#660000;BGC:#ffffff;AH:center;VLC:#996600;GL:0;S:http://www.fsu.com;GALT:#000000;AWFID:f0dcc05e5725f941;" />
	<input type="hidden" name="domains" value="fsu.edu;film.fsu.edu" />
    <div class="searchBar">
      <select class="headerSearchSelect" name="sitesearch">
        <option selected="selected" value="">Web</option>
        <option value="film.fsu.edu">Film.fsu.edu</option>
        <option value="fsu.edu">FSU.edu</option>
      </select>
      <input class="headerSearchBox" type="text" onclick="this.value=''" value="Search" size="20" name="q"/>
      <input class="headerSubmitSearch" type="image" alt="Submit Search" src="/design/filmdev/images/fsuimages/fsuSearch2.gif" name="sa" />
    </div>
  </form>

  </div>

<div style="clear:both"></div>
<!--
<div class="topAlert">
<a href="http://www.fsu.edu/~alerts/"><h1>FSU ALERT!</h1> <p>For further information, please click this banner to be redirected to the FSU Alert Web page.</p></a>
</div>
-->

	<div class="tofContainer">
				
		<div class="tof">
		
		<div id="flashcontent">
	      
		  {include uri='design:parts/supergraphic.tpl'}
			
		</div>
        
        </div>
        
	</div>
	
<div class="mainContent">
<!--added 12/17/12-->
<div class="infoBox">
{include uri='design:parts/extra_info.tpl' infoboxes=$my_page_notes}
</div>
<!--end-->

<div class='main_wrap'>
{$module_result.content}


</div>
	
</div>

  <div style="clear:both; height:15px;"></div>
            
            <div id="mainmenu">

{include uri='design:parts/navmenu.tpl'}

<!--<div style="clear:both; padding-top:20px;"></div>
<ul class="socialmedia">
  <li><a href="/socialmedia/" class="keySites" style="text-align:left;">FOLLOW US:</a></li>
  <li><a href="/socialmedia/" class="socialmedia"></a></li>
</ul>-->
	
      		</div>
            

</div>
<div class="accessibleText"><hr /></div>
	<div class="footer">		
	  <div class="footerText">
	{include uri='design:parts/footer.tpl'}
    </div>
	</div>




</body>
</html>
