{* Expert Override - Full view *} 
<div class="content-view-full">
	<div class="class-faculty">
		<div id="user-top">
			<h1>
				{$node.object.data_map.first_name.content|wash()} {$node.object.data_map.last_name.content|wash()}
			</h1>
				<div class="attribute-title">
		{attribute_view_gui attribute=$node.object.data_map.position} 
				</div>
		</div>
		<div id="maindetails">
				{section show=$node.object.data_map.additional_information.content} 
				<div id='user-additionalinfo'>
			<!--<h4>Additional Information</h4>-->
{let matrix=$node.object.data_map.additional_information.content} {section loop=$matrix.rows.sequential} {section show=$:item.columns.1} {* Show the field's value *} 
				<div class="MatrixSpec">
					<div class="MatrixSpecName">
						{$:item.columns.0}:&nbsp; 
					</div>
					<div class="MatrixSpecResult">
						{$:item.columns.1} 
					</div>
				</div>
				{/section} {/section} {/let} {/section} 
			<h4>Contact Information</h4>
{section show=$node.object.data_map.telephone.content} 
				<div class="MatrixSpec">
					<label>Phone:</label> {attribute_view_gui attribute=$node.object.data_map.telephone} 
				</div>
				{/section} 

			 {if eq($node.object.data_map.hideemail.value,0)}
				<div class="MatrixSpec">
						<label>Email:</label>
						<a href='{concat("mailto:", $node.object.data_map.user_account.content.email)}'>
 {$node.object.data_map.user_account.content.email}
					</a>
				</div>
			{/if}


			</div>
			{section show=$node.object.data_map.profile.has_content} 
			<div class="attribute-long">
			{section show=first_set($node.object.data_map.image.content, $related_object.object.data_map.image.content)} 
			<div id='user-image'>
				{attribute_view_gui attribute=first_set($node.object.data_map.image, $related_object.object.data_map.image) image_class='medium'} 
			</div>
			{/section} 
				{attribute_view_gui attribute=$node.object.data_map.profile} 
			</div>
			{/section} 
			<div class='clear'>
            {let page_limit=10
                 list_items=array()
                 list_count=0}
						{set list_count=fetch( 'content', 'list_count', hash( 'parent_node_id', $node.node_id, class_filter_type, exclude, class_filter_array, array('infobox', 'quicklinks') ) )}
								{section show=gt($list_count,0)}
									<hr />
									
										{set list_items=fetch_alias( children, hash( parent_node_id, $node.node_id,
																																 offset, $view_parameters.offset,
																																 sort_by, $node.sort_array,
																																 class_filter_type, exclude,
																																 class_filter_array, array('infobox', 'quicklinks'),
																																 limit, $page_limit ) )}
										<div class="content-view-children">
												{section var=child loop=$list_items sequence=array(bglight,bgdark)}
														{node_view_gui view=line content_node=$child}
												{/section}
										</div>
												{include name=navigator
														 uri='design:navigator/google.tpl'
														 page_uri=$node.url_alias
														 item_count=$list_count
														 view_parameters=$view_parameters
														 item_limit=$page_limit}
								{/section}    
            {/let}
			</div>
		</div>
	</div>
</div>
