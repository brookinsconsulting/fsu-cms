{* Folder - Full view *}


<div class="content-view-full">
    <div class="class-folder {if $node.path_string|explode('/')|contains(2010)}faculty_section{/if}">

		<div class="attribute-header">
        	<h1>{attribute_view_gui attribute=$node.data_map.name}</h1>
		</div>
		
		{if eq( ezini( 'folder', 'SummaryInFullView', 'content.ini' ), 'enabled' )}
        	{if $node.object.data_map.short_description.has_content}
            	<div class="attribute-short">
                	{attribute_view_gui attribute=$node.data_map.short_description}
            	</div>
        	{/if}
		{/if}

        {if $node.object.data_map.description.has_content}
            <div class="attribute-long">
                {attribute_view_gui attribute=$node.data_map.description}
            </div>
        {/if} 


{* If the Show Grandchildren option is checked, link the names of the grandchildren as well as the children *}

		{section show=eq($node.object.data_map.show_children.content.0,2)}
			{if $node.path_string|explode('/')|contains(2010)}
			   {def $child_list=hash()}
			   {include name=SubMenu uri='design:node/view/user_drill.tpl' drill_from=$node child_list=$child_list}
			{else}
			{let folder_list=fetch( content, list, hash(parent_node_id, $node.node_id, sort_by, $node.sort_array, class_filter_type, exclude, class_filter_array, array('infobox', 'quicklinks', 'banner')))}
				{section name=Folder loop=$folder_list}
					<h2>{if $node.path_string|explode('/')|contains(2010)|not}<a href={$:item.url_alias|ezroot}>{/if}{$Folder:item.name|wash}{if $node.path_string|explode('/')|contains(2010)|not}</a>{/if}</h2>
					<ul>
					{section name=Child loop=fetch( 'content', 'list', hash(parent_node_id, $Folder:item.node_id, class_filter_type, exclude, class_filter_array, array('infobox', 'quicklinks', 'banner')))}
					   <li><a href={$:item.url_alias|ezroot}>{$:item.name|wash}</a></li>
					{/section}
					</ul>
				{/section}
			{/let}
			{/if}
		
		{/section}

{* If the Show Children box is checked, show the line view of the children, up to the page limit *}
        {section show=is_unset( $versionview_mode )}
        {section show=or($node.path_string|contains("/8060/") , eq($node.object.data_map.show_children.content.0,1) )}
            {let page_limit=$node.path_string|explode('/')|contains('354')|choose(10,14)
                 list_items=array()
                 list_count=0}
	    {if $node.data_map.number_of_children.has_content}
	    	{set $page_limit=$node.data_map.number_of_children.data_int}
	    {/if}
            {section show=or( $view_parameters.day, $view_parameters.month, $view_parameters.year )}
                {let time_filter=array( and,
                                        array( 'published', '>=',
                                               maketime( 0, 0, 0,
                                                         $view_parameters.month, cond( $view_parameters.day, $view_parameters.day, 1 ), $view_parameters.year ) ),
                                        array( 'published', '<=',
                                               maketime( 23, 59, 59,
                                                         cond( $view_parameters.day, $view_parameters.month, $view_parameters.month|inc ), cond( $view_parameters.day, $view_parameters.day, 0 ), $view_parameters.year ) ) )}
                {set list_items=fetch_alias( children, hash( parent_node_id, $node.node_id,
                                                             offset, $view_parameters.offset,
                                                             attribute_filter, $time_filter,
                                                             sort_by, $node.sort_array,
										 class_filter_type, exclude, 
										 class_filter_array, array('infobox', 'quicklinks', 'banner'),
                                                             limit, $page_limit ) )
                     list_count=fetch_alias( children_count, hash( parent_node_id, $node.node_id ) )}
                {/let}
            {section-else}
		{def $sortArray = $node.sort_array}
{if eq($node.node_id, 7465)}
{elseif eq($node.node_id,10550)}
{set $sortArray = array('attribute',false(),344)}
{/if}
                {set list_items=fetch_alias( children, hash( parent_node_id, $node.node_id,
                                                             offset, $view_parameters.offset,
                                                             sort_by, $sortArray,
							     class_filter_type, exclude,
							     class_filter_array, array('infobox', 'quicklinks', 'banner'),
                                                             limit, $page_limit ) )}
                {set list_count=fetch_alias( children_count, hash( parent_node_id, $node.node_id, class_filter_type, exclude, class_filter_array, array('infobox', 'quicklinks', 'banner') ) )}
            {/section}
            {if $list_items|count}<div class="content-view-children">
		  <div class='col_2 first_col'>
                {foreach $list_items as $key => $child}
                    {if gt(sum($key,1), div($page_limit,2))}
				{run-once}</div><div class='col_2'>{/run-once}
                    {/if}
                    {node_view_gui view=line content_node=$child}
                {/foreach}
		   </div>
            </div>{/if}
            {include name=navigator
                     uri='design:navigator/google.tpl'
                     page_uri=$node.url_alias
                     item_count=$list_count
                     view_parameters=$view_parameters
                     item_limit=$page_limit}
            {/let}
        {/section}
        {/section}    
<div class='clearfix'></div>
</div>
</div>
