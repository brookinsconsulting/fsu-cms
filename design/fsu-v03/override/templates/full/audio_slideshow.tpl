{* Audio Slideshow - Full view *}

<div class="content-view-full">
    <div class="class-audio_slideshow">

    <div class="content-media">
      {def $filefullpath=$node.object.data_map.path.content|trim('/')}
	  {def $filecontent=$filefullpath|file_get_contents}
	  {set filecontent=$filecontent|explode("<center>")[1]}
	  {set filecontent=$filecontent|explode("</center>")[0]}
	  {set filecontent=$filecontent|explode("soundslider.swf")|implode(concat("/", $filefullpath|explode("/index.html")[0], "/soundslider.swf"))}
	  {$filecontent}
	  {undef}
    </div>

{if $node.data_map.description.has_content}
    <div class="attribute-long">
	{attribute_view_gui attribute=$node.data_map.description}
    </div>
{/if}

    </div>
</div>