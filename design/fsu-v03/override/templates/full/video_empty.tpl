{* Video - Full Empty view *}

<div class='video-block'>


{if is_set($node.data_map.description)}
	{def $description = $node.data_map.description.content.output.output_text}
{else}
	{def $botr = botr_api($node.data_map.video.content)
		 $description = $botr['response'][0]['video']['description']}
{/if}


{def $vid_id = $node.data_map.video.content
	 $player = ezgetvars()['player']
}
	
{if or(is_set($player)|not, $player|eq(''))}
	{set $player = ezini( 'BOTRSettings', 'DefaultPlayer', 'botr.ini' )}
{/if}

{def $player_r = botr_player($player)}

<div id="botr_{$vid_id}_{$player}_div" style='width: {sum($player_r['width'], 15)}px; height: {sum($player_r['height'], 15)}px' class="botrplayer"></div>

{literal}
<script type="text/javascript">

if (typeof(dhtmlLoadScript) == 'undefined') {

	function dhtmlLoadScript(url)
	{
	   var e = document.createElement("script");
	   e.src = url;
	   e.type="text/javascript";
	   document.getElementsByTagName("head")[0].appendChild(e); 
	}

}

{/literal}

if (typeof(botrObject) == 'undefined') dhtmlLoadScript('/extension/multimedia/design/standard/javascript/ezbotr_object.js');
dhtmlLoadScript('/extension/multimedia/design/standard/javascript/bits_modified.php?keys={$vid_id}-{$player}');

</script>

	{if $node.data_map.still_frame.content.is_valid}
		<div id='coverup' onclick="javascript: this.style.display = 'none';">
		{attribute_view_gui attribute=$node.data_map.still_frame image_class='still_frame_large'}
		</div>
	{/if}

</div>


