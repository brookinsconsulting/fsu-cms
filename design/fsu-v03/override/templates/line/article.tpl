{* Article - Line view *}
{if is_unset($image_class)}{def $image_class='articlethumbnail'}{/if}

<div class="content-view-line">
    <div class="class-article float-break">

    <h2><a href={$node.url_alias|ezurl}>{$node.data_map.title.content|wash}</a></h2>

    {section show=$node.data_map.image.has_content}
        <div class="attribute-image">
            {attribute_view_gui image_class=$image_class href=$node.url_alias|ezurl attribute=$node.data_map.image}
        </div>
    {/section}

    {if $node.data_map.intro.content.is_empty|not}
    <div class="attribute-short">
        {attribute_view_gui attribute=$node.data_map.intro}
    </div>
    {else}
	<div class='empty_spacer'></div>
	{/if}

    </div>
</div>