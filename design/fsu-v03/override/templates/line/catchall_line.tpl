{*?template charset=utf-8?*}
<div class="content-view-line">
<div class="class-{$node.object.class_identifier}">
<div class="default {if is_set($node.data_map.image)}clearfix{/if}">

{if eq($node.object.class_identifier,'link')}
	{node_view_gui content_node=$node view='full'}
{else}

{let myShortDesc = first_set($node.object.data_map.intro,$node.object.data_map.summary,$node.object.data_map.short_description, false())
     myName = first_set($node.object.data_map.name,$node.object.data_map.title, false())}

    {if eq($node.object.class_identifier,'gallery')}
	{def $kids=fetch(content,list,hash(parent_node_id, $node.node_id))|shuffle}
	{attribute_view_gui attribute=$kids[0].data_map.image image_class='small'}
	{undef}
    {/if}

    {section show=and($myName, $myName.has_content)}
	    <h2><a href={$node.url_alias|ezroot}>{attribute_view_gui attribute=$myName}</a></h2>{if is_set($node.data_map.date_from)}&nbsp;<span class='date_inline'>({$node.data_map.date_from.data_int|datetime( 'custom', '%F %j, %Y' )})</span>{/if}
    {section-else}
	    <h2><a href={$node.url_alias|ezroot}>{$node.name|wash}</a></h2>
    {/section}

    {section show=and(is_set($node.data_map.image), $node.data_map.image.content.is_valid)}
       <div class="attribute-image">
	   {if eq($node.object.class_identifier, 'user')}<a href={$node.url_alias|ezroot}>{/if}{attribute_view_gui attribute=$node.object.data_map.image align=right image_class='small'}{if eq($node.object.class_identifier, 'user')}</a>{/if}
       </div>
    {/section}

    {section show=and($myShortDesc, $myShortDesc.has_content)}
	    {attribute_view_gui attribute=$myShortDesc}
    {/section}

{/let}

{/if}


</div>
<div class='clear'></div>
</div>
</div>