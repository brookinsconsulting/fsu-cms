{def $image_variation="false"
     $attribute_parameters=$object_parameters}
{if is_set( $attribute_parameters.size )}
{set $image_variation=$object.data_map.image.content[$attribute_parameters.size]}
{else}
{set $image_variation=$object.data_map.image.content[ezini( 'ImageSettings', 'DefaultEmbedAlias', 'content.ini' )]}
{/if}

{if is_set( $link_parameters.href )}

	{def $href = $link_parameters.href
		 $target = $link_parameters.target
		 $link_object=cond(is_set($link_parameters.node_id), fetch(content, node, hash(node_id, $link_parameters.node_id)).object, $href|get_link_object)
	     $attribute=first_set($link_object.data_map.file, 0)}


	{if and($link_object, is_set($link_object.data_map.file) )}
		{pagedata_set("mimes", $attribute.content.mime_type, true())}

		<a class='inline' href={concat( 'content/download/', $attribute.contentobject_id, '/', $attribute.id)|ezroot}><img src={$image_variation.full_path|ezroot} alt="{$object.data_map.image.content.alternative_text|wash(xhtml)}&nbsp;({$attribute.content.mime_type_part} {$attribute.content.filesize|si( byte )}) " /></a>

	{else}
		<a href={or($href|begins_with('http'), $href|begins_with('mailto'), $href|begins_with('#'))|choose($href|ezroot,$href)}{section show=$id} id="{$id}"{/section}{section show=$title} title="{$title}"{/section}{section show=$target} target="{$target}"{/section}{section show=ne($classification|trim,'')} class="{$classification|wash} inline"{/section}><img src={$image_variation.full_path|ezroot} alt="{$object.data_map.image.content.alternative_text|wash(xhtml)}" /></a>
	{/if}

{else}

	<img class='inline' src={$image_variation.full_path|ezroot} alt="{$object.data_map.image.content.alternative_text|wash(xhtml)}" />

{/if}

