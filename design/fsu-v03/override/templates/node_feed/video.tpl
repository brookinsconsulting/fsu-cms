{if 1|eq(0)}

{* Video - Node Feed View *}
{if is_unset($expanded)}{def $expanded = false()}{/if}
{if is_unset($embed)}{def $embed = false()}{/if}
{def $vid_data = $node.data_map.vid.content|video
     $description = first_set($node.data_map.description.output.output_text,$vid_data['description'])}

{*$vid_data|attribute(show,1,false())|debug()}
{$node.data_map.vid|attribute(show,1,false())|debug()*}

<div class="content-view-node-feed">
   <div class="class-video">
{if $expanded}
   <div class='video-block'>
	<object width="320" height="265" id="embedded_player" type="application/x-shockwave-flash" data="http://service.twistage.com/plugins/player.swf?v={$node.data_map.vid.content}">
	    <param name="movie" value="http://service.twistage.com/plugins/player.swf?v={$node.data_map.vid.content}"/>
	    <param name="allowfullscreen" value="true"/>
		<param name="wmode" value="transparent">
	    <param name="allowscriptaccess" value="always"/>
	    <param name="base" value="http://service.twistage.com"/>
	</object>
	{if $node.data_map.still_frame.content.is_valid}
	<div id='coverup' onclick="javascript: this.style.display = 'none';">{attribute_view_gui attribute=$node.data_map.still_frame image_class='still_frame_large'}</div>
	{/if}
   </div>
	<h3>{if $embed}Video: {/if}<a href="{$node.url_alias|ezroot(no)}">{$node.name|wash()}</a></h3>
   {if $embed}
	<div class='video-details'>
		{if $description|trim|begins_with('<p>')|not}<p>{/if}{$description}{if $description|trim|begins_with('<p>')|not}</p>{/if}
	</div>
   {/if}

{else}
{*$node.data_map|attribute(show,1,false())|debug()*}

<div class="attribute-image">
<a href="{$node.url_alias|ezroot(no)}">
   {if and(is_set($node.data_map.still_frame) , $node.data_map.still_frame.content.is_valid)}<img src="{$node.data_map.still_frame.content['original'].url|ezroot(no)}" alt="{$node.name|wash}" />{else}<img src='http://video.thinkcreative.com/screenshots/{$node.data_map.vid.content}?width=150' width=150 alt='{$node.name|wash}' />{/if}
</a>
</div>
<div class="attribute-header"><h3><a href={$node.url_alias|ezroot}>{$node.name|wash()}</a></h3></div>
<div class="video-title">{cond( $node.data_map.description.has_content, $node.data_map.description.content.output.output_text, $vid_data['description'] )}</div>
<div style='width: 100%; clear: both'></div>


{/if}
   </div>
</div>


{else}






















{if is_set($node.data_map.description)}
	{def $description = $node.data_map.description.content.output.output_text}
{else}
	{def $botr = botr_api($node.data_map.video.content)
		 $description = $botr['response'][0]['video']['description']}
{/if}



{def $vid_id = $node.data_map.video.content
	 $player = ezgetvars()['player']
}
	
{if or(is_set($player)|not, $player|eq(''))}
	{set $player = 'QcUXU3a7'}
{/if}

{def $player_r = botr_player($player)}


<div class="content-view-node-feed">
   <div class="class-video">
{if $expanded}
   <div class='video-block'>


	<div id="botr_{$vid_id}_{$player}_div" style='width: {sum($player_r['width'], 15)}px; height: {sum($player_r['height'], 15)}px' class="botrplayer"></div>


	{literal}
	<script type="text/javascript">

	if (typeof(dhtmlLoadScript) == 'undefined') {

		function dhtmlLoadScript(url)
		{
		   var e = document.createElement("script");
		   e.src = url;
		   e.type="text/javascript";
		   document.getElementsByTagName("head")[0].appendChild(e); 
		}

	}

	{/literal}
	
	if (typeof(botrObject) == 'undefined') dhtmlLoadScript('/extension/multimedia/design/standard/javascript/ezbotr_object.js');
	dhtmlLoadScript('/extension/multimedia/design/standard/javascript/bits_modified.php?keys={$vid_id}-{$player}');

	</script>


	{if $node.data_map.still_frame.content.is_valid}
	<div id='coverup' onclick="javascript: this.style.display = 'none';">{attribute_view_gui attribute=$node.data_map.still_frame image_class='still_frame_large'}</div>
	{/if}
   </div>
	<h3>{if $embed}Video: {/if}<a href="{$node.url_alias|ezroot(no)}">{$node.data_map.title.content|wash()}</a></h3>
   {if $embed}
	<div class='video-details'>
		{if $description|trim|begins_with('<p>')|not}<p>{/if}{$description}{if $description|trim|begins_with('<p>')|not}</p>{/if}
	</div>
   {/if}

{else}
{*$node.data_map|attribute(show,1,false())|debug()*}

<div class="attribute-image">
<a href="{$node.url_alias|ezroot(no)}">
   {if and(is_set($node.data_map.still_frame) , $node.data_map.still_frame.content.is_valid)}<img src="{$node.data_map.still_frame.content['original'].url|ezroot(no)}" alt="{$node.name|wash}" />{else}<img src='http://cdn.thinkcreative.com/thumbs/{$node.data_map.video.content}-150.jpg' width=150 alt='{$node.name|wash}' />{/if}
</a>
</div>
<div class="attribute-header"><h3><a href={$node.url_alias|ezroot}>{$node.name|wash()}</a></h3></div>
{if $node.data_map.description.has_content}<div class="video-title">{$node.data_map.description.content.output.output_text}</div>{/if}
<div style='width: 100%; clear: both'></div>


{/if}
   </div>
</div>













{/if}
