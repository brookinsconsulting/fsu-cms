{* Article - Node Feed View *}

{def $readMore = concat('... <a class="read_more" href="',$node.url_alias|ezroot(no),'">Read Story</a>')}

<div class="article_node_feed{if $extra_class} {$extra_class}{/if}">

	<div class="date-icon column">
	{attribute_view_gui image_class='linkbox' href=$node|sitelink() attribute=$node.data_map.image}
	</div>

	<span class="evbodwrap">
	<h3><a href={$node.url_alias|ezroot}>{$node.name|wash()}</a></h3>

	{$node.data_map.intro.content.output.output_text|html_shorten($length, $readMore)}</a>
	
	<div class='addthis'>
	<!-- ADDTHIS BUTTON BEGIN - NODE FEED -->
	<script type="text/javascript">
	addthis_pub             = 'fsumedia';
	addthis_logo            = 'http://news.fsu.edu/video/search.gif';
	addthis_logo_background = 'cccc99';
	addthis_logo_color      = '540115';
	addthis_brand           = 'News.fsu.edu';
	addthis_options         = 'favorites, email, digg, delicious, myspace, facebook, google, live, more';
	</script>
	<a href="http://www.addthis.com/bookmark.php" onmouseover='return addthis_open(this, "", "http://news.fsu.edu{$node.url_alias|ezroot(no)|explode("'")|implode('`')|explode('"')|implode('`')}?article_id={$node.node_id}-{currentdate()}", "{$node.name|explode("'")|implode('`')|explode('"')|implode('`')}")' onmouseout="addthis_close()" onclick="return addthis_sendto()"><img src="http://s9.addthis.com/button1-share.gif" width="125" height="16" border="0" alt="" vspace="10" /></a>
	<script type="text/javascript" src="http://s7.addthis.com/js/152/addthis_widget.js"></script>
	<!-- ADDTHIS BUTTON END -->
	</div>

	</span>


</div>