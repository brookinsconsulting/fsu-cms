{def $footer=get_footer(true())
	 $footer_link=false()
	 $navigation_count=count($footer.data_map.navigation.content.relation_list)
	 $column_item_count = 0
	 $column_item_limit = 3
}
<footer>
		<nav id="footer-navigation">
			<ul class="column menu vertical">
			{foreach $footer.data_map.navigation.content.relation_list as $key=>$relation}
				{if not($relation.in_trash)}
					{set $column_item_count = $column_item_count|inc()}
					{if gt($column_item_count, $column_item_limit)}
						</ul>
						<ul class="column menu vertical">
						{set $column_item_count = 1}
					{/if}
					{set $footer_link=fetch('content','node',hash('node_id',$relation.node_id))}
					<li{if eq($key,sub($navigation_count,1))} class="last"{/if}><a href={$footer_link|sitelink()}{if and(eq($footer_link.class_identifier,'link'), $footer_link.data_map.new_window.data_int)} target="_blank"{/if}><span {if is_set($footer_link.data_map.icon)}class='{$footer_link.data_map.icon.content.0}'{/if}></span>{$footer_link.name|wash()}</a></li>
				{/if}
			{/foreach}
			</ul>
		</nav>
{if $footer.data_map.footer_text.has_content}  
	<div id="footer-content"><div class="content-view-full">{attribute_view_gui attribute=$footer.data_map.content}</div></div>
{/if}
{if and(is_set($footer.data_map.copyright),$footer.data_map.copyright.has_content)}
	<div id="copyright">{attribute_view_gui attribute=$footer.data_map.copyright}</div>
{else}
	<div id="copyright">Copyright &copy; {$pagedata.copyright_span} {attribute_view_gui attribute=$pagedata['template_look'].data_map.footer_text}. All rights reserved.</div>
{/if}

{literal}
<script type="text/javascript">

  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-33643578-1']);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();

</script>
{/literal}

</footer>
{include uri="design:page/footer_scripts.tpl"}
