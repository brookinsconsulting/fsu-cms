{* Article - Line view *}
{if is_unset($image_class)}{def $image_class='articlethumbnail'}{/if}

<div class="content-view-line">
    <div class="class-article float-break">

    <h2><a href={$node.url_alias|ezurl}>{$node.data_map.title.content|wash}</a></h2>

	<div class='addthis'>
	<!-- ADDTHIS BUTTON BEGIN -->
	<script type="text/javascript">
	addthis_pub             = 'fsumedia';
	addthis_logo            = 'http://news.fsu.edu/video/search.gif';
	addthis_logo_background = 'cccc99';
	addthis_logo_color      = '540115';
	addthis_brand           = 'news.fsu.edu';
	addthis_options         = 'favorites, email, digg, delicious, myspace, facebook, google, live, more';
	</script>
	<a href="http://www.addthis.com/bookmark.php" onmouseover="return addthis_open(this, '', '[URL]', '[TITLE]')" onmouseout="addthis_close()" onclick="return addthis_sendto()"><img src="http://s9.addthis.com/button1-share.gif" width="125" height="16" border="0" alt="" vspace="10" /></a>
	<script type="text/javascript" src="http://s7.addthis.com/js/152/addthis_widget.js"></script>
	<!-- ADDTHIS BUTTON END -->
	</div>

    {section show=$node.data_map.image.has_content}
        <div class="attribute-image">
            {attribute_view_gui image_class=$image_class href=$node.url_alias|ezurl attribute=$node.data_map.image}
        </div>
    {/section}

    {section show=$node.data_map.intro.content.is_empty|not}
    <div class="attribute-short">
        {attribute_view_gui attribute=$node.data_map.intro}
    </div>
    {/section}

    </div>
</div>