{* Page Feed - Embed *}

{def $children = fetch('content', 'list', hash('parent_node_id', $object.main_node_id,
					       'limit', $object_parameters.limit,
					       'sort_by', $object.main_node.sort_array,
					       'class_filter_type', 'exclude',
					       'class_filter_array', array('folder')))
     $length = 100}

{if eq($object.main_node_id, 3170)}
{set $children = fetch('content', 'tree', hash('parent_node_id', $object.main_node_id,
					       'limit', $object_parameters.limit,
					       'sort_by', array('published', false()),
					       'class_filter_type', 'include',
					       'class_filter_array', array('video','botr')))}
{/if}

<div class='tag_header'><h2>{$object_parameters.header|wash()}</h2></div>

{foreach $children as $key => $child}
	{node_view_gui content_node=$child view='node_feed' expanded=lt($key, $object_parameters.expanded_count) length=$length}
{/foreach}

<a class="more-link" href="{$object.main_node.url_alias|ezroot(no)}/(offset)/{$object_parameters.limit}">{$object_parameters.more_text|wash()}</a>
