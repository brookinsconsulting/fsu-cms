{if is_set($node.data_map.description)}
	{def $description = $node.data_map.description.content.output.output_text}
{else}
	{def $botr = botr_api($node.data_map.video.content)
		 $description = $botr['response'][0]['video']['description']}
{/if}


<div class="content-view-node-feed">
   <div class="class-video">
	
{if $expanded}

	{set-block scope=relative variable=vid_html}
	{if $node.data_map.video.content}<div class='video-block'>

	{attribute_view_gui attribute=$node.data_map.video player='QcUXU3a7'}

	{if $node.data_map.still_frame.content.is_valid}
	<div id='coverup' onclick="javascript: this.style.display = 'none';">{attribute_view_gui attribute=$node.data_map.still_frame image_class='still_frame_large'}</div>
	{/if}
	
    </div>{/if}
	{/set-block}
	{if $vid_html|contains('botrplayer')}{$vid_html}{/if}

	<h3>{if $embed}Video: {/if}<a href="{$node.url_alias|ezroot(no)}">{$node.data_map.title.content|wash()}</a></h3>
	
   {if $embed}
	<div class='video-details'>
		{if $description|trim|begins_with('<p>')|not}<p>{/if}{$description}{if $description|trim|begins_with('<p>')|not}</p>{/if}
	</div>
   {/if}
   

{else}

<div class="attribute-image">
<a href="{$node.url_alias|ezroot(no)}">
   {if and(is_set($node.data_map.still_frame) , $node.data_map.still_frame.content.is_valid)}<img src="{$node.data_map.still_frame.content['original'].url|ezroot(no)}" alt="{$node.name|wash}" />{else}<img src='http://cdn.thinkcreative.com/thumbs/{$node.data_map.video.content}-150.jpg' width=150 alt='{$node.name|wash}' />{/if}
</a>
</div>

<div class="attribute-header"><h3><a href={$node.url_alias|ezroot}>{$node.name|wash()}</a></h3></div>

{if $node.data_map.description.has_content}<div class="video-title">{$node.data_map.description.content.output.output_text}</div>{/if}

{if $description}
	<div class='video-details'>
	<p>{$description|shorten(120)}</p>
	</div>
{else}
	<div style='width: 100%; clear: both'></div>
{/if}


{/if}

   </div>
</div>


