<div class="press_link_listbox">

	<div class="date-icon column">
			{attribute_view_gui attribute=$node.data_map.icon.content.data_map.image image_class='press_link_icon'}
	</div>


	<span class="evbodwrap">
	<h4>{$node.data_map.date.content.timestamp|datetime('custom', '%n-%j-%y')}</h4>

	<p><a href='{$node.data_map.location.content}'>{$node.name}</a></p>

	</span>


</div>