{* Image - MyFloridaStage View *}

<div class='image_myfloridastate'>
	{attribute_view_gui attribute=$node.data_map.image}
	{set-block scope=relative variable=title}{attribute_view_gui attribute=$node.data_map.caption}{/set-block}
	<span class='caption'>{$title|strip_tags}</span>
</div>