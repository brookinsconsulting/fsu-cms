{def $todaystart=maketime( '00','00', '00', currentdate()|datetime( custom, '%n'), currentdate()|datetime( custom, '%j'), currentdate()|datetime( custom, '%Y') )}

<div class="event_listbox">

	{def $todaystart=maketime( '00','00', '00', currentdate()|datetime( custom, '%n'), currentdate()|datetime( custom, '%j'), currentdate()|datetime( custom, '%Y') )
	     $datestamp=lt($node.data_map.from_time.data_int,$todaystart)|choose($node.data_map.from_time.data_int,$todaystart)}
	<div class="date-icon column">
			<div class="month">{$datestamp|datetime(custom, '%M')}</div>
			<div class="date">{$datestamp|datetime(custom, '%j')}</div>
	</div>


	<span class="evbodwrap">
	<h4><a href={$node.object.main_node.url_alias|ezroot}>{$node.data_map.title.content}</a></h4>


	{if and($node.data_map.to_time.has_content, ne($node.data_map.to_time.data_int|datetime(custom, '%j'), $node.data_map.from_time.data_int|datetime(custom, '%j')) )}
	<p>Until {$node.data_map.to_time.content.timestamp|datetime(custom, '%M %j')}</p>
	{/if}

	{if and($node.data_map.from_time.has_content, $node.data_map.to_time.has_content)}

	<p>{$node.data_map.from_time.content.timestamp|l10n( 'shorttime' )} - {$node.data_map.to_time.content.timestamp|l10n( 'shorttime' )}</p>

	{else}

	{if $node.data_map.from_time.has_content}
	<p>Starts {$node.data_map.from_time.content.timestamp|l10n( 'shorttime' )}</p>
	{/if}

	{if $node.data_map.to_time.has_content}
	<p>Ends {$node.data_map.to_time.content.timestamp|l10n( 'shorttime' )}</p>
	{/if}

	{/if}
	</span>


</div>