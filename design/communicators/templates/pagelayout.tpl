<!DOCTYPE html>
<html lang="{$site.http_equiv.Content-language|wash()}" xmlns="http://www.w3.org/1999/xhtml" xml:lang="{$site.http_equiv.Content-language|wash()}">
{if not(is_set($extra_cache_key))}{def $extra_cache_key=''}{/if}
{def $pagedata = pagedata()
	 $basket_is_empty = cond($current_user.is_logged_in,fetch(shop,basket).is_empty,1)
	 $locales = fetch('content','translation_list')
	 $pagedesign = $pagedata.template_look
	 $indexpage = $pagedata.root_node_id
	 $content_info = first_set($module_result.content_info, false())
	 $persistent_infoboxes = array()
	 $cufon_fonts=cond(ezini_hasvariable('JavaScriptSettings','CufonFontsList','design.ini'),ezini('JavaScriptSettings','CufonFontsList','design.ini'),false())
	 $has_cufon=and($cufon_fonts,count($cufon_fonts))
}
{cache-block keys=array($module_result.uri, $basket_is_empty, $current_user.contentobject_id, $access_type.name, $extra_cache_key)}
<head>

{include uri='design:parts/page_head.tpl'}

{if $current_user.is_logged_in}
{literal}
<script type="text/javascript">
if (typeof Meebo == 'undefined') {
    var M=Meebo=function(){(M._=M._||[]).push(arguments)},w=window,a='addEventListener',b='attachEvent',c='load',
    d=function(){M.T(c);M(c)},z=M.$={0:+new Date};
    M.T=function(a){z[a]=+new Date-z[0]};if(w[a]){w[a](c,d,false)}else{w[b]('on'+c,d)};M.v=3;
    (function(_){var d=document,b=d.body,c;if(!b){c=arguments.callee;
    return setTimeout(function(){c(_)},100)}var a='appendChild',c='createElement',
    m=b.insertBefore(d[c]('div'),b.firstChild),n=m[a](d[c]('m')),i=d[c]('iframe');
    m.style.display='none';m.id='meebo';i.frameBorder="0";n[a](i).id="meebo-iframe";
    function s(){return['<body onload=\'var d=document;d.getElementsByTagName("head")[0].',
    a,'(d.',c,'("script")).sr','c="//',_.stage?'stage-':'',
    'cim.meebo.com','/cim?iv=',M.v,'&network=',_.network,_.lang?'&lang='+_.lang:'',
    _.d?'&domain='+_.d:'','"\'></bo','dy>'].join('')}try{
    d=i.contentWindow.document.open();d.write(s());d.close()}catch(e){
    _.d=d.domain;i['sr'+'c']='javascript:d=document.open();d.write("'+s().replace(/"/g,'\\"')+'");d.close();'}M.T(1)})
    ({ network: 'fsucom_jo57ho', stage: false });
    Meebo("makeEverythingSharable");    
}
</script>
{/literal}
{/if}

</head>

<body class="{$pagedata.site_classes|implode(' ')|explode('sidebar')|implode('nosidebar')|explode('nonosidebar')|implode('nosidebar')}">

	<div id='pagewrap'>

		<div class="accessibleText">Screenreader Navigation - [ <a href="#tof">Skip to Content</a>&nbsp;|&nbsp;<a href="#mainNav">Skip to Main Navigation</a> ]<hr /></div>

		<div class="header"><img id='main_logo' src={'fsucom_logo.png'|ezimage} alt='The Florida State University - Return to FSU.com Home Page' /><a id='fsuedulogolink' href='http://fsu.edu'>&nbsp;</a>{if ne($module_result.node_id,$pagedata.root_node_id)}<a id='mainlogolink' href='/'>&nbsp;</a>{/if}
			{include uri='design:parts/search_box.tpl'}
		</div>
		
		<div id="navigation">
			<div id='global-menubar'>
			{include uri='design:menu/menubar.tpl' orientation='horizontal' submenu=true() show_header=false() attribute_filter=array(array('priority', 'between', array(99, 200)))}
			</div>
		</div>

		<div class="accessibleText"><a id="tof"></a></div>
		
		<div id="mainbox">
		<div class="bofCentered">
		<div class="bofContainer">
		<div class="bofCentered">
		
		<section id="site-columns" class='bof'>
			{if $pagedata.website_toolbar}
				{include uri='design:page_toolbar.tpl'}
			{/if}
			{*if $pagedata.has_sidebar}
				<aside id="sidebar" role="complementary">
					{if and($pagedata.has_sidemenu,eq($pagedata.sidemenu_position,'left'))}{include uri="design:menu/sidemenubar.tpl" show_header=false()}{/if}
					{if and(is_set($pagedata.persistent_variable.sidebar), $pagedata.persistent_variable.sidebar)}
						<section>{$pagedata.persistent_variable.sidebar}</section>
					{/if}
					{if and($infoboxes,$infoboxes['left'])}{include uri="design:parts/extrainfo.tpl" infoboxes=$infoboxes['left'] infoboxes_only=true()}{/if}
				</aside>
			{/if*}
				<section class="{$pagedata.content_classes|implode(' ')}" id="site-main-content" role="main">
				{*if $pagedata.show_path}
					{include uri='design:page_toppath.tpl' delimiter='&raquo;'}
				{/if*}
{/cache-block}
					{$module_result.content}
{cache-block keys=array($module_result.uri, $basket_is_empty, $current_user.contentobject.id, $access_type.name, $extra_cache_key)}
				</section>
			{if $pagedata.has_extrainfo}
				<aside id="extrainfo" role="complementary">
				{if and($pagedata.has_sidemenu,eq($pagedata.sidemenu_position,'right'))}{include uri="design:menu/sidemenubar.tpl" show_header=false()}{/if}
				{include uri="design:parts/extrainfo.tpl" infoboxes=cond(and($infoboxes,$infoboxes['right']),$infoboxes['right'],false()) infoboxes_only=false()}
				</aside>
			{/if}
			{if and(is_set($pagedata.persistent_variable.bottomarea), $pagedata.persistent_variable.bottomarea)}
				<section id="bottomarea">{$pagedata.persistent_variable.bottomarea}</section>
			{/if}
		</section>
		{include uri="design:page_footer.tpl"}
		
		</div></div></div></div>
		
	</div>
	{* This comment will be replaced with actual debug report (if debug is on). *}
	<!--DEBUG_REPORT-->
{/cache-block}
</body>

</html>

