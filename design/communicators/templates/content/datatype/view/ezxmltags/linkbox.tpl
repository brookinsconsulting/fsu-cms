{def $node_id = $page|explode('://')[1]
	 $node = fetch('content','node',hash('node_id',$node_id))
	 $dataNodes = fetch('content', 'list', hash('parent_node_id',$node_id,
						'limit', first_set(cond($ticker_limit,$ticker_limit,$limit),3),
						'sort_by', $node.sort_array))
}


<div id='rotatornode_{$node_id}' class='linkbox'>
	
{foreach $dataNodes as $d}
	<div class='linkbox_item'>
	<div class='attribute-text'>{attribute_view_gui attribute='description'}</div>
	</div>
{/foreach}

</div>