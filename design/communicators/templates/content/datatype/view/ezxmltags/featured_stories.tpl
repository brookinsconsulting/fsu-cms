{* Featured Stories - Custom Tag *}
{def $featuredStoriesNode = fetch('content','node'hash('node_id',7465))
     $featuredStories = fetch('content', 'list', hash('parent_node_id', $featuredStoriesNode.node_id,
						      'sort_by', $featuredStoriesNode.sort_array,
						      'class_filter_type', 'include',
						      'class_filter_array', array('article'),
						      'limit', $limit))
     $jsTabControls = array()}
{*'sort_by', array('published', false())*}

{$featuredStoriesNode.sort_array|debug}
{run-once}
<script type="text/javascript" src="{'javascript/jcarousellite_1.0.1.js'|ezdesign(no)}"></script>
{/run-once}

<div class="featured-stories">
   <ul class="tabset-navigation">
{*	<li><img class="previous-btn" src="{'images/arrows/sm-left-arrow.png'|ezdesign(no)}" width="25" height="25" /></li>*}
	<li class="tab-btn previous-btn control-btn"><a href="#"><span>&laquo;</span></a></li>
{foreach $featuredStories as $key => $story}
   {set $jsTabControls = $jsTabControls|append(concat('#btn-slide-', $key))}
	<li id="btn-slide-{$key}" class="tab-btn {cond( eq($key,0), ' active','' )}"><a href="#">{$key|inc()}</a></li>
{/foreach}
	<li class="tab-btn next-btn control-btn"><a href="#"><span>&raquo;</span></a></li>
	<li class="tab-btn play-btn control-btn"><a href="#"><span>&gt;</span></a></li>
{*
	<li><img class="next-btn" src="{'images/arrows/sm-right-arrow.png'|ezdesign(no)}" width="25" height="25" /></li>
	<li><img class="pause-btn" src="{'images/arrows/sm-pause.png'|ezdesign(no)}" width="25" height="25" /></li>
*}
   </ul>
<div class="tabset-header">
<img src="{'images/featured-stories.png'|ezdesign(no)}" width="231" height="29" />
<img src="{'images/presented-by.png'|ezdesign(no)}" width="64" height="15" />&nbsp;<img src="{'images/presented_by/yellow-pages.png'|ezdesign(no)}" />
</div>

<div id="stories-tabset" class="tabset-slides">
<ul>
{foreach $featuredStories as $key => $story}
  <li id="slide-{$key}">
	{node_view_gui content_node=$story view='node_feed' expanded=true() length=400 image_class='medium'}
  </li>
{/foreach}
</ul>
</div>

<script type="text/javascript">
var tabControls = ["{$jsTabControls|implode('","')}"];
{literal}
$(function(){
var currentSlideId = null;
	$(".tab-btn a").each(function(){return false;});
	$(".tabset-slides").jCarouselLite({
		auto:8000,
		btnPrev:".previous-btn",
		btnNext:".next-btn",
		btnGo:tabControls,
		btnActions:{pause:".pause-btn",play:".play-btn"},
		speed: 500,
		visible:1,
		beforeStart:function(a){currentSlideId='#btn-'+a[0].id;},
		afterEnd:function(a){
			$(currentSlideId).removeClass('active');
			$('#btn-'+a[0].id).addClass('active');
		}
	});
});
{/literal}
</script>
</div>