{*?template charset=utf-8?*}
{default use_url_translation=false()}
    <td class="{$sequence}">
	{if is_set($content_node.data_map.link)}
	<a href="{$node.data_map.url.content}">>>{$node.name|wash}</a>
	{else}
	<a href={cond( $use_url_translation, $node.url_alias|ezroot,
                   		true(), concat( "/content/view/full/", $node.main_node_id )|ezroot )}>{$node.name|wash}</a>
      {/if}
    </td>

    <td class="{$sequence} darker">
	{if eq($node.object.class_identifier, 'spa_stc_element')}
	{foreach $node.path|reverse as $pathstep}
	{if eq($pathstep.class_identifier, 'spa_stc')}
      <nobr><a href={$pathstep.url_alias|ezroot}>{$pathstep.name|wash}</a></nobr>
	{break}
	{/if}
	{/foreach}

	{else}
	
	{foreach $node.path|reverse as $pathstep}
	{if eq($pathstep.class_identifier, 'divisionhome')}
      <nobr><a href={$pathstep.url_alias|ezroot}>{$pathstep.name|wash}</a></nobr>
	{break}
	{/if}
	{/foreach}

	{/if}
    </td>


    <td class="{$sequence}">
      <nobr>{$node.object.class_name|wash}</nobr>
    </td>
{/default}
