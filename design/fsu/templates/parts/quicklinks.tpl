{*?template charset=utf-8?*}
{def $borderclass=''}
{if is_set($module_result.node_id)}
<div class="quickLinksContainer">
<div class="quickLinksCenter">
			
<div class="quickLinksPosition"> 
<div id='schoolname'>{$site_name}</div>

{let quickfolders=fetch('content', 'list', hash('parent_node_id', first_set($module_result.node_id,$site_root_node), class_filter_type, 'include', class_filter_array, array('quicklinks')))
     home_quickfolder=array()
     home_quickfolders=array()
     quickfolder = hash('node_id', 0)
     quicklinks=array()
     home_node_id=$module_result.path.1.node_id}


{if $quickfolders|count}
	{set quickfolder = $quickfolders.0}
   {/if}

   {if $quickfolder.node_id}
   	{set quicklinks=fetch('content', 'list', hash('parent_node_id', $quickfolder.node_id, 'sort_by', $quickfolder.sort_array))}
   {/if}

   {if ne($home_node_id, first_set($module_result.node_id,$site_root_node))}
	{set home_quickfolders=fetch('content', 'list', hash('parent_node_id', $home_node_id, class_filter_type, 'include', class_filter_array, array('quicklinks')))}
       {if $home_quickfolders|count}
	   {set home_quickfolder = $home_quickfolders.0
		 quicklinks = $quicklinks|merge(fetch('content', 'list', hash('parent_node_id', $home_quickfolder.node_id, 'sort_by', $home_quickfolder.sort_array)) )}
	{/if}
   {/if}

   {if $quicklinks|count}
<div class="accessibleText"> 
<h4>{first_set($quickfolder.name, $home_quickfolder.name)|wash}</h4>
</div>



   <div class="quickLinks" onmouseover="this.className='quickLinksHover'" onmouseout="this.className='quickLinks'"> 
   <span class='links_title'>{first_set($quickfolder.name, $home_quickfolder.name)|wash}</span><ul>
	{section var=thislink loop=$quicklinks}
	  {set borderclass=''}
	{if $thislink.data_map.location.content}
   	<li class='{$borderclass}'><a href='{$thislink.data_map.location.content|ezurl(no)}'>{$thislink.name|wash}</a></li>
	{else}
   	<li class='{$borderclass}'><a href='{$thislink.object.main_node.url_alias|ezurl(no)}'>{$thislink.name|wash}</a></li>
	{/if}
	{/section}
   </ul>
   </div>
   {/if}

{/let}

<div class="accessibleText"> <hr/></div>
</div>
</div>
</div>
{/if}

{undef}