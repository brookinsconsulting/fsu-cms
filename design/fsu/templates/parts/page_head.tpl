<head>
{def $basket_is_empty   = cond($current_user.is_logged_in, fetch( shop, basket ).is_empty, 1)
     $current_node_id   = first_set($module_result.node_id, 0)
     $user_hash         = concat($current_user.role_id_list|implode( ',' ), ',', $current_user.limited_assignment_value_list|implode( ',' ))}

{cache-block keys=array($uri_string, $basket_is_empty, $user_hash)}
{def $pagestyle       = 'nosidemenu noextrainfo'
     $infobox_count   = 0
     $locales         = fetch( 'content', 'translation_list' )
     $pagerootdepth   = ezini( 'SiteSettings', 'RootNodeDepth', 'site.ini' )
     $indexpage       = 2
     $path_normalized = ''
     $pagedesign      = fetch( 'content', 'object', hash( 'object_id', '54' ) )
}
{include uri='design:page_head.tpl'}

{def $current_node = fetch('content','node', hash('node_id', $module_result.node_id))}
<title>{if $current_node.data_map.meta_title.has_content}{$current_node.data_map.meta_title.content|wash()|trim()}{else}{$site_title}{/if}</title>

{section show=and(is_set($#Header:extra_data),is_array($#Header:extra_data))}
{section name=ExtraData loop=$#Header:extra_data}
{$:item}
{/section}
{/section}

{* check if we need a http-equiv refresh *}
{if $site.redirect}
<meta http-equiv="Refresh" content="{$site.redirect.timer}; URL={$site.redirect.location}" />

{/if}

{section name=HTTP loop=$site.http_equiv}
<meta http-equiv="{$HTTP:key|wash}" content="{$HTTP:item|wash}" />

{/section}

{foreach $site.meta as $key=>$item}
{if array('keywords','description')|contains($key)}
{if and(is_set($current_node.data_map[concat('meta_',$key)]), $current_node.data_map[concat('meta_',$key)].has_content)}
{set-block variable='item'}{attribute_view_gui attribute=$current_node.data_map[concat('meta_',$key)]}{/set-block}
{/if}
{if and(eq($key, 'keywords'), ne($current_node.data_map.meta_tags.content,''))}
{set-block variable='item'}{attribute_view_gui attribute=$current_node.data_map[concat('meta_tags')]}{/set-block}
{/if}
{/if}
<meta name="{$key|wash()}" content="{$item|wash()|trim()}" />
{/foreach}

<style type="text/css">
    @import url({"stylesheets/core.css"|ezdesign(double)});
    @import url({"stylesheets/pagelayout.css"|ezdesign(double)});
    @import url({"stylesheets/content.css"|ezdesign(double)});
    @import url({"stylesheets/websitetoolbar.css"|ezdesign(double)});
    @import url({ezini('StylesheetSettings','ClassesCSS','design.ini')|ezroot(double)});
    @import url({ezini('StylesheetSettings','SiteCSS','design.ini')|ezroot(double)});
    {foreach ezini( 'StylesheetSettings', 'CSSFileList', 'design.ini' ) as $css_file}
    @import url({concat( 'stylesheets/', $css_file )|ezdesign});
    {/foreach}
    @import url({"stylesheets/fsu_ez.css"|ezdesign(double)});
</style>
<link rel="stylesheet" type="text/css" href={"stylesheets/print.css"|ezdesign} media="print" />
<!-- IE conditional comments; for bug fixes for different IE versions -->
<!--[if IE 5]>     <style type="text/css"> @import url({"stylesheets/browsers/ie5.css"|ezdesign(no)});    </style> <![endif]-->
<!--[if lte IE 7]> <style type="text/css"> @import url({"stylesheets/browsers/ie7lte.css"|ezdesign(no)}); </style> <![endif]-->
{foreach ezini( 'JavaScriptSettings', 'JavaScriptList', 'design.ini' ) as $script}
    <script language="javascript" type="text/javascript" src={concat( 'javascript/', $script )|ezdesign}></script>
{/foreach}
</head>
