{*Mp3 - Full view *} 
<div class="content-view-full">
	<div class="class-mp3">

			<h1>
				{$node.object.name|wash()}
			</h1>

			<div class="attribute-file">
			    {attribute_view_gui attribute=$node.object.data_map.mp3} 
			</div>
	</div>
</div>
