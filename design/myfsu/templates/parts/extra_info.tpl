
{if is_set($infoboxes)|not}{def $infoboxes=fetch( 'content', 'list', hash( 'parent_node_id', $current_node_id,
                                                'class_filter_type', 'include',
                                                'class_filter_array', array( 'infobox' ),
												'sort_by', fetch(content, node, hash(node_id, $module_result.node_id)).sort_array ))}{/if}
                                                
{if gt($infoboxes|count, 0)}
{foreach $infoboxes as $infobox}
    {node_view_gui content_node=$infobox view='infobox'}
{/foreach}
{/if}