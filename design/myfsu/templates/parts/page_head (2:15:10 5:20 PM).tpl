<head>
{def $basket_is_empty   = cond($current_user.is_logged_in, fetch( shop, basket ).is_empty, 1)
     $current_node_id   = first_set($module_result.node_id, 0)
     $user_hash         = concat($current_user.role_id_list|implode( ',' ), ',', $current_user.limited_assignment_value_list|implode( ',' ))}

{cache-block keys=array($uri_string, $basket_is_empty, $user_hash)}
{def $pagestyle       = 'nosidemenu noextrainfo'
     $infobox_count   = 0
     $locales         = fetch( 'content', 'translation_list' )
     $pagerootdepth   = ezini( 'SiteSettings', 'RootNodeDepth', 'site.ini' )
     $indexpage       = 2
     $path_normalized = ''
     $pagedesign      = fetch( 'content', 'object', hash( 'object_id', '54' ) )
}
{include uri='design:page_head.tpl'}

<style type="text/css">
    @import url({"stylesheets/core.css"|ezdesign(double)});
    @import url({"stylesheets/pagelayout.css"|ezdesign(double)});
    @import url({"stylesheets/content.css"|ezdesign(double)});{/cache-block}
{if and( is_set($module_result.node_id), $current_user.is_logged_in, is_set( $module_result.content_info.viewmode ), ne( $module_result.content_info.viewmode, 'sitemap' ) ) }
    @import url({"stylesheets/websitetoolbar.css"|ezdesign(double)});
{/if}
{* no longer needed-MLM 052809     @import url({ezini('StylesheetSettings','ClassesCSS','design.ini')|ezroot(double)});
    @import url({ezini('StylesheetSettings','SiteCSS','design.ini')|ezroot(double)});  *}
    {foreach ezini( 'StylesheetSettings', 'CSSFileList', 'design.ini' ) as $css_file}
    @import url({concat( 'stylesheets/', $css_file )|ezdesign});
    {/foreach}
    @import url({"stylesheets/fsu_ez.css"|ezdesign(double)});
</style>
<link rel="stylesheet" type="text/css" href={"stylesheets/print.css"|ezdesign} media="print" />
<!-- IE conditional comments; for bug fixes for different IE versions -->
<!--[if IE 5]>     <style type="text/css"> @import url({"stylesheets/browsers/ie5.css"|ezdesign(no)});    </style> <![endif]-->
<!--[if lte IE 7]> <style type="text/css"> @import url({"stylesheets/browsers/ie7lte.css"|ezdesign(no)}); </style> <![endif]-->
{foreach ezini( 'JavaScriptSettings', 'JavaScriptList', 'design.ini' ) as $script}
    <script language="javascript" type="text/javascript" src={concat( 'javascript/', $script )|ezdesign}></script>
{/foreach}
</head>