{def $this_root_node = fetch(content, node, hash(node_id, ezini( 'NodeSettings', 'RootNode', 'content.ini' )))
     $treeclass = ''
	 $thisnode_id=$module_result.node_id
	 $thisnode=fetch(content,node,hash(node_id, $thisnode_id) )
	 $pathar=$thisnode.path_array
	 $loopme=$thisnode.path|reverse
	 $checkme=array()
	 $startnode=array()
	 $startnodetype=''

}  

{if eq($thisnode.data_map.navreset.data_int,1)}
   {set startnode=$thisnode
		startnodetype='resetonthisnode'}
{elseif eq($thisnode_id, $this_root_node.node_id)}
   {set startnode=$this_root_node
		startnodetype='siteroot'}
{else}
{foreach $loopme as $index => $this_step}
   {set checkme=$index}
   {if eq($this_step.data_map.navreset.data_int,1)}	
   	{set checkme=$index|inc}
   	{break}
   {/if}
{/foreach}
{set startnode = fetch(content,node,hash(node_id, $loopme[$checkme|dec].node_id) )}
{if ne($startnode.node_id,$this_root_node.node_id)}
	{set startnodetype = 'resetonparent'}
{else}		
	{set startnodetype = 'siteroot'}
{/if}
{/if}
{if eq($startnodetype,resetonthisnode)}
<h3 style="margin-left:15px"> {$startnode.name}</h3>
{elseif eq($startnodetype,resetonparent)}
<h3 style="margin-left:0px"><a href={$startnode.url_alias|ezroot}>{$startnode.name}</a></h3>

{/if}

<ul id='l_nav'> 
{foreach fetch(content, list, hash(parent_node_id, $startnode.node_id, class_filter_type, 'include', class_filter_array, ezini( 'MenuContentSettings', 'LeftIdentifierList', 'menu.ini' ), sort_by, $startnode.sort_array)) as $key => $menu}
    {if ne($menu.node_id,87765)}    
        {set treeclass = or(array('Info Page', 'Article','Link')|contains($menu.object.class_name)|not, and(array('Info Page', 'Article','Link')|contains($menu.object.class_name),$menu.data_map.nav_display.data_int))|choose('hidden', '')}
        <li class="{$treeclass}">   
        {if eq($menu.node_id, $module_result.node_id)}                       
	        <span class='noLink'>{$menu.name|wash|shorten(45)}</span>
		{elseif eq($menu.object.class_identifier, 'link')}
	       <a href='{$menu.data_map.location.content}' title='{$menu.name|wash}'{if $menu.data_map.new_window.data_int} target="_blank"{/if}>{$menu.name|wash}</a>
        {else}
	        <a {and(gt($module_result.path|count,3),eq($menu.node_id,$module_result.path[3]['node_id'])|choose("","class='noLink'"))} href={$menu.url_alias|ezroot}{if $menu.data_map.new_window.data_int} target="_blank"{/if}>{$menu.name|wash|shorten(45)}</a> 
        {/if}  
        {if fetch(content, list_count, hash(parent_node_id, $menu.node_id, class_filter_type, 'include', class_filter_array, ezini( 'MenuContentSettings', 'LeftIdentifierList', 'menu.ini' )))}<ol> 
            {foreach fetch(content, list, hash(parent_node_id, $menu.node_id, class_filter_type, 'include', class_filter_array, ezini( 'MenuContentSettings', 'LeftIdentifierList', 'menu.ini' ), sort_by, $menu.sort_array)) as $key2 => $menu2}
                {set treeclass = or(array('Info Page', 'Article','Link')|contains($menu2.object.class_name)|not, and(array('Info Page', 'Article','Link')|contains($menu2.object.class_name),$menu2.data_map.nav_display.data_int))|choose('hidden', '')}
                <li class="{$treeclass}">
				{if eq($menu2.node_id, $module_result.node_id)}                       
	              <span class='noLink'>{$menu2.name|wash|shorten(45)}</span>
				{elseif eq($menu2.object.class_identifier, 'link')}
			       <a href='{$menu2.data_map.location.content}' title='{$menu2.name|wash}'{if $menu2.data_map.new_window.data_int} target="_blank"{/if}>{$menu2.name|wash}</a>
                {else}
	              <a {and(gt($module_result.path|count,3),eq($menu2.node_id,$module_result.path[3]['node_id'])|choose("","class='noLink'"))} href={$menu2.url_alias|ezroot}{if $menu2.data_map.new_window.data_int} target="_blank"{/if}>{$menu2.name|wash|shorten(45)}</a>                 
                {/if}</li>
            {/foreach}
        </ol>{/if} 
	 </li>  
    {/if} 
{/foreach}

</ul>
{if eq($startnodetype,resetonthisnode)}
<div class='returntoparent'>
<a href={$startnode.parent.url_alias|ezroot}>&#x25B2; Return to {$startnode.parent.name}</a>
</div>
{elseif eq($startnodetype,resetonparent)}
<div class='returntoparent'>
<a href={$startnode.url_alias|ezroot}>&#x25B2; Return to {$startnode.name}</a>
</div>
{/if}

{undef}



           

