
    {let ExcludeArray=array('alert', 'comment', 'forum_reply', 'forum_topic', 'page_note', 'promo', 'quicklinks', 'footer', 'faq_question', 'event', 'weblog', 'news_item')
	   menu_loop=fetch( 'content', 'list', hash( 'parent_node_id', $this_menu.node_id, 'sort_by',  $this_menu.sort_array, 'class_filter_type', 'exclude', 'class_filter_array', $ExcludeArray ) )
	   my_url=''}

	{if $menu_loop|count}

	   <ul id='{concat($pass_class,'_001')}' class="menu">

	   {foreach $menu_loop as $key => $menu_cell}

		{if and(is_set($menu_cell.object.data_map.location), $menu_cell.object.data_map.location.has_content)}

		   {set my_url = $menu_cell.object.data_map.location.content}

		{elseif $menu_cell.object.data_map.internal_link.content.relation_browse|count}

		   {set my_url = fetch(content,object,hash(object_id,$menu_cell.object.data_map.internal_link.content.relation_browse.0.contentobject_id ,object_version,$menu_cell.object.data_map.internal_link.content.relation_browse.0.contentobject_version)).main_node.url_alias|ezroot(no)}

		{else}

		   {set my_url = eq($menu_cell.node_id, $menu_cell.object.main_node_id)|choose(fetch('content', 'node', hash('node_id', $menu_cell.object.main_node_id)).url_alias|ezroot(no),$menu_cell.url_alias|ezroot(no))}

		{/if}

	   	<li><a class='{$orig_class}' href='{$my_url}'>{$menu_cell.object.data_map.short_name.has_content|choose($menu_cell.name|wash, $menu_cell.object.data_map.short_name.content|wash)}</a>


		{if and(lt($menu_cell.depth, 5),gt($menu_cell.children_count,0))}
	   	{include name=SubFlat uri="design:menu/flat_top_loop.tpl" this_menu=$menu_cell pass_class=concat($pass_class,'_', $menu_cell.number) orig_class=$orig_class}
		{/if}
 
	   	</li>

	   {/foreach}

	   </ul>

	{/if}

    {/let}