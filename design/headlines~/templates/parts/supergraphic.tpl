
{let super_id = and( eq(module_params()['function_name'], 'versionview' ) , eq($root_node.parent.node_id, 506) )|choose($module_result.node_id,4895)
     supergraphicnode=fetch( 'content', 'node', hash( 'node_id', fetch('content', 'node', hash('node_id', $super_id)).main_node_id ))
     allsupergraphics=fetch( 'content', 'list', hash( 'parent_node_id', 4897, class_filter_type, 'exclude', class_filter_array, array('homepage_flash')))
     relatedobjects=array()
     supergraphicarray=array()
     my_caption_at=''  
}

{if eq($module_result.node_id,4895)}
  <div id="supergraphic_container"><img name='supergraphic' src="/design/headlines/images/headlines_banner.jpg" /></div><!-- <div id="supergraphic_container"> -->
{else}


{set relatedobjects=$supergraphicnode.object.related_contentobject_array}



{foreach $relatedobjects as $thissobject}
   {if eq($thissobject.main_parent_node_id,1088)}
	{set supergraphicarray=$supergraphicarray|append($thissobject)}
   {/if}
{/foreach}


{if gt($supergraphicarray|count,0)}
   {set supergraphicarray = $supergraphicarray|shuffle}
{else}
   {set supergraphicarray = $allsupergraphics|shuffle}
{/if}

{set my_caption_at = $supergraphicarray.0.data_map.caption}

{switch match=first_set($supergraphicarray.0.class_identifier, "")}

  {case match="homepage_flash"}
<!-- DISPLAY HOMEPAGE FLASH --> 
  <div id="supergraphic_container">
  </div><!-- <div id="supergraphic_container"> --> 

  <script type="text/javascript">
          var so = new SWFObject("/design/film_school/flash/FSU_News.swf", "", "795", "245", "7", "#000");
	   so.addVariable("dataLoc","{concat("/layout/set/xml", first_set($supergraphicarray.0.main_node.url_alias,$supergraphicarray.0.url_alias)|ezroot('no'))}&amp;speed=8000");
	   so.addParam("quality", "high");
	   so.addParam("wmode", "transparent");
          so.write("supergraphic_container");
  </script>

  {/case}

  {case match="flash"}
<!-- DISPLAY FLASH --> 
  <div id="supergraphic_container">
  {attribute_view_gui attribute=$supergraphicarray.0.data_map.file }
  </div><!-- <div id="supergraphic_container"> --> 
  {/case}

  {case match="image"}
<!-- DISPLAY PAGE SUPERGRAPHIC --> 
{if $supergraphicarray.0.data_map.profilelink.has_content}


  <div id="supergraphic_container"><a href ={$supergraphicarray.0.data_map.profilelink.content}><img alt = '{$supergraphicarray.0.data_map.image.content.alternative_text}' name='supergraphic' src={$supergraphicarray.0.data_map.image.content.supergraphic.url|ezroot}/></a></div><!-- <div id="supergraphic_container"> -->

{else}

  {def $imagerelations=fetch( 'content', 'reverse_related_objects', hash( 'object_id', first_set($supergraphicarray.0.id, $supergraphicarray.0.contentobject_id), 'all_relations', true()))}
  <div id="supergraphic_container">{if and($imagerelations|count, ne($imagerelations.0.main_node.node_id,$supergraphicnode.node_id))}<a href ={$imagerelations.0.main_node.url_alias|ezroot}>{/if}<img alt = '{$supergraphicarray.0.data_map.caption.has_content|choose('Page supergraphic', $supergraphicarray.0.data_map.caption.content.output.output_text|strip_tags|trim)}' name='supergraphic' src={$supergraphicarray.0.data_map.image.content.supergraphic.url|ezroot}/>{if $imagerelations|count}</a>{/if}</div><!-- <div id="supergraphic_container"> -->

{/if}

  {/case}


  {case}
  {/case}

{/switch}

{/if}


{* if and($supergraphicarray|count, $supergraphicarray.0.data_map.caption.has_content)}
<div class="fold" style='height: 20px;'>
<div class="foldBorders" style='height: 20px;'>
<div class="attribute-caption-sg" style='color: white;'>
{$my_caption_at.content.output.output_text}</div>
</div>
</div>
{else *}
<div class="fold"><div class="foldBorders" style='height: 10px;'>



</div></div>
<div id="topmenu_wrapper">
{include uri='design:menu/flat_top.tpl'}
</div>
{* /if *}

{undef} 

{/let}
