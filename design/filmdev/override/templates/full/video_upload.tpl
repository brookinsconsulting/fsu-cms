{set-block scope=root variable=cache_ttl}0{/set-block}

{* Video Upload - Full view *}

{if fetch(user,current_user).is_logged_in}

<div class="content-view-full">
    <div class="class-video-upload">
        <div class="attribute-header">
            <h1>{attribute_view_gui attribute=$node.data_map.name}</h1>
        </div>
      	{if ezhttp_hasvariable( '_video_id', 'get' )}
            <div class='vid_upload_success'>
            <h2>Your video was uploaded.</h2>
            <p>Your video was successfully upload and will appear shortly. Thank you!</p>
            </div>
        {/if}
        {if $node.object.data_map.description.has_content}
            <div class="attribute-long">
                {attribute_view_gui attribute=$node.data_map.description}
            </div>
        {/if}

        <form id='channel_form' name='channel_form' method='post' action='/video/wizard'>
        <input type='hidden' name="ContentLanguageCode" value="eng-US"/>
        <input type="hidden" name="ClassIdentifier" value="videoembed" />
        <input type="hidden" name="NewButton" value="Create here" />
        <input type="hidden" name="ViewMode" value="full" />
        <div><p><strong>1. Select the location you would like to upload to: </strong>
		<select id='NodeID' name='NodeID'>
        <option> - Select one: - </option>
        {foreach fetch(content, list, hash(parent_node_id, $node.data_map.start_node.content.main_node.node_id, class_filter_type, 'include', class_filter_array, array($node.data_map.target.content))) as $this_node}
        <option value='{$this_node.node_id}'>{$this_node.name|wash}</option>
        {/foreach}
        </select>
        </p></div>
        <br/><div><p><strong>2. Choose the type of video content you wish to add:</strong></p></div>
        <div class='choices'>
        <div class='choice_box'>
        <p>Upload a video file from your computer in any video file format:</p>
        <table>
        <tr><td>.avi</td><td>.mpeg</td></tr>
        <tr><td>.mov</td><td>.flv</td></tr>
        <tr><td>.wmv</td><td>.3gp</td></tr>
        <tr><td>.mp4</td><td>.3g2</td></tr>
        </table>
        <img class='fauxlink channel_submit' onclick = "if (channel_form.NodeID.value > 0) {ldelim}channel_form.action='/video/wizard'; channel_form.submit();{rdelim} else {ldelim} alert('Please select a location for your video!'); {rdelim}" alt = 'Upload your video button'  src={'uploadyourvideo.png'|ezimage}/>
        </div>
        </div>
        </form>
        {if $node.object.data_map.disclaimer.has_content}
            <div class="attribute-long disclaimer">
                {attribute_view_gui attribute=$node.data_map.disclaimer}
            </div>
        {/if}
    </div>
</div>
{else}
<div class='upload_login'>
	{include uri='design:user/login.tpl' site_access=hash('allowed',1) instructions=$node.data_map.login_instructions.content.output.output_text}
</div>
{/if}