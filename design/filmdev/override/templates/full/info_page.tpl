{* Info Page - Full view *}

<div class="content-view-full">
   <div class="class-info-page">

      <div class="attribute-header">
	<h1>{attribute_view_gui attribute=$node.data_map.name}</h1>
      </div>

{if eq( ezini( 'folder', 'SummaryInFullView', 'content.ini' ), 'enabled' )}
   {if $node.data_map.short_description.has_content}
      <div class="attribute-short">
	{attribute_view_gui attribute=$node.data_map.short_description}
      </div>
   {/if}
{/if}

{if $node.data_map.description.has_content}
      <div class="attribute-long">
	{attribute_view_gui attribute=$node.data_map.description}
      </div>
{/if} 
   </div>
</div>