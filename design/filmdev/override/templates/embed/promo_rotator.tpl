{* Promo Rotator - Embed View *}
{def $children = fetch('content', 'list', hash('parent_node_id', $object.main_node_id,
					       'class_filter_type', 'include',
					       'class_filter_array', array('image','promo'),
					       'sort_by', $object.main_node.sort_array,))
     $childLink=false()}


<div class="gallery promo-rotator">
<a class="prev-arrow" href="#">Previous</a>
<div>
<ul>
{foreach $children as $child}
{if $child.data_map.internal_link.has_content}
{set $childLink = $child.data_map.internal_link.content.main_node.url_alias|ezroot(no)}
{elseif $child.data_map.external_link.has_content}
{set $childLink = $child.data_map.external_link.content}
{else}
{set $childLink = false()}
{/if}
<li>{if $childLink}<a href="{$childLink}">{/if}{attribute_view_gui attribute=$child.data_map.image image_class='original'}{if $childLink}</a>{/if}</li>
{/foreach}
</ul>
</div>
<a class="next-arrow" href="#">Next</a>
</div>
