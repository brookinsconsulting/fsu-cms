{* Event - homepage view *}

{if $new_date}
{def $todaystart=maketime( '00','00', '00', currentdate()|datetime( custom, '%n'), currentdate()|datetime( custom, '%j'), currentdate()|datetime( custom, '%Y') )
     $datestamp=lt($object.data_map.from_time.data_int,$todaystart)|choose($object.data_map.from_time.data_int,$todaystart)}
<div class="date-icon column">
		<div class="month">{$datestamp|datetime(custom, '%M')}</div>
		<div class="date">{$datestamp|datetime(custom, '%j')}</div>
</div>
{else}
<div class='date-sub'>&nbsp;</div>
{/if}

<span class="evbodwrap">
<h4><a href={$object.main_node.url_alias|ezroot}>{$object.data_map.title.content}</a></h4>


{* 

{if $object.data_map.short_description.has_content}
{attribute_view_gui attribute=$object.data_map.short_description}
{/if}

*}

{if and($object.data_map.to_time.has_content, ne($object.data_map.to_time.data_int|datetime(custom, '%j'), $object.data_map.from_time.data_int|datetime(custom, '%j')) )}
<p>Until {$object.data_map.to_time.content.timestamp|datetime(custom, '%M %j')}</p>
{/if}

{if and($object.data_map.from_time.has_content, $object.data_map.to_time.has_content)}

<p>{$object.data_map.from_time.content.timestamp|l10n( 'shorttime' )} - {$object.data_map.to_time.content.timestamp|l10n( 'shorttime' )}</p>

{else}

{if $object.data_map.from_time.has_content}
<p>Starts {$object.data_map.from_time.content.timestamp|l10n( 'shorttime' )}</p>
{/if}

{if $object.data_map.to_time.has_content}
<p>Ends {$object.data_map.to_time.content.timestamp|l10n( 'shorttime' )}</p>
{/if}

{/if}
</span>

