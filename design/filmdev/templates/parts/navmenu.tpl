{def $root_id = ezini( 'NodeSettings', 'RootNode', 'content.ini' )
	 $this_root_node = fetch(content, node, hash(node_id, $root_id))
     $treeclass = ''
	 $thisnode_id=$module_result.node_id
	 $thisnode=fetch(content,node,hash(node_id, $thisnode_id) )
	 $pathar=$thisnode.path_array
	 $loopme=$thisnode.path|reverse
	 $checkme=array()
	 $startnode=array()
	 $startnodetype=''
	 $temp = array()
}  


{if eq($thisnode.data_map.navreset.data_int,1)}
   {set startnode=$thisnode
		startnodetype='resetonthisnode'}
{elseif eq($thisnode_id, $this_root_node.node_id)}
   {set startnode=$this_root_node
		startnodetype='siteroot'}
{else}
{foreach $loopme as $index => $this_step}
   {set checkme=$index}
   {if eq($this_step.data_map.navreset.data_int,1)}	
   	{set checkme=$index|inc}
   	{break}
   {/if}
{/foreach}
{set startnode = fetch(content,node,hash(node_id, $loopme[$checkme|dec].node_id) )}
{if ne($startnode.node_id,$this_root_node.node_id)}
	{set startnodetype = 'resetonparent'}
{else}		
	{set startnodetype = 'siteroot'}
{/if}
{/if}

{def $navMenuItems = fetch(content, list, hash( parent_node_id, $startnode.node_id,
						class_filter_type, 'include',
						class_filter_array, ezini( 'MenuContentSettings', 'LeftIdentifierList', 'menu.ini' ),
						sort_by, $startnode.sort_array,
						'attribute_filter', array('and',array('priority', '>', '0'), array('priority', '<=', '50')) )) }

<ul> 
{foreach $navMenuItems as $key => $menu}
    {if ne($menu.node_id,1149)}    
        {set treeclass = or(array('Info Page', 'Article','Link')|contains($menu.object.class_name)|not, and(array('Info Page', 'Article','Link')|contains($menu.object.class_name),$menu.data_map.nav_display.data_int))|choose(array('hidden'), array())}
		{if and($key|eq(0), $root_id|ne($menu.node_id))}{set $treeclass = $treeclass|append('firstnavli')}{/if}
        <li {if $treeclass|count}class="{$treeclass|implode(' ')}"{/if}>
		{if eq($menu.object.class_identifier, 'link')}
	       <a href='{$menu.data_map.location.content}' title='{$menu.name|wash}' id='{$menu.name|wash}'{if $menu.data_map.new_window.data_int} target="_blank"{/if}>{$menu.name|wash}</a>
        {else}
	        <a {and(gt($module_result.path|count,3),eq($menu.node_id,$module_result.path[3]['node_id'])|choose("","class='noLink'"))} href={$menu.url_alias|ezroot}{if $menu.data_map.new_window.data_int} target="_blank"{/if}>{$menu.name|wash|shorten(45)}</a>                 
        {/if}

	{if $menu.children|count}
		{set $temp = fetch(content, list, hash(parent_node_id, $menu.node_id, class_filter_type, 'include', class_filter_array, ezini( 'MenuContentSettings', 'LeftIdentifierList', 'menu.ini' ), sort_by, $menu.sort_array))}
		{if $temp|count}
		<ul> 
	    {foreach $temp as $key2 => $menu2}
		{set treeclass = or(array('Info Page', 'Article','Link')|contains($menu2.object.class_name)|not, and(array('Info Page', 'Article','Link')|contains($menu2.object.class_name),$menu2.data_map.nav_display.data_int))|choose(array('hidden'), array())}
		{if $key|eq(sub($navMenuItems|count,1))}{set $treeclass = $treeclass|append('last')}{/if}
		<li {if $treeclass|count}class="{$treeclass|implode(' ')}"{/if}>
		{if eq($menu2.object.class_identifier, 'link')}
	    	<a href='{$menu2.data_map.location.content}' title='{$menu2.name|wash}' id='{$menu2.name|wash}'{if $menu2.data_map.new_window.data_int} target="_blank"{/if}>{$menu2.name|wash}</a>
		{else}
			<a {and(gt($module_result.path|count,3),eq($menu2.node_id,$module_result.path[3]['node_id'])|choose("","class='noLink'"))} href={$menu2.url_alias|ezroot}{if $menu2.data_map.new_window.data_int} target="_blank"{/if}>{$menu2.name|wash|shorten(45)}</a>                 
		{/if}</li>
	    {/foreach}
	</ul>{/if}{/if}

	 </li>  
    {/if} 
{/foreach}

{def $otherNavMenuItems = fetch(content, list, hash( parent_node_id, $startnode.node_id,
						class_filter_type, 'include',
						class_filter_array, ezini( 'MenuContentSettings', 'LeftIdentifierList', 'menu.ini' ),
						sort_by, $startnode.sort_array,
						'attribute_filter', array('and',array('priority', '>', '49'), array('priority', '<=', '100')) )) }
						
	 <li><a href="/" class="keySites">KEY SITES &gt;&gt;</a>
	    <ul>
	      <li class="leftsidebarQL">
	        <div style="float:left; width:205px;">
		{foreach $otherNavMenuItems as $ok => $o}
			<p><a href={$o|sitelink}>{$o.name}</a></p>
			{if and($ok|ne(0), $ok|sum(1)|mod(12)|eq(0))}
			</div>
			<div style="float:left; width:205px;">
			{/if}
		{/foreach}
			</div>

	      </li>
	    </ul>
	  </li>
</ul>



{undef}



           

