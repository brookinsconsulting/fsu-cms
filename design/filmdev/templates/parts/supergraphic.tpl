{* 
	
	SG_Folder      = eznode id number for Supergraphics (type is Folder) - media library/images/Deptname Supergraphics - Film School Supergraphics Folder
    FB_Folder      = eznode id number for the Flash Banner (type is Folder) - first level of directory  FSU/   -   Flash Rotation/Banner Articles 
    Fixed_Home_Image = full path (eg /design/mydesign/images/homebanner.jpg - depreciated - for archive sites - you should use child banner object instead)
  
*}



{def $SG_Folder=ezini('SuperSettings','SupergraphicsFolder', 'content.ini')}
{def $FB_Folder=ezini('SuperSettings','FlashArticles', 'content.ini')}
{def $Fixed_Home_Image=ezini('SuperSettings','FixedHomeBannerPath', 'content.ini')
     $vidURL = ''
     $kids = array()
     $vid_node_id = false()}

{def $Dept_Site_FrontPage=ezini('NodeSettings','RootNode', 'content.ini')}

{if and(is_set($Fixed_Home_Image), ne('', $Fixed_Home_Image), eq($root_node.node_id, $Dept_Site_FrontPage))}

<div id="supergraphic_container"><img name='supergraphic' src="{$Fixed_Home_Image}" /></div><!-- <div id="supergraphic_container"> -->

{else}

	{literal}
	
	<style>
		
		.attribute-header {
			display: none!important;
		}
		
	</style>
	
	{/literal}

	{let super_id = and( eq(module_params()['function_name'], 'versionview' ) , eq($root_node.parent.node_id, $FB_Folder) )|choose($module_result.node_id,$Dept_Site_FrontPage)
	     supergraphicnode=fetch( 'content', 'node', hash( 'node_id', fetch('content', 'node', hash('node_id', $super_id, limitation, array())).main_node_id, limitation, array() ))
	     allsupergraphics=fetch( 'content', 'list', hash( 'parent_node_id', $SG_Folder, class_filter_type, 'exclude', class_filter_array, array('homepage_flash'), limitation, array(), 'sort_by', array( 'priority', false() ) ))
	     relatedobjects=array()
	     supergraphicarray=array()
	     my_caption_at='' 
		 bannerkids = fetch(content, list, hash(parent_node_id, $module_result.node_id, limitation, array(), class_filter_type, 'include', class_filter_array, array('banner')))
	}

	{if $bannerkids|count}

		{if $bannerkids.0.data_map.use_flash.data_int}
		
		{def $targets = fetch(content, tree, hash(parent_node_id, $super_id, sort_by, $node.sort_array, attribute_filter, array(array('article/flash_include', '=', 1)) ))}
		{if $node.node_id|eq(1710)} {* film school *}
			{set $targets = fetch(content, tree, hash(parent_node_id, $node.node_id, sort_by, array('priority', false()), attribute_filter, array(array('article/flash_include', '=', 1)) ))}
		{/if}

		  <div id="supergraphic_container">
			{foreach $targets as $this_target}
			
				{set $kids = fetch(content, list, hash(parent_node_id, $this_target.node_id, class_filter_type, 'include', class_filter_array, array('botr')))
				     $vid_node_id = ''}
				
				{if count($kids)}
			
					{def $vid_id = $kids[0].data_map.video.content
						 $player = 'Dn1R8k0l'
					}

					{set $vidURL = concat('/extension/multimedia/design/standard/javascript/bits_modified.php?keys=', $vid_id, '-', $player)
					     $vid_node_id = $kids[0].node_id}
					
					<script type='text/javascript'>ban_data['ban_{$vid_node_id}'] = '{$vidURL}'</script>
				
				{/if}
			
				<img id="ban_{$vid_node_id}" alt="<p class='flashheadline'>{$this_target.data_map.flash_head.content|htmlentities}</p><p class='flashsubtitle'>{$this_target.data_map.flash_sub_head.content|htmlentities}</p><p><a href='{fetch('content', 'node', hash('node_id', $this_target.node_id)).url_alias|ezroot('no')}' class='super_more_link'><img src='/design/filmdev/images/more.gif'/></a></p>" src="{$this_target.data_map.flash_image.content.supergraphic_film.url|ezroot('no')}" height="279" width="700" />
			{/foreach}
		  </div><!-- <div id="supergraphic_container"> --> 
		  <div id="rotating-background-text"></div>
		  <div id="rotating-background-video"></div>
		  <div id="super-mask"></div>
		
		<div id='control-bar'>
		<div id="rotating-background-controls">
			<a class="prev" href="#">&nbsp;</a><a class="play" href="#">&nbsp;</a><a class="next" href="#">&nbsp;</a>
			<span id='controls-multimedia'><a href='#'>Video</a></span>
		</div>
		</div>

		  	<script type="text/javascript">
		
			{literal}

			if (typeof(dhtmlLoadScript) == 'undefined') {

				function dhtmlLoadScript(url)
				{
				   var e = document.createElement("script");
				   e.src = url;
				   e.type="text/javascript";
				   document.getElementsByTagName("head")[0].appendChild(e); 
				}

			}
			
			$(function(){
				$("#super-mask").css({'opacity': 0, 'z-index': 0});
				$("#control-bar").css('opacity', 0.7);
				$("#rotating-background-controls .play").click(function(){
					if ($('#control-bar .play').css('background-position') == undefined) {
						if ($('#control-bar .play').css('background-position-y').indexOf('-12') !== -1) {
							$CycleRotation.pause('supergraphic_container');
							$('#control-bar .play').css('background-position-y', '0px');
						} else {
							$CycleRotation.play('supergraphic_container');
							$('#control-bar .play').css('background-position-y', '-12px');
						}
					} else {
						if ($('#control-bar .play').css('background-position').indexOf('-12') !== -1) {
							$CycleRotation.pause('supergraphic_container');
							$('#control-bar .play').css('background-position', '0px 0px');
						} else {
							$CycleRotation.play('supergraphic_container');
							$('#control-bar .play').css('background-position', '0px -12px');
						}
					}
					$("#super-mask").css({'opacity': 0, 'z-index': 0});
					$("#rotating-background-video").empty();
					return false;
				})
			})
			
			var mynode = false;

			{/literal}
			
			if (typeof(botrObject) == 'undefined') dhtmlLoadScript('/extension/multimedia/design/standard/javascript/ezbotr_object.js');
			
				(function(){ldelim}
					$CycleRotation.register('supergraphic_container',{ldelim}
						'selector':'#supergraphic_container',
						'type':'slideshow',
						'animation':{ldelim}
							'timeout':6000,
							'fx':'turnUp',
							'prev':'#rotating-background-controls .prev',
							'next':'#rotating-background-controls .next'
						{rdelim},
						'events':{ldelim}
							'before':function(){ldelim}
								$('#controls-multimedia a').unbind('click').text('');
								$('#rotating-background-text').css('display', 'none').html($(this).attr('alt')).fadeIn();
								$('#rotating-background-text').children('p').animate({ldelim} paddingLeft: '20px' {rdelim}, 300);
								if (ban_data[$(this).attr('id')] != undefined) {ldelim}
									mynode = $(this).attr('id').replace(/ban_/, '');
									$('#controls-multimedia a').text('VIDEO').click(function(){ldelim}
										$("#super-mask").css({ldelim}'opacity': 0.61, 'z-index': 1001{rdelim});
										$CycleRotation.pause('supergraphic_container');
										$('#control-bar .play').css('background-position', '0px 0px');
										basicajax_get_html('/layout/set/empty/content/view/full/' + mynode + '?player=Dn1R8k0l', 'rotating-background-video', function() {ldelim}
											if (typeof dhtmlLoadScript !='undefined') dhtmlLoadScript(ban_data["ban_" + mynode]); 
										{rdelim});
										return false;
									{rdelim});
								{rdelim} else {ldelim}
									
								{rdelim}
							{rdelim}
						{rdelim}
					{rdelim});
				{rdelim})();
			</script>

		{else}

			{def $imagerelations=$bannerkids.0.data_map.image_link.content}
			<div id="supergraphic_container">{if and($imagerelations|is_object, ne($imagerelations.main_node.node_id,$supergraphicnode.node_id))}<a href ={$imagerelations.main_node.url_alias|ezroot}>{/if}<img alt = '{$bannerkids.0.data_map.caption.has_content|choose('Page supergraphic', $bannerkids.0.data_map.caption.content.output.output_text|strip_tags|trim)}' name='supergraphic' src={$bannerkids.0.data_map.image.content.supergraphic_film_inner.url|ezroot}/>{if $imagerelations|is_object}</a>{/if}</div><!-- <div id="supergraphic_container"> -->

		{/if}

	{else}

		{if and(is_set($supergraphicnode.data_map.flash_image), $supergraphicnode.data_map.flash_image.content.is_valid)}
	
		  	<div id="supergraphic_container"><img name='supergraphic' src={$supergraphicnode.data_map.flash_image.content.supergraphic_film.url|ezroot} alt="{$supergraphicnode.data_map.flash_head.content.data_text|count_chars()|choose('Page supergraphic', $supergraphicnode.data_map.flash_head.content.data_text|wash)}" /></div><!-- <div id="supergraphic_container"> -->
						
			{literal}

			<style>

				.tofContainer .tof {
					height: 279px!important;
				}
				
				#leftMenu {
					margin-top: -229px!important;
				}

			</style>

			{/literal}

		{else}


			{set relatedobjects=$supergraphicnode.object.related_contentobject_array}

			{foreach $relatedobjects as $thissobject}
			   {if eq($thissobject.main_parent_node_id,$SG_Folder)}
				{set supergraphicarray=$supergraphicarray|append($thissobject)}
			   {/if}
			{/foreach}

			
			{if gt($supergraphicarray|count,0)}
			   {set supergraphicarray = $supergraphicarray|shuffle}
			{else}
			   {set supergraphicarray = $allsupergraphics|shuffle}
			{/if}

			{set my_caption_at = $supergraphicarray.0.data_map.caption}

			{switch match=first_set($supergraphicarray.0.class_identifier, "")}

			  {case match="homepage_flash"}
			<!-- DISPLAY HOMEPAGE FLASH --> 

			  <div id="supergraphic_container">
				<img alt="Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos." src="http://banfflakelouise.thinkcreativeinternal.net/images/file/17792/big_banner/0/background" height="254" width="700"/>
				<img alt="Aenean pulvinar condimentum mauris eu tristique." src="http://banfflakelouise.thinkcreativeinternal.net/images/file/17791/big_banner/0/background" height="254" width="700"/>
				<img alt="In auctor nibh ut diam dictum in vulputate sem bibendum." src="http://banfflakelouise.thinkcreativeinternal.net/images/file/17780/big_banner/0/background" height="254" width="700" />
			  </div><!-- <div id="supergraphic_container"> --> 
			  <div id="rotating-background-text"></div>

			<div id="rotating-background-controls">
				<a class="prev" href="#">prev</a> | <span>Photos</span> | <a class="next" href="#">next</a>
			</div>

			  	<script type="text/javascript">
					(function(){ldelim}
						$CycleRotation.register('supergraphic_container',{ldelim}
							'selector':'#supergraphic_container',
							'type':'slideshow',
							'animation':{ldelim}
								'timeout':6000,
								'fx':'turnUp',
								'prev':'#rotating-background-controls .prev',
								'next':'#rotating-background-controls .next'
							{rdelim},
							'events':{ldelim}
								'before':function(){ldelim}
									$('#rotating-background-text').text($(this).attr('alt'));
								{rdelim}
							{rdelim}
						{rdelim});
					{rdelim})();
				</script>

			  {/case}

			  {case match="flash"}
			<!-- DISPLAY FLASH --> 
			  <div id="supergraphic_container">
			  {attribute_view_gui attribute=$supergraphicarray.0.data_map.file }
			  </div><!-- <div id="supergraphic_container"> --> 
			  {/case}

			  {case match="image"}
			<!-- DISPLAY PAGE SUPERGRAPHIC --> 
			  {def $imagerelations=fetch( 'content', 'reverse_related_objects', hash( 'object_id', first_set($supergraphicarray.0.id, $supergraphicarray.0.contentobject_id), 'all_relations', true(), limitation, array()))}
			  <div id="supergraphic_container">{if and($imagerelations|count, ne($imagerelations.0.main_node.node_id,$supergraphicnode.node_id))}<a href ={$imagerelations.0.main_node.url_alias|ezroot}>{/if}<img alt = '{$supergraphicarray.0.data_map.caption.has_content|choose('Page supergraphic', $supergraphicarray.0.data_map.caption.content.output.output_text|strip_tags|trim)}' name='supergraphic' src={$supergraphicarray.0.data_map.image.content.supergraphic_film_inner.url|ezroot}/>{if $imagerelations|count}</a>{/if}</div><!-- <div id="supergraphic_container"> -->
			  {/case}

			  {case}
			  {/case}

			{/switch}

		{/if}

	{/if}

	{if and($supergraphicarray|count, $supergraphicarray.0.data_map.caption.has_content)}
	
		{*<div class="fold" style='height: 20px;'>
		<div class="foldBorders" style='height: 20px;'>
		<div class="attribute-caption-sg" style='color: white;'>
		{$my_caption_at.content.output.output_text}</div>
		</div>
		</div>*}

	{/if}

	{undef} 

	{/let}

{/if}
