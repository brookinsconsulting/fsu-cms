
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en-US" lang="en-US">	
															<head>
                      
    <title>FSU_com / FSU - FSU.com - FSU Communications Group</title>

    
    
    
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

        <meta http-equiv="Content-language" content="en-US" />

    
        <meta name="author" content="Florida State University Communications Group" />

        <meta name="copyright" content="eZ Systems" />

        <meta name="description" content="Content Management System" />

        <meta name="keywords" content="fsu, news, florida state, campus" />

    
    <meta name="MSSmartTagsPreventParsing" content="TRUE" />
    <meta name="generator" content="eZ Publish" />
    
<link rel="Home" href="/" title="FSU.com - FSU Communications Group front page" />
<link rel="Index" href="/" />

<link rel="Top"  href="/" title="FSU_com / FSU - FSU.com - FSU Communications Group" />
<link rel="Search" href="/content/advancedsearch" title="Search FSU.com - FSU Communications Group" />
<link rel="Shortcut icon" href="/design/fsu/images/favicon.ico" type="image/x-icon" />
<link rel="Copyright" href="/ezinfo/copyright" />
<link rel="Author" href="/ezinfo/about" />
<link rel="Alternate" href="/layout/set/print/" media="print" title="Printable version" />
<style type="text/css">
    @import url("/extension/ezwebin/design/ezwebin/stylesheets/core.css");
    @import url("/extension/ezwebin/design/ezwebin/stylesheets/pagelayout.css");
    @import url("/design/fsu/stylesheets/content.css");
    @import url("/extension/ezwebin/design/ezwebin/stylesheets/websitetoolbar.css");
    @import url("/var/storage/packages/ez_systems/ezwebin_design/files/default/file/classes-colors.css");
    @import url("/var/storage/packages/ez_systems/ezwebin_design/files/default/file/site-colors.css");
    @import url("/design/fsu/stylesheets/fsu_ez.css");
        @import url("/design/fsu/stylesheets/StyleLevel2.css");
        @import url("/design/fsucom/stylesheets/fsucom.css");

    </style>
<link rel="stylesheet" type="text/css" href="/extension/ezwebin/design/ezwebin/stylesheets/print.css" media="print" />
<!-- IE conditional comments; for bug fixes for different IE versions -->
<!--[if IE 5]>     <style type="text/css"> @import url(/design/fsu/stylesheets/browsers/ie5.css);    </style> <![endif]-->
<!--[if lte IE 7]> <style type="text/css"> @import url(/design/fsu/stylesheets/browsers/ie7lte.css); </style> <![endif]-->    <script language="javascript" type="text/javascript" src="/design/fsucom/javascript/jquery-1.3.2.min.js"></script>    <script language="javascript" type="text/javascript" src="/design/fsucom/javascript/jquery.galleryScroll1.3.1.js"></script>    <script language="javascript" type="text/javascript" src="/design/fsucom/javascript/jquery.galleryScroll1.3.1-vertical.js"></script>    <script language="javascript" type="text/javascript" src="/design/fsucom/javascript/jquery-ui-1.7.custom.min.js"></script>    <script language="javascript" type="text/javascript" src="/design/fsucom/javascript/site_custom.js"></script>    <script language="javascript" type="text/javascript" src="/design/fsucom/javascript/thickbox_compressed.js"></script>    <script language="javascript" type="text/javascript" src="/design/fsucom/javascript/cufon-yui.js"></script>    <script language="javascript" type="text/javascript" src="/design/fsucom/javascript/agaramond.js"></script>    <script language="javascript" type="text/javascript" src="/extension/ngflashvideo/design/standard/javascript/ufo.js"></script>    <script language="javascript" type="text/javascript" src="/design/fsu/javascript/fsu_common.js"></script>    <script language="javascript" type="text/javascript" src="/design/fsucom/javascript/jquery.js"></script>    <script language="javascript" type="text/javascript" src="/design/fsucom/javascript/thickbox_compressed.js"></script>

<script type="text/javascript">
         Cufon.replace('h1');
</script>
<script type="text/javascript">
	$(function(){

		// Accordion
		$("#accordion").accordion({ header: "h3" });

		// Tabs
		$('#tabs').tabs();
		
		//hover states on the static widgets
		$('#dialog_link, ul#icons li').hover(
			function() { $(this).addClass('ui-state-hover'); }, 
			function() { $(this).removeClass('ui-state-hover'); }
		);
		
	});
</script>
</head>                                                                
<body class="subtree_level_0_node_id_2 subtree_level_1_node_id_3132 site_fsucom mod_name_content mod_func_view mod_view_full class_frontpage node_3132">



		<div class="bof" id="col_wrapper">
				  				
			
			
			<div id='full_width_float'>

			<div class="bofMiddleColumn no_back">

			
			  <div class="bofMiddleText no_page_notes">
				

<div id='maincontent_design'>

Test

</div><!-- id='maincontent_design' -->
			  </div>
			</div>
		</div>

<div class="bofLeftColumn">  
   
Woo

         
</div>

</div>

</body>
</html>

