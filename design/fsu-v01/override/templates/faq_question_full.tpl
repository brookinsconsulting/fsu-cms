{* FAQ Question - Full view *}



<li>	
<div class="content-view-full">
    <div class="class-faq_question">
        <h3>{attribute_view_gui attribute=$node.object.data_map.name}</h3>
        <div class="attribute-long">
            {attribute_view_gui attribute=$node.object.data_map.answer}
        </div>
    </div>
</div>

<a href={fetch(content, node, hash('node_id', $node.parent_node_id)).url_alias|ezroot}>Back to related questions</a>

</li>
