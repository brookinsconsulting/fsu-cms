<div class="content-view-embed">
<div class="class-profile" style="width: {$object.data_map.image.content[$object_parameters.size].width}px">
    <div class="attribute-image">{if is_set( $link_parameters.href )}{attribute_view_gui attribute=$object.data_map.image image_class=$object_parameters.size href=$link_parameters.href|ezroot target=$link_parameters.target}{else}{attribute_view_gui attribute=$object.data_map.image image_class=$object_parameters.size}{/if}</div>

    {if $object.data_map.job_description.has_content}
    {if is_set( $object.data_map.image.content[$object_parameters.size].width )}
    <div class="attribute-job_description" style="width: {$object.data_map.image.content[$object_parameters.size].width}px"> {else}
        <div class="attribute-job_description"> {/if}
            {attribute_view_gui attribute=$object.data_map.job_description} </div>
        {/if} </div>
</div>