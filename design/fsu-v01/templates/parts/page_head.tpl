<head>
{def $basket_is_empty   = cond($current_user.is_logged_in, fetch( shop, basket ).is_empty, 1)
     $current_node_id   = first_set($module_result.node_id, 0)
     $user_hash         = concat($current_user.role_id_list|implode( ',' ), ',', $current_user.limited_assignment_value_list|implode( ',' ))}

{def $pagestyle       = 'nosidemenu noextrainfo'
     $infobox_count   = 0
     $locales         = fetch( 'content', 'translation_list' )
     $pagerootdepth   = ezini( 'SiteSettings', 'RootNodeDepth', 'site.ini' )
     $indexpage       = 2
     $path_normalized = ''
     $pagedesign      = fetch( 'content', 'object', hash( 'object_id', '54' ) )
}


{if is_unset( $load_css_file_list )}
    {def $load_css_file_list = true()}
{/if}

{if $load_css_file_list}
  {ezcss_load( array( 'core.css',
                      'debug.css',
                      'pagelayout.css',
                      'content.css',
                      'websitetoolbar.css',
                      ezini( 'StylesheetSettings', 'CSSFileList', 'design.ini' ),
                      ezini( 'StylesheetSettings', 'FrontendCSSFileList', 'design.ini' ) ))}
{else}
  {ezcss_load( array( 'core.css',
                      'debug.css',
                      'pagelayout.css',
                      'content.css',
                      'websitetoolbar.css' ))}
{/if}

{def $current_node = fetch('content','node', hash('node_id', $module_result.node_id))}
{set-block scope=root variable=site_title}
FSU - {$site.title|wash}
{/set-block}
<title>{if $current_node.data_map.meta_title.has_content}{$current_node.data_map.meta_title.content|wash()|trim()}{else}{$site_title}{/if}</title>

{section show=and(is_set($#Header:extra_data),is_array($#Header:extra_data))}
{section name=ExtraData loop=$#Header:extra_data}
{$:item}
{/section}
{/section}

{* check if we need a http-equiv refresh *}
{if $site.redirect}
<meta http-equiv="Refresh" content="{$site.redirect.timer}; URL={$site.redirect.location}" />

{/if}

{section name=HTTP loop=$site.http_equiv}
<meta http-equiv="{$HTTP:key|wash}" content="{$HTTP:item|wash}" />

{/section}

{foreach $site.meta as $key=>$item}
{if array('keywords','description')|contains($key)}
{if and(is_set($current_node.data_map[concat('meta_',$key)]), $current_node.data_map[concat('meta_',$key)].has_content)}
{set-block variable='item'}{attribute_view_gui attribute=$current_node.data_map[concat('meta_',$key)]}{/set-block}
{/if}
{if and(eq($key, 'keywords'), ne($current_node.data_map.meta_tags.content,''))}
{set-block variable='item'}{attribute_view_gui attribute=$current_node.data_map[concat('meta_tags')]}{/set-block}
{/if}
{/if}
<meta name="{$key|wash()}" content="{$item|wash()|trim()}" />
{/foreach}

<style type="text/css">

{* no longer needed-MLM 052809     @import url({ezini('StylesheetSettings','ClassesCSS','design.ini')|ezroot(double)});
    @import url({ezini('StylesheetSettings','SiteCSS','design.ini')|ezroot(double)});  *}
    {foreach ezini( 'StylesheetSettings', 'CSSFileList', 'design.ini' ) as $css_file}
    @import url({concat( 'stylesheets/', $css_file )|ezdesign});
    {/foreach}
    @import url({"stylesheets/fsu_ez.css"|ezdesign(double)});
</style>

<link rel="stylesheet" type="text/css" href={ezini('StylesheetSettings','ClassesCSS','design.ini')|ezroot()} />
<link rel="stylesheet" type="text/css" href={ezini('StylesheetSettings','SiteCSS','design.ini')|ezroot()} />
<link rel="stylesheet" type="text/css" href={"stylesheets/print.css"|ezdesign} media="print" />
<!-- IE conditional comments; for bug fixes for different IE versions -->
<!--[if IE 5]>     <style type="text/css"> @import url({"stylesheets/browsers/ie5.css"|ezdesign(no)});    </style> <![endif]-->
<!--[if lte IE 7]> <style type="text/css"> @import url({"stylesheets/browsers/ie7lte.css"|ezdesign(no)}); </style> <![endif]-->
{*
{foreach ezini( 'JavaScriptSettings', 'JavaScriptList', 'design.ini' ) as $script}
    <script language="javascript" type="text/javascript" src={concat( 'javascript/', $script )|ezdesign}></script>
{/foreach}
*}

{* Load JavaScript dependencys + JavaScriptList *}
{ezscript_load( array( ezini( 'JavaScriptSettings', 'JavaScriptList', 'design.ini' ), ezini( 'JavaScriptSettings', 'FrontendJavaScriptList', 'design.ini' ) ) )}

{if $current_user.is_logged_in}
    <script language="javascript" type="text/javascript" src={"javascript/jquery.spellayt.min.js"|ezdesign}></script>
    <script language="javascript" type="text/javascript" src={"javascript/jquery.bgiframe.min.js"|ezdesign}></script>
    <style type="text/css" src={"stylesheets/jquery.spellayt.css"|ezdesign}></style>
{literal}
	<script type="text/javascript">
	        $(function(){
 				$('input').spellayt();
			});
	</script>
{/literal}
{/if}

{* Google Analytics Tracking Code *}
{literal}
<script type="text/javascript">

  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-5213390-1']);
  _gaq.push(['_setDomainName', 'fsu.edu']);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();

</script>
{/literal}
</head>
