{def $docs=treemenu( $module_result.path, is_set($module_result.node_id)|choose(144, $module_result.node_id ), array( 'folder', 'article', 'info_page', 'forum', 'frontpage', 'event_calendar', 'faq_list', 'gallery', 'weblog'), 1, 2)
     $depth=1 $last=hash('level', 0) $lastlevel=0 $treeclass=''}    

{if is_set($module_result.node_id)}
<ul id='l_nav'> 

{foreach $docs as $menu}     
   {set lastlevel=$last.level
	 treeclass = or(array('Info Page', 'Article')|contains($menu.class_name)|not, and(array('Info Page', 'Article')|contains($menu.class_name),$menu.data_map.nav_display.data_int))|choose('hidden', '')}     
   {while and($lastlevel|ne(0), $lastlevel|gt($menu.level))}         
	</ol>                 
	</li>   
	{set lastlevel=$lastlevel|sub(1)}     
   {/while}        
   {if and($last|ne(0), $last.level|lt($menu.level))}         
	<ol>                      
   {/if}  
   <li class="{$treeclass}">        
   {if eq($menu.id, $module_result.node_id)}                       
		<span class='noLink'>{$menu.text|shorten(45)}</span>
   {else}
	<a {and(gt($module_result.path|count,3),eq($menu.id,$module_result.path[3]['node_id'])|choose("","class='noLink'"))} href={$menu.url_alias|ezroot}>{$menu.text|shorten(45)}</a>                 
   {/if}      
      
   {set last=$menu} 
{/foreach}  

{while $depth|gt(1)}
   </li></ol>
   {set depth=$depth|sub(1)}
{/while}
           
</ul>

{else}

<ul>
<ol><a href='/'>Home</a>
</ol>
</ul>


{/if}


{undef}
