{* FAQ List - Full view *}
<div class="content-view-full">
    <div class="class-folder">

        <h1>{$node.object.data_map.name.content|wash()}</h1>

       <hr>

        {section show=$node.object.data_map.description.has_content}
            <div class="attribute-short">
                {attribute_view_gui attribute=$node.object.data_map.description}
            </div>
        {/section}


        {section show=is_unset( $versionview_mode )}
            {let page_limit=100
                 list_items=array()
                 list_count=0}

                {set list_items=fetch_alias( children, hash( parent_node_id, $node.node_id,
                                                             offset, $view_parameters.offset,
                                                             sort_by, $node.sort_array,
							     class_filter_type, 'include',
							     class_filter_array, array( 'faq_question' ),
                                                             limit, $page_limit ) )}
                {set list_count=$list_items|count}

	    	{section show=gt($list_items|count,0)}

	    	    <h2>Frequent Questions and Answers</h2>

	    	    <hr>

            	    <div class="content-view-children"><ol>
                	{section var=child loop=$list_items sequence=array(bglight,bgdark)}
                    	    {node_view_gui view=line content_node=$child}
                	{/section}
            	    </ol></div>

            	{/section}

		<hr>

        {section show=$node.object.data_map.footer.has_content}
            <div class="attribute-short">
                {attribute_view_gui attribute=$node.object.data_map.footer}
            </div>
        {/section}


            	{include name=navigator
                     uri='design:navigator/google.tpl'
                     page_uri=$node.url_alias
                     item_count=$list_count
                     view_parameters=$view_parameters
                     item_limit=$page_limit}
            {/let}

        {/section}

    </div>
</div>
