<hr />
<div id='reader_links'>

{def $icon_size='small' 
     $reader_link_ar=hash('text/xml', array('MS Excel Viewer','http://www.microsoft.com/downloads/details.aspx?FamilyID=c8378bf4-996c-4569-b547-75edbd03aaf0&displaylang=EN'), 'application/pdf', array('Adobe PDF reader','http://get.adobe.com/reader/'), 'application/vnd.ms-powerpoint', array('MS Powerpoint Viewer','http://www.microsoft.com/downloads/details.aspx?FamilyID=428d5727-43ab-4f24-90b7-a94784af71a4&displaylang=en'), 'application/msword', array('MS Word Viewer','http://www.microsoft.com/downloads/en/details.aspx?FamilyID=3657ce88-7cfa-457a-9aec-f4f827f20cac&displaylang=en'), 'application/vnd.ms-excel', array('MS Excel Viewer','http://www.microsoft.com/downloads/details.aspx?FamilyID=c8378bf4-996c-4569-b547-75edbd03aaf0&displaylang=EN'), 'video/quicktime', array('Quicktime Media Player','http://www.apple.com/quicktime/download/win.html'), 'video', array('Windows Media Player','http://www.microsoft.com/windows/windowsmedia/player/download/download.aspx'), 'audio', array('Windows Media Player','http://www.microsoft.com/windows/windowsmedia/player/download/download.aspx'))}

<h3>Document reader download links</h3>

{foreach $readers|explode('|')|unique|reverse as $this_reader offset 1 }

   {if is_set($reader_link_ar[$this_reader])}
   {$this_reader|mimetype_icon( $icon_size,  $this_reader)}&nbsp;<a href = '{$reader_link_ar[$this_reader].1}'>{$reader_link_ar[$this_reader].0}</a>
   {delimiter}&nbsp;|&nbsp;{/delimiter}
   {/if}

{/foreach}



</div>