{* Info page template *}

<div class="content-view-full">
    <div class="class-info-page">

<div class="attribute-header">
<h1>{$node.name|wash}</h1>
</div>


{if $node.object.data_map.description.has_content}
   <div class="attribute-long">
   {attribute_view_gui attribute=$node.data_map.description}
   </div>
{/if}


</div></div>

