{def $comment_count = fetch( 'content', 'list_count', hash( 'parent_node_id', $node.node_id,
                                                                    'class_filter_type', 'include',
                                                                    'class_filter_array', array( 'comment' ) ) )}
<div class='stat-box-line'>
   <p class='stat-date'><strong>Added:</strong> {$node.object.published|nicedate}</p>
   <p class='stat-views'><strong>Views:</strong> {$vid_data['hits_count']}</p>
   <p class='stat-length'><strong>Length (mins):</strong> {$vid_data['duration']|minsandsecs}</p>
	{if gt($comment_count,1)}
   <p class='stat-comments'><strong><a href={concat( $node.url_alias, "#comments" )|ezroot}>Comments:</strong> {$comment_count}</a></p>
	{/if}
</div>

