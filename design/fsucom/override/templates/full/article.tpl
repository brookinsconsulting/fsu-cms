{* Article - Full view *}

    <div class="content-view-full">
        <div class="class-article">

        <div class="attribute-header">
            <h1>{$node.data_map.title.content|wash()}</h1>
        </div>

		<div class='addthis'>
		<!-- ADDTHIS BUTTON BEGIN -->
		<script type="text/javascript">
		addthis_pub             = 'fsumedia';
		addthis_logo            = 'http://www.fsu.com/video/search.gif';
		addthis_logo_background = 'cccc99';
		addthis_logo_color      = '540115';
		addthis_brand           = 'FSU.com';
		addthis_options         = 'favorites, email, digg, delicious, myspace, facebook, google, live, more';
		</script>
		<a href="http://www.addthis.com/bookmark.php" onmouseover="return addthis_open(this, '', '[URL]', '[TITLE]')" onmouseout="addthis_close()" onclick="return addthis_sendto()"><img src="http://s9.addthis.com/button1-share.gif" width="125" height="16" border="0" alt="" vspace="10" /></a>
		<script type="text/javascript" src="http://s7.addthis.com/js/152/addthis_widget.js"></script>
		<!-- ADDTHIS BUTTON END -->
		</div>

        <div class="attribute-byline">
        {if $node.data_map.author.content.is_empty|not()}
        <p class="author">
             {attribute_view_gui attribute=$node.data_map.author}
        </p>
        {/if}
        <p class="date">
             {$node.object.published|l10n(shortdatetime)}
        </p>
        </div>

        {if eq( ezini( 'article', 'ImageInFullView', 'content.ini' ), 'enabled' )}
            {if $node.data_map.image.has_content}
                <div class='object-left'>
		   <div class="attribute-image">
                    {attribute_view_gui attribute=$node.data_map.image image_class=medium}
                    {if $node.data_map.image_caption.has_content}
                    <div class="attribute-caption" style="width:{$node.data_map.image.content['medium'].width}px;">
                        {attribute_view_gui attribute=$node.data_map.image_caption}
                    </div>
                    {/if}
		   </div>
		</div>
            {/if}
        {/if}

        {if eq( ezini( 'article', 'SummaryInFullView', 'content.ini' ), 'enabled' )}
            {if $node.data_map.intro.content.is_empty|not}
                <div class="attribute-short">
                    {attribute_view_gui attribute=$node.data_map.intro}
                </div>
            {/if}
        {/if}

        {if $node.data_map.body.content.is_empty|not}
            <div class="attribute-long">
                {attribute_view_gui attribute=$node.data_map.body}
            </div>
        {/if}

        {if is_unset( $versionview_mode )}
        {if $node.data_map.enable_comments.data_int}
            <h1>{"Comments"|i18n("design/ezwebin/full/article")}</h1>
                <div class="content-view-children">
                    {foreach fetch_alias( comments, hash( parent_node_id, $node.node_id ) ) as $comment}
                        {node_view_gui view='line' content_node=$comment}
                    {/foreach}
                </div>

                {if fetch( 'content', 'access', hash( 'access', 'create',
                                                      'contentobject', $node,
                                                      'contentclass_id', 'comment' ) )}
                    <form method="post" action={"content/action"|ezroot}>
                    <input type="hidden" name="ClassIdentifier" value="comment" />
                    <input type="hidden" name="NodeID" value="{$node.object.main_node.node_id}" />
                    <input type="hidden" name="ContentLanguageCode" value="{ezini( 'RegionalSettings', 'Locale', 'site.ini')}" />
                    <input class="button new_comment" type="submit" name="NewButton" value="{'New comment'|i18n( 'design/ezwebin/full/article' )}" />
                    </form>
                {else}
                    <p>{'%login_link_startLog in%login_link_end or %create_link_startcreate a user account%create_link_end to comment.'|i18n( 'design/ezwebin/full/article', , hash( '%login_link_start', concat( '<a href="', '/user/login'|ezroot(no), '">' ), '%login_link_end', '</a>', '%create_link_start', concat( '<a href="', "/user/register"|ezroot(no), '">' ), '%create_link_end', '</a>' ) )}</p>
                {/if}
        {/if}
        {/if}

		{include uri='design:parts/related_recent.tpl' node=$node}


		<div class="presented-by">
{*
	<!--/* OpenX Javascript Tag v2.8.0 */-->

	<!--/*
	  * The backup image section of this tag has been generated for use on a
	  * non-SSL page. If this tag is to be placed on an SSL page, change the
	  *   'http://www.fsu.com/openx/www/delivery/...'
	  * to
	  *   'https://www.fsu.com/openx/www/delivery/...'
	  *
	  * This noscript section of this tag only shows image banners. There
	  * is no width or height in these banners, so if you want these tags to
	  * allocate space for the ad before it shows, you will need to add this
	  * information to the <img> tag.
	  *
	  * If you do not want to deal with the intricities of the noscript
	  * section, delete the tag (from <noscript>... to </noscript>). On
	  * average, the noscript tag is called from less than 1% of internet
	  * users.
	  */-->
*}
	<script type='text/javascript'><!--//<![CDATA[
	   var m3_u = (location.protocol=='https:'?'https://www.fsu.com/openx/www/delivery/ajs.php':'http://www.fsu.com/openx/www/delivery/ajs.php');
	   var m3_r = Math.floor(Math.random()*99999999999);
	   if (!document.MAX_used) document.MAX_used = ',';
	   document.write ("<scr"+"ipt type='text/javascript' src='"+m3_u);
	   document.write ("?zoneid=4");
	   document.write ('&amp;cb=' + m3_r);
	   if (document.MAX_used != ',') document.write ("&amp;exclude=" + document.MAX_used);
	   document.write (document.charset ? '&amp;charset='+document.charset : (document.characterSet ? '&amp;charset='+document.characterSet : ''));
	   document.write ("&amp;loc=" + escape(window.location));
	   if (document.referrer) document.write ("&amp;referer=" + escape(document.referrer));
	   if (document.context) document.write ("&context=" + escape(document.context));
	   if (document.mmm_fo) document.write ("&amp;mmm_fo=1");
	   document.write ("'><\/scr"+"ipt>");
	//]]>--></script><noscript><a href='http://www.fsu.com/openx/www/delivery/ck.php?n=a3ef782d&amp;cb=INSERT_RANDOM_NUMBER_HERE' target='_blank'><img src='http://www.fsu.com/openx/www/delivery/avw.php?zoneid=4&amp;cb=INSERT_RANDOM_NUMBER_HERE&amp;n=a3ef782d' border='0' alt='' /></a></noscript>
	
	</div>

{* hide in favor of addthis widget 
        {def $tipafriend_access=fetch( 'user', 'has_access_to', hash( 'module', 'content',
                                                                      'function', 'tipafriend' ) )}
        {if and( ezmodule( 'content/tipafriend' ), $tipafriend_access )}
        <div class="attribute-tipafriend">
            <p><a href={concat( "/content/tipafriend/", $node.node_id )|ezroot} title="{'Tip a friend'|i18n( 'design/ezwebin/full/article' )}">{'Tip a friend'|i18n( 'design/ezwebin/full/article' )}</a></p>
        </div>
        {/if}
*}
        </div>
    </div>
