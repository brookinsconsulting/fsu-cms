{* Article - Node Feed View *}
{if is_unset($expanded)}{def $expanded = false()}{/if}
{if is_unset($length)}{def $length=100}{/if}
{if is_unset($image_class)}{def $image_class='articlethumbnail'}{/if}
{if is_unset($summary)}{def $summary=true()}{/if}
{if is_unset($datetime)}{def $datetime=false()}{/if}

<div class="content-view-node-feed">
   <div class="class-article float-break">
{if $expanded}
   {if $node.data_map.image.has_content}
	<div class="attribute-image">{attribute_view_gui image_class=$image_class href=$node.url_alias|ezroot attribute=$node.data_map.image}</div>
   {/if}
	<div class="attribute-header"><h3><a href={$node.url_alias|ezroot}>{$node.name|wash()}</a></h3></div>
   {if and($summary, not($node.data_map.intro.content.is_empty) )}
{def $readMore = concat('... <a href="',$node.url_alias|ezroot(no),'">Read Story...</a>')}
	<div class="attribute-short">{$node.data_map.intro.content.output.output_text|html_shorten($length, $readMore)}{* <a href="{$node.url_alias|ezroot(no)}">Read Story...*}</a></div>
   {/if}
{else}
	<div class="attribute-header"><a href="{$node.url_alias|ezroot(no)}">{$node.name}</a> {* $node.object.published|datetime('custom', '%M. %j, %Y') *}</div>
{/if}

   {* if $datetime}
	<div class="datetime">{$node.object.published|datetime('custom', $datetime)}</div>
   {/if *}
   </div>
</div>