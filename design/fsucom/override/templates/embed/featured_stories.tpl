{* Featured Stories - Embed *}
{def $featuredStories = fetch('content', 'list', hash('parent_node_id', $object.main_node_id,
						      'sort_by', array('priority', true()),
						      'class_filter_type', 'include',
						      'class_filter_array', array('article'),
						      'limit', $limit))
     $jsTabControls = array()}

{run-once}
<script type="text/javascript" src="{'javascript/jcarousellite_1.0.1.js'|ezdesign(no)}"></script>
{/run-once}
{*
	<li><img class="previous-btn" src="{'images/arrows/sm-left-arrow.png'|ezdesign(no)}" width="25" height="25" /></li>
	<li><img class="next-btn" src="{'images/arrows/sm-right-arrow.png'|ezdesign(no)}" width="25" height="25" /></li>
	<li><img class="pause-btn" src="{'images/arrows/sm-pause.png'|ezdesign(no)}" width="25" height="25" /></li>
*}
<div class="featured-stories">
{if gt($featuredStories|count(), 1)}
   <ul class="tabset-navigation">
	<li class="tab-btn previous-btn control-btn"><a href="#"><span>&laquo;</span></a></li>
{foreach $featuredStories as $key => $story}
   {set $jsTabControls = $jsTabControls|append(concat('#btn-slide-', $key))}
	<li id="btn-slide-{$key}" class="tab-btn {cond( eq($key,0), ' active','' )}"><a href="#">{$key|inc()}</a></li>
{/foreach}
	<li class="tab-btn next-btn control-btn"><a href="#"><span>&raquo;</span></a></li>
	<li class="tab-btn play-btn control-btn"><a href="#"><span>&gt;</span></a></li>
   </ul>
{/if}
<div class="tabset-header">
<img src="{'images/featured-stories.png'|ezdesign(no)}" width="231" height="29" />
{* <img src="{'images/presented-by.png'|ezdesign(no)}" width="64" height="15" />&nbsp; *}
{*
<!--/* OpenX Javascript Tag v2.8.0 */-->

<!--/*
  * The backup image section of this tag has been generated for use on a
  * non-SSL page. If this tag is to be placed on an SSL page, change the
  *   'http://fsucom.thinkcreative.com/openx/www/delivery/...'
  * to
  *   'https://fsucom.thinkcreative.com/openx/www/delivery/...'
  *
  * This noscript section of this tag only shows image banners. There
  * is no width or height in these banners, so if you want these tags to
  * allocate space for the ad before it shows, you will need to add this
  * information to the <img> tag.
  *
  * If you do not want to deal with the intricities of the noscript
  * section, delete the tag (from <noscript>... to </noscript>). On
  * average, the noscript tag is called from less than 1% of internet
  * users.
  */-->
*}

{*
<script type='text/javascript'><!--//<![CDATA[
   var m3_u = (location.protocol=='https:'?'https://fsucom.thinkcreative.com/openx/www/delivery/ajs.php':'http://fsucom.thinkcreative.com/openx/www/delivery/ajs.php');
   var m3_r = Math.floor(Math.random()*99999999999);
   if (!document.MAX_used) document.MAX_used = ',';
   document.write ("<scr"+"ipt type='text/javascript' src='"+m3_u);
   document.write ("?zoneid=3");
   document.write ('&amp;cb=' + m3_r);
   if (document.MAX_used != ',') document.write ("&amp;exclude=" + document.MAX_used);
   document.write (document.charset ? '&amp;charset='+document.charset : (document.characterSet ? '&amp;charset='+document.characterSet : ''));
   document.write ("&amp;loc=" + escape(window.location));
   if (document.referrer) document.write ("&amp;referer=" + escape(document.referrer));
   if (document.context) document.write ("&context=" + escape(document.context));
   if (document.mmm_fo) document.write ("&amp;mmm_fo=1");
   document.write ("'><\/scr"+"ipt>");
//]]>--></script><noscript><a href='http://fsucom.thinkcreative.com/openx/www/delivery/ck.php?n=a3a17c55&amp;cb=INSERT_RANDOM_NUMBER_HERE' target='_blank'><img src='http://fsucom.thinkcreative.com/openx/www/delivery/avw.php?zoneid=3&amp;cb=INSERT_RANDOM_NUMBER_HERE&amp;n=a3a17c55' border='0' alt='' /></a></noscript>
*}

</div>

<div id="stories-tabset" class="tabset-slides{cond( eq($featuredStories|count(),1), ' single', ' multiple')}">
<ul>
{foreach $featuredStories as $key => $story}
  <li id="slide-{$key}">
	{node_view_gui content_node=$story view='node_feed' expanded=true() length=400 image_class='featuredstory'}
  </li>
{/foreach}
</ul>
</div>

{if gt($featuredStories|count(), 1)}
<script type="text/javascript">
var tabControls = ["{$jsTabControls|implode('","')}"];
var autoSlide = {cond( gt($featuredStories|count(),1), 10000, 'null' )};
feature_slides();
</script>
{/if}

</div>
