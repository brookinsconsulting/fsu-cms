{* Page Feed - Embed *}

{def $children = fetch('content', 'list', hash('parent_node_id', $object.main_node_id,
					       'limit', $object_parameters.limit,
					       'sort_by', $object.main_node.sort_array,
					       'class_filter_type', 'exclude',
					       'class_filter_array', array('folder')))
     $length = 100}

{if eq($object.main_node_id, 3170)}
{set $children = fetch('content', 'tree', hash('parent_node_id', $object.main_node_id,
					       'limit', $object_parameters.limit,
					       'sort_by', array('published', false()),
					       'class_filter_type', 'include',
					       'class_filter_array', array('video','botr')))}
{/if}

{* <div class="presented-by"><img src="{'images/presented-by.png'|ezdesign(no)}" width="64" height="15" />&nbsp;

	<script type='text/javascript'><!--//<![CDATA[
	   var m3_u = (location.protocol=='https:'?'https://fsucom.thinkcreative.com/openx/www/delivery/ajs.php':'http://fsucom.thinkcreative.com/openx/www/delivery/ajs.php');
	   var m3_r = Math.floor(Math.random()*99999999999);
	   if (!document.MAX_used) document.MAX_used = ',';
	   document.write ("<scr"+"ipt type='text/javascript' src='"+m3_u);
	   document.write ("?zoneid=1");
	   document.write ('&amp;cb=' + m3_r);
	   if (document.MAX_used != ',') document.write ("&amp;exclude=" + document.MAX_used);
	   document.write (document.charset ? '&amp;charset='+document.charset : (document.characterSet ? '&amp;charset='+document.characterSet : ''));
	   document.write ("&amp;loc=" + escape(window.location));
	   if (document.referrer) document.write ("&amp;referer=" + escape(document.referrer));
	   if (document.context) document.write ("&context=" + escape(document.context));
	   if (document.mmm_fo) document.write ("&amp;mmm_fo=1");
	   document.write ("'><\/scr"+"ipt>");
	//]]>--></script><noscript><a href='http://fsucom.thinkcreative.com/openx/www/delivery/ck.php?n=a3bfa046&amp;cb=INSERT_RANDOM_NUMBER_HERE' target='_blank'><img src='http://fsucom.thinkcreative.com/openx/www/delivery/avw.php?zoneid=1&amp;cb=INSERT_RANDOM_NUMBER_HERE&amp;n=a3bfa046' border='0' alt='' /></a></noscript>
	
	
	</div>
*}

<h2>{$object_parameters.header|wash()}</h2>

{foreach $children as $key => $child}
	{node_view_gui content_node=$child view='node_feed' expanded=lt($key, $object_parameters.expanded_count) length=$length}
{/foreach}

<hr />

<a class="more-link" href="{$object.main_node.url_alias|ezroot(no)}/(offset)/{$object_parameters.limit}">{$object_parameters.more_text|wash()}</a>
