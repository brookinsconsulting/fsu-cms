{* Audio Slideshow - Line View *}

<div class="content-view-line">
    <div class="class-audio_slideshow">

{if $node.data_map.image.has_content}
	<div class="attribute-image">
		{attribute_view_gui attribute=$node.data_map.image image_class='medium' href=$node.url_alias|ezroot}
	</div>
{/if}
         <h2><a href="{$node.url_alias|ezroot('no')}" title="{$node.name|wash()}">{$node.name|wash()}</a></h2>

{if $node.data_map.description.has_content}
    <div class="attribute-long">
	{attribute_view_gui attribute=$node.data_map.description}
    </div>
{/if}
<div class='break'></div>
    </div>
</div>