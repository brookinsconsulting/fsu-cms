{if 1|eq(0)}

{def $vid_data = $node.data_map.vid.content|video
     $vid_image = 'video-bkgnd.png'|ezimage(no)
     $description = $node.data_map.description.content.output.output_text}

<div class="content-view-line">
    <div class="class-video float-break">


<div class='vid-box-line'>
{if $node.data_map.still_frame.content.is_valid}
    {set vid_image = $node.data_map.still_frame.content['still_frame'].url|ezroot(no)}
<a href={$node.url_alias|ezroot}><img src='{$vid_image}' alt='{$node.name|wash}' /></a>
{else}
<a href={$node.url_alias|ezroot}><img src='http://video.thinkcreative.com/screenshots/{$node.data_map.vid.content}?width=320' width=270 
alt='{$node.name|wash}' 
/></a>
{/if}
</div>

<div class='com-box-line'>
<h3><a href={$node.url_alias|ezroot}>{$node.name|wash}</a></h3>
{if $description|trim|begins_with('<p>')|not}<p>{/if}{$description}{if $description|trim|begins_with('<p>')|not}</p>{/if}
</div>

{include uri='design:parts/vid_stats.tpl'}

    </div>

</div>

{undef}


{else}




{if is_set($node.data_map.description)}
	{def $description = $node.data_map.description.content.output.output_text}
{else}
	{def $botr = botr_api($node.data_map.video.content)
		 $description = $botr['response'][0]['video']['description']}
{/if}


{def $vid_image = 'video-bkgnd.png'|ezimage(no)}


<div class="content-view-line">
    <div class="class-video float-break">


<div class='vid-box-line'>
{if $node.data_map.still_frame.content.is_valid}
    {set vid_image = $node.data_map.still_frame.content['still_frame'].url|ezroot(no)}
<a href={$node.url_alias|ezroot}><img src='{$vid_image}' alt='{$node.name|wash}' /></a>
{else}
<a href={$node.url_alias|ezroot}><img src='http://cdn.thinkcreative.com/thumbs/{$node.data_map.video.content}-270.jpg' width=270 
alt='{$node.name|wash}' 
/></a>
{/if}
</div>

<div class='com-box-line'>
<h3><a href={$node.url_alias|ezroot}>{$node.name|wash}</a></h3>
{if $description|trim|begins_with('<p>')|not}<p>{/if}{$description}{if $description|trim|begins_with('<p>')|not}</p>{/if}
</div>

    </div>

</div>

{undef}


{/if}
