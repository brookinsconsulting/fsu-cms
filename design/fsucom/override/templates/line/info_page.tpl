{* Info Page - Line view *}

<div class="content-view-line">
   <div class="class-info-page">

      <div class="attribute-header">
	<h2><a href={$node.url_alias|ezroot()}>{$node.data_map.name|wash()}</a></h2>
      </div>

{if $node.data_map.short_description.has_content}
      <div class="attribute-short">
	{attribute_view_gui attribute=$node.data_map.short_description}
      </div>
{/if}

   </div>
</div>