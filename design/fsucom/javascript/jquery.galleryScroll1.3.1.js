/*
 * jQuery galleryScroll v1.3.1 
 *
 * Copyright (c) 2008 Taranets Aleksey
 * email: aleks_tar@ukr.net
 * Licensed under the MIT License:
 * http://www.opensource.org/licenses/mit-license.php
 */

/*
	************* OPTIONS ************************************** default ****************
	btPrev         - link for previos [selector]    	btPrev: 'a.link-prev'
	btNext         - link for next [selector]		btNext: 'a.link-next'
	holderList     - image list holder [Tag name]		holderList: 'div'
	scrollElParent - list [Tag name]			scrollElParent: 'ul'
	scrollEl       - list element [Tag name]		scrollEl: 'li'
	slideNum       - view slide numbers [boolean]		slideNum: false
	duration       - duration slide [1000 - 1sec]		duration : 1000
	step           - slide step [int]			step: false
	circleSlide    - slide circle [boolean]			circleSlide: true
	disableClass   - class for disable link	[string] 	disableClass: 'disable'
	funcOnclick    - callback function			funcOnclick: null
	innerMargin    - inner margin, use width step [px]      innerMargin:0
	autoSlide      - auto slide [1000 - 1sec]               autoSlide:false
	*************************************************************************************
*/

jQuery.fn.galleryScroll = function(_options){
	// defaults options	
	var _options = jQuery.extend({
		btPrev: 'a.prev-arrow',
		btNext: 'a.next-arrow',
		holderList: 'div',
		scrollElParent: 'ul',
		scrollEl: 'li',
		slideNum: false,
		duration : 1000,
		step: 1,
		circleSlide: true,
		disableClass: 'disable',
		funcOnclick: null,
		autoSlide: 4000,
		innerMargin:0,
		hardRewind:true
	},_options);

	return this.each(function(){
		var _this = $(this);

		var _gWidth = jQuery(_options.holderList,_this).get(0).offsetWidth;
		var _liWidth = jQuery(_options.scrollEl,_this).get(0).offsetWidth;
		var _liSum = jQuery(_options.scrollEl,_this).length * _liWidth;
		var _margin = 0;
		var f = 0;
		var _step = 0;
		var _timerSlide = null;
		if (!_options.step) _step = _gWidth; else _step = _options.step*_liWidth;
		
		if (!_options.circleSlide) {
			if (_options.innerMargin == _margin)
				jQuery(_options.btPrev,_this).addClass(_options.disableClass);
		}
		if (_options.slideNum && !_options.step) {
			var _lastSection = 0;
			var _sectionWidth = 0;
			while(_sectionWidth < _liSum)
			{
				 _sectionWidth = _sectionWidth + _gWidth;
				 if(_sectionWidth > _liSum) {
					_lastSection = _sectionWidth - _liSum;
				 }
			}
		}
		if (_options.autoSlide) {
			_timerSlide = setTimeout(function(){
				jQuery.fn.galleryScroll.autoSlide(_options.autoSlide)
			}, _options.autoSlide);
			jQuery(_options.scrollElParent,_this).hover(function(){
				clearTimeout(_timerSlide);
			}, function(){
				_timerSlide = setTimeout(function(){
					jQuery.fn.galleryScroll.autoSlide(_options.autoSlide)
				}, _options.autoSlide);
			});
		}
	
		// click button 'Next'
		jQuery(_options.btNext,_this).click(function(){
			jQuery(_options.btPrev,_this).removeClass(_options.disableClass);
			if (_liSum - _gWidth  <= _margin + _step - _options.innerMargin) {
				if (f == 0) {
					_margin = _liSum - _gWidth  + _options.innerMargin;
					f = 1;
					if (!_options.circleSlide) 
						jQuery(this).addClass(_options.disableClass);
				} 
				else {
					if (_options.circleSlide) 
						_margin = _options.innerMargin;
					f = 0;
				}
			} else _margin = _margin + _step;
			
			jQuery(_options.scrollElParent,_this).animate({marginLeft: -_margin+"px"}, {queue:false,duration: _options.duration });
			
			if (_timerSlide) {
				clearTimeout(_timerSlide);
				_timerSlide = setTimeout(function(){
					jQuery.fn.galleryScroll.autoSlide(_options.autoSlide)
				}, _options.autoSlide);
			}
			
			if (_options.slideNum && !_options.step) jQuery.fn.galleryScroll.numListActive(_margin,_options.slideNum,_gWidth,_lastSection);		
			if (jQuery.isFunction(_options.funcOnclick)) {
				_options.funcOnclick.apply(_this);
			}
			return false;
		});
		// click button 'Prev'
		jQuery(_options.btPrev, _this).click(function(){
			jQuery(_options.btNext,_this).removeClass(_options.disableClass);
			if (_margin - _step == -_step - _options.innerMargin ) {
				if (!_options.circleSlide) {
					jQuery(this).addClass(_options.disableClass);
					_margin = _options.innerMargin;
				} else 	{_margin = _liSum - _gWidth;f=1;}
			}
			else if (_margin - _step < _options.innerMargin && _margin - _step > -_step) _margin = _options.innerMargin;
			else {_margin = _margin - _step;f=0;};
			if (!_options.circleSlide && _margin == -_options.innerMargin) jQuery(this).addClass(_options.disableClass);
			
			jQuery(_options.scrollElParent,_this).animate({marginLeft: -_margin + "px"}, {queue:false, duration: _options.duration});
			
			if (_options.slideNum && !_options.step) jQuery.fn.galleryScroll.numListActive(_margin,_options.slideNum,_gWidth,_lastSection);
			
			if (_timerSlide) {
				clearTimeout(_timerSlide);
				_timerSlide = setTimeout(function(){
					jQuery.fn.galleryScroll.autoSlide(_options.autoSlide)
				}, _options.autoSlide);
			}
			
			if (jQuery.isFunction(_options.funcOnclick)) {
				_options.funcOnclick.apply(_this);
			}
			return false;
		});
		// auto slide
		jQuery.fn.galleryScroll.autoSlide = function(autoSlideDuration){
			hard_rewind = false;
			if (_options.circleSlide) {
				if (_liSum - _gWidth  <= _margin + _step - _options.innerMargin) {
					if (f == 0) {
						_margin = _liSum - _gWidth  + _options.innerMargin;
						f = 1;
						if (!_options.circleSlide) 
							jQuery(this).addClass(_options.disableClass);
					} 
					else {
						if (_options.circleSlide) 
							hard_rewind = true;
							_margin = _options.innerMargin;
						f = 0;
					}
				} else _margin = _margin + _step;
				
				if (hard_rewind && _options.hardRewind) {
					jQuery(_options.scrollElParent,_this).css({marginLeft: -_margin+"px"});
					_margin = _margin + _step;
				}
				
				jQuery(_options.scrollElParent,_this).animate({marginLeft: -_margin+"px"}, {queue:false,duration: _options.duration });
				
				_timerSlide = setTimeout(function(){
					jQuery.fn.galleryScroll.autoSlide(_options.autoSlide)
				}, _options.autoSlide);
			}

		};
		// Number list
		jQuery.fn.galleryScroll.numListCreate = function(_elNumList, _liSumWidth, _width, _section){
			var _numListElC = '';
			var _num = 1;
			var _difference = _liSumWidth + _section;
			while(_difference > 0)
			{
			     _difference = _difference - _width;
				 _numListElC += '<li><a href="">'+_num+'</a></li>';
				 _num++;
			}
			$(_elNumList).html('<ul>'+_numListElC+'</ul>');
		};
		jQuery.fn.galleryScroll.numListActive = function(_marginEl, _slideNum, _width, _section){
			$('a',_slideNum).removeClass('active');
			var _activeRange = _width - _section-1;
			var _n = 0;
			if (_marginEl != 0) {
				while (_marginEl > _activeRange) {
					_activeRange = (_n * _width) -_section-1;
					_n++;
				}
			}
			var _a  = (_activeRange+_section+1)/_width - 1;
			$('a',_slideNum).eq(_a).addClass('active');
		};
		if (_options.slideNum && !_options.step) {
			jQuery.fn.galleryScroll.numListCreate(_options.slideNum, _liSum, _gWidth,_lastSection);
			jQuery.fn.galleryScroll.numListActive(_margin, _options.slideNum,_gWidth,_lastSection);
			
			jQuery('a',_options.slideNum).click(function(){
				jQuery(_options.btPrev,_this).removeClass(_options.disableClass);
				jQuery(_options.btNext,_this).removeClass(_options.disableClass);
				
				var _indexNum = jQuery('a',_options.slideNum).index($(this));
				_margin = _step*_indexNum;
				if (_margin + _step > _liSum) {
					_margin = _margin - (_margin - _liSum) - _step;
					if (!_options.circleSlide) jQuery(_options.btNext, _this).addClass(_options.disableClass);
				}
				jQuery(_options.scrollElParent,_this).animate({marginLeft: -_margin + "px"}, {queue:false, duration: _options.duration});
				
				if (!_options.circleSlide && _margin==0) jQuery(_options.btPrev,_this).addClass(_options.disableClass);
				jQuery.fn.galleryScroll.numListActive(_margin, _options.slideNum,_gWidth,_lastSection);
				
				if (_timerSlide) {
					clearTimeout(_timerSlide);
					_timerSlide = setTimeout(function(){
						jQuery.fn.galleryScroll.autoSlide(_options.autoSlide)
					}, _options.autoSlide);
				}
				
				return false;
			});
		}
	});
}
