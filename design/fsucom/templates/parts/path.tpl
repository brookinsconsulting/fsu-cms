  <!-- Path content: START -->
  <p id="path">
  {foreach $module_result.path as $path offset 2}

  {run-once}
	<a href='/'>FSU.com</a>&nbsp;&nbsp;>&nbsp;&nbsp;
  {/run-once}

  {if $path.url}
    <a href={cond( is_set( $path.url_alias ), $path.url_alias,$path.url )|ezroot}>{$path.text|wash}</a>
  {else}
    {$path.text|wash}
  {/if}
  {delimiter}&nbsp;&nbsp;>&nbsp;&nbsp;{/delimiter}
  {/foreach}
  </p>
  <!-- Path content: END -->