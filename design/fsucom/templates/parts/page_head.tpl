<head>
{def $basket_is_empty   = cond($current_user.is_logged_in, fetch( shop, basket ).is_empty, 1)
     $current_node_id   = first_set($module_result.node_id, 0)
     $user_hash         = concat($current_user.role_id_list|implode( ',' ), ',', $current_user.limited_assignment_value_list|implode( ',' ))}

{cache-block keys=array($uri_string, $basket_is_empty, $user_hash)}
{def $pagestyle       = 'nosidemenu noextrainfo'
     $infobox_count   = 0
     $locales         = fetch( 'content', 'translation_list' )
     $pagerootdepth   = ezini( 'SiteSettings', 'RootNodeDepth', 'site.ini' )
     $indexpage       = 2
     $path_normalized = ''
     $pagedesign      = fetch( 'content', 'object', hash( 'object_id', '54' ) )
}
{include uri='design:page_head.tpl'}

<style type="text/css">
    @import url({"stylesheets/core.css"|ezdesign(double)});
    @import url({"stylesheets/pagelayout.css"|ezdesign(double)});
    @import url({"stylesheets/content.css"|ezdesign(double)});
    @import url({"stylesheets/websitetoolbar.css"|ezdesign(double)});
    @import url({ezini('StylesheetSettings','ClassesCSS','design.ini')|ezroot(double)});
    @import url({ezini('StylesheetSettings','SiteCSS','design.ini')|ezroot(double)});
    @import url({"stylesheets/fsu_ez.css"|ezdesign(double)});
    {foreach ezini( 'StylesheetSettings', 'CSSFileList', 'design.ini' ) as $css_file}
    @import url({concat( 'stylesheets/', $css_file )|ezdesign});
    {/foreach}
</style>
<link rel="stylesheet" type="text/css" href={"stylesheets/print.css"|ezdesign} media="print" />
<!-- IE conditional comments; for bug fixes for different IE versions -->
<!--[if IE 5]>     <style type="text/css"> @import url({"stylesheets/browsers/ie5.css"|ezdesign(no)});    </style> <![endif]-->
<!--[if lte IE 7]> <style type="text/css"> @import url({"stylesheets/browsers/ie7lte.css"|ezdesign(no)}); </style> <![endif]-->
{foreach ezini( 'JavaScriptSettings', 'JavaScriptList', 'design.ini' )|unique as $script}
    <script language="javascript" type="text/javascript" src={concat( 'javascript/', $script )|ezdesign}></script>
{/foreach}

{* 
	
<script language="javascript" type="text/javascript" src="/design/fsucom/javascript/jquery-1.3.2.min.js"></script>
<script language="javascript" type="text/javascript" src="/design/fsucom/javascript/jquery.galleryScroll1.3.1.js"></script>
<script language="javascript" type="text/javascript" src="/design/fsucom/javascript/jquery.galleryScroll1.3.1-vertical.js"></script>
<script language="javascript" type="text/javascript" src="/design/fsucom/javascript/jquery-ui-1.7.custom.min.js"></script>
<script language="javascript" type="text/javascript" src="/design/fsucom/javascript/thickbox_compressed.js"></script>
<script language="javascript" type="text/javascript" src="/design/fsucom/javascript/cufon-yui.js"></script>
<script language="javascript" type="text/javascript" src="/design/fsucom/javascript/agaramond.js"></script>
<script language="javascript" type="text/javascript" src="/design/fsucom/javascript/jquery.corner.js"></script>
<script language="javascript" type="text/javascript" src="/design/fsucom/javascript/jquery.equalheights.js"></script>
<script language="javascript" type="text/javascript" src="/design/fsucom/javascript/site_custom.js"></script>
<script language="javascript" type="text/javascript" src="/extension/phpicalendar/design/standard/javascript/phpicalendar/sessvars.js"></script>
<script language="javascript" type="text/javascript" src="/extension/ngflashvideo/design/standard/javascript/ufo.js"></script>
<script language="javascript" type="text/javascript" src="/extension/basicajax/design/standard/javascript/basicajax.js"></script>

*}

<!--[if IE]>
<link rel="stylesheet" type="text/css" href="{'stylesheets/ie.css'|ezdesign(no)}"  />
{*
<script type="text/javascript" src="{'javascript/excanvas.js'|ezdesign(no)}"></script>
<script type="text/javascript" src="{'javascript/roundCorners-1.0.1.js'|ezdesign(no)}"></script>
<script type="text/javascript" src="{'javascript/ie.js'|ezdesign(no)}"></script>
*}
<![endif]-->

{literal}
<script type="text/javascript">
	$(function(){

		// Accordion
		$("#accordion").accordion({ header: "h3" });

		// Tabs
		$('#tabs').tabs();
		
		//hover states on the static widgets
		$('#dialog_link, ul#icons li').hover(
			function() { $(this).addClass('ui-state-hover'); }, 
			function() { $(this).removeClass('ui-state-hover'); }
		);
		
	});
</script>{/literal}
</head>