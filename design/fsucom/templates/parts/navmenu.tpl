{* BEGIN FLYOUTS PART ONE *}
{*
{if or($current_user.groups|contains(12392), $current_user.groups|contains(12))}
*}
{literal}
<style>
#mainbox .bofLeftColumn {
/* causes links underneath where flyouts would appear to be unclickable
	width: 405px;
*/
}
.mod_func_edit #mainbox .bofLeftColumn {
	width: 225px;
}

#mainbox .bofLeftColumn div.class-infobox {
	width: 200px;
}
#mainbox .bofLeftColumn li {
	clear: both;
}
#mainbox .bofLeftColumn li:hover ol {
	left: 195px;
}
#mainbox .bofLeftColumn li:hover ol li {
	margin-left: 4px;
}
#mainbox .bofLeftColumn li:hover ol li a {
	padding-left: 15px;
	margin-left: 0;
}
#mainbox .bofLeftColumn li:hover ol li a:hover {
	width: 180px;
}
#mainbox .bofLeftColumn ol {
	background: #F8F8EA none repeat scroll 0%;
	left: -1000px;
	position: absolute;
	top: 0px;
	width: 195px;
}
html > body #l_nav {
	overflow: visible;
	float: left;
}
</style>
<script>
$(function(){
	
	
	if (typeof EventWorker == 'undefined') {
		
		function EventWorker() {
	        this.addHandler = EventWorker.addHandler;
	    }

	    // event worker static method
	    EventWorker.addHandler =
	     function(eventRef, func) {
	         var eventHandlers = eval(eventRef);
	         if (typeof eventHandlers == 'function') { // not first handler
	             eval(eventRef + " = function(event) {eventHandlers(event); func(event);}");
	         } else { // first handler
	             eval(eventRef + " = func;");
	         }
	     }
	}
    

	ieHover = function() {
	   if (document.getElementById('l_nav')) {
		var ieLIs = document.getElementById('l_nav').getElementsByTagName('li');
		for (var i=0; i<ieLIs.length; i++) if (ieLIs[i]) {
			ieLIs[i].onmouseover=function() {this.className+=" sfhover";}
			ieLIs[i].onmouseout=function() {this.className=this.className.replace(' sfhover', '');}
		}
	   }
	}

	function add_iframes() {
		if (document.getElementById('l_nav')) {
			var ieULs = document.getElementById('l_nav').getElementsByTagName('ol');
			/** IE script to cover <select> elements with <iframe>s **/
			for (j=0; j<ieULs.length; j++) {
				ieULs[j].innerHTML = ('<iframe security="restricted" src="/design/fsu/javascript/blank.html" scrolling="no" frameborder="0"></iframe>' + ieULs[j].innerHTML);
				var ieMat = ieULs[j].firstChild;
					ieMat.style.width=ieULs[j].offsetWidth+"px";
					ieMat.style.height=ieULs[j].offsetHeight+"px";	
					ieULs[j].style.zIndex="99";
			}
		}
	}

	if (window.attachEvent) window.attachEvent('onload', ieHover);
	EventWorker.addHandler("window.onload", add_iframes);
	
});
</script>
{/literal}
{*
{/if}
*}
{* END FLYOUTS PART ONE *}

{def $this_root_node = fetch(content, node, hash(node_id, ezini( 'NodeSettings', 'RootNode', 'content.ini' )))
     $treeclass = ''
	 $thisnode_id=$module_result.node_id
	 $thisnode=fetch(content,node,hash(node_id, $thisnode_id) )
	 $pathar=$thisnode.path_array
	 $loopme=$thisnode.path|reverse
	 $checkme=array()
	 $startnode=array()
	 $startnodetype=''
}  

{if eq($thisnode.data_map.navreset.data_int,1)}
   {set startnode=$thisnode
		startnodetype='resetonthisnode'}
{elseif eq($thisnode_id, $this_root_node.node_id)}
   {set startnode=$this_root_node
		startnodetype='siteroot'}
{else}
{foreach $loopme as $index => $this_step}
   {set checkme=$index}
   {if eq($this_step.data_map.navreset.data_int,1)}	
   	{set checkme=$index|inc}
   	{break}
   {/if}
{/foreach}
{set startnode = fetch(content,node,hash(node_id, $loopme[$checkme|dec].node_id) )}
{if ne($startnode.node_id,$this_root_node.node_id)}
	{set startnodetype = 'resetonparent'}
{else}		
	{set startnodetype = 'siteroot'}
{/if}
{/if}
{if eq($startnodetype,resetonthisnode)}
<h4>{$startnode.name}</h4>
{elseif eq($startnodetype,resetonparent)}
<h4><a href={$startnode.url_alias|ezroot}>{$startnode.name}</a></h4>
{/if}

{def $navMenuItems = fetch(content, list, hash( parent_node_id, $startnode.node_id,
						class_filter_type, 'include',
						class_filter_array, ezini( 'MenuContentSettings', 'LeftIdentifierList', 'menu.ini' ),
						sort_by, $startnode.sort_array,
						'attribute_filter', array('and',array('priority', '>', '0')) )) }
{if gt($navMenuItems|count(), 0)}
<ul id='l_nav'> 
{foreach $navMenuItems as $key => $menu}
    {if ne($menu.node_id,1149)}    
        {set treeclass = or(array('Info Page', 'Article','Link')|contains($menu.object.class_name)|not, and(array('Info Page', 'Article','Link')|contains($menu.object.class_name),$menu.data_map.nav_display.data_int))|choose('hidden', '')}
        <li class="{$treeclass}">   
        {if eq($menu.node_id, $module_result.node_id)} 
	        <span class='noLink'>{$menu.name|wash|shorten(45)}</span>
		{elseif eq($menu.object.class_name,'Left Nav Divider')}
			<span class='divider'>{$menu.name|wash}</span>
		{elseif eq($menu.object.class_identifier, 'link')}
	       <a href='{$menu.data_map.location.content}' title='{$menu.name|wash}' id='{$menu.name|wash}'{if $menu.data_map.new_window.data_int} target="_blank"{/if}>{$menu.name|wash}</a>
        {else}
	        <a {and(gt($module_result.path|count,3),eq($menu.node_id,$module_result.path[3]['node_id'])|choose("","class='noLink'"))} href={$menu.url_alias|ezroot}{if $menu.data_map.new_window.data_int} target="_blank"{/if}>{$menu.name|wash|shorten(45)}</a>                 
        {/if}
{*
{concat($menu.name|wash(), ' | ', $menu.children|count())|debug()}
*}
{* BEGIN FLYOUTS PART TWO *}
{*
{if or($current_user.groups|contains(12392), $current_user.groups|contains(12))}
*}
	{if $menu.children|count}<ol> 
	    {foreach fetch(content, list, hash(parent_node_id, $menu.node_id, class_filter_type, 'include', class_filter_array, ezini( 'MenuContentSettings', 'LeftIdentifierList', 'menu.ini' ), sort_by, $menu.sort_array)) as $key2 => $menu2}
		{set treeclass = or(array('Info Page', 'Article','Link')|contains($menu2.object.class_name)|not, and(array('Info Page', 'Article','Link')|contains($menu2.object.class_name),$menu2.data_map.nav_display.data_int))|choose('hidden', '')}
		<li class="{$treeclass}">
				{if eq($menu2.node_id, $module_result.node_id)}                       
		      <span class='noLink'>{$menu2.name|wash|shorten(45)}</span>
				{elseif eq($menu2.object.class_identifier, 'link')}
			       <a href='{$menu2.data_map.location.content}' title='{$menu2.name|wash}' id='{$menu2.name|wash}'{if $menu2.data_map.new_window.data_int} target="_blank"{/if}>{$menu2.name|wash}</a>
		{else}
		      <a {and(gt($module_result.path|count,3),eq($menu2.node_id,$module_result.path[3]['node_id'])|choose("","class='noLink'"))} href={$menu2.url_alias|ezroot}{if $menu2.data_map.new_window.data_int} target="_blank"{/if}>{$menu2.name|wash|shorten(45)}</a>                 
		{/if}</li>
	    {/foreach}
	</ol>{/if}
{*
{/if}
*}
{* END FLYOUTS PART TWO *}
	 </li>  
    {/if} 
{/foreach}

</ul>
   {if eq($startnodetype,resetonthisnode)}
<div class='returntoparent'>
<a href={$startnode.parent.url_alias|ezroot}>&#x25B2; Return to {$startnode.parent.name}</a>
</div>
   {elseif eq($startnodetype,resetonparent)}
<div class='returntoparent'>
<a href={$startnode.url_alias|ezroot}>&#x25B2; Return to {$startnode.name}</a>
</div>
   {/if}

{/if}
{undef}



           

