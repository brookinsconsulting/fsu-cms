{*?template charset=utf-8?*}

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="{$site.http_equiv.Content-language|wash}" lang="{$site.http_equiv.Content-language|wash}">
{def $site_root_node = ezini( 'NodeSettings', 'RootNode', 'content.ini' ) 
	 $pagedata = pagedata()
	 $site_root_obj = fetch(content, node, hash(node_id, $site_root_node)).object
	 $site_short_name = ezini( 'SiteSettings', 'SiteShortName', 'site.ini' ) 
	 $site_name = ezini( 'SiteSettings', 'SiteName', 'site.ini' ) 
	 $relateds=fetch( 'content', 'related_objects',
			hash( 'object_id', $site_root_obj.id,
	      	'all_relations', 'common' ) )
    $look = 0
	$root_node = fetch(content, node, hash(node_id, $module_result.node_id))
	$use_this_node = cond(is_set($module_result.node_id), $root_node, $site_root_node)}
	
	{foreach $relateds as $this_r}
		{if eq($this_r.class_identifier,'template_look')}
			{set look = $this_r}
			{break}
		{/if}
	{/foreach}

{cache-block keys=array($uri_string, $site_root_node)}

{include uri='design:parts/page_head.tpl'}

{def $path_normalized = ''}

{foreach $module_result.path as $index => $path}
    {if $index|ge($pagerootdepth)}
        {set $path_array = $path_array|append($path)}
    {/if}
    {if is_set($path.node_id)}
        {set $path_normalized = $path_normalized|append( concat('subtree_level_', $index, '_node_id_', $path.node_id, ' ' ))}
    {/if}
{/foreach}

{def $extraclasses = concat('site_',ezini('DesignSettings','SiteDesign','site.ini'),' mod_name_',module_params()['module_name'],' mod_func_',module_params()['function_name'],' mod_view_',module_params()['parameters']['ViewMode'])}

<body class="{$path_normalized|trim} {$extraclasses} class_{fetch(content, node, hash(node_id, $module_result.node_id)).object.class_identifier} node_{$module_result.node_id}">
{if $current_user.is_logged_in}
{literal}
<script type="text/javascript">
if (typeof Meebo == 'undefined') {
    var M=Meebo=function(){(M._=M._||[]).push(arguments)},w=window,a='addEventListener',b='attachEvent',c='load',
    d=function(){M.T(c);M(c)},z=M.$={0:+new Date};
    M.T=function(a){z[a]=+new Date-z[0]};if(w[a]){w[a](c,d,false)}else{w[b]('on'+c,d)};M.v=3;
    (function(_){var d=document,b=d.body,c;if(!b){c=arguments.callee;
    return setTimeout(function(){c(_)},100)}var a='appendChild',c='createElement',
    m=b.insertBefore(d[c]('div'),b.firstChild),n=m[a](d[c]('m')),i=d[c]('iframe');
    m.style.display='none';m.id='meebo';i.frameBorder="0";n[a](i).id="meebo-iframe";
    function s(){return['<body onload=\'var d=document;d.getElementsByTagName("head")[0].',
    a,'(d.',c,'("script")).sr','c="//',_.stage?'stage-':'',
    'cim.meebo.com','/cim?iv=',M.v,'&network=',_.network,_.lang?'&lang='+_.lang:'',
    _.d?'&domain='+_.d:'','"\'></bo','dy>'].join('')}try{
    d=i.contentWindow.document.open();d.write(s());d.close()}catch(e){
    _.d=d.domain;i['sr'+'c']='javascript:d=document.open();d.write("'+s().replace(/"/g,'\\"')+'");d.close();'}M.T(1)})
    ({ network: 'fsucom_jo57ho', stage: false });
    Meebo("makeEverythingSharable");    
}
</script>
{/literal}
{/if}
<div class="accessibleText">Screenreader Navigation - [ <a href="#tof">Skip to Content</a>&nbsp;|&nbsp;<a href="#mainNav">Skip to Main Navigation</a> ]<hr /></div>

<div class="header">{if ne($module_result.node_id,$site_root_node)}<a href='/'>{/if}<img src={'fsu_logo.png'|ezimage} alt='The Florida State University - Return to FSU.com Home Page' />{if ne($module_result.node_id,$site_root_node)}</a>{/if}
	{include uri='design:parts/search_box.tpl'}
	{*{if $current_user.is_logged_in}*}
	<div class="facebook_link" style="position:relative;top:135px;left:290px">
		<a style="background:url('/design/fsucom/images/share_facebook.jpg') no-repeat right;padding-right:20px" href="http://www.facebook.com/floridastate">Visit us on Facebook</a>
	</div>
	{*{/if}*}
</div>

<div class="accessibleText"><a id="tof"></a></div>

{/cache-block}

<div id="mainbox">
<div class="bofCentered">
  <div class="bofContainer">
	<div class="bofCentered">
		<div class="bof" id="col_wrapper">
		{cache-block keys=array( $uri_string, $current_user.contentobject_id, $site_root_node )}
		  {if and( is_set($module_result.node_id), $current_user.is_logged_in, is_set( $module_result.content_info.viewmode ), ne( $module_result.content_info.viewmode, 'sitemap' ) ) }
		  <!-- Toolbar area: START -->
		  <div id="toolbar">
		  {include uri='design:parts/website_toolbar.tpl'}
		  </div>
		  <!-- Toolbar area: END -->
		  {/if}
		{/cache-block}
		{cache-block keys=array( $uri_string, $current_user.contentobject_id, $site_root_node )}

			{def $my_page_notes=fetch('content', 'list', hash( parent_node_id, $use_this_node.node_id, 'class_filter_type', 'include', 'class_filter_array', array('infobox'), 'sort_by', $use_this_node.sort_array ))
			     $r_col_extra_class = 'no_page_notes'
			     $m_col_extra_class = 'no_back'}
{set $my_page_notes=''}
			{if gt($my_page_notes|count, 0)}
			    {*set r_col_extra_class = ''
				   m_col_extra_class = ''*}
			{/if}

			<div id='full_width_float'>

			<div class="bofMiddleColumn {$m_col_extra_class}">

			{if gt($my_page_notes|count, 0)}

				<div class="bofRightColumn">
				<div class="accessibleText"><hr /></div>
				<div class="rightLinks" style="clear:both;">



			   {include uri='design:parts/extra_info.tpl' infoboxes=$my_page_notes}


				</div>
				</div>
			{/if}

			  <div class="bofMiddleText {$r_col_extra_class}">
				

<div id='maincontent_design'>


{include uri='design:parts/path.tpl'}

{/cache-block}
{$module_result.content}
{cache-block keys=array( $uri_string, $current_user.contentobject_id, $site_root_node )}


				
{if and(is_set($pagedata['mimes']), ne($pagedata['mimes'],''))}{include uri='design:parts/reader_links.tpl' readers=$pagedata['mimes']|implode("|")}{/if}



</div><!-- id='maincontent_design' -->
			  </div>
			</div>
		</div>

   <div class="bofLeftColumn">
	{include uri='design:parts/navmenu.tpl'}
{def $siteInfoboxes = fetch( 'content', 'list', hash('parent_node_id', $site_root_node,
						     'sort_by', $use_this_node.sort_array,
						     'class_filter_type', 'include',
						     'class_filter_array', array('infobox')) )}
{foreach $siteInfoboxes as $infobox}
	{node_view_gui content_node=$infobox view='infobox'}
{/foreach}

   </div>

</div>
</div>
</div>
</div>
</div>
<div id='thread'>
<div id='thread_left'></div>
<marquee behavior="scroll" direction="left" scrollamount="1" loop='100'>
<p id='threadtext'>Strength Skill Character Strength Skill Character Strength Skill Character Strength Skill Character Strength Skill Character Strength Skill Character Strength Skill Character Strength Skill Character Strength Skill Character Strength Skill Character Strength Skill Character Strength Skill Character Strength Skill Character Strength Skill Character Strength Skill Character Strength Skill Character Strength Skill Character Strength Skill Character Strength Skill Character Strength Skill Character Strength Skill Character Strength Skill Character Strength Skill Character Strength Skill Character Strength Skill Character Strength Skill Character </p></marquee>
<div id='thread_right'></div>
</div>
{include uri='design:parts/footer.tpl'}

{/cache-block}
{literal}
<script type="text/javascript">
		var gaJsHost = (("https:" == document.location.protocol) ? "https://ssl." : "http://www.");
		document.write(unescape("%3Cscript src='" + gaJsHost + "google-analytics.com/ga.js' type='text/javascript'%3E%3C/script%3E"));
	</script>
	<script type="text/javascript">
		var pageTracker = _gat._getTracker("UA-5509735-1");
		pageTracker._trackPageview();
	</script>
	<script src="http://static.getclicky.com/js" type="text/javascript"></script>
	<script type="text/javascript">clicky.init(148060);</script>
	<noscript><p><img alt="Clicky" width="1" height="1" src="http://static.getclicky.com/148060ns.gif" /></p></noscript>
	{/literal}
{if $current_user.is_logged_in}
	{literal}
	<script type="text/javascript">
	  Meebo("domReady");
	</script>
{/literal}
{/if}
</body>
</html>

{undef}

