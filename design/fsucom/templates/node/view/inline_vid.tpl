{def $vid_image = ''
     $vid_href=$node.url_alias|ezroot(no)
     $layoutSet=cond($wide_view, 'popup', 'empty')
     $wideViewParam=cond($wide_view, '/(wide)/true', '')}
{*$vid_image = 'vid_placeholder.jpg'|ezimage(no)*}
<div class="content-view-inline-vid">
    <div class="class-video">

{def $vidURL = concat("content/download/",$node.contentobject_id,"/",$node.content.contentobject_attribute_id,"/",$node.content.original_filename)|ezurl('no')}
{*
saveAdHTML('flash_box'); 
showAdHTML('flash_box');
*}
{if and(is_set($node.data_map.still_frame) , $node.data_map.still_frame.content.is_valid)}
    {set vid_image = $node.data_map.still_frame.content['still_frame'].url|ezroot(no)}
<a href="javascript: basicajax_get_html('/layout/set/empty/content/view/full/{$node.node_id}{$wideViewParam}', 'expertvideos', function(){ldelim} if (typeof loadVideo !='undefined') loadVideo('{$vidURL}'); if (typeof domtab !='undefined') domtab.init.call(){rdelim});"><img src='{$vid_image}' alt="{$node.name|wash}" /></a>
{else}
	{if 1|eq(0)}
		<a href="javascript: basicajax_get_html('/layout/set/empty/content/view/full/{$node.node_id}{$wideViewParam}', 'expertvideos', function(){ldelim} if (typeof loadVideo !='undefined') loadVideo('{$vidURL}'); if (typeof domtab !='undefined') domtab.init.call(){rdelim});"><img src='http://video.thinkcreative.com/screenshots/{$node.data_map.vid.content}?width=150' width=150 alt="{$node.name|wash}" /></a>
	{else}
	
		{def $vid_id = $node.data_map.video.content
			 $player = ezgetvars()['player']
		}

		{if or(is_set($player)|not, $player|eq(''))}
			{set $player = ezini( 'BOTRSettings', 'DefaultPlayer', 'botr.ini' )}
		{/if}
	
		{set $vidURL = concat('/extension/multimedia/design/standard/javascript/bits_modified.php?keys=', $vid_id, '-', $player)}
		<a href="javascript: basicajax_get_html('/layout/set/empty/content/view/full/{$node.node_id}{$wideViewParam}', 'expertvideos', function(){ldelim} if (typeof dhtmlLoadScript !='undefined') dhtmlLoadScript('{$vidURL}'); if (typeof domtab !='undefined') domtab.init.call(){rdelim});"><img src='http://cdn.thinkcreative.com/thumbs/{$node.data_map.video.content}-150.jpg' width=150 alt="{$node.name|wash}" /></a>
	{/if}
{/if}

<div class='vid-caption'><a href="javascript: basicajax_get_html('/layout/set/empty/content/view/full/{$node.node_id}{$wideViewParam}', 'expertvideos', function(){ldelim} if (typeof dhtmlLoadScript !='undefined') dhtmlLoadScript('{$vidURL}'); if (typeof domtab !='undefined') domtab.init.call(){rdelim});">{$node.name|wash()|html_shorten(40,'')}</a></div>


    </div>
</div>

