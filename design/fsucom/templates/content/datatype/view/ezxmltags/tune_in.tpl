{* Tune In - Custom Tag *}
{def $tabArray = ''
     $tabNodes = hash()
     $sortArray = false()}

{foreach $source|explode(',') as $tabData}
   {set $tabArray = $tabData|explode(':')
        $tabLimit = first_set($tabArray[2], 3)
	$sortArray = cond( eq($tabArray[1],10550), array('attribute',false(),344), fetch('content','node',hash('node_id',$tabArray[1])).sort_array )
        $tabNodes = $tabNodes|merge( hash($tabArray[0], fetch( 'content','tree', hash('parent_node_id', $tabArray[1],
											'sort_by', $sortArray,
											'limit', 3,
											'class_filter_type', 'exclude',
											'class_filter_array', array('folder','image','link')) )) )}
{/foreach}

<div class="tune-in" id='tabs'>
   <h1>{$header|wash()}</h1>
   <ul>
{foreach $tabNodes as $key => $tab}
     <li><a href='#tab-{$key|md5()}'>{$key|wash()}</a></li>
{/foreach}
   </ul>

{foreach $tabNodes as $key => $tabContent}
   <div id="tab-{$key|md5()}" class='tabdiv'>
      {foreach $tabContent as $contentNode}
	{node_view_gui content_node=$contentNode view='line'}
      {/foreach}
   </div>
{/foreach}
</div>