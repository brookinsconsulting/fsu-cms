{* Events Home Include *}

        {let todaystart=maketime( '00','00', '00', currentdate()|datetime( custom, '%n'), currentdate()|datetime( custom, '%j'), currentdate()|datetime( custom, '%Y') )
		theseevents=fetch( 'content', 'tree', hash('parent_node_id', 6080, limit, 8,
			'sort_by', array('attribute', true(), 'event/title' ), 'sort_by', array('attribute', true(), 'event/from_time' ),
	 		'attribute_filter',
			array('and', array('event/from_time', 'not_between', array(1,sub($todaystart,1))), array('event/to_time', 'not_between', array(1,sub($todaystart,1))) ) ))}
		{section show=gt($theseevents|count,0)}

		   <div id='events'>

		   <h2><a href='#'>Events</a></h2>

               <div class="content-view-children">
	        {def $new_date=true()}
                    {section var=child loop=$theseevents last-value}
				{if $child.index}{set new_date=and(ne($child.data_map.from_time.data_int, $child.last.data_map.from_time.data_int),gt($child.data_map.from_time.data_int,$todaystart))|choose(false(),true())}{/if}
				<div class='event {if $new_date|not}same{/if}'>
                     	{content_view_gui view=event_home_view content_object=$child.object new_date=$new_date}
				</div>
                    {/section}
		 {undef}
               </div>
			

		   </div>
		   <hr />
{*		   <div class='maincallink'><a href='/main_tcc_calendar'>View Full Calendar</a></div>
		   <div class='maincallink'><a href='/tcc/calendar/(month)/{currentdate()|datetime( 'custom', '%n' )}/(day)/{currentdate()|datetime( 'custom', '%j' )}/(year)/{currentdate()|datetime( 'custom', '%Y' )}/(view)/Day'>Around Campus Today</a></div> 
*}
		{/section}

	{/let}
