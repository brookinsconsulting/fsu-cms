  <!-- Path content: START -->
  <p id="path">
  {foreach $module_result.path as $path offset 1}

  {run-once}
	<a href='http://www.fsu.edu'>Florida State</a>&nbsp;/&nbsp;
  {/run-once}

  {if $path.url}
    <a href={cond( is_set( $path.url_alias ), $path.url_alias,$path.url )|ezroot}>{$path.text|wash}</a>
  {else}
    {$path.text|wash}
  {/if}
  {delimiter}/{/delimiter}
  {/foreach}
  </p>
  <!-- Path content: END -->