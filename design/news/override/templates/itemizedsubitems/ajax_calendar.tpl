{let todaystart=maketime( '00','00', '00', currentdate()|datetime( custom, '%n'), currentdate()|datetime( custom, '%j'), currentdate()|datetime( custom, '%Y') )
     new_date=true()}
<div class="content-view-embed">
	<div class="class-ajax-calendar">
	{def $children = array()
	     $limit = 5
	     $offset = 0}

	{if is_set( $object_parameters.limit )}
		{set $limit = $object_parameters.limit}
	{/if}

	{if is_set( $object_parameters.offset )}
		{set $offset = $object_parameters.offset}
	{/if}

    {set $children=fetch( content, list, hash( 'parent_node_id', $object.main_node_id, 
											   'limit', $limit,
											   'offset', $offset,
											   'class_filter_type', 'include',
											   'class_filter_array', array( 'ajax_event' ),
											   'sort_by', array( 'attribute', true(), 'ajax_event/from_time' ),
											   'attribute_filter',
											   array('and', array('ajax_event/from_time', 'not_between', array(1,sub($todaystart,1))), array('ajax_event/to_time', 'not_between', array(1,sub($todaystart,1)))) ) ) }
{*    <h2><a href={$object.main_node.url_alias|ezurl}>Upcoming Events</a></h2>*}
	
	{if $children|count()}
    
    <ul>
    {section var=child loop=$children last-value}
				<li><div class='event {if $new_date|not}same{/if}'>
				{if $child.index}{set new_date=and(ne($child.data_map.from_time.data_int, $child.last.data_map.from_time.data_int),gt($child.data_map.from_time.data_int,$todaystart))|choose(false(),true())}{/if}
                     	{content_view_gui view=event_home_view content_object=$child.object new_date=$new_date}
				</div></li>
    {/section}
    </ul>
	{else}
	<div class='noevents'>There are no upcoming events at this time. Please check back later.</div>	
	{/if}
	</div>
</div>
