{def $data_location = $node.data_map.flash_data_location.content.main_node
     $class_filter = array($node.data_map.flash_data_class.content)
     $targets = fetch(content, list, hash(parent_node_id, $data_location.node_id, sort_by, $data_location.sort_array, class_filter_type, 'include', class_filter_array, $class_filter))}
<news>
	<data>
{foreach $targets as $this_target}
	{if $this_target.data_map.flash_image.content.is_valid}
	 <pic>
		<image><![CDATA[{$this_target.data_map.flash_image.content.supergraphic.url|ezroot('no')}]]></image>
		<caption><![CDATA[<p class='flashheadline'>{$this_target.data_map.flash_head.content}</p><br/><p class='flashsubtitle'>{$this_target.data_map.flash_sub_head.content}</p>]]></caption>
		<link><![CDATA[{fetch('content', 'node', hash('node_id', $this_target.main_node_id)).url_alias|ezroot('no')}]]></link> 
	 </pic>
	 {/if}
{/foreach}
	</data>
</news>
{undef}

