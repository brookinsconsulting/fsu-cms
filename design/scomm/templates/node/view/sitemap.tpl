{set-block scope=root variable=cache_ttl}0{/set-block}

{let page_limit=16
     col_count=2
     children=fetch('content','list',hash(parent_node_id,$node.node_id,limit,$page_limit,offset,$view_parameters.offset, 'class_filter_type', 'exclude', 'class_filter_array', array('event', 'faq_question', 'alert', 'footer', 'timeline_entry', 'weblog', 'promo')))
     child_count=fetch('content','list_count',hash(parent_node_id,$node.node_id))}

<div class="maincontentheader">
<h1>Site Map</h1>
</div>

{if $withwarning}
<h2>The page you were searching for could not be located. Please use the resources below to find the content you need.<br/></h2>
{/if}

{* {include uri="design:dynrelationbrowse.tpl" browse=hash('parent_node_id',2 , 'class_filter_type', 'exclude', 'class_filter_array', array(38,39) )} *}

{include uri="design:dynamic_search/dynamic_search_main.tpl}

<table id="sitemap">
<tr>
{section name=Child loop=$children}
    <td>
    <h2><a href={$Child:item.url_alias|ezroot}>{$Child:item.name}</a></h2>

    {let sub_children=fetch('content','list',hash(parent_node_id,$Child:item.node_id,limit,$page_limit, 'class_filter_type', 'exclude', 'class_filter_array', array('event', 'faq_question', 'alert', 'footer', 'timeline_entry', 'weblog', 'promo')))
         sub_child_count=fetch('content','list_count',hash(parent_node_id,$Child:item.node_id))}

    <ul>
    {section name=SubChild loop=$:sub_children}

	{if $:item.data_map.location.has_content}
   	<li><a href={$:item.data_map.location.content|ezurl}>{$:item.name|wash}</a></li>
	{else}
   	<li><a href='{$:item.object.main_node.url_alias|ezroot(no)}'>{$:item.name|wash}</a></li>
	{/if}

    {/section}
    </ul>

    {/let}

    </td>
    {delimiter modulo=$col_count}
</tr>
<tr>
    {/delimiter}
{/section}
</tr>
</table>

            {include name=navigator
                     uri='design:navigator/google.tpl'
                     page_uri=$node.url_alias
                     item_count=$list_count
                     view_parameters=$view_parameters
                     item_limit=$page_limit}

{/let}