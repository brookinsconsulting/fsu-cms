{*?template charset=utf-8?*}

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="{$site.http_equiv.Content-language|wash}" lang="{$site.http_equiv.Content-language|wash}">
{cache-block keys=$uri_string}
{def $root_node = fetch(content, node, hash(node_id, $module_result.node_id))}

{include uri='design:parts/page_head.tpl'}

{def $path_normalized = ''}

{foreach $module_result.path as $index => $path}
    {if $index|ge($pagerootdepth)}
        {set $path_array = $path_array|append($path)}
    {/if}
    {if is_set($path.node_id)}
        {set $path_normalized = $path_normalized|append( concat('subtree_level_', $index, '_node_id_', $path.node_id, ' ' ))}
    {/if}
{/foreach}

<body class="{$path_normalized|trim}">

<div class="accessibleText">Screenreader Navigation - [<a href="#tof">Skip to Content</a>
 
	&nbsp;|&nbsp;<a href="#mainNav">Skip to Main Navigation</a>
 
	]<hr />
</div>
<div class="headerContainer">
	<div class="header">

		<div class="headerFSUSeal"><a href="http://fsu.edu"><img src={"headerFSUSeal.gif"|ezimage} width="70" height="74" alt="[FSU Seal Image]" /></a></div>
		<div class="headerFSUTitle"><a href="http://fsu.edu"><img src={"headerFSUWordmark.gif"|ezimage} width="317" height="55" alt="Florida State University" /></a></div>


{include uri='design:parts/search_box.tpl'}
		
	</div>
	
</div>

<div class="accessibleText"><hr /></div>
	

{include uri='design:parts/quicklinks.tpl'}
	

<div class="accessibleText"><a id="tof"></a></div>

{/cache-block}

{cache-block keys=array( $uri_string, $current_user.contentobject_id )}
  {if and( is_set($module_result.node_id), $current_user.is_logged_in, is_set( $module_result.content_info.viewmode ), ne( $module_result.content_info.viewmode, 'sitemap' ) ) }
  <!-- Toolbar area: START -->
  <div id="toolbar">
  {include uri='design:parts/website_toolbar.tpl'}
  </div>
  <!-- Toolbar area: END -->
  {/if}
{/cache-block}

<div class="tofContainer">

{include uri='design:parts/supergraphic.tpl'}

</div>

{cache-block keys=array( $uri_string, $current_user.contentobject_id )}
<div id="mainbox" class="bofContainer">
<div class="bofCentered">
  <div class="bofContainer">
	<div class="bofCentered">
		<div class="bof" id="col_wrapper">

			{def $my_page_notes=fetch('content', 'list', hash('parent_node_id', is_set($module_result.node_id)|choose(2258, $module_result.node_id ), 'class_filter_type', 'include', 'class_filter_array', array('infobox'), 'sort_by' fetch(content, node, hash(node_id, is_set($module_result.node_id)|choose(2258, $module_result.node_id ))).sort_array ))
			     $r_col_extra_class = 'no_page_notes'
			     $m_col_extra_class = 'no_back'}

			{if gt($my_page_notes|count, 0)}
			    {set r_col_extra_class = ''
				   m_col_extra_class = ''}
			{/if}

			<div id='full_width_float'>

			<div class="bofMiddleColumn {$m_col_extra_class}">

			{if gt($my_page_notes|count, 0)}

				<div class="bofRightColumn">
				<div class="accessibleText"><hr /></div>
				<div class="rightLinks" style="clear:both;">



			   {include uri='design:parts/extra_info.tpl' infoboxes=$my_page_notes}


				</div>
				</div>
			{/if}

			  <div class="bofMiddleText {$r_col_extra_class}">

<div id='maincontent_design'>


{include uri='design:parts/path.tpl'}

{/cache-block}
{$module_result.content}
{cache-block keys=array( $uri_string, $current_user.contentobject_id )}


</div><!-- id='maincontent_design' -->


			  </div>

			</div>

		</div>
			
		<div class="bofLeftColumn">

<a href='/'>Return to Homepage</a> 
{include uri='design:parts/navmenu.tpl'}
<ul>
<a href="http://one.fsu.edu/community/page.aspx?pid=845" style="padding-top:10px; padding-bottom:0px; text-align:center;"><img src={"donate.png"|ezimage} alt="Donate" /></a></ul>

		</div>
		</div>





	</div>

</div>
</div>
</div>

<div class="footerContainer">

{include uri='design:parts/footer.tpl'}

</div>



{/cache-block}
</body>
</html>

{undef}

