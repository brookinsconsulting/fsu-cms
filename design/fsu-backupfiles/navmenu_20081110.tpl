{def $this_root_node = fetch(content, node, hash(node_id, ezini( 'NodeSettings', 'RootNode', 'content.ini' )))
     $classes = array( 'folder', 'article', 'info_page', 'forum', 'frontpage', 'event_calendar', 'faq_list', 'gallery', 'weblog','link')
     $treeclass = ''
}  

  
<ul id='l_nav'> 

{foreach fetch(content, list, hash(parent_node_id, $this_root_node.node_id, class_filter_type, 'include', class_filter_array, $classes, sort_by, $this_root_node.sort_array)) as $key => $menu}
    {if ne($menu.node_id,1149)}    
        {set treeclass = or(array('Info Page', 'Article','Link')|contains($menu.object.class_name)|not, and(array('Info Page', 'Article','Link')|contains($menu.object.class_name),$menu.data_map.nav_display.data_int))|choose('hidden', '')}
        <li class="{$treeclass}">   
        {if eq($menu.node_id, $module_result.node_id)}                       
	        <span class='noLink'>{$menu.name|wash|shorten(45)}</span>
		{elseif eq($menu.object.class_identifier, 'link')}
	       <a href='{$menu.data_map.location.content}' title='{$menu.name|wash}' id='{$menu.name|wash}'>{$menu.name|wash}</a>
        {else}
	        <a {and(gt($module_result.path|count,3),eq($menu.node_id,$module_result.path[3]['node_id'])|choose("","class='noLink'"))} href={$menu.url_alias|ezroot}>{$menu.name|wash|shorten(45)}</a>                 
        {/if}  
        {if $menu.children|count}<ol> 
            {foreach fetch(content, list, hash(parent_node_id, $menu.node_id, class_filter_type, 'include', class_filter_array, $classes, sort_by, $menu.sort_array)) as $key2 => $menu2}
                {set treeclass = or(array('Info Page', 'Article','Link')|contains($menu2.object.class_name)|not, and(array('Info Page', 'Article','Link')|contains($menu2.object.class_name),$menu2.data_map.nav_display.data_int))|choose('hidden', '')}
                <li class="{$treeclass}">
				{if eq($menu2.node_id, $module_result.node_id)}                       
	              <span class='noLink'>{$menu2.name|wash|shorten(45)}</span>
				{elseif eq($menu2.object.class_identifier, 'link')}
			       <a href='{$menu2.data_map.location.content}' title='{$menu2.name|wash}' id='{$menu2.name|wash}'>{$menu2.name|wash}</a>
                {else}
	              <a {and(gt($module_result.path|count,3),eq($menu2.node_id,$module_result.path[3]['node_id'])|choose("","class='noLink'"))} href={$menu2.url_alias|ezroot}>{$menu2.name|wash|shorten(45)}</a>                 
                {/if}</li>
            {/foreach}
        </ol>{/if} 
	 </li>  
    {/if} 
{/foreach}

</ul>

{undef}



           

