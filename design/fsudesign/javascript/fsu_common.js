 function EventWorker() {
   this.addHandler = EventWorker.addHandler;
 }


 EventWorker.addHandler = function (eventRef, func) {
  var eventHandlers = eval(eventRef);
  if (typeof eventHandlers == 'function') {
   eval(eventRef + " = function(event) {eventHandlers(event); func(event);}");  
  } else {
   eval(eventRef + " = func;");
  }
 }

 function getElementsByClass(node,searchClass,tag) {
   var classElements = new Array();
   var els = node.getElementsByTagName(tag); // use "*" for all elements
   var elsLen = els.length;
   var pattern = new RegExp("\\b"+searchClass+"\\b");
   for (i = 0, j = 0; i < elsLen; i++) {
     if ( pattern.test(els[i].className) ) {
       classElements[j] = els[i];
       j++;
     }
   }
   return classElements;
 }
