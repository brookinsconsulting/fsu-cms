{let kids = fetch(content, list, hash(parent_node_id, $drill_from.node_id, sort_by, $drill_from.sort_array, class_filter_type, 'include', class_filter_array, array('folder','user')))}
<ul>
{section var=this_kid loop=$kids}
	{if eq($this_kid.object.class_identifier, 'user')}
	<li>{node_view_gui content_node=$this_kid view='line'}</li>
	{else}
	<h2>{$this_kid.name|wash}</h2>
	<li>{include name=SubMenu uri='design:node/view/user_drill.tpl' drill_from=$this_kid}</li>
	{/if}
{/section}
</ul>
{/let}


