{*?template charset=utf-8?*}

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="{$site.http_equiv.Content-language|wash}" lang="{$site.http_equiv.Content-language|wash}">
{def $site_root_node = ezini( 'NodeSettings', 'RootNode', 'content.ini' ) 
	 $site_short_name = ezini( 'SiteSettings', 'SiteShortName', 'site.ini' ) 
	 $site_name = ezini( 'SiteSettings', 'SiteName', 'site.ini' ) 
	 $root_node = fetch(content, node, hash(node_id, $module_result.node_id))
	 $use_this_node = cond(is_set($module_result.node_id), $root_node, $site_root_node)
	 $content_info = $module_result.content_info}
	

{cache-block keys=array($uri_string, $current_user.contentobject_id, $site_root_node)}
{def $root_node = fetch(content, node, hash(node_id, $module_result.node_id))}

{include uri='design:parts/page_head.tpl'}

{def $path_normalized = ''}

{foreach $module_result.path as $index => $path}
    {if $index|ge($pagerootdepth)}
        {set $path_array = $path_array|append($path)}
    {/if}
    {if is_set($path.node_id)}
        {set $path_normalized = $path_normalized|append( concat('subtree_level_', $index, '_node_id_', $path.node_id, ' ' ))}
    {/if}
{/foreach}

{def $extraclasses = concat('site_',ezini('DesignSettings','SiteDesign','site.ini'),' mod_name_',module_params()['module_name'],' mod_func_',module_params()['function_name'],' mod_view_',module_params()['parameters']['ViewMode'])
	 $pagestyle = ''}

{if and( is_set( $content_info.class_identifier ), ezini( 'MenuSettings', 'HideLeftMenuClasses', 'menu.ini' )|contains( $content_info.class_identifier ) )}
    {set $pagestyle = 'nosidemenu'}
{/if}
<body class="{$path_normalized|trim} {$extraclasses} class_{fetch(content, node, hash(node_id, $module_result.node_id)).object.class_identifier} node_{$module_result.node_id} {$pagestyle}">

<div class="accessibleText">Screenreader Navigation - [ <a href="#tof">Skip to Content</a>&nbsp;|&nbsp;<a href="#mainNav">Skip to Main Navigation</a> ]<hr /></div>

<div class="headerContainer">
	<div class="header">

		<div class="headerFSUSeal"><a href="http://fsu.edu"><img src={"headerFSUSeal.png"|ezimage} width="70" height="74" alt="[FSU Seal Image] - Return to Home" /></a></div>
		<div class="headerFSUTitle"><a href="http://fsu.edu"><img src={"headerFSUWordmark.png"|ezimage} width="360" height="55" alt="Florida State University - Return to Home" /></a></div>


{include uri='design:parts/search_box.tpl'}
		
	</div>
	
</div>

<div class="accessibleText"><hr /></div>
	

{include uri='design:parts/quicklinks.tpl'}
	
{/cache-block}

<div class="accessibleText"><a id="tof"></a></div>

{cache-block keys=array( $uri_string, $current_user.contentobject_id, $site_root_node )}
<!-- Toolbar area: START -->

    {include uri='design:parts/website_toolbar.tpl' current_node_id=$module_result.node_id}

<!-- Toolbar area: END -->
{/cache-block}

<div class="tofContainer">

{include uri='design:parts/supergraphic.tpl'}

</div>

{cache-block keys=array( $uri_string, $current_user.contentobject_id, $site_root_node )}
<div id="mainbox" class="bofContainer">
<div class="bofCentered">
  <div class="bofContainer">
	<div class="bofCentered">
		<div class="bof" id="col_wrapper">

			{def $my_page_notes=fetch('content', 'list', hash( parent_node_id, $use_this_node.node_id, 'class_filter_type', 'include', 'class_filter_array', array('infobox'), 'sort_by', $use_this_node.sort_array ))			     
				 $r_col_extra_class = 'no_page_notes'
			     $m_col_extra_class = 'no_back'}

			{if gt($my_page_notes|count, 0)}
			    {set r_col_extra_class = ''
				   m_col_extra_class = ''}
			{/if}

			<div id='full_width_float'>
		<div class="bofMiddleColumn {$m_col_extra_class}">
			{if gt($my_page_notes|count, 0)}
				<div class="bofRightColumn">
				<div class="accessibleText"><hr /></div>
				<div class="rightLinks" style="clear:both;">
			   {include uri='design:parts/extra_info.tpl' infoboxes=$my_page_notes}
				</div>
				</div>
			{/if}
			  <div class="bofMiddleText {$r_col_extra_class}">
			<div id='maincontent_design'>
			{include uri='design:parts/path.tpl'}
			{/cache-block}
			{$module_result.content}
			{cache-block keys=array( $uri_string, $current_user.contentobject_id, $site_root_node )}	
			{def $pagedata = pagedata()}		
			{$pagedata|debug('pl')}	
			{if and(is_set($pagedata["persistent_variable"]['mimes']), ne($pagedata["persistent_variable"]['mimes'],''))}{include uri='design:parts/reader_links.tpl' readers=$pagedata["persistent_variable"]['mimes']|implode("|")}{/if}
			</div><!-- id='maincontent_design' -->
	  	</div>
		</div>
		</div>
		<div class="bofLeftColumn">
			
			{def $site_root_obj = fetch(content, node, hash(node_id, $site_root_node)).object
				 $relateds=fetch( 'content', 'related_objects',
	 	 						  hash( 'object_id', $site_root_obj.id,
		      					  'all_relations', 'common' ) )
	     		 $look = 0}
			
			{foreach $relateds as $this_r}
				{if eq($this_r.class_identifier,'template_look')}
					{set look = $this_r}
					{break}
				{/if}
			{/foreach}
			
	{if ne($site_root_node, $module_result.node_id)}<a href='/'>{else}<div id='navlogo'>{/if}{if $look.data_map.image.content.is_valid}{attribute_view_gui attribute=$look.data_map.image image_class='original'}{else}{if ne($site_root_node, $module_result.node_id)}Home{/if}{/if}{if ne($site_root_node, $module_result.node_id)}</a>{else}</div>{/if}
	{include uri='design:parts/navmenu.tpl'}
		</div>
		</div>
	</div>
</div>
</div>
</div>

<div class="footerContainer">

{include uri='design:parts/footer.tpl'}

</div>
{/cache-block}
</body>
</html>

{undef}

