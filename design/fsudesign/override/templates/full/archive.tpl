{* Archive Page - Full view *}

{def $output = $node.data_map.html.data_text|htmlspecialchars_decode
	 $out_r = $output|explode('../../../../media')
	 $out_final = $out_r|implode('http://fsu.com/media')}

<div class="content-view-full">
    <div class="class-archive">

		<div class="attribute-header">
        	<h1>{attribute_view_gui attribute=$node.data_map.name}</h1>
		</div>

        {if $node.object.data_map.html.data_text}
            <div class="attribute-long">
                {$out_final}
            </div>
        {/if} 
<div class='clearfix'></div>
</div>
</div>
