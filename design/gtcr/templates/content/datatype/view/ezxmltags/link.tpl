{if $href|begins_with('content/view/')}

{def $obid = $href|explode('/')|reverse[0]
    $object = fetch(content, node, hash(node_id, $obid))
	$attribute = first_set($object.data_map.file, 0)
	$withmime = false()}

{else}

{def $object=$href|get_link_object
     $attribute=first_set($object.data_map.file, 0)
	 $withmime = true()}

{/if}

{if and($object, is_set($object.data_map.file) )}
{pagedata_set("mimes", $attribute.content.mime_type, true())}

<a href={concat( 'content/download/', $attribute.contentobject_id, '/', $attribute.id)|ezroot}>{$content}</a>{if $withmime}&nbsp;({$attribute.content.mime_type_part} {$attribute.content.filesize|si( byte )}){/if}

{else}
<a href="{or($href|begins_with('http'), $href|begins_with('mailto'), $href|begins_with('#'))|choose($href|tclink(no),$href)}"{section show=$id} id="{$id}"{/section}{section show=$title} title="{$title}"{/section}{section show=$target} target="{$target}"{/section}{section show=ne($classification|trim,'')} class="{$classification|wash}"{/section}>{$content}</a>
{/if}
{undef}