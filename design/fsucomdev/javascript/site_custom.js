var current_super = 1
var max_super = 3
var super_timer
var myAnim_old
var myAnim_new

sifr_stuff=function(){}

function getElementsByClass(node,searchClass,tag) {
  var classElements = new Array();
  var els = node.getElementsByTagName(tag); // use "*" for all elements
  var elsLen = els.length;
  var pattern = new RegExp("\\b"+searchClass+"\\b");
  for (i = 0, j = 0; i < elsLen; i++) {
    if ( pattern.test(els[i].className) ) {
      classElements[j] = els[i];
      j++;
    }
  }
  return classElements;
}

function super_rotator() {
       current_super=current_super+1;
	if (current_super > max_super) current_super = 1; 
	show_slide(current_super);
}

function show_slide(i) {
   current_slide = getElementsByClass(document, 'super-slide selected', 'IMG')[0];
   new_slide = document.getElementById('slide_'+i+'_super_img');
   if (new_slide != current_slide) {
   if (typeof myAnim_old == 'object') myAnim_old.stop; 
   if (typeof myAnim_new == 'object') myAnim_new.stop; 
   myAnim_old = new YAHOO.util.Anim(current_slide, { opacity: { to: 0 } }, .5, YAHOO.util.Easing.easeOut);
   myAnim_new = new YAHOO.util.Anim(new_slide, { opacity: { to: 1 } }, .5, YAHOO.util.Easing.easeOut);
   myAnim_new.onComplete.subscribe(change_slide_class);
   myAnim_new.animate();
   myAnim_old.animate();  	
   }
}

function change_slide_class() {
   current_slide = getElementsByClass(document, 'super-slide selected', 'IMG')[0];
   new_slide = this.getEl();
   current_slide.className = current_slide.className.replace(/ selected/, " not-selected");
   new_slide.className = new_slide.className.replace(/ not-selected/, " selected");  
}

function hideleft(obj) {
   var myAnim = new YAHOO.util.Anim(obj, { height: { to: 0 } }, .4, YAHOO.util.Easing.easeOut);
   myAnim.onComplete.subscribe(function(){ obj=this.getEl();obj.style.height = 'auto';obj.style.left = '-5000px';obj.style.position = 'absolute';});
   myAnim.animate(); 
}

function antihideleft(obj) {
   var start_height = obj.offsetHeight;
   obj.style.height = '0px';
   obj.style.overflow = 'hidden';
   obj.style.left = '0px';
   obj.style.display = 'block';
   obj.style.position = 'relative';
   var myAnim = new YAHOO.util.Anim(obj, { height: { to: start_height } }, .4, YAHOO.util.Easing.easeOut);
   myAnim.animate(); 
}

function start_super_slides() {
	super_timer = window.setInterval( "super_rotator();", (5000) )
}


function marquee() {
    // basic version is: $('div.demo marquee').marquee() - but we're doing some sexy extras
    $('div#thread marquee').marquee();
}

$(document).ready(function(){
	loadFooterLogos();
	$('div.gallery').galleryScroll({'autoSlide':false});
	$('.page_feed').equalHeights();
	
	// nav dropdowns
	$("#navigation ul.menu > li").each(function() {
		if ($(this).find("ul").length > 0){
			var obj=$(this),
			ul=obj.find("ul");
			// $("<span>").text("+").appendTo(obj.children(":first"));
			obj.hover(function(){
				if($.browser.msie){
					ul.stop(true,true).show();
				}else{
					ul.stop(true,true).slideDown();
				}
			},function(){
				if($.browser.msie){
					ul.stop(true,true).hide();
				}else{
					ul.stop(true,true).slideUp();
				}
			});
		}
	});
	
//	$('div.gallery2').galleryScrollV();
//	$('.tabset-slides').corner("10px");
//	$('.page_feed').corner("4px");

});

loadFooterLogos = function() {
	$('div#footerLogos span').hover(function() {
	var ghostImage = $(this).find('img.ghost-image'), mainImage = $(this).find('img.main-image');
	      if(mainImage.length) {
		ghostImage.css('display','none');
		mainImage.css('display','inline');
	   }
	},
	   function() {
	var ghostImage = $(this).find('img.ghost-image'), mainImage = $(this).find('img.main-image');
	      if(mainImage.length) {
		mainImage.css('display','none');
		ghostImage.css('display','inline');
	   }
	});
}

if (navigator.appVersion.match(/MSIE([^;]+);/) != null && parseInt(navigator.appVersion.match(/MSIE([^;]+);/).pop()) > 8) Cufon.set('engine', 'canvas'); 
Cufon.replace('h1');
Cufon.replace('.link_linkbox h2');
Cufon.replace('.style_node_feed h3');
Cufon.replace('.article_featured h3');
Cufon.replace('.evbodwrap h4');
Cufon.replace('.content-view-line h2');

//EventWorker.addHandler("window.onload", start_super_slides);

function feature_slides() {
/*	$(document).ready(function(){*/
	var currentSlideId = null;
		$(".tab-btn a").each(function(){return false;});
		$(".tabset-slides").jCarouselLite({
			auto:autoSlide,
			btnPrev:".previous-btn",
			btnNext:".next-btn",
			btnGo:tabControls,
			btnActions:{pause:".pause-btn",play:".play-btn"},
			speed: 1000,
			visible:1,
			beforeStart:function(a){
				currentSlideId='#btn-'+a[0].id;},
			afterEnd:function(a){
				$(currentSlideId).removeClass('active');
				$('#btn-'+a[0].id).addClass('active');
			}
		});
/*	});*/
}

var cur_vbn_tab=0;
vbn_tab_root = 0;
function set_vidbynode_tabs() {
    if (vbn_tab_root == 0) {
	 var first_tab = document.getElementById('vbn_tab_0');
        vbn_tab_root=first_tab.parentNode.parentNode;
    }
    tabs=getElementsByClass(vbn_tab_root,'vidbytab_tab','H3');
    for (i in tabs) {
        tabs[i].onmouseover = function() { 
            if (this.id.replace(/vbn_tab_/,'') != cur_vbn_tab) {
			this.parentNode.className = 'vidbytab_tab_holder selected';
			document.getElementById('vbn_tab_'+cur_vbn_tab).parentNode.className = 'vidbytab_tab_holder';
			cur_vbn_tab = this.id.replace(/vbn_tab_/,'');
            }
        }
    }
}