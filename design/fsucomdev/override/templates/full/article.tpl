{* Article - Full view *}

{def $usedate = cond($node.data_map.publish_date.has_content, $node.data_map.publish_date.content.timestamp, $node.object.published)}
<p class="date">{currentdate()|datetime('custom', '%l, %F %j, %Y')|upcase}</p>

<header>
	<h1>{$node.data_map.title.content|wash()}</h1>
</header>

<div class='addthis'>
<!-- ADDTHIS BUTTON BEGIN -->
<script type="text/javascript">
addthis_pub             = 'fsumedia';
addthis_logo            = 'http://news.fsu.edu/video/search.gif';
addthis_logo_background = 'cccc99';
addthis_logo_color      = '540115';
addthis_brand           = 'news.fsu.edu';
addthis_options         = 'favorites, email, digg, delicious, myspace, facebook, google, live, more';
</script>
<a href="http://www.addthis.com/bookmark.php" onmouseover="return addthis_open(this, '', '[URL]', '[TITLE]')" onmouseout="addthis_close()" onclick="return addthis_sendto()"><img src="http://s9.addthis.com/button1-share.gif" width="125" height="16" border="0" alt="" vspace="10" /></a>
<script type="text/javascript" src="http://s7.addthis.com/js/152/addthis_widget.js"></script>
<!-- ADDTHIS BUTTON END -->
</div>

<div class="attribute-byline">
{if $node.data_map.author.content.is_empty|not()}
<p class="author">
     <a href='mailto:{$node.data_map.author.content.author_list.0.email}'>{attribute_view_gui attribute=$node.data_map.author}</a>
</p>
{/if}
<p class="date">
     {$usedate|datetime('custom', '%m/%d/%Y %g:%i %a')}
</p>
</div>

{if eq(ezini('article','ImageInFullView','content.ini'),'enabled')}
	{if $node.data_map.image.has_content}
		<div class='object-left'>
		<div class="attribute-image">
			{attribute_view_gui attribute=$node.data_map.image image_class=medium}
			{if $node.data_map.image_caption.has_content}
			<div class="caption" style="width: {$node.data_map.image.content.medium.width}px">
				{attribute_view_gui attribute=$node.data_map.image_caption}
			</div>
			{/if}
		</div>
		</div>
	{/if}
{/if}

{if eq(ezini('article','SummaryInFullView','content.ini'),'enabled')}
	{if $node.data_map.intro.content.is_empty|not}
		<div class="attribute-short">
			{attribute_view_gui attribute=$node.data_map.intro}
		</div>
	{/if}
{/if}

{if $node.data_map.body.content.is_empty|not}
	<div class="attribute-long">
		{attribute_view_gui attribute=$node.data_map.body}
	</div>
{/if}

{if and(is_set($node.data_map.star_rating),$node.data_map.star_rating.has_content)}
<div class="attribute-star-rating">
	{attribute_view_gui attribute=$node.data_map.star_rating}
</div>
{/if}

{*include uri='design:parts/related_content.tpl'*}
{include uri="design:parts/comments.tpl" class=array('advanced','module') section=true()}

{def $tipafriend_access=fetch('user','has_access_to',hash('module','content','function','tipafriend'))}
{if and(ezmodule('content/tipafriend'),$tipafriend_access)}
<div class="attribute-tipafriend">
	<p><a href={concat("/content/tipafriend/",$node.node_id)|sitelink()} title="{'Tip a friend'|i18n('design/ezwebin/full/article')}">{'Tip a friend'|i18n('design/ezwebin/full/article')}</a></p>
</div>
{/if}
