{* Article - Featured View *}

{def $bannerClass = first_set($banner_class, 'fsucom_featuredstory')
	 $banners = fetch('content', 'list', hash('parent_node_id', $node.node_id,
										  'sort_by', $node.sort_array,
										  'class_filter_type', 'include',
										  'class_filter_array', array('image', 'botr')))}

<div class='article_featured'>
	
{if count($banners)}

	<div class="featured_banner_container">
	<div class="featured_banner_wrapper">
	
	
		<div class='banners_container' id="banners_{$node.node_id}">
			{foreach $banners as $banner}
			<div class="featured_banner_content">
			{if $banner.object.class_identifier|eq('image')}
			<img alt="{$banner.data_map.caption.content.output.output_text|strip_tags}" src="{$banner.data_map.image.content[$bannerClass].url|sitelink('no')}" width="{$banner.data_map.image.content[$bannerClass].width}" height="{$banner.data_map.image.content[$bannerClass].height}" />
			{else}
			{attribute_view_gui attribute=$banner.data_map.video player='edxUhQUY'}
			{/if}
			<div class="featured_banner_content_wrap">{$banner.data_map.caption.content.output.output_text}</div>
			</div>
			{/foreach}
		</div>
		</div>
	</div>
	<nav class="banner_controls" id='banner_controls_{$node.node_id}'><ul class="menu horizontal"></ul></nav>

	<script type="text/javascript">
	
	{literal}
	
			$CycleRotation.register('banners_{/literal}{$node.node_id}{literal}',{
				'type':'slideshow',
				'selector':'#banners_{/literal}{$node.node_id}{literal}',
				'animation':{
					'timeout':5000,
					'pause':true,
					'pager':'#banner_controls_{/literal}{$node.node_id}{literal} .menu',
					'pagerAnchorBuilder':function(i,s){
						return '<li class="jump-control-'+i+'"><a href="#">'+(++i)+'</a></li>';
					}
				},
				'onAfter': function(){

				}
			});
			
	{/literal}
			
	</script>

{/if}


{if is_unset($expanded)}{def $expanded = true()}{/if}
{if is_unset($length)}{def $length=100}{/if}
{if is_unset($image_class)}{def $image_class='articlethumbnail'}{/if}
{if is_unset($summary)}{def $summary=true()}{/if}
{if is_unset($datetime)}{def $datetime=false()}{/if}

<div class="content-view-node-feed">
   <div class="class-article float-break">
{if $expanded}
   {if $node.data_map.image.has_content}
	<div class="attribute-image">{attribute_view_gui image_class=$image_class href=$node|sitelink attribute=$node.data_map.image}</div>
   {/if}
	<div class="attribute-header"><h3><a href={$node|sitelink}>{$node.name|wash()}</a></h3></div>
   {if and($summary, not($node.data_map.intro.content.is_empty) )}
   {def $readMore = concat('... <a class="read_more" href="',$node|sitelink(no),'">Read Story</a>')}
	<div class="attribute-short">{$node.data_map.intro.content.output.output_text|html_shorten($length, $readMore)}</a></div>
	
	<div class='addthis'>
	<!-- ADDTHIS BUTTON BEGIN -->
	<script type="text/javascript">
	addthis_pub             = 'fsumedia';
	addthis_logo            = 'http://news.fsu.edu/video/search.gif';
	addthis_logo_background = 'cccc99';
	addthis_logo_color      = '540115';
	addthis_brand           = 'News.fsu.edu';
	addthis_options         = 'favorites, email, digg, delicious, myspace, facebook, google, live, more';
	</script>
	<a href="http://www.addthis.com/bookmark.php" onmouseover="return addthis_open(this, '', '[URL]', '[TITLE]')" onmouseout="addthis_close()" onclick="return addthis_sendto()"><img src="http://s9.addthis.com/button1-share.gif" width="125" height="16" border="0" alt="" vspace="10" /></a>
	<script type="text/javascript" src="http://s7.addthis.com/js/152/addthis_widget.js"></script>
	<!-- ADDTHIS BUTTON END -->
	</div>
	
   {/if}
{else}
	<div class="attribute-header"><a href="{$node|sitelink(no)}">{$node.name}</a></div>
{/if}
   </div>
</div>

</div>