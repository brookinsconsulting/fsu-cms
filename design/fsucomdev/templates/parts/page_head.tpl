{default enable_help=true() enable_link=true()}

{let name=Path
     path=$module_result.path
     reverse_path=array()}
  {section show=is_set($module_result.title_path)}
    {set path=$module_result.title_path}
  {/section}
  {section loop=$:path offset=2}
    {set reverse_path=$:reverse_path|array_prepend($:item)}
  {/section}

{set-block scope=root variable=site_title}
{section loop=$Path:reverse_path}{$:item.text|wash}{delimiter} / {/delimiter}{/section} - {$site.title|wash}
{/set-block}

{/let}

    <title>{if $current_node.data_map.meta_title.has_content}{$current_node.data_map.meta_title.content|wash()|trim()}{else}{$site_title}{/if}</title>

    {section show=and(is_set($#Header:extra_data),is_array($#Header:extra_data))}
      {section name=ExtraData loop=$#Header:extra_data}
      {$:item}
      {/section}
    {/section}

    {* check if we need a http-equiv refresh *}
    {section show=$site.redirect}
    <meta http-equiv="Refresh" content="{$site.redirect.timer}; URL={$site.redirect.location}" />

    {/section}

    {section name=HTTP loop=$site.http_equiv}
    <meta http-equiv="{$HTTP:key|wash}" content="{$HTTP:item|wash}" />

    {/section}

    {foreach $site.meta as $key=>$item}
    {if array('keywords','description')|contains($key)}
    {if and(is_set($current_node.data_map[concat('meta_',$key)]), $current_node.data_map[concat('meta_',$key)].has_content)}
    {set-block variable='item'}{attribute_view_gui attribute=$current_node.data_map[concat('meta_',$key)]}{/set-block}
    {/if}
    {if and(eq($key, 'keywords'), ne($current_node.data_map.meta_tags.content,''))}
    {set-block variable='item'}{attribute_view_gui attribute=$current_node.data_map[concat('meta_tags')]}{/set-block}
    {/if}
    {/if}
    <meta name="{$key|wash()}" content="{$item|wash()|trim()}" />
    {/foreach}

    <meta name="MSSmartTagsPreventParsing" content="TRUE" />
    <meta name="generator" content="eZ Publish" />
    <meta name="google-site-verification" content="0DokJpz7o9NBI9uuDBU8VhLFdRr1F_4hnDDk7cxSIa4" />
{section show=$enable_link}
    {include uri="design:link.tpl" enable_help=$enable_help enable_link=$enable_link}
{/section}

{/default}

<style type="text/css">
    @import url({"stylesheets/core.css"|ezdesign(double)});
    @import url({"stylesheets/pagelayout.css"|ezdesign(double)});
    @import url({"stylesheets/content.css"|ezdesign(double)});
    @import url({"stylesheets/websitetoolbar.css"|ezdesign(double)});
	@import url({"stylesheets/pagecore.css"|ezdesign(double)});
</style>

{def $ini_css = cond(is_unset($load_css_file_list),ezini('StylesheetSettings', 'CSSFileList', 'design.ini'),false())
	 $frontend_css = cond(ezini_hasvariable('StylesheetSettings', 'FrontendCSSFileList', 'design.ini'),ezini('StylesheetSettings', 'FrontendCSSFileList', 'design.ini'),false())
	 $stylesheets_list = array('fsu_ez.css')
}
{if $frontend_css}{set $ini_css=cond($ini_css,$ini_css|merge($frontend_css),$frontend_css)}{/if}
{ezcss_load(cond($ini_css,$stylesheets_list|merge($ini_css),$stylesheets_list))}

<link rel="stylesheet" type="text/css" href={"stylesheets/print.css"|ezdesign} media="print" />

<!-- IE conditional comments; for bug fixes for different IE versions -->
<!--[if IE 5]><link rel="stylesheet" type="text/css" href="{"stylesheets/browsers/ie5.css"|ezdesign(no)}"  /><![endif]-->
<!--[if lte IE 7]><link rel="stylesheet" type="text/css" href="{"stylesheets/browsers/ie7lte.css"|ezdesign(no)}"  /><![endif]-->
<!--[if lte IE 7]><link rel="stylesheet" type="text/css" href="{"stylesheets/ie/fsucom.ie.css"|ezdesign(no)}"  /><![endif]-->
<!--[if IE 8]><link rel="stylesheet" type="text/css" href="{"stylesheets/ie/fsucom.ie8.css"|ezdesign(no)}"  /><![endif]-->
<!--[if IE 9]><link rel="stylesheet" type="text/css" href="{"stylesheets/ie/fsucom.ie9.css"|ezdesign(no)}"  /><![endif]-->
<!--[if lte IE 7]><link rel="stylesheet" type="text/css" href="{"stylesheets/ie/lte7.css"|ezdesign(no)}"  /><![endif]-->
<!--[if IE]><link rel="stylesheet" type="text/css" href="{'stylesheets/ie.css'|ezdesign(no)}"  /><![endif]-->


{def $ini_scripts = cond(is_unset($load_script_list),ezini('JavaScriptSettings','JavaScriptList','design.ini'),false())
	 $frontend_scripts = cond(ezini_hasvariable('JavaScriptSettings', 'FrontendJavaScriptList', 'design.ini'),ezini('JavaScriptSettings', 'FrontendJavaScriptList', 'design.ini'),false())
	 $javascript_list = array()
}
{if $frontend_scripts}{set $ini_scripts=cond($ini_scripts,$ini_scripts|merge($frontend_scripts),$frontend_scripts)}{/if}
{ezscript_load(cond($ini_scripts,$javascript_list|merge($ini_scripts),$javascript_list))}

{literal}
<script type="text/javascript">
	$(function(){

		// Accordion
		$("#accordion").accordion({ header: "h3" });

		// Tabs
		$('#tabs').tabs();
		
		//hover states on the static widgets
		$('#dialog_link, ul#icons li').hover(
			function() { $(this).addClass('ui-state-hover'); }, 
			function() { $(this).removeClass('ui-state-hover'); }
		);
		
	});
</script>
<!-- Google Analytics Code - 24/7 -->
<script type="text/javascript">

  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-33643578-1']);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();

</script>

<!-- Google Analytics Code - fsu.edu -->
<script type="text/javascript">
var gaJsHost = (("https:" == document.location.protocol) ? "https://ssl." : "http://www.");
document.write(unescape("%3Cscript src='" + gaJsHost + "google-analytics.com/ga.js' type='text/javascript'%3E%3C/script%3E"));
</script>
<script type="text/javascript">
try {
var pageTracker = _gat._getTracker("UA-5213390-1");
pageTracker._trackPageview();
} catch(err) {}</script>
{/literal}

<!--[if lt IE 9]><script type="text/javascript" src={'javascript/ie/html5.js'|ezdesign()} charset="utf-8"></script><![endif]-->