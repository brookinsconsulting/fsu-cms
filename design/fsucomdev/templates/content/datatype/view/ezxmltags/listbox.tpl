{if or($content|eq(''),$content|contains('listbox'))}

{def $node_id = $source|explode('://')[1]
	 $classes_r = $classes|explode(',')
	 $node = fetch('content','node',hash('node_id',$node_id))
	 $fetch_type = cond(fetch(content, list_count, hash(parent_node_id, $node_id, class_filter_type, 'include', class_filter_array, $classes_r))|gt(0), 'list', 'tree')
}

{if $fetch_type|eq('list')}
	{def  $dataNodes = fetch('content', 'list', hash('parent_node_id',$node_id,
						'limit', first_set(cond($ticker_limit,$ticker_limit,$limit),3),
						'class_filter_type', 'include', 
						'class_filter_array', $classes_r,
						'attribute_filter', cond($filter|ne(''), $filter|eval(), array(array('depth','>',0))),
						'sort_by', cond($sort|ne(''), $sort|eval(), $node.sort_array)))}
{else}
	{def  $dataNodes = fetch('content', 'tree', hash('parent_node_id',$node_id,
						'limit', first_set(cond($ticker_limit,$ticker_limit,$limit),3),
						'class_filter_type', 'include', 
						'class_filter_array', $classes_r,
						'attribute_filter', cond($filter|ne(''), $filter|eval(), array(array('depth','>',0))),
						'sort_by', cond($sort|ne(''), $sort|eval(), $node.sort_array)))}
{/if}

<script type="text/javascript">

	var cycle_functions_{$node.node_id} = new Array();
	
	{literal}
	
	$(document).ready(function(){
		if (typeof jwplayer == 'function') {
			jwplayer().onPlay(
			    function(object) {
				  $CycleRotation.pause($('#'+this.id).parents('.banners_container').attr("id"));
			    }
			);
		
			jwplayer().onPause(
			    function(object) {
				  $CycleRotation.play($('#'+this.id).parents('.banners_container').attr("id"));
			    }
			);
		}
	});

	{/literal}
	
</script>

<div class='listbox style_{$linestyle}'{if or($background, $border)} style='{if $background}background:{$background};{/if}{if $border}border:{$border};{/if}'{/if}>
{if $header}<div class='tag_header'><h2>{if $more_link}<span class='morelink'><img src={'images/morelink.png'|ezdesign}><a href='{cond($more_link_href, $more_link_href, $node.url_alias|ezroot(no))}'>{$more_link}</a></span>{/if}{if $linestyle|eq('featured')}<h4>{currentdate()|datetime('custom', '%l, %F %j, %Y')}</h4>{/if}{$header}</h2></div>{/if}
<div id='listbox_items_{$node_id}' class='item_wrapper'>
	
{foreach $dataNodes as $k=>$d}
	{node_view_gui content_node=$d view=$linestyle my_parent_id=$node.node_id extra_class=cond($k|eq($dataNodes|count|sub(1)), 'last', false())}
{/foreach}

</div>

{if and($effect|eq('slide'), $dataNodes|count|gt(1))}

<script type="text/javascript">

		$CycleRotation.register('banners_{$node.node_id}{literal}',{
			'type':'slideshow',
			'selector':'#listbox_items_{/literal}{$node.node_id}{literal}',
			'animation':{
				'timeout':5000,
				'autostart':false,
				'pause':true,
				'pager':'#listbox_controls_{/literal}{$node.node_id}{literal} .menu',
				'pagerAnchorBuilder':function(i,s){
					return '<li class="jump-control-'+i+'"><a href="#">'+(++i)+'</a></li>';
				}
			},
			'onAfter': function(){
				$CycleRotation.pause('banners_{/literal}{$node.node_id}{literal}');
			}
		});{/literal}

</script>

<div class='banner_controls' id="listbox_controls_{$node.node_id}"><ul class='menu horizontal'></ul></div>

{elseif and($effect|eq('rotate'), $dataNodes|count|gt(1))}

<script type="text/javascript">
	$("#listbox_items_{$node.node_id}{literal}").before('<div id="main_rotate_nav"><span class="feature_label">Top Stories: </span><span class="featurenav" id="nav_{/literal}{$node.node_id}{literal}"></span></div>').cycle({'pager': $("#nav_{/literal}{$node.node_id}{literal}")}).cycle('pause');{/literal}
</script>

{/if}

{if and($linestyle|ne('featured'), $dataNodes.0.object.class_identifier|eq('article'))}<a class='more_articles' href='{$dataNodes.0.parent.url_alias|ezroot(no)}'>More articles</a>{/if}

</div>

{else}

<div class='listbox style_literal'>

{$content|htmlspecialchars_decode()}

</div>

{/if}