{*  
SG_Folder      = eznode id number for Supergraphics (type is Folder) - media library/images/Deptname Supergraphics - Film School Supergraphics Folder

    HF_Homepage_Flash     = eznode id number for Homepage Flash (type is Homepage Flash) - inside Supergraphics Folder 
    
    FB_Folder      = eznode id number for the Flash Banner (type is Folder) - first level of directory  FSU/   -   Flash Rotation/Banner Articles
    
    Dept_Site_FrontPage      = eznode id number for the Department Site (type is Frontpage) - first level of directory  FSU/   Site Frontpage Node  
    
    Dept_Site_Directory      = directory for the Department Site (example: film_school) - first level of directory    - film_school, mbarker
*}

{def $SG_Folder='3867'}
{def $FB_Folder='6067'}
{def $HF_Homepage_Flash='6066'}
{def $Dept_Site_FrontPage='3866'}
{def $Dept_Site_Directory='training_omni'}

{let super_id = and( eq(module_params()['function_name'], 'versionview' ) , eq($root_node.parent.node_id, $FB_Folder) )|choose($module_result.node_id,$Dept_Site_FrontPage)
     supergraphicnode=fetch( 'content', 'node', hash( 'node_id', fetch('content', 'node', hash('node_id', $super_id, limitation, array())).main_node_id, limitation, array() ))
     allsupergraphics=fetch( 'content', 'list', hash( 'parent_node_id', $SG_Folder, class_filter_type, 'exclude', class_filter_array, array('homepage_flash'), limitation, array()))
     relatedobjects=array()
     supergraphicarray=array()
     my_caption_at=''  
}

{if and(is_set($supergraphicnode.data_map.flash_image), $supergraphicnode.data_map.flash_image.content.is_valid)}
  <div id="supergraphic_container"><img name='supergraphic' src={$supergraphicnode.data_map.flash_image.content.supergraphic.url|ezroot} alt="{$supergraphicnode.data_map.flash_head.content.data_text|count_chars()|choose('Page supergraphic', $supergraphicnode.data_map.flash_head.content.data_text|wash)}" /></div><!-- <div id="supergraphic_container"> -->
{else}


{set relatedobjects=$supergraphicnode.object.related_contentobject_array}



{foreach $relatedobjects as $thissobject}
   {if eq($thissobject.main_parent_node_id,$SG_Folder)}
	{set supergraphicarray=$supergraphicarray|append($thissobject)}
   {/if}
{/foreach}


{if gt($supergraphicarray|count,0)}
   {set supergraphicarray = $supergraphicarray|shuffle}
{else}
   {set supergraphicarray = $allsupergraphics|shuffle}
{/if}

{set my_caption_at = $supergraphicarray.0.data_map.caption}

{switch match=first_set($supergraphicarray.0.class_identifier, "")}

  {case match="homepage_flash"}
<!-- DISPLAY HOMEPAGE FLASH --> 
  <div id="supergraphic_container">
  </div><!-- <div id="supergraphic_container"> --> 

  <script type="text/javascript">
          var so = new SWFObject("{concat('/design/', $Dept_Site_Directory)}/flash/FSU_News.swf", "", "795", "245", "7", "#000");
	   so.addVariable("dataLoc","{concat("/layout/set/xml", first_set($supergraphicarray.0.main_node.url_alias,$supergraphicarray.0.url_alias)|ezroot('no'))}&amp;speed=8000");
	   so.addParam("quality", "high");
	   so.addParam("wmode", "transparent");
          so.write("supergraphic_container");
  </script>

  {/case}

  {case match="flash"}
<!-- DISPLAY FLASH --> 
  <div id="supergraphic_container">
  {attribute_view_gui attribute=$supergraphicarray.0.data_map.file }
  </div><!-- <div id="supergraphic_container"> --> 
  {/case}

  {case match="image"}
<!-- DISPLAY PAGE SUPERGRAPHIC --> 
  {def $imagerelations=fetch( 'content', 'reverse_related_objects', hash( 'object_id', first_set($supergraphicarray.0.id, $supergraphicarray.0.contentobject_id), 'all_relations', true()))}
  <div id="supergraphic_container">{if and($imagerelations|count, ne($imagerelations.0.main_node.node_id,$supergraphicnode.node_id))}<a href ={$imagerelations.0.main_node.url_alias|ezroot}>{/if}<img alt = '{$supergraphicarray.0.data_map.caption.has_content|choose('Page supergraphic', $supergraphicarray.0.data_map.caption.content.output.output_text|strip_tags|trim)}' name='supergraphic' src={$supergraphicarray.0.data_map.image.content.supergraphic.url|ezroot}/>{if $imagerelations|count}</a>{/if}</div><!-- <div id="supergraphic_container"> -->
  {/case}

  {case}
  {/case}

{/switch}

{/if}


{if and($supergraphicarray|count, $supergraphicarray.0.data_map.caption.has_content)}
<div class="fold" style='height: 20px;'>
<div class="foldBorders" style='height: 20px;'>
<div class="attribute-caption-sg" style='color: white;'>
{$my_caption_at.content.output.output_text}</div>
</div>
</div>
{else}
<div class="fold"><div class="foldBorders"></div></div>
{/if}

{undef} 

{/let}
