<?php

if (!file_put_contents("vidimportlog.txt", "Starting...\r\n", FILE_APPEND )) die('Could not write to log file!');


require("extension/multimedia/classes/botr_api.php");

$db = eZDB::instance();

$new_q = "select * from ezcontentobject_attribute where contentclassattribute_id = 372";

$results = $db->arrayQuery($new_q);

foreach ($results as $r) {
	
	$log = "";
	
	$new_q = "select * from ezcontentobject_attribute where contentclassattribute_id = 487 and contentobject_id = ". $r['contentobject_id'];
	$results = $db->arrayQuery($new_q);
	if (!count($results)) continue;
	if ($results[0]['data_text'] != '') continue;
	
	$upurl = "http://service.twistage.com/videos/".$r['data_text']."/file.source";
	$log .= "Pulling $upurl\r\n";
	$execme = "wget --server-response --spider $upurl";
	$output = array($execme);
	exec($execme.' 2>&1', $output);
	
	preg_match('/Content-Disposition: attachment; filename="([^"]*)"/', implode("\n", $output), $matches);
	
	if (count($matches) > 1) {
	
		$vidname = $matches[1];
	
		$execme = "wget -O '/tmp/tmpvid/$vidname' $upurl";
		$output = array($execme);
		exec($execme.' 2>&1', $output);
	

		$ini = new eZINI('botr.ini');
		$Key = $ini->variable('BOTRSettings', 'Key');
		$Private = $ini->variable('BOTRSettings', 'Private');
		$botr_api = new Botr_API($Key,$Private);
	
	
		$response = $botr_api->call('/videos/create', array('title'=>$vidname));

		if ($response['status'] != 'error') { 

			$target_url  = 'http://'.$response['link']['address'].$response['link']['path'];
			$target_url .= '?key='.$response['link']['query']['key'];
			$target_url .= '&api_format=xml';
		
			$token =  $response['link']['query']['token'];
		
			$file_to_upload = array('file'=>'@'. "/tmp/tmpvid/$vidname", 'token' => $token);
			
			unset($ch);
			
			$ch = curl_init();
			curl_setopt($ch, CURLOPT_URL, $target_url);
			curl_setopt($ch, CURLOPT_POST,1);
			curl_setopt($ch, CURLOPT_POSTFIELDS, $file_to_upload);
			ob_start();
			$check=curl_exec ($ch);
			$result=ob_get_contents();
			ob_end_clean();
			curl_close ($ch);
			
			$xml = new SimpleXMLElement($result);
			
			$mykey = $xml->video[0]['key'];
			$myid = $r['contentobject_id'];
			
			$dome = "update ezcontentobject_attribute set data_text='$mykey' where contentclassattribute_id = 487 and contentobject_id = $myid";
			
			$log .= $dome;
			
			$results = $db->query($dome);

		} else {

		$log .= "Could not get post url!";	

		}
		
		unlink("/tmp/tmpvid/$vidname");
		file_put_contents("vidimportlog.txt", $log."\r\n\r\n", FILE_APPEND );
	
	} 
}

?>