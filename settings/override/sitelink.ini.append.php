<?php /* #?ini charset="utf-8"?


[OperatorSettings]
SiteLinkClassList[]
SiteLinkClassList[]=link
SiteLinkClassList[]=file


# Forces that an absolute url be used. Possible Values: enabled|disabled
ForceAbsoluteURL=disabled

# Sets the default link type for all SiteLink classes
DefaultLinkType=internal

#Allows the site.ini siteaccess list to be overridden to specify a default hostname when more than one hostname matches a siteaccess
HostOverride=enabled

SiteAccess[]
SiteAccess[film2]=film.fsu.edu
SiteAccess[eoas]=eoas.fsu.edu


[DataTypeSettings]
ClassList[]
ClassList[ezbinaryfile]=SiteLinkBinaryFile
ClassList[ezobjectrelation]=SiteLinkObjectRelation
ClassList[ezurl]=SiteLinkURL


#[class_identifier]
# Used to override the operator setting for a class
#DefaultLinkType=
# Specifiy the attribute identifier for to use for each link type
#LinkTypeList[]
# Specifiy an override data type class to use
#DataTypeClass=

[link]
DefaultLinkType=external
LinkTypeList[external]=location

[file]
DefaultLinkType=download
LinkTypeList[download]=file


/* ?>
