<?php /* #?ini charset="utf-8"?

[IndexOptions]
OptimizeOnCommit=disabled

[IndexBoost]
MetaField[]
Class[]
Class[folder]=3.0
Class[frontpage]=3.0
Class[faq_question]=2.0
Class[article]=2.0
Class[info_page]=2.0
Attribute[]
Attribute[title]=1.5
Attribute[name]=3.0
Attribute[tags]=1.5
Datatype[]
Datatype[ezkeyword]=3.0
ReverseRelatedScale=0.4

[IndexExclude]
ClassIdentifierList[]
ClassIdentifierList[]=banner
ClassIdentifierList[]=cjw_contexthelp_class
ClassIdentifierList[]=cjw_contexthelp_class_attribute
ClassIdentifierList[]=cjw_contexthelp_container
ClassIdentifierList[]=video_upload
ClassIdentifierList[]=quicklinks

[SearchFilters]
RawFilterList[]
FilterHiddenFromDB=disabled
*/ ?>