<?php /* #?ini charset="utf-8"?

[ImageMagick]
IsEnabled=true
ExecutablePath=/usr/bin
Executable=convert
Filters[]=thumb=-resize 'x%1' -resize '%1x<' -resize 50%
Filters[]=centerimg=-gravity center -crop %1x%2+0+0 +repage
Filters[]=strip=-strip
Filters[]=geometry/scaleheightuponly=-geometry x%1<

[AliasSettings]
AliasList[]=small
AliasList[]=medium
AliasList[]=listitem
AliasList[]=articleimage
AliasList[]=articlethumbnail
AliasList[]=articlefeed
AliasList[]=gallerythumbnail
AliasList[]=gallerythumbnail120
AliasList[]=galleryline
AliasList[]=imagelarge
AliasList[]=large
AliasList[]=rss
AliasList[]=logo
AliasList[]=infoboximage
AliasList[]=billboard
AliasList[]=supergraphic
AliasList[]=fullwidth
AliasList[]=featuredstory
AliasList[]=fsucom_featuredstory
AliasList[]=linkbox
AliasList[]=myfloridastate
AliasList[]=press_link_icon

[press_link_icon]
Reference=
Filters[]=geometry/scaledownonly=30; 35
Filters[]=colorspace=RGB

[fullwidth]
Reference=
Filters[]=geometry/scalewidth=570
Filters[]=colorspace=RGB
[small]
Reference=
Filters[]=geometry/scaledownonly=100;160
Filters[]=colorspace=RGB

[medium]
Reference=
Filters[]=geometry/scaledownonly=200;290
Filters[]=colorspace=RGB

[large]
Reference=
Filters[]=geometry/scale=300;300
Filters[]=colorspace=RGB

[rss]
Reference=
Filters[]=geometry/scale=88;31
Filters[]=colorspace=RGB

[logo]
Reference=
Filters[]=geometry/scaleheight=36
Filters[]=colorspace=RGB

[listitem]
Reference=
Filters[]=geometry/scaledownonly=130;190
Filters[]=colorspace=RGB

[articleimage]
Reference=
Filters[]=geometry/scaledownonly=170;350
Filters[]=colorspace=RGB

[articlethumbnail]
Reference=
Filters[]=geometry/scaledownonly=70;150
Filters[]=colorspace=RGB

[articlefeed]
Reference=
Filters[]=geometry/scaledownonly=58;46
Filters[]=colorspace=RGB

[gallerythumbnail]
Reference=
Filters[]=geometry/scaledownonly=120;200
Filters[]=colorspace=RGB

[gallerythumbnail120]
Reference=
Filters[]=geometry/scaledownonly=120;90
Filters[]=colorspace=RGB

[galleryline]
Reference=
Filters[]=geometry/scaledownonly=70;150
Filters[]=colorspace=RGB

[imagelarge]
Reference=
Filters[]=geometry/scaledownonly=550;730
Filters[]=colorspace=RGB

[infoboximage]
Reference=
Filters[]=geometry/scalewidth=150
Filters[]=colorspace=RGB

[billboard]
Reference=
Filters[]=geometry/scalewidth=754

[supergraphic]
Reference=
Filters[]
Filters[]=geometry/scalewidth=795
Filters[]=geometry/crop=795;250;0;0
Filters[]=colorspace=RGB

[fsucom_featuredstory]
Reference=
Filters[]=geometry/scalewidth=500
Filters[]=geometry/scaleheightuponly=300
Filters[]=centerimg=500;300
Filters[]=strip=
Filters[]=colorspace=RGB

[featuredstory]
Reference=
Filters[]=thumb=300
Filters[]=centerimg=150;150
Filters[]=strip=
Filters[]=colorspace=RGB

[linkbox]
Reference=
Filters[]=geometry/scalewidth=168
Filters[]=centerimg=168;176
Filters[]=strip
Filters[]=colorspace/transparent
Filters[]=colorspace=RGB

[myfloridastate]
Reference=
Filters[]=geometry/scalewidth=332
Filters[]=centerimg=332;200
Filters[]=strip
Filters[]=colorspace/transparent
Filters[]=colorspace=RGB
*/ ?>
