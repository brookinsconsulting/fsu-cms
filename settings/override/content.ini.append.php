<?php /* #?ini charset="utf-8"?

[SitemapExclude]
Class[]
Class[]=banner
#Class[]=folder

[header]
AvailableClasses[]=newsHead

[factbox]
CustomAttributes[]
CustomAttributes[]=title
CustomAttributes[]=align
CustomAttributesDefaults[title]=factbox

[table]
AvailableClasses[]=list
AvailableClasses[]=cols
AvailableClasses[]=comparison
AvailableClasses[]=gold
AvailableClasses[]=default
AvailableClasses[]=gold2
ClassDescription[list]=List
ClassDescription[cols]=Timetable
ClassDescription[comparison]=Comparison Table
ClassDescription[default]=Default
ClassDescription[gold2]=Gold Table Centered Text

[object]
AvailableClasses[]=itemized_sub_items
AvailableClasses[]=itemized_subtree_items
AvailableClasses[]=highlighted_object
AvailableClasses[]=vertically_listed_sub_items
AvailableClasses[]=horizontally_listed_sub_items
ClassDescription[itemized_sub_items]=Itemized Sub Items
ClassDescription[itemized_subtree_items]=Itemized Subtree Items
ClassDescription[highlighted_object]=Highlighted Object
ClassDescription[vertically_listed_sub_items]=Vertically Listed Sub Items
ClassDescription[horizontally_listed_sub_items]=Horizontally Listed Sub Items

[embed]
AvailableClasses[]=itemized_sub_items
AvailableClasses[]=itemized_subtree_items
AvailableClasses[]=highlighted_object
AvailableClasses[]=vertically_listed_sub_items
AvailableClasses[]=horizontally_listed_sub_items
AvailableClasses[]=promo_rotator
AvailableClasses[]=page_feed
AvailableClasses[]=featured_stories
AvailableClasses[]=plain_images
ClassDescription[itemized_sub_items]=Itemized Sub Items
ClassDescription[itemized_subtree_items]=Itemized Subtree Items
ClassDescription[highlighted_object]=Highlighted Object
ClassDescription[vertically_listed_sub_items]=Vertically Listed Sub Items
ClassDescription[horizontally_listed_sub_items]=Horizontally Listed Sub Items
ClassDescription[promo_rotator]=Promo Rotator
ClassDescription[page_feed]=Page Feed
ClassDescription[featured_stories]=Featured Stories
ClassDescription[plain_images]=Plain Images
CustomAttributes[]
CustomAttributes[]=limit
CustomAttributes[]=header
CustomAttributes[]=more_text
CustomAttributes[]=expanded_count


[paragraph]
AvailableClasses[]
AvailableClasses[]=center
AllowEmpty=true

[quote]
CustomAttributesDefaults[align]=right 

[literal]
AvailableClasses[]=html

[li]
AvailableClasses[]=nobullet
ClassDescription[nobullet]=No Bullet

[article]
SummaryInFullView=disabled

[folder]
SummaryInFullView=disabled

[CustomTagSettings]
AvailableCustomTags[]=h1
AvailableCustomTags[]=featured_stories
AvailableCustomTags[]=tune_in
AvailableCustomTags[]=media_player
AvailableCustomTags[]=hover_image
AvailableCustomTags[]=listbox

[featured_stories]
CustomAttributes[]
CustomAttributes[]=limit

[listbox]
CustomAttributes[]
CustomAttributes[]=limit
CustomAttributes[]=source
CustomAttributes[]=classes
CustomAttributes[]=sort
CustomAttributes[]=filter
CustomAttributes[]=linestyle
CustomAttributes[]=border
CustomAttributes[]=background
CustomAttributes[]=effect
CustomAttributes[]=header
CustomAttributes[]=more_link
CustomAttributes[]=more_link_href

[tune_in]
CustomAttributes[]
CustomAttributes[]=header
CustomAttributes[]=source

[hover_image]
CustomAttributes[]
CustomAttributes[]=hover

[VersionManagement]
VersionHistoryClass[]
VersionHistoryClass[]=10
*/ ?>
