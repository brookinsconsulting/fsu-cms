<?php /* #?ini charset="utf-8"?

[InformationCollectionSettings]
EmailReceiver=

[SiteSettings]
SiteShortName=The Film School
SiteName=The College of Motion Picture, Television and Recording Arts
SiteURL=film.fsu.edu
LoginPage=embedded
IndexPage=/content/view/full/1710/
SiteRootNodeID=1710
RootNodeDepth=2
DefaultPage=/content/view/full/1710/
MetaDataArray[author]=Florida State University Film School
MetaDataArray[keywords]=education, post-secondary florida state university, graduate school, career advancement, 
MetaDataArray[description]=Florida State University is a Carnegie Doctoral/Research 
MetaDataArray[classification]=Continuing Education, Professional Development, 

[UserSettings]
RegistrationEmail=

[SiteAccessSettings]
RequireUserLogin=false
RelatedSiteAccessList[]=film2
RelatedSiteAccessList[]=fsu_admin
ShowHiddenNodes=false
PathPrefix=Film-School

[DesignSettings]
SiteDesign=film2
AdditionalSiteDesignList[]=fsudesign
AdditionalSiteDesignList[]=ezwebin
AdditionalSiteDesignList[]=base

[DebugSettings]
DebugOutput=enabled
DebugRedirection=disabled

[TemplateSettings]
Debug=disabled
ShowXHTMLCode=disabled
ShowUsedTemplates=enabled
