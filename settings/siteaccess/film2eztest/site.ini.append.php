<?php /* #?ini charset="utf-8"?

[InformationCollectionSettings]
EmailReceiver=

[SiteSettings]
SiteShortName=College of Motion Picture Arts
SiteName=College of Motion Picture Arts
SiteURL=film.fsu.edu
LoginPage=embedded
IndexPage=/content/view/full/1710/
SiteRootNodeID=1710
RootNodeDepth=2
DefaultPage=/content/view/full/1710/
MetaDataArray[author]=Florida State University College of Motion Picture Arts
MetaDataArray[keywords]=education, post-secondary florida state university, graduate school, career advancement, film school
MetaDataArray[description]=Florida State University is a Carnegie Doctoral/Research 
MetaDataArray[classification]=Continuing Education, Professional Development, 

[UserSettings]
RegistrationEmail=

[SiteAccessSettings]
RequireUserLogin=false
RelatedSiteAccessList[]=film2eztest
RelatedSiteAccessList[]=fsu_admin
ShowHiddenNodes=false
PathPrefix=Film-School

[DesignSettings]
SiteDesign=film2
AdditionalSiteDesignList[]=fsudesign
AdditionalSiteDesignList[]=ezwebin
AdditionalSiteDesignList[]=base

[DebugSettings]
DebugOutput=enabled
DebugRedirection=disabled

[TemplateSettings]
Debug=disabled
ShowXHTMLCode=disabled
ShowUsedTemplates=enabled

[DatabaseSettings]
SQLOutput=disabled
*/ ?>
