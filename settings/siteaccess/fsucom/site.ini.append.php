<?php /* #?ini charset="utf-8"?

[ContentSettings]
StaticCache=enabled

[ExtensionSettings]
ActiveExtensions[]=eztika

[InformationCollectionSettings]
EmailReceiver=

[SiteSettings]
SiteName=FSU.com
SiteURL=fsu.com
LoginPage=embedded
IndexPage=/content/view/full/68133/
RootNodeDepth=2
DefaultPage=/content/view/full/68133/
MetaDataArray[author]=Florida State University Communications Group
MetaDataArray[keywords]=fsu, news, florida state, campus
SiteShortName=FSU.com
SiteRootNodeID=68133

[UserSettings]
RegistrationEmail=

[SiteAccessSettings]
RequireUserLogin=false
RelatedSiteAccessList[]=fsu_com
#RelatedSiteAccessList[]=gradstudies
RelatedSiteAccessList[]=fsu_admin
ShowHiddenNodes=false
PathPrefix=FSU_com

[DesignSettings]
SiteDesign=fsucomdev
AdditionalSiteDesignList[]=framework
AdditionalSiteDesignList[]=fsucom
AdditionalSiteDesignList[]=fsu
AdditionalSiteDesignList[]=ezwebin
AdditionalSiteDesignList[]=base

[DebugSettings]
DebugOutput=enabled
DebugRedirection=disabled

[TemplateSettings]
Debug=disabled
ShowXHTMLCode=disabled
ShowUsedTemplates=enabled

[DatabaseSettings]
SQLOutput=disabled
*/ ?>

