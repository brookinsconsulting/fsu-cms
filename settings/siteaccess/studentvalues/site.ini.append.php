<?php /* #?ini charset="utf-8"?

[InformationCollectionSettings]
EmailReceiver=

[SiteSettings]
SiteShortName=Student Values
SiteName=Jon C. Dalton Institute on College Student Values 
SiteURL=studentvalues.ez.fsu.edu
LoginPage=embedded
IndexPage=/content/view/full/54867
RootNodeDepth=2
DefaultPage=/content/view/full/54867
MetaDataArray[author]=Florida State University Jon C. Dalton Institute on College Student Values
MetaDataArray[keywords]=education, post-secondary florida state university, graduate school, career advancement, 
MetaDataArray[description]=Florida State University is a Carnegie Doctoral/Research 
MetaDataArray[classification]=Continuing Education, Professional Development, 

[UserSettings]
RegistrationEmail=

[SiteAccessSettings]
RequireUserLogin=false
RelatedSiteAccessList[]=studentvalues
RelatedSiteAccessList[]=fsu_admin
ShowHiddenNodes=false
PathPrefix=Jon-C.-Dalton-Institute-on-College-Student-Values

[DesignSettings]
SiteDesign=studentvalues
AdditionalSiteDesignList[]=ezfind
AdditionalSiteDesignList[]=fsudesign
AdditionalSiteDesignList[]=ezwebin
AdditionalSiteDesignList[]=base

[SearchSettings]
SearchEngine=ezsolr

*/ ?>