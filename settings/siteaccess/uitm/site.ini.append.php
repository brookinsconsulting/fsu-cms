<?php /* #?ini charset="utf-8"?

[InformationCollectionSettings]
EmailReceiver=

[SiteSettings]
SiteShortName=UITM
SiteName=University IT Managers
SiteURL=uitm.ez.fsu.edu
LoginPage=embedded
IndexPage=/content/view/full/63023
RootNodeDepth=2
DefaultPage=/content/view/full/63023
MetaDataArray[author]=Florida State University
MetaDataArray[keywords]=education, post-secondary florida state university, 
MetaDataArray[description]=Florida State University is a Carnegie Doctoral/Research 
MetaDataArray[classification]=Continuing Education, Professional Development, 

[UserSettings]
RegistrationEmail=

[SiteAccessSettings]
RequireUserLogin=false
RelatedSiteAccessList[]=uitm
RelatedSiteAccessList[]=fsu_admin
ShowHiddenNodes=false
PathPrefix=University-IT-Managers

[DesignSettings]
SiteDesign=uitm
AdditionalSiteDesignList[]=ezfind
AdditionalSiteDesignList[]=fsu-v01
AdditionalSiteDesignList[]=ezwebin
AdditionalSiteDesignList[]=base

[SearchSettings]
SearchEngine=ezsolr

*/ ?>