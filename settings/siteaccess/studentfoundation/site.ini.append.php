<?php /* #?ini charset="utf-8"?

[InformationCollectionSettings]
EmailReceiver=

[SiteSettings]
SiteShortName=Student Foundation
SiteName=Student Foundation
SiteURL=studentfoundation.ez.fsu.edu
LoginPage=embedded
IndexPage=/content/view/full/58242
RootNodeDepth=2
DefaultPage=/content/view/full/58242
MetaDataArray[author]=Florida State University
MetaDataArray[keywords]=Florida State University, Student Foundation
MetaDataArray[description]=Florida State University, Student Foundation 
MetaDataArray[classification]=Florida State University, Student Foundation

[UserSettings]
RegistrationEmail=

[SiteAccessSettings]
RequireUserLogin=false
RelatedSiteAccessList[]=studentfoundation
RelatedSiteAccessList[]=fsu_admin
ShowHiddenNodes=false
PathPrefix=Student-Foundation

[DesignSettings]
SiteDesign=studentfoundation
AdditionalSiteDesignList[]=ezfind
AdditionalSiteDesignList[]=fsu-v01
AdditionalSiteDesignList[]=ezwebin
AdditionalSiteDesignList[]=base

[SearchSettings]
SearchEngine=ezsolr

*/ ?>