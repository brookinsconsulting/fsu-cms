<?php /* #?ini charset="utf-8"?

[InformationCollectionSettings]
EmailReceiver=

[SiteSettings]
SiteShortName=Office Of Greek Life
SiteName=Office Of Greek Life
SiteURL=greeklife.ez.fsu.edu
LoginPage=embedded
IndexPage=/content/view/full/61444
RootNodeDepth=2
DefaultPage=/content/view/full/61444
MetaDataArray[author]=Florida State University
MetaDataArray[keywords]=Florida State University, Office of Greek Life
MetaDataArray[description]=Florida State University, Office of Greek Life
MetaDataArray[classification]=Florida State University, Office of Greek Life 

[UserSettings]
RegistrationEmail=

[SiteAccessSettings]
RequireUserLogin=false
RelatedSiteAccessList[]=greek_life
RelatedSiteAccessList[]=fsu_admin
ShowHiddenNodes=false
PathPrefix=Office-of-Greek-Life

[DesignSettings]
SiteDesign=greek_life
AdditionalSiteDesignList[]=ezfind
AdditionalSiteDesignList[]=fsu-v01
AdditionalSiteDesignList[]=ezwebin
AdditionalSiteDesignList[]=base

[SearchSettings]
SearchEngine=ezsolr

*/ ?>