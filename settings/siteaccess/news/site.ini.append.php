<?php /* #?ini charset="utf-8"?

[InformationCollectionSettings]
EmailReceiver=

[SiteSettings]
SiteShortName=News
SiteName=Florida State University News 
SiteURL=news.ez.fsu.edu
LoginPage=embedded
IndexPage=/content/view/full/53006
RootNodeDepth=2
DefaultPage=/content/view/full/53006
MetaDataArray[author]=Florida State University News
MetaDataArray[keywords]=education, post-secondary florida state university, graduate school, career advancement, 
MetaDataArray[description]=Florida State University is a Carnegie Doctoral/Research 
MetaDataArray[classification]=Continuing Education, Professional Development, 

[UserSettings]
RegistrationEmail=

[SiteAccessSettings]
RequireUserLogin=false
RelatedSiteAccessList[]=news
RelatedSiteAccessList[]=fsu_admin
ShowHiddenNodes=false
PathPrefix=Florida-State-University-News

[DesignSettings]
SiteDesign=news
AdditionalSiteDesignList[]=ezfind
AdditionalSiteDesignList[]=fsu-v01
AdditionalSiteDesignList[]=ezwebin
AdditionalSiteDesignList[]=base

[SearchSettings]
SearchEngine=ezsolr

*/ ?>