<?php /* #?ini charset="utf-8"?

[InformationCollectionSettings]
EmailReceiver=

[SiteSettings]
SiteShortName=Student Affairs Office of Communications
SiteName=Student Affairs Office of Communications
SiteURL=communications.studentaffairs.fsu.edu
LoginPage=embedded
IndexPage=/content/view/full/44298
RootNodeDepth=2
DefaultPage=/content/view/full/44298
MetaDataArray[author]=Florida State University Student Affairs Communications
MetaDataArray[keywords]=Florida State University Student Affairs Communications 
MetaDataArray[description]=Florida State University Student Affairs Communications
MetaDataArray[classification]=Florida State University Student Affairs Communications

[UserSettings]
RegistrationEmail=

[SiteAccessSettings]
RequireUserLogin=false
RelatedSiteAccessList[]=sa_communications
RelatedSiteAccessList[]=fsu_admin
ShowHiddenNodes=false
PathPrefix=Student-Affairs-Office-of-Communications

[DesignSettings]
SiteDesign=sa_communications
AdditionalSiteDesignList[]=ezfind
AdditionalSiteDesignList[]=fsudesign
AdditionalSiteDesignList[]=ezwebin
AdditionalSiteDesignList[]=base

[SearchSettings]
SearchEngine=ezsolr

*/ ?>