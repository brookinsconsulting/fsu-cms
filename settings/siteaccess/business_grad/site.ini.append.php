<?php /* #?ini charset="utf-8"?

[InformationCollectionSettings]
EmailReceiver=

[SiteSettings]
SiteShortName=Business Graduate Program
SiteName=Florida State University College of Business Graduate Program
SiteURL=grad.cob.ezpublish2.fsu.edu
LoginPage=embedded
IndexPage=/content/view/full/8860
RootNodeDepth=2
DefaultPage=/content/view/full/8860
MetaDataArray[author]=Florida State University Business Graduage Program
MetaDataArray[keywords]=education, post-secondary florida state university, graduate school, career advancement, 
MetaDataArray[description]=Florida State University is a Carnegie Doctoral/Research 
MetaDataArray[classification]=Continuing Education, Professional Development, 

[UserSettings]
RegistrationEmail=

[SiteAccessSettings]
RequireUserLogin=false
RelatedSiteAccessList[]=business_grad
RelatedSiteAccessList[]=fsu_admin
ShowHiddenNodes=false
PathPrefix=College-of-Business-Graduate-Programs

[DesignSettings]
SiteDesign=business_grad
AdditionalSiteDesignList[]=fsu
AdditionalSiteDesignList[]=ezwebin
AdditionalSiteDesignList[]=base

*/ ?>