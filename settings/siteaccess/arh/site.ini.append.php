<?php /* #?ini charset="utf-8"?

[InformationCollectionSettings]
EmailReceiver=

[SiteSettings]
SiteShortName=Department of Art History
SiteName=Department of Art History 
SiteURL=arh.ezpublish2.fsu.edu
LoginPage=embedded
IndexPage=/content/view/full/47118
RootNodeDepth=2
DefaultPage=/content/view/full/47118
MetaDataArray[author]=Florida State University Department of Art History
MetaDataArray[keywords]=education, post-secondary florida state university, graduate school, career advancement, 
MetaDataArray[description]=Florida State University is a Carnegie Doctoral/Research 
MetaDataArray[classification]=Continuing Education, Professional Development, 

[UserSettings]
RegistrationEmail=

[SiteAccessSettings]
RequireUserLogin=false
RelatedSiteAccessList[]=arh
RelatedSiteAccessList[]=fsu_admin
ShowHiddenNodes=false
PathPrefix=Department-of-Art-History

[DesignSettings]
SiteDesign=arh
AdditionalSiteDesignList[]=ezfind
AdditionalSiteDesignList[]=fsu-v01
AdditionalSiteDesignList[]=ezwebin
AdditionalSiteDesignList[]=base

[SearchSettings]
SearchEngine=ezsolr

*/ ?>