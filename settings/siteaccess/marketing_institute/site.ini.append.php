<?php /* #?ini charset="utf-8"?

[InformationCollectionSettings]
EmailReceiver=

[SiteSettings]
SiteShortName=Marketing Institute
SiteName=The Marketing Institute 
SiteURL=tmi.cob.ezpublish2.fsu.edu
LoginPage=embedded
IndexPage=/content/view/full/42389
RootNodeDepth=2
DefaultPage=/content/view/full/42389
MetaDataArray[author]=Florida State University The Graduate School
MetaDataArray[keywords]=education, post-secondary florida state university, graduate school, career advancement, 
MetaDataArray[description]=Florida State University is a Carnegie Doctoral/Research 
MetaDataArray[classification]=Continuing Education, Professional Development, 

[UserSettings]
RegistrationEmail=

[SiteAccessSettings]
RequireUserLogin=false
RelatedSiteAccessList[]=marketing_institute
RelatedSiteAccessList[]=fsu_admin
ShowHiddenNodes=false
PathPrefix=Marketing-Institute

[DesignSettings]
SiteDesign=marketing_institute
AdditionalSiteDesignList[]=ezfind
AdditionalSiteDesignList[]=fsu-v01
AdditionalSiteDesignList[]=ezwebin
AdditionalSiteDesignList[]=base

[SearchSettings]
SearchEngine=ezsolr

*/ ?>