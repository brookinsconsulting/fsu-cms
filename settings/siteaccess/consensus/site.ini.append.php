<?php /* #?ini charset="utf-8"?

[InformationCollectionSettings]
EmailReceiver=

[SiteSettings]
SiteShortName=concensus
SiteName=Florida Conflict Resolution Consortium
SiteURL=consensus.ezpublish2.fsu.edu
LoginPage=embedded
IndexPage=/content/view/full/15136
RootNodeDepth=2
DefaultPage=/content/view/full/15136
MetaDataArray[author]=Florida Conflict Resolution Consortium
MetaDataArray[keywords]=education, post-secondary florida state university, graduate school, career advancement, 
MetaDataArray[description]=Florida State University is a Carnegie Doctoral/Research 
MetaDataArray[classification]=Continuing Education, Professional Development, 

[UserSettings]
RegistrationEmail=

[SiteAccessSettings]
RequireUserLogin=false
RelatedSiteAccessList[]=consensus
RelatedSiteAccessList[]=fsu_admin
ShowHiddenNodes=false
PathPrefix=Florida-Confict-Resolution-Consortium

[DesignSettings]
SiteDesign=consensus
AdditionalSiteDesignList[]=fsudesign
AdditionalSiteDesignList[]=ezwebin
AdditionalSiteDesignList[]=base

*/ ?>