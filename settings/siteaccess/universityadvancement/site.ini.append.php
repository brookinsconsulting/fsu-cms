<?php /* #?ini charset="utf-8"?

[ContentSettings]
StaticCache=enabled

[InformationCollectionSettings]
EmailReceiver=

[SiteSettings]
SiteShortName=Division of University Advancement
SiteName=Division of University Advancement
SiteURL=advancement.fsu.edu
LoginPage=embedded
IndexPage=/content/view/full/39981
RootNodeDepth=2
DefaultPage=/content/view/full/39981
MetaDataArray[author]=Florida State University Division of University Advancement
MetaDataArray[keywords]=education, post-secondary florida state university, Advancement, university advancement, 
MetaDataArray[description]=Florida State University is a Carnegie Doctoral/Research 
MetaDataArray[classification]=Continuing Education, Professional Development, 

[UserSettings]
RegistrationEmail=

[SiteAccessSettings]
RequireUserLogin=false
RelatedSiteAccessList[]=universityadvancement
RelatedSiteAccessList[]=fsu_admin
ShowHiddenNodes=false
PathPrefix=University-Advancement

[DesignSettings]
SiteDesign=universityadvancement
AdditionalSiteDesignList[]=fsudesign
AdditionalSiteDesignList[]=ezwebin
AdditionalSiteDesignList[]=base

*/ ?>
