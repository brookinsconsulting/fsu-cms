<?php /* #?ini charset="utf-8"?

[InformationCollectionSettings]
EmailReceiver=

[SiteSettings]
SiteShortName=FSU Panama
SiteName=FSU Panama
SiteURL=panama.ezpublish2.fsu.edu
LoginPage=embedded
IndexPage=/content/view/full/44062
RootNodeDepth=2
DefaultPage=/content/view/full/44062
MetaDataArray[author]=Florida State University Panama
MetaDataArray[keywords]=education, post-secondary florida state university, panama, international, 
MetaDataArray[description]=Florida State University is a Carnegie Doctoral/Research 
MetaDataArray[classification]=Continuing Education, Professional Development, 

[UserSettings]
RegistrationEmail=

[SiteAccessSettings]
RequireUserLogin=false
RelatedSiteAccessList[]=fsupanama
RelatedSiteAccessList[]=fsu_admin
ShowHiddenNodes=false
PathPrefix=FSU-Panama

[DesignSettings]
SiteDesign=fsupanama
AdditionalSiteDesignList[]=ezfind
AdditionalSiteDesignList[]=fsudesign
AdditionalSiteDesignList[]=ezwebin
AdditionalSiteDesignList[]=base

[SearchSettings]
SearchEngine=ezsolr

*/ ?>