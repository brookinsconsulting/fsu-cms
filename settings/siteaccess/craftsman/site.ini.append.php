<?php /* #?ini charset="utf-8"?

[InformationCollectionSettings]
EmailReceiver=

[SiteSettings]
SiteShortName=Master Craftsman Studio
SiteName=Florida State University Master Craftsman Studio
SiteURL=craft.ezpublish2.fsu.edu
LoginPage=embedded
IndexPage=/content/view/full/8858
RootNodeDepth=2
DefaultPage=/content/view/full/8858
MetaDataArray[author]=Florida State University Master Craftsman Studios
MetaDataArray[keywords]=education, post-secondary florida state university, graduate school, career advancement, 
MetaDataArray[description]=Florida State University is a Carnegie Doctoral/Research 
MetaDataArray[classification]=Continuing Education, Professional Development, 

[UserSettings]
RegistrationEmail=

[SiteAccessSettings]
RequireUserLogin=false
RelatedSiteAccessList[]=craftsman
RelatedSiteAccessList[]=fsu_admin
ShowHiddenNodes=false
PathPrefix=Master-Craftsman-Studios

[DesignSettings]
SiteDesign=craftsman
AdditionalSiteDesignList[]=fsudesign
AdditionalSiteDesignList[]=ezwebin
AdditionalSiteDesignList[]=base

*/ ?>