<?php /* #?ini charset="utf-8"?

[InformationCollectionSettings]
EmailReceiver=

[SiteSettings]
SiteShortName=Police Department
SiteName=Florida State University Police Department 
SiteURL=police.ezpublish2.fsu.edu
LoginPage=embedded
IndexPage=/content/view/full/6327
RootNodeDepth=2
DefaultPage=/content/view/full/6327
MetaDataArray[author]=Florida State University Police Department
MetaDataArray[keywords]=police department,florida state university, protectcion, emergency, safety, crime
MetaDataArray[description]=Florida State University Police Department 
MetaDataArray[classification]=Police Department

[UserSettings]
RegistrationEmail=

[SiteAccessSettings]
RequireUserLogin=false
RelatedSiteAccessList[]=police
RelatedSiteAccessList[]=fsu_admin
ShowHiddenNodes=false
PathPrefix=Police-Department

[DesignSettings]
SiteDesign=police
AdditionalSiteDesignList[]=fsudesign
AdditionalSiteDesignList[]=ezwebin
AdditionalSiteDesignList[]=base

*/ ?>