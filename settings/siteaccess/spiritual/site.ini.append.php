<?php /* #?ini charset="utf-8"?

[InformationCollectionSettings]
EmailReceiver=

[SiteSettings]
SiteShortName=FSU Spiritual Life Project
SiteName=FSU Spiritual Life Project 
SiteURL=spirituldsa.ez.fsu.edu
LoginPage=embedded
IndexPage=/content/view/full/57927
RootNodeDepth=2
DefaultPage=/content/view/full/57927
MetaDataArray[author]=FSU Spiritual Life Project
MetaDataArray[keywords]=education, post-secondary florida state university, spiritual, project, 
MetaDataArray[description]=Florida State University is a Carnegie Doctoral/Research 
MetaDataArray[classification]=Continuing Education, Professional Development, 

[UserSettings]
RegistrationEmail=

[SiteAccessSettings]
RequireUserLogin=false
RelatedSiteAccessList[]=spiritual
RelatedSiteAccessList[]=fsu_admin
ShowHiddenNodes=false
PathPrefix=FSU-Spiritual-Life-Project

[DesignSettings]
SiteDesign=spiritual
AdditionalSiteDesignList[]=ezfind
AdditionalSiteDesignList[]=fsu-v01
AdditionalSiteDesignList[]=ezwebin
AdditionalSiteDesignList[]=base

[SearchSettings]
SearchEngine=ezsolr

*/ ?>