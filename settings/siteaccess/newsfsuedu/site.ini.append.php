<?php /* #?ini charset="utf-8"?

[ContentSettings]
StaticCache=enabled

[InformationCollectionSettings]
EmailReceiver=

[SiteSettings]
SiteName=Florida State 24/7
SiteURL=news.fsu.edu
LoginPage=embedded
IndexPage=/content/view/full/3132/
RootNodeDepth=2
DefaultPage=/content/view/full/3132/
MetaDataArray[author]=Florida State University News
MetaDataArray[keywords]=fsu, news, florida state, campus
SiteShortName=Florida State 24/7
SiteRootNodeID=3132

[UserSettings]
RegistrationEmail=

[SiteAccessSettings]
RequireUserLogin=false
RelatedSiteAccessList[]=fsu_com
#RelatedSiteAccessList[]=gradstudies
RelatedSiteAccessList[]=fsu_admin
ShowHiddenNodes=false
PathPrefix=FSU_com_dev

[DesignSettings]
SiteDesign=fsucomdev
AdditionalSiteDesignList[]=framework
AdditionalSiteDesignList[]=tcgallery
AdditionalSiteDesignList[]=fsucom
AdditionalSiteDesignList[]=fsu
AdditionalSiteDesignList[]=ezwebin
AdditionalSiteDesignList[]=base

[DebugSettings]
DebugOutput=enabled
DebugByUser=disabled
DebugRedirection=disabled

[TemplateSettings]
Debug=disabled
ShowXHTMLCode=disabled
ShowUsedTemplates=enabled

[DatabaseSettings]
SQLOutput=disabled
*/ ?>
