<?php /* #?ini charset="utf-8"?

[InformationCollectionSettings]
EmailReceiver=

[SiteSettings]
SiteShortName=Faculty Handbook
SiteName=Faculty Handbook
SiteURL=facultyhandbook.ezpublish2.fsu.edu
LoginPage=embedded
IndexPage=/content/view/full/20764
RootNodeDepth=2
DefaultPage=/content/view/full/20764
MetaDataArray[author]=Florida State University Faculty Handbook
MetaDataArray[keywords]=Faculty Handbook, florida state university
MetaDataArray[description]=Florida State University Faculty Handbook
MetaDataArray[classification]=Faculty Handbook Florida State University

[UserSettings]
RegistrationEmail=

[SiteAccessSettings]
RequireUserLogin=false
RelatedSiteAccessList[]=faculty_handbook
RelatedSiteAccessList[]=fsu_admin
ShowHiddenNodes=false
PathPrefix=Faculty-Handbook

[DesignSettings]
SiteDesign=faculty_handbook
AdditionalSiteDesignList[]=fsudesign
AdditionalSiteDesignList[]=ezwebin
AdditionalSiteDesignList[]=base

*/ ?>