<?php /* #?ini charset="utf-8"?

[InformationCollectionSettings]
EmailReceiver=

[SiteSettings]
SiteShortName=HPS
SiteName=History & Philosophy of Science
SiteURL=ez.fsu.edu
LoginPage=embedded
IndexPage=/content/view/full/60476
RootNodeDepth=2
DefaultPage=/content/view/full/60476
MetaDataArray[author]=Florida State University
MetaDataArray[keywords]=education, post-secondary florida state university, program, history, philosophy, science, religion
MetaDataArray[description]=Florida State University is a Carnegie Doctoral/Research 
MetaDataArray[classification]=Continuing Education, Professional Development, 

[UserSettings]
RegistrationEmail=

[SiteAccessSettings]
RequireUserLogin=false
RelatedSiteAccessList[]=hps
RelatedSiteAccessList[]=fsu_admin
ShowHiddenNodes=false
PathPrefix=History-and-Philosophy-of-Science

[DesignSettings]
SiteDesign=hps
AdditionalSiteDesignList[]=ezfind
AdditionalSiteDesignList[]=fsu-v01
AdditionalSiteDesignList[]=ezwebin
AdditionalSiteDesignList[]=base

[SearchSettings]
SearchEngine=ezsolr

*/ ?>