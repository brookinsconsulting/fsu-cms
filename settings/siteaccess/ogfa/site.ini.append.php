<?php /* #?ini charset="utf-8"?

[InformationCollectionSettings]
EmailReceiver=

[SiteSettings]
SiteShortName=Graduate Fellowships and Awards
SiteName=Florida State University Graduate Fellowships and Awards
SiteURL=ogfa.fsu.edu
LoginPage=embedded
IndexPage=/content/view/full/15132
RootNodeDepth=2
DefaultPage=/content/view/full/15132
MetaDataArray[author]=Florida State University Graduate Fellowships and Awards
MetaDataArray[keywords]=education, post-secondary florida state university, graduate school, career advancement, 
MetaDataArray[description]=Florida State University is a Carnegie Doctoral/Research 
MetaDataArray[classification]=Continuing Education, Professional Development, 

[UserSettings]
RegistrationEmail=

[SiteAccessSettings]
RequireUserLogin=false
RelatedSiteAccessList[]=ogfa
RelatedSiteAccessList[]=fsu_admin
ShowHiddenNodes=false
PathPrefix=Office-of-Graduate-Fellowships-and-Awards

[DesignSettings]
SiteDesign=ogfa
AdditionalSiteDesignList[]=fsudesign
AdditionalSiteDesignList[]=ezwebin
AdditionalSiteDesignList[]=base

*/ ?>