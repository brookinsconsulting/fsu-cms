<?php /* #?ini charset="utf-8"?

[InformationCollectionSettings]
EmailReceiver=

[SiteSettings]
SiteShortName=Pepper Institute on Aging and Public Policy
SiteName=Pepper Institute on Aging and Public Policy
SiteURL=pepperinstitute.ezpublish2.fsu.edu
LoginPage=embedded
IndexPage=/content/view/full/38300
RootNodeDepth=2
DefaultPage=/content/view/full/38300
MetaDataArray[author]=Pepper Institute on Aging and Public Policy
MetaDataArray[keywords]=education, post-secondary florida state university, pepper institute, aging, public policy 
MetaDataArray[description]=Florida State University is a Carnegie Doctoral/Research 
MetaDataArray[classification]=Continuing Education, Professional Development, 

[UserSettings]
RegistrationEmail=

[SiteAccessSettings]
RequireUserLogin=false
RelatedSiteAccessList[]=pepper
RelatedSiteAccessList[]=fsu_admin
ShowHiddenNodes=false
PathPrefix=Pepper-Institute-on-Aging-and-Public-Policy

[DesignSettings]
SiteDesign=pepper
AdditionalSiteDesignList[]=fsudesign
AdditionalSiteDesignList[]=ezwebin
AdditionalSiteDesignList[]=base

*/ ?>