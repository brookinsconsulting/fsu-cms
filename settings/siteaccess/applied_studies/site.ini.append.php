<?php /* #?ini charset="utf-8"?

[InformationCollectionSettings]
EmailReceiver=

[SiteSettings]
SiteShortName=College of Applied Studies
SiteName=College of Applied Studies 
SiteURL=appliedstudies.ezpublish2.fsu.edu
LoginPage=embedded
IndexPage=/content/view/full/38006
RootNodeDepth=2
DefaultPage=/content/view/full/38006
MetaDataArray[author]=Florida State University Paname City College of Applied Studies
MetaDataArray[keywords]=education, Florida State University, Paname City College of Applied Studies
MetaDataArray[description]=Florida State University, Paname City College of Applied Studies
MetaDataArray[classification]=Florida State University, Paname City College of Applied Studies

[UserSettings]
RegistrationEmail=

[SiteAccessSettings]
RequireUserLogin=false
RelatedSiteAccessList[]=applied_studies
RelatedSiteAccessList[]=fsu_admin
ShowHiddenNodes=false
PathPrefix=College-of-Applied-Studies

[DesignSettings]
SiteDesign=applied_studies
AdditionalSiteDesignList[]=ezfind
AdditionalSiteDesignList[]=fsu-v01
AdditionalSiteDesignList[]=ezwebin
AdditionalSiteDesignList[]=base

*/ ?>