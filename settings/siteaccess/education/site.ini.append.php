<?php /* #?ini charset="utf-8"?

[InformationCollectionSettings]
EmailReceiver=

[SiteSettings]
SiteShortName=Education
SiteName=College of Education
SiteURL=coe.fsu.edu
LoginPage=embedded
IndexPage=/content/view/full/2600
RootNodeDepth=2
DefaultPage=/content/view/full/2600
MetaDataArray[author]=College of Education
MetaDataArray[keywords]=education, post-secondary florida state university, graduate school, career advancement, 
MetaDataArray[description]=Florida State University is a Carnegie Doctoral/Research 
MetaDataArray[classification]=Continuing Education, Professional Development, 

[UserSettings]
RegistrationEmail=

[SiteAccessSettings]
RequireUserLogin=false
RelatedSiteAccessList[]=education
RelatedSiteAccessList[]=fsu_admin
ShowHiddenNodes=false
PathPrefix=Education

[DesignSettings]
SiteDesign=education
AdditionalSiteDesignList[]=fsu-v01
AdditionalSiteDesignList[]=ezwebin
AdditionalSiteDesignList[]=base

*/ ?>