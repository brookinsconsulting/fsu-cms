<?php /* #?ini charset="utf-8"?

[InformationCollectionSettings]
EmailReceiver=

[SiteSettings]
SiteShortName= The Jim Moran Institute
SiteName=The Jim Moran Institute for Global Entrepreneurship
SiteURL=jmi.fsu.edu
LoginPage=embedded
IndexPage=/content/view/full/23497
RootNodeDepth=2
DefaultPage=/content/view/full/23497
MetaDataArray[author]=Florida State University College of Business Jim Moran Institute
MetaDataArray[keywords]=Florida State University College of Business Jim Moran Institute 
MetaDataArray[description]=Florida State University College of Business Jim Moran Institute 
MetaDataArray[classification]=Florida State University College of Business Jim Moran Institute

[UserSettings]
RegistrationEmail=

[SiteAccessSettings]
RequireUserLogin=false
RelatedSiteAccessList[]=jmi
RelatedSiteAccessList[]=fsu_admin
ShowHiddenNodes=false
PathPrefix=The-Jim-Moran-Institute-for-Global-Entrepreneurship

[DesignSettings]
SiteDesign=jmi
AdditionalSiteDesignList[]=fsudesign
AdditionalSiteDesignList[]=ezwebin
AdditionalSiteDesignList[]=base

[DebugSettings]
DebugOutput=enabled
DebugRedirection=disabled

[TemplateSettings]
Debug=disabled
ShowXHTMLCode=disabled
ShowUsedTemplates=enabled

[DatabaseSettings]
SQLOutput=disabled
*/ ?>