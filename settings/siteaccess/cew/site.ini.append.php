<?php /* #?ini charset="utf-8"?

[InformationCollectionSettings]
EmailReceiver=

[SiteSettings]
SiteShortName=FSU Center for Everyday Writing
SiteName=FSU Center for Everyday Writing
SiteURL=cewenglish.ez.fsu.edu
LoginPage=embedded
IndexPage=/content/view/full/82660
RootNodeDepth=2
DefaultPage=/content/view/full/82660
MetaDataArray[author]=Florida State University, FSU Center for Everyday Writing, English Department, College of
Arts and Sciences
MetaDataArray[keywords]=Florida State University, FSU Center for Everyday Writing, English Department, College of
Arts and Sciences 
MetaDataArray[description]=Florida State University, FSU Center for Everyday Writing, English Department, College of
Arts and Sciences 
MetaDataArray[classification]=Florida State University, FSU Center for Everyday Writing, English Department, College of
Arts and Sciences 

[UserSettings]
RegistrationEmail=

[SiteAccessSettings]
RequireUserLogin=false
RelatedSiteAccessList[]=cew
RelatedSiteAccessList[]=fsu_admin
ShowHiddenNodes=false
PathPrefix=FSU-Center-for-Everyday-Writing

[DesignSettings]
SiteDesign=cew
AdditionalSiteDesignList[]=ezfind
AdditionalSiteDesignList[]=fsu-v01
AdditionalSiteDesignList[]=ezwebin
AdditionalSiteDesignList[]=base

[SearchSettings]
SearchEngine=ezsolr

*/ ?>