<?php /* #?ini charset="utf-8"?

[InformationCollectionSettings]
EmailReceiver=

[SiteSettings]
SiteShortName=BOT
SiteName=Board of Trustees
SiteURL=ez.fsu.edu
LoginPage=embedded
IndexPage=/content/view/full/66122
RootNodeDepth=2
DefaultPage=/content/view/full/66122
MetaDataArray[author]=Florida State University
MetaDataArray[keywords]=education, post-secondary florida state university, 
MetaDataArray[description]=Florida State University is a Carnegie Doctoral/Research 
MetaDataArray[classification]=Continuing Education, Professional Development, 

[UserSettings]
RegistrationEmail=

[SiteAccessSettings]
RequireUserLogin=false
RelatedSiteAccessList[]=bot
RelatedSiteAccessList[]=fsu_admin
ShowHiddenNodes=false
PathPrefix=Board-of-Trustees

[DesignSettings]
SiteDesign=bot
AdditionalSiteDesignList[]=ezfind
AdditionalSiteDesignList[]=fsu-v01
AdditionalSiteDesignList[]=ezwebin
AdditionalSiteDesignList[]=base

[SearchSettings]
SearchEngine=ezsolr

*/ ?>