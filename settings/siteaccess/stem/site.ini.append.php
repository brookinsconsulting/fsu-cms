<?php /* #?ini charset="utf-8"?

[InformationCollectionSettings]
EmailReceiver=

[SiteSettings]
SiteShortName=STEM
SiteName=Florida State University Science, Technology, Engineering and Mathematics (STEM) 
SiteURL=stem.fsu.edu
LoginPage=embedded
IndexPage=/content/view/full/8861
RootNodeDepth=2
DefaultPage=/content/view/full/8861
MetaDataArray[author]=Florida State University Science, Technology, Engineering and Mathematics (STEM) 
MetaDataArray[keywords]=education, post-secondary florida state university, graduate school, career advancement, Science, Technology, Engineering and Mathematics (STEM) 
MetaDataArray[description]=Florida State University is a Carnegie Doctoral/Research 
MetaDataArray[classification]=Continuing Education, Professional Development, Science, Technology, Engineering and Mathematics (STEM) 

[UserSettings]
RegistrationEmail=

[SiteAccessSettings]
RequireUserLogin=false
RelatedSiteAccessList[]=stem
RelatedSiteAccessList[]=fsu_admin
ShowHiddenNodes=false
PathPrefix=STEM

[DesignSettings]
SiteDesign=stem
AdditionalSiteDesignList[]=fsudesign
AdditionalSiteDesignList[]=ezwebin
AdditionalSiteDesignList[]=base

*/ ?>