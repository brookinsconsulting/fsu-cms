<?php /* #?ini charset="utf-8"?

[ContentSettings]
StaticCache=enabled

[InformationCollectionSettings]
EmailReceiver=

[SiteSettings]
SiteName=Communicators Network
SiteURL=communicatorsnetwork.ez.fsu.edu
LoginPage=embedded
IndexPage=/content/view/full/71628/
RootNodeDepth=2
DefaultPage=/content/view/full/71628/
MetaDataArray[author]=Communicators Network
MetaDataArray[keywords]=fsu, communicators, network, florida state, campus
SiteShortName=Communicators Network
SiteRootNodeID=71628

[UserSettings]
RegistrationEmail=

[SiteAccessSettings]
RequireUserLogin=false
RelatedSiteAccessList[]=communicators
#RelatedSiteAccessList[]=gradstudies
RelatedSiteAccessList[]=fsu_admin
ShowHiddenNodes=false
PathPrefix=Communicators-Network

[DesignSettings]
SiteDesign=communicators
AdditionalSiteDesignList[]=framework
AdditionalSiteDesignList[]=tcgallery
AdditionalSiteDesignList[]=fsucom
AdditionalSiteDesignList[]=fsu
AdditionalSiteDesignList[]=ezwebin
AdditionalSiteDesignList[]=base

[DebugSettings]
DebugOutput=enabled
DebugByUser=disabled
DebugRedirection=disabled

[TemplateSettings]
Debug=disabled
ShowXHTMLCode=disabled
ShowUsedTemplates=enabled

[DatabaseSettings]
SQLOutput=disabled
*/ ?>
