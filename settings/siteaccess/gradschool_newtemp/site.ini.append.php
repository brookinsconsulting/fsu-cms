<?php /* #?ini charset="utf-8"?

[InformationCollectionSettings]
EmailReceiver=

[SiteSettings]
SiteShortName=The Graduate School
SiteName=The Graduate School 
SiteURL=gradstudies.ezpublish2.fsu.edu
LoginPage=embedded
IndexPage=/content/view/full/1058
RootNodeDepth=2
DefaultPage=/content/view/full/1058
MetaDataArray[author]=Florida State University The Graduate School
MetaDataArray[keywords]=education, post-secondary florida state university, graduate school, career advancement, 
MetaDataArray[description]=Florida State University is a Carnegie Doctoral/Research 
MetaDataArray[classification]=Continuing Education, Professional Development, 

[UserSettings]
RegistrationEmail=

[SiteAccessSettings]
RequireUserLogin=false
RelatedSiteAccessList[]=gradschool_newtemp
RelatedSiteAccessList[]=fsu_admin
ShowHiddenNodes=false
PathPrefix=The-Graduate-School

[DesignSettings]
SiteDesign=gradschool_newtemp
AdditionalSiteDesignList[]=ezfind
AdditionalSiteDesignList[]=fsu-v02
AdditionalSiteDesignList[]=ezwebin
AdditionalSiteDesignList[]=base

[SearchSettings]
SearchEngine=ezsolr

*/ ?>