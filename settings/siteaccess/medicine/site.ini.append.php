<?php /* #?ini charset="utf-8"?

[InformationCollectionSettings]
EmailReceiver=

[SiteSettings]
SiteShortName=College of Medicine
SiteName=Florida State University College of Medicine
SiteURL=med.ezpublish2.fsu.edu
LoginPage=embedded
IndexPage=/content/view/full/15135
RootNodeDepth=2
DefaultPage=/content/view/full/15135
MetaDataArray[author]=Florida State University Collelge of Medicine
MetaDataArray[keywords]=education, post-secondary florida state university, graduate school, career advancement, 
MetaDataArray[description]=Florida State University is a Carnegie Doctoral/Research 
MetaDataArray[classification]=Continuing Education, Professional Development, 

[UserSettings]
RegistrationEmail=

[SiteAccessSettings]
RequireUserLogin=false
RelatedSiteAccessList[]=medicine
RelatedSiteAccessList[]=fsu_admin
ShowHiddenNodes=false
PathPrefix=College-of-Medicine

[DesignSettings]
SiteDesign=medicine
AdditionalSiteDesignList[]=fsudesign
AdditionalSiteDesignList[]=ezwebin
AdditionalSiteDesignList[]=base

*/ ?>