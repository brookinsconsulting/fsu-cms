<?php /* #?ini charset="utf-8"?

[InformationCollectionSettings]
EmailReceiver=

[Session]
SessionNamePerSiteAccess=disabled

[SiteSettings]
SiteName=College of Musick-Music Therapy
SiteURL=therapy.music.ezpublish2.fsu.edu
LoginPage=embedded
IndexPage=/content/view/full/6080/
RootNodeDepth=2
DefaultPage=/content/view/full/6080/
MetaDataArray[author]=College of Music-Music Therapy
MetaDataArray[keywords]=school, florida, education, music therapy, nicu, college

[UserSettings]
RegistrationEmail=

[SiteAccessSettings]
RequireUserLogin=false
RelatedSiteAccessList[]=nicu-mt
RelatedSiteAccessList[]=fsu_admin
ShowHiddenNodes=false
PathPrefix=College-of-Music-Music-Therapy
PathPrefixExclude[]
PathPrefixExclude[]=Media
PathPrefixExclude[]=Users
PathPrefixExclude[]=Flash-Rotation-Articles
PathPrefixExclude[]=Test-Area

[DesignSettings]
SiteDesign=nicu-mt
AdditionalSiteDesignList[]=fsudesign
AdditionalSiteDesignList[]=ezwebin
AdditionalSiteDesignList[]=base

[RegionalSettings]
Locale=eng-US
ContentObjectLocale=eng-US
ShowUntranslatedObjects=disabled
SiteLanguageList[]=eng-US
TextTranslation=disabled

[FileSettings]
VarDir=var/ezwebin_site

[ContentSettings]
TranslationList=

[MailSettings]
AdminEmail=web@ucs.fsu.edu
EmailSender=

[DebugSettings]
DebugOutput=enabled
DebugByUser=enabled
DebugRedirection=disabled
DebugUserIDList[]=195
DebugUserIDList[]=14

[TemplateSettings]
Debug=disabled
ShowXHTMLCode=enabled
ShowUsedTemplates=enabled

[DatabaseSettings]
DatabaseImplementation=ezmysql
Server=localhost
Port=
User=root
Password=t6ntnimB
Database=fsu_livedata
Charset=
Socket=disabled
SQLOutput=disabled
*/ ?>
