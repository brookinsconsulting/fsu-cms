<?php /* #?ini charset="utf-8"?

[InformationCollectionSettings]
EmailReceiver=

[SiteSettings]
SiteShortName=Earth, Ocean and Atmospheric Science 
SiteName=Earth, Ocean and Atmospheric Science 
SiteURL=eoas.fsu.edu
LoginPage=embedded
IndexPage=/content/view/full/16821
RootNodeDepth=2
DefaultPage=/content/view/full/16821
MetaDataArray[author]=Florida State University Earth, Ocean and Atmospheric Science  
MetaDataArray[keywords]=Florida State University, Atmospheric, Earth & Oceanographic Science
MetaDataArray[description]=Florida State University, Atmospheric, Earth & Oceanographic Science 
MetaDataArray[classification]=Florida State University, Atmospheric, Earth & Oceanographic Science

[UserSettings]
RegistrationEmail=

[SiteAccessSettings]
RequireUserLogin=false
RelatedSiteAccessList[]=eoas
RelatedSiteAccessList[]=fsu_admin
ShowHiddenNodes=false
PathPrefix=Earth-Ocean-and-Atmospheric-Science 

[DesignSettings]
SiteDesign=eoas
AdditionalSiteDesignList[]=fsudesign
AdditionalSiteDesignList[]=ezwebin
AdditionalSiteDesignList[]=base

[DebugSettings]
DebugOutput=enabled
DebugRedirection=disabled

[TemplateSettings]
Debug=disabled
ShowXHTMLCode=disabled
ShowUsedTemplates=enabled

[DatabaseSettings]
SQLOutput=disabled
*/ ?>