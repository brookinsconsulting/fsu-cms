<?php /* #?ini charset="utf-8"?

[InformationCollectionSettings]
EmailReceiver=

[SiteSettings]
SiteShortName=Sustainable Campus
SiteName=Sustainable Campus
SiteURL=sustainablecampus.ez.fsu.edu
LoginPage=embedded
IndexPage=/content/view/full/84291
RootNodeDepth=2
DefaultPage=/content/view/full/84291
MetaDataArray[author]=Florida State University, Sustainable Campus
MetaDataArray[keywords]=Florida State University, Sustainable Campus 
MetaDataArray[description]=Florida State University, Sustainable Campus 
MetaDataArray[classification]=Florida State University, Sustainable Campus 

[UserSettings]
RegistrationEmail=

[SiteAccessSettings]
RequireUserLogin=false
RelatedSiteAccessList[]=sustainabe
RelatedSiteAccessList[]=fsu_admin
ShowHiddenNodes=false
PathPrefix=Sustainable-Campus

[DesignSettings]
SiteDesign=sustainabe
AdditionalSiteDesignList[]=ezfind
AdditionalSiteDesignList[]=fsu-v01
AdditionalSiteDesignList[]=ezwebin
AdditionalSiteDesignList[]=base

[SearchSettings]
SearchEngine=ezsolr

*/ ?>