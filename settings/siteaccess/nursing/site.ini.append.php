<?php /* #?ini charset="utf-8"?

[InformationCollectionSettings]
EmailReceiver=

[SiteSettings]
SiteShortName=College of Nursing
SiteName=Florida State University College of Nursing
SiteURL=nursing.ezpublish2.fsu.edu
LoginPage=embedded
IndexPage=/content/view/full/14047
RootNodeDepth=2
DefaultPage=/content/view/full/14047
MetaDataArray[author]=Florida State University College of Nursing
MetaDataArray[keywords]=education, post-secondary florida state university, graduate school, career advancement, 
MetaDataArray[description]=Florida State University is a Carnegie Doctoral/Research 
MetaDataArray[classification]=Continuing Education, Professional Development, 

[UserSettings]
RegistrationEmail=

[SiteAccessSettings]
RequireUserLogin=false
RelatedSiteAccessList[]=nursing
RelatedSiteAccessList[]=fsu_admin
ShowHiddenNodes=false
PathPrefix=College-of-Nursing

[DesignSettings]
SiteDesign=nursing
AdditionalSiteDesignList[]=fsu-v01
AdditionalSiteDesignList[]=ezwebin
AdditionalSiteDesignList[]=base

*/ ?>