<?php /* #?ini charset="utf-8"?

[InformationCollectionSettings]
EmailReceiver=

[SiteSettings]
SiteShortName=ITS
SiteName=Information Technology Services 
SiteURL=its.fsu.edu
LoginPage=embedded
IndexPage=/content/view/full/8857
RootNodeDepth=2
DefaultPage=/content/view/full/8857
MetaDataArray[author]=Information Technology Services 
MetaDataArray[keywords]=education, post-secondary florida state university, graduate school, career advancement, 
MetaDataArray[description]=Florida State University is a Carnegie Doctoral/Research 
MetaDataArray[classification]=Continuing Education, Professional Development, 

[UserSettings]
RegistrationEmail=

[SiteAccessSettings]
RequireUserLogin=false
RelatedSiteAccessList[]=oti
RelatedSiteAccessList[]=fsu_admin
ShowHiddenNodes=false
PathPrefix=Information-Technology-Services

[DesignSettings]
SiteDesign=oti
AdditionalSiteDesignList[]=fsudesign
AdditionalSiteDesignList[]=ezwebin
AdditionalSiteDesignList[]=base

[DebugSettings]
DebugOutput=enabled
DebugRedirection=disabled

[TemplateSettings]
Debug=disabled
ShowXHTMLCode=enabled
ShowUsedTemplates=enabled

[DatabaseSettings]
SQLOutput=disabled

[SearchSettings]
SearchEngine=ezsolr
*/ ?>