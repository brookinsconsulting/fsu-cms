<?php /* #?ini charset="utf-8"?

[InformationCollectionSettings]
EmailReceiver=

[SiteSettings]
SiteShortName=Computing Security
SiteName=OTI Computing Security
SiteURL=security.ezpublish2.fsu.edu
LoginPage=embedded
IndexPage=/content/view/full/15133
RootNodeDepth=2
DefaultPage=/content/view/full/15133
MetaDataArray[author]=Computing Security
MetaDataArray[keywords]=education, post-secondary florida state university, graduate school, career advancement, 
MetaDataArray[description]=Florida State University is a Carnegie Doctoral/Research 
MetaDataArray[classification]=Continuing Education, Professional Development, 

[UserSettings]
RegistrationEmail=

[SiteAccessSettings]
RequireUserLogin=false
RelatedSiteAccessList[]=itsecurity
RelatedSiteAccessList[]=fsu_admin
ShowHiddenNodes=false
PathPrefix=Computing-Security

[DesignSettings]
SiteDesign=itsecurity
AdditionalSiteDesignList[]=fsudesign
AdditionalSiteDesignList[]=ezwebin
AdditionalSiteDesignList[]=base

*/ ?>