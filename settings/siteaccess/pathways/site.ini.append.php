<?php /* #?ini charset="utf-8"?

[InformationCollectionSettings]
EmailReceiver=

[SiteSettings]
SiteShortName=Pathways
SiteName=Pathways of Excellence
SiteURL=pathways.fsu.edu
LoginPage=embedded
IndexPage=/content/view/full/2203/
RootNodeDepth=2
DefaultPage=/content/view/full/2203/
MetaDataArray[author]=Pathways of Excellence
MetaDataArray[keywords]=school, florida, education

[UserSettings]
RegistrationEmail=

[SiteAccessSettings]
RequireUserLogin=false
RelatedSiteAccessList[]=pathways
RelatedSiteAccessList[]=fsu_admin
ShowHiddenNodes=false
PathPrefix=Pathways-of-Excellence

[DesignSettings]
SiteDesign=pathways
AdditionalSiteDesignList[]=fsu-v01
AdditionalSiteDesignList[]=ezwebin
AdditionalSiteDesignList[]=base

*/ ?>
