<?php /* #?ini charset="utf-8"?

[InformationCollectionSettings]
EmailReceiver=

[SiteSettings]
SiteShortName=Student Affairs Office of Research and Assessment
SiteName=Student Affairs Office of Research and Assessment
SiteURL=research.studentaffairs.fsu.edu
LoginPage=embedded
IndexPage=/content/view/full/40937
RootNodeDepth=2
DefaultPage=/content/view/full/40937
MetaDataArray[author]=Florida State University Student Affairs Office of Research and Assessment
MetaDataArray[keywords]=Florida State University Student Affairs Office of Research and Assessment
MetaDataArray[description]=Florida State University Student Affairs Office of Research and Assessment
MetaDataArray[classification]=Florida State University Student Affairs Office of Research and Assessment

[UserSettings]
RegistrationEmail=

[SiteAccessSettings]
RequireUserLogin=false
RelatedSiteAccessList[]=sa_research
RelatedSiteAccessList[]=fsu_admin
ShowHiddenNodes=false
PathPrefix=Student-Affairs-Office-of-Research-and-Assessment

[DesignSettings]
SiteDesign=sa_research
AdditionalSiteDesignList[]=ezfind
AdditionalSiteDesignList[]=fsu-v01
AdditionalSiteDesignList[]=ezwebin
AdditionalSiteDesignList[]=base

[SearchSettings]
SearchEngine=ezsolr

*/ ?>