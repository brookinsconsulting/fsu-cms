<?php /* #?ini charset="utf-8"?

[InformationCollectionSettings]
EmailReceiver=

[SiteSettings]
SiteShortName=Service Center
SiteName=FSU Service Center
SiteURL=ez.fsu.edu
LoginPage=embedded
IndexPage=/content/view/full/62281
RootNodeDepth=2
DefaultPage=/content/view/full/62281
MetaDataArray[author]=Florida State University FSU Service Center
MetaDataArray[keywords]=education, post-secondary florida state university, FSU Service Center
MetaDataArray[description]=Florida State University is a Carnegie Doctoral/Research 
MetaDataArray[classification]=Continuing Education, Professional Development, 

[UserSettings]
RegistrationEmail=

[SiteAccessSettings]
RequireUserLogin=false
RelatedSiteAccessList[]=usc
RelatedSiteAccessList[]=fsu_admin
ShowHiddenNodes=false
PathPrefix=FSU-Service-Center

[DesignSettings]
SiteDesign=usc
AdditionalSiteDesignList[]=ezfind
AdditionalSiteDesignList[]=fsudesign
AdditionalSiteDesignList[]=ezwebin
AdditionalSiteDesignList[]=base

[SearchSettings]
SearchEngine=ezsolr

*/ ?>