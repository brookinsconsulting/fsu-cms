<?php /* #?ini charset="utf-8"?

[InformationCollectionSettings]
EmailReceiver=

[SiteSettings]
SiteShortName=Office of the President
SiteName=Office of the President
SiteURL=president.ez.fsu.edu
LoginPage=embedded
IndexPage=/content/view/full/83115
RootNodeDepth=2
DefaultPage=/content/view/full/83115
MetaDataArray[author]=Florida State University Office of the President
MetaDataArray[keywords]=Florida State University Office of the President 
MetaDataArray[description]=Florida State University Office of the President 
MetaDataArray[classification]=Florida State University Office of the President

[UserSettings]
RegistrationEmail=

[SiteAccessSettings]
RequireUserLogin=false
RelatedSiteAccessList[]=president
RelatedSiteAccessList[]=fsu_admin
ShowHiddenNodes=false
PathPrefix=Office-of-the-President

[DesignSettings]
SiteDesign=president
AdditionalSiteDesignList[]=ezfind
AdditionalSiteDesignList[]=fsu-v01
AdditionalSiteDesignList[]=ezwebin
AdditionalSiteDesignList[]=base

[SearchSettings]
SearchEngine=ezsolr

*/ ?>