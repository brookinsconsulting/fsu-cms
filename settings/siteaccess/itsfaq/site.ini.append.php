<?php /* #?ini charset="utf-8"?

[InformationCollectionSettings]
EmailReceiver=

[SiteSettings]
SiteShortName=ITS FAQ
SiteName=ITS Frequently Asked Questions
SiteURL=faqits.ez.fsu.edu
LoginPage=embedded
IndexPage=/content/view/full/58345
RootNodeDepth=2
DefaultPage=/content/view/full/58345
MetaDataArray[author]=Florida State University
MetaDataArray[keywords]=education, post-secondary florida state university, 
MetaDataArray[description]=Florida State University is a Carnegie Doctoral/Research 
MetaDataArray[classification]=Continuing Education, Professional Development, 

[UserSettings]
RegistrationEmail=

[SiteAccessSettings]
RequireUserLogin=false
RelatedSiteAccessList[]=itsfaq
RelatedSiteAccessList[]=fsu_admin
ShowHiddenNodes=false
PathPrefix=ITS-Knowledge-Base

[DesignSettings]
SiteDesign=itsfaq
AdditionalSiteDesignList[]=ezfind
AdditionalSiteDesignList[]=fsudesign
AdditionalSiteDesignList[]=ezwebin
AdditionalSiteDesignList[]=base

[SearchSettings]
SearchEngine=ezsolr

*/ ?>