<?php /* #?ini charset="utf-8"?

[InformationCollectionSettings]
EmailReceiver=

[SiteSettings]
SiteShortName=Dean of Students Department
SiteName=Dean of Students Department
SiteURL=deanofstudents.ez.fsu.edu
LoginPage=embedded
IndexPage=/content/view/full/59595
RootNodeDepth=2
DefaultPage=/content/view/full/59595
MetaDataArray[author]=Florida State University
MetaDataArray[keywords]=Dean of Students 
MetaDataArray[description]=Dean of Students 
MetaDataArray[classification]=Dean of Students 

[UserSettings]
RegistrationEmail=

[SiteAccessSettings]
RequireUserLogin=false
RelatedSiteAccessList[]=dean_students
RelatedSiteAccessList[]=fsu_admin
ShowHiddenNodes=false
PathPrefix=Dean-of-Students-Department

[DesignSettings]
SiteDesign=dean_students
AdditionalSiteDesignList[]=ezfind
AdditionalSiteDesignList[]=fsu-v01
AdditionalSiteDesignList[]=ezwebin
AdditionalSiteDesignList[]=base

[SearchSettings]
SearchEngine=ezsolr

*/ ?>