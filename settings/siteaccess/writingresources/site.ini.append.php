<?php /* #?ini charset="utf-8"?

[InformationCollectionSettings]
EmailReceiver=

[SiteSettings]
SiteShortName=English Department Writing Resources
SiteName=English Department Writing Resources 
SiteURL=wr.english.ezpublish2.fsu.edu
LoginPage=embedded
IndexPage=/content/view/full/32464
RootNodeDepth=2
DefaultPage=/content/view/full/32464
MetaDataArray[author]=Florida State University English Department Writing Resources
MetaDataArray[keywords]=education, post-secondary florida state university, english department, writing resources, 
MetaDataArray[description]=Florida State University is a Carnegie Doctoral/Research 
MetaDataArray[classification]=Continuing Education, Professional Development, 

[UserSettings]
RegistrationEmail=

[SiteAccessSettings]
RequireUserLogin=false
RelatedSiteAccessList[]=writingresources
RelatedSiteAccessList[]=fsu_admin
ShowHiddenNodes=false
PathPrefix=English-Department-Writing-Resources

[DesignSettings]
SiteDesign=writingresources
AdditionalSiteDesignList[]=fsudesign
AdditionalSiteDesignList[]=ezwebin
AdditionalSiteDesignList[]=base

*/ ?>