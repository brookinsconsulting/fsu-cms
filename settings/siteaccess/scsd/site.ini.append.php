<?php /* #?ini charset="utf-8"?

[InformationCollectionSettings]
EmailReceiver=

[SiteSettings]
SiteShortName=CCI / School of Communication Science & Disorders
SiteName=CCI / School of Communication Science & Disorders 
SiteURL=commdisorders.cci.fsu.edu
LoginPage=embedded
IndexPage=/content/view/full/12396
RootNodeDepth=2
DefaultPage=/content/view/full/12396
MetaDataArray[author]=CCI / School of Communication Science & Disorders
MetaDataArray[keywords]=education, post-secondary florida state university, graduate school, career advancement, 
MetaDataArray[description]=Florida State University is a Carnegie Doctoral/Research 
MetaDataArray[classification]=Continuing Education, Professional Development, 

[UserSettings]
RegistrationEmail=

[SiteAccessSettings]
RequireUserLogin=false
RelatedSiteAccessList[]=scsd
RelatedSiteAccessList[]=fsu_admin
ShowHiddenNodes=false
PathPrefix=CCI-School-of-Communication-Science-Disorders

[DesignSettings]
SiteDesign=scsd
AdditionalSiteDesignList[]=fsudesign
AdditionalSiteDesignList[]=ezwebin
AdditionalSiteDesignList[]=base

*/ ?>