<?php /* #?ini charset="utf-8"?

[InformationCollectionSettings]
EmailReceiver=

[SiteSettings]
SiteShortName=CDPH
SiteName=Center for Demography and Population Health
SiteURL=popcenter.ez.fsu.edu
LoginPage=embedded
IndexPage=/content/view/full/61537
RootNodeDepth=2
DefaultPage=/content/view/full/61537
MetaDataArray[author]=Florida State University
MetaDataArray[keywords]=education, post-secondary florida state university, center, demography, population, health
MetaDataArray[description]=Florida State University is a Carnegie Doctoral/Research 
MetaDataArray[classification]=Continuing Education, Professional Development, 

[UserSettings]
RegistrationEmail=

[SiteAccessSettings]
RequireUserLogin=false
RelatedSiteAccessList[]=cdph
RelatedSiteAccessList[]=fsu_admin
ShowHiddenNodes=false
PathPrefix=Center-for-Demography-and-Population-Health

[DesignSettings]
SiteDesign=cdph
AdditionalSiteDesignList[]=ezfind
AdditionalSiteDesignList[]=fsu-v01
AdditionalSiteDesignList[]=ezwebin
AdditionalSiteDesignList[]=base

[SearchSettings]
SearchEngine=ezsolr

*/ ?>