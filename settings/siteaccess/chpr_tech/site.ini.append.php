<?php /* #?ini charset="utf-8"?

[InformationCollectionSettings]
EmailReceiver=

[SiteSettings]
SiteShortName=CHPR Technical Support
SiteName=CHPR Technical Support 
SiteURL=helpdod.ez.fsu.edu
LoginPage=embedded
IndexPage=/content/view/full/52592
RootNodeDepth=2
DefaultPage=/content/view/full/52592
MetaDataArray[author]=CHPR Technical Support
MetaDataArray[keywords]=education, post-secondary florida state university, graduate school, career advancement, 
MetaDataArray[description]=Florida State University is a Carnegie Doctoral/Research 
MetaDataArray[classification]=Continuing Education, Professional Development, 

[UserSettings]
RegistrationEmail=

[SiteAccessSettings]
RequireUserLogin=false
RelatedSiteAccessList[]=chpr_tech
RelatedSiteAccessList[]=fsu_admin
ShowHiddenNodes=false
PathPrefix=CHPR-Technical-Support

[DesignSettings]
SiteDesign=chpr_tech
AdditionalSiteDesignList[]=ezfind
AdditionalSiteDesignList[]=fsu-v01
AdditionalSiteDesignList[]=ezwebin
AdditionalSiteDesignList[]=base

[SearchSettings]
SearchEngine=ezsolr

*/ ?>