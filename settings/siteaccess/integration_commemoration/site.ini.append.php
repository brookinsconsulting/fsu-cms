<?php /* #?ini charset="utf-8"?

[InformationCollectionSettings]
EmailReceiver=

[SiteSettings]
SiteShortName=Integration Commemoration
SiteName=Integration Commemoration
SiteURL=integration.ez.fsu.edu
LoginPage=embedded
IndexPage=/content/view/full/72825
RootNodeDepth=2
DefaultPage=/content/view/full/72825
MetaDataArray[author]=Florida State University, Integration Commemoration
MetaDataArray[keywords]=Florida State University, Integration Commemoration 
MetaDataArray[description]=Florida State University, Integration Commemoration 
MetaDataArray[classification]=Florida State University, Integration Commemoration 

[UserSettings]
RegistrationEmail=

[SiteAccessSettings]
RequireUserLogin=false
RelatedSiteAccessList[]=integration_commemoration
RelatedSiteAccessList[]=fsu_admin
ShowHiddenNodes=false
PathPrefix=Integration-Commemoration

[DesignSettings]
SiteDesign=integration_commemoration
AdditionalSiteDesignList[]=ezfind
AdditionalSiteDesignList[]=fsu-v01
AdditionalSiteDesignList[]=ezwebin
AdditionalSiteDesignList[]=base

[SearchSettings]
SearchEngine=ezsolr

*/ ?>