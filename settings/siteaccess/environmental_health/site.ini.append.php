<?php /* #?ini charset="utf-8"?

[InformationCollectionSettings]
EmailReceiver=

[SiteSettings]
SiteShortName=Environmental Health & Safety
SiteName=Environmental Health & Safety 
SiteURL=safety.ezpublish2.fsu.edu
LoginPage=embedded
IndexPage=/content/view/full/17938
RootNodeDepth=2
DefaultPage=/content/view/full/17938
MetaDataArray[author]=Environmental Health & Safety
MetaDataArray[keywords]=Environmental Health & Safety, florida state university, 
MetaDataArray[description]=Florida State University Environmental Health & Safety
MetaDataArray[classification]=Environmental Health & Safety 

[UserSettings]
RegistrationEmail=

[SiteAccessSettings]
RequireUserLogin=false
RelatedSiteAccessList[]=environmental_health
RelatedSiteAccessList[]=fsu_admin
ShowHiddenNodes=false
PathPrefix=Environmental-Health-Safety

[DesignSettings]
SiteDesign=environmental_health
AdditionalSiteDesignList[]=fsudesign
AdditionalSiteDesignList[]=ezwebin
AdditionalSiteDesignList[]=base

*/ ?>