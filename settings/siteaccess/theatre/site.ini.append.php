<?php /* #?ini charset="utf-8"?

[InformationCollectionSettings]
EmailReceiver=

[SiteSettings]
SiteShortName=School of Theatre
SiteName=School of Theatre 
SiteURL=theatre.fsu.edu
LoginPage=embedded
IndexPage=/content/view/full/6634
RootNodeDepth=2
DefaultPage=/content/view/full/6634
MetaDataArray[author]=School of Theatre
MetaDataArray[keywords]=school of theatre,florida state university, drama, stage, acting, actor, actress
MetaDataArray[description]=School of Theatre
MetaDataArray[classification]=School of Theatre

[UserSettings]
RegistrationEmail=

[SiteAccessSettings]
RequireUserLogin=false
RelatedSiteAccessList[]=theatre
RelatedSiteAccessList[]=fsu_admin
ShowHiddenNodes=false
PathPrefix=School-of-Theatre

[DesignSettings]
SiteDesign=theatre
AdditionalSiteDesignList[]=fsu-v01
AdditionalSiteDesignList[]=ezwebin
AdditionalSiteDesignList[]=base

*/ ?>