<?php /* #?ini charset="utf-8"?

[InformationCollectionSettings]
EmailReceiver=

[SiteSettings]
SiteShortName=University Regulations
SiteName=University Regulations 
SiteURL=regulations.ezpublish2.fsu.edu
LoginPage=embedded
IndexPage=/content/view/full/20176
RootNodeDepth=2
DefaultPage=/content/view/full/20176
MetaDataArray[author]=University Regulations
MetaDataArray[keywords]=education, post-secondary florida state university, regulations, 
MetaDataArray[description]=Florida State University is a Carnegie Doctoral/Research 
MetaDataArray[classification]=Continuing Education, Professional Development, 

[UserSettings]
RegistrationEmail=

[SiteAccessSettings]
RequireUserLogin=false
RelatedSiteAccessList[]=universityregulations
RelatedSiteAccessList[]=fsu_admin
ShowHiddenNodes=false
PathPrefix=University-Regulations

[DesignSettings]
SiteDesign=universityregulations
AdditionalSiteDesignList[]=fsudesign
AdditionalSiteDesignList[]=ezwebin
AdditionalSiteDesignList[]=base

*/ ?>