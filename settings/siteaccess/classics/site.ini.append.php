<?php /* #?ini charset="utf-8"?

[InformationCollectionSettings]
EmailReceiver=

[SiteSettings]
SiteShortName=Department of Classics
SiteName=Department of Classics 
SiteURL=classics.ez.fsu.edu
LoginPage=embedded
IndexPage=/content/view/full/56325
RootNodeDepth=2
DefaultPage=/content/view/full/56325
MetaDataArray[author]=Florida State University Department of Classics
MetaDataArray[keywords]=Florida State University Department of Classics 
MetaDataArray[description]=Florida State University Department of Classics 
MetaDataArray[classification]=Florida State University Department of Classics

[UserSettings]
RegistrationEmail=

[SiteAccessSettings]
RequireUserLogin=false
RelatedSiteAccessList[]=classics
RelatedSiteAccessList[]=fsu_admin
ShowHiddenNodes=false
PathPrefix=Department-of-Classics

[DesignSettings]
SiteDesign=classics
AdditionalSiteDesignList[]=ezfind
AdditionalSiteDesignList[]=fsu-v01
AdditionalSiteDesignList[]=ezwebin
AdditionalSiteDesignList[]=base

[SearchSettings]
SearchEngine=ezsolr

*/ ?>