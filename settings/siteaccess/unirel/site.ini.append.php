<?php /* #?ini charset="utf-8"?

[InformationCollectionSettings]
EmailReceiver=

[SiteSettings]
SiteShortName=University Relations
SiteName=University Relations
SiteURL=unirel.ezpublish2.fsu.edu
LoginPage=embedded
IndexPage=/content/view/full/16819
RootNodeDepth=2
DefaultPage=/content/view/full/16819
MetaDataArray[author]=Florida State University University Relations
MetaDataArray[keywords]=education, post-secondary florida state university, graduate school, career advancement, University Relations
MetaDataArray[description]=Florida State University is a Carnegie Doctoral/Research 
MetaDataArray[classification]=Continuing Education, Professional Development, University Relations

[UserSettings]
RegistrationEmail=

[SiteAccessSettings]
RequireUserLogin=false
RelatedSiteAccessList[]=unirel
RelatedSiteAccessList[]=fsu_admin
ShowHiddenNodes=false
PathPrefix=University-Relations

[DesignSettings]
SiteDesign=unirel
AdditionalSiteDesignList[]=fsudesign
AdditionalSiteDesignList[]=ezwebin
AdditionalSiteDesignList[]=base

*/ ?>