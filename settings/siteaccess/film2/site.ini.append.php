<?php /* #?ini charset="utf-8"?

[InformationCollectionSettings]
EmailReceiver=

[SiteSettings]
SiteShortName=College of Motion Picture Arts
SiteName=College of Motion Picture Arts
SiteURL=film.fsu.edu
LoginPage=embedded
IndexPage=/content/view/full/1710
RootNodeDepth=2
DefaultPage=/content/view/full/1710
MetaDataArray[author]=Florida State University College of Motion Picture Arts, Film School
MetaDataArray[keywords]=Florida State University College of Motion Picture Arts, Film School 
MetaDataArray[description]=Florida State University College of Motion Picture Arts, Film School 
MetaDataArray[classification]=Florida State University College of Motion Picture Arts, Film School 

[UserSettings]
RegistrationEmail=

[SiteAccessSettings]
RequireUserLogin=false
RelatedSiteAccessList[]=film2
RelatedSiteAccessList[]=fsu_admin
ShowHiddenNodes=false
PathPrefix=College-of-Motion-Picture-Arts

[DesignSettings]
SiteDesign=film2
AdditionalSiteDesignList[]=ezfind
AdditionalSiteDesignList[]=fsu-v01
AdditionalSiteDesignList[]=ezwebin
AdditionalSiteDesignList[]=base

[SearchSettings]
SearchEngine=ezsolr

*/ ?>