<?php /* #?ini charset="utf-8"?

[InformationCollectionSettings]
EmailReceiver=

[SiteSettings]
SiteShortName=Middle East Center
SiteName=Middle East Center 
SiteURL=mec.ez.fsu.edu
LoginPage=embedded
IndexPage=/content/view/full/52746
RootNodeDepth=2
DefaultPage=/content/view/full/52746
MetaDataArray[author]=Florida State University Middle East Center
MetaDataArray[keywords]=education, post-secondary florida state university, graduate school, career advancement, 
MetaDataArray[description]=Florida State University is a Carnegie Doctoral/Research 
MetaDataArray[classification]=Continuing Education, Professional Development, 

[UserSettings]
RegistrationEmail=

[SiteAccessSettings]
RequireUserLogin=false
RelatedSiteAccessList[]=mec
RelatedSiteAccessList[]=fsu_admin
ShowHiddenNodes=false
PathPrefix=Middle-East-Center

[DesignSettings]
SiteDesign=mec
AdditionalSiteDesignList[]=ezfind
AdditionalSiteDesignList[]=fsu-v01
AdditionalSiteDesignList[]=ezwebin
AdditionalSiteDesignList[]=base

[SearchSettings]
SearchEngine=ezsolr

*/ ?>