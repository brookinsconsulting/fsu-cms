<?php /* #?ini charset="utf-8"?

[InformationCollectionSettings]
EmailReceiver=

[Session]
SessionNamePerSiteAccess=disabled

[SiteSettings]
SiteName=Headlines
SiteURL=headlines.ezpublish2.fsu.edu
LoginPage=embedded
IndexPage=/content/view/full/4895/
RootNodeDepth=2
DefaultPage=/content/view/full/4895/
MetaDataArray[author]=Headlines
MetaDataArray[keywords]=school, florida state university, headlines around the world, news, stories

[UserSettings]
RegistrationEmail=

[SiteAccessSettings]
RequireUserLogin=false
RelatedSiteAccessList[]=headlines
RelatedSiteAccessList[]=fsu_admin
ShowHiddenNodes=false
PathPrefix=Florida-State-Headlines-Around-the-World
PathPrefixExclude[]
PathPrefixExclude[]=Media
PathPrefixExclude[]=Users
PathPrefixExclude[]=Flash-Rotation-Articles
PathPrefixExclude[]=Test-Area

[DesignSettings]
SiteDesign=headlines
AdditionalSiteDesignList[]=fsu-v01
AdditionalSiteDesignList[]=ezwebin
AdditionalSiteDesignList[]=base

[RegionalSettings]
Locale=eng-US
ContentObjectLocale=eng-US
ShowUntranslatedObjects=disabled
SiteLanguageList[]=eng-US
TextTranslation=disabled

[FileSettings]
VarDir=var/ezwebin_site

[ContentSettings]
TranslationList=

[MailSettings]
AdminEmail=web@ucs.fsu.edu
EmailSender=

[DebugSettings]
DebugOutput=enabled
DebugByUser=enabled
DebugRedirection=disabled
DebugUserIDList[]=195
DebugUserIDList[]=14

[TemplateSettings]
Debug=disabled
ShowXHTMLCode=enabled
ShowUsedTemplates=enabled

[DatabaseSettings]
DatabaseImplementation=ezmysql
Server=localhost
Port=
User=root
Password=t6ntnimB
Database=fsu_livedata
Charset=
Socket=disabled
SQLOutput=disabled
*/ ?>
