<?php /* #?ini charset="utf-8"?

[InformationCollectionSettings]
EmailReceiver=

[SiteSettings]
SiteShortName=ECAP
SiteName=Early Childhood Autism Program
SiteURL=ez.fsu.edu
LoginPage=embedded
IndexPage=/content/view/full/92981
RootNodeDepth=2
DefaultPage=/content/view/full/92981
MetaDataArray[author]=Florida State University
MetaDataArray[keywords]=education, post-secondary florida state university, 
MetaDataArray[description]=Florida State University is a Carnegie Doctoral/Research 
MetaDataArray[classification]=Continuing Education, Professional Development, 

[UserSettings]
RegistrationEmail=

[SiteAccessSettings]
RequireUserLogin=false
RelatedSiteAccessList[]=ecap
RelatedSiteAccessList[]=fsu_admin
ShowHiddenNodes=false
PathPrefix=Panama-City-ECAP

[DesignSettings]
SiteDesign=ecap
AdditionalSiteDesignList[]=ezfind
AdditionalSiteDesignList[]=fsu-v01
AdditionalSiteDesignList[]=ezwebin
AdditionalSiteDesignList[]=base

[SearchSettings]
SearchEngine=ezsolr

*/ ?>