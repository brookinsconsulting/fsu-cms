<?php /* #?ini charset="utf-8"?

[InformationCollectionSettings]
EmailReceiver=

[SiteSettings]
SiteShortName=Visual System
SiteName=The Florida State University Voice and Visual System
SiteURL=visualsystem.fsu.edu
LoginPage=embedded
IndexPage=/content/view/full/7759
RootNodeDepth=2
DefaultPage=/content/view/full/7759
MetaDataArray[author]=The Florida State University Voice and Visual System
MetaDataArray[keywords]=education, post-secondary florida state university, graduate school, career advancement, 
MetaDataArray[description]=The Florida State University Voice and Visual System
MetaDataArray[classification]=Continuing Education, Professional Development, 

[UserSettings]
RegistrationEmail=

[SiteAccessSettings]
RequireUserLogin=false
RelatedSiteAccessList[]=identityguide
RelatedSiteAccessList[]=fsu_admin
ShowHiddenNodes=false
PathPrefix=Voice-and-Visual-System

[DesignSettings]
SiteDesign=fsu-v01
AdditionalSiteDesignList[]=fsu
AdditionalSiteDesignList[]=ezwebin
AdditionalSiteDesignList[]=base

*/ ?>