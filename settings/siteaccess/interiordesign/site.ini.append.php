<?php /* #?ini charset="utf-8"?

[InformationCollectionSettings]
EmailReceiver=

[SiteSettings]
SiteShortName=Interior Design
SiteName=Department of Interior Design
SiteURL=interiordesign.ez.fsu.edu
LoginPage=embedded
IndexPage=/content/view/full/76871
RootNodeDepth=2
DefaultPage=/content/view/full/76871
MetaDataArray[author]=Florida State University Department of Interior Design
MetaDataArray[keywords]=Florida State University Department of Interior Design 
MetaDataArray[description]=Florida State University Department of Interior Design
MetaDataArray[classification]=Florida State University Department of Interior Design 

[UserSettings]
RegistrationEmail=

[SiteAccessSettings]
RequireUserLogin=false
RelatedSiteAccessList[]=interiordesign
RelatedSiteAccessList[]=fsu_admin
ShowHiddenNodes=false
PathPrefix=Department-of-Interior-Design

[DesignSettings]
SiteDesign=interiordesign
AdditionalSiteDesignList[]=ezfind
AdditionalSiteDesignList[]=fsu-v01
AdditionalSiteDesignList[]=ezwebin
AdditionalSiteDesignList[]=base

[SearchSettings]
SearchEngine=ezsolr

*/ ?>