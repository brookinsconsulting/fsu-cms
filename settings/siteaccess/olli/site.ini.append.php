<?php /* #?ini charset="utf-8"?

[InformationCollectionSettings]
EmailReceiver=

[SiteSettings]
SiteShortName=OLLI
SiteName=Osher Lifelong Learning Institute 
SiteURL=olli.fsu.edu
LoginPage=embedded
IndexPage=/content/view/full/52729
RootNodeDepth=2
DefaultPage=/content/view/full/52729
MetaDataArray[author]=Osher Lifelong Learning Institute
MetaDataArray[keywords]=education, post-secondary florida state university, graduate school, career advancement, 
MetaDataArray[description]=Florida State University is a Carnegie Doctoral/Research 
MetaDataArray[classification]=Continuing Education, Professional Development, 

[UserSettings]
RegistrationEmail=

[SiteAccessSettings]
RequireUserLogin=false
RelatedSiteAccessList[]=olli
RelatedSiteAccessList[]=fsu_admin
ShowHiddenNodes=false
PathPrefix=Osher-Lifelong-Learning-Institute

[DesignSettings]
SiteDesign=olli
AdditionalSiteDesignList[]=ezfind
AdditionalSiteDesignList[]=fsu-v01
AdditionalSiteDesignList[]=ezwebin
AdditionalSiteDesignList[]=base

[SearchSettings]
SearchEngine=ezsolr

*/ ?>