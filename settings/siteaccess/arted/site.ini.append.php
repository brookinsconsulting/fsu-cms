<?php /* #?ini charset="utf-8"?

[InformationCollectionSettings]
EmailReceiver=

[SiteSettings]
SiteShortName=Department of Art Education
SiteName=Department of Art Education
SiteURL=are.ezpublish2.fsu.edu
LoginPage=embedded
IndexPage=/content/view/full/8859
RootNodeDepth=2
DefaultPage=/content/view/full/8859
MetaDataArray[author]=Florida State University Art Education
MetaDataArray[keywords]=education, post-secondary florida state university, graduate school, career advancement, 
MetaDataArray[description]=Florida State University is a Carnegie Doctoral/Research 
MetaDataArray[classification]=Continuing Education, Professional Development, 

[UserSettings]
RegistrationEmail=

[SiteAccessSettings]
RequireUserLogin=false
RelatedSiteAccessList[]=arted
RelatedSiteAccessList[]=fsu_admin
ShowHiddenNodes=false
PathPrefix=Department-of-Art-Education

[DesignSettings]
SiteDesign=arted
AdditionalSiteDesignList[]=fsudesign
AdditionalSiteDesignList[]=ezwebin
AdditionalSiteDesignList[]=base

[SuperSettings]
SupergraphicsFolder=2646
FlashArticles=xxx
FixedHomeBannerPath=/design/arted/images/banner.jpg 

*/ ?>