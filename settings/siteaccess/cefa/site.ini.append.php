<?php /* #?ini charset="utf-8"?

[InformationCollectionSettings]
EmailReceiver=

[SiteSettings]
SiteShortName=CEFA
SiteName=Center for Economic Forecasting and Analysis 
SiteURL=cefa.ezpublish2.fsu.edu
LoginPage=embedded
IndexPage=/content/view/full/45536
RootNodeDepth=2
DefaultPage=/content/view/full/45536
MetaDataArray[author]=Florida State University Center for Economic Forecasting and Analysis
MetaDataArray[keywords]=Florida State University Center for Economic Forecasting and Analysis, CEFA 
MetaDataArray[description]=Florida State University Center for Economic Forecasting and Analysis, CEFA 
MetaDataArray[classification]=Florida State University Center for Economic Forecasting and Analysis, CEFA 

[UserSettings]
RegistrationEmail=

[SiteAccessSettings]
RequireUserLogin=false
RelatedSiteAccessList[]=cefa
RelatedSiteAccessList[]=fsu_admin
ShowHiddenNodes=false
PathPrefix=CEFA

[DesignSettings]
SiteDesign=cefa
AdditionalSiteDesignList[]=ezfind
AdditionalSiteDesignList[]=fsu-v01
AdditionalSiteDesignList[]=ezwebin
AdditionalSiteDesignList[]=base

[SearchSettings]
SearchEngine=ezsolr

*/ ?>