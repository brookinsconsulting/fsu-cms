<?php /* #?ini charset="utf-8"?

[InformationCollectionSettings]
EmailReceiver=

[SiteSettings]
SiteShortName=Faculty Senate
SiteName=Faculty Senate 
SiteURL=ezpublish2.fsu.edu
LoginPage=embedded
IndexPage=/content/view/full/20770
RootNodeDepth=2
DefaultPage=/content/view/full/20770
MetaDataArray[author]=Faculty Senate
MetaDataArray[keywords]=Faculty Senate
MetaDataArray[description]=Faculty Senate 
MetaDataArray[classification]=Faculty Senate 

[UserSettings]
RegistrationEmail=

[SiteAccessSettings]
RequireUserLogin=false
RelatedSiteAccessList[]=facsenate
RelatedSiteAccessList[]=fsu_admin
ShowHiddenNodes=false
PathPrefix=Faculty-Senate

[DesignSettings]
SiteDesign=facsenate
AdditionalSiteDesignList[]=fsudesign
AdditionalSiteDesignList[]=ezwebin
AdditionalSiteDesignList[]=base

*/ ?>