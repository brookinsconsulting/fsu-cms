<?php /* #?ini charset="utf-8"?

[InformationCollectionSettings]
EmailReceiver=

[SiteSettings]
SiteShortName=ITS Service Center
SiteName=ITS Service Center
SiteURL=helpdesk.fsu.edu
LoginPage=embedded
IndexPage=/content/view/full/17248
RootNodeDepth=2
DefaultPage=/content/view/full/17248
MetaDataArray[author]=ITS Service Center
MetaDataArray[keywords]=education, post-secondary florida state university, graduate school, career advancement, 
MetaDataArray[description]=Florida State University is a Carnegie Doctoral/Research 
MetaDataArray[classification]=Continuing Education, Professional Development, 

[UserSettings]
RegistrationEmail=

[SiteAccessSettings]
RequireUserLogin=false
RelatedSiteAccessList[]=helpdesk
RelatedSiteAccessList[]=fsu_admin
ShowHiddenNodes=false
PathPrefix=ITS-Service-Center

[DesignSettings]
SiteDesign=helpdesk
AdditionalSiteDesignList[]=fsudesign
AdditionalSiteDesignList[]=ezwebin
AdditionalSiteDesignList[]=base

*/ ?>