<?php /* #?ini charset="utf-8"?

[InformationCollectionSettings]
EmailReceiver=

[SiteSettings]
SiteShortName=Division of Student Affairs
SiteName=Division of Student Affairs
SiteURL=studentaffairs.ez.fsu.edu
LoginPage=embedded
IndexPage=/content/view/full/94262
RootNodeDepth=2
DefaultPage=/content/view/full/94262
MetaDataArray[author]=Florida State University Division of Student Affairs
MetaDataArray[keywords]=Florida State University Division of Student Affairs 
MetaDataArray[description]=Florida State University Division of Student Affairs 
MetaDataArray[classification]=Florida State University Division of Student Affairs 

[UserSettings]
RegistrationEmail=

[SiteAccessSettings]
RequireUserLogin=false
RelatedSiteAccessList[]=student_affairs
RelatedSiteAccessList[]=fsu_admin
ShowHiddenNodes=false
PathPrefix=Division-of-Student-Affairs

[DesignSettings]
SiteDesign=student_affairs
AdditionalSiteDesignList[]=ezfind
AdditionalSiteDesignList[]=fsu-v01
AdditionalSiteDesignList[]=ezwebin
AdditionalSiteDesignList[]=base

[SearchSettings]
SearchEngine=ezsolr

*/ ?>