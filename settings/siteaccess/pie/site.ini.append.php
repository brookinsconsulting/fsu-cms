<?php /* #?ini charset="utf-8"?

[InformationCollectionSettings]
EmailReceiver=

[SiteSettings]
SiteShortName=Program for Instructional Excellence
SiteName=Program for Instructional Excellence
SiteURL=pie.ezpublish2.fsu.edu
LoginPage=embedded
IndexPage=/content/view/full/44444
RootNodeDepth=2
DefaultPage=/content/view/full/44444
MetaDataArray[author]=Program for Instructional Excellence
MetaDataArray[keywords]=education, post-secondary florida state university, Program for Instructional Excellence,
MetaDataArray[description]=Florida State University is a Carnegie Doctoral/Research
MetaDataArray[classification]=Continuing Education, Professional Development,

[UserSettings]
RegistrationEmail=

[SiteAccessSettings]
RequireUserLogin=false
RelatedSiteAccessList[]=pie
RelatedSiteAccessList[]=fsu_admin
ShowHiddenNodes=false
PathPrefix=Program-for-Instructional-Excellence-PIE

[DesignSettings]
SiteDesign=pie
AdditionalSiteDesignList[]=ezfind
AdditionalSiteDesignList[]=fsudesign
AdditionalSiteDesignList[]=ezwebin
AdditionalSiteDesignList[]=base

[SearchSettings]
SearchEngine=ezsolr

*/ ?>