<?php /* #?ini charset="utf-8"?

[InformationCollectionSettings]
EmailReceiver=

[SiteSettings]
SiteShortName=College of Motion Picture Arts
SiteName=College of Motion Picture Arts 
SiteURL=film.ezpublish2.fsu.edu
LoginPage=embedded
IndexPage=/content/view/full/1710
RootNodeDepth=2
DefaultPage=/content/view/full/1710
MetaDataArray[author]=Florida State University College of Motion Picture Arts Film School
MetaDataArray[keywords]=Florida State University College of Motion Picture Arts Film School 
MetaDataArray[description]=Florida State University College of Motion Picture Arts Film School 
MetaDataArray[classification]=Florida State University College of Motion Picture Arts Film School 

[UserSettings]
RegistrationEmail=

[SiteAccessSettings]
RequireUserLogin=false
RelatedSiteAccessList[]=film_newtemp
RelatedSiteAccessList[]=fsu_admin
ShowHiddenNodes=false
PathPrefix=Film-School

[DesignSettings]
SiteDesign=film_newtemp
AdditionalSiteDesignList[]=ezfind
AdditionalSiteDesignList[]=fsu-v02
AdditionalSiteDesignList[]=ezwebin
AdditionalSiteDesignList[]=base

[SearchSettings]
SearchEngine=ezsolr

*/ ?>