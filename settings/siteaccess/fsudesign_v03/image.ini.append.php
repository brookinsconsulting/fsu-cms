<?php /* #?ini charset="utf-8"?

[ImageMagick]
Filters[]=geometry/scaleheightuponly=-geometry x%1<

[AliasSettings]
AliasList[]=supergraphic_film

[AliasSettings]
AliasList[]=supergraphic_film_inner

[supergraphic_film]
Reference=
Filters[]=geometry/scalewidth=950
Filters[]=geometry/scaleheightuponly=400
Filters[]=centerimg=950;400
Filters[]=strip
Filters[]=colorspace=RGB


[supergraphic_film_inner]
Reference=
Filters[]=geometry/scalewidth=950
Filters[]=strip
Filters[]=colorspace=RGB
*/ ?>
