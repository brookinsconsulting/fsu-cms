<?php /* #?ini charset="utf-8"?

[ContentSettings]
StaticCache=disabled

[InformationCollectionSettings]
EmailReceiver=

[SiteSettings]
SiteShortName=College of Motion Picture Arts
SiteName=College of Motion Picture Arts
SiteURL=webteam.ez.fsu.edu
LoginPage=embedded
IndexPage=/content/view/full/90721/
SiteRootNodeID=90721
RootNodeDepth=2
DefaultPage=/content/view/full/90721/
MetaDataArray[author]=Florida State University College of Motion Picture Arts
MetaDataArray[keywords]=education, post-secondary florida state university, graduate school, career advancement, film school
MetaDataArray[description]=Florida State University is a Carnegie Doctoral/Research 
MetaDataArray[classification]=Continuing Education, Professional Development, 

[UserSettings]
RegistrationEmail=

[SiteAccessSettings]
RequireUserLogin=false
ShowHiddenNodes=false
PathPrefix=Design-fsu-v03

[DesignSettings]
SiteDesign=fsu-v03
AdditionalSiteDesignList[]=fsu
AdditionalSiteDesignList[]=fsudesign
AdditionalSiteDesignList[]=ezwebin
AdditionalSiteDesignList[]=base

[DebugSettings]
DebugOutput=enabled
DebugRedirection=disabled

[TemplateSettings]
Debug=disabled
ShowXHTMLCode=disabled
ShowUsedTemplates=enabled

[DatabaseSettings]
SQLOutput=disabled
*/ ?>
