<?php /* #?ini charset="utf-8"?

[DebugSettings]
DebugOutput=enabled
DebugRedirection=disabled

[InformationCollectionSettings]
EmailReceiver=

[SiteSettings]
SiteShortName=Budget and Analysis
SiteName=Budget and Analysis
SiteURL=bad.fsu.edu
LoginPage=embedded
IndexPage=/content/view/full/7019
RootNodeDepth=2
DefaultPage=/content/view/full/7019
MetaDataArray[author]=Budget and Analysis
MetaDataArray[keywords]=Budget and Analysis,florida state university
MetaDataArray[description]=Budget and Analysis
MetaDataArray[classification]=Budget and Analysis

[UserSettings]
RegistrationEmail=

[SiteAccessSettings]
RequireUserLogin=false
RelatedSiteAccessList[]=bad
RelatedSiteAccessList[]=fsu_admin
ShowHiddenNodes=false
PathPrefix=Budget-Analysis

[DesignSettings]
SiteDesign=bad
AdditionalSiteDesignList[]=fsu-v01
AdditionalSiteDesignList[]=ezwebin
AdditionalSiteDesignList[]=base

[TemplateSettings]
Debug=disabled
ShowXHTMLCode=enabled
ShowUsedTemplates=enabled

[DatabaseSettings]
SQLOutput=disabled
*/ ?>