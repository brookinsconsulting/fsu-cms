<?php /* #?ini charset="utf-8"?

[InformationCollectionSettings]
EmailReceiver=

[SiteSettings]
SiteShortName=Student Rights & Responsibilities
SiteName=Student Rights & Responsibilities
SiteURL=ssr.ez.fsu.edu
LoginPage=embedded
IndexPage=/content/view/full/88391
RootNodeDepth=2
DefaultPage=/content/view/full/88391
MetaDataArray[author]=Florida State University, Student Rights & Responsibilities
MetaDataArray[keywords]=Florida State University, Student Rights & Responsibilities 
MetaDataArray[description]=Florida State University, Student Rights & Responsibilities 
MetaDataArray[classification]=Florida State University, Student Rights & Responsibilities 

[UserSettings]
RegistrationEmail=

[SiteAccessSettings]
RequireUserLogin=false
RelatedSiteAccessList[]=srr
RelatedSiteAccessList[]=fsu_admin
ShowHiddenNodes=false
PathPrefix=Student-Rights-Responsibilities

[DesignSettings]
SiteDesign=srr
AdditionalSiteDesignList[]=ezfind
AdditionalSiteDesignList[]=fsu-v01
AdditionalSiteDesignList[]=ezwebin
AdditionalSiteDesignList[]=base

[SearchSettings]
SearchEngine=ezsolr

*/ ?>