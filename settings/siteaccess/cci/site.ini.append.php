<?php /* #?ini charset="utf-8"?

[InformationCollectionSettings]
EmailReceiver=

[SiteSettings]
SiteShortName=CCI
SiteName=College of Communication & Information 
SiteURL=ezpublish2.fsu.edu
LoginPage=embedded
IndexPage=/content/view/full/19897
RootNodeDepth=2
DefaultPage=/content/view/full/19897
MetaDataArray[author]=College of Communication & Information
MetaDataArray[keywords]=education, post-secondary florida state university, graduate school, career advancement, 
MetaDataArray[description]=Florida State University is a Carnegie Doctoral/Research 
MetaDataArray[classification]=Continuing Education, Professional Development, 

[UserSettings]
RegistrationEmail=

[SiteAccessSettings]
RequireUserLogin=false
RelatedSiteAccessList[]=cci
RelatedSiteAccessList[]=fsu_admin
ShowHiddenNodes=false
PathPrefix=College-of-Communication-Information

[DesignSettings]
SiteDesign=cci
AdditionalSiteDesignList[]=fsudesign
AdditionalSiteDesignList[]=ezwebin

[DebugSettings]
DebugOutput=enabled
DebugRedirection=disabled

[TemplateSettings]
Debug=disabled
ShowXHTMLCode=disabled
ShowUsedTemplates=enabled

[DatabaseSettings]
SQLOutput=disabled
*/ ?>