<?php /* #?ini charset="utf-8"?

[InformationCollectionSettings]
EmailReceiver=

[SiteSettings]
SiteShortName=DSH
SiteName=Dedman School of Hospitality 
SiteURL=ezpublish2.fsu.edu
LoginPage=embedded
IndexPage=/content/view/full/19992
RootNodeDepth=2
DefaultPage=/content/view/full/19992
MetaDataArray[author]=Dedman School of Hospitality 
MetaDataArray[keywords]=education, post-secondary florida state university, graduate school, career advancement, 
MetaDataArray[description]=Florida State University is a Carnegie Doctoral/Research 
MetaDataArray[classification]=Continuing Education, Professional Development, 

[UserSettings]
RegistrationEmail=

[SiteAccessSettings]
RequireUserLogin=false
RelatedSiteAccessList[]=dsh
RelatedSiteAccessList[]=fsu_admin
ShowHiddenNodes=false
PathPrefix=Dedman-School-of-Hospitality

[DesignSettings]
SiteDesign=dsh
AdditionalSiteDesignList[]=fsudesign
AdditionalSiteDesignList[]=ezwebin
AdditionalSiteDesignList[]=base

[DebugSettings]
DebugOutput=enabled
DebugRedirection=disabled

[TemplateSettings]
Debug=disabled
ShowXHTMLCode=disabled
ShowUsedTemplates=enabled

[DatabaseSettings]
SQLOutput=disabled
*/ ?>