<?php /* #?ini charset="utf-8"?

[MenuSettings]
AvailableMenuArray[]
AvailableMenuArray[]=TopOnly
AvailableMenuArray[]=LeftOnly
AvailableMenuArray[]=DoubleTop
AvailableMenuArray[]=LeftTop
HideLeftMenuClasses[]
HideLeftMenuClasses[]=ajax_calendar
HideLeftMenuClasses[]=external_cal
HideLeftMenuClasses[]=multicalendar
HideLeftMenuClasses[]=ajax_calendar
HideLeftMenuClasses[]=external_cal
HideLeftMenuClasses[]=multicalendar

[SelectedMenu]
CurrentMenu=TopOnly
TopMenu=flat_top
LeftMenu=

[TopOnly]
TitleText=Only top menu
MenuThumbnail=menu/top_only.jpg
TopMenu=flat_top
LeftMenu=

[LeftOnly]
TitleText=Left menu
MenuThumbnail=menu/left_only.jpg
TopMenu=
LeftMenu=flat_left

[DoubleTop]
TitleText=Double top menu
MenuThumbnail=menu/double_top.jpg
TopMenu=double_top
LeftMenu=

[LeftTop]
TitleText=Left and top
MenuThumbnail=menu/left_top.jpg
TopMenu=flat_top
LeftMenu=flat_left

[MenuContentSettings]
TopIdentifierList[]
TopIdentifierList[]=folder
TopIdentifierList[]=feedback_form
TopIdentifierList[]=frontpage
TopIdentifierList[]=folder
TopIdentifierList[]=feedback_form
TopIdentifierList[]=gallery
TopIdentifierList[]=forum
TopIdentifierList[]=documentation_page
TopIdentifierList[]=forums
TopIdentifierList[]=event_calendar
TopIdentifierList[]=multicalendar
TopIdentifierList[]=link
TopIdentifierList[]=blog
TopIdentifierList[]=frontpage
TopIdentifierList[]=video_upload
TopIdentifierList[]=folder
TopIdentifierList[]=feedback_form
TopIdentifierList[]=gallery
TopIdentifierList[]=forum
TopIdentifierList[]=documentation_page
TopIdentifierList[]=forums
TopIdentifierList[]=event_calendar
TopIdentifierList[]=multicalendar
TopIdentifierList[]=link
TopIdentifierList[]=blog
TopIdentifierList[]=frontpage
TopIdentifierList[]=video_upload
LeftIdentifierList[]
LeftIdentifierList[]=folder
LeftIdentifierList[]=frontpage
LeftIdentifierList[]=feedback_form
LeftIdentifierList[]=weblog
LeftIdentifierList[]=gallery
LeftIdentifierList[]=link
LeftIdentifierList[]=info_page
LeftIdentifierList[]=article
LeftIdentifierList[]=folder
LeftIdentifierList[]=frontpage
LeftIdentifierList[]=feedback_form
LeftIdentifierList[]=weblog
LeftIdentifierList[]=gallery
LeftIdentifierList[]=link
LeftIdentifierList[]=info_page
LeftIdentifierList[]=article
LeftIdentifierList[]=event_calendar
LeftIdentifierList[]=folder
LeftIdentifierList[]=frontpage
LeftIdentifierList[]=feedback_form
LeftIdentifierList[]=weblog
LeftIdentifierList[]=gallery
LeftIdentifierList[]=link
LeftIdentifierList[]=info_page
LeftIdentifierList[]=article
LeftIdentifierList[]=event_calendar

[NavigationPart]
Part[]
Part[ezcontentnavigationpart]=Content structure
Part[ezmedianavigationpart]=Media library
Part[ezusernavigationpart]=User accounts
Part[ezshopnavigationpart]=Webshop
Part[ezvisualnavigationpart]=Design
Part[ezsetupnavigationpart]=Setup
Part[ezmynavigationpart]=My account
Part[podcastnavigationpart]=Podcasts

[TopAdminMenu]
Tabs[]
Tabs[]=content
Tabs[]=media
Tabs[]=users
Tabs[]=shop
Tabs[]=design
Tabs[]=setup
Tabs[]=my_account
Tabs[]=podcast
Tabs[]=podcast

[Topmenu_content]
URL[]
URL[default]=content/view/full/2
URL[browse]=content/browse/2
NavigationPartIdentifier=ezcontentnavigationpart
Enabled[]
Enabled[default]=true
Enabled[browse]=true
Enabled[edit]=false
Shown[]
Shown[default]=true
Shown[navigation]=true
Shown[browse]=true

[Topmenu_media]
NavigationPartIdentifier=ezmedianavigationpart
URL[]
URL[default]=content/view/full/43
URL[browse]=content/browse/43
Enabled[]
Enabled[default]=true
Enabled[browse]=true
Enabled[edit]=false
Shown[]
Shown[default]=true
Shown[navigation]=true
Shown[browse]=true

[Topmenu_users]
NavigationPartIdentifier=ezusernavigationpart
URL[]
URL[default]=content/view/full/5
URL[browse]=content/browse/5
Enabled[]
Enabled[default]=true
Enabled[browse]=true
Enabled[edit]=false
Shown[]
Shown[default]=true
Shown[navigation]=true
Shown[browse]=true

[Topmenu_shop]
NavigationPartIdentifier=ezshopnavigationpart
URL[]
URL[default]=shop/orderlist
Enabled[]
Enabled[default]=true
Enabled[browse]=false
Enabled[edit]=false
Shown[]
Shown[navigation]=true
Shown[default]=false
Shown[browse]=true

[Topmenu_design]
NavigationPartIdentifier=ezvisualnavigationpart
URL[]
URL[default]=visual/menuconfig
Enabled[]
Enabled[default]=true
Enabled[browse]=false
Enabled[edit]=false
Shown[]
Shown[navigation]=true
Shown[default]=true
Shown[browse]=true

[Topmenu_setup]
NavigationPartIdentifier=ezsetupnavigationpart
URL[]
URL[default]=setup/cache
Enabled[]
Enabled[default]=true
Enabled[browse]=false
Enabled[edit]=false
Shown[]
Shown[default]=true
Shown[navigation]=true
Shown[browse]=true

[Topmenu_my_account]
NavigationPartIdentifier=ezmynavigationpart
URL[]
URL[default]=content/draft
Enabled[]
Enabled[default]=true
Enabled[browse]=false
Enabled[edit]=false
Shown[]
Shown[default]=true
Shown[navigation]=true
Shown[browse]=true

[LeftMenuSettings]
MenuWidth[]
MenuWidth[small]=13
MenuWidth[medium]=19
MenuWidth[large]=25

[Topmenu_podcast]
NavigationPartIdentifier=podcastnavigationpart
Name=Podcasts
Enabled[]
Enabled[default]=false
Enabled[browse]=false
Enabled[edit]=false
Shown[]
Shown[default]=false
Shown[navigation]=false
Shown[browse]=false
*/ ?>