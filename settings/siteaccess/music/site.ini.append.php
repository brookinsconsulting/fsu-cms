<?php /* #?ini charset="utf-8"?

[ContentSettings]
StaticCache=enabled

[InformationCollectionSettings]
EmailReceiver=

[SiteSettings]
SiteShortName=College of Music
SiteName=College of Music
SiteURL=home.music.ezpublish2.fsu.edu
LoginPage=embedded
IndexPage=/content/view/full/7744
RootNodeDepth=2
DefaultPage=/content/view/full/7744
MetaDataArray[author]=Florida State University College of Music
MetaDataArray[keywords]=education, College of Music, florida state university, instrumental,orchestra, recital 
MetaDataArray[description]=Florida State University College of Music 
MetaDataArray[classification]=Florida State University College of Music 

[UserSettings]
RegistrationEmail=

[SiteAccessSettings]
RequireUserLogin=false
RelatedSiteAccessList[]=music
RelatedSiteAccessList[]=fsu_admin
ShowHiddenNodes=false
PathPrefix=College-of-Music

[DesignSettings]
SiteDesign=music
AdditionalSiteDesignList[]=fsudesign
AdditionalSiteDesignList[]=ezwebin
AdditionalSiteDesignList[]=base

[DebugSettings]
DebugOutput=enabled
DebugRedirection=disabled

[TemplateSettings]
Debug=disabled
ShowXHTMLCode=disabled
ShowUsedTemplates=enabled

[DatabaseSettings]
SQLOutput=disabled
*/ ?>