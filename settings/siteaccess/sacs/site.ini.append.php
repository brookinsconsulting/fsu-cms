<?php /* #?ini charset="utf-8"?

[InformationCollectionSettings]
EmailReceiver=

[SiteSettings]
SiteShortName=SACS Accreditation
SiteName=SACS Accreditation
SiteURL=sacs.ez.fsu.edu
LoginPage=embedded
IndexPage=/content/view/full/80461
RootNodeDepth=2
DefaultPage=/content/view/full/80461
MetaDataArray[author]=Florida State University, SACS Accreditation
MetaDataArray[keywords]=Florida State University, SACS Accreditation 
MetaDataArray[description]=Florida State University, SACS Accreditation 
MetaDataArray[classification]=Florida State University, SACS Accreditation 

[UserSettings]
RegistrationEmail=

[SiteAccessSettings]
RequireUserLogin=false
RelatedSiteAccessList[]=sacs
RelatedSiteAccessList[]=fsu_admin
ShowHiddenNodes=false
PathPrefix=SACS-Accreditation

[DesignSettings]
SiteDesign=sacs
AdditionalSiteDesignList[]=ezfind
AdditionalSiteDesignList[]=fsu-v01
AdditionalSiteDesignList[]=ezwebin
AdditionalSiteDesignList[]=base

[SearchSettings]
SearchEngine=ezsolr

*/ ?>