<?php /* #?ini charset="utf-8"?

[InformationCollectionSettings]
EmailReceiver=

[SiteSettings]
SiteShortName=Inspector General
SiteName=Office of Inspector General Services
SiteURL=auditservices.ez.fsu.edu
LoginPage=embedded
IndexPage=/content/view/full/16822
RootNodeDepth=2
DefaultPage=/content/view/full/16822
MetaDataArray[author]=Florida State University, Office of Inspector General Services
MetaDataArray[keywords]=Florida State University, Office of Inspector General Services
MetaDataArray[description]=Florida State University, Office of Inspector General Services 
MetaDataArray[classification]=Florida State University, Office of Inspector General Services

[UserSettings]
RegistrationEmail=

[SiteAccessSettings]
RequireUserLogin=false
RelatedSiteAccessList[]=auditservices
RelatedSiteAccessList[]=fsu_admin
ShowHiddenNodes=false
PathPrefix=Office-of-Inspector-General-Services

[DesignSettings]
SiteDesign=auditservices
AdditionalSiteDesignList[]=fsudesign
AdditionalSiteDesignList[]=ezwebin
AdditionalSiteDesignList[]=base

*/ ?>