<?php /* #?ini charset="utf-8"?

[InformationCollectionSettings]
EmailReceiver=

[SiteSettings]
SiteShortName=Healthy Campus Committee
SiteName=Healthy Campus Committee
SiteURL=healthynolestudentaffairs.ez.fsu.edu
LoginPage=embedded
IndexPage=/content/view/full/63357
RootNodeDepth=2
DefaultPage=/content/view/full/63357
MetaDataArray[author]=Florida State University
MetaDataArray[keywords]=Florida State University, healthy noles, health, Healthy Campus Committee, student affairs  
MetaDataArray[description]=Florida State University, healthy noles, health, Healthy Campus Committee, student affairs
MetaDataArray[classification]=Florida State University, healthy noles, health, Healthy Campus Committee, student affairs 

[UserSettings]
RegistrationEmail=

[SiteAccessSettings]
RequireUserLogin=false
RelatedSiteAccessList[]=healthy_campus
RelatedSiteAccessList[]=fsu_admin
ShowHiddenNodes=false
PathPrefix=Healthy-Campus-Committee

[DesignSettings]
SiteDesign=Healthy Campus Committee
AdditionalSiteDesignList[]=ezfind
AdditionalSiteDesignList[]=fsu-v01
AdditionalSiteDesignList[]=ezwebin
AdditionalSiteDesignList[]=base

[SearchSettings]
SearchEngine=ezsolr

*/ ?>