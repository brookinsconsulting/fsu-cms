<?php /* #?ini charset="utf-8"?

[InformationCollectionSettings]
EmailReceiver=

[SiteSettings]
SiteShortName=Office of Faculty Development and Advancement
SiteName=Office of Faculty Development and Advancement
SiteURL=dof.ezpublish2.fsu.edu
LoginPage=embedded
IndexPage=/content/view/full/20769
RootNodeDepth=2
DefaultPage=/content/view/full/20769
MetaDataArray[author]=Florida State University Office of Faculty Development and Advancement
MetaDataArray[keywords]=Florida State University Office of Faculty Development and Advancement
MetaDataArray[description]=Florida State UniversityOffice of Faculty Development and Advancement
MetaDataArray[classification]=Florida State University Office of Faculty Development and Advancement

[UserSettings]
RegistrationEmail=

[SiteAccessSettings]
RequireUserLogin=false
RelatedSiteAccessList[]=dof
RelatedSiteAccessList[]=fsu_admin
ShowHiddenNodes=false
PathPrefix=Office-of-Faculty-Development-and-Advancement

[DesignSettings]
SiteDesign=dof
AdditionalSiteDesignList[]=fsudesign
AdditionalSiteDesignList[]=ezwebin
AdditionalSiteDesignList[]=base

*/ ?>