<?php /* #?ini charset="utf-8"?

[InformationCollectionSettings]
EmailReceiver=

[SiteSettings]
SiteShortName=CCI / School of Communication
SiteName=CCI / School of Communication
SiteURL=comm.cci.fsu.edu
LoginPage=embedded
IndexPage=/content/view/full/12397
RootNodeDepth=2
DefaultPage=/content/view/full/12397
MetaDataArray[author]=CCI / School of Communication
MetaDataArray[keywords]=education, post-secondary florida state university, graduate school, career advancement, 
MetaDataArray[description]=Florida State University is a Carnegie Doctoral/Research 
MetaDataArray[classification]=Continuing Education, Professional Development, 

[UserSettings]
RegistrationEmail=

[SiteAccessSettings]
RequireUserLogin=false
RelatedSiteAccessList[]=scomm
RelatedSiteAccessList[]=fsu_admin
ShowHiddenNodes=false
PathPrefix=CCI-School-of-Communication

[DesignSettings]
SiteDesign=scomm
AdditionalSiteDesignList[]=fsudesign
AdditionalSiteDesignList[]=ezwebin
AdditionalSiteDesignList[]=base

*/ ?>