<?php /* #?ini charset="utf-8"?

[InformationCollectionSettings]
EmailReceiver=

[SiteSettings]
SiteShortName=The Center for Multicultural Affairs
SiteName=The Center for Multicultural Affairs
SiteURL=cma.ez.fsu.edu
LoginPage=embedded
IndexPage=/content/view/full/76827
RootNodeDepth=2
DefaultPage=/content/view/full/76827
MetaDataArray[author]=Florida State University, The Center for Multicultural Affairs
MetaDataArray[keywords]=Florida State University, The Center for Multicultural Affairs 
MetaDataArray[description]=Florida State University, The Center for Multicultural Affairs 
MetaDataArray[classification]=Florida State University, The Center for Multicultural Affairs 

[UserSettings]
RegistrationEmail=

[SiteAccessSettings]
RequireUserLogin=false
RelatedSiteAccessList[]=multicultural_affairs
RelatedSiteAccessList[]=fsu_admin
ShowHiddenNodes=false
PathPrefix=The-Center-for-Multicultural-Affairs

[DesignSettings]
SiteDesign=multicultural_affairs
AdditionalSiteDesignList[]=ezfind
AdditionalSiteDesignList[]=fsu-v01
AdditionalSiteDesignList[]=ezwebin
AdditionalSiteDesignList[]=base

[SearchSettings]
SearchEngine=ezsolr

*/ ?>