<?php /* #?ini charset="utf-8"?

[InformationCollectionSettings]
EmailReceiver=

[Session]
SessionNamePerSiteAccess=disabled

[SiteSettings]
SiteName=OMNI Training
SiteURL=omni.training.fsu.edu
LoginPage=embedded
IndexPage=/content/view/full/3866/
RootNodeDepth=2
DefaultPage=/content/view/full/3866/
MetaDataArray[author]=/Training for OMNI
MetaDataArray[keywords]=enterprise, resource, planning, florida, training, omni

[UserSettings]
RegistrationEmail=

[SiteAccessSettings]
RequireUserLogin=false
RelatedSiteAccessList[]=training_omni
RelatedSiteAccessList[]=fsu_admin
ShowHiddenNodes=false
PathPrefix=OMNI-Training
PathPrefixExclude[]
PathPrefixExclude[]=Media
PathPrefixExclude[]=Users
PathPrefixExclude[]=Flash-Rotation-Articles
PathPrefixExclude[]=Test-Area

[DesignSettings]
SiteDesign=training_omni
AdditionalSiteDesignList[]=fsudesign
AdditionalSiteDesignList[]=ezwebin
AdditionalSiteDesignList[]=base

[RegionalSettings]
Locale=eng-US
ContentObjectLocale=eng-US
ShowUntranslatedObjects=disabled
SiteLanguageList[]=eng-US
TextTranslation=disabled

[FileSettings]
VarDir=var/ezwebin_site

[ContentSettings]
TranslationList=

[MailSettings]
AdminEmail=web@ucs.fsu.edu
EmailSender=

[DebugSettings]
DebugOutput=enabled
DebugByUser=enabled
DebugRedirection=disabled
DebugUserIDList[]=195
DebugUserIDList[]=14

[TemplateSettings]
Debug=disabled
ShowXHTMLCode=enabled
ShowUsedTemplates=enabled

[DatabaseSettings]
DatabaseImplementation=ezmysql
Server=localhost
Port=
User=root
Password=t6ntnimB
Database=fsu_livedata
Charset=
Socket=disabled
SQLOutput=disabled
*/ ?>
