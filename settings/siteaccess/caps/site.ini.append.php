<?php /* #?ini charset="utf-8"?

[InformationCollectionSettings]
EmailReceiver=

[SiteSettings]
SiteShortName=CAPS
SiteName=Center for Advanced Power Systems
SiteURL=ez.fsu.edu
LoginPage=embedded
IndexPage=/content/view/full/4530
RootNodeDepth=2
DefaultPage=/content/view/full/4530
MetaDataArray[author]=Florida State University
MetaDataArray[keywords]=education, post-secondary florida state university, 
MetaDataArray[description]=Florida State University is a Carnegie Doctoral/Research 
MetaDataArray[classification]=Continuing Education, Professional Development, 

[UserSettings]
RegistrationEmail=

[SiteAccessSettings]
RequireUserLogin=false
RelatedSiteAccessList[]=caps
RelatedSiteAccessList[]=fsu_admin
ShowHiddenNodes=false
PathPrefix=CAPS

[DesignSettings]
SiteDesign=caps
AdditionalSiteDesignList[]=ezfind
AdditionalSiteDesignList[]=fsu-v01
AdditionalSiteDesignList[]=ezwebin
AdditionalSiteDesignList[]=base

[SearchSettings]
SearchEngine=ezsolr

*/ ?>