<?php /* #?ini charset="utf-8"?

[InformationCollectionSettings]
EmailReceiver=

[SiteSettings]
SiteShortName=Governmental Relations
SiteName=Florida State University Governmental Relations
SiteURL=govrel.ezpublish2.fsu.edu
LoginPage=embedded
IndexPage=/content/view/full/16820
RootNodeDepth=2
DefaultPage=/content/view/full/16820
MetaDataArray[author]=Florida State University Governmental Relations
MetaDataArray[keywords]=education, post-secondary florida state university, graduate school, career advancement, Government Relations
MetaDataArray[description]=Florida State University is a Carnegie Doctoral/Research 
MetaDataArray[classification]=Continuing Education, Professional Development, Government Relations

[UserSettings]
RegistrationEmail=

[SiteAccessSettings]
RequireUserLogin=false
RelatedSiteAccessList[]=govrel
RelatedSiteAccessList[]=fsu_admin
ShowHiddenNodes=false
PathPrefix=Governmental-Relations

[DesignSettings]
SiteDesign=govrel
AdditionalSiteDesignList[]=fsudesign
AdditionalSiteDesignList[]=ezwebin
AdditionalSiteDesignList[]=base

*/ ?>