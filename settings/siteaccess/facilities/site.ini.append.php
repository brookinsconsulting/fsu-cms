<?php /* #?ini charset="utf-8"?

[InformationCollectionSettings]
EmailReceiver=

[SiteSettings]
SiteShortName=Facilities
SiteName=Florida State University Facilities
SiteURL=facilities.ezpublish2.fsu.edu
LoginPage=embedded
IndexPage=/content/view/full/16816
RootNodeDepth=2
DefaultPage=/content/view/full/16816
MetaDataArray[author]=Florida State University Facilities
MetaDataArray[keywords]=education, post-secondary florida state university, graduate school, career advancement, 
MetaDataArray[description]=Florida State University is a Carnegie Doctoral/Research 
MetaDataArray[classification]=Continuing Education, Professional Development, 

[UserSettings]
RegistrationEmail=

[SiteAccessSettings]
RequireUserLogin=false
RelatedSiteAccessList[]=facilities
RelatedSiteAccessList[]=fsu_admin
ShowHiddenNodes=false
PathPrefix=Facilities

[DesignSettings]
SiteDesign=facilities
AdditionalSiteDesignList[]=fsu-v01
AdditionalSiteDesignList[]=ezwebin
AdditionalSiteDesignList[]=base

*/ ?>