<?php /* #?ini charset="utf-8"?

[InformationCollectionSettings]
EmailReceiver=

[SiteSettings]
SiteShortName=Guide to Computing Resources 
SiteName=Florida State University Guide to Computing Resources 
SiteURL=gtcr.fsu.edu
LoginPage=embedded
IndexPage=/content/view/full/16815
RootNodeDepth=2
DefaultPage=/content/view/full/16815
MetaDataArray[author]=Florida State University Guide to Computing Resources 
MetaDataArray[keywords]=education, post-secondary florida state university, graduate school, career advancement, 
MetaDataArray[description]=Florida State University is a Carnegie Doctoral/Research 
MetaDataArray[classification]=Continuing Education, Professional Development, 

[UserSettings]
RegistrationEmail=

[SiteAccessSettings]
RequireUserLogin=false
RelatedSiteAccessList[]=gtcr
RelatedSiteAccessList[]=fsu_admin
ShowHiddenNodes=false
PathPrefix=Guide-to-Computing-Resources

[DesignSettings]
SiteDesign=gtcr
AdditionalSiteDesignList[]=fsudesign
AdditionalSiteDesignList[]=ezwebin
AdditionalSiteDesignList[]=base

*/ ?>