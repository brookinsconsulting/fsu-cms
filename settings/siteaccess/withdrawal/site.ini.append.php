<?php /* #?ini charset="utf-8"?

[InformationCollectionSettings]
EmailReceiver=

[SiteSettings]
SiteShortName=Withdrawals
SiteName=Office of Withdrawal Services
SiteURL=ez.fsu.edu
LoginPage=embedded
IndexPage=/content/view/full/91707
RootNodeDepth=2
DefaultPage=/content/view/full/91707
MetaDataArray[author]=Florida State University
MetaDataArray[keywords]=education, post-secondary florida state university, Office of Withdrawal Services, withdrawal, withdrawals
MetaDataArray[description]=Florida State University is a Carnegie Doctoral/Research 
MetaDataArray[classification]=Continuing Education, Professional Development, 

[UserSettings]
RegistrationEmail=

[SiteAccessSettings]
RequireUserLogin=false
RelatedSiteAccessList[]=withdrawal
RelatedSiteAccessList[]=fsu_admin
ShowHiddenNodes=false
PathPrefix=Office-of-Withdrawal-Services

[DesignSettings]
SiteDesign=withdrawal
AdditionalSiteDesignList[]=ezfind
AdditionalSiteDesignList[]=fsu-v01
AdditionalSiteDesignList[]=ezwebin
AdditionalSiteDesignList[]=base

[SearchSettings]
SearchEngine=ezsolr

*/ ?>