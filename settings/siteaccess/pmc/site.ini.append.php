<?php /* #?ini charset="utf-8"?

[InformationCollectionSettings]
EmailReceiver=

[SiteSettings]
SiteShortName=PMC
SiteName=Project Management Center 
SiteURL=pmc.comm.ezpublish2.fsu.edu
LoginPage=embedded
IndexPage=/content/view/full/6840
RootNodeDepth=2
DefaultPage=/content/view/full/6840
MetaDataArray[author]=Florida State University Project Management Center, Communications
MetaDataArray[keywords]=project management center,florida state university, communications
MetaDataArray[description]=Project Management Center 
MetaDataArray[classification]=Project Management Center

[UserSettings]
RegistrationEmail=

[SiteAccessSettings]
RequireUserLogin=false
RelatedSiteAccessList[]=pmc
RelatedSiteAccessList[]=fsu_admin
ShowHiddenNodes=false
PathPrefix=CCI-School-of-Communication

[DesignSettings]
SiteDesign=pmc
AdditionalSiteDesignList[]=fsudesign
AdditionalSiteDesignList[]=ezwebin
AdditionalSiteDesignList[]=base

*/ ?>