<?php /* #?ini charset="utf-8"?

[InformationCollectionSettings]
EmailReceiver=

[SiteSettings]
SiteShortName=Human Sciences
SiteName=College of Human Sciences 
SiteURL=chs.ezpublish2.fsu.edu
LoginPage=embedded
IndexPage=/content/view/full/43391
RootNodeDepth=2
DefaultPage=/content/view/full/43391
MetaDataArray[author]=Florida State University College of Human Sciences
MetaDataArray[keywords]=education, post-secondary florida state university, graduate school, career advancement, 
MetaDataArray[description]=Florida State University is a Carnegie Doctoral/Research 
MetaDataArray[classification]=Continuing Education, Professional Development, 

[UserSettings]
RegistrationEmail=

[SiteAccessSettings]
RequireUserLogin=false
RelatedSiteAccessList[]=chs
RelatedSiteAccessList[]=fsu_admin
ShowHiddenNodes=false
PathPrefix=College-of-Human-Sciences

[DesignSettings]
SiteDesign=chs
AdditionalSiteDesignList[]=ezfind
AdditionalSiteDesignList[]=fsu-v02
AdditionalSiteDesignList[]=ezwebin
AdditionalSiteDesignList[]=base

[SearchSettings]
SearchEngine=ezsolr

*/ ?>