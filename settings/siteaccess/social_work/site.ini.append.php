<?php /* #?ini charset="utf-8"?

[InformationCollectionSettings]
EmailReceiver=

[SiteSettings]
SiteShortName=Social Work
SiteName=College of Social Work
SiteURL=ez.fsu.edu
LoginPage=embedded
IndexPage=/content/view/full/94773
RootNodeDepth=2
DefaultPage=/content/view/full/94773
MetaDataArray[author]=Florida State University
MetaDataArray[keywords]=education, post-secondary florida state university, 
MetaDataArray[description]=Florida State University is a Carnegie Doctoral/Research 
MetaDataArray[classification]=Continuing Education, Professional Development, 

[UserSettings]
RegistrationEmail=

[SiteAccessSettings]
RequireUserLogin=false
RelatedSiteAccessList[]=social_work
RelatedSiteAccessList[]=fsu_admin
ShowHiddenNodes=false
PathPrefix=College-of-Social-Work

[DesignSettings]
SiteDesign=social_work
AdditionalSiteDesignList[]=ezfind
AdditionalSiteDesignList[]=fsu-v01
AdditionalSiteDesignList[]=ezwebin
AdditionalSiteDesignList[]=base

[SearchSettings]
SearchEngine=ezsolr

*/ ?>