<?php /* #?ini charset="utf-8"?

[InformationCollectionSettings]
EmailReceiver=

[SiteSettings]
SiteShortName=TEC
SiteName=Technology Enhanced Classrooms
SiteURL=condortecad.ez.fsu.edu
LoginPage=embedded
IndexPage=/content/view/full/100905
RootNodeDepth=2
DefaultPage=/content/view/full/100905
MetaDataArray[author]=Florida State University
MetaDataArray[keywords]=Florida State University, Technology, Enhanced, Classroom, TEC, ITS, Information, Services
MetaDataArray[description]=Florida State University is a Carnegie Doctoral/Research 
MetaDataArray[classification]=Continuing Education, Professional Development, 

[UserSettings]
RegistrationEmail=

[SiteAccessSettings]
RequireUserLogin=false
RelatedSiteAccessList[]=tec
RelatedSiteAccessList[]=fsu_admin
ShowHiddenNodes=false
PathPrefix=TEC

[DesignSettings]
SiteDesign=tec
AdditionalSiteDesignList[]=ezfind
AdditionalSiteDesignList[]=fsu-v01
AdditionalSiteDesignList[]=ezwebin
AdditionalSiteDesignList[]=base

[SearchSettings]
SearchEngine=ezsolr

*/ ?>