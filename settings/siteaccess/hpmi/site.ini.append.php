<?php /* #?ini charset="utf-8"?

[InformationCollectionSettings]
EmailReceiver=

[SiteSettings]
SiteShortName=HPMI
SiteName=High-Performance Materials Institute
SiteURL=ez.fsu.edu
LoginPage=embedded
IndexPage=/content/view/full/80140
RootNodeDepth=2
DefaultPage=/content/view/full/80140
MetaDataArray[author]=Florida State University
MetaDataArray[keywords]=education, post-secondary florida state university, 
MetaDataArray[description]=Florida State University is a Carnegie Doctoral/Research 
MetaDataArray[classification]=Continuing Education, Professional Development, 

[UserSettings]
RegistrationEmail=

[SiteAccessSettings]
RequireUserLogin=false
RelatedSiteAccessList[]=hpmi
RelatedSiteAccessList[]=fsu_admin
ShowHiddenNodes=false
PathPrefix=HPMI

[DesignSettings]
SiteDesign=hpmi
AdditionalSiteDesignList[]=ezfind
AdditionalSiteDesignList[]=fsu-v01
AdditionalSiteDesignList[]=ezwebin
AdditionalSiteDesignList[]=base

[SearchSettings]
SearchEngine=ezsolr

*/ ?>