<?php /* #?ini charset="utf-8"?

[InformationCollectionSettings]
EmailReceiver=

[SiteSettings]
SiteShortName=Department of Philosophy
SiteName=Florida State University Department of Philosophy 
SiteURL=philosophy.fsu.edu
LoginPage=embedded
IndexPage=/content/view/full/25565
RootNodeDepth=2
DefaultPage=/content/view/full/25565
MetaDataArray[author]=Florida State University Department of Philosophy
MetaDataArray[keywords]=education, post-secondary florida state university, philosophy, 
MetaDataArray[description]=Florida State University is a Carnegie Doctoral/Research 
MetaDataArray[classification]=Continuing Education, Professional Development, 

[UserSettings]
RegistrationEmail=

[SiteAccessSettings]
RequireUserLogin=false
RelatedSiteAccessList[]=philosophy
RelatedSiteAccessList[]=fsu_admin
ShowHiddenNodes=false
PathPrefix=Department-of-Philosophy

[DesignSettings]
SiteDesign=philosophy
AdditionalSiteDesignList[]=fsudesign
AdditionalSiteDesignList[]=ezwebin
AdditionalSiteDesignList[]=base

[DebugSettings]
DebugOutput=enabled
DebugRedirection=disabled

[TemplateSettings]
Debug=disabled
ShowXHTMLCode=enabled
ShowUsedTemplates=enabled

[DatabaseSettings]
SQLOutput=disabled
*/ ?>