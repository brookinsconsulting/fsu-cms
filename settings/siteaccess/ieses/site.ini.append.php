<?php /* #?ini charset="utf-8"?

[InformationCollectionSettings]
EmailReceiver=

[SiteSettings]
SiteShortName=IESES
SiteName=IESES 
SiteURL=ieses.ezpublish2.fsu.edu
LoginPage=embedded
IndexPage=/content/view/full/4797
RootNodeDepth=2
DefaultPage=/content/view/full/4797
MetaDataArray[author]=IESES, Institute for Energy Systems, Economic and Sustainability, florida state university
MetaDataArray[keywords]=IESES, Institute for Energy Systems, Economic and Sustainability, florida state university 
MetaDataArray[description]=IESES, Institute for Energy Systems, Economic and Sustainability, florida state university
MetaDataArray[classification]=IESES, Institute for Energy Systems, Economic and Sustainability, florida state university 

[UserSettings]
RegistrationEmail=

[SiteAccessSettings]
RequireUserLogin=false
RelatedSiteAccessList[]=ieses
RelatedSiteAccessList[]=fsu_admin
ShowHiddenNodes=false
PathPrefix=IESES

[DesignSettings]
SiteDesign=ieses
AdditionalSiteDesignList[]=fsu-v01
AdditionalSiteDesignList[]=ezwebin
AdditionalSiteDesignList[]=base

[SearchSettings]
SearchEngine=ezsolr

*/ ?>