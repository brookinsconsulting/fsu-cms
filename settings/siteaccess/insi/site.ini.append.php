<?php /* #?ini charset="utf-8"?

[InformationCollectionSettings]
EmailReceiver=

[SiteSettings]
SiteShortName=INSI
SiteName=Integrative NanoScience Institute
SiteURL=insi.ez.fsu.edu
LoginPage=embedded
IndexPage=/content/view/full/62925
RootNodeDepth=2
DefaultPage=/content/view/full/62925
MetaDataArray[author]=Florida State University
MetaDataArray[keywords]=Integrative NanoScience Institute, Florida State University, Science 
MetaDataArray[description]=Integrative NanoScience Institute, Florida State University, Science
MetaDataArray[classification]=Integrative NanoScience Institute, Florida State University, Science 

[UserSettings]
RegistrationEmail=

[SiteAccessSettings]
RequireUserLogin=false
RelatedSiteAccessList[]=insi
RelatedSiteAccessList[]=fsu_admin
ShowHiddenNodes=false
PathPrefix=Integrative-NanoScience-Institute

[DesignSettings]
SiteDesign=insi
AdditionalSiteDesignList[]=ezfind
AdditionalSiteDesignList[]=fsu-v01
AdditionalSiteDesignList[]=ezwebin
AdditionalSiteDesignList[]=base

[SearchSettings]
SearchEngine=ezsolr

*/ ?>