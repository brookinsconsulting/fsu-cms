<?php /* #?ini charset="utf-8"?

[InformationCollectionSettings]
EmailReceiver=

[SiteSettings]
SiteShortName=Institute of Molecular Biophysics
SiteName=Institute of Molecular Biophysics 
SiteURL=sb.ez.fsu.edu
LoginPage=embedded
IndexPage=/content/view/full/52808
RootNodeDepth=2
DefaultPage=/content/view/full/52808
MetaDataArray[author]=Florida State University Institute of Molecular Biophysics
MetaDataArray[keywords]=education, post-secondary florida state university, graduate school, career advancement, 
MetaDataArray[description]=Florida State University is a Carnegie Doctoral/Research 
MetaDataArray[classification]=Continuing Education, Professional Development, 

[UserSettings]
RegistrationEmail=

[SiteAccessSettings]
RequireUserLogin=false
RelatedSiteAccessList[]=imb
RelatedSiteAccessList[]=fsu_admin
ShowHiddenNodes=false
PathPrefix=Institute-of-Molecular-Biophysics

[DesignSettings]
SiteDesign=imb
AdditionalSiteDesignList[]=ezfind
AdditionalSiteDesignList[]=fsu-v01
AdditionalSiteDesignList[]=ezwebin
AdditionalSiteDesignList[]=base

[SearchSettings]
SearchEngine=ezsolr

*/ ?>