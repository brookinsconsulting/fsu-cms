<?php /* #?ini charset="utf-8"?

[InformationCollectionSettings]
EmailReceiver=

[SiteSettings]
SiteShortName=otc
SiteName=otc
SiteURL=otc.ezpublish2.fsu.edu
LoginPage=embedded
IndexPage=/content/view/full/20222
RootNodeDepth=2
DefaultPage=/content/view/full/20222
MetaDataArray[author]=Office of Telecommunications
MetaDataArray[keywords]=education, post-secondary florida state university, graduate school, career advancement, 
MetaDataArray[description]=Florida State University is a Carnegie Doctoral/Research 
MetaDataArray[classification]=Continuing Education, Professional Development, 

[UserSettings]
RegistrationEmail=

[SiteAccessSettings]
RequireUserLogin=false
RelatedSiteAccessList[]=otc
RelatedSiteAccessList[]=fsu_admin
ShowHiddenNodes=false
PathPrefix=Communications

[DesignSettings]
SiteDesign=otc
AdditionalSiteDesignList[]=fsudesign
AdditionalSiteDesignList[]=ezwebin
AdditionalSiteDesignList[]=base

*/ ?>