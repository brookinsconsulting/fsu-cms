<?php /* #?ini charset="utf-8"?

[InformationCollectionSettings]
EmailReceiver=

[SiteSettings]
SiteShortName=University Communications
SiteName=Florida State University University Communications
SiteURL=unicomm.ezpublish2.fsu.edu
LoginPage=embedded
IndexPage=/content/view/full/16818
RootNodeDepth=2
DefaultPage=/content/view/full/16818
MetaDataArray[author]=Florida State University University Communications
MetaDataArray[keywords]=education, post-secondary florida state university, graduate school, career advancement, University Communications
MetaDataArray[description]=Florida State University is a Carnegie Doctoral/Research 
MetaDataArray[classification]=Continuing Education, Professional Development, University Communications

[UserSettings]
RegistrationEmail=

[SiteAccessSettings]
RequireUserLogin=false
RelatedSiteAccessList[]=unicomm
RelatedSiteAccessList[]=fsu_admin
ShowHiddenNodes=false
PathPrefix=University-Communications

[DesignSettings]
SiteDesign=unicomm
AdditionalSiteDesignList[]=fsudesign
AdditionalSiteDesignList[]=ezwebin
AdditionalSiteDesignList[]=base

*/ ?>