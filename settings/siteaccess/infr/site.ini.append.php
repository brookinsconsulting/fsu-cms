<?php /* #?ini charset="utf-8"?

[InformationCollectionSettings]
EmailReceiver=

[SiteSettings]
SiteShortName=INFR
SiteName=Institute on Napoleon and the French Revolution 
SiteURL=ez.fsu.edu
LoginPage=embedded
IndexPage=/content/view/full/82712
RootNodeDepth=2
DefaultPage=/content/view/full/82712
MetaDataArray[author]=Florida State University
MetaDataArray[keywords]=Institute, Napoleon, French, Revolution, education, post-secondary florida state university, 
MetaDataArray[description]=Florida State University is a Carnegie Doctoral/Research 
MetaDataArray[classification]=Continuing Education, Professional Development, 

[UserSettings]
RegistrationEmail=

[SiteAccessSettings]
RequireUserLogin=false
RelatedSiteAccessList[]=infr
RelatedSiteAccessList[]=fsu_admin
ShowHiddenNodes=false
PathPrefix=INFR

[DesignSettings]
SiteDesign=infr
AdditionalSiteDesignList[]=ezfind
AdditionalSiteDesignList[]=fsu-v01
AdditionalSiteDesignList[]=ezwebin
AdditionalSiteDesignList[]=base

[SearchSettings]
SearchEngine=ezsolr

*/ ?>