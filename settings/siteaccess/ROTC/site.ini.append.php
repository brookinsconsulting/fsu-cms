<?php /* #?ini charset="utf-8"?

[InformationCollectionSettings]
EmailReceiver=

[SiteSettings]
SiteShortName=ROTC
SiteName=ROTC
SiteURL=rotc.ezpublish2.fsu.edu
LoginPage=embedded
IndexPage=/content/view/full/35072
RootNodeDepth=2
DefaultPage=/content/view/full/35072
MetaDataArray[author]=Florida State University ROTC
MetaDataArray[keywords]=education, post-secondary florida state university, ROTC, Army, Air Force, career advancement, 
MetaDataArray[description]=Florida State University is a Carnegie Doctoral/Research 
MetaDataArray[classification]=Continuing Education, Professional Development, 

[UserSettings]
RegistrationEmail=

[SiteAccessSettings]
RequireUserLogin=false
RelatedSiteAccessList[]=ROTC
RelatedSiteAccessList[]=fsu_admin
ShowHiddenNodes=false
PathPrefix=ROTC

[DesignSettings]
SiteDesign=ROTC
AdditionalSiteDesignList[]=fsudesign
AdditionalSiteDesignList[]=ezwebin
AdditionalSiteDesignList[]=base

*/ ?>