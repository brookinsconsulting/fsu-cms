<?php /* #?ini charset="utf-8"?

[InformationCollectionSettings]
EmailReceiver=

[SiteSettings]
SiteShortName=PIH
SiteName=Program in Interdisciplinary Humanities
SiteURL=pih.ez.fsu.edu
LoginPage=embedded
IndexPage=/content/view/full/96967
RootNodeDepth=2
DefaultPage=/content/view/full/96967
MetaDataArray[author]=Florida State University, Program in Interdisciplinary Humanities
MetaDataArray[keywords]=Florida State University, Program in Interdisciplinary Humanities 
MetaDataArray[description]=Florida State University, Program in Interdisciplinary Humanities 
MetaDataArray[classification]=Florida State University, Program in Interdisciplinary Humanities 

[UserSettings]
RegistrationEmail=

[SiteAccessSettings]
RequireUserLogin=false
RelatedSiteAccessList[]=pih
RelatedSiteAccessList[]=fsu_admin
ShowHiddenNodes=false
PathPrefix=PIH

[DesignSettings]
SiteDesign=iph
AdditionalSiteDesignList[]=ezfind
AdditionalSiteDesignList[]=fsudesign
AdditionalSiteDesignList[]=ezwebin
AdditionalSiteDesignList[]=base

[SearchSettings]
SearchEngine=ezsolr

*/ ?>