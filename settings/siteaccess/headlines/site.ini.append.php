<?php /* #?ini charset="utf-8"?

[InformationCollectionSettings]
EmailReceiver=

[SiteSettings]
SiteShortName=Headlines
SiteName=Florida State Headlines Around the World
SiteURL=headlines.ez.fsu.edu
LoginPage=embedded
IndexPage=/content/view/full/4895/
RootNodeDepth=2
DefaultPage=/content/view/full/4895/
MetaDataArray[author]=Headlines
MetaDataArray[keywords]=school, florida state university, headlines around the world, news, stories 

[UserSettings]
RegistrationEmail=

[SiteAccessSettings]
RequireUserLogin=false
RelatedSiteAccessList[]=headlines
RelatedSiteAccessList[]=fsu_admin
ShowHiddenNodes=false
PathPrefix=Florida-State-Headlines-Around-the-World

[DesignSettings]
SiteDesign=headlines
AdditionalSiteDesignList[]=fsu-v02
AdditionalSiteDesignList[]=ezwebin
AdditionalSiteDesignList[]=base

[DebugSettings]
DebugOutput=enabled
DebugRedirection=disabled

[TemplateSettings]
Debug=disabled
ShowXHTMLCode=enabled
ShowUsedTemplates=enabled

[DatabaseSettings]
SQLOutput=disabled

*/ ?>