<?php /* #?ini charset="utf-8"?

[InformationCollectionSettings]
EmailReceiver=

[SiteSettings]
SiteShortName=Office of Postdoctoral Affairs
SiteName=Office of Postdoctoral Affairs
SiteURL=opda.ez.fsu.edu
LoginPage=embedded
IndexPage=/content/view/full/90174
RootNodeDepth=2
DefaultPage=/content/view/full/90174
MetaDataArray[author]=Florida State University, Office of Postdoctoral Affairs
MetaDataArray[keywords]=Florida State University, Office of Postdoctoral Affairs
MetaDataArray[description]=Florida State University, Office of Postdoctoral Affairs
MetaDataArray[classification]=Florida State University, Office of Postdoctoral Affairs

[UserSettings]
RegistrationEmail=

[SiteAccessSettings]
RequireUserLogin=false
RelatedSiteAccessList[]=opda
RelatedSiteAccessList[]=fsu_admin
ShowHiddenNodes=false
PathPrefix=Office-of-Postdoctoral-Affairs

[DesignSettings]
SiteDesign=opda
AdditionalSiteDesignList[]=ezfind
AdditionalSiteDesignList[]=fsu-v01
AdditionalSiteDesignList[]=ezwebin
AdditionalSiteDesignList[]=base

[SearchSettings]
SearchEngine=ezsolr

*/ ?>