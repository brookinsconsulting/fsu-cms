<?php /* #?ini charset="utf-8"?

[InformationCollectionSettings]
EmailReceiver=

[SiteSettings]
SiteShortName=Campus Recreation
SiteName=Campus Recreation
SiteURL=campusrecez.fsu.edu
LoginPage=embedded
IndexPage=/content/view/full/76832
RootNodeDepth=2
DefaultPage=/content/view/full/76832
MetaDataArray[author]=Florida State University, Campus Recreation
MetaDataArray[keywords]=Florida State University, Campus Recreation 
MetaDataArray[description]=Florida State University, Campus Recreation 
MetaDataArray[classification]=Florida State University, Campus Recreation

[UserSettings]
RegistrationEmail=

[SiteAccessSettings]
RequireUserLogin=false
RelatedSiteAccessList[]=campus_recreation
RelatedSiteAccessList[]=fsu_admin
ShowHiddenNodes=false
PathPrefix=Campus-Recreation

[DesignSettings]
SiteDesign=campus_recreation
AdditionalSiteDesignList[]=ezfind
AdditionalSiteDesignList[]=fsudesign
AdditionalSiteDesignList[]=ezwebin
AdditionalSiteDesignList[]=base

[SearchSettings]
SearchEngine=ezsolr

*/ ?>