<?php /* #?ini charset="utf-8"?

[InformationCollectionSettings]
EmailReceiver=

[SiteSettings]
SiteShortName=History of Text Technologies
SiteName=History of Text Technologies 
SiteURL=hott.ezpublish2.fsu.edu
LoginPage=embedded
IndexPage=/content/view/full/20279
RootNodeDepth=2
DefaultPage=/content/view/full/20279
MetaDataArray[author]=Florida State University History of Text Technologies
MetaDataArray[keywords]=education, florida state university, History of Text Technologies
MetaDataArray[description]=Florida State University History of Text Technologies
MetaDataArray[classification]=History of Text Technologies 

[UserSettings]
RegistrationEmail=

[SiteAccessSettings]
RequireUserLogin=false
RelatedSiteAccessList[]=hott
RelatedSiteAccessList[]=fsu_admin
ShowHiddenNodes=false
PathPrefix=History-of-Text-Technologies

[DesignSettings]
SiteDesign=hott
AdditionalSiteDesignList[]=fsudesign
AdditionalSiteDesignList[]=ezwebin
AdditionalSiteDesignList[]=base

*/ ?>