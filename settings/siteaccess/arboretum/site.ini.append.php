<?php /* #?ini charset="utf-8"?

[InformationCollectionSettings]
EmailReceiver=

[SiteSettings]
SiteShortName=Arboretum
SiteName=Arboretum 
SiteURL=arboretum.facilities.ezpublish2.fsu.edu
LoginPage=embedded
IndexPage=/content/view/full/40447
RootNodeDepth=2
DefaultPage=/content/view/full/40447
MetaDataArray[author]=Arboretum
MetaDataArray[keywords]=education, post-secondary florida state university, arboretum, facilities, 
MetaDataArray[description]=Florida State University is a Carnegie Doctoral/Research 
MetaDataArray[classification]=Continuing Education, Professional Development, 

[UserSettings]
RegistrationEmail=

[SiteAccessSettings]
RequireUserLogin=false
RelatedSiteAccessList[]=arboretum
RelatedSiteAccessList[]=fsu_admin
ShowHiddenNodes=false
PathPrefix=Arboretum

[DesignSettings]
SiteDesign=arboretum
AdditionalSiteDesignList[]=fsudesign
AdditionalSiteDesignList[]=ezwebin
AdditionalSiteDesignList[]=base

*/ ?>