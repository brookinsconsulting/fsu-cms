<?php /* #?ini charset="utf-8"?

[InformationCollectionSettings]
EmailReceiver=

[SiteSettings]
SiteShortName=Alumni and Friends
SiteName=Alumni and Friends of Criminology 
SiteURL=afc.criminology.fsu.edu
LoginPage=embedded
IndexPage=/content/view/full/27981
RootNodeDepth=2
DefaultPage=/content/view/full/27981
MetaDataArray[author]=Florida State University College of Criminology and Criminal Justice
MetaDataArray[keywords]=education, post-secondary florida state university, graduate school, career advancement, 
MetaDataArray[description]=Florida State University is a Carnegie Doctoral/Research 
MetaDataArray[classification]=Continuing Education, Professional Development, 

[UserSettings]
RegistrationEmail=

[SiteAccessSettings]
RequireUserLogin=false
RelatedSiteAccessList[]=criminology_alumni
RelatedSiteAccessList[]=fsu_admin
ShowHiddenNodes=false
PathPrefix=Alumni-and-Friends-of-Criminology

[DesignSettings]
SiteDesign=criminology_alumni
AdditionalSiteDesignList[]=fsu-v01
AdditionalSiteDesignList[]=ezwebin
AdditionalSiteDesignList[]=base

*/ ?>