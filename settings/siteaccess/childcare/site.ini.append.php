<?php /* #?ini charset="utf-8"?

[InformationCollectionSettings]
EmailReceiver=

[SiteSettings]
SiteShortName=FSU Child Development Programs
SiteName=FSU Child Development Programs 
SiteURL=childcare.ez.fsu.edu
LoginPage=embedded
IndexPage=/content/view/full/55721
RootNodeDepth=2
DefaultPage=/content/view/full/55721
MetaDataArray[author]=Florida State University, FSU Child Development Programs
MetaDataArray[keywords]=Florida State University, FSU Child Development Programs 
MetaDataArray[description]=Florida State University, FSU Child Development Programs 
MetaDataArray[classification]=Florida State University, FSU Child Development Programs 

[UserSettings]
RegistrationEmail=

[SiteAccessSettings]
RequireUserLogin=false
RelatedSiteAccessList[]=childcare
RelatedSiteAccessList[]=fsu_admin
ShowHiddenNodes=false
PathPrefix=FSU-Child-Development-Programs

[DesignSettings]
SiteDesign=childcare
AdditionalSiteDesignList[]=ezfind
AdditionalSiteDesignList[]=fsu-v01
AdditionalSiteDesignList[]=ezwebin
AdditionalSiteDesignList[]=base

[SearchSettings]
SearchEngine=ezsolr

*/ ?>