<?php /* #?ini charset="utf-8"?

[StylesheetSettings]
SiteCSS=var/storage/packages/ez_systems/ezwebin_design/files/default/file/site-colors.css
ClassesCSS=var/storage/packages/ez_systems/ezwebin_design/files/default/file/classes-colors.css
CSSFileList[]
CSSFileList[]=StyleLevel2.css
CSSFileList[]=thickbox.css
CSSFileList[]=jquery-ui-1.7.custom.css
CSSFileList[]=fsucom.css

[JavaScriptSettings]
JavaScriptList[]
JavaScriptList[]=jquery-1.5.1.min.js
JavaScriptList[]=jquery.tools_1.2.5_all.min.js
#JavaScriptList[]=jquery.marquee.js
JavaScriptList[]=jquery.galleryScroll1.3.1.js
JavaScriptList[]=jquery.galleryScroll1.3.1-vertical.js
JavaScriptList[]=jquery-ui-1.7.custom.min.js
JavaScriptList[]=thickbox_compressed.js
JavaScriptList[]=cufon-yui.js
JavaScriptList[]=agaramond.js
JavaScriptList[]=jquery.corner.js
JavaScriptList[]=jquery.equalheights.js
JavaScriptList[]=site_custom.js

*/ ?>
