<?php /* #?ini charset="utf-8"?

[MenuSettings]
AvailableMenuArray[]
AvailableMenuArray[]=TopOnly
AvailableMenuArray[]=LeftOnly
AvailableMenuArray[]=DoubleTop
AvailableMenuArray[]=LeftTop
HideSideMenuClasses[]=frontpage
HideSideMenuClasses[]=blog
HideSideMenuClasses[]=blog_post
HideSideMenuClasses[]=ajax_calendar
HideSideMenuClasses[]=external_calendar
HideSideMenuClasses[]=article

[SelectedMenu]
CurrentMenu=LeftTop
TopMenu=flat_top
LeftMenu=flat_left

[TopOnly]
TitleText=Only top menu
MenuThumbnail=menu/top_only.jpg
TopMenu=flat_top
LeftMenu=

[LeftOnly]
TitleText=Left menu
MenuThumbnail=menu/left_only.jpg
TopMenu=
LeftMenu=flat_left

[DoubleTop]
TitleText=Double top menu
MenuThumbnail=menu/double_top.jpg
TopMenu=double_top
LeftMenu=

[LeftTop]
TitleText=Left and top
MenuThumbnail=menu/left_top.jpg
TopMenu=flat_top
LeftMenu=flat_left

[MenuContentSettings]
TopIdentifierList[]=folder
TopIdentifierList[]=feedback_form 
TopIdentifierList[]=video_upload
LeftIdentifierList[]=folder
LeftIdentifierList[]=feedback_form
LeftIdentifierList[]=nav_divider
LeftIdentifierList[]=ajax_calendar


*/ ?>