<?php /* #?ini charset="utf-8"?

[MenuContentSettings]
LeftIdentifierList[]=folder
LeftIdentifierList[]=feedback_form
LeftIdentifierList[]=gallery
LeftIdentifierList[]=forum
LeftIdentifierList[]=faq_list
LeftIdentifierList[]=info_page

[NavigationPart]
Part[ezcontentnavigationpart]=Content structure
Part[ezmedianavigationpart]=Media library
Part[ezusernavigationpart]=User accounts
Part[ezvisualnavigationpart]=Design
Part[ezsetupnavigationpart]=Setup
Part[ezmynavigationpart]=My account

[TopAdminMenu]
Tabs[]
Tabs[]=content
Tabs[]=media
Tabs[]=users
Tabs[]=design
Tabs[]=setup
Tabs[]=my_account
*/ ?>