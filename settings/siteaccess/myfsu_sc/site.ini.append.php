<?php /* #?ini charset="utf-8"?

[InformationCollectionSettings]
EmailReceiver=

[SiteSettings]
SiteShortName=myFSU Student Central
SiteName=myFSU Student Central
SiteURL=sc.my.fsu.edu
LoginPage=embedded
IndexPage=/content/view/full/2946
RootNodeDepth=2
DefaultPage=/content/view/full/2946
MetaDataArray[author]=Florida State University
MetaDataArray[keywords]=myFSU Student Central, Florida State University, student information system, online training resources, student accounts, faculty resources, enrollment
MetaDataArray[description]=Learn about myFSU Student Central, Florida State University’s student information system, and access online training resources for topics including enrollment, student accounts and faculty resources.
MetaDataArray[classification]=myFSU Student Central, Florida State University, student information system, online training resources, student accounts, faculty resources, enrollment

[UserSettings]
RegistrationEmail=

[SiteAccessSettings]
RequireUserLogin=false
RelatedSiteAccessList[]=myfsu_sc
RelatedSiteAccessList[]=fsu_admin
ShowHiddenNodes=false
PathPrefix=myFSU-Student-Central

[DesignSettings]
SiteDesign=myfsu
AdditionalSiteDesignList[]=ezfind
AdditionalSiteDesignList[]=fsu-v01
AdditionalSiteDesignList[]=ezwebin
AdditionalSiteDesignList[]=base

[SearchSettings]
SearchEngine=ezsolr

*/ ?>