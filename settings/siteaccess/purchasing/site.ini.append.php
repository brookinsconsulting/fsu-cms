<?php /* #?ini charset="utf-8"?

[InformationCollectionSettings]
EmailReceiver=

[SiteSettings]
SiteShortName=Purchasing Services
SiteName=Purchasing Services 
SiteURL=purchasing.ezpublish2.fsu.edu
LoginPage=embedded
IndexPage=/content/view/full/6872
RootNodeDepth=2
DefaultPage=/content/view/full/6872
MetaDataArray[author]=Purchasing Services
MetaDataArray[keywords]=purchasing services,florida state university, receiving, orders
MetaDataArray[description]=Purchasing Services 
MetaDataArray[classification]=Purchasing Services

[UserSettings]
RegistrationEmail=

[SiteAccessSettings]
RequireUserLogin=false
RelatedSiteAccessList[]=purchasing
RelatedSiteAccessList[]=fsu_admin
ShowHiddenNodes=false
PathPrefix=Purchasing-Services

[DesignSettings]
SiteDesign=purchasing
AdditionalSiteDesignList[]=fsudesign
AdditionalSiteDesignList[]=ezwebin
AdditionalSiteDesignList[]=base

[DebugSettings]
DebugOutput=disabled
DebugRedirection=disabled

[TemplateSettings]
Debug=disabled
ShowXHTMLCode=disabled
ShowUsedTemplates=disabled

[DatabaseSettings]
SQLOutput=disabled
*/ ?>