<?php /* #?ini charset="utf-8"?

[InformationCollectionSettings]
EmailReceiver=

[SiteSettings]
SiteShortName=OGC
SiteName=Office of the General Counsel
SiteURL=generalcounsel.ez.fsu.edu
LoginPage=embedded
IndexPage=/content/view/full/93578
RootNodeDepth=2
DefaultPage=/content/view/full/93578
MetaDataArray[author]=Florida State University
MetaDataArray[keywords]=education, post-secondary florida state university, 
MetaDataArray[description]=Florida State University is a Carnegie Doctoral/Research 
MetaDataArray[classification]=Continuing Education, Professional Development, 

[UserSettings]
RegistrationEmail=

[SiteAccessSettings]
RequireUserLogin=false
RelatedSiteAccessList[]=ogc
RelatedSiteAccessList[]=fsu_admin
ShowHiddenNodes=false
PathPrefix=OGC

[DesignSettings]
SiteDesign=ogc
AdditionalSiteDesignList[]=ezfind
AdditionalSiteDesignList[]=fsu-v01
AdditionalSiteDesignList[]=ezwebin
AdditionalSiteDesignList[]=base

[SearchSettings]
SearchEngine=ezsolr

*/ ?>