<?php /* #?ini charset="utf-8"?

[InformationCollectionSettings]
EmailReceiver=

[SiteSettings]
SiteShortName=Institute on World War II
SiteName=The Institute on World War II and the Human Experience
SiteURL=ww2.ezpublish2.fsu.edu
LoginPage=embedded
IndexPage=/content/view/full/21167
RootNodeDepth=2
DefaultPage=/content/view/full/21167
MetaDataArray[author]=Florida State University The Institute on World War II and the Human Experience
MetaDataArray[keywords]=Florida State University, The Institute on World War II and the Human Experience, Arts and Sciences
MetaDataArray[description]=Florida State University, The Institute on World War II and the Human Experience, Arts and Sciences 
MetaDataArray[classification]=Florida State University, The Institute on World War II and the Human Experience, Arts and Sciences

[UserSettings]
RegistrationEmail=

[SiteAccessSettings]
RequireUserLogin=false
RelatedSiteAccessList[]=ww2
RelatedSiteAccessList[]=fsu_admin
ShowHiddenNodes=false
PathPrefix=Institute-on-World-War-II

[DesignSettings]
SiteDesign=ww2
AdditionalSiteDesignList[]=fsudesign
AdditionalSiteDesignList[]=ezwebin
AdditionalSiteDesignList[]=base

[DebugSettings]
DebugOutput=enabled
DebugRedirection=disabled

[TemplateSettings]
Debug=disabled
ShowXHTMLCode=enabled
ShowUsedTemplates=enabled

[DatabaseSettings]
SQLOutput=disabled
*/ ?>