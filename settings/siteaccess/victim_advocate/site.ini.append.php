<?php /* #?ini charset="utf-8"?

[InformationCollectionSettings]
EmailReceiver=

[SiteSettings]
SiteShortName=Victim Advocate Program
SiteName=Victim Advocate Program
SiteURL=victimadvocate.ez.fsu.edu
LoginPage=embedded
IndexPage=/content/view/full/59593
RootNodeDepth=2
DefaultPage=/content/view/full/59593
MetaDataArray[author]=Florida State University
MetaDataArray[keywords]=Victim Advocate Program, Dean of Students Department
MetaDataArray[description]=Victim Advocate Program, Dean of Students Department 
MetaDataArray[classification]=Victim Advocate Program, Dean of Students Department 

[UserSettings]
RegistrationEmail=

[SiteAccessSettings]
RequireUserLogin=false
RelatedSiteAccessList[]=victim_advocate
RelatedSiteAccessList[]=fsu_admin
ShowHiddenNodes=false
PathPrefix=Victim-Advocate-Program

[DesignSettings]
SiteDesign=victim_advocate
AdditionalSiteDesignList[]=ezfind
AdditionalSiteDesignList[]=fsudesign
AdditionalSiteDesignList[]=ezwebin
AdditionalSiteDesignList[]=base

[SearchSettings]
SearchEngine=ezsolr

*/ ?>