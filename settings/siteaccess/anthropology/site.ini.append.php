<?php /* #?ini charset="utf-8"?

[InformationCollectionSettings]
EmailReceiver=

[SiteSettings]
SiteShortName=Anthropology Department
SiteName=Anthropology Department
SiteURL=anthro.ez.fsu.edu
LoginPage=embedded
IndexPage=/content/view/full/73247
RootNodeDepth=2
DefaultPage=/content/view/full/73247
MetaDataArray[author]=Florida State University, Anthropology Department
MetaDataArray[keywords]=Florida State University, Anthropology Department 
MetaDataArray[description]=Florida State University, Anthropology Department 
MetaDataArray[classification]=Florida State University, Anthropology Department 

[UserSettings]
RegistrationEmail=

[SiteAccessSettings]
RequireUserLogin=false
RelatedSiteAccessList[]=anthropology
RelatedSiteAccessList[]=fsu_admin
ShowHiddenNodes=false
PathPrefix=Anthropology-Department

[DesignSettings]
SiteDesign=anthropology
AdditionalSiteDesignList[]=ezfind
AdditionalSiteDesignList[]=fsu-v01
AdditionalSiteDesignList[]=ezwebin
AdditionalSiteDesignList[]=base

[SearchSettings]
SearchEngine=ezsolr

*/ ?>