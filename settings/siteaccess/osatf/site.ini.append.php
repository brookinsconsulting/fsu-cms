<?php /* #?ini charset="utf-8"?

[InformationCollectionSettings]
EmailReceiver=

[SiteSettings]
SiteShortName=OSATF
SiteName=Oil Spill Academic Task Force 
SiteURL=oilspill.ezpublish2.fsu.edu
LoginPage=embedded
IndexPage=/content/view/full/34834/
RootNodeDepth=2
DefaultPage=/content/view/full/34834/
MetaDataArray[author]=OSATF
MetaDataArray[keywords]=education, post-secondary florida state university, graduate school, career advancement, 
MetaDataArray[description]=Florida State University is a Carnegie Doctoral/Research 
MetaDataArray[classification]=Continuing Education, Professional Development, 

[UserSettings]
RegistrationEmail=

[SiteAccessSettings]
RequireUserLogin=false
RelatedSiteAccessList[]=osatf
RelatedSiteAccessList[]=fsu_admin
ShowHiddenNodes=false
PathPrefix=OSATF

[DesignSettings]
SiteDesign=osatf
AdditionalSiteDesignList[]=fsudesign
AdditionalSiteDesignList[]=ezwebin
AdditionalSiteDesignList[]=base

*/ ?>