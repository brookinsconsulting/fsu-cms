<?php /* #?ini charset="utf-8"?

[InformationCollectionSettings]
EmailReceiver=

[SiteSettings]
SiteShortName=Center for Hispanic Marketing Communication
SiteName=Center for Hispanic Marketing Communication
SiteURL=hmc.comm.fsu.edu
LoginPage=embedded
IndexPage=/content/view/full/2148
RootNodeDepth=2
DefaultPage=/content/view/full/2148
MetaDataArray[author]=Hispanic Marketing Center
MetaDataArray[keywords]=Hispanic Marketing Center, Communications, Florida Stated University
MetaDataArray[description]=Hispanic Marketing Center, Communications, Florida Stated University 
MetaDataArray[classification]=Hispanic Marketing Center, Communications, Florida Stated University, Online and classroom courses and research to better understand US Hispanic consumers

[UserSettings]
RegistrationEmail=

[SiteAccessSettings]
RequireUserLogin=false
RelatedSiteAccessList[]=hispanic_center
RelatedSiteAccessList[]=fsu_admin
ShowHiddenNodes=false
PathPrefix=Center-for-Hispanic-Marketing-Communication

[DesignSettings]
SiteDesign=hispanic_center
AdditionalSiteDesignList[]=ezfind
AdditionalSiteDesignList[]=fsudesign
AdditionalSiteDesignList[]=ezwebin
AdditionalSiteDesignList[]=base

[SearchSettings]
SearchEngine=ezsolr

*/ ?>