<?php /* #?ini charset="utf-8"?

[InformationCollectionSettings]
EmailReceiver=

[Session]
SessionNamePerSiteAccess=disabled

[SiteSettings]
SiteName=Hispanic Marketing Center
SiteURL=hmc.comm.ezpublish2.fsu.edu
LoginPage=embedded
IndexPage=/content/view/full/2148/
RootNodeDepth=2
DefaultPage=/content/view/full/2148/
MetaDataArray[author]=Hispanic Marketing Center
MetaDataArray[keywords]=school, florida, education, super-16, super-35, 35mm

[UserSettings]
RegistrationEmail=

[SiteAccessSettings]
RequireUserLogin=false
RelatedSiteAccessList[]=hispanic_center
RelatedSiteAccessList[]=fsu_admin
ShowHiddenNodes=false
PathPrefix=Center-for-Hispanic-Marketing-Communication
PathPrefixExclude[]
PathPrefixExclude[]=Media
PathPrefixExclude[]=Users
PathPrefixExclude[]=Flash-Rotation-Articles
PathPrefixExclude[]=Test-Area

[DesignSettings]
SiteDesign=hispanic_center
AdditionalSiteDesignList[]=fsu
AdditionalSiteDesignList[]=ezwebin
AdditionalSiteDesignList[]=base

[RegionalSettings]
Locale=eng-US
ContentObjectLocale=eng-US
ShowUntranslatedObjects=disabled
SiteLanguageList[]=eng-US
TextTranslation=disabled

[FileSettings]
VarDir=var/ezwebin_site

[ContentSettings]
TranslationList=

[MailSettings]
AdminEmail=web@ucs.fsu.edu
EmailSender=

[DebugSettings]
DebugOutput=enabled
DebugByUser=enabled
DebugRedirection=disabled
DebugUserIDList[]=195
DebugUserIDList[]=14

[TemplateSettings]
Debug=disabled
ShowXHTMLCode=enabled
ShowUsedTemplates=enabled

[DatabaseSettings]
DatabaseImplementation=ezmysql
Server=localhost
Port=
User=root
Password=t6ntnimB
Database=fsu_livedata
Charset=
Socket=disabled
SQLOutput=disabled
*/ ?>
