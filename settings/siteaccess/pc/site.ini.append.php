<?php /* #?ini charset="utf-8"?

[InformationCollectionSettings]
EmailReceiver=

[SiteSettings]
SiteShortName=Panama City Campus
SiteName=Panama City Campus
SiteURL=pc.fsu.edu
LoginPage=embedded
IndexPage=/content/view/full/16823
RootNodeDepth=2
DefaultPage=/content/view/full/16823
MetaDataArray[author]=Florida State University Panama City Campus
MetaDataArray[keywords]=education, post-secondary florida state university, graduate school, career advancement, Panama City Campus
MetaDataArray[description]=Florida State University is a Carnegie Doctoral/Research 
MetaDataArray[classification]=Continuing Education, Professional Development, Panama City Campus

[UserSettings]
RegistrationEmail=

[SiteAccessSettings]
RequireUserLogin=false
RelatedSiteAccessList[]=pc
RelatedSiteAccessList[]=fsu_admin
ShowHiddenNodes=false
PathPrefix=Panama-City-Campus

[DesignSettings]
SiteDesign=pc
AdditionalSiteDesignList[]=ezfind
AdditionalSiteDesignList[]=fsu-v01
AdditionalSiteDesignList[]=ezwebin
AdditionalSiteDesignList[]=base


[DebugSettings]
DebugOutput=enabled

[SearchSettings]
searchEngine=ezsolr

*/ ?>