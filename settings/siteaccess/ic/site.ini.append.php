<?php /* #?ini charset="utf-8"?

[InformationCollectionSettings]
EmailReceiver=

[SiteSettings]
SiteShortName=International Center
SiteName=Florida State University International Center
SiteURL=ic.ezpublish2.fsu.edu
LoginPage=embedded
IndexPage=/content/view/full/16814
RootNodeDepth=2
DefaultPage=/content/view/full/16814
MetaDataArray[author]=Florida State University International Center
MetaDataArray[keywords]=education, post-secondary florida state university, graduate school, career advancement, 
MetaDataArray[description]=Florida State University is a Carnegie Doctoral/Research 
MetaDataArray[classification]=Continuing Education, Professional Development, 

[UserSettings]
RegistrationEmail=

[SiteAccessSettings]
RequireUserLogin=false
RelatedSiteAccessList[]=ic
RelatedSiteAccessList[]=fsu_admin
ShowHiddenNodes=false
PathPrefix=International-Center

[DesignSettings]
SiteDesign=ic
AdditionalSiteDesignList[]=fsudesign
AdditionalSiteDesignList[]=ezwebin
AdditionalSiteDesignList[]=base

*/ ?>