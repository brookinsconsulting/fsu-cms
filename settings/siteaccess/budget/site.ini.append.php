<?php /* #?ini charset="utf-8"?

[DebugSettings]
DebugOutput=enabled
DebugRedirection=disabled

[InformationCollectionSettings]
EmailReceiver=

[SiteSettings]
SiteShortName=Budget Office
SiteName=Budget Office
SiteURL=budget.fsu.edu
LoginPage=embedded
IndexPage=/content/view/full/7019
RootNodeDepth=2
DefaultPage=/content/view/full/7019
MetaDataArray[author]=Budget Office
MetaDataArray[keywords]=Budget Office,florida state university
MetaDataArray[description]=Budget Office
MetaDataArray[classification]=Budget Office

[UserSettings]
RegistrationEmail=

[SiteAccessSettings]
RequireUserLogin=false
RelatedSiteAccessList[]=budget
RelatedSiteAccessList[]=fsu_admin
ShowHiddenNodes=false
PathPrefix=Budget-Office

[DesignSettings]
SiteDesign=budget
AdditionalSiteDesignList[]=fsu-v01
AdditionalSiteDesignList[]=ezwebin
AdditionalSiteDesignList[]=base

[TemplateSettings]
Debug=disabled
ShowXHTMLCode=enabled
ShowUsedTemplates=enabled

[DatabaseSettings]
SQLOutput=disabled
*/ ?>