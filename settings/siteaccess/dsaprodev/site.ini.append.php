<?php /* #?ini charset="utf-8"?

[InformationCollectionSettings]
EmailReceiver=

[SiteSettings]
SiteShortName=DSA S&P Dev
SiteName=Student Affairs Staff and Professional Development 
SiteURL=ez.fsu.edu
LoginPage=embedded
IndexPage=/content/view/full/71337
RootNodeDepth=2
DefaultPage=/content/view/full/71337
MetaDataArray[author]=Florida State University
MetaDataArray[keywords]=education, post-secondary florida state university, 
MetaDataArray[description]=Florida State University is a Carnegie Doctoral/Research 
MetaDataArray[classification]=Continuing Education, Professional Development, 

[UserSettings]
RegistrationEmail=

[SiteAccessSettings]
RequireUserLogin=false
RelatedSiteAccessList[]=dsaprodev
RelatedSiteAccessList[]=fsu_admin
ShowHiddenNodes=false
PathPrefix=Student-Affairs-Staff-and-Professional-Development 

[DesignSettings]
SiteDesign=dsaprodev
AdditionalSiteDesignList[]=ezfind
AdditionalSiteDesignList[]=fsu-v01
AdditionalSiteDesignList[]=ezwebin
AdditionalSiteDesignList[]=base

[SearchSettings]
SearchEngine=ezsolr

*/ ?>