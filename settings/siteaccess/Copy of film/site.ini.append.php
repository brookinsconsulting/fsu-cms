<?php /* #?ini charset="utf-8"?

[InformationCollectionSettings]
EmailReceiver=

[SiteSettings]
SiteName=The College of Motion Picture, Television and Recording Arts
SiteURL=film.fsu.edu
LoginPage=embedded
IndexPage=/content/view/full/1710/
RootNodeDepth=2
DefaultPage=/content/view/full/1710/
MetaDataArray[author]=Florida State University Film School
MetaDataArray[keywords]=film, school, florida, education, super-16, super-35, 35mm

SiteShortName=Film School
SiteRootNodeID=1710

[UserSettings]
RegistrationEmail=

[SiteAccessSettings]
RequireUserLogin=false
RelatedSiteAccessList[]=film
RelatedSiteAccessList[]=fsu_admin
ShowHiddenNodes=false
PathPrefix=Film-School

[DesignSettings]
SiteDesign=film_school
AdditionalSiteDesignList[]=fsu
AdditionalSiteDesignList[]=ezwebin
AdditionalSiteDesignList[]=base

*/ ?>
