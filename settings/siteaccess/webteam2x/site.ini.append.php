<?php /* #?ini charset="utf-8"?

[InformationCollectionSettings]
EmailReceiver=

[SiteSettings]
SiteShortName=UCS Web Team
SiteName=Florida State University - UCS Web Team
SiteURL=webteam.ezpublish2.fsu.edu
LoginPage=embedded
IndexPage=/content/view/full/2408
RootNodeDepth=2
DefaultPage=/content/view/full/2408
MetaDataArray[author]=Florida State University 
MetaDataArray[keywords]=education, post-secondary florida state university, graduate school, career advancement, 
MetaDataArray[description]=Florida State University is a Carnegie Doctoral/Research 
MetaDataArray[classification]=Continuing Education, Professional Development, 

[UserSettings]
RegistrationEmail=

[SiteAccessSettings]
RequireUserLogin=false
RelatedSiteAccessList[]=webteam2
RelatedSiteAccessList[]=fsu_admin
ShowHiddenNodes=false
PathPrefix=Web-Team-Test-Site

[DesignSettings]
SiteDesign=fsu
AdditionalSiteDesignList[]=fsu
AdditionalSiteDesignList[]=ezwebin
AdditionalSiteDesignList[]=base

*/ ?>