<?php /* #?ini charset="utf-8"?

[InformationCollectionSettings]
EmailReceiver=

[SiteSettings]
SiteShortName=The FSU Flying High Circus
SiteName=The FSU Flying High Circus
SiteURL=circus.ez.fsu.edu
LoginPage=embedded
IndexPage=/content/view/full/76819
RootNodeDepth=2
DefaultPage=/content/view/full/76819
MetaDataArray[author]=Florida State University, The FSU Flying High Circus
MetaDataArray[keywords]=Florida State University, The FSU Flying High Circus
MetaDataArray[description]=Florida State University, The FSU Flying High Circus
MetaDataArray[classification]=Florida State University, The FSU Flying High Circus 

[UserSettings]
RegistrationEmail=

[SiteAccessSettings]
RequireUserLogin=false
RelatedSiteAccessList[]=circus
RelatedSiteAccessList[]=fsu_admin
ShowHiddenNodes=false
PathPrefix=The-FSU-Flying-High-Circus

[DesignSettings]
SiteDesign=circus
AdditionalSiteDesignList[]=ezfind
AdditionalSiteDesignList[]=fsu-v01
AdditionalSiteDesignList[]=ezwebin
AdditionalSiteDesignList[]=base

[SearchSettings]
SearchEngine=ezsolr

*/ ?>