<?php /* #?ini charset="utf-8"?

[InformationCollectionSettings]
EmailReceiver=

[SiteSettings]
SiteShortName=Transportation & Parking
SiteName=Transportation & Parking
SiteURL=parking.ezpublish2.fsu.edu
LoginPage=embedded
IndexPage=/content/view/full/17912
RootNodeDepth=2
DefaultPage=/content/view/full/17912
MetaDataArray[author]=Parking & Transportation
MetaDataArray[keywords]=parking, transportation, florida state university 
MetaDataArray[description]=Florida State University  
MetaDataArray[classification]=parking, transportation, florida state university 

[UserSettings]
RegistrationEmail=

[SiteAccessSettings]
RequireUserLogin=false
RelatedSiteAccessList[]=transportation_parking
RelatedSiteAccessList[]=fsu_admin
ShowHiddenNodes=false
PathPrefix=Transportation-and-Parking-Services

[DesignSettings]
SiteDesign=transportation_parking
AdditionalSiteDesignList[]=fsudesign
AdditionalSiteDesignList[]=ezwebin
AdditionalSiteDesignList[]=base

*/ ?>