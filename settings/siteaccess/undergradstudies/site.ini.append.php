<?php /* #?ini charset="utf-8"?

[InformationCollectionSettings]
EmailReceiver=

[Session]
SessionNamePerSiteAccess=disabled

[SiteSettings]
SiteName=Division of Undergraduate Studies
SiteURL=undergrad.ezpublish2.fsu.edu
LoginPage=embedded
IndexPage=/content/view/full/3788/
RootNodeDepth=2
DefaultPage=/content/view/full/3788/
MetaDataArray[author]=Division of Undergraduate Studies
MetaDataArray[keywords]=school, florida, education, undergraduate, student, studies

[UserSettings]
RegistrationEmail=

[SiteAccessSettings]
RequireUserLogin=false
RelatedSiteAccessList[]=undergradstudies
RelatedSiteAccessList[]=fsu_admin
ShowHiddenNodes=false
PathPrefix=Undergraduate-Studies
PathPrefixExclude[]
PathPrefixExclude[]=Media
PathPrefixExclude[]=Users
PathPrefixExclude[]=Flash-Rotation-Articles
PathPrefixExclude[]=Test-Area

[DesignSettings]
SiteDesign=undergradstudies
AdditionalSiteDesignList[]=fsu-v01
AdditionalSiteDesignList[]=ezwebin
AdditionalSiteDesignList[]=base

[RegionalSettings]
Locale=eng-US
ContentObjectLocale=eng-US
ShowUntranslatedObjects=disabled
SiteLanguageList[]=eng-US
TextTranslation=disabled

[FileSettings]
VarDir=var/ezwebin_site

[ContentSettings]
TranslationList=

[MailSettings]
AdminEmail=web@ucs.fsu.edu
EmailSender=

[DebugSettings]
DebugOutput=enabled
DebugByUser=enabled
DebugRedirection=disabled
DebugUserIDList[]=195
DebugUserIDList[]=14

[TemplateSettings]
Debug=disabled
ShowXHTMLCode=enabled
ShowUsedTemplates=enabled

[DatabaseSettings]
DatabaseImplementation=ezmysql
Server=localhost
Port=
User=root
Password=t6ntnimB
Database=fsu_livedata
Charset=
Socket=disabled
SQLOutput=disabled
*/ ?>
