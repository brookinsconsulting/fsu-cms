<?php /* #?ini charset="utf-8"?

[InformationCollectionSettings]
EmailReceiver=

[SiteSettings]
SiteShortName=Askew Student Life Cinema 
SiteName=Askew Student Life Cinema 
SiteURL=movies.ez.fsu.edu
LoginPage=embedded
IndexPage=/content/view/full/67205
RootNodeDepth=2
DefaultPage=/content/view/full/67205
MetaDataArray[author]=Florida State University, Askew Student Life Cinema, movies
MetaDataArray[keywords]=Florida State University, Askew Student Life Cinema, movies 
MetaDataArray[description]=Florida State University, Askew Student Life Cinema, movies 
MetaDataArray[classification]=Florida State University, Askew Student Life Cinema, movies 

[UserSettings]
RegistrationEmail=

[SiteAccessSettings]
RequireUserLogin=false
RelatedSiteAccessList[]=movies
RelatedSiteAccessList[]=fsu_admin
ShowHiddenNodes=false
PathPrefix=Askew-Student-Life-Cinema

[DesignSettings]
SiteDesign=movies
AdditionalSiteDesignList[]=ezfind
AdditionalSiteDesignList[]=fsu-v01
AdditionalSiteDesignList[]=ezwebin
AdditionalSiteDesignList[]=base

[SearchSettings]
SearchEngine=ezsolr

*/ ?>