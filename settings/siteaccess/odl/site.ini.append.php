<?php /* #?ini charset="utf-8"?

[InformationCollectionSettings]
EmailReceiver=

[SiteSettings]
SiteShortName=Office of Distance Learning
SiteName=Office of Distance Learning
SiteURL=odl.ez.fsu.edu
LoginPage=embedded
IndexPage=/content/view/full/79307
RootNodeDepth=2
DefaultPage=/content/view/full/79307
MetaDataArray[author]=Florida State University
MetaDataArray[keywords]=Office of Distance Learning, Florida State University
MetaDataArray[description]=Office of Distance Learning, Florida State University 
MetaDataArray[classification]=Office of Distance Learning, Florida State University 

[UserSettings]
RegistrationEmail=

[SiteAccessSettings]
RequireUserLogin=false
RelatedSiteAccessList[]=odl
RelatedSiteAccessList[]=fsu_admin
ShowHiddenNodes=false
PathPrefix=Office-of-Distance-Learning

[DesignSettings]
SiteDesign=odl
AdditionalSiteDesignList[]=ezfind
AdditionalSiteDesignList[]=fsu-v01
AdditionalSiteDesignList[]=ezwebin
AdditionalSiteDesignList[]=base

[SearchSettings]
SearchEngine=ezsolr

*/ ?>