<?php /* #?ini charset="utf-8"?

[ImageMagick]
Filters[]=geometry/scaleheightuponly=-geometry x%1<

[AliasSettings]
AliasList[]=supergraphic_film

[AliasSettings]
AliasList[]=supergraphic_film_inner

[supergraphic_film]
Reference=
Filters[]=geometry/scalewidth=700
Filters[]=geometry/scaleheightuponly=279
Filters[]=centerimg=700;279
Filters[]=strip
Filters[]=colorspace=RGB


[supergraphic_film_inner]
Reference=
Filters[]=geometry/scalewidth=700
Filters[]=geometry/scaleheightuponly=130
Filters[]=centerimg=700;130
Filters[]=strip
Filters[]=colorspace=RGB
*/ ?>
