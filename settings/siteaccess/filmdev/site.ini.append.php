<?php /* #?ini charset="utf-8"?

[ContentSettings]
StaticCache=enabled

[InformationCollectionSettings]
EmailReceiver=

[SiteSettings]
SiteShortName=College of Motion Picture Arts
SiteName=College of Motion Picture Arts
SiteURL=filmdev.fsu.edu
LoginPage=embedded
IndexPage=/content/view/full/64133/
SiteRootNodeID=64133
RootNodeDepth=2
DefaultPage=/content/view/full/64133/
MetaDataArray[author]=Florida State University College of Motion Picture Arts
MetaDataArray[keywords]=education, post-secondary florida state university, graduate school, career advancement, film school
MetaDataArray[description]=Florida State University is a Carnegie Doctoral/Research 
MetaDataArray[classification]=Continuing Education, Professional Development, 

[UserSettings]
RegistrationEmail=

[SiteAccessSettings]
RequireUserLogin=false
ShowHiddenNodes=false
PathPrefix=Film-School-Dev

[DesignSettings]
SiteDesign=filmdev
AdditionalSiteDesignList[]=fsu
AdditionalSiteDesignList[]=fsudesign
AdditionalSiteDesignList[]=ezwebin
AdditionalSiteDesignList[]=base

[DebugSettings]
DebugOutput=enabled
DebugRedirection=disabled

[TemplateSettings]
Debug=disabled
ShowXHTMLCode=disabled
ShowUsedTemplates=enabled

[DatabaseSettings]
SQLOutput=disabled
*/ ?>
