<?php /* #?ini charset="utf-8"?

[InformationCollectionSettings]
EmailReceiver=

[SiteSettings]
SiteShortName=FSU Libraries
SiteName=FSU Libraries
SiteURL=lib.ezpublish2.fsu.edu
LoginPage=embedded
IndexPage=/content/view/full/44161
RootNodeDepth=2
DefaultPage=/content/view/full/44161
MetaDataArray[author]=FSU Libraries
MetaDataArray[keywords]=FSU Libraries, Florida State University
MetaDataArray[description]=Florida State University, FSU Libraries
MetaDataArray[classification]=Florida State University, FSU Libraries

[UserSettings]
RegistrationEmail=

[SiteAccessSettings]
RequireUserLogin=false
RelatedSiteAccessList[]=fsu_libraries
RelatedSiteAccessList[]=fsu_admin
ShowHiddenNodes=false
PathPrefix=FSU-Libraries

[DesignSettings]
SiteDesign=fsu_libraries
AdditionalSiteDesignList[]=ezfind
AdditionalSiteDesignList[]=fsudesign
AdditionalSiteDesignList[]=ezwebin
AdditionalSiteDesignList[]=base

[SearchSettings]
SearchEngine=ezsolr

*/ ?>