<?php /* #?ini charset="utf-8"?

[InformationCollectionSettings]
EmailReceiver=

[SiteSettings]
SiteShortName=Fine Arts Ticket Office
SiteName=Fine Arts Ticket Office 
SiteURL=tickets.ezpublish2.fsu.edu
LoginPage=embedded
IndexPage=/content/view/full/22683
RootNodeDepth=2
DefaultPage=/content/view/full/22683
MetaDataArray[author]=Fine Arts Ticket Office
MetaDataArray[keywords]=florida state university, Fine Arts Ticket Office, events, shows, entertainment
MetaDataArray[description]=florida state university, Fine Arts Ticket Office, events, shows, entertainment 
MetaDataArray[classification]=florida state university, Fine Arts Ticket Office, events, shows, entertainment

[UserSettings]
RegistrationEmail=

[SiteAccessSettings]
RequireUserLogin=false
RelatedSiteAccessList[]=tickets
RelatedSiteAccessList[]=fsu_admin
ShowHiddenNodes=false
PathPrefix=Fine-Arts-Ticket-Office

[DesignSettings]
SiteDesign=tickets
AdditionalSiteDesignList[]=fsu-v01
AdditionalSiteDesignList[]=ezwebin
AdditionalSiteDesignList[]=base

*/ ?>