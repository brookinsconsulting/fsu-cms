<?php /* #?ini charset="utf-8"?

[InformationCollectionSettings]
EmailReceiver=

[SiteSettings]
SiteShortName=LSI
SiteName=Learning Systems Institute
SiteURL=lsi.ez.fsu.edu
LoginPage=embedded
IndexPage=/content/view/full/93820
RootNodeDepth=2
DefaultPage=/content/view/full/93820
MetaDataArray[author]=Florida State University
MetaDataArray[keywords]=education, post-secondary florida state university, 
MetaDataArray[description]=Florida State University is a Carnegie Doctoral/Research 
MetaDataArray[classification]=Continuing Education, Professional Development, 

[UserSettings]
RegistrationEmail=

[SiteAccessSettings]
RequireUserLogin=false
RelatedSiteAccessList[]=lsi
RelatedSiteAccessList[]=fsu_admin
ShowHiddenNodes=false
PathPrefix=LSI

[DesignSettings]
SiteDesign=lsi
AdditionalSiteDesignList[]=ezfind
AdditionalSiteDesignList[]=fsu-v01
AdditionalSiteDesignList[]=ezwebin
AdditionalSiteDesignList[]=base

[SearchSettings]
SearchEngine=ezsolr

*/ ?>