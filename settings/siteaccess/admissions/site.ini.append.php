<?php /* #?ini charset="utf-8"?

[InformationCollectionSettings]
EmailReceiver=

[SiteSettings]
SiteShortName=Grad School
SiteName=Florida State University The Graduate School 
SiteURL=gradstudies.fsu.edu
LoginPage=embedded
IndexPage=/content/view/full/1058
RootNodeDepth=2
DefaultPage=/content/view/full/1058
MetaDataArray[author]=Florida State University The Graduate School
MetaDataArray[keywords]=education, post-secondary florida state university, graduate school, career advancement, 
MetaDataArray[description]=Florida State University is a Carnegie Doctoral/Research 
MetaDataArray[classification]=Continuing Education, Professional Development, 

[UserSettings]
RegistrationEmail=

[SiteAccessSettings]
RequireUserLogin=false
RelatedSiteAccessList[]=gradstudies
RelatedSiteAccessList[]=fsu_admin
ShowHiddenNodes=false
PathPrefix=The-Graduate-School

[DesignSettings]
SiteDesign=gradstudies
AdditionalSiteDesignList[]=fsu
AdditionalSiteDesignList[]=ezwebin
AdditionalSiteDesignList[]=base

*/ ?>