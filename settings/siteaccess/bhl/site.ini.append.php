<?php /* #?ini charset="utf-8"?

[InformationCollectionSettings]
EmailReceiver=

[SiteSettings]
SiteShortName=BHL Center
SiteName=BHL Center 
SiteURL=bhlcenter.ezpublish2.fsu.edu
LoginPage=embedded
IndexPage=/content/view/full/24006
RootNodeDepth=2
DefaultPage=/content/view/full/24006
MetaDataArray[author]=BHL Center, Center on Better Health and Life for Underserved Populations
MetaDataArray[keywords]=BHL Center, Center on Better Health and Life for Underserved Populations 
MetaDataArray[description]=BHL Center, Center on Better Health and Life for Underserved Populations 
MetaDataArray[classification]=BHL Center, Center on Better Health and Life for Underserved Populations

[UserSettings]
RegistrationEmail=

[SiteAccessSettings]
RequireUserLogin=false
RelatedSiteAccessList[]=bhl
RelatedSiteAccessList[]=fsu_admin
ShowHiddenNodes=false
PathPrefix=BHL-Center

[DesignSettings]
SiteDesign=bhl
AdditionalSiteDesignList[]=fsu-v02
AdditionalSiteDesignList[]=ezwebin
AdditionalSiteDesignList[]=base

[DebugSettings]
DebugOutput=enabled
DebugByUser=disabled
DebugRedirection=disabled

[TemplateSettings]
Debug=disabled
ShowXHTMLCode=enabled
ShowUsedTemplates=enabled

[DatabaseSettings]
SQLOutput=disabled
*/ ?>