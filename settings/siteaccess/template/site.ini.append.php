<?php /* #?ini charset="utf-8"?

[InformationCollectionSettings]
EmailReceiver=

[SiteSettings]
SiteShortName=WILL SHOW IN SEARCH
SiteName=WILL SHOW AS TITLE ABOVE BANNER
SiteURL=ez.fsu.edu
LoginPage=embedded
IndexPage=/content/view/full/000000
RootNodeDepth=2
DefaultPage=/content/view/full/000000
MetaDataArray[author]=Florida State University
MetaDataArray[keywords]=education, post-secondary florida state university, 
MetaDataArray[description]=Florida State University is a Carnegie Doctoral/Research 
MetaDataArray[classification]=Continuing Education, Professional Development, 

[UserSettings]
RegistrationEmail=

[SiteAccessSettings]
RequireUserLogin=false
RelatedSiteAccessList[]=SITEFOLDERNAME
RelatedSiteAccessList[]=fsu_admin
ShowHiddenNodes=false
PathPrefix=ACTUAL-FRONTPAGE-SHORTNAME-USING-HYPHENS

[DesignSettings]
SiteDesign=SITEFOLDERNAME
AdditionalSiteDesignList[]=ezfind
AdditionalSiteDesignList[]=fsu-v01
AdditionalSiteDesignList[]=ezwebin
AdditionalSiteDesignList[]=base

[SearchSettings]
SearchEngine=ezsolr

*/ ?>