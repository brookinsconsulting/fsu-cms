<?php /* #?ini charset="utf-8"?

[InformationCollectionSettings]
EmailReceiver=

[SiteSettings]
SiteShortName=Inspector General
SiteName=Office of Inspector General Services
SiteURL=igs.ez.fsu.edu
LoginPage=embedded
IndexPage=/content/view/full/16822
RootNodeDepth=2
DefaultPage=/content/view/full/16822
MetaDataArray[author]=Florida State University, Office of Inspector General Services
MetaDataArray[keywords]=Florida State University, Office of Inspector General Services, Ethics Officer, ethics, ethic, policy, fraudulent, unethical, dishonest, acts
MetaDataArray[description]=Florida State University, Office of Inspector General Services 
MetaDataArray[classification]=Florida State University, Office of Inspector General Services

[UserSettings]
RegistrationEmail=

[SiteAccessSettings]
RequireUserLogin=false
RelatedSiteAccessList[]=igs
RelatedSiteAccessList[]=fsu_admin
ShowHiddenNodes=false
PathPrefix=Office-of-Inspector-General-Services

[DesignSettings]
SiteDesign=igs
AdditionalSiteDesignList[]=fsudesign
AdditionalSiteDesignList[]=ezwebin
AdditionalSiteDesignList[]=base

*/ ?>