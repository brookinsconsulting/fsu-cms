<?php /* #?ini charset="utf-8"?

[InformationCollectionSettings]
EmailReceiver=

[SiteSettings]
SiteShortName=Retired Faculty
SiteName=Association of Retired Faculty
SiteURL=retiredfaculty.ez.fsu.edu
LoginPage=embedded
IndexPage=/content/view/full/88736
RootNodeDepth=2
DefaultPage=/content/view/full/88736
MetaDataArray[author]=Florida State University
MetaDataArray[keywords]=education, post-secondary florida state university, 
MetaDataArray[description]=Florida State University is a Carnegie Doctoral/Research 
MetaDataArray[classification]=Continuing Education, Professional Development, 

[UserSettings]
RegistrationEmail=

[SiteAccessSettings]
RequireUserLogin=false
RelatedSiteAccessList[]=retiredfaculty
RelatedSiteAccessList[]=fsu_admin
ShowHiddenNodes=false
PathPrefix=Association-of-Retired-Faculty

[DesignSettings]
SiteDesign=retiredfaculty
AdditionalSiteDesignList[]=ezfind
AdditionalSiteDesignList[]=fsu-v01
AdditionalSiteDesignList[]=ezwebin
AdditionalSiteDesignList[]=base

[SearchSettings]
SearchEngine=ezsolr

*/ ?>