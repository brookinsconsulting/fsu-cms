<?php /* #?ini charset="utf-8"?

[InformationCollectionSettings]
EmailReceiver=

[SiteSettings]
SiteShortName=Pres Retreat
SiteName=President's Retreat
SiteURL=presidentsretreat.ez.fsu.edu
LoginPage=embedded
IndexPage=/content/view/full/89666
RootNodeDepth=2
DefaultPage=/content/view/full/89666
MetaDataArray[author]=Florida State University
MetaDataArray[keywords]=education, post-secondary florida state university, 
MetaDataArray[description]=Florida State University is a Carnegie Doctoral/Research 
MetaDataArray[classification]=Continuing Education, Professional Development, 

[UserSettings]
RegistrationEmail=

[SiteAccessSettings]
RequireUserLogin=false
RelatedSiteAccessList[]=presretreat
RelatedSiteAccessList[]=fsu_admin
ShowHiddenNodes=false
PathPrefix=President-s-Retreat

[DesignSettings]
SiteDesign=presretreat
AdditionalSiteDesignList[]=ezfind
AdditionalSiteDesignList[]=fsudesign
AdditionalSiteDesignList[]=ezwebin
AdditionalSiteDesignList[]=base

[SearchSettings]
SearchEngine=ezsolr

*/ ?>