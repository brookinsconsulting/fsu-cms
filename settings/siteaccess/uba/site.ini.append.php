<?php /* #?ini charset="utf-8"?

[InformationCollectionSettings]
EmailReceiver=

[SiteSettings]
SiteShortName=UBA
SiteName=University Business Administrators Program 
SiteURL=uba.fsu.edu
LoginPage=embedded
IndexPage=/content/view/full/6621
RootNodeDepth=2
DefaultPage=/content/view/full/6621
MetaDataArray[author]=University Business Administrators Program 
MetaDataArray[keywords]=University Business Administrators Program, florida state university
MetaDataArray[description]=University Business Administrators Program 
MetaDataArray[classification]=University Business Administrators Program 

[UserSettings]
RegistrationEmail=

[SiteAccessSettings]
RequireUserLogin=false
RelatedSiteAccessList[]=uba
RelatedSiteAccessList[]=fsu_admin
ShowHiddenNodes=false
PathPrefix=UBA

[DesignSettings]
SiteDesign=uba
AdditionalSiteDesignList[]=fsu-v01
AdditionalSiteDesignList[]=ezwebin
AdditionalSiteDesignList[]=base

*/ ?>