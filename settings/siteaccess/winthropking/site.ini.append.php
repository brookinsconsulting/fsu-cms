<?php /* #?ini charset="utf-8"?

[InformationCollectionSettings]
EmailReceiver=

[SiteSettings]
SiteShortName=Winthrop-King Institute
SiteName=Winthrop-King Institute for Contemporary French and Francophone Studies
SiteURL=winthropking.ez.fsu.edu
LoginPage=embedded
IndexPage=/content/view/full/86599
RootNodeDepth=2
DefaultPage=/content/view/full/86599
MetaDataArray[author]=Florida State University, Winthrop-King Institute for Contemporary French and Francophone Studies
MetaDataArray[keywords]=Florida State University, Winthrop-King Institute for Contemporary French and Francophone Studies 
MetaDataArray[description]=Florida State University, Winthrop-King Institute for Contemporary French and Francophone Studies
MetaDataArray[classification]=Florida State University, Winthrop-King Institute for Contemporary French and Francophone Studies 

[UserSettings]
RegistrationEmail=

[SiteAccessSettings]
RequireUserLogin=false
RelatedSiteAccessList[]=winthropking
RelatedSiteAccessList[]=fsu_admin
ShowHiddenNodes=false
PathPrefix=Winthrop-King-Institute-for-Contemporary-French-and-Francophone-Studies

[DesignSettings]
SiteDesign=winthropking
AdditionalSiteDesignList[]=ezfind
AdditionalSiteDesignList[]=fsu-v02
AdditionalSiteDesignList[]=ezwebin
AdditionalSiteDesignList[]=base

[SearchSettings]
SearchEngine=ezsolr

*/ ?>