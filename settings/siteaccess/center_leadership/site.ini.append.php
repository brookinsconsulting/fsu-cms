<?php /* #?ini charset="utf-8"?

[InformationCollectionSettings]
EmailReceiver=

[SiteSettings]
SiteShortName=The Center for Leadership & Social Change
SiteName=The Center for Leadership & Social Change
SiteURL=thecenter.ez.fsu.edu
LoginPage=embedded
IndexPage=/content/view/full/75981
RootNodeDepth=2
DefaultPage=/content/view/full/75981
MetaDataArray[author]=Florida State University, The Center for Leadership & Social Change, student affairs
MetaDataArray[keywords]=Florida State University, The Center for Leadership & Social Change, student affairs 
MetaDataArray[description]=Florida State University, The Center for Leadership & Social Change, student affairs
MetaDataArray[classification]=Florida State University, The Center for Leadership & Social Change, student affairs 

[UserSettings]
RegistrationEmail=

[SiteAccessSettings]
RequireUserLogin=false
RelatedSiteAccessList[]=center_leadership
RelatedSiteAccessList[]=fsu_admin
ShowHiddenNodes=false
PathPrefix=The-Center-for-Leadership-Social-Change

[DesignSettings]
SiteDesign=center_leadership
AdditionalSiteDesignList[]=ezfind
AdditionalSiteDesignList[]=fsu-v02
AdditionalSiteDesignList[]=ezwebin
AdditionalSiteDesignList[]=base

[SearchSettings]
SearchEngine=ezsolr

*/ ?>