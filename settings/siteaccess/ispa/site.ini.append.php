<?php /* #?ini charset="utf-8"?

[InformationCollectionSettings]
EmailReceiver=

[SiteSettings]
SiteShortName=ISPA
SiteName=ISPA
SiteURL=ispa.ezpublish2.fsu.edu
LoginPage=embedded
IndexPage=/content/view/full/7175
RootNodeDepth=2
DefaultPage=/content/view/full/7175
MetaDataArray[author]=Institute of Science and Public Affairs
MetaDataArray[keywords]=Institute of Science and Public Affairs, ISPA
MetaDataArray[description]=Institute of Science and Public Affairs 
MetaDataArray[classification]=Institute of Science and Public Affairs

[UserSettings]
RegistrationEmail=

[SiteAccessSettings]
RequireUserLogin=false
RelatedSiteAccessList[]=ispa
RelatedSiteAccessList[]=fsu_admin
ShowHiddenNodes=false
PathPrefix=ISPA

[DesignSettings]
SiteDesign=ispa
AdditionalSiteDesignList[]=fsudesign
AdditionalSiteDesignList[]=ezwebin
AdditionalSiteDesignList[]=base

*/ ?>