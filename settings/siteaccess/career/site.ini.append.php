<?php /* #?ini charset="utf-8"?

[InformationCollectionSettings]
EmailReceiver=

[SiteSettings]
SiteShortName=Career Center
SiteName=The Career Center
SiteURL=ez.fsu.edu
LoginPage=embedded
IndexPage=/content/view/full/79788
RootNodeDepth=2
DefaultPage=/content/view/full/79788
MetaDataArray[author]=Florida State University
MetaDataArray[keywords]=education, post-secondary florida state university, 
MetaDataArray[description]=Florida State University is a Carnegie Doctoral/Research 
MetaDataArray[classification]=Continuing Education, Professional Development, 

[UserSettings]
RegistrationEmail=

[SiteAccessSettings]
RequireUserLogin=false
RelatedSiteAccessList[]=career
RelatedSiteAccessList[]=fsu_admin
ShowHiddenNodes=false
PathPrefix=Career-Center

[DesignSettings]
SiteDesign=career
AdditionalSiteDesignList[]=ezfind
AdditionalSiteDesignList[]=fsu-v01
AdditionalSiteDesignList[]=ezwebin
AdditionalSiteDesignList[]=base

[SearchSettings]
SearchEngine=ezsolr

*/ ?>