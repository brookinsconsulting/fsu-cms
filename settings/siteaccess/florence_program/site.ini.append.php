<?php /* #?ini charset="utf-8"?

[InformationCollectionSettings]
EmailReceiver=

[SiteSettings]
SiteShortName=Florence Program
SiteName=Florence Program
SiteURL=florenceinternational.ez.fsu.edu
LoginPage=embedded
IndexPage=/content/view/full/66744
RootNodeDepth=2
DefaultPage=/content/view/full/66744
MetaDataArray[author]=Florida State University. International Program, Study Abroad, Florence, Program
MetaDataArray[keywords]=Florida State University, International Program, Study Abroad, Florence, Program
MetaDataArray[description]=Florida State University, International Program, Study Abroad, Florence, Program
MetaDataArray[classification]=Florida State University, International Program, Study Abroad, Florence, Program

[UserSettings]
RegistrationEmail=

[SiteAccessSettings]
RequireUserLogin=false
RelatedSiteAccessList[]=florence_program
RelatedSiteAccessList[]=fsu_admin
ShowHiddenNodes=false
PathPrefix=Florence-Program

[DesignSettings]
SiteDesign=florence_program
AdditionalSiteDesignList[]=ezfind
AdditionalSiteDesignList[]=fsu-v01
AdditionalSiteDesignList[]=ezwebin
AdditionalSiteDesignList[]=base

[SearchSettings]
SearchEngine=ezsolr

*/ ?>