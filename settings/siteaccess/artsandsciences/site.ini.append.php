<?php /* #?ini charset="utf-8"?

[InformationCollectionSettings]
EmailReceiver=

[SiteSettings]
SiteShortName=Arts and Sciences
SiteName=College of Arts and Sciences
SiteURL=artsandsciences.fsu.edu
LoginPage=embedded
IndexPage=/content/view/full/4456
RootNodeDepth=2
DefaultPage=/content/view/full/4456
MetaDataArray[author]=Florida State University College of Arts and Sciences
MetaDataArray[keywords]=school, florida state university,arts, sciences, college
MetaDataArray[description]=Florida State University is a Carnegie Doctoral/Research 
MetaDataArray[classification]=Continuing Education, Professional Development, 

[UserSettings]
RegistrationEmail=

[SiteAccessSettings]
RequireUserLogin=false
RelatedSiteAccessList[]=artsandsciences_new
RelatedSiteAccessList[]=fsu_admin
ShowHiddenNodes=false
PathPrefix=Arts-and-Sciences

[DesignSettings]
SiteDesign=artsandsciences_new
AdditionalSiteDesignList[]=ezfind
AdditionalSiteDesignList[]=fsu-v02
AdditionalSiteDesignList[]=ezwebin
AdditionalSiteDesignList[]=base

[SearchSettings]
SearchEngine=ezsolr

*/ ?>