<?php /* #?ini charset="utf-8"?

[InformationCollectionSettings]
EmailReceiver=

[SiteSettings]
SiteShortName=Infrastructure
SiteName=Infrastructure
SiteURL=infrastructure.ais.ezpublish2.fsu.edu
LoginPage=embedded
IndexPage=/content/view/full/7024
RootNodeDepth=2
DefaultPage=/content/view/full/7024
MetaDataArray[author]=Infrastructure
MetaDataArray[keywords]=Infrastructure,florida state university,ais
MetaDataArray[description]=Infrastructure
MetaDataArray[classification]=Infrastructure

[UserSettings]
RegistrationEmail=

[SiteAccessSettings]
RequireUserLogin=false
RelatedSiteAccessList[]=infrastructure
RelatedSiteAccessList[]=fsu_admin
ShowHiddenNodes=false
PathPrefix=Infrastructure

[DesignSettings]
SiteDesign=infrastructure
AdditionalSiteDesignList[]=fsudesign
AdditionalSiteDesignList[]=ezwebin
AdditionalSiteDesignList[]=base

*/ ?>