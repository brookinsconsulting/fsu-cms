<?php /* #?ini charset="utf-8"?

[InformationCollectionSettings]
EmailReceiver=

[SiteSettings]
SiteShortName=Interfaith Council
SiteName=Interfaith Council
SiteURL=interfaithcouncil.ez.fsu.edu
LoginPage=embedded
IndexPage=/content/view/full/65032
RootNodeDepth=2
DefaultPage=/content/view/full/65032
MetaDataArray[author]=Florida State University
MetaDataArray[keywords]=Interfaith Council, Florida State University 
MetaDataArray[description]=Interfaith Council, Florida State University 
MetaDataArray[classification]=Interfaith Council, Florida State University

[UserSettings]
RegistrationEmail=

[SiteAccessSettings]
RequireUserLogin=false
RelatedSiteAccessList[]=interfaith_council
RelatedSiteAccessList[]=fsu_admin
ShowHiddenNodes=false
PathPrefix=Interfaith-Council

[DesignSettings]
SiteDesign=interfaith_council
AdditionalSiteDesignList[]=ezfind
AdditionalSiteDesignList[]=fsu-v01
AdditionalSiteDesignList[]=ezwebin
AdditionalSiteDesignList[]=base

[SearchSettings]
SearchEngine=ezsolr

*/ ?>