<?php /* #?ini charset="utf-8"?

[InformationCollectionSettings]
EmailReceiver=

[SiteSettings]
SiteShortName=Materials Science Program
SiteName=Materials Science Program 
SiteURL=msp.gradschool.fsu.edu
LoginPage=embedded
IndexPage=/content/view/full/36866
RootNodeDepth=2
DefaultPage=/content/view/full/36866
MetaDataArray[author]=Materials Science Program
MetaDataArray[keywords]=education, post-secondary florida state university, graduate school, career advancement, materials science program
MetaDataArray[description]=Florida State University is a Carnegie Doctoral/Research 
MetaDataArray[classification]=Continuing Education, Professional Development, 

[UserSettings]
RegistrationEmail=

[SiteAccessSettings]
RequireUserLogin=false
RelatedSiteAccessList[]=msp
RelatedSiteAccessList[]=fsu_admin
ShowHiddenNodes=false
PathPrefix=Materials-Science-Program

[DesignSettings]
SiteDesign=msp
AdditionalSiteDesignList[]=fsudesign
AdditionalSiteDesignList[]=ezwebin
AdditionalSiteDesignList[]=base

*/ ?>