<?php /* #?ini charset="utf-8"?

[InformationCollectionSettings]
EmailReceiver=

[SiteSettings]
SiteShortName=College of Business
SiteName=College of Business 
SiteURL=cob.fsu.edu
LoginPage=embedded
IndexPage=/content/view/full/3186
RootNodeDepth=2
DefaultPage=/content/view/full/3186
MetaDataArray[author]=Florida State University College of Business
MetaDataArray[keywords]=education, post-secondary florida state university, college, business, 
MetaDataArray[description]=Florida State University is a Carnegie Doctoral/Research 
MetaDataArray[classification]=Continuing Education, Professional Development, 

[UserSettings]
RegistrationEmail=

[SiteAccessSettings]
RequireUserLogin=false
RelatedSiteAccessList[]=business
RelatedSiteAccessList[]=fsu_admin
ShowHiddenNodes=false
PathPrefix=College-of-Business

[DesignSettings]
SiteDesign=business
AdditionalSiteDesignList[]=ezfind
AdditionalSiteDesignList[]=fsu-v01
AdditionalSiteDesignList[]=ezwebin
AdditionalSiteDesignList[]=base

[SearchSettings]
SearchEngine=ezsolr

[DebugSettings]
DebugOutput=enabled
DebugByUser=disabled
DebugRedirection=disabled

[TemplateSettings]
Debug=disabled
ShowXHTMLCode=enabled
ShowUsedTemplates=enabled

[DatabaseSettings]
SQLOutput=disabled
*/ ?>
