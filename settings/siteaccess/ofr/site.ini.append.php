<?php /* #?ini charset="utf-8"?

[InformationCollectionSettings]
EmailReceiver=

[SiteSettings]
SiteShortName=Office of Faculty Recognition
SiteName=Office of Faculty Recognition 
SiteURL=ofr.fsu.edu
LoginPage=embedded
IndexPage=/content/view/full/20633
RootNodeDepth=2
DefaultPage=/content/view/full/20633
MetaDataArray[author]=Florida State University Office of Faculty Recognition
MetaDataArray[keywords]=Florida State University Office of Faculty Recognition 
MetaDataArray[description]=Florida State University Office of Faculty Recognition 
MetaDataArray[classification]=Florida State University Office of Faculty Recognition

[UserSettings]
RegistrationEmail=

[SiteAccessSettings]
RequireUserLogin=false
RelatedSiteAccessList[]=ofr
RelatedSiteAccessList[]=fsu_admin
ShowHiddenNodes=false
PathPrefix=Office-of-Faculty-Recognition

[DesignSettings]
SiteDesign=ofr
AdditionalSiteDesignList[]=fsudesign
AdditionalSiteDesignList[]=ezwebin
AdditionalSiteDesignList[]=base

*/ ?>