<?php /* #?ini charset="utf-8"?

[InformationCollectionSettings]
EmailReceiver=

[SiteSettings]
SiteShortName=International Programs
SiteName=International Programs
SiteURL=international.ezpublish2.fsu.edu
LoginPage=embedded
IndexPage=/content/view/full/26003
RootNodeDepth=2
DefaultPage=/content/view/full/26003
MetaDataArray[author]=Florida State University International Programs
MetaDataArray[keywords]=Florida State University International Programs, study abroad 
MetaDataArray[description]=Florida State University International Programs, study abroad
MetaDataArray[classification]=Florida State University International Programs, study abroad 

[UserSettings]
RegistrationEmail=

[SiteAccessSettings]
RequireUserLogin=false
RelatedSiteAccessList[]=ip
RelatedSiteAccessList[]=fsu_admin
ShowHiddenNodes=false
PathPrefix=International-Programs

[DesignSettings]
SiteDesign=ip
AdditionalSiteDesignList[]=fsudesign
AdditionalSiteDesignList[]=ezwebin
AdditionalSiteDesignList[]=base

*/ ?>