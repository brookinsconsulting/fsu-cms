<?php /* #?ini charset="utf-8"?
[SSLZoneSettings]
SSLZones=enabled
ModuleViewAccessMode[user/login]=ssl

[InformationCollectionSettings]
EmailReceiver=

[SiteSettings]
SiteName=Florida State University
SiteURL=ezadmin.fsu.edu
LoginPage=custom
DefaultPage=content/dashboard

[UserSettings]
RegistrationEmail=

[SiteAccessSettings]
RequireUserLogin=true
ShowHiddenNodes=true
AnonymousAccessList[]=odf/upload_import
AnonymousAccessList[]=odf/authenticate
AnonymousAccessList[]=odf/upload_export

[DesignSettings]
SiteDesign=fsu_admin2
AdditionalSiteDesignList[]=fsu_admin
AdditionalSiteDesignList[]=admin2
AdditionalSiteDesignList[]=admin

[RegionalSettings]
Locale=eng-US
ContentObjectLocale=eng-US
ShowUntranslatedObjects=enabled
SiteLanguageList[]=eng-US
TextTranslation=disabled
TranslationExtensions[]=ezoe
TranslationExtensions[]=ezodf
TranslationExtensions[]=ezwebin

[FileSettings]
VarDir=var/ezwebin_site

[ContentSettings]
CachedViewPreferences[full]=admin_navigation_content=0;admin_navigation_details=0;admin_navigation_languages=0;admin_navigation_locations=0;admin_navigation_relations=0;admin_navigation_roles=0;admin_navigation_policies=0;admin_navigation_content=0;admin_navigation_translations=0;admin_children_viewmode=list;admin_list_limit=1;admin_edit_show_locations=0;admin_leftmenu_width=10;admin_url_list_limit=10;admin_url_view_limit=10;admin_section_list_limit=1;admin_orderlist_sortfield=user_name;admin_orderlist_sortorder=desc;admin_search_stats_limit=1;admin_treemenu=1;admin_bookmarkmenu=1;admin_left_menu_width=13
TranslationList=
ViewCaching=enabled

[MailSettings]
AdminEmail=david@thinkcreative.com
EmailSender=

[DebugSettings]
DebugOutput=enabled
DebugRedirection=disabled
DebugIPList[]=64.139.92.105
DebugIPList[]=82.247.156.143
DebugIPList[]=85.19.74.66
DebugIPList[]=98.230.27.191

[TemplateSettings]
Debug=disabled
ShowXHTMLCode=enabled
ShowUsedTemplates=enabled
ExtensionAutoloadPath[]=ezoe
ExtensionAutoloadPath[]=ezwebin
AutoloadPathList[]=extension/objectrelationbrowse/kernel/common/

[MediaClassSettings]
ImageClassIdentifiers[]=image

[ImageDataTypeSettings]
AvailableImageDataTypes[]=ezimage

[SSLZoneSettings]
ModuleViewAccessMode[ezoe/*]=keep

[RoleSettings]
PolicyOmitList[]=odf/upload_import
PolicyOmitList[]=odf/authenticate
PolicyOmitList[]=odf/upload_export

[SearchSettings]
SearchEngine=ezsolr

[DatabaseSettings]
SQLOutput=disabled
*/ ?>
