<?php /* #?ini charset="utf-8"?

[InformationCollectionSettings]
EmailReceiver=

[SiteSettings]
SiteShortName=Controller's Office
SiteName=Controller's Office
SiteURL=controller.vpfa.fsu.edu
LoginPage=embedded
IndexPage=/content/view/full/2568
RootNodeDepth=2
DefaultPage=/content/view/full/2568
MetaDataArray[author]=Florida State University, Controller's Office, VPFA, Finance and Administration
MetaDataArray[keywords]=Florida State University, Controller's Office, VPFA, Finance and Administration
MetaDataArray[description]=Florida State University, Controller's Office, VPFA, Finance and Administration
MetaDataArray[classification]=Florida State University, Controller's Office, VPFA, Finance and Administration 

[UserSettings]
RegistrationEmail=

[SiteAccessSettings]
RequireUserLogin=false
RelatedSiteAccessList[]=controller
RelatedSiteAccessList[]=fsu_admin
ShowHiddenNodes=false
PathPrefix=Controller-s-Office

[DesignSettings]
SiteDesign=controller
AdditionalSiteDesignList[]=ezfind
AdditionalSiteDesignList[]=fsu-v02
AdditionalSiteDesignList[]=ezwebin
AdditionalSiteDesignList[]=base

[SearchSettings]
SearchEngine=ezsolr

*/ ?>