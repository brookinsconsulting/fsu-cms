<?php /* #?ini charset="utf-8"?

[InformationCollectionSettings]
EmailReceiver=

[SiteSettings]
SiteShortName=NASPA
SiteName=NASPA and NOLES Undergraduate Fellows Program
SiteURL=n2ufpdsa.ezadmin.fsu.edu
LoginPage=embedded
IndexPage=/content/view/full/55722
RootNodeDepth=2
DefaultPage=/content/view/full/55722
MetaDataArray[author]=NASPA and NOLES Undergraduate Fellows Program
MetaDataArray[keywords]=education, post-secondary florida state university, undergraduate, naspa, fellows
MetaDataArray[description]=Florida State University is a Carnegie Doctoral/Research
MetaDataArray[classification]=Continuing Education, Professional Development

[UserSettings]
RegistrationEmail=

[SiteAccessSettings]
RequireUserLogin=false
RelatedSiteAccessList[]=naspa
RelatedSiteAccessList[]=fsu_admin
ShowHiddenNodes=false
PathPrefix=NASPA-and-NOLES-Undergraduate-Fellows-Program

[DesignSettings]
SiteDesign=naspa
AdditionalSiteDesignList[]=ezfind
AdditionalSiteDesignList[]=fsu-v01
AdditionalSiteDesignList[]=ezwebin
AdditionalSiteDesignList[]=base

[SearchSettings]
SearchEngine=ezsolr

*/ ?>