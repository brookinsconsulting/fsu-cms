<?php /* #?ini charset="utf-8"?

[InformationCollectionSettings]
EmailReceiver=

[SiteSettings]
SiteShortName=Modern Languages
SiteName=Florida State University Modern Languages & Linguistics 
SiteURL=modlang.ez.fsu.edu
LoginPage=embedded
IndexPage=/content/view/full/16817
RootNodeDepth=2
DefaultPage=/content/view/full/16817
MetaDataArray[author]=Florida State University Modern Languages & Linguistics 
MetaDataArray[keywords]=education, post-secondary florida state university, graduate school, career advancement, Modern Languages & Logistics 
MetaDataArray[description]=Florida State University is a Carnegie Doctoral/Research 
MetaDataArray[classification]=Continuing Education, Professional Development, Modern Languages & Linguistics

[UserSettings]
RegistrationEmail=

[SiteAccessSettings]
RequireUserLogin=false
RelatedSiteAccessList[]=modlang
RelatedSiteAccessList[]=fsu_admin
ShowHiddenNodes=false
PathPrefix=Department-of-Modern-Languages-and-Linguistics

[DesignSettings]
SiteDesign=modlang
AdditionalSiteDesignList[]=fsudesign
AdditionalSiteDesignList[]=ezwebin
AdditionalSiteDesignList[]=base

*/ ?>