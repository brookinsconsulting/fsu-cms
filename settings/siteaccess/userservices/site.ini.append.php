<?php /* #?ini charset="utf-8"?

[InformationCollectionSettings]
EmailReceiver=

[SiteSettings]
SiteShortName=OTI User Services
SiteName=OTI User Services
SiteURL=us.ezpublish2.fsu.edu
LoginPage=embedded
IndexPage=/content/view/full/15134
RootNodeDepth=2
DefaultPage=/content/view/full/15134
MetaDataArray[author]=OTI User Services
MetaDataArray[keywords]=education, post-secondary florida state university, graduate school, career advancement, 
MetaDataArray[description]=Florida State University is a Carnegie Doctoral/Research 
MetaDataArray[classification]=Continuing Education, Professional Development, 

[UserSettings]
RegistrationEmail=

[SiteAccessSettings]
RequireUserLogin=false
RelatedSiteAccessList[]=userservices
RelatedSiteAccessList[]=fsu_admin
ShowHiddenNodes=false
PathPrefix=Computing-User-Services

[DesignSettings]
SiteDesign=userservices
AdditionalSiteDesignList[]=fsudesign
AdditionalSiteDesignList[]=ezwebin
AdditionalSiteDesignList[]=base

*/ ?>