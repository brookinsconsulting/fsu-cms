<?php /* #?ini charset="utf-8"?

[InformationCollectionSettings]
EmailReceiver=

[SiteSettings]
SiteShortName=Department of Art
SiteName=Department of Art 
SiteURL=art.ezpublish2.fsu.edu
LoginPage=embedded
IndexPage=/content/view/full/22694
RootNodeDepth=2
DefaultPage=/content/view/full/22694
MetaDataArray[author]=Florida State University Department of Art
MetaDataArray[keywords]=florida state university, department of art
MetaDataArray[description]=florida state university, department of art 
MetaDataArray[classification]=florida state university, department of art 

[UserSettings]
RegistrationEmail=

[SiteAccessSettings]
RequireUserLogin=false
RelatedSiteAccessList[]=art
RelatedSiteAccessList[]=fsu_admin
ShowHiddenNodes=false
PathPrefix=Department-of-Art

[DesignSettings]
SiteDesign=art
AdditionalSiteDesignList[]=ezfind
AdditionalSiteDesignList[]=fsu-v01
AdditionalSiteDesignList[]=ezwebin
AdditionalSiteDesignList[]=base

[SearchSettings]
SearchEngine=ezsolr

*/ ?>