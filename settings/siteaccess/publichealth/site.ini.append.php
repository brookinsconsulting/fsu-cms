<?php /* #?ini charset="utf-8"?

[InformationCollectionSettings]
EmailReceiver=

[SiteSettings]
SiteShortName=Master's of Public Health
SiteName=Master's of Public Health 
SiteURL=publichealthcoss.ez.fsu.edu
LoginPage=embedded
IndexPage=/content/view/full/52672
RootNodeDepth=2
DefaultPage=/content/view/full/52672
MetaDataArray[author]=Master's of Public Health
MetaDataArray[keywords]=education, post-secondary florida state university, graduate school, career advancement, 
MetaDataArray[description]=Florida State University is a Carnegie Doctoral/Research 
MetaDataArray[classification]=Continuing Education, Professional Development, 

[UserSettings]
RegistrationEmail=

[SiteAccessSettings]
RequireUserLogin=false
RelatedSiteAccessList[]=publichealth
RelatedSiteAccessList[]=fsu_admin
ShowHiddenNodes=false
PathPrefix=Master's-of-Public-Health

[DesignSettings]
SiteDesign=publichealth
AdditionalSiteDesignList[]=ezfind
AdditionalSiteDesignList[]=fsu-v01
AdditionalSiteDesignList[]=ezwebin
AdditionalSiteDesignList[]=base

[SearchSettings]
SearchEngine=ezsolr

*/ ?>